﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;
// System.Collections.Generic.IList`1<CI.HttpClient.IHttpContent>
struct IList_1_t3315270331;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t1943082916;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t254341728;
// Library.Code.WebRequest.UserDataNetworkInfoProvider
struct UserDataNetworkInfoProvider_t529481330;
// System.IO.Stream
struct Stream_t3255436806;
// System.Text.Encoding
struct Encoding_t663144255;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct IEnumerable_1_t1993471762;
// System.Net.Cache.RequestCachePolicy
struct RequestCachePolicy_t2663429579;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t1197680765;
// System.Net.CookieContainer
struct CookieContainer_t2808809223;
// System.Net.ICredentials
struct ICredentials_t3855617113;
// System.Collections.Generic.IDictionary`2<System.Net.HttpRequestHeader,System.String>
struct IDictionary_2_t147951471;
// System.Net.IWebProxy
struct IWebProxy_t3916853445;
// System.Collections.Generic.List`1<System.Net.HttpWebRequest>
struct List_1_t1320525645;
// CI.HttpClient.Core.IDispatcher
struct IDispatcher_t3960463028;
// System.Action`1<CI.HttpClient.UploadStatusMessage>
struct Action_1_t3988964304;
// System.Net.HttpWebRequest
struct HttpWebRequest_t1951404513;
// System.Net.HttpWebResponse
struct HttpWebResponse_t2828383075;
// System.Uri
struct Uri_t19570940;
// System.Action`1<CI.HttpClient.HttpResponseMessage`1<System.String>>
struct Action_1_t1180396435;
// CI.HttpClient.HttpClient
struct HttpClient_t2113786063;
// CI.HttpClient.IHttpContent
struct IHttpContent_t2774329730;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// DrawerHandler
struct DrawerHandler_t321192673;
// UnityEngine.Transform
struct Transform_t3275118058;
// Library.Code.DI.DepedencyProvider
struct DepedencyProvider_t366508974;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// Library.Code.UI.Remark.NewRemarkHandlerMobile
struct NewRemarkHandlerMobile_t798103230;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// Library.Code.UI.PrefabGenerator.CreateInterface
struct CreateInterface_t1919332287;
// Library.Code.UI.PopUp.PopUpStuff
struct PopUpStuff_t3426923330;
// Library.Code.Facades.PathHolder.PathHolder
struct PathHolder_t2114773181;
// Library.Code.UI.Remark.RemarkHandlerMobileTwoPanels
struct RemarkHandlerMobileTwoPanels_t1827359049;
// Library.Code.UI.Remark.RemarkHandlerMobile
struct RemarkHandlerMobile_t1067069532;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Void
struct Void_t1841601450;
// UnityEngine.UI.Button
struct Button_t2872111280;
// System.Action
struct Action_t3226471752;
// System.Action`1<CI.HttpClient.HttpResponseMessage`1<System.Byte[]>>
struct Action_1_t2548510215;
// Library.Code.Networking._3dElement.DownloadElement3d
struct DownloadElement3d_t3227939199;
// Library.Code.DI.Element3d.Element3dModule
struct Element3dModule_t2912470317;
// Library.Code.DI.Elements.ElementProvider
struct ElementProvider_t3018113545;
// Library.Code.DI.Element3d.CheckpointModule
struct CheckpointModule_t504654608;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// Library.Code.DI.Checklist.ChecklistProvider
struct ChecklistProvider_t3393423783;
// Library.Code.DI.Element3d.Element3dProvider
struct Element3dProvider_t3146139312;
// Library.Code.DI.GroupinModel.GroupingModelProvider
struct GroupingModelProvider_t2551172685;
// Library.Code.DI.ElementHandler.ElementHandlerModule
struct ElementHandlerModule_t3201733362;
// Library.Code.Communication.Communication
struct Communication_t424466906;
// Library.Code.Facades.CheckListF.CurrentCheckListItemFacade
struct CurrentCheckListItemFacade_t552048566;
// Library.Code.Domain.Entities.Checklist.ItemStatus
struct ItemStatus_t1378751697;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t2530419979;
// UnityEngine.Font
struct Font_t4239498691;
// TMPro.TextMeshPro
struct TextMeshPro_t2521834357;
// TMPro.TextContainer
struct TextContainer_t4263764796;
// UnityEngine.TextMesh
struct TextMesh_t1641806576;
// UnityEngine.Material
struct Material_t193706927;
// Library.Code.DI.Message.MessageFacadeModule
struct MessageFacadeModule_t1147318505;
// Library.Code.DI.Message.SendMessageModule
struct SendMessageModule_t690017393;
// Library.Code.DI.SendFile.SendFileModule
struct SendFileModule_t3843131166;
// UnityEngine.UI.Slider
struct Slider_t297367283;
// Library.Code.Facades.ModelGrouping.ModelGroupFacade
struct ModelGroupFacade_t1233541942;
// Library.Code.UI.PrefabGenerator.PrefabGenerator
struct PrefabGenerator_t3099279183;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t2719087314;
// Library.Code.Facades.Messages.MessagesFacade
struct MessagesFacade_t3479558102;
// Library.Code.Facades.Camera.CameraFacade
struct CameraFacade_t4129143927;
// Library.Code.Facades._3dElementDownload.Element3dFacade
struct Element3dFacade_t3552582347;
// Library.Code.Domain.Entities.Element
struct Element_t2276588008;
// Library.Code.Facades.SubjectSingle`1<Library.Code.Domain.Dtos.ElementWithObject>
struct SubjectSingle_1_t2023482901;
// Library.Code.UI.RotateButton
struct RotateButton_t1854395031;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// DrawerScene
struct DrawerScene_t240065691;
// UnityEngine.Sprite
struct Sprite_t309593783;
// System.Action`1<UnityEngine.GameObject>
struct Action_1_t1558332529;
// Library.Code.DI.Prefab.PrefabProvider
struct PrefabProvider_t1422780085;
// Library.Code.DI.MessageTempaltes.MessageTemplatesProvider
struct MessageTemplatesProvider_t2923081561;
// Library.Code.DI.Message.MessageProvider
struct MessageProvider_t618924372;
// Library.Code.DI.PopUp.PopUpShower
struct PopUpShower_t3186557280;
// LanguageStrings
struct LanguageStrings_t1206686884;
// UnityEngine.Animator
struct Animator_t69676727;
// System.Collections.Generic.Queue`1<System.Action>
struct Queue_1_t3046128587;
// Library.Code.DI.MessageTempaltes.MessageTempaltesModule
struct MessageTempaltesModule_t744814112;
// Library.Code.DI.Checklist.ChecklistModule
struct ChecklistModule_t3684412132;
// Library.Code.DI.Elements.ElementApproveStatusModule
struct ElementApproveStatusModule_t4176563737;
// Library.Code.DI.Elements.ElementFacadeModule
struct ElementFacadeModule_t1081315832;
// Library.Code.DI.GroupinModel.GroupingModelModule
struct GroupingModelModule_t337983572;
// Library.Code.DI.Models.ModelFacadeModule
struct ModelFacadeModule_t3252945753;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef MULTIPARTCONTENT_T115045161_H
#define MULTIPARTCONTENT_T115045161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.MultipartContent
struct  MultipartContent_t115045161  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<CI.HttpClient.IHttpContent> CI.HttpClient.MultipartContent::_content
	RuntimeObject* ____content_1;
	// System.String CI.HttpClient.MultipartContent::_boundary
	String_t* ____boundary_2;
	// System.Int64 CI.HttpClient.MultipartContent::_contentLength
	int64_t ____contentLength_3;
	// System.Byte[] CI.HttpClient.MultipartContent::<BoundaryStartBytes>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CBoundaryStartBytesU3Ek__BackingField_4;
	// System.Byte[] CI.HttpClient.MultipartContent::<BoundaryEndBytes>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CBoundaryEndBytesU3Ek__BackingField_5;
	// System.Byte[] CI.HttpClient.MultipartContent::<CRLFBytes>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CCRLFBytesU3Ek__BackingField_6;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> CI.HttpClient.MultipartContent::<Headers>k__BackingField
	RuntimeObject* ___U3CHeadersU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of__content_1() { return static_cast<int32_t>(offsetof(MultipartContent_t115045161, ____content_1)); }
	inline RuntimeObject* get__content_1() const { return ____content_1; }
	inline RuntimeObject** get_address_of__content_1() { return &____content_1; }
	inline void set__content_1(RuntimeObject* value)
	{
		____content_1 = value;
		Il2CppCodeGenWriteBarrier((&____content_1), value);
	}

	inline static int32_t get_offset_of__boundary_2() { return static_cast<int32_t>(offsetof(MultipartContent_t115045161, ____boundary_2)); }
	inline String_t* get__boundary_2() const { return ____boundary_2; }
	inline String_t** get_address_of__boundary_2() { return &____boundary_2; }
	inline void set__boundary_2(String_t* value)
	{
		____boundary_2 = value;
		Il2CppCodeGenWriteBarrier((&____boundary_2), value);
	}

	inline static int32_t get_offset_of__contentLength_3() { return static_cast<int32_t>(offsetof(MultipartContent_t115045161, ____contentLength_3)); }
	inline int64_t get__contentLength_3() const { return ____contentLength_3; }
	inline int64_t* get_address_of__contentLength_3() { return &____contentLength_3; }
	inline void set__contentLength_3(int64_t value)
	{
		____contentLength_3 = value;
	}

	inline static int32_t get_offset_of_U3CBoundaryStartBytesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MultipartContent_t115045161, ___U3CBoundaryStartBytesU3Ek__BackingField_4)); }
	inline ByteU5BU5D_t3397334013* get_U3CBoundaryStartBytesU3Ek__BackingField_4() const { return ___U3CBoundaryStartBytesU3Ek__BackingField_4; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CBoundaryStartBytesU3Ek__BackingField_4() { return &___U3CBoundaryStartBytesU3Ek__BackingField_4; }
	inline void set_U3CBoundaryStartBytesU3Ek__BackingField_4(ByteU5BU5D_t3397334013* value)
	{
		___U3CBoundaryStartBytesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBoundaryStartBytesU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CBoundaryEndBytesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(MultipartContent_t115045161, ___U3CBoundaryEndBytesU3Ek__BackingField_5)); }
	inline ByteU5BU5D_t3397334013* get_U3CBoundaryEndBytesU3Ek__BackingField_5() const { return ___U3CBoundaryEndBytesU3Ek__BackingField_5; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CBoundaryEndBytesU3Ek__BackingField_5() { return &___U3CBoundaryEndBytesU3Ek__BackingField_5; }
	inline void set_U3CBoundaryEndBytesU3Ek__BackingField_5(ByteU5BU5D_t3397334013* value)
	{
		___U3CBoundaryEndBytesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBoundaryEndBytesU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CCRLFBytesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(MultipartContent_t115045161, ___U3CCRLFBytesU3Ek__BackingField_6)); }
	inline ByteU5BU5D_t3397334013* get_U3CCRLFBytesU3Ek__BackingField_6() const { return ___U3CCRLFBytesU3Ek__BackingField_6; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CCRLFBytesU3Ek__BackingField_6() { return &___U3CCRLFBytesU3Ek__BackingField_6; }
	inline void set_U3CCRLFBytesU3Ek__BackingField_6(ByteU5BU5D_t3397334013* value)
	{
		___U3CCRLFBytesU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCRLFBytesU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(MultipartContent_t115045161, ___U3CHeadersU3Ek__BackingField_7)); }
	inline RuntimeObject* get_U3CHeadersU3Ek__BackingField_7() const { return ___U3CHeadersU3Ek__BackingField_7; }
	inline RuntimeObject** get_address_of_U3CHeadersU3Ek__BackingField_7() { return &___U3CHeadersU3Ek__BackingField_7; }
	inline void set_U3CHeadersU3Ek__BackingField_7(RuntimeObject* value)
	{
		___U3CHeadersU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTIPARTCONTENT_T115045161_H
#ifndef U3CRETRUNUNITYWEBREQUESTU3EC__ANONSTOREY0_T3849400577_H
#define U3CRETRUNUNITYWEBREQUESTU3EC__ANONSTOREY0_T3849400577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.WebRequest.WebRequestBuilder/<retrunUnityWebRequest>c__AnonStorey0
struct  U3CretrunUnityWebRequestU3Ec__AnonStorey0_t3849400577  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest Library.Code.WebRequest.WebRequestBuilder/<retrunUnityWebRequest>c__AnonStorey0::request
	UnityWebRequest_t254341728 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(U3CretrunUnityWebRequestU3Ec__AnonStorey0_t3849400577, ___request_0)); }
	inline UnityWebRequest_t254341728 * get_request_0() const { return ___request_0; }
	inline UnityWebRequest_t254341728 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(UnityWebRequest_t254341728 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRETRUNUNITYWEBREQUESTU3EC__ANONSTOREY0_T3849400577_H
#ifndef HEADERCLASS_T2331629453_H
#define HEADERCLASS_T2331629453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.WebRequest.HeaderClass
struct  HeaderClass_t2331629453  : public RuntimeObject
{
public:
	// System.String Library.Code.WebRequest.HeaderClass::header
	String_t* ___header_0;
	// System.String Library.Code.WebRequest.HeaderClass::headerValue
	String_t* ___headerValue_1;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderClass_t2331629453, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((&___header_0), value);
	}

	inline static int32_t get_offset_of_headerValue_1() { return static_cast<int32_t>(offsetof(HeaderClass_t2331629453, ___headerValue_1)); }
	inline String_t* get_headerValue_1() const { return ___headerValue_1; }
	inline String_t** get_address_of_headerValue_1() { return &___headerValue_1; }
	inline void set_headerValue_1(String_t* value)
	{
		___headerValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___headerValue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERCLASS_T2331629453_H
#ifndef WEBREQUEST_T921143439_H
#define WEBREQUEST_T921143439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.WebRequest.WebRequest
struct  WebRequest_t921143439  : public RuntimeObject
{
public:
	// Library.Code.WebRequest.UserDataNetworkInfoProvider Library.Code.WebRequest.WebRequest::_dataNetworkInfoProvider
	RuntimeObject* ____dataNetworkInfoProvider_0;

public:
	inline static int32_t get_offset_of__dataNetworkInfoProvider_0() { return static_cast<int32_t>(offsetof(WebRequest_t921143439, ____dataNetworkInfoProvider_0)); }
	inline RuntimeObject* get__dataNetworkInfoProvider_0() const { return ____dataNetworkInfoProvider_0; }
	inline RuntimeObject** get_address_of__dataNetworkInfoProvider_0() { return &____dataNetworkInfoProvider_0; }
	inline void set__dataNetworkInfoProvider_0(RuntimeObject* value)
	{
		____dataNetworkInfoProvider_0 = value;
		Il2CppCodeGenWriteBarrier((&____dataNetworkInfoProvider_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUEST_T921143439_H
#ifndef RETURNASSETBUNDLEFORELEMENT_T2016413217_H
#define RETURNASSETBUNDLEFORELEMENT_T2016413217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Utils.Platforms.ReturnAssetBundleForElement
struct  ReturnAssetBundleForElement_t2016413217  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RETURNASSETBUNDLEFORELEMENT_T2016413217_H
#ifndef FILEREADERIMPL_T2415093035_H
#define FILEREADERIMPL_T2415093035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Utils.FileReader.FileReaderImpl
struct  FileReaderImpl_t2415093035  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEREADERIMPL_T2415093035_H
#ifndef STREAMCONTENT_T2445532195_H
#define STREAMCONTENT_T2445532195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.StreamContent
struct  StreamContent_t2445532195  : public RuntimeObject
{
public:
	// System.IO.Stream CI.HttpClient.StreamContent::_stream
	Stream_t3255436806 * ____stream_0;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> CI.HttpClient.StreamContent::<Headers>k__BackingField
	RuntimeObject* ___U3CHeadersU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of__stream_0() { return static_cast<int32_t>(offsetof(StreamContent_t2445532195, ____stream_0)); }
	inline Stream_t3255436806 * get__stream_0() const { return ____stream_0; }
	inline Stream_t3255436806 ** get_address_of__stream_0() { return &____stream_0; }
	inline void set__stream_0(Stream_t3255436806 * value)
	{
		____stream_0 = value;
		Il2CppCodeGenWriteBarrier((&____stream_0), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(StreamContent_t2445532195, ___U3CHeadersU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CHeadersU3Ek__BackingField_1() const { return ___U3CHeadersU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CHeadersU3Ek__BackingField_1() { return &___U3CHeadersU3Ek__BackingField_1; }
	inline void set_U3CHeadersU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CHeadersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMCONTENT_T2445532195_H
#ifndef STRINGCONTENT_T615964002_H
#define STRINGCONTENT_T615964002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.StringContent
struct  StringContent_t615964002  : public RuntimeObject
{
public:
	// System.String CI.HttpClient.StringContent::_content
	String_t* ____content_1;
	// System.Text.Encoding CI.HttpClient.StringContent::_encoding
	Encoding_t663144255 * ____encoding_2;
	// System.Byte[] CI.HttpClient.StringContent::_serialisedContent
	ByteU5BU5D_t3397334013* ____serialisedContent_3;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> CI.HttpClient.StringContent::<Headers>k__BackingField
	RuntimeObject* ___U3CHeadersU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of__content_1() { return static_cast<int32_t>(offsetof(StringContent_t615964002, ____content_1)); }
	inline String_t* get__content_1() const { return ____content_1; }
	inline String_t** get_address_of__content_1() { return &____content_1; }
	inline void set__content_1(String_t* value)
	{
		____content_1 = value;
		Il2CppCodeGenWriteBarrier((&____content_1), value);
	}

	inline static int32_t get_offset_of__encoding_2() { return static_cast<int32_t>(offsetof(StringContent_t615964002, ____encoding_2)); }
	inline Encoding_t663144255 * get__encoding_2() const { return ____encoding_2; }
	inline Encoding_t663144255 ** get_address_of__encoding_2() { return &____encoding_2; }
	inline void set__encoding_2(Encoding_t663144255 * value)
	{
		____encoding_2 = value;
		Il2CppCodeGenWriteBarrier((&____encoding_2), value);
	}

	inline static int32_t get_offset_of__serialisedContent_3() { return static_cast<int32_t>(offsetof(StringContent_t615964002, ____serialisedContent_3)); }
	inline ByteU5BU5D_t3397334013* get__serialisedContent_3() const { return ____serialisedContent_3; }
	inline ByteU5BU5D_t3397334013** get_address_of__serialisedContent_3() { return &____serialisedContent_3; }
	inline void set__serialisedContent_3(ByteU5BU5D_t3397334013* value)
	{
		____serialisedContent_3 = value;
		Il2CppCodeGenWriteBarrier((&____serialisedContent_3), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(StringContent_t615964002, ___U3CHeadersU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CHeadersU3Ek__BackingField_4() const { return ___U3CHeadersU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CHeadersU3Ek__BackingField_4() { return &___U3CHeadersU3Ek__BackingField_4; }
	inline void set_U3CHeadersU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CHeadersU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGCONTENT_T615964002_H
#ifndef UPLOADSTATUSMESSAGE_T4187164922_H
#define UPLOADSTATUSMESSAGE_T4187164922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.UploadStatusMessage
struct  UploadStatusMessage_t4187164922  : public RuntimeObject
{
public:
	// System.Int64 CI.HttpClient.UploadStatusMessage::<ContentLength>k__BackingField
	int64_t ___U3CContentLengthU3Ek__BackingField_0;
	// System.Int64 CI.HttpClient.UploadStatusMessage::<TotalContentUploaded>k__BackingField
	int64_t ___U3CTotalContentUploadedU3Ek__BackingField_1;
	// System.Int64 CI.HttpClient.UploadStatusMessage::<ContentUploadedThisRound>k__BackingField
	int64_t ___U3CContentUploadedThisRoundU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CContentLengthU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UploadStatusMessage_t4187164922, ___U3CContentLengthU3Ek__BackingField_0)); }
	inline int64_t get_U3CContentLengthU3Ek__BackingField_0() const { return ___U3CContentLengthU3Ek__BackingField_0; }
	inline int64_t* get_address_of_U3CContentLengthU3Ek__BackingField_0() { return &___U3CContentLengthU3Ek__BackingField_0; }
	inline void set_U3CContentLengthU3Ek__BackingField_0(int64_t value)
	{
		___U3CContentLengthU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CTotalContentUploadedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UploadStatusMessage_t4187164922, ___U3CTotalContentUploadedU3Ek__BackingField_1)); }
	inline int64_t get_U3CTotalContentUploadedU3Ek__BackingField_1() const { return ___U3CTotalContentUploadedU3Ek__BackingField_1; }
	inline int64_t* get_address_of_U3CTotalContentUploadedU3Ek__BackingField_1() { return &___U3CTotalContentUploadedU3Ek__BackingField_1; }
	inline void set_U3CTotalContentUploadedU3Ek__BackingField_1(int64_t value)
	{
		___U3CTotalContentUploadedU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CContentUploadedThisRoundU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UploadStatusMessage_t4187164922, ___U3CContentUploadedThisRoundU3Ek__BackingField_2)); }
	inline int64_t get_U3CContentUploadedThisRoundU3Ek__BackingField_2() const { return ___U3CContentUploadedThisRoundU3Ek__BackingField_2; }
	inline int64_t* get_address_of_U3CContentUploadedThisRoundU3Ek__BackingField_2() { return &___U3CContentUploadedThisRoundU3Ek__BackingField_2; }
	inline void set_U3CContentUploadedThisRoundU3Ek__BackingField_2(int64_t value)
	{
		___U3CContentUploadedThisRoundU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADSTATUSMESSAGE_T4187164922_H
#ifndef FORMURLENCODEDCONTENT_T50233144_H
#define FORMURLENCODEDCONTENT_T50233144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.FormUrlEncodedContent
struct  FormUrlEncodedContent_t50233144  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> CI.HttpClient.FormUrlEncodedContent::_nameValueCollection
	RuntimeObject* ____nameValueCollection_0;
	// System.Byte[] CI.HttpClient.FormUrlEncodedContent::_serialisedContent
	ByteU5BU5D_t3397334013* ____serialisedContent_1;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> CI.HttpClient.FormUrlEncodedContent::<Headers>k__BackingField
	RuntimeObject* ___U3CHeadersU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of__nameValueCollection_0() { return static_cast<int32_t>(offsetof(FormUrlEncodedContent_t50233144, ____nameValueCollection_0)); }
	inline RuntimeObject* get__nameValueCollection_0() const { return ____nameValueCollection_0; }
	inline RuntimeObject** get_address_of__nameValueCollection_0() { return &____nameValueCollection_0; }
	inline void set__nameValueCollection_0(RuntimeObject* value)
	{
		____nameValueCollection_0 = value;
		Il2CppCodeGenWriteBarrier((&____nameValueCollection_0), value);
	}

	inline static int32_t get_offset_of__serialisedContent_1() { return static_cast<int32_t>(offsetof(FormUrlEncodedContent_t50233144, ____serialisedContent_1)); }
	inline ByteU5BU5D_t3397334013* get__serialisedContent_1() const { return ____serialisedContent_1; }
	inline ByteU5BU5D_t3397334013** get_address_of__serialisedContent_1() { return &____serialisedContent_1; }
	inline void set__serialisedContent_1(ByteU5BU5D_t3397334013* value)
	{
		____serialisedContent_1 = value;
		Il2CppCodeGenWriteBarrier((&____serialisedContent_1), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FormUrlEncodedContent_t50233144, ___U3CHeadersU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CHeadersU3Ek__BackingField_2() const { return ___U3CHeadersU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CHeadersU3Ek__BackingField_2() { return &___U3CHeadersU3Ek__BackingField_2; }
	inline void set_U3CHeadersU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CHeadersU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMURLENCODEDCONTENT_T50233144_H
#ifndef HTTPCLIENT_T2113786063_H
#define HTTPCLIENT_T2113786063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.HttpClient
struct  HttpClient_t2113786063  : public RuntimeObject
{
public:
	// System.Int32 CI.HttpClient.HttpClient::<DownloadBlockSize>k__BackingField
	int32_t ___U3CDownloadBlockSizeU3Ek__BackingField_4;
	// System.Int32 CI.HttpClient.HttpClient::<UploadBlockSize>k__BackingField
	int32_t ___U3CUploadBlockSizeU3Ek__BackingField_5;
	// System.Int32 CI.HttpClient.HttpClient::<Timeout>k__BackingField
	int32_t ___U3CTimeoutU3Ek__BackingField_6;
	// System.Int32 CI.HttpClient.HttpClient::<ReadWriteTimeout>k__BackingField
	int32_t ___U3CReadWriteTimeoutU3Ek__BackingField_7;
	// System.Net.Cache.RequestCachePolicy CI.HttpClient.HttpClient::<Cache>k__BackingField
	RequestCachePolicy_t2663429579 * ___U3CCacheU3Ek__BackingField_8;
	// System.Security.Cryptography.X509Certificates.X509CertificateCollection CI.HttpClient.HttpClient::<Certificates>k__BackingField
	X509CertificateCollection_t1197680765 * ___U3CCertificatesU3Ek__BackingField_9;
	// System.Net.CookieContainer CI.HttpClient.HttpClient::<Cookies>k__BackingField
	CookieContainer_t2808809223 * ___U3CCookiesU3Ek__BackingField_10;
	// System.Net.ICredentials CI.HttpClient.HttpClient::<Credentials>k__BackingField
	RuntimeObject* ___U3CCredentialsU3Ek__BackingField_11;
	// System.Boolean CI.HttpClient.HttpClient::<KeepAlive>k__BackingField
	bool ___U3CKeepAliveU3Ek__BackingField_12;
	// System.Collections.Generic.IDictionary`2<System.Net.HttpRequestHeader,System.String> CI.HttpClient.HttpClient::<Headers>k__BackingField
	RuntimeObject* ___U3CHeadersU3Ek__BackingField_13;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> CI.HttpClient.HttpClient::<CustomHeaders>k__BackingField
	RuntimeObject* ___U3CCustomHeadersU3Ek__BackingField_14;
	// System.Net.IWebProxy CI.HttpClient.HttpClient::<Proxy>k__BackingField
	RuntimeObject* ___U3CProxyU3Ek__BackingField_15;
	// System.Collections.Generic.List`1<System.Net.HttpWebRequest> CI.HttpClient.HttpClient::_requests
	List_1_t1320525645 * ____requests_16;
	// System.Object CI.HttpClient.HttpClient::_lock
	RuntimeObject * ____lock_17;

public:
	inline static int32_t get_offset_of_U3CDownloadBlockSizeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HttpClient_t2113786063, ___U3CDownloadBlockSizeU3Ek__BackingField_4)); }
	inline int32_t get_U3CDownloadBlockSizeU3Ek__BackingField_4() const { return ___U3CDownloadBlockSizeU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CDownloadBlockSizeU3Ek__BackingField_4() { return &___U3CDownloadBlockSizeU3Ek__BackingField_4; }
	inline void set_U3CDownloadBlockSizeU3Ek__BackingField_4(int32_t value)
	{
		___U3CDownloadBlockSizeU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CUploadBlockSizeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(HttpClient_t2113786063, ___U3CUploadBlockSizeU3Ek__BackingField_5)); }
	inline int32_t get_U3CUploadBlockSizeU3Ek__BackingField_5() const { return ___U3CUploadBlockSizeU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CUploadBlockSizeU3Ek__BackingField_5() { return &___U3CUploadBlockSizeU3Ek__BackingField_5; }
	inline void set_U3CUploadBlockSizeU3Ek__BackingField_5(int32_t value)
	{
		___U3CUploadBlockSizeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CTimeoutU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(HttpClient_t2113786063, ___U3CTimeoutU3Ek__BackingField_6)); }
	inline int32_t get_U3CTimeoutU3Ek__BackingField_6() const { return ___U3CTimeoutU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CTimeoutU3Ek__BackingField_6() { return &___U3CTimeoutU3Ek__BackingField_6; }
	inline void set_U3CTimeoutU3Ek__BackingField_6(int32_t value)
	{
		___U3CTimeoutU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CReadWriteTimeoutU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(HttpClient_t2113786063, ___U3CReadWriteTimeoutU3Ek__BackingField_7)); }
	inline int32_t get_U3CReadWriteTimeoutU3Ek__BackingField_7() const { return ___U3CReadWriteTimeoutU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CReadWriteTimeoutU3Ek__BackingField_7() { return &___U3CReadWriteTimeoutU3Ek__BackingField_7; }
	inline void set_U3CReadWriteTimeoutU3Ek__BackingField_7(int32_t value)
	{
		___U3CReadWriteTimeoutU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CCacheU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(HttpClient_t2113786063, ___U3CCacheU3Ek__BackingField_8)); }
	inline RequestCachePolicy_t2663429579 * get_U3CCacheU3Ek__BackingField_8() const { return ___U3CCacheU3Ek__BackingField_8; }
	inline RequestCachePolicy_t2663429579 ** get_address_of_U3CCacheU3Ek__BackingField_8() { return &___U3CCacheU3Ek__BackingField_8; }
	inline void set_U3CCacheU3Ek__BackingField_8(RequestCachePolicy_t2663429579 * value)
	{
		___U3CCacheU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCacheU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CCertificatesU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(HttpClient_t2113786063, ___U3CCertificatesU3Ek__BackingField_9)); }
	inline X509CertificateCollection_t1197680765 * get_U3CCertificatesU3Ek__BackingField_9() const { return ___U3CCertificatesU3Ek__BackingField_9; }
	inline X509CertificateCollection_t1197680765 ** get_address_of_U3CCertificatesU3Ek__BackingField_9() { return &___U3CCertificatesU3Ek__BackingField_9; }
	inline void set_U3CCertificatesU3Ek__BackingField_9(X509CertificateCollection_t1197680765 * value)
	{
		___U3CCertificatesU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCertificatesU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CCookiesU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(HttpClient_t2113786063, ___U3CCookiesU3Ek__BackingField_10)); }
	inline CookieContainer_t2808809223 * get_U3CCookiesU3Ek__BackingField_10() const { return ___U3CCookiesU3Ek__BackingField_10; }
	inline CookieContainer_t2808809223 ** get_address_of_U3CCookiesU3Ek__BackingField_10() { return &___U3CCookiesU3Ek__BackingField_10; }
	inline void set_U3CCookiesU3Ek__BackingField_10(CookieContainer_t2808809223 * value)
	{
		___U3CCookiesU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCookiesU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CCredentialsU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(HttpClient_t2113786063, ___U3CCredentialsU3Ek__BackingField_11)); }
	inline RuntimeObject* get_U3CCredentialsU3Ek__BackingField_11() const { return ___U3CCredentialsU3Ek__BackingField_11; }
	inline RuntimeObject** get_address_of_U3CCredentialsU3Ek__BackingField_11() { return &___U3CCredentialsU3Ek__BackingField_11; }
	inline void set_U3CCredentialsU3Ek__BackingField_11(RuntimeObject* value)
	{
		___U3CCredentialsU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCredentialsU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CKeepAliveU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(HttpClient_t2113786063, ___U3CKeepAliveU3Ek__BackingField_12)); }
	inline bool get_U3CKeepAliveU3Ek__BackingField_12() const { return ___U3CKeepAliveU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CKeepAliveU3Ek__BackingField_12() { return &___U3CKeepAliveU3Ek__BackingField_12; }
	inline void set_U3CKeepAliveU3Ek__BackingField_12(bool value)
	{
		___U3CKeepAliveU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(HttpClient_t2113786063, ___U3CHeadersU3Ek__BackingField_13)); }
	inline RuntimeObject* get_U3CHeadersU3Ek__BackingField_13() const { return ___U3CHeadersU3Ek__BackingField_13; }
	inline RuntimeObject** get_address_of_U3CHeadersU3Ek__BackingField_13() { return &___U3CHeadersU3Ek__BackingField_13; }
	inline void set_U3CHeadersU3Ek__BackingField_13(RuntimeObject* value)
	{
		___U3CHeadersU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CCustomHeadersU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(HttpClient_t2113786063, ___U3CCustomHeadersU3Ek__BackingField_14)); }
	inline RuntimeObject* get_U3CCustomHeadersU3Ek__BackingField_14() const { return ___U3CCustomHeadersU3Ek__BackingField_14; }
	inline RuntimeObject** get_address_of_U3CCustomHeadersU3Ek__BackingField_14() { return &___U3CCustomHeadersU3Ek__BackingField_14; }
	inline void set_U3CCustomHeadersU3Ek__BackingField_14(RuntimeObject* value)
	{
		___U3CCustomHeadersU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomHeadersU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CProxyU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(HttpClient_t2113786063, ___U3CProxyU3Ek__BackingField_15)); }
	inline RuntimeObject* get_U3CProxyU3Ek__BackingField_15() const { return ___U3CProxyU3Ek__BackingField_15; }
	inline RuntimeObject** get_address_of_U3CProxyU3Ek__BackingField_15() { return &___U3CProxyU3Ek__BackingField_15; }
	inline void set_U3CProxyU3Ek__BackingField_15(RuntimeObject* value)
	{
		___U3CProxyU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProxyU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of__requests_16() { return static_cast<int32_t>(offsetof(HttpClient_t2113786063, ____requests_16)); }
	inline List_1_t1320525645 * get__requests_16() const { return ____requests_16; }
	inline List_1_t1320525645 ** get_address_of__requests_16() { return &____requests_16; }
	inline void set__requests_16(List_1_t1320525645 * value)
	{
		____requests_16 = value;
		Il2CppCodeGenWriteBarrier((&____requests_16), value);
	}

	inline static int32_t get_offset_of__lock_17() { return static_cast<int32_t>(offsetof(HttpClient_t2113786063, ____lock_17)); }
	inline RuntimeObject * get__lock_17() const { return ____lock_17; }
	inline RuntimeObject ** get_address_of__lock_17() { return &____lock_17; }
	inline void set__lock_17(RuntimeObject * value)
	{
		____lock_17 = value;
		Il2CppCodeGenWriteBarrier((&____lock_17), value);
	}
};

struct HttpClient_t2113786063_StaticFields
{
public:
	// CI.HttpClient.Core.IDispatcher CI.HttpClient.HttpClient::_dispatcher
	RuntimeObject* ____dispatcher_18;

public:
	inline static int32_t get_offset_of__dispatcher_18() { return static_cast<int32_t>(offsetof(HttpClient_t2113786063_StaticFields, ____dispatcher_18)); }
	inline RuntimeObject* get__dispatcher_18() const { return ____dispatcher_18; }
	inline RuntimeObject** get_address_of__dispatcher_18() { return &____dispatcher_18; }
	inline void set__dispatcher_18(RuntimeObject* value)
	{
		____dispatcher_18 = value;
		Il2CppCodeGenWriteBarrier((&____dispatcher_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCLIENT_T2113786063_H
#ifndef U3CRAISEUPLOADSTATUSCALLBACKU3EC__ANONSTOREY0_T3210131969_H
#define U3CRAISEUPLOADSTATUSCALLBACKU3EC__ANONSTOREY0_T3210131969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.Core.HttpBase/<RaiseUploadStatusCallback>c__AnonStorey0
struct  U3CRaiseUploadStatusCallbackU3Ec__AnonStorey0_t3210131969  : public RuntimeObject
{
public:
	// System.Action`1<CI.HttpClient.UploadStatusMessage> CI.HttpClient.Core.HttpBase/<RaiseUploadStatusCallback>c__AnonStorey0::uploadStatusCallback
	Action_1_t3988964304 * ___uploadStatusCallback_0;
	// System.Int64 CI.HttpClient.Core.HttpBase/<RaiseUploadStatusCallback>c__AnonStorey0::contentLength
	int64_t ___contentLength_1;
	// System.Int64 CI.HttpClient.Core.HttpBase/<RaiseUploadStatusCallback>c__AnonStorey0::contentUploadedThisRound
	int64_t ___contentUploadedThisRound_2;
	// System.Int64 CI.HttpClient.Core.HttpBase/<RaiseUploadStatusCallback>c__AnonStorey0::totalContentUploaded
	int64_t ___totalContentUploaded_3;

public:
	inline static int32_t get_offset_of_uploadStatusCallback_0() { return static_cast<int32_t>(offsetof(U3CRaiseUploadStatusCallbackU3Ec__AnonStorey0_t3210131969, ___uploadStatusCallback_0)); }
	inline Action_1_t3988964304 * get_uploadStatusCallback_0() const { return ___uploadStatusCallback_0; }
	inline Action_1_t3988964304 ** get_address_of_uploadStatusCallback_0() { return &___uploadStatusCallback_0; }
	inline void set_uploadStatusCallback_0(Action_1_t3988964304 * value)
	{
		___uploadStatusCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___uploadStatusCallback_0), value);
	}

	inline static int32_t get_offset_of_contentLength_1() { return static_cast<int32_t>(offsetof(U3CRaiseUploadStatusCallbackU3Ec__AnonStorey0_t3210131969, ___contentLength_1)); }
	inline int64_t get_contentLength_1() const { return ___contentLength_1; }
	inline int64_t* get_address_of_contentLength_1() { return &___contentLength_1; }
	inline void set_contentLength_1(int64_t value)
	{
		___contentLength_1 = value;
	}

	inline static int32_t get_offset_of_contentUploadedThisRound_2() { return static_cast<int32_t>(offsetof(U3CRaiseUploadStatusCallbackU3Ec__AnonStorey0_t3210131969, ___contentUploadedThisRound_2)); }
	inline int64_t get_contentUploadedThisRound_2() const { return ___contentUploadedThisRound_2; }
	inline int64_t* get_address_of_contentUploadedThisRound_2() { return &___contentUploadedThisRound_2; }
	inline void set_contentUploadedThisRound_2(int64_t value)
	{
		___contentUploadedThisRound_2 = value;
	}

	inline static int32_t get_offset_of_totalContentUploaded_3() { return static_cast<int32_t>(offsetof(U3CRaiseUploadStatusCallbackU3Ec__AnonStorey0_t3210131969, ___totalContentUploaded_3)); }
	inline int64_t get_totalContentUploaded_3() const { return ___totalContentUploaded_3; }
	inline int64_t* get_address_of_totalContentUploaded_3() { return &___totalContentUploaded_3; }
	inline void set_totalContentUploaded_3(int64_t value)
	{
		___totalContentUploaded_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRAISEUPLOADSTATUSCALLBACKU3EC__ANONSTOREY0_T3210131969_H
#ifndef HTTPBASE_T4034084527_H
#define HTTPBASE_T4034084527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.Core.HttpBase
struct  HttpBase_t4034084527  : public RuntimeObject
{
public:
	// System.Net.HttpWebRequest CI.HttpClient.Core.HttpBase::_request
	HttpWebRequest_t1951404513 * ____request_0;
	// System.Net.HttpWebResponse CI.HttpClient.Core.HttpBase::_response
	HttpWebResponse_t2828383075 * ____response_1;
	// CI.HttpClient.Core.IDispatcher CI.HttpClient.Core.HttpBase::_dispatcher
	RuntimeObject* ____dispatcher_2;

public:
	inline static int32_t get_offset_of__request_0() { return static_cast<int32_t>(offsetof(HttpBase_t4034084527, ____request_0)); }
	inline HttpWebRequest_t1951404513 * get__request_0() const { return ____request_0; }
	inline HttpWebRequest_t1951404513 ** get_address_of__request_0() { return &____request_0; }
	inline void set__request_0(HttpWebRequest_t1951404513 * value)
	{
		____request_0 = value;
		Il2CppCodeGenWriteBarrier((&____request_0), value);
	}

	inline static int32_t get_offset_of__response_1() { return static_cast<int32_t>(offsetof(HttpBase_t4034084527, ____response_1)); }
	inline HttpWebResponse_t2828383075 * get__response_1() const { return ____response_1; }
	inline HttpWebResponse_t2828383075 ** get_address_of__response_1() { return &____response_1; }
	inline void set__response_1(HttpWebResponse_t2828383075 * value)
	{
		____response_1 = value;
		Il2CppCodeGenWriteBarrier((&____response_1), value);
	}

	inline static int32_t get_offset_of__dispatcher_2() { return static_cast<int32_t>(offsetof(HttpBase_t4034084527, ____dispatcher_2)); }
	inline RuntimeObject* get__dispatcher_2() const { return ____dispatcher_2; }
	inline RuntimeObject** get_address_of__dispatcher_2() { return &____dispatcher_2; }
	inline void set__dispatcher_2(RuntimeObject* value)
	{
		____dispatcher_2 = value;
		Il2CppCodeGenWriteBarrier((&____dispatcher_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPBASE_T4034084527_H
#ifndef U3CDELETEU3EC__ANONSTOREY0_T1672730659_H
#define U3CDELETEU3EC__ANONSTOREY0_T1672730659_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.HttpClient/<Delete>c__AnonStorey0
struct  U3CDeleteU3Ec__AnonStorey0_t1672730659  : public RuntimeObject
{
public:
	// System.Uri CI.HttpClient.HttpClient/<Delete>c__AnonStorey0::uri
	Uri_t19570940 * ___uri_0;
	// System.Action`1<CI.HttpClient.HttpResponseMessage`1<System.String>> CI.HttpClient.HttpClient/<Delete>c__AnonStorey0::responseCallback
	Action_1_t1180396435 * ___responseCallback_1;
	// CI.HttpClient.HttpClient CI.HttpClient.HttpClient/<Delete>c__AnonStorey0::$this
	HttpClient_t2113786063 * ___U24this_2;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(U3CDeleteU3Ec__AnonStorey0_t1672730659, ___uri_0)); }
	inline Uri_t19570940 * get_uri_0() const { return ___uri_0; }
	inline Uri_t19570940 ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_t19570940 * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_responseCallback_1() { return static_cast<int32_t>(offsetof(U3CDeleteU3Ec__AnonStorey0_t1672730659, ___responseCallback_1)); }
	inline Action_1_t1180396435 * get_responseCallback_1() const { return ___responseCallback_1; }
	inline Action_1_t1180396435 ** get_address_of_responseCallback_1() { return &___responseCallback_1; }
	inline void set_responseCallback_1(Action_1_t1180396435 * value)
	{
		___responseCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___responseCallback_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CDeleteU3Ec__AnonStorey0_t1672730659, ___U24this_2)); }
	inline HttpClient_t2113786063 * get_U24this_2() const { return ___U24this_2; }
	inline HttpClient_t2113786063 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(HttpClient_t2113786063 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELETEU3EC__ANONSTOREY0_T1672730659_H
#ifndef U3CGETSTRINGU3EC__ANONSTOREY2_T2072622603_H
#define U3CGETSTRINGU3EC__ANONSTOREY2_T2072622603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.HttpClient/<GetString>c__AnonStorey2
struct  U3CGetStringU3Ec__AnonStorey2_t2072622603  : public RuntimeObject
{
public:
	// System.Uri CI.HttpClient.HttpClient/<GetString>c__AnonStorey2::uri
	Uri_t19570940 * ___uri_0;
	// System.Action`1<CI.HttpClient.HttpResponseMessage`1<System.String>> CI.HttpClient.HttpClient/<GetString>c__AnonStorey2::responseCallback
	Action_1_t1180396435 * ___responseCallback_1;
	// CI.HttpClient.HttpClient CI.HttpClient.HttpClient/<GetString>c__AnonStorey2::$this
	HttpClient_t2113786063 * ___U24this_2;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(U3CGetStringU3Ec__AnonStorey2_t2072622603, ___uri_0)); }
	inline Uri_t19570940 * get_uri_0() const { return ___uri_0; }
	inline Uri_t19570940 ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_t19570940 * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_responseCallback_1() { return static_cast<int32_t>(offsetof(U3CGetStringU3Ec__AnonStorey2_t2072622603, ___responseCallback_1)); }
	inline Action_1_t1180396435 * get_responseCallback_1() const { return ___responseCallback_1; }
	inline Action_1_t1180396435 ** get_address_of_responseCallback_1() { return &___responseCallback_1; }
	inline void set_responseCallback_1(Action_1_t1180396435 * value)
	{
		___responseCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___responseCallback_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGetStringU3Ec__AnonStorey2_t2072622603, ___U24this_2)); }
	inline HttpClient_t2113786063 * get_U24this_2() const { return ___U24this_2; }
	inline HttpClient_t2113786063 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(HttpClient_t2113786063 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETSTRINGU3EC__ANONSTOREY2_T2072622603_H
#ifndef U3CPATCHU3EC__ANONSTOREY4_T2473826542_H
#define U3CPATCHU3EC__ANONSTOREY4_T2473826542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.HttpClient/<Patch>c__AnonStorey4
struct  U3CPatchU3Ec__AnonStorey4_t2473826542  : public RuntimeObject
{
public:
	// System.Uri CI.HttpClient.HttpClient/<Patch>c__AnonStorey4::uri
	Uri_t19570940 * ___uri_0;
	// CI.HttpClient.IHttpContent CI.HttpClient.HttpClient/<Patch>c__AnonStorey4::content
	RuntimeObject* ___content_1;
	// System.Action`1<CI.HttpClient.HttpResponseMessage`1<System.String>> CI.HttpClient.HttpClient/<Patch>c__AnonStorey4::responseCallback
	Action_1_t1180396435 * ___responseCallback_2;
	// System.Action`1<CI.HttpClient.UploadStatusMessage> CI.HttpClient.HttpClient/<Patch>c__AnonStorey4::uploadStatusCallback
	Action_1_t3988964304 * ___uploadStatusCallback_3;
	// CI.HttpClient.HttpClient CI.HttpClient.HttpClient/<Patch>c__AnonStorey4::$this
	HttpClient_t2113786063 * ___U24this_4;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(U3CPatchU3Ec__AnonStorey4_t2473826542, ___uri_0)); }
	inline Uri_t19570940 * get_uri_0() const { return ___uri_0; }
	inline Uri_t19570940 ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_t19570940 * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(U3CPatchU3Ec__AnonStorey4_t2473826542, ___content_1)); }
	inline RuntimeObject* get_content_1() const { return ___content_1; }
	inline RuntimeObject** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(RuntimeObject* value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_1), value);
	}

	inline static int32_t get_offset_of_responseCallback_2() { return static_cast<int32_t>(offsetof(U3CPatchU3Ec__AnonStorey4_t2473826542, ___responseCallback_2)); }
	inline Action_1_t1180396435 * get_responseCallback_2() const { return ___responseCallback_2; }
	inline Action_1_t1180396435 ** get_address_of_responseCallback_2() { return &___responseCallback_2; }
	inline void set_responseCallback_2(Action_1_t1180396435 * value)
	{
		___responseCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___responseCallback_2), value);
	}

	inline static int32_t get_offset_of_uploadStatusCallback_3() { return static_cast<int32_t>(offsetof(U3CPatchU3Ec__AnonStorey4_t2473826542, ___uploadStatusCallback_3)); }
	inline Action_1_t3988964304 * get_uploadStatusCallback_3() const { return ___uploadStatusCallback_3; }
	inline Action_1_t3988964304 ** get_address_of_uploadStatusCallback_3() { return &___uploadStatusCallback_3; }
	inline void set_uploadStatusCallback_3(Action_1_t3988964304 * value)
	{
		___uploadStatusCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___uploadStatusCallback_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CPatchU3Ec__AnonStorey4_t2473826542, ___U24this_4)); }
	inline HttpClient_t2113786063 * get_U24this_4() const { return ___U24this_4; }
	inline HttpClient_t2113786063 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(HttpClient_t2113786063 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPATCHU3EC__ANONSTOREY4_T2473826542_H
#ifndef U3CPOSTU3EC__ANONSTOREY6_T1971601810_H
#define U3CPOSTU3EC__ANONSTOREY6_T1971601810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.HttpClient/<Post>c__AnonStorey6
struct  U3CPostU3Ec__AnonStorey6_t1971601810  : public RuntimeObject
{
public:
	// System.Uri CI.HttpClient.HttpClient/<Post>c__AnonStorey6::uri
	Uri_t19570940 * ___uri_0;
	// CI.HttpClient.IHttpContent CI.HttpClient.HttpClient/<Post>c__AnonStorey6::content
	RuntimeObject* ___content_1;
	// System.Action`1<CI.HttpClient.HttpResponseMessage`1<System.String>> CI.HttpClient.HttpClient/<Post>c__AnonStorey6::responseCallback
	Action_1_t1180396435 * ___responseCallback_2;
	// System.Action`1<CI.HttpClient.UploadStatusMessage> CI.HttpClient.HttpClient/<Post>c__AnonStorey6::uploadStatusCallback
	Action_1_t3988964304 * ___uploadStatusCallback_3;
	// CI.HttpClient.HttpClient CI.HttpClient.HttpClient/<Post>c__AnonStorey6::$this
	HttpClient_t2113786063 * ___U24this_4;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey6_t1971601810, ___uri_0)); }
	inline Uri_t19570940 * get_uri_0() const { return ___uri_0; }
	inline Uri_t19570940 ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_t19570940 * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey6_t1971601810, ___content_1)); }
	inline RuntimeObject* get_content_1() const { return ___content_1; }
	inline RuntimeObject** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(RuntimeObject* value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_1), value);
	}

	inline static int32_t get_offset_of_responseCallback_2() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey6_t1971601810, ___responseCallback_2)); }
	inline Action_1_t1180396435 * get_responseCallback_2() const { return ___responseCallback_2; }
	inline Action_1_t1180396435 ** get_address_of_responseCallback_2() { return &___responseCallback_2; }
	inline void set_responseCallback_2(Action_1_t1180396435 * value)
	{
		___responseCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___responseCallback_2), value);
	}

	inline static int32_t get_offset_of_uploadStatusCallback_3() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey6_t1971601810, ___uploadStatusCallback_3)); }
	inline Action_1_t3988964304 * get_uploadStatusCallback_3() const { return ___uploadStatusCallback_3; }
	inline Action_1_t3988964304 ** get_address_of_uploadStatusCallback_3() { return &___uploadStatusCallback_3; }
	inline void set_uploadStatusCallback_3(Action_1_t3988964304 * value)
	{
		___uploadStatusCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___uploadStatusCallback_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey6_t1971601810, ___U24this_4)); }
	inline HttpClient_t2113786063 * get_U24this_4() const { return ___U24this_4; }
	inline HttpClient_t2113786063 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(HttpClient_t2113786063 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOSTU3EC__ANONSTOREY6_T1971601810_H
#ifndef U3CPUTU3EC__ANONSTOREY8_T4219266303_H
#define U3CPUTU3EC__ANONSTOREY8_T4219266303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.HttpClient/<Put>c__AnonStorey8
struct  U3CPutU3Ec__AnonStorey8_t4219266303  : public RuntimeObject
{
public:
	// System.Uri CI.HttpClient.HttpClient/<Put>c__AnonStorey8::uri
	Uri_t19570940 * ___uri_0;
	// CI.HttpClient.IHttpContent CI.HttpClient.HttpClient/<Put>c__AnonStorey8::content
	RuntimeObject* ___content_1;
	// System.Action`1<CI.HttpClient.HttpResponseMessage`1<System.String>> CI.HttpClient.HttpClient/<Put>c__AnonStorey8::responseCallback
	Action_1_t1180396435 * ___responseCallback_2;
	// System.Action`1<CI.HttpClient.UploadStatusMessage> CI.HttpClient.HttpClient/<Put>c__AnonStorey8::uploadStatusCallback
	Action_1_t3988964304 * ___uploadStatusCallback_3;
	// CI.HttpClient.HttpClient CI.HttpClient.HttpClient/<Put>c__AnonStorey8::$this
	HttpClient_t2113786063 * ___U24this_4;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(U3CPutU3Ec__AnonStorey8_t4219266303, ___uri_0)); }
	inline Uri_t19570940 * get_uri_0() const { return ___uri_0; }
	inline Uri_t19570940 ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_t19570940 * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(U3CPutU3Ec__AnonStorey8_t4219266303, ___content_1)); }
	inline RuntimeObject* get_content_1() const { return ___content_1; }
	inline RuntimeObject** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(RuntimeObject* value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_1), value);
	}

	inline static int32_t get_offset_of_responseCallback_2() { return static_cast<int32_t>(offsetof(U3CPutU3Ec__AnonStorey8_t4219266303, ___responseCallback_2)); }
	inline Action_1_t1180396435 * get_responseCallback_2() const { return ___responseCallback_2; }
	inline Action_1_t1180396435 ** get_address_of_responseCallback_2() { return &___responseCallback_2; }
	inline void set_responseCallback_2(Action_1_t1180396435 * value)
	{
		___responseCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___responseCallback_2), value);
	}

	inline static int32_t get_offset_of_uploadStatusCallback_3() { return static_cast<int32_t>(offsetof(U3CPutU3Ec__AnonStorey8_t4219266303, ___uploadStatusCallback_3)); }
	inline Action_1_t3988964304 * get_uploadStatusCallback_3() const { return ___uploadStatusCallback_3; }
	inline Action_1_t3988964304 ** get_address_of_uploadStatusCallback_3() { return &___uploadStatusCallback_3; }
	inline void set_uploadStatusCallback_3(Action_1_t3988964304 * value)
	{
		___uploadStatusCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___uploadStatusCallback_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CPutU3Ec__AnonStorey8_t4219266303, ___U24this_4)); }
	inline HttpClient_t2113786063 * get_U24this_4() const { return ___U24this_4; }
	inline HttpClient_t2113786063 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(HttpClient_t2113786063 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPUTU3EC__ANONSTOREY8_T4219266303_H
#ifndef BYTEARRAYCONTENT_T1139747420_H
#define BYTEARRAYCONTENT_T1139747420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.ByteArrayContent
struct  ByteArrayContent_t1139747420  : public RuntimeObject
{
public:
	// System.Byte[] CI.HttpClient.ByteArrayContent::_content
	ByteU5BU5D_t3397334013* ____content_0;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> CI.HttpClient.ByteArrayContent::<Headers>k__BackingField
	RuntimeObject* ___U3CHeadersU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of__content_0() { return static_cast<int32_t>(offsetof(ByteArrayContent_t1139747420, ____content_0)); }
	inline ByteU5BU5D_t3397334013* get__content_0() const { return ____content_0; }
	inline ByteU5BU5D_t3397334013** get_address_of__content_0() { return &____content_0; }
	inline void set__content_0(ByteU5BU5D_t3397334013* value)
	{
		____content_0 = value;
		Il2CppCodeGenWriteBarrier((&____content_0), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ByteArrayContent_t1139747420, ___U3CHeadersU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CHeadersU3Ek__BackingField_1() const { return ___U3CHeadersU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CHeadersU3Ek__BackingField_1() { return &___U3CHeadersU3Ek__BackingField_1; }
	inline void set_U3CHeadersU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CHeadersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEARRAYCONTENT_T1139747420_H
#ifndef U3CINSTANTIATEDRAWERBUTTONU3EC__ANONSTOREY0_T2380289698_H
#define U3CINSTANTIATEDRAWERBUTTONU3EC__ANONSTOREY0_T2380289698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrawerHandler/<InstantiateDrawerButton>c__AnonStorey0
struct  U3CInstantiateDrawerButtonU3Ec__AnonStorey0_t2380289698  : public RuntimeObject
{
public:
	// UnityEngine.GameObject DrawerHandler/<InstantiateDrawerButton>c__AnonStorey0::objectInScene
	GameObject_t1756533147 * ___objectInScene_0;
	// DrawerHandler DrawerHandler/<InstantiateDrawerButton>c__AnonStorey0::$this
	DrawerHandler_t321192673 * ___U24this_1;

public:
	inline static int32_t get_offset_of_objectInScene_0() { return static_cast<int32_t>(offsetof(U3CInstantiateDrawerButtonU3Ec__AnonStorey0_t2380289698, ___objectInScene_0)); }
	inline GameObject_t1756533147 * get_objectInScene_0() const { return ___objectInScene_0; }
	inline GameObject_t1756533147 ** get_address_of_objectInScene_0() { return &___objectInScene_0; }
	inline void set_objectInScene_0(GameObject_t1756533147 * value)
	{
		___objectInScene_0 = value;
		Il2CppCodeGenWriteBarrier((&___objectInScene_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CInstantiateDrawerButtonU3Ec__AnonStorey0_t2380289698, ___U24this_1)); }
	inline DrawerHandler_t321192673 * get_U24this_1() const { return ___U24this_1; }
	inline DrawerHandler_t321192673 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(DrawerHandler_t321192673 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINSTANTIATEDRAWERBUTTONU3EC__ANONSTOREY0_T2380289698_H
#ifndef U3CINITIALIZESAVEBUTTONU3EC__ANONSTOREY0_T600697625_H
#define U3CINITIALIZESAVEBUTTONU3EC__ANONSTOREY0_T600697625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.Remark.NewRemarkHandlerMobile/<initializeSaveButton>c__AnonStorey0
struct  U3CinitializeSaveButtonU3Ec__AnonStorey0_t600697625  : public RuntimeObject
{
public:
	// UnityEngine.Transform Library.Code.UI.Remark.NewRemarkHandlerMobile/<initializeSaveButton>c__AnonStorey0::remarkTransform
	Transform_t3275118058 * ___remarkTransform_0;
	// Library.Code.DI.DepedencyProvider Library.Code.UI.Remark.NewRemarkHandlerMobile/<initializeSaveButton>c__AnonStorey0::depedencyProvider
	RuntimeObject* ___depedencyProvider_1;
	// UnityEngine.UI.InputField Library.Code.UI.Remark.NewRemarkHandlerMobile/<initializeSaveButton>c__AnonStorey0::inputField
	InputField_t1631627530 * ___inputField_2;
	// Library.Code.UI.Remark.NewRemarkHandlerMobile Library.Code.UI.Remark.NewRemarkHandlerMobile/<initializeSaveButton>c__AnonStorey0::$this
	NewRemarkHandlerMobile_t798103230 * ___U24this_3;

public:
	inline static int32_t get_offset_of_remarkTransform_0() { return static_cast<int32_t>(offsetof(U3CinitializeSaveButtonU3Ec__AnonStorey0_t600697625, ___remarkTransform_0)); }
	inline Transform_t3275118058 * get_remarkTransform_0() const { return ___remarkTransform_0; }
	inline Transform_t3275118058 ** get_address_of_remarkTransform_0() { return &___remarkTransform_0; }
	inline void set_remarkTransform_0(Transform_t3275118058 * value)
	{
		___remarkTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___remarkTransform_0), value);
	}

	inline static int32_t get_offset_of_depedencyProvider_1() { return static_cast<int32_t>(offsetof(U3CinitializeSaveButtonU3Ec__AnonStorey0_t600697625, ___depedencyProvider_1)); }
	inline RuntimeObject* get_depedencyProvider_1() const { return ___depedencyProvider_1; }
	inline RuntimeObject** get_address_of_depedencyProvider_1() { return &___depedencyProvider_1; }
	inline void set_depedencyProvider_1(RuntimeObject* value)
	{
		___depedencyProvider_1 = value;
		Il2CppCodeGenWriteBarrier((&___depedencyProvider_1), value);
	}

	inline static int32_t get_offset_of_inputField_2() { return static_cast<int32_t>(offsetof(U3CinitializeSaveButtonU3Ec__AnonStorey0_t600697625, ___inputField_2)); }
	inline InputField_t1631627530 * get_inputField_2() const { return ___inputField_2; }
	inline InputField_t1631627530 ** get_address_of_inputField_2() { return &___inputField_2; }
	inline void set_inputField_2(InputField_t1631627530 * value)
	{
		___inputField_2 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CinitializeSaveButtonU3Ec__AnonStorey0_t600697625, ___U24this_3)); }
	inline NewRemarkHandlerMobile_t798103230 * get_U24this_3() const { return ___U24this_3; }
	inline NewRemarkHandlerMobile_t798103230 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(NewRemarkHandlerMobile_t798103230 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITIALIZESAVEBUTTONU3EC__ANONSTOREY0_T600697625_H
#ifndef POPUPHANDLER_T3865200444_H
#define POPUPHANDLER_T3865200444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.PopUp.PopUpHandler
struct  PopUpHandler_t3865200444  : public RuntimeObject
{
public:
	// UnityEngine.Transform Library.Code.UI.PopUp.PopUpHandler::backgroundTransform
	Transform_t3275118058 * ___backgroundTransform_0;
	// UnityEngine.GameObject Library.Code.UI.PopUp.PopUpHandler::gameObject
	GameObject_t1756533147 * ___gameObject_1;

public:
	inline static int32_t get_offset_of_backgroundTransform_0() { return static_cast<int32_t>(offsetof(PopUpHandler_t3865200444, ___backgroundTransform_0)); }
	inline Transform_t3275118058 * get_backgroundTransform_0() const { return ___backgroundTransform_0; }
	inline Transform_t3275118058 ** get_address_of_backgroundTransform_0() { return &___backgroundTransform_0; }
	inline void set_backgroundTransform_0(Transform_t3275118058 * value)
	{
		___backgroundTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundTransform_0), value);
	}

	inline static int32_t get_offset_of_gameObject_1() { return static_cast<int32_t>(offsetof(PopUpHandler_t3865200444, ___gameObject_1)); }
	inline GameObject_t1756533147 * get_gameObject_1() const { return ___gameObject_1; }
	inline GameObject_t1756533147 ** get_address_of_gameObject_1() { return &___gameObject_1; }
	inline void set_gameObject_1(GameObject_t1756533147 * value)
	{
		___gameObject_1 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPUPHANDLER_T3865200444_H
#ifndef MESSAGELISTBUILDERTWOPANELS_T2629535697_H
#define MESSAGELISTBUILDERTWOPANELS_T2629535697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MessagesList.New.MessageListBuilderTwoPanels
struct  MessageListBuilderTwoPanels_t2629535697  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGELISTBUILDERTWOPANELS_T2629535697_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t169632028* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1927440687 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t169632028* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t169632028** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t169632028* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___inner_exception_1)); }
	inline Exception_t1927440687 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1927440687 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1927440687 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1927440687_H
#ifndef MESSAGELISTBUILDER_T1028067940_H
#define MESSAGELISTBUILDER_T1028067940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MessagesList.MessageListBuilder
struct  MessageListBuilder_t1028067940  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGELISTBUILDER_T1028067940_H
#ifndef PREFABGENERATORIMP_T2478736189_H
#define PREFABGENERATORIMP_T2478736189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.PrefabGenerator.PrefabGeneratorImp
struct  PrefabGeneratorImp_t2478736189  : public RuntimeObject
{
public:
	// Library.Code.UI.PrefabGenerator.CreateInterface Library.Code.UI.PrefabGenerator.PrefabGeneratorImp::_createInterface
	RuntimeObject* ____createInterface_0;
	// System.String Library.Code.UI.PrefabGenerator.PrefabGeneratorImp::pathToUIPrefabs
	String_t* ___pathToUIPrefabs_1;

public:
	inline static int32_t get_offset_of__createInterface_0() { return static_cast<int32_t>(offsetof(PrefabGeneratorImp_t2478736189, ____createInterface_0)); }
	inline RuntimeObject* get__createInterface_0() const { return ____createInterface_0; }
	inline RuntimeObject** get_address_of__createInterface_0() { return &____createInterface_0; }
	inline void set__createInterface_0(RuntimeObject* value)
	{
		____createInterface_0 = value;
		Il2CppCodeGenWriteBarrier((&____createInterface_0), value);
	}

	inline static int32_t get_offset_of_pathToUIPrefabs_1() { return static_cast<int32_t>(offsetof(PrefabGeneratorImp_t2478736189, ___pathToUIPrefabs_1)); }
	inline String_t* get_pathToUIPrefabs_1() const { return ___pathToUIPrefabs_1; }
	inline String_t** get_address_of_pathToUIPrefabs_1() { return &___pathToUIPrefabs_1; }
	inline void set_pathToUIPrefabs_1(String_t* value)
	{
		___pathToUIPrefabs_1 = value;
		Il2CppCodeGenWriteBarrier((&___pathToUIPrefabs_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABGENERATORIMP_T2478736189_H
#ifndef U3CHANDLEPOPUPU3EC__ANONSTOREY0_T3839772636_H
#define U3CHANDLEPOPUPU3EC__ANONSTOREY0_T3839772636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.PopUp.WarningPopUp/<handlePopUp>c__AnonStorey0
struct  U3ChandlePopUpU3Ec__AnonStorey0_t3839772636  : public RuntimeObject
{
public:
	// Library.Code.UI.PopUp.PopUpStuff Library.Code.UI.PopUp.WarningPopUp/<handlePopUp>c__AnonStorey0::popUpStuff
	PopUpStuff_t3426923330 * ___popUpStuff_0;

public:
	inline static int32_t get_offset_of_popUpStuff_0() { return static_cast<int32_t>(offsetof(U3ChandlePopUpU3Ec__AnonStorey0_t3839772636, ___popUpStuff_0)); }
	inline PopUpStuff_t3426923330 * get_popUpStuff_0() const { return ___popUpStuff_0; }
	inline PopUpStuff_t3426923330 ** get_address_of_popUpStuff_0() { return &___popUpStuff_0; }
	inline void set_popUpStuff_0(PopUpStuff_t3426923330 * value)
	{
		___popUpStuff_0 = value;
		Il2CppCodeGenWriteBarrier((&___popUpStuff_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHANDLEPOPUPU3EC__ANONSTOREY0_T3839772636_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef U3CINITIALIZCATCHPHOTOU3EC__ANONSTOREY1_T1231633819_H
#define U3CINITIALIZCATCHPHOTOU3EC__ANONSTOREY1_T1231633819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.Remark.NewRemarkHandlerMobile/<initializCatchPhoto>c__AnonStorey1
struct  U3CinitializCatchPhotoU3Ec__AnonStorey1_t1231633819  : public RuntimeObject
{
public:
	// Library.Code.Facades.PathHolder.PathHolder Library.Code.UI.Remark.NewRemarkHandlerMobile/<initializCatchPhoto>c__AnonStorey1::holder
	RuntimeObject* ___holder_0;
	// UnityEngine.Transform Library.Code.UI.Remark.NewRemarkHandlerMobile/<initializCatchPhoto>c__AnonStorey1::takePhoto
	Transform_t3275118058 * ___takePhoto_1;
	// UnityEngine.Transform Library.Code.UI.Remark.NewRemarkHandlerMobile/<initializCatchPhoto>c__AnonStorey1::takeAudio
	Transform_t3275118058 * ___takeAudio_2;
	// Library.Code.UI.Remark.NewRemarkHandlerMobile Library.Code.UI.Remark.NewRemarkHandlerMobile/<initializCatchPhoto>c__AnonStorey1::$this
	NewRemarkHandlerMobile_t798103230 * ___U24this_3;

public:
	inline static int32_t get_offset_of_holder_0() { return static_cast<int32_t>(offsetof(U3CinitializCatchPhotoU3Ec__AnonStorey1_t1231633819, ___holder_0)); }
	inline RuntimeObject* get_holder_0() const { return ___holder_0; }
	inline RuntimeObject** get_address_of_holder_0() { return &___holder_0; }
	inline void set_holder_0(RuntimeObject* value)
	{
		___holder_0 = value;
		Il2CppCodeGenWriteBarrier((&___holder_0), value);
	}

	inline static int32_t get_offset_of_takePhoto_1() { return static_cast<int32_t>(offsetof(U3CinitializCatchPhotoU3Ec__AnonStorey1_t1231633819, ___takePhoto_1)); }
	inline Transform_t3275118058 * get_takePhoto_1() const { return ___takePhoto_1; }
	inline Transform_t3275118058 ** get_address_of_takePhoto_1() { return &___takePhoto_1; }
	inline void set_takePhoto_1(Transform_t3275118058 * value)
	{
		___takePhoto_1 = value;
		Il2CppCodeGenWriteBarrier((&___takePhoto_1), value);
	}

	inline static int32_t get_offset_of_takeAudio_2() { return static_cast<int32_t>(offsetof(U3CinitializCatchPhotoU3Ec__AnonStorey1_t1231633819, ___takeAudio_2)); }
	inline Transform_t3275118058 * get_takeAudio_2() const { return ___takeAudio_2; }
	inline Transform_t3275118058 ** get_address_of_takeAudio_2() { return &___takeAudio_2; }
	inline void set_takeAudio_2(Transform_t3275118058 * value)
	{
		___takeAudio_2 = value;
		Il2CppCodeGenWriteBarrier((&___takeAudio_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CinitializCatchPhotoU3Ec__AnonStorey1_t1231633819, ___U24this_3)); }
	inline NewRemarkHandlerMobile_t798103230 * get_U24this_3() const { return ___U24this_3; }
	inline NewRemarkHandlerMobile_t798103230 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(NewRemarkHandlerMobile_t798103230 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITIALIZCATCHPHOTOU3EC__ANONSTOREY1_T1231633819_H
#ifndef U3CINSTANTIATEREMARKBUTTONU3EC__ANONSTOREY2_T2468962027_H
#define U3CINSTANTIATEREMARKBUTTONU3EC__ANONSTOREY2_T2468962027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrawerHandler/<InstantiateRemarkButton>c__AnonStorey2
struct  U3CInstantiateRemarkButtonU3Ec__AnonStorey2_t2468962027  : public RuntimeObject
{
public:
	// UnityEngine.GameObject DrawerHandler/<InstantiateRemarkButton>c__AnonStorey2::objectInScene
	GameObject_t1756533147 * ___objectInScene_0;
	// DrawerHandler DrawerHandler/<InstantiateRemarkButton>c__AnonStorey2::$this
	DrawerHandler_t321192673 * ___U24this_1;

public:
	inline static int32_t get_offset_of_objectInScene_0() { return static_cast<int32_t>(offsetof(U3CInstantiateRemarkButtonU3Ec__AnonStorey2_t2468962027, ___objectInScene_0)); }
	inline GameObject_t1756533147 * get_objectInScene_0() const { return ___objectInScene_0; }
	inline GameObject_t1756533147 ** get_address_of_objectInScene_0() { return &___objectInScene_0; }
	inline void set_objectInScene_0(GameObject_t1756533147 * value)
	{
		___objectInScene_0 = value;
		Il2CppCodeGenWriteBarrier((&___objectInScene_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CInstantiateRemarkButtonU3Ec__AnonStorey2_t2468962027, ___U24this_1)); }
	inline DrawerHandler_t321192673 * get_U24this_1() const { return ___U24this_1; }
	inline DrawerHandler_t321192673 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(DrawerHandler_t321192673 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINSTANTIATEREMARKBUTTONU3EC__ANONSTOREY2_T2468962027_H
#ifndef U3CINITIALIZESAVEBUTTONU3EC__ANONSTOREY0_T1415142742_H
#define U3CINITIALIZESAVEBUTTONU3EC__ANONSTOREY0_T1415142742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.Remark.RemarkHandlerMobileTwoPanels/<initializeSaveButton>c__AnonStorey0
struct  U3CinitializeSaveButtonU3Ec__AnonStorey0_t1415142742  : public RuntimeObject
{
public:
	// UnityEngine.Transform Library.Code.UI.Remark.RemarkHandlerMobileTwoPanels/<initializeSaveButton>c__AnonStorey0::remarkTransform
	Transform_t3275118058 * ___remarkTransform_0;
	// Library.Code.DI.DepedencyProvider Library.Code.UI.Remark.RemarkHandlerMobileTwoPanels/<initializeSaveButton>c__AnonStorey0::depedencyProvider
	RuntimeObject* ___depedencyProvider_1;
	// UnityEngine.UI.InputField Library.Code.UI.Remark.RemarkHandlerMobileTwoPanels/<initializeSaveButton>c__AnonStorey0::inputField
	InputField_t1631627530 * ___inputField_2;
	// Library.Code.UI.Remark.RemarkHandlerMobileTwoPanels Library.Code.UI.Remark.RemarkHandlerMobileTwoPanels/<initializeSaveButton>c__AnonStorey0::$this
	RemarkHandlerMobileTwoPanels_t1827359049 * ___U24this_3;

public:
	inline static int32_t get_offset_of_remarkTransform_0() { return static_cast<int32_t>(offsetof(U3CinitializeSaveButtonU3Ec__AnonStorey0_t1415142742, ___remarkTransform_0)); }
	inline Transform_t3275118058 * get_remarkTransform_0() const { return ___remarkTransform_0; }
	inline Transform_t3275118058 ** get_address_of_remarkTransform_0() { return &___remarkTransform_0; }
	inline void set_remarkTransform_0(Transform_t3275118058 * value)
	{
		___remarkTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___remarkTransform_0), value);
	}

	inline static int32_t get_offset_of_depedencyProvider_1() { return static_cast<int32_t>(offsetof(U3CinitializeSaveButtonU3Ec__AnonStorey0_t1415142742, ___depedencyProvider_1)); }
	inline RuntimeObject* get_depedencyProvider_1() const { return ___depedencyProvider_1; }
	inline RuntimeObject** get_address_of_depedencyProvider_1() { return &___depedencyProvider_1; }
	inline void set_depedencyProvider_1(RuntimeObject* value)
	{
		___depedencyProvider_1 = value;
		Il2CppCodeGenWriteBarrier((&___depedencyProvider_1), value);
	}

	inline static int32_t get_offset_of_inputField_2() { return static_cast<int32_t>(offsetof(U3CinitializeSaveButtonU3Ec__AnonStorey0_t1415142742, ___inputField_2)); }
	inline InputField_t1631627530 * get_inputField_2() const { return ___inputField_2; }
	inline InputField_t1631627530 ** get_address_of_inputField_2() { return &___inputField_2; }
	inline void set_inputField_2(InputField_t1631627530 * value)
	{
		___inputField_2 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CinitializeSaveButtonU3Ec__AnonStorey0_t1415142742, ___U24this_3)); }
	inline RemarkHandlerMobileTwoPanels_t1827359049 * get_U24this_3() const { return ___U24this_3; }
	inline RemarkHandlerMobileTwoPanels_t1827359049 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(RemarkHandlerMobileTwoPanels_t1827359049 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITIALIZESAVEBUTTONU3EC__ANONSTOREY0_T1415142742_H
#ifndef U3CINITIALIZESAVEBUTTONU3EC__ANONSTOREY0_T3054089613_H
#define U3CINITIALIZESAVEBUTTONU3EC__ANONSTOREY0_T3054089613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.Remark.RemarkHandlerMobile/<initializeSaveButton>c__AnonStorey0
struct  U3CinitializeSaveButtonU3Ec__AnonStorey0_t3054089613  : public RuntimeObject
{
public:
	// Library.Code.DI.DepedencyProvider Library.Code.UI.Remark.RemarkHandlerMobile/<initializeSaveButton>c__AnonStorey0::depedencyProvider
	RuntimeObject* ___depedencyProvider_0;
	// Library.Code.UI.Remark.RemarkHandlerMobile Library.Code.UI.Remark.RemarkHandlerMobile/<initializeSaveButton>c__AnonStorey0::$this
	RemarkHandlerMobile_t1067069532 * ___U24this_1;

public:
	inline static int32_t get_offset_of_depedencyProvider_0() { return static_cast<int32_t>(offsetof(U3CinitializeSaveButtonU3Ec__AnonStorey0_t3054089613, ___depedencyProvider_0)); }
	inline RuntimeObject* get_depedencyProvider_0() const { return ___depedencyProvider_0; }
	inline RuntimeObject** get_address_of_depedencyProvider_0() { return &___depedencyProvider_0; }
	inline void set_depedencyProvider_0(RuntimeObject* value)
	{
		___depedencyProvider_0 = value;
		Il2CppCodeGenWriteBarrier((&___depedencyProvider_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CinitializeSaveButtonU3Ec__AnonStorey0_t3054089613, ___U24this_1)); }
	inline RemarkHandlerMobile_t1067069532 * get_U24this_1() const { return ___U24this_1; }
	inline RemarkHandlerMobile_t1067069532 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(RemarkHandlerMobile_t1067069532 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITIALIZESAVEBUTTONU3EC__ANONSTOREY0_T3054089613_H
#ifndef U3CINITIALIZECATCHPHOTOU3EC__ANONSTOREY1_T3556158843_H
#define U3CINITIALIZECATCHPHOTOU3EC__ANONSTOREY1_T3556158843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.Remark.RemarkHandlerMobileTwoPanels/<initializeCatchPhoto>c__AnonStorey1
struct  U3CinitializeCatchPhotoU3Ec__AnonStorey1_t3556158843  : public RuntimeObject
{
public:
	// Library.Code.Facades.PathHolder.PathHolder Library.Code.UI.Remark.RemarkHandlerMobileTwoPanels/<initializeCatchPhoto>c__AnonStorey1::holder
	RuntimeObject* ___holder_0;
	// UnityEngine.Transform Library.Code.UI.Remark.RemarkHandlerMobileTwoPanels/<initializeCatchPhoto>c__AnonStorey1::takePhoto
	Transform_t3275118058 * ___takePhoto_1;
	// UnityEngine.Transform Library.Code.UI.Remark.RemarkHandlerMobileTwoPanels/<initializeCatchPhoto>c__AnonStorey1::takeAudio
	Transform_t3275118058 * ___takeAudio_2;
	// Library.Code.UI.Remark.RemarkHandlerMobileTwoPanels Library.Code.UI.Remark.RemarkHandlerMobileTwoPanels/<initializeCatchPhoto>c__AnonStorey1::$this
	RemarkHandlerMobileTwoPanels_t1827359049 * ___U24this_3;

public:
	inline static int32_t get_offset_of_holder_0() { return static_cast<int32_t>(offsetof(U3CinitializeCatchPhotoU3Ec__AnonStorey1_t3556158843, ___holder_0)); }
	inline RuntimeObject* get_holder_0() const { return ___holder_0; }
	inline RuntimeObject** get_address_of_holder_0() { return &___holder_0; }
	inline void set_holder_0(RuntimeObject* value)
	{
		___holder_0 = value;
		Il2CppCodeGenWriteBarrier((&___holder_0), value);
	}

	inline static int32_t get_offset_of_takePhoto_1() { return static_cast<int32_t>(offsetof(U3CinitializeCatchPhotoU3Ec__AnonStorey1_t3556158843, ___takePhoto_1)); }
	inline Transform_t3275118058 * get_takePhoto_1() const { return ___takePhoto_1; }
	inline Transform_t3275118058 ** get_address_of_takePhoto_1() { return &___takePhoto_1; }
	inline void set_takePhoto_1(Transform_t3275118058 * value)
	{
		___takePhoto_1 = value;
		Il2CppCodeGenWriteBarrier((&___takePhoto_1), value);
	}

	inline static int32_t get_offset_of_takeAudio_2() { return static_cast<int32_t>(offsetof(U3CinitializeCatchPhotoU3Ec__AnonStorey1_t3556158843, ___takeAudio_2)); }
	inline Transform_t3275118058 * get_takeAudio_2() const { return ___takeAudio_2; }
	inline Transform_t3275118058 ** get_address_of_takeAudio_2() { return &___takeAudio_2; }
	inline void set_takeAudio_2(Transform_t3275118058 * value)
	{
		___takeAudio_2 = value;
		Il2CppCodeGenWriteBarrier((&___takeAudio_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CinitializeCatchPhotoU3Ec__AnonStorey1_t3556158843, ___U24this_3)); }
	inline RemarkHandlerMobileTwoPanels_t1827359049 * get_U24this_3() const { return ___U24this_3; }
	inline RemarkHandlerMobileTwoPanels_t1827359049 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(RemarkHandlerMobileTwoPanels_t1827359049 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITIALIZECATCHPHOTOU3EC__ANONSTOREY1_T3556158843_H
#ifndef U3CINSTANTIATEDRAWERBUTTONNEWU3EC__ANONSTOREY1_T2226637809_H
#define U3CINSTANTIATEDRAWERBUTTONNEWU3EC__ANONSTOREY1_T2226637809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrawerHandler/<InstantiateDrawerButtonNew>c__AnonStorey1
struct  U3CInstantiateDrawerButtonNewU3Ec__AnonStorey1_t2226637809  : public RuntimeObject
{
public:
	// UnityEngine.GameObject DrawerHandler/<InstantiateDrawerButtonNew>c__AnonStorey1::objectInScene
	GameObject_t1756533147 * ___objectInScene_0;
	// DrawerHandler DrawerHandler/<InstantiateDrawerButtonNew>c__AnonStorey1::$this
	DrawerHandler_t321192673 * ___U24this_1;

public:
	inline static int32_t get_offset_of_objectInScene_0() { return static_cast<int32_t>(offsetof(U3CInstantiateDrawerButtonNewU3Ec__AnonStorey1_t2226637809, ___objectInScene_0)); }
	inline GameObject_t1756533147 * get_objectInScene_0() const { return ___objectInScene_0; }
	inline GameObject_t1756533147 ** get_address_of_objectInScene_0() { return &___objectInScene_0; }
	inline void set_objectInScene_0(GameObject_t1756533147 * value)
	{
		___objectInScene_0 = value;
		Il2CppCodeGenWriteBarrier((&___objectInScene_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CInstantiateDrawerButtonNewU3Ec__AnonStorey1_t2226637809, ___U24this_1)); }
	inline DrawerHandler_t321192673 * get_U24this_1() const { return ___U24this_1; }
	inline DrawerHandler_t321192673 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(DrawerHandler_t321192673 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINSTANTIATEDRAWERBUTTONNEWU3EC__ANONSTOREY1_T2226637809_H
#ifndef U3CINITIALIZCATCHPHOTOU3EC__ANONSTOREY1_T471598695_H
#define U3CINITIALIZCATCHPHOTOU3EC__ANONSTOREY1_T471598695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.Remark.RemarkHandlerMobile/<initializCatchPhoto>c__AnonStorey1
struct  U3CinitializCatchPhotoU3Ec__AnonStorey1_t471598695  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text Library.Code.UI.Remark.RemarkHandlerMobile/<initializCatchPhoto>c__AnonStorey1::labelPhoto
	Text_t356221433 * ___labelPhoto_0;
	// Library.Code.Facades.PathHolder.PathHolder Library.Code.UI.Remark.RemarkHandlerMobile/<initializCatchPhoto>c__AnonStorey1::holder
	RuntimeObject* ___holder_1;
	// Library.Code.UI.Remark.RemarkHandlerMobile Library.Code.UI.Remark.RemarkHandlerMobile/<initializCatchPhoto>c__AnonStorey1::$this
	RemarkHandlerMobile_t1067069532 * ___U24this_2;

public:
	inline static int32_t get_offset_of_labelPhoto_0() { return static_cast<int32_t>(offsetof(U3CinitializCatchPhotoU3Ec__AnonStorey1_t471598695, ___labelPhoto_0)); }
	inline Text_t356221433 * get_labelPhoto_0() const { return ___labelPhoto_0; }
	inline Text_t356221433 ** get_address_of_labelPhoto_0() { return &___labelPhoto_0; }
	inline void set_labelPhoto_0(Text_t356221433 * value)
	{
		___labelPhoto_0 = value;
		Il2CppCodeGenWriteBarrier((&___labelPhoto_0), value);
	}

	inline static int32_t get_offset_of_holder_1() { return static_cast<int32_t>(offsetof(U3CinitializCatchPhotoU3Ec__AnonStorey1_t471598695, ___holder_1)); }
	inline RuntimeObject* get_holder_1() const { return ___holder_1; }
	inline RuntimeObject** get_address_of_holder_1() { return &___holder_1; }
	inline void set_holder_1(RuntimeObject* value)
	{
		___holder_1 = value;
		Il2CppCodeGenWriteBarrier((&___holder_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CinitializCatchPhotoU3Ec__AnonStorey1_t471598695, ___U24this_2)); }
	inline RemarkHandlerMobile_t1067069532 * get_U24this_2() const { return ___U24this_2; }
	inline RemarkHandlerMobile_t1067069532 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(RemarkHandlerMobile_t1067069532 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITIALIZCATCHPHOTOU3EC__ANONSTOREY1_T471598695_H
#ifndef HTTPPUT_T653802679_H
#define HTTPPUT_T653802679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.Core.HttpPut
struct  HttpPut_t653802679  : public HttpBase_t4034084527
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPPUT_T653802679_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef MESSAGELISTBUILDERIMP_T1204586794_H
#define MESSAGELISTBUILDERIMP_T1204586794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI.MessageListBuilderImp
struct  MessageListBuilderImp_t1204586794  : public MessageListBuilder_t1028067940
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGELISTBUILDERIMP_T1204586794_H
#ifndef MULTIPARTFORMDATACONTENT_T1375345943_H
#define MULTIPARTFORMDATACONTENT_T1375345943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.MultipartFormDataContent
struct  MultipartFormDataContent_t1375345943  : public MultipartContent_t115045161
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTIPARTFORMDATACONTENT_T1375345943_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef HTTPPOST_T2219433260_H
#define HTTPPOST_T2219433260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.Core.HttpPost
struct  HttpPost_t2219433260  : public HttpBase_t4034084527
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPPOST_T2219433260_H
#ifndef WARNINGPOPUP_T2981532944_H
#define WARNINGPOPUP_T2981532944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.PopUp.WarningPopUp
struct  WarningPopUp_t2981532944  : public PopUpHandler_t3865200444
{
public:
	// UnityEngine.UI.Button Library.Code.UI.PopUp.WarningPopUp::okButton
	Button_t2872111280 * ___okButton_2;
	// UnityEngine.UI.Text Library.Code.UI.PopUp.WarningPopUp::titleText
	Text_t356221433 * ___titleText_3;
	// UnityEngine.UI.Text Library.Code.UI.PopUp.WarningPopUp::infoText
	Text_t356221433 * ___infoText_4;

public:
	inline static int32_t get_offset_of_okButton_2() { return static_cast<int32_t>(offsetof(WarningPopUp_t2981532944, ___okButton_2)); }
	inline Button_t2872111280 * get_okButton_2() const { return ___okButton_2; }
	inline Button_t2872111280 ** get_address_of_okButton_2() { return &___okButton_2; }
	inline void set_okButton_2(Button_t2872111280 * value)
	{
		___okButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___okButton_2), value);
	}

	inline static int32_t get_offset_of_titleText_3() { return static_cast<int32_t>(offsetof(WarningPopUp_t2981532944, ___titleText_3)); }
	inline Text_t356221433 * get_titleText_3() const { return ___titleText_3; }
	inline Text_t356221433 ** get_address_of_titleText_3() { return &___titleText_3; }
	inline void set_titleText_3(Text_t356221433 * value)
	{
		___titleText_3 = value;
		Il2CppCodeGenWriteBarrier((&___titleText_3), value);
	}

	inline static int32_t get_offset_of_infoText_4() { return static_cast<int32_t>(offsetof(WarningPopUp_t2981532944, ___infoText_4)); }
	inline Text_t356221433 * get_infoText_4() const { return ___infoText_4; }
	inline Text_t356221433 ** get_address_of_infoText_4() { return &___infoText_4; }
	inline void set_infoText_4(Text_t356221433 * value)
	{
		___infoText_4 = value;
		Il2CppCodeGenWriteBarrier((&___infoText_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WARNINGPOPUP_T2981532944_H
#ifndef NOHTTPMETHODSETEXCEPTION_T721769271_H
#define NOHTTPMETHODSETEXCEPTION_T721769271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.WebRequest.NoHttpMethodSetException
struct  NoHttpMethodSetException_t721769271  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOHTTPMETHODSETEXCEPTION_T721769271_H
#ifndef NOURLSETTED_T1586831947_H
#define NOURLSETTED_T1586831947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.WebRequest.NoUrlSetted
struct  NoUrlSetted_t1586831947  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOURLSETTED_T1586831947_H
#ifndef HTTPDELETE_T3529924879_H
#define HTTPDELETE_T3529924879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.Core.HttpDelete
struct  HttpDelete_t3529924879  : public HttpBase_t4034084527
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPDELETE_T3529924879_H
#ifndef HTTPGET_T2212980058_H
#define HTTPGET_T2212980058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.Core.HttpGet
struct  HttpGet_t2212980058  : public HttpBase_t4034084527
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPGET_T2212980058_H
#ifndef HTTPPATCH_T3849283608_H
#define HTTPPATCH_T3849283608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.Core.HttpPatch
struct  HttpPatch_t3849283608  : public HttpBase_t4034084527
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPPATCH_T3849283608_H
#ifndef TEXTCOLOR_T3858664002_H
#define TEXTCOLOR_T3858664002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextColor
struct  TextColor_t3858664002  : public RuntimeObject
{
public:

public:
};

struct TextColor_t3858664002_StaticFields
{
public:
	// UnityEngine.Color TextColor::textPrimaryColor
	Color_t2020392075  ___textPrimaryColor_0;
	// UnityEngine.Color TextColor::textSecondaryColor
	Color_t2020392075  ___textSecondaryColor_1;
	// UnityEngine.Color TextColor::textAccentColor
	Color_t2020392075  ___textAccentColor_2;
	// UnityEngine.Color TextColor::textTetriaryColor
	Color_t2020392075  ___textTetriaryColor_3;
	// UnityEngine.Color TextColor::testTextPrimaryColor
	Color_t2020392075  ___testTextPrimaryColor_4;
	// UnityEngine.Color TextColor::testTextSecondaryColor
	Color_t2020392075  ___testTextSecondaryColor_5;
	// UnityEngine.Color TextColor::testTextAccentColor
	Color_t2020392075  ___testTextAccentColor_6;
	// UnityEngine.Color TextColor::testTextTetriaryColor
	Color_t2020392075  ___testTextTetriaryColor_7;

public:
	inline static int32_t get_offset_of_textPrimaryColor_0() { return static_cast<int32_t>(offsetof(TextColor_t3858664002_StaticFields, ___textPrimaryColor_0)); }
	inline Color_t2020392075  get_textPrimaryColor_0() const { return ___textPrimaryColor_0; }
	inline Color_t2020392075 * get_address_of_textPrimaryColor_0() { return &___textPrimaryColor_0; }
	inline void set_textPrimaryColor_0(Color_t2020392075  value)
	{
		___textPrimaryColor_0 = value;
	}

	inline static int32_t get_offset_of_textSecondaryColor_1() { return static_cast<int32_t>(offsetof(TextColor_t3858664002_StaticFields, ___textSecondaryColor_1)); }
	inline Color_t2020392075  get_textSecondaryColor_1() const { return ___textSecondaryColor_1; }
	inline Color_t2020392075 * get_address_of_textSecondaryColor_1() { return &___textSecondaryColor_1; }
	inline void set_textSecondaryColor_1(Color_t2020392075  value)
	{
		___textSecondaryColor_1 = value;
	}

	inline static int32_t get_offset_of_textAccentColor_2() { return static_cast<int32_t>(offsetof(TextColor_t3858664002_StaticFields, ___textAccentColor_2)); }
	inline Color_t2020392075  get_textAccentColor_2() const { return ___textAccentColor_2; }
	inline Color_t2020392075 * get_address_of_textAccentColor_2() { return &___textAccentColor_2; }
	inline void set_textAccentColor_2(Color_t2020392075  value)
	{
		___textAccentColor_2 = value;
	}

	inline static int32_t get_offset_of_textTetriaryColor_3() { return static_cast<int32_t>(offsetof(TextColor_t3858664002_StaticFields, ___textTetriaryColor_3)); }
	inline Color_t2020392075  get_textTetriaryColor_3() const { return ___textTetriaryColor_3; }
	inline Color_t2020392075 * get_address_of_textTetriaryColor_3() { return &___textTetriaryColor_3; }
	inline void set_textTetriaryColor_3(Color_t2020392075  value)
	{
		___textTetriaryColor_3 = value;
	}

	inline static int32_t get_offset_of_testTextPrimaryColor_4() { return static_cast<int32_t>(offsetof(TextColor_t3858664002_StaticFields, ___testTextPrimaryColor_4)); }
	inline Color_t2020392075  get_testTextPrimaryColor_4() const { return ___testTextPrimaryColor_4; }
	inline Color_t2020392075 * get_address_of_testTextPrimaryColor_4() { return &___testTextPrimaryColor_4; }
	inline void set_testTextPrimaryColor_4(Color_t2020392075  value)
	{
		___testTextPrimaryColor_4 = value;
	}

	inline static int32_t get_offset_of_testTextSecondaryColor_5() { return static_cast<int32_t>(offsetof(TextColor_t3858664002_StaticFields, ___testTextSecondaryColor_5)); }
	inline Color_t2020392075  get_testTextSecondaryColor_5() const { return ___testTextSecondaryColor_5; }
	inline Color_t2020392075 * get_address_of_testTextSecondaryColor_5() { return &___testTextSecondaryColor_5; }
	inline void set_testTextSecondaryColor_5(Color_t2020392075  value)
	{
		___testTextSecondaryColor_5 = value;
	}

	inline static int32_t get_offset_of_testTextAccentColor_6() { return static_cast<int32_t>(offsetof(TextColor_t3858664002_StaticFields, ___testTextAccentColor_6)); }
	inline Color_t2020392075  get_testTextAccentColor_6() const { return ___testTextAccentColor_6; }
	inline Color_t2020392075 * get_address_of_testTextAccentColor_6() { return &___testTextAccentColor_6; }
	inline void set_testTextAccentColor_6(Color_t2020392075  value)
	{
		___testTextAccentColor_6 = value;
	}

	inline static int32_t get_offset_of_testTextTetriaryColor_7() { return static_cast<int32_t>(offsetof(TextColor_t3858664002_StaticFields, ___testTextTetriaryColor_7)); }
	inline Color_t2020392075  get_testTextTetriaryColor_7() const { return ___testTextTetriaryColor_7; }
	inline Color_t2020392075 * get_address_of_testTextTetriaryColor_7() { return &___testTextTetriaryColor_7; }
	inline void set_testTextTetriaryColor_7(Color_t2020392075  value)
	{
		___testTextTetriaryColor_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTCOLOR_T3858664002_H
#ifndef POPUPS_T1014109545_H
#define POPUPS_T1014109545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.PopUp.PopUps
struct  PopUps_t1014109545 
{
public:
	// System.Int32 Library.Code.UI.PopUp.PopUps::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PopUps_t1014109545, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPUPS_T1014109545_H
#ifndef CONTENTREADACTION_T1342170461_H
#define CONTENTREADACTION_T1342170461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.ContentReadAction
struct  ContentReadAction_t1342170461 
{
public:
	// System.Int32 CI.HttpClient.ContentReadAction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ContentReadAction_t1342170461, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTREADACTION_T1342170461_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef HTTPCOMPLETIONOPTION_T1897188955_H
#define HTTPCOMPLETIONOPTION_T1897188955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.HttpCompletionOption
struct  HttpCompletionOption_t1897188955 
{
public:
	// System.Int32 CI.HttpClient.HttpCompletionOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpCompletionOption_t1897188955, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCOMPLETIONOPTION_T1897188955_H
#ifndef WEBREQUESTTYPE_T625788535_H
#define WEBREQUESTTYPE_T625788535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.WebRequest.WebRequestType
struct  WebRequestType_t625788535 
{
public:
	// System.Int32 Library.Code.WebRequest.WebRequestType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WebRequestType_t625788535, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTTYPE_T625788535_H
#ifndef HTTPACTION_T943117648_H
#define HTTPACTION_T943117648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.HttpAction
struct  HttpAction_t943117648 
{
public:
	// System.Int32 CI.HttpClient.HttpAction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpAction_t943117648, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPACTION_T943117648_H
#ifndef PREFABS_T3645706183_H
#define PREFABS_T3645706183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.PrefabGenerator.Prefabs
struct  Prefabs_t3645706183 
{
public:
	// System.Int32 Library.Code.UI.PrefabGenerator.Prefabs::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Prefabs_t3645706183, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABS_T3645706183_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef POPUPSTUFF_T3426923330_H
#define POPUPSTUFF_T3426923330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.PopUp.PopUpStuff
struct  PopUpStuff_t3426923330  : public RuntimeObject
{
public:
	// System.String Library.Code.UI.PopUp.PopUpStuff::title
	String_t* ___title_0;
	// System.String Library.Code.UI.PopUp.PopUpStuff::text
	String_t* ___text_1;
	// System.Action Library.Code.UI.PopUp.PopUpStuff::okAction
	Action_t3226471752 * ___okAction_2;
	// System.Action Library.Code.UI.PopUp.PopUpStuff::cancelAction
	Action_t3226471752 * ___cancelAction_3;
	// Library.Code.UI.PopUp.PopUps Library.Code.UI.PopUp.PopUpStuff::type
	int32_t ___type_4;

public:
	inline static int32_t get_offset_of_title_0() { return static_cast<int32_t>(offsetof(PopUpStuff_t3426923330, ___title_0)); }
	inline String_t* get_title_0() const { return ___title_0; }
	inline String_t** get_address_of_title_0() { return &___title_0; }
	inline void set_title_0(String_t* value)
	{
		___title_0 = value;
		Il2CppCodeGenWriteBarrier((&___title_0), value);
	}

	inline static int32_t get_offset_of_text_1() { return static_cast<int32_t>(offsetof(PopUpStuff_t3426923330, ___text_1)); }
	inline String_t* get_text_1() const { return ___text_1; }
	inline String_t** get_address_of_text_1() { return &___text_1; }
	inline void set_text_1(String_t* value)
	{
		___text_1 = value;
		Il2CppCodeGenWriteBarrier((&___text_1), value);
	}

	inline static int32_t get_offset_of_okAction_2() { return static_cast<int32_t>(offsetof(PopUpStuff_t3426923330, ___okAction_2)); }
	inline Action_t3226471752 * get_okAction_2() const { return ___okAction_2; }
	inline Action_t3226471752 ** get_address_of_okAction_2() { return &___okAction_2; }
	inline void set_okAction_2(Action_t3226471752 * value)
	{
		___okAction_2 = value;
		Il2CppCodeGenWriteBarrier((&___okAction_2), value);
	}

	inline static int32_t get_offset_of_cancelAction_3() { return static_cast<int32_t>(offsetof(PopUpStuff_t3426923330, ___cancelAction_3)); }
	inline Action_t3226471752 * get_cancelAction_3() const { return ___cancelAction_3; }
	inline Action_t3226471752 ** get_address_of_cancelAction_3() { return &___cancelAction_3; }
	inline void set_cancelAction_3(Action_t3226471752 * value)
	{
		___cancelAction_3 = value;
		Il2CppCodeGenWriteBarrier((&___cancelAction_3), value);
	}

	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(PopUpStuff_t3426923330, ___type_4)); }
	inline int32_t get_type_4() const { return ___type_4; }
	inline int32_t* get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(int32_t value)
	{
		___type_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPUPSTUFF_T3426923330_H
#ifndef U3CPOSTU3EC__ANONSTOREY7_T1971601809_H
#define U3CPOSTU3EC__ANONSTOREY7_T1971601809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.HttpClient/<Post>c__AnonStorey7
struct  U3CPostU3Ec__AnonStorey7_t1971601809  : public RuntimeObject
{
public:
	// System.Uri CI.HttpClient.HttpClient/<Post>c__AnonStorey7::uri
	Uri_t19570940 * ___uri_0;
	// CI.HttpClient.IHttpContent CI.HttpClient.HttpClient/<Post>c__AnonStorey7::content
	RuntimeObject* ___content_1;
	// CI.HttpClient.HttpCompletionOption CI.HttpClient.HttpClient/<Post>c__AnonStorey7::completionOption
	int32_t ___completionOption_2;
	// System.Action`1<CI.HttpClient.HttpResponseMessage`1<System.Byte[]>> CI.HttpClient.HttpClient/<Post>c__AnonStorey7::responseCallback
	Action_1_t2548510215 * ___responseCallback_3;
	// System.Action`1<CI.HttpClient.UploadStatusMessage> CI.HttpClient.HttpClient/<Post>c__AnonStorey7::uploadStatusCallback
	Action_1_t3988964304 * ___uploadStatusCallback_4;
	// CI.HttpClient.HttpClient CI.HttpClient.HttpClient/<Post>c__AnonStorey7::$this
	HttpClient_t2113786063 * ___U24this_5;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey7_t1971601809, ___uri_0)); }
	inline Uri_t19570940 * get_uri_0() const { return ___uri_0; }
	inline Uri_t19570940 ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_t19570940 * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey7_t1971601809, ___content_1)); }
	inline RuntimeObject* get_content_1() const { return ___content_1; }
	inline RuntimeObject** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(RuntimeObject* value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_1), value);
	}

	inline static int32_t get_offset_of_completionOption_2() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey7_t1971601809, ___completionOption_2)); }
	inline int32_t get_completionOption_2() const { return ___completionOption_2; }
	inline int32_t* get_address_of_completionOption_2() { return &___completionOption_2; }
	inline void set_completionOption_2(int32_t value)
	{
		___completionOption_2 = value;
	}

	inline static int32_t get_offset_of_responseCallback_3() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey7_t1971601809, ___responseCallback_3)); }
	inline Action_1_t2548510215 * get_responseCallback_3() const { return ___responseCallback_3; }
	inline Action_1_t2548510215 ** get_address_of_responseCallback_3() { return &___responseCallback_3; }
	inline void set_responseCallback_3(Action_1_t2548510215 * value)
	{
		___responseCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___responseCallback_3), value);
	}

	inline static int32_t get_offset_of_uploadStatusCallback_4() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey7_t1971601809, ___uploadStatusCallback_4)); }
	inline Action_1_t3988964304 * get_uploadStatusCallback_4() const { return ___uploadStatusCallback_4; }
	inline Action_1_t3988964304 ** get_address_of_uploadStatusCallback_4() { return &___uploadStatusCallback_4; }
	inline void set_uploadStatusCallback_4(Action_1_t3988964304 * value)
	{
		___uploadStatusCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___uploadStatusCallback_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey7_t1971601809, ___U24this_5)); }
	inline HttpClient_t2113786063 * get_U24this_5() const { return ___U24this_5; }
	inline HttpClient_t2113786063 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(HttpClient_t2113786063 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOSTU3EC__ANONSTOREY7_T1971601809_H
#ifndef U3CPATCHU3EC__ANONSTOREY5_T907742601_H
#define U3CPATCHU3EC__ANONSTOREY5_T907742601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.HttpClient/<Patch>c__AnonStorey5
struct  U3CPatchU3Ec__AnonStorey5_t907742601  : public RuntimeObject
{
public:
	// System.Uri CI.HttpClient.HttpClient/<Patch>c__AnonStorey5::uri
	Uri_t19570940 * ___uri_0;
	// CI.HttpClient.IHttpContent CI.HttpClient.HttpClient/<Patch>c__AnonStorey5::content
	RuntimeObject* ___content_1;
	// CI.HttpClient.HttpCompletionOption CI.HttpClient.HttpClient/<Patch>c__AnonStorey5::completionOption
	int32_t ___completionOption_2;
	// System.Action`1<CI.HttpClient.HttpResponseMessage`1<System.Byte[]>> CI.HttpClient.HttpClient/<Patch>c__AnonStorey5::responseCallback
	Action_1_t2548510215 * ___responseCallback_3;
	// System.Action`1<CI.HttpClient.UploadStatusMessage> CI.HttpClient.HttpClient/<Patch>c__AnonStorey5::uploadStatusCallback
	Action_1_t3988964304 * ___uploadStatusCallback_4;
	// CI.HttpClient.HttpClient CI.HttpClient.HttpClient/<Patch>c__AnonStorey5::$this
	HttpClient_t2113786063 * ___U24this_5;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(U3CPatchU3Ec__AnonStorey5_t907742601, ___uri_0)); }
	inline Uri_t19570940 * get_uri_0() const { return ___uri_0; }
	inline Uri_t19570940 ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_t19570940 * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(U3CPatchU3Ec__AnonStorey5_t907742601, ___content_1)); }
	inline RuntimeObject* get_content_1() const { return ___content_1; }
	inline RuntimeObject** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(RuntimeObject* value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_1), value);
	}

	inline static int32_t get_offset_of_completionOption_2() { return static_cast<int32_t>(offsetof(U3CPatchU3Ec__AnonStorey5_t907742601, ___completionOption_2)); }
	inline int32_t get_completionOption_2() const { return ___completionOption_2; }
	inline int32_t* get_address_of_completionOption_2() { return &___completionOption_2; }
	inline void set_completionOption_2(int32_t value)
	{
		___completionOption_2 = value;
	}

	inline static int32_t get_offset_of_responseCallback_3() { return static_cast<int32_t>(offsetof(U3CPatchU3Ec__AnonStorey5_t907742601, ___responseCallback_3)); }
	inline Action_1_t2548510215 * get_responseCallback_3() const { return ___responseCallback_3; }
	inline Action_1_t2548510215 ** get_address_of_responseCallback_3() { return &___responseCallback_3; }
	inline void set_responseCallback_3(Action_1_t2548510215 * value)
	{
		___responseCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___responseCallback_3), value);
	}

	inline static int32_t get_offset_of_uploadStatusCallback_4() { return static_cast<int32_t>(offsetof(U3CPatchU3Ec__AnonStorey5_t907742601, ___uploadStatusCallback_4)); }
	inline Action_1_t3988964304 * get_uploadStatusCallback_4() const { return ___uploadStatusCallback_4; }
	inline Action_1_t3988964304 ** get_address_of_uploadStatusCallback_4() { return &___uploadStatusCallback_4; }
	inline void set_uploadStatusCallback_4(Action_1_t3988964304 * value)
	{
		___uploadStatusCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___uploadStatusCallback_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CPatchU3Ec__AnonStorey5_t907742601, ___U24this_5)); }
	inline HttpClient_t2113786063 * get_U24this_5() const { return ___U24this_5; }
	inline HttpClient_t2113786063 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(HttpClient_t2113786063 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPATCHU3EC__ANONSTOREY5_T907742601_H
#ifndef U3CGETBYTEARRAYU3EC__ANONSTOREY3_T424213474_H
#define U3CGETBYTEARRAYU3EC__ANONSTOREY3_T424213474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.HttpClient/<GetByteArray>c__AnonStorey3
struct  U3CGetByteArrayU3Ec__AnonStorey3_t424213474  : public RuntimeObject
{
public:
	// System.Uri CI.HttpClient.HttpClient/<GetByteArray>c__AnonStorey3::uri
	Uri_t19570940 * ___uri_0;
	// CI.HttpClient.HttpCompletionOption CI.HttpClient.HttpClient/<GetByteArray>c__AnonStorey3::completionOption
	int32_t ___completionOption_1;
	// System.Action`1<CI.HttpClient.HttpResponseMessage`1<System.Byte[]>> CI.HttpClient.HttpClient/<GetByteArray>c__AnonStorey3::responseCallback
	Action_1_t2548510215 * ___responseCallback_2;
	// CI.HttpClient.HttpClient CI.HttpClient.HttpClient/<GetByteArray>c__AnonStorey3::$this
	HttpClient_t2113786063 * ___U24this_3;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(U3CGetByteArrayU3Ec__AnonStorey3_t424213474, ___uri_0)); }
	inline Uri_t19570940 * get_uri_0() const { return ___uri_0; }
	inline Uri_t19570940 ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_t19570940 * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_completionOption_1() { return static_cast<int32_t>(offsetof(U3CGetByteArrayU3Ec__AnonStorey3_t424213474, ___completionOption_1)); }
	inline int32_t get_completionOption_1() const { return ___completionOption_1; }
	inline int32_t* get_address_of_completionOption_1() { return &___completionOption_1; }
	inline void set_completionOption_1(int32_t value)
	{
		___completionOption_1 = value;
	}

	inline static int32_t get_offset_of_responseCallback_2() { return static_cast<int32_t>(offsetof(U3CGetByteArrayU3Ec__AnonStorey3_t424213474, ___responseCallback_2)); }
	inline Action_1_t2548510215 * get_responseCallback_2() const { return ___responseCallback_2; }
	inline Action_1_t2548510215 ** get_address_of_responseCallback_2() { return &___responseCallback_2; }
	inline void set_responseCallback_2(Action_1_t2548510215 * value)
	{
		___responseCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___responseCallback_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CGetByteArrayU3Ec__AnonStorey3_t424213474, ___U24this_3)); }
	inline HttpClient_t2113786063 * get_U24this_3() const { return ___U24this_3; }
	inline HttpClient_t2113786063 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(HttpClient_t2113786063 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETBYTEARRAYU3EC__ANONSTOREY3_T424213474_H
#ifndef U3CDELETEU3EC__ANONSTOREY1_T1672730660_H
#define U3CDELETEU3EC__ANONSTOREY1_T1672730660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.HttpClient/<Delete>c__AnonStorey1
struct  U3CDeleteU3Ec__AnonStorey1_t1672730660  : public RuntimeObject
{
public:
	// System.Uri CI.HttpClient.HttpClient/<Delete>c__AnonStorey1::uri
	Uri_t19570940 * ___uri_0;
	// CI.HttpClient.HttpCompletionOption CI.HttpClient.HttpClient/<Delete>c__AnonStorey1::completionOption
	int32_t ___completionOption_1;
	// System.Action`1<CI.HttpClient.HttpResponseMessage`1<System.Byte[]>> CI.HttpClient.HttpClient/<Delete>c__AnonStorey1::responseCallback
	Action_1_t2548510215 * ___responseCallback_2;
	// CI.HttpClient.HttpClient CI.HttpClient.HttpClient/<Delete>c__AnonStorey1::$this
	HttpClient_t2113786063 * ___U24this_3;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(U3CDeleteU3Ec__AnonStorey1_t1672730660, ___uri_0)); }
	inline Uri_t19570940 * get_uri_0() const { return ___uri_0; }
	inline Uri_t19570940 ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_t19570940 * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_completionOption_1() { return static_cast<int32_t>(offsetof(U3CDeleteU3Ec__AnonStorey1_t1672730660, ___completionOption_1)); }
	inline int32_t get_completionOption_1() const { return ___completionOption_1; }
	inline int32_t* get_address_of_completionOption_1() { return &___completionOption_1; }
	inline void set_completionOption_1(int32_t value)
	{
		___completionOption_1 = value;
	}

	inline static int32_t get_offset_of_responseCallback_2() { return static_cast<int32_t>(offsetof(U3CDeleteU3Ec__AnonStorey1_t1672730660, ___responseCallback_2)); }
	inline Action_1_t2548510215 * get_responseCallback_2() const { return ___responseCallback_2; }
	inline Action_1_t2548510215 ** get_address_of_responseCallback_2() { return &___responseCallback_2; }
	inline void set_responseCallback_2(Action_1_t2548510215 * value)
	{
		___responseCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___responseCallback_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CDeleteU3Ec__AnonStorey1_t1672730660, ___U24this_3)); }
	inline HttpClient_t2113786063 * get_U24this_3() const { return ___U24this_3; }
	inline HttpClient_t2113786063 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(HttpClient_t2113786063 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELETEU3EC__ANONSTOREY1_T1672730660_H
#ifndef U3CPUTU3EC__ANONSTOREY9_T1490382948_H
#define U3CPUTU3EC__ANONSTOREY9_T1490382948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.HttpClient/<Put>c__AnonStorey9
struct  U3CPutU3Ec__AnonStorey9_t1490382948  : public RuntimeObject
{
public:
	// System.Uri CI.HttpClient.HttpClient/<Put>c__AnonStorey9::uri
	Uri_t19570940 * ___uri_0;
	// CI.HttpClient.IHttpContent CI.HttpClient.HttpClient/<Put>c__AnonStorey9::content
	RuntimeObject* ___content_1;
	// CI.HttpClient.HttpCompletionOption CI.HttpClient.HttpClient/<Put>c__AnonStorey9::completionOption
	int32_t ___completionOption_2;
	// System.Action`1<CI.HttpClient.HttpResponseMessage`1<System.Byte[]>> CI.HttpClient.HttpClient/<Put>c__AnonStorey9::responseCallback
	Action_1_t2548510215 * ___responseCallback_3;
	// System.Action`1<CI.HttpClient.UploadStatusMessage> CI.HttpClient.HttpClient/<Put>c__AnonStorey9::uploadStatusCallback
	Action_1_t3988964304 * ___uploadStatusCallback_4;
	// CI.HttpClient.HttpClient CI.HttpClient.HttpClient/<Put>c__AnonStorey9::$this
	HttpClient_t2113786063 * ___U24this_5;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(U3CPutU3Ec__AnonStorey9_t1490382948, ___uri_0)); }
	inline Uri_t19570940 * get_uri_0() const { return ___uri_0; }
	inline Uri_t19570940 ** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(Uri_t19570940 * value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(U3CPutU3Ec__AnonStorey9_t1490382948, ___content_1)); }
	inline RuntimeObject* get_content_1() const { return ___content_1; }
	inline RuntimeObject** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(RuntimeObject* value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_1), value);
	}

	inline static int32_t get_offset_of_completionOption_2() { return static_cast<int32_t>(offsetof(U3CPutU3Ec__AnonStorey9_t1490382948, ___completionOption_2)); }
	inline int32_t get_completionOption_2() const { return ___completionOption_2; }
	inline int32_t* get_address_of_completionOption_2() { return &___completionOption_2; }
	inline void set_completionOption_2(int32_t value)
	{
		___completionOption_2 = value;
	}

	inline static int32_t get_offset_of_responseCallback_3() { return static_cast<int32_t>(offsetof(U3CPutU3Ec__AnonStorey9_t1490382948, ___responseCallback_3)); }
	inline Action_1_t2548510215 * get_responseCallback_3() const { return ___responseCallback_3; }
	inline Action_1_t2548510215 ** get_address_of_responseCallback_3() { return &___responseCallback_3; }
	inline void set_responseCallback_3(Action_1_t2548510215 * value)
	{
		___responseCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___responseCallback_3), value);
	}

	inline static int32_t get_offset_of_uploadStatusCallback_4() { return static_cast<int32_t>(offsetof(U3CPutU3Ec__AnonStorey9_t1490382948, ___uploadStatusCallback_4)); }
	inline Action_1_t3988964304 * get_uploadStatusCallback_4() const { return ___uploadStatusCallback_4; }
	inline Action_1_t3988964304 ** get_address_of_uploadStatusCallback_4() { return &___uploadStatusCallback_4; }
	inline void set_uploadStatusCallback_4(Action_1_t3988964304 * value)
	{
		___uploadStatusCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___uploadStatusCallback_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CPutU3Ec__AnonStorey9_t1490382948, ___U24this_5)); }
	inline HttpClient_t2113786063 * get_U24this_5() const { return ___U24this_5; }
	inline HttpClient_t2113786063 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(HttpClient_t2113786063 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPUTU3EC__ANONSTOREY9_T1490382948_H
#ifndef WEBREQUESTBUILDER_T3983605310_H
#define WEBREQUESTBUILDER_T3983605310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.WebRequest.WebRequestBuilder
struct  WebRequestBuilder_t3983605310  : public RuntimeObject
{
public:
	// Library.Code.WebRequest.WebRequestType Library.Code.WebRequest.WebRequestBuilder::webRequestType
	int32_t ___webRequestType_0;
	// Library.Code.WebRequest.UserDataNetworkInfoProvider Library.Code.WebRequest.WebRequestBuilder::_dataNetworkInfoProvider
	RuntimeObject* ____dataNetworkInfoProvider_1;
	// System.String Library.Code.WebRequest.WebRequestBuilder::url
	String_t* ___url_2;
	// System.String Library.Code.WebRequest.WebRequestBuilder::data
	String_t* ___data_3;

public:
	inline static int32_t get_offset_of_webRequestType_0() { return static_cast<int32_t>(offsetof(WebRequestBuilder_t3983605310, ___webRequestType_0)); }
	inline int32_t get_webRequestType_0() const { return ___webRequestType_0; }
	inline int32_t* get_address_of_webRequestType_0() { return &___webRequestType_0; }
	inline void set_webRequestType_0(int32_t value)
	{
		___webRequestType_0 = value;
	}

	inline static int32_t get_offset_of__dataNetworkInfoProvider_1() { return static_cast<int32_t>(offsetof(WebRequestBuilder_t3983605310, ____dataNetworkInfoProvider_1)); }
	inline RuntimeObject* get__dataNetworkInfoProvider_1() const { return ____dataNetworkInfoProvider_1; }
	inline RuntimeObject** get_address_of__dataNetworkInfoProvider_1() { return &____dataNetworkInfoProvider_1; }
	inline void set__dataNetworkInfoProvider_1(RuntimeObject* value)
	{
		____dataNetworkInfoProvider_1 = value;
		Il2CppCodeGenWriteBarrier((&____dataNetworkInfoProvider_1), value);
	}

	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(WebRequestBuilder_t3983605310, ___url_2)); }
	inline String_t* get_url_2() const { return ___url_2; }
	inline String_t** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(String_t* value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier((&___url_2), value);
	}

	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(WebRequestBuilder_t3983605310, ___data_3)); }
	inline String_t* get_data_3() const { return ___data_3; }
	inline String_t** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(String_t* value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier((&___data_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTBUILDER_T3983605310_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef ELEMENT3DPROVIDER_T3146139312_H
#define ELEMENT3DPROVIDER_T3146139312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.Element3d.Element3dProvider
struct  Element3dProvider_t3146139312  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Networking._3dElement.DownloadElement3d Library.Code.DI.Element3d.Element3dProvider::_downloadElement3D
	RuntimeObject* ____downloadElement3D_2;
	// Library.Code.DI.Element3d.Element3dModule Library.Code.DI.Element3d.Element3dProvider::_element3DModule
	Element3dModule_t2912470317 * ____element3DModule_3;
	// Library.Code.DI.Elements.ElementProvider Library.Code.DI.Element3d.Element3dProvider::_elementProvider
	ElementProvider_t3018113545 * ____elementProvider_4;
	// Library.Code.DI.Element3d.CheckpointModule Library.Code.DI.Element3d.Element3dProvider::_checkpointModule
	CheckpointModule_t504654608 * ____checkpointModule_5;

public:
	inline static int32_t get_offset_of__downloadElement3D_2() { return static_cast<int32_t>(offsetof(Element3dProvider_t3146139312, ____downloadElement3D_2)); }
	inline RuntimeObject* get__downloadElement3D_2() const { return ____downloadElement3D_2; }
	inline RuntimeObject** get_address_of__downloadElement3D_2() { return &____downloadElement3D_2; }
	inline void set__downloadElement3D_2(RuntimeObject* value)
	{
		____downloadElement3D_2 = value;
		Il2CppCodeGenWriteBarrier((&____downloadElement3D_2), value);
	}

	inline static int32_t get_offset_of__element3DModule_3() { return static_cast<int32_t>(offsetof(Element3dProvider_t3146139312, ____element3DModule_3)); }
	inline Element3dModule_t2912470317 * get__element3DModule_3() const { return ____element3DModule_3; }
	inline Element3dModule_t2912470317 ** get_address_of__element3DModule_3() { return &____element3DModule_3; }
	inline void set__element3DModule_3(Element3dModule_t2912470317 * value)
	{
		____element3DModule_3 = value;
		Il2CppCodeGenWriteBarrier((&____element3DModule_3), value);
	}

	inline static int32_t get_offset_of__elementProvider_4() { return static_cast<int32_t>(offsetof(Element3dProvider_t3146139312, ____elementProvider_4)); }
	inline ElementProvider_t3018113545 * get__elementProvider_4() const { return ____elementProvider_4; }
	inline ElementProvider_t3018113545 ** get_address_of__elementProvider_4() { return &____elementProvider_4; }
	inline void set__elementProvider_4(ElementProvider_t3018113545 * value)
	{
		____elementProvider_4 = value;
		Il2CppCodeGenWriteBarrier((&____elementProvider_4), value);
	}

	inline static int32_t get_offset_of__checkpointModule_5() { return static_cast<int32_t>(offsetof(Element3dProvider_t3146139312, ____checkpointModule_5)); }
	inline CheckpointModule_t504654608 * get__checkpointModule_5() const { return ____checkpointModule_5; }
	inline CheckpointModule_t504654608 ** get_address_of__checkpointModule_5() { return &____checkpointModule_5; }
	inline void set__checkpointModule_5(CheckpointModule_t504654608 * value)
	{
		____checkpointModule_5 = value;
		Il2CppCodeGenWriteBarrier((&____checkpointModule_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT3DPROVIDER_T3146139312_H
#ifndef PROGRESSBAR_T521705726_H
#define PROGRESSBAR_T521705726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ProgressBar
struct  ProgressBar_t521705726  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.RectTransform Library.Code.UI.ProgressBar::rectComponent
	RectTransform_t3349966182 * ___rectComponent_2;
	// System.Single Library.Code.UI.ProgressBar::rotateSpeed
	float ___rotateSpeed_3;

public:
	inline static int32_t get_offset_of_rectComponent_2() { return static_cast<int32_t>(offsetof(ProgressBar_t521705726, ___rectComponent_2)); }
	inline RectTransform_t3349966182 * get_rectComponent_2() const { return ___rectComponent_2; }
	inline RectTransform_t3349966182 ** get_address_of_rectComponent_2() { return &___rectComponent_2; }
	inline void set_rectComponent_2(RectTransform_t3349966182 * value)
	{
		___rectComponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___rectComponent_2), value);
	}

	inline static int32_t get_offset_of_rotateSpeed_3() { return static_cast<int32_t>(offsetof(ProgressBar_t521705726, ___rotateSpeed_3)); }
	inline float get_rotateSpeed_3() const { return ___rotateSpeed_3; }
	inline float* get_address_of_rotateSpeed_3() { return &___rotateSpeed_3; }
	inline void set_rotateSpeed_3(float value)
	{
		___rotateSpeed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSBAR_T521705726_H
#ifndef ELEMENTHANDLERPROVIDER_T2338674629_H
#define ELEMENTHANDLERPROVIDER_T2338674629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.ElementHandler.ElementHandlerProvider
struct  ElementHandlerProvider_t2338674629  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.DI.Checklist.ChecklistProvider Library.Code.DI.ElementHandler.ElementHandlerProvider::_checklistProvider
	ChecklistProvider_t3393423783 * ____checklistProvider_2;
	// Library.Code.DI.Element3d.Element3dProvider Library.Code.DI.ElementHandler.ElementHandlerProvider::_element3DProvider
	Element3dProvider_t3146139312 * ____element3DProvider_3;
	// Library.Code.DI.GroupinModel.GroupingModelProvider Library.Code.DI.ElementHandler.ElementHandlerProvider::_modelGroupFacade
	GroupingModelProvider_t2551172685 * ____modelGroupFacade_4;
	// Library.Code.DI.ElementHandler.ElementHandlerModule Library.Code.DI.ElementHandler.ElementHandlerProvider::_elementHandlerModule
	ElementHandlerModule_t3201733362 * ____elementHandlerModule_5;

public:
	inline static int32_t get_offset_of__checklistProvider_2() { return static_cast<int32_t>(offsetof(ElementHandlerProvider_t2338674629, ____checklistProvider_2)); }
	inline ChecklistProvider_t3393423783 * get__checklistProvider_2() const { return ____checklistProvider_2; }
	inline ChecklistProvider_t3393423783 ** get_address_of__checklistProvider_2() { return &____checklistProvider_2; }
	inline void set__checklistProvider_2(ChecklistProvider_t3393423783 * value)
	{
		____checklistProvider_2 = value;
		Il2CppCodeGenWriteBarrier((&____checklistProvider_2), value);
	}

	inline static int32_t get_offset_of__element3DProvider_3() { return static_cast<int32_t>(offsetof(ElementHandlerProvider_t2338674629, ____element3DProvider_3)); }
	inline Element3dProvider_t3146139312 * get__element3DProvider_3() const { return ____element3DProvider_3; }
	inline Element3dProvider_t3146139312 ** get_address_of__element3DProvider_3() { return &____element3DProvider_3; }
	inline void set__element3DProvider_3(Element3dProvider_t3146139312 * value)
	{
		____element3DProvider_3 = value;
		Il2CppCodeGenWriteBarrier((&____element3DProvider_3), value);
	}

	inline static int32_t get_offset_of__modelGroupFacade_4() { return static_cast<int32_t>(offsetof(ElementHandlerProvider_t2338674629, ____modelGroupFacade_4)); }
	inline GroupingModelProvider_t2551172685 * get__modelGroupFacade_4() const { return ____modelGroupFacade_4; }
	inline GroupingModelProvider_t2551172685 ** get_address_of__modelGroupFacade_4() { return &____modelGroupFacade_4; }
	inline void set__modelGroupFacade_4(GroupingModelProvider_t2551172685 * value)
	{
		____modelGroupFacade_4 = value;
		Il2CppCodeGenWriteBarrier((&____modelGroupFacade_4), value);
	}

	inline static int32_t get_offset_of__elementHandlerModule_5() { return static_cast<int32_t>(offsetof(ElementHandlerProvider_t2338674629, ____elementHandlerModule_5)); }
	inline ElementHandlerModule_t3201733362 * get__elementHandlerModule_5() const { return ____elementHandlerModule_5; }
	inline ElementHandlerModule_t3201733362 ** get_address_of__elementHandlerModule_5() { return &____elementHandlerModule_5; }
	inline void set__elementHandlerModule_5(ElementHandlerModule_t3201733362 * value)
	{
		____elementHandlerModule_5 = value;
		Il2CppCodeGenWriteBarrier((&____elementHandlerModule_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTHANDLERPROVIDER_T2338674629_H
#ifndef CLOSEREMARK_T2638852306_H
#define CLOSEREMARK_T2638852306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.Remark.CloseRemark
struct  CloseRemark_t2638852306  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Library.Code.UI.Remark.CloseRemark::_remarkPanel
	GameObject_t1756533147 * ____remarkPanel_2;
	// System.Boolean Library.Code.UI.Remark.CloseRemark::_shouldRemoveFiles
	bool ____shouldRemoveFiles_3;

public:
	inline static int32_t get_offset_of__remarkPanel_2() { return static_cast<int32_t>(offsetof(CloseRemark_t2638852306, ____remarkPanel_2)); }
	inline GameObject_t1756533147 * get__remarkPanel_2() const { return ____remarkPanel_2; }
	inline GameObject_t1756533147 ** get_address_of__remarkPanel_2() { return &____remarkPanel_2; }
	inline void set__remarkPanel_2(GameObject_t1756533147 * value)
	{
		____remarkPanel_2 = value;
		Il2CppCodeGenWriteBarrier((&____remarkPanel_2), value);
	}

	inline static int32_t get_offset_of__shouldRemoveFiles_3() { return static_cast<int32_t>(offsetof(CloseRemark_t2638852306, ____shouldRemoveFiles_3)); }
	inline bool get__shouldRemoveFiles_3() const { return ____shouldRemoveFiles_3; }
	inline bool* get_address_of__shouldRemoveFiles_3() { return &____shouldRemoveFiles_3; }
	inline void set__shouldRemoveFiles_3(bool value)
	{
		____shouldRemoveFiles_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSEREMARK_T2638852306_H
#ifndef NEWREMARKHANDLERMOBILE_T798103230_H
#define NEWREMARKHANDLERMOBILE_T798103230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.Remark.NewRemarkHandlerMobile
struct  NewRemarkHandlerMobile_t798103230  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Communication.Communication Library.Code.UI.Remark.NewRemarkHandlerMobile::communication
	Communication_t424466906 * ___communication_2;
	// Library.Code.Facades.CheckListF.CurrentCheckListItemFacade Library.Code.UI.Remark.NewRemarkHandlerMobile::_currentCheckListItemFacade
	RuntimeObject* ____currentCheckListItemFacade_3;
	// UnityEngine.GameObject Library.Code.UI.Remark.NewRemarkHandlerMobile::currentStatusScene
	GameObject_t1756533147 * ___currentStatusScene_4;
	// UnityEngine.UI.Button Library.Code.UI.Remark.NewRemarkHandlerMobile::currentItemButton
	Button_t2872111280 * ___currentItemButton_5;
	// Library.Code.Domain.Entities.Checklist.ItemStatus Library.Code.UI.Remark.NewRemarkHandlerMobile::forcedItemStauts
	ItemStatus_t1378751697 * ___forcedItemStauts_6;

public:
	inline static int32_t get_offset_of_communication_2() { return static_cast<int32_t>(offsetof(NewRemarkHandlerMobile_t798103230, ___communication_2)); }
	inline Communication_t424466906 * get_communication_2() const { return ___communication_2; }
	inline Communication_t424466906 ** get_address_of_communication_2() { return &___communication_2; }
	inline void set_communication_2(Communication_t424466906 * value)
	{
		___communication_2 = value;
		Il2CppCodeGenWriteBarrier((&___communication_2), value);
	}

	inline static int32_t get_offset_of__currentCheckListItemFacade_3() { return static_cast<int32_t>(offsetof(NewRemarkHandlerMobile_t798103230, ____currentCheckListItemFacade_3)); }
	inline RuntimeObject* get__currentCheckListItemFacade_3() const { return ____currentCheckListItemFacade_3; }
	inline RuntimeObject** get_address_of__currentCheckListItemFacade_3() { return &____currentCheckListItemFacade_3; }
	inline void set__currentCheckListItemFacade_3(RuntimeObject* value)
	{
		____currentCheckListItemFacade_3 = value;
		Il2CppCodeGenWriteBarrier((&____currentCheckListItemFacade_3), value);
	}

	inline static int32_t get_offset_of_currentStatusScene_4() { return static_cast<int32_t>(offsetof(NewRemarkHandlerMobile_t798103230, ___currentStatusScene_4)); }
	inline GameObject_t1756533147 * get_currentStatusScene_4() const { return ___currentStatusScene_4; }
	inline GameObject_t1756533147 ** get_address_of_currentStatusScene_4() { return &___currentStatusScene_4; }
	inline void set_currentStatusScene_4(GameObject_t1756533147 * value)
	{
		___currentStatusScene_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentStatusScene_4), value);
	}

	inline static int32_t get_offset_of_currentItemButton_5() { return static_cast<int32_t>(offsetof(NewRemarkHandlerMobile_t798103230, ___currentItemButton_5)); }
	inline Button_t2872111280 * get_currentItemButton_5() const { return ___currentItemButton_5; }
	inline Button_t2872111280 ** get_address_of_currentItemButton_5() { return &___currentItemButton_5; }
	inline void set_currentItemButton_5(Button_t2872111280 * value)
	{
		___currentItemButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___currentItemButton_5), value);
	}

	inline static int32_t get_offset_of_forcedItemStauts_6() { return static_cast<int32_t>(offsetof(NewRemarkHandlerMobile_t798103230, ___forcedItemStauts_6)); }
	inline ItemStatus_t1378751697 * get_forcedItemStauts_6() const { return ___forcedItemStauts_6; }
	inline ItemStatus_t1378751697 ** get_address_of_forcedItemStauts_6() { return &___forcedItemStauts_6; }
	inline void set_forcedItemStauts_6(ItemStatus_t1378751697 * value)
	{
		___forcedItemStauts_6 = value;
		Il2CppCodeGenWriteBarrier((&___forcedItemStauts_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWREMARKHANDLERMOBILE_T798103230_H
#ifndef REMARKHANDLER_T1175353018_H
#define REMARKHANDLER_T1175353018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.Remark.RemarkHandler
struct  RemarkHandler_t1175353018  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct RemarkHandler_t1175353018_StaticFields
{
public:
	// UnityEngine.Events.UnityAction Library.Code.UI.Remark.RemarkHandler::<>f__am$cache0
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache0_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(RemarkHandler_t1175353018_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMARKHANDLER_T1175353018_H
#ifndef BENCHMARK01_T2768175604_H
#define BENCHMARK01_T2768175604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01
struct  Benchmark01_t2768175604  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 TMPro.Examples.Benchmark01::BenchmarkType
	int32_t ___BenchmarkType_2;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01::TMProFont
	TMP_FontAsset_t2530419979 * ___TMProFont_3;
	// UnityEngine.Font TMPro.Examples.Benchmark01::TextMeshFont
	Font_t4239498691 * ___TextMeshFont_4;
	// TMPro.TextMeshPro TMPro.Examples.Benchmark01::m_textMeshPro
	TextMeshPro_t2521834357 * ___m_textMeshPro_5;
	// TMPro.TextContainer TMPro.Examples.Benchmark01::m_textContainer
	TextContainer_t4263764796 * ___m_textContainer_6;
	// UnityEngine.TextMesh TMPro.Examples.Benchmark01::m_textMesh
	TextMesh_t1641806576 * ___m_textMesh_7;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material01
	Material_t193706927 * ___m_material01_10;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material02
	Material_t193706927 * ___m_material02_11;

public:
	inline static int32_t get_offset_of_BenchmarkType_2() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___BenchmarkType_2)); }
	inline int32_t get_BenchmarkType_2() const { return ___BenchmarkType_2; }
	inline int32_t* get_address_of_BenchmarkType_2() { return &___BenchmarkType_2; }
	inline void set_BenchmarkType_2(int32_t value)
	{
		___BenchmarkType_2 = value;
	}

	inline static int32_t get_offset_of_TMProFont_3() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___TMProFont_3)); }
	inline TMP_FontAsset_t2530419979 * get_TMProFont_3() const { return ___TMProFont_3; }
	inline TMP_FontAsset_t2530419979 ** get_address_of_TMProFont_3() { return &___TMProFont_3; }
	inline void set_TMProFont_3(TMP_FontAsset_t2530419979 * value)
	{
		___TMProFont_3 = value;
		Il2CppCodeGenWriteBarrier((&___TMProFont_3), value);
	}

	inline static int32_t get_offset_of_TextMeshFont_4() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___TextMeshFont_4)); }
	inline Font_t4239498691 * get_TextMeshFont_4() const { return ___TextMeshFont_4; }
	inline Font_t4239498691 ** get_address_of_TextMeshFont_4() { return &___TextMeshFont_4; }
	inline void set_TextMeshFont_4(Font_t4239498691 * value)
	{
		___TextMeshFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshFont_4), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_5() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___m_textMeshPro_5)); }
	inline TextMeshPro_t2521834357 * get_m_textMeshPro_5() const { return ___m_textMeshPro_5; }
	inline TextMeshPro_t2521834357 ** get_address_of_m_textMeshPro_5() { return &___m_textMeshPro_5; }
	inline void set_m_textMeshPro_5(TextMeshPro_t2521834357 * value)
	{
		___m_textMeshPro_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_5), value);
	}

	inline static int32_t get_offset_of_m_textContainer_6() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___m_textContainer_6)); }
	inline TextContainer_t4263764796 * get_m_textContainer_6() const { return ___m_textContainer_6; }
	inline TextContainer_t4263764796 ** get_address_of_m_textContainer_6() { return &___m_textContainer_6; }
	inline void set_m_textContainer_6(TextContainer_t4263764796 * value)
	{
		___m_textContainer_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainer_6), value);
	}

	inline static int32_t get_offset_of_m_textMesh_7() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___m_textMesh_7)); }
	inline TextMesh_t1641806576 * get_m_textMesh_7() const { return ___m_textMesh_7; }
	inline TextMesh_t1641806576 ** get_address_of_m_textMesh_7() { return &___m_textMesh_7; }
	inline void set_m_textMesh_7(TextMesh_t1641806576 * value)
	{
		___m_textMesh_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_7), value);
	}

	inline static int32_t get_offset_of_m_material01_10() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___m_material01_10)); }
	inline Material_t193706927 * get_m_material01_10() const { return ___m_material01_10; }
	inline Material_t193706927 ** get_address_of_m_material01_10() { return &___m_material01_10; }
	inline void set_m_material01_10(Material_t193706927 * value)
	{
		___m_material01_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_material01_10), value);
	}

	inline static int32_t get_offset_of_m_material02_11() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___m_material02_11)); }
	inline Material_t193706927 * get_m_material02_11() const { return ___m_material02_11; }
	inline Material_t193706927 ** get_address_of_m_material02_11() { return &___m_material02_11; }
	inline void set_m_material02_11(Material_t193706927 * value)
	{
		___m_material02_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_material02_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK01_T2768175604_H
#ifndef MESSAGEPROVIDER_T618924372_H
#define MESSAGEPROVIDER_T618924372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.Message.MessageProvider
struct  MessageProvider_t618924372  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.DI.Message.MessageFacadeModule Library.Code.DI.Message.MessageProvider::_messageFacadeModule
	MessageFacadeModule_t1147318505 * ____messageFacadeModule_2;
	// Library.Code.DI.Message.SendMessageModule Library.Code.DI.Message.MessageProvider::_sendMessageModule
	SendMessageModule_t690017393 * ____sendMessageModule_3;
	// Library.Code.DI.SendFile.SendFileModule Library.Code.DI.Message.MessageProvider::_sendFileModule
	SendFileModule_t3843131166 * ____sendFileModule_4;

public:
	inline static int32_t get_offset_of__messageFacadeModule_2() { return static_cast<int32_t>(offsetof(MessageProvider_t618924372, ____messageFacadeModule_2)); }
	inline MessageFacadeModule_t1147318505 * get__messageFacadeModule_2() const { return ____messageFacadeModule_2; }
	inline MessageFacadeModule_t1147318505 ** get_address_of__messageFacadeModule_2() { return &____messageFacadeModule_2; }
	inline void set__messageFacadeModule_2(MessageFacadeModule_t1147318505 * value)
	{
		____messageFacadeModule_2 = value;
		Il2CppCodeGenWriteBarrier((&____messageFacadeModule_2), value);
	}

	inline static int32_t get_offset_of__sendMessageModule_3() { return static_cast<int32_t>(offsetof(MessageProvider_t618924372, ____sendMessageModule_3)); }
	inline SendMessageModule_t690017393 * get__sendMessageModule_3() const { return ____sendMessageModule_3; }
	inline SendMessageModule_t690017393 ** get_address_of__sendMessageModule_3() { return &____sendMessageModule_3; }
	inline void set__sendMessageModule_3(SendMessageModule_t690017393 * value)
	{
		____sendMessageModule_3 = value;
		Il2CppCodeGenWriteBarrier((&____sendMessageModule_3), value);
	}

	inline static int32_t get_offset_of__sendFileModule_4() { return static_cast<int32_t>(offsetof(MessageProvider_t618924372, ____sendFileModule_4)); }
	inline SendFileModule_t3843131166 * get__sendFileModule_4() const { return ____sendFileModule_4; }
	inline SendFileModule_t3843131166 ** get_address_of__sendFileModule_4() { return &____sendFileModule_4; }
	inline void set__sendFileModule_4(SendFileModule_t3843131166 * value)
	{
		____sendFileModule_4 = value;
		Il2CppCodeGenWriteBarrier((&____sendFileModule_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEPROVIDER_T618924372_H
#ifndef PROVIDERWITHAPIFACTORY_1_T2855966632_H
#define PROVIDERWITHAPIFACTORY_1_T2855966632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.ProviderWithApiFactory`1<Library.Code.Networking.Models.ModelApi>
struct  ProviderWithApiFactory_1_t2855966632  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERWITHAPIFACTORY_1_T2855966632_H
#ifndef PROVIDERWITHAPIFACTORY_1_T2940439303_H
#define PROVIDERWITHAPIFACTORY_1_T2940439303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.ProviderWithApiFactory`1<Library.Code.Networking.MessageTemplate.MessageTemplatesApi>
struct  ProviderWithApiFactory_1_t2940439303  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERWITHAPIFACTORY_1_T2940439303_H
#ifndef PROVIDERWITHAPIFACTORY_1_T1428020601_H
#define PROVIDERWITHAPIFACTORY_1_T1428020601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.ProviderWithApiFactory`1<Library.Code.Networking.GroupModel.GroupingModelApi>
struct  ProviderWithApiFactory_1_t1428020601  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERWITHAPIFACTORY_1_T1428020601_H
#ifndef PROVIDERWITHAPIFACTORY_1_T4144576901_H
#define PROVIDERWITHAPIFACTORY_1_T4144576901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.ProviderWithApiFactory`1<Library.Code.Networking.Elements.ElementApi>
struct  ProviderWithApiFactory_1_t4144576901  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERWITHAPIFACTORY_1_T4144576901_H
#ifndef PROVIDERWITHAPIFACTORY_1_T1501685989_H
#define PROVIDERWITHAPIFACTORY_1_T1501685989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.ProviderWithApiFactory`1<Library.Code.Networking.Elements.ElementStatusApi>
struct  ProviderWithApiFactory_1_t1501685989  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERWITHAPIFACTORY_1_T1501685989_H
#ifndef PROVIDERWITHAPIFACTORY_1_T992121606_H
#define PROVIDERWITHAPIFACTORY_1_T992121606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.ProviderWithApiFactory`1<Library.Code.Networking.Checklist.LoadChecklistForElementApi>
struct  ProviderWithApiFactory_1_t992121606  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERWITHAPIFACTORY_1_T992121606_H
#ifndef EXAMPLESCENEMANAGERCONTROLLER_T4160392891_H
#define EXAMPLESCENEMANAGERCONTROLLER_T4160392891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleSceneManagerController
struct  ExampleSceneManagerController_t4160392891  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text ExampleSceneManagerController::LeftText
	Text_t356221433 * ___LeftText_2;
	// UnityEngine.UI.Text ExampleSceneManagerController::RightText
	Text_t356221433 * ___RightText_3;
	// UnityEngine.UI.Slider ExampleSceneManagerController::ProgressSlider
	Slider_t297367283 * ___ProgressSlider_4;

public:
	inline static int32_t get_offset_of_LeftText_2() { return static_cast<int32_t>(offsetof(ExampleSceneManagerController_t4160392891, ___LeftText_2)); }
	inline Text_t356221433 * get_LeftText_2() const { return ___LeftText_2; }
	inline Text_t356221433 ** get_address_of_LeftText_2() { return &___LeftText_2; }
	inline void set_LeftText_2(Text_t356221433 * value)
	{
		___LeftText_2 = value;
		Il2CppCodeGenWriteBarrier((&___LeftText_2), value);
	}

	inline static int32_t get_offset_of_RightText_3() { return static_cast<int32_t>(offsetof(ExampleSceneManagerController_t4160392891, ___RightText_3)); }
	inline Text_t356221433 * get_RightText_3() const { return ___RightText_3; }
	inline Text_t356221433 ** get_address_of_RightText_3() { return &___RightText_3; }
	inline void set_RightText_3(Text_t356221433 * value)
	{
		___RightText_3 = value;
		Il2CppCodeGenWriteBarrier((&___RightText_3), value);
	}

	inline static int32_t get_offset_of_ProgressSlider_4() { return static_cast<int32_t>(offsetof(ExampleSceneManagerController_t4160392891, ___ProgressSlider_4)); }
	inline Slider_t297367283 * get_ProgressSlider_4() const { return ___ProgressSlider_4; }
	inline Slider_t297367283 ** get_address_of_ProgressSlider_4() { return &___ProgressSlider_4; }
	inline void set_ProgressSlider_4(Slider_t297367283 * value)
	{
		___ProgressSlider_4 = value;
		Il2CppCodeGenWriteBarrier((&___ProgressSlider_4), value);
	}
};

struct ExampleSceneManagerController_t4160392891_StaticFields
{
public:
	// System.Action`1<CI.HttpClient.HttpResponseMessage`1<System.Byte[]>> ExampleSceneManagerController::<>f__am$cache0
	Action_1_t2548510215 * ___U3CU3Ef__amU24cache0_5;
	// System.Action`1<CI.HttpClient.HttpResponseMessage`1<System.Byte[]>> ExampleSceneManagerController::<>f__am$cache1
	Action_1_t2548510215 * ___U3CU3Ef__amU24cache1_6;
	// System.Action`1<CI.HttpClient.HttpResponseMessage`1<System.Byte[]>> ExampleSceneManagerController::<>f__am$cache2
	Action_1_t2548510215 * ___U3CU3Ef__amU24cache2_7;
	// System.Action`1<CI.HttpClient.HttpResponseMessage`1<System.Byte[]>> ExampleSceneManagerController::<>f__am$cache3
	Action_1_t2548510215 * ___U3CU3Ef__amU24cache3_8;
	// System.Action`1<CI.HttpClient.HttpResponseMessage`1<System.Byte[]>> ExampleSceneManagerController::<>f__am$cache4
	Action_1_t2548510215 * ___U3CU3Ef__amU24cache4_9;
	// System.Action`1<CI.HttpClient.HttpResponseMessage`1<System.Byte[]>> ExampleSceneManagerController::<>f__am$cache5
	Action_1_t2548510215 * ___U3CU3Ef__amU24cache5_10;
	// System.Action`1<CI.HttpClient.HttpResponseMessage`1<System.Byte[]>> ExampleSceneManagerController::<>f__am$cache6
	Action_1_t2548510215 * ___U3CU3Ef__amU24cache6_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(ExampleSceneManagerController_t4160392891_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Action_1_t2548510215 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Action_1_t2548510215 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Action_1_t2548510215 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_6() { return static_cast<int32_t>(offsetof(ExampleSceneManagerController_t4160392891_StaticFields, ___U3CU3Ef__amU24cache1_6)); }
	inline Action_1_t2548510215 * get_U3CU3Ef__amU24cache1_6() const { return ___U3CU3Ef__amU24cache1_6; }
	inline Action_1_t2548510215 ** get_address_of_U3CU3Ef__amU24cache1_6() { return &___U3CU3Ef__amU24cache1_6; }
	inline void set_U3CU3Ef__amU24cache1_6(Action_1_t2548510215 * value)
	{
		___U3CU3Ef__amU24cache1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_7() { return static_cast<int32_t>(offsetof(ExampleSceneManagerController_t4160392891_StaticFields, ___U3CU3Ef__amU24cache2_7)); }
	inline Action_1_t2548510215 * get_U3CU3Ef__amU24cache2_7() const { return ___U3CU3Ef__amU24cache2_7; }
	inline Action_1_t2548510215 ** get_address_of_U3CU3Ef__amU24cache2_7() { return &___U3CU3Ef__amU24cache2_7; }
	inline void set_U3CU3Ef__amU24cache2_7(Action_1_t2548510215 * value)
	{
		___U3CU3Ef__amU24cache2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_8() { return static_cast<int32_t>(offsetof(ExampleSceneManagerController_t4160392891_StaticFields, ___U3CU3Ef__amU24cache3_8)); }
	inline Action_1_t2548510215 * get_U3CU3Ef__amU24cache3_8() const { return ___U3CU3Ef__amU24cache3_8; }
	inline Action_1_t2548510215 ** get_address_of_U3CU3Ef__amU24cache3_8() { return &___U3CU3Ef__amU24cache3_8; }
	inline void set_U3CU3Ef__amU24cache3_8(Action_1_t2548510215 * value)
	{
		___U3CU3Ef__amU24cache3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_9() { return static_cast<int32_t>(offsetof(ExampleSceneManagerController_t4160392891_StaticFields, ___U3CU3Ef__amU24cache4_9)); }
	inline Action_1_t2548510215 * get_U3CU3Ef__amU24cache4_9() const { return ___U3CU3Ef__amU24cache4_9; }
	inline Action_1_t2548510215 ** get_address_of_U3CU3Ef__amU24cache4_9() { return &___U3CU3Ef__amU24cache4_9; }
	inline void set_U3CU3Ef__amU24cache4_9(Action_1_t2548510215 * value)
	{
		___U3CU3Ef__amU24cache4_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_10() { return static_cast<int32_t>(offsetof(ExampleSceneManagerController_t4160392891_StaticFields, ___U3CU3Ef__amU24cache5_10)); }
	inline Action_1_t2548510215 * get_U3CU3Ef__amU24cache5_10() const { return ___U3CU3Ef__amU24cache5_10; }
	inline Action_1_t2548510215 ** get_address_of_U3CU3Ef__amU24cache5_10() { return &___U3CU3Ef__amU24cache5_10; }
	inline void set_U3CU3Ef__amU24cache5_10(Action_1_t2548510215 * value)
	{
		___U3CU3Ef__amU24cache5_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_11() { return static_cast<int32_t>(offsetof(ExampleSceneManagerController_t4160392891_StaticFields, ___U3CU3Ef__amU24cache6_11)); }
	inline Action_1_t2548510215 * get_U3CU3Ef__amU24cache6_11() const { return ___U3CU3Ef__amU24cache6_11; }
	inline Action_1_t2548510215 ** get_address_of_U3CU3Ef__amU24cache6_11() { return &___U3CU3Ef__amU24cache6_11; }
	inline void set_U3CU3Ef__amU24cache6_11(Action_1_t2548510215 * value)
	{
		___U3CU3Ef__amU24cache6_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache6_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLESCENEMANAGERCONTROLLER_T4160392891_H
#ifndef GROUPINCOLORNEW_T4212717807_H
#define GROUPINCOLORNEW_T4212717807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.UI.GroupinColorModel.GroupinColorNew
struct  GroupinColorNew_t4212717807  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Facades.ModelGrouping.ModelGroupFacade Library.UI.GroupinColorModel.GroupinColorNew::_modelGroupFacade
	RuntimeObject* ____modelGroupFacade_2;
	// Library.Code.UI.PrefabGenerator.PrefabGenerator Library.UI.GroupinColorModel.GroupinColorNew::_prefabGenerator
	RuntimeObject* ____prefabGenerator_3;
	// System.Boolean Library.UI.GroupinColorModel.GroupinColorNew::started
	bool ___started_4;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> Library.UI.GroupinColorModel.GroupinColorNew::existingItems
	List_1_t2719087314 * ___existingItems_5;
	// System.Single Library.UI.GroupinColorModel.GroupinColorNew::topMargin
	float ___topMargin_6;

public:
	inline static int32_t get_offset_of__modelGroupFacade_2() { return static_cast<int32_t>(offsetof(GroupinColorNew_t4212717807, ____modelGroupFacade_2)); }
	inline RuntimeObject* get__modelGroupFacade_2() const { return ____modelGroupFacade_2; }
	inline RuntimeObject** get_address_of__modelGroupFacade_2() { return &____modelGroupFacade_2; }
	inline void set__modelGroupFacade_2(RuntimeObject* value)
	{
		____modelGroupFacade_2 = value;
		Il2CppCodeGenWriteBarrier((&____modelGroupFacade_2), value);
	}

	inline static int32_t get_offset_of__prefabGenerator_3() { return static_cast<int32_t>(offsetof(GroupinColorNew_t4212717807, ____prefabGenerator_3)); }
	inline RuntimeObject* get__prefabGenerator_3() const { return ____prefabGenerator_3; }
	inline RuntimeObject** get_address_of__prefabGenerator_3() { return &____prefabGenerator_3; }
	inline void set__prefabGenerator_3(RuntimeObject* value)
	{
		____prefabGenerator_3 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_3), value);
	}

	inline static int32_t get_offset_of_started_4() { return static_cast<int32_t>(offsetof(GroupinColorNew_t4212717807, ___started_4)); }
	inline bool get_started_4() const { return ___started_4; }
	inline bool* get_address_of_started_4() { return &___started_4; }
	inline void set_started_4(bool value)
	{
		___started_4 = value;
	}

	inline static int32_t get_offset_of_existingItems_5() { return static_cast<int32_t>(offsetof(GroupinColorNew_t4212717807, ___existingItems_5)); }
	inline List_1_t2719087314 * get_existingItems_5() const { return ___existingItems_5; }
	inline List_1_t2719087314 ** get_address_of_existingItems_5() { return &___existingItems_5; }
	inline void set_existingItems_5(List_1_t2719087314 * value)
	{
		___existingItems_5 = value;
		Il2CppCodeGenWriteBarrier((&___existingItems_5), value);
	}

	inline static int32_t get_offset_of_topMargin_6() { return static_cast<int32_t>(offsetof(GroupinColorNew_t4212717807, ___topMargin_6)); }
	inline float get_topMargin_6() const { return ___topMargin_6; }
	inline float* get_address_of_topMargin_6() { return &___topMargin_6; }
	inline void set_topMargin_6(float value)
	{
		___topMargin_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPINCOLORNEW_T4212717807_H
#ifndef GROUPINCOLOR_T2175596517_H
#define GROUPINCOLOR_T2175596517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.UI.GroupinColorModel.GroupinColor
struct  GroupinColor_t2175596517  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Facades.ModelGrouping.ModelGroupFacade Library.UI.GroupinColorModel.GroupinColor::_modelGroupFacade
	RuntimeObject* ____modelGroupFacade_2;
	// Library.Code.UI.PrefabGenerator.PrefabGenerator Library.UI.GroupinColorModel.GroupinColor::_prefabGenerator
	RuntimeObject* ____prefabGenerator_3;
	// System.Boolean Library.UI.GroupinColorModel.GroupinColor::started
	bool ___started_4;

public:
	inline static int32_t get_offset_of__modelGroupFacade_2() { return static_cast<int32_t>(offsetof(GroupinColor_t2175596517, ____modelGroupFacade_2)); }
	inline RuntimeObject* get__modelGroupFacade_2() const { return ____modelGroupFacade_2; }
	inline RuntimeObject** get_address_of__modelGroupFacade_2() { return &____modelGroupFacade_2; }
	inline void set__modelGroupFacade_2(RuntimeObject* value)
	{
		____modelGroupFacade_2 = value;
		Il2CppCodeGenWriteBarrier((&____modelGroupFacade_2), value);
	}

	inline static int32_t get_offset_of__prefabGenerator_3() { return static_cast<int32_t>(offsetof(GroupinColor_t2175596517, ____prefabGenerator_3)); }
	inline RuntimeObject* get__prefabGenerator_3() const { return ____prefabGenerator_3; }
	inline RuntimeObject** get_address_of__prefabGenerator_3() { return &____prefabGenerator_3; }
	inline void set__prefabGenerator_3(RuntimeObject* value)
	{
		____prefabGenerator_3 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_3), value);
	}

	inline static int32_t get_offset_of_started_4() { return static_cast<int32_t>(offsetof(GroupinColor_t2175596517, ___started_4)); }
	inline bool get_started_4() const { return ___started_4; }
	inline bool* get_address_of_started_4() { return &___started_4; }
	inline void set_started_4(bool value)
	{
		___started_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPINCOLOR_T2175596517_H
#ifndef PREFABPROVIDER_T1422780085_H
#define PREFABPROVIDER_T1422780085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.Prefab.PrefabProvider
struct  PrefabProvider_t1422780085  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.UI.PrefabGenerator.PrefabGenerator Library.Code.DI.Prefab.PrefabProvider::_prefabGenerator
	RuntimeObject* ____prefabGenerator_2;

public:
	inline static int32_t get_offset_of__prefabGenerator_2() { return static_cast<int32_t>(offsetof(PrefabProvider_t1422780085, ____prefabGenerator_2)); }
	inline RuntimeObject* get__prefabGenerator_2() const { return ____prefabGenerator_2; }
	inline RuntimeObject** get_address_of__prefabGenerator_2() { return &____prefabGenerator_2; }
	inline void set__prefabGenerator_2(RuntimeObject* value)
	{
		____prefabGenerator_2 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABPROVIDER_T1422780085_H
#ifndef REMARKHANDLERMOBILE_T1067069532_H
#define REMARKHANDLERMOBILE_T1067069532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.Remark.RemarkHandlerMobile
struct  RemarkHandlerMobile_t1067069532  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Communication.Communication Library.Code.UI.Remark.RemarkHandlerMobile::communication
	Communication_t424466906 * ___communication_2;
	// Library.Code.Facades.CheckListF.CurrentCheckListItemFacade Library.Code.UI.Remark.RemarkHandlerMobile::_currentCheckListItemFacade
	RuntimeObject* ____currentCheckListItemFacade_3;
	// UnityEngine.GameObject Library.Code.UI.Remark.RemarkHandlerMobile::currentStatusScene
	GameObject_t1756533147 * ___currentStatusScene_4;
	// UnityEngine.UI.Button Library.Code.UI.Remark.RemarkHandlerMobile::currentItemButton
	Button_t2872111280 * ___currentItemButton_5;

public:
	inline static int32_t get_offset_of_communication_2() { return static_cast<int32_t>(offsetof(RemarkHandlerMobile_t1067069532, ___communication_2)); }
	inline Communication_t424466906 * get_communication_2() const { return ___communication_2; }
	inline Communication_t424466906 ** get_address_of_communication_2() { return &___communication_2; }
	inline void set_communication_2(Communication_t424466906 * value)
	{
		___communication_2 = value;
		Il2CppCodeGenWriteBarrier((&___communication_2), value);
	}

	inline static int32_t get_offset_of__currentCheckListItemFacade_3() { return static_cast<int32_t>(offsetof(RemarkHandlerMobile_t1067069532, ____currentCheckListItemFacade_3)); }
	inline RuntimeObject* get__currentCheckListItemFacade_3() const { return ____currentCheckListItemFacade_3; }
	inline RuntimeObject** get_address_of__currentCheckListItemFacade_3() { return &____currentCheckListItemFacade_3; }
	inline void set__currentCheckListItemFacade_3(RuntimeObject* value)
	{
		____currentCheckListItemFacade_3 = value;
		Il2CppCodeGenWriteBarrier((&____currentCheckListItemFacade_3), value);
	}

	inline static int32_t get_offset_of_currentStatusScene_4() { return static_cast<int32_t>(offsetof(RemarkHandlerMobile_t1067069532, ___currentStatusScene_4)); }
	inline GameObject_t1756533147 * get_currentStatusScene_4() const { return ___currentStatusScene_4; }
	inline GameObject_t1756533147 ** get_address_of_currentStatusScene_4() { return &___currentStatusScene_4; }
	inline void set_currentStatusScene_4(GameObject_t1756533147 * value)
	{
		___currentStatusScene_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentStatusScene_4), value);
	}

	inline static int32_t get_offset_of_currentItemButton_5() { return static_cast<int32_t>(offsetof(RemarkHandlerMobile_t1067069532, ___currentItemButton_5)); }
	inline Button_t2872111280 * get_currentItemButton_5() const { return ___currentItemButton_5; }
	inline Button_t2872111280 ** get_address_of_currentItemButton_5() { return &___currentItemButton_5; }
	inline void set_currentItemButton_5(Button_t2872111280 * value)
	{
		___currentItemButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___currentItemButton_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMARKHANDLERMOBILE_T1067069532_H
#ifndef REMARKHANDLERMOBILETWOPANELS_T1827359049_H
#define REMARKHANDLERMOBILETWOPANELS_T1827359049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.Remark.RemarkHandlerMobileTwoPanels
struct  RemarkHandlerMobileTwoPanels_t1827359049  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Communication.Communication Library.Code.UI.Remark.RemarkHandlerMobileTwoPanels::communication
	Communication_t424466906 * ___communication_2;
	// Library.Code.Facades.CheckListF.CurrentCheckListItemFacade Library.Code.UI.Remark.RemarkHandlerMobileTwoPanels::_currentCheckListItemFacade
	RuntimeObject* ____currentCheckListItemFacade_3;
	// UnityEngine.GameObject Library.Code.UI.Remark.RemarkHandlerMobileTwoPanels::currentStatusScene
	GameObject_t1756533147 * ___currentStatusScene_4;
	// UnityEngine.UI.Button Library.Code.UI.Remark.RemarkHandlerMobileTwoPanels::currentItemButton
	Button_t2872111280 * ___currentItemButton_5;
	// Library.Code.Domain.Entities.Checklist.ItemStatus Library.Code.UI.Remark.RemarkHandlerMobileTwoPanels::forcedItemStauts
	ItemStatus_t1378751697 * ___forcedItemStauts_6;

public:
	inline static int32_t get_offset_of_communication_2() { return static_cast<int32_t>(offsetof(RemarkHandlerMobileTwoPanels_t1827359049, ___communication_2)); }
	inline Communication_t424466906 * get_communication_2() const { return ___communication_2; }
	inline Communication_t424466906 ** get_address_of_communication_2() { return &___communication_2; }
	inline void set_communication_2(Communication_t424466906 * value)
	{
		___communication_2 = value;
		Il2CppCodeGenWriteBarrier((&___communication_2), value);
	}

	inline static int32_t get_offset_of__currentCheckListItemFacade_3() { return static_cast<int32_t>(offsetof(RemarkHandlerMobileTwoPanels_t1827359049, ____currentCheckListItemFacade_3)); }
	inline RuntimeObject* get__currentCheckListItemFacade_3() const { return ____currentCheckListItemFacade_3; }
	inline RuntimeObject** get_address_of__currentCheckListItemFacade_3() { return &____currentCheckListItemFacade_3; }
	inline void set__currentCheckListItemFacade_3(RuntimeObject* value)
	{
		____currentCheckListItemFacade_3 = value;
		Il2CppCodeGenWriteBarrier((&____currentCheckListItemFacade_3), value);
	}

	inline static int32_t get_offset_of_currentStatusScene_4() { return static_cast<int32_t>(offsetof(RemarkHandlerMobileTwoPanels_t1827359049, ___currentStatusScene_4)); }
	inline GameObject_t1756533147 * get_currentStatusScene_4() const { return ___currentStatusScene_4; }
	inline GameObject_t1756533147 ** get_address_of_currentStatusScene_4() { return &___currentStatusScene_4; }
	inline void set_currentStatusScene_4(GameObject_t1756533147 * value)
	{
		___currentStatusScene_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentStatusScene_4), value);
	}

	inline static int32_t get_offset_of_currentItemButton_5() { return static_cast<int32_t>(offsetof(RemarkHandlerMobileTwoPanels_t1827359049, ___currentItemButton_5)); }
	inline Button_t2872111280 * get_currentItemButton_5() const { return ___currentItemButton_5; }
	inline Button_t2872111280 ** get_address_of_currentItemButton_5() { return &___currentItemButton_5; }
	inline void set_currentItemButton_5(Button_t2872111280 * value)
	{
		___currentItemButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___currentItemButton_5), value);
	}

	inline static int32_t get_offset_of_forcedItemStauts_6() { return static_cast<int32_t>(offsetof(RemarkHandlerMobileTwoPanels_t1827359049, ___forcedItemStauts_6)); }
	inline ItemStatus_t1378751697 * get_forcedItemStauts_6() const { return ___forcedItemStauts_6; }
	inline ItemStatus_t1378751697 ** get_address_of_forcedItemStauts_6() { return &___forcedItemStauts_6; }
	inline void set_forcedItemStauts_6(ItemStatus_t1378751697 * value)
	{
		___forcedItemStauts_6 = value;
		Il2CppCodeGenWriteBarrier((&___forcedItemStauts_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMARKHANDLERMOBILETWOPANELS_T1827359049_H
#ifndef SAVEREMARK_T4091223213_H
#define SAVEREMARK_T4091223213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.Remark.SaveRemark
struct  SaveRemark_t4091223213  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Facades.Messages.MessagesFacade Library.Code.UI.Remark.SaveRemark::_messagesFacade
	RuntimeObject* ____messagesFacade_2;
	// Library.Code.Facades.Camera.CameraFacade Library.Code.UI.Remark.SaveRemark::_cameraPositionProvider
	RuntimeObject* ____cameraPositionProvider_3;
	// UnityEngine.UI.InputField Library.Code.UI.Remark.SaveRemark::_inputField
	InputField_t1631627530 * ____inputField_4;
	// Library.Code.Facades._3dElementDownload.Element3dFacade Library.Code.UI.Remark.SaveRemark::_element3DFacade
	RuntimeObject* ____element3DFacade_5;
	// Library.Code.Domain.Entities.Element Library.Code.UI.Remark.SaveRemark::_currentElement
	Element_t2276588008 * ____currentElement_6;
	// Library.Code.Facades.CheckListF.CurrentCheckListItemFacade Library.Code.UI.Remark.SaveRemark::_currentCheckListItemFacade
	RuntimeObject* ____currentCheckListItemFacade_7;
	// Library.Code.Facades.PathHolder.PathHolder Library.Code.UI.Remark.SaveRemark::_pathHolder
	RuntimeObject* ____pathHolder_8;
	// System.Boolean Library.Code.UI.Remark.SaveRemark::started
	bool ___started_9;
	// UnityEngine.Events.UnityAction Library.Code.UI.Remark.SaveRemark::onSaveAction
	UnityAction_t4025899511 * ___onSaveAction_10;
	// Library.Code.Domain.Entities.Checklist.ItemStatus Library.Code.UI.Remark.SaveRemark::currentItemStatus
	ItemStatus_t1378751697 * ___currentItemStatus_11;

public:
	inline static int32_t get_offset_of__messagesFacade_2() { return static_cast<int32_t>(offsetof(SaveRemark_t4091223213, ____messagesFacade_2)); }
	inline RuntimeObject* get__messagesFacade_2() const { return ____messagesFacade_2; }
	inline RuntimeObject** get_address_of__messagesFacade_2() { return &____messagesFacade_2; }
	inline void set__messagesFacade_2(RuntimeObject* value)
	{
		____messagesFacade_2 = value;
		Il2CppCodeGenWriteBarrier((&____messagesFacade_2), value);
	}

	inline static int32_t get_offset_of__cameraPositionProvider_3() { return static_cast<int32_t>(offsetof(SaveRemark_t4091223213, ____cameraPositionProvider_3)); }
	inline RuntimeObject* get__cameraPositionProvider_3() const { return ____cameraPositionProvider_3; }
	inline RuntimeObject** get_address_of__cameraPositionProvider_3() { return &____cameraPositionProvider_3; }
	inline void set__cameraPositionProvider_3(RuntimeObject* value)
	{
		____cameraPositionProvider_3 = value;
		Il2CppCodeGenWriteBarrier((&____cameraPositionProvider_3), value);
	}

	inline static int32_t get_offset_of__inputField_4() { return static_cast<int32_t>(offsetof(SaveRemark_t4091223213, ____inputField_4)); }
	inline InputField_t1631627530 * get__inputField_4() const { return ____inputField_4; }
	inline InputField_t1631627530 ** get_address_of__inputField_4() { return &____inputField_4; }
	inline void set__inputField_4(InputField_t1631627530 * value)
	{
		____inputField_4 = value;
		Il2CppCodeGenWriteBarrier((&____inputField_4), value);
	}

	inline static int32_t get_offset_of__element3DFacade_5() { return static_cast<int32_t>(offsetof(SaveRemark_t4091223213, ____element3DFacade_5)); }
	inline RuntimeObject* get__element3DFacade_5() const { return ____element3DFacade_5; }
	inline RuntimeObject** get_address_of__element3DFacade_5() { return &____element3DFacade_5; }
	inline void set__element3DFacade_5(RuntimeObject* value)
	{
		____element3DFacade_5 = value;
		Il2CppCodeGenWriteBarrier((&____element3DFacade_5), value);
	}

	inline static int32_t get_offset_of__currentElement_6() { return static_cast<int32_t>(offsetof(SaveRemark_t4091223213, ____currentElement_6)); }
	inline Element_t2276588008 * get__currentElement_6() const { return ____currentElement_6; }
	inline Element_t2276588008 ** get_address_of__currentElement_6() { return &____currentElement_6; }
	inline void set__currentElement_6(Element_t2276588008 * value)
	{
		____currentElement_6 = value;
		Il2CppCodeGenWriteBarrier((&____currentElement_6), value);
	}

	inline static int32_t get_offset_of__currentCheckListItemFacade_7() { return static_cast<int32_t>(offsetof(SaveRemark_t4091223213, ____currentCheckListItemFacade_7)); }
	inline RuntimeObject* get__currentCheckListItemFacade_7() const { return ____currentCheckListItemFacade_7; }
	inline RuntimeObject** get_address_of__currentCheckListItemFacade_7() { return &____currentCheckListItemFacade_7; }
	inline void set__currentCheckListItemFacade_7(RuntimeObject* value)
	{
		____currentCheckListItemFacade_7 = value;
		Il2CppCodeGenWriteBarrier((&____currentCheckListItemFacade_7), value);
	}

	inline static int32_t get_offset_of__pathHolder_8() { return static_cast<int32_t>(offsetof(SaveRemark_t4091223213, ____pathHolder_8)); }
	inline RuntimeObject* get__pathHolder_8() const { return ____pathHolder_8; }
	inline RuntimeObject** get_address_of__pathHolder_8() { return &____pathHolder_8; }
	inline void set__pathHolder_8(RuntimeObject* value)
	{
		____pathHolder_8 = value;
		Il2CppCodeGenWriteBarrier((&____pathHolder_8), value);
	}

	inline static int32_t get_offset_of_started_9() { return static_cast<int32_t>(offsetof(SaveRemark_t4091223213, ___started_9)); }
	inline bool get_started_9() const { return ___started_9; }
	inline bool* get_address_of_started_9() { return &___started_9; }
	inline void set_started_9(bool value)
	{
		___started_9 = value;
	}

	inline static int32_t get_offset_of_onSaveAction_10() { return static_cast<int32_t>(offsetof(SaveRemark_t4091223213, ___onSaveAction_10)); }
	inline UnityAction_t4025899511 * get_onSaveAction_10() const { return ___onSaveAction_10; }
	inline UnityAction_t4025899511 ** get_address_of_onSaveAction_10() { return &___onSaveAction_10; }
	inline void set_onSaveAction_10(UnityAction_t4025899511 * value)
	{
		___onSaveAction_10 = value;
		Il2CppCodeGenWriteBarrier((&___onSaveAction_10), value);
	}

	inline static int32_t get_offset_of_currentItemStatus_11() { return static_cast<int32_t>(offsetof(SaveRemark_t4091223213, ___currentItemStatus_11)); }
	inline ItemStatus_t1378751697 * get_currentItemStatus_11() const { return ___currentItemStatus_11; }
	inline ItemStatus_t1378751697 ** get_address_of_currentItemStatus_11() { return &___currentItemStatus_11; }
	inline void set_currentItemStatus_11(ItemStatus_t1378751697 * value)
	{
		___currentItemStatus_11 = value;
		Il2CppCodeGenWriteBarrier((&___currentItemStatus_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEREMARK_T4091223213_H
#ifndef MOVEOBJECTHANDLER_T509454862_H
#define MOVEOBJECTHANDLER_T509454862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Move.MoveObjectHandler
struct  MoveObjectHandler_t509454862  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Facades.SubjectSingle`1<Library.Code.Domain.Dtos.ElementWithObject> Move.MoveObjectHandler::single
	RuntimeObject* ___single_2;
	// Library.Code.UI.RotateButton Move.MoveObjectHandler::clock
	RotateButton_t1854395031 * ___clock_3;
	// Library.Code.UI.RotateButton Move.MoveObjectHandler::antiClock
	RotateButton_t1854395031 * ___antiClock_4;
	// Library.Code.UI.RotateButton Move.MoveObjectHandler::up
	RotateButton_t1854395031 * ___up_5;
	// Library.Code.UI.RotateButton Move.MoveObjectHandler::down
	RotateButton_t1854395031 * ___down_6;

public:
	inline static int32_t get_offset_of_single_2() { return static_cast<int32_t>(offsetof(MoveObjectHandler_t509454862, ___single_2)); }
	inline RuntimeObject* get_single_2() const { return ___single_2; }
	inline RuntimeObject** get_address_of_single_2() { return &___single_2; }
	inline void set_single_2(RuntimeObject* value)
	{
		___single_2 = value;
		Il2CppCodeGenWriteBarrier((&___single_2), value);
	}

	inline static int32_t get_offset_of_clock_3() { return static_cast<int32_t>(offsetof(MoveObjectHandler_t509454862, ___clock_3)); }
	inline RotateButton_t1854395031 * get_clock_3() const { return ___clock_3; }
	inline RotateButton_t1854395031 ** get_address_of_clock_3() { return &___clock_3; }
	inline void set_clock_3(RotateButton_t1854395031 * value)
	{
		___clock_3 = value;
		Il2CppCodeGenWriteBarrier((&___clock_3), value);
	}

	inline static int32_t get_offset_of_antiClock_4() { return static_cast<int32_t>(offsetof(MoveObjectHandler_t509454862, ___antiClock_4)); }
	inline RotateButton_t1854395031 * get_antiClock_4() const { return ___antiClock_4; }
	inline RotateButton_t1854395031 ** get_address_of_antiClock_4() { return &___antiClock_4; }
	inline void set_antiClock_4(RotateButton_t1854395031 * value)
	{
		___antiClock_4 = value;
		Il2CppCodeGenWriteBarrier((&___antiClock_4), value);
	}

	inline static int32_t get_offset_of_up_5() { return static_cast<int32_t>(offsetof(MoveObjectHandler_t509454862, ___up_5)); }
	inline RotateButton_t1854395031 * get_up_5() const { return ___up_5; }
	inline RotateButton_t1854395031 ** get_address_of_up_5() { return &___up_5; }
	inline void set_up_5(RotateButton_t1854395031 * value)
	{
		___up_5 = value;
		Il2CppCodeGenWriteBarrier((&___up_5), value);
	}

	inline static int32_t get_offset_of_down_6() { return static_cast<int32_t>(offsetof(MoveObjectHandler_t509454862, ___down_6)); }
	inline RotateButton_t1854395031 * get_down_6() const { return ___down_6; }
	inline RotateButton_t1854395031 ** get_address_of_down_6() { return &___down_6; }
	inline void set_down_6(RotateButton_t1854395031 * value)
	{
		___down_6 = value;
		Il2CppCodeGenWriteBarrier((&___down_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEOBJECTHANDLER_T509454862_H
#ifndef DRAWERHANDLER_T321192673_H
#define DRAWERHANDLER_T321192673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrawerHandler
struct  DrawerHandler_t321192673  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject DrawerHandler::buttonContainer
	GameObject_t1756533147 * ___buttonContainer_2;
	// UnityEngine.GameObject DrawerHandler::buttonPrefab
	GameObject_t1756533147 * ___buttonPrefab_3;
	// UnityEngine.GameObject DrawerHandler::buttonRemark
	GameObject_t1756533147 * ___buttonRemark_4;
	// UnityEngine.GameObject DrawerHandler::buttonCheckList
	GameObject_t1756533147 * ___buttonCheckList_5;
	// UnityEngine.GameObject DrawerHandler::buttonElementColors
	GameObject_t1756533147 * ___buttonElementColors_6;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> DrawerHandler::buttons
	List_1_t1125654279 * ___buttons_7;
	// UnityEngine.GameObject DrawerHandler::buttomMessages
	GameObject_t1756533147 * ___buttomMessages_8;
	// System.String DrawerHandler::pathToUIPrefabDrawerButtons
	String_t* ___pathToUIPrefabDrawerButtons_9;
	// UnityEngine.GameObject DrawerHandler::listContainer
	GameObject_t1756533147 * ___listContainer_10;
	// DrawerScene DrawerHandler::drawerScene
	DrawerScene_t240065691 * ___drawerScene_11;
	// Library.Code.UI.PrefabGenerator.PrefabGenerator DrawerHandler::_prefabGenerator
	RuntimeObject* ____prefabGenerator_12;
	// System.String DrawerHandler::pathToUIDrawerButtonGraphics
	String_t* ___pathToUIDrawerButtonGraphics_13;
	// UnityEngine.Sprite DrawerHandler::buttonTop
	Sprite_t309593783 * ___buttonTop_14;
	// UnityEngine.Sprite DrawerHandler::buttonMid
	Sprite_t309593783 * ___buttonMid_15;
	// UnityEngine.Sprite DrawerHandler::buttonBottom
	Sprite_t309593783 * ___buttonBottom_16;

public:
	inline static int32_t get_offset_of_buttonContainer_2() { return static_cast<int32_t>(offsetof(DrawerHandler_t321192673, ___buttonContainer_2)); }
	inline GameObject_t1756533147 * get_buttonContainer_2() const { return ___buttonContainer_2; }
	inline GameObject_t1756533147 ** get_address_of_buttonContainer_2() { return &___buttonContainer_2; }
	inline void set_buttonContainer_2(GameObject_t1756533147 * value)
	{
		___buttonContainer_2 = value;
		Il2CppCodeGenWriteBarrier((&___buttonContainer_2), value);
	}

	inline static int32_t get_offset_of_buttonPrefab_3() { return static_cast<int32_t>(offsetof(DrawerHandler_t321192673, ___buttonPrefab_3)); }
	inline GameObject_t1756533147 * get_buttonPrefab_3() const { return ___buttonPrefab_3; }
	inline GameObject_t1756533147 ** get_address_of_buttonPrefab_3() { return &___buttonPrefab_3; }
	inline void set_buttonPrefab_3(GameObject_t1756533147 * value)
	{
		___buttonPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___buttonPrefab_3), value);
	}

	inline static int32_t get_offset_of_buttonRemark_4() { return static_cast<int32_t>(offsetof(DrawerHandler_t321192673, ___buttonRemark_4)); }
	inline GameObject_t1756533147 * get_buttonRemark_4() const { return ___buttonRemark_4; }
	inline GameObject_t1756533147 ** get_address_of_buttonRemark_4() { return &___buttonRemark_4; }
	inline void set_buttonRemark_4(GameObject_t1756533147 * value)
	{
		___buttonRemark_4 = value;
		Il2CppCodeGenWriteBarrier((&___buttonRemark_4), value);
	}

	inline static int32_t get_offset_of_buttonCheckList_5() { return static_cast<int32_t>(offsetof(DrawerHandler_t321192673, ___buttonCheckList_5)); }
	inline GameObject_t1756533147 * get_buttonCheckList_5() const { return ___buttonCheckList_5; }
	inline GameObject_t1756533147 ** get_address_of_buttonCheckList_5() { return &___buttonCheckList_5; }
	inline void set_buttonCheckList_5(GameObject_t1756533147 * value)
	{
		___buttonCheckList_5 = value;
		Il2CppCodeGenWriteBarrier((&___buttonCheckList_5), value);
	}

	inline static int32_t get_offset_of_buttonElementColors_6() { return static_cast<int32_t>(offsetof(DrawerHandler_t321192673, ___buttonElementColors_6)); }
	inline GameObject_t1756533147 * get_buttonElementColors_6() const { return ___buttonElementColors_6; }
	inline GameObject_t1756533147 ** get_address_of_buttonElementColors_6() { return &___buttonElementColors_6; }
	inline void set_buttonElementColors_6(GameObject_t1756533147 * value)
	{
		___buttonElementColors_6 = value;
		Il2CppCodeGenWriteBarrier((&___buttonElementColors_6), value);
	}

	inline static int32_t get_offset_of_buttons_7() { return static_cast<int32_t>(offsetof(DrawerHandler_t321192673, ___buttons_7)); }
	inline List_1_t1125654279 * get_buttons_7() const { return ___buttons_7; }
	inline List_1_t1125654279 ** get_address_of_buttons_7() { return &___buttons_7; }
	inline void set_buttons_7(List_1_t1125654279 * value)
	{
		___buttons_7 = value;
		Il2CppCodeGenWriteBarrier((&___buttons_7), value);
	}

	inline static int32_t get_offset_of_buttomMessages_8() { return static_cast<int32_t>(offsetof(DrawerHandler_t321192673, ___buttomMessages_8)); }
	inline GameObject_t1756533147 * get_buttomMessages_8() const { return ___buttomMessages_8; }
	inline GameObject_t1756533147 ** get_address_of_buttomMessages_8() { return &___buttomMessages_8; }
	inline void set_buttomMessages_8(GameObject_t1756533147 * value)
	{
		___buttomMessages_8 = value;
		Il2CppCodeGenWriteBarrier((&___buttomMessages_8), value);
	}

	inline static int32_t get_offset_of_pathToUIPrefabDrawerButtons_9() { return static_cast<int32_t>(offsetof(DrawerHandler_t321192673, ___pathToUIPrefabDrawerButtons_9)); }
	inline String_t* get_pathToUIPrefabDrawerButtons_9() const { return ___pathToUIPrefabDrawerButtons_9; }
	inline String_t** get_address_of_pathToUIPrefabDrawerButtons_9() { return &___pathToUIPrefabDrawerButtons_9; }
	inline void set_pathToUIPrefabDrawerButtons_9(String_t* value)
	{
		___pathToUIPrefabDrawerButtons_9 = value;
		Il2CppCodeGenWriteBarrier((&___pathToUIPrefabDrawerButtons_9), value);
	}

	inline static int32_t get_offset_of_listContainer_10() { return static_cast<int32_t>(offsetof(DrawerHandler_t321192673, ___listContainer_10)); }
	inline GameObject_t1756533147 * get_listContainer_10() const { return ___listContainer_10; }
	inline GameObject_t1756533147 ** get_address_of_listContainer_10() { return &___listContainer_10; }
	inline void set_listContainer_10(GameObject_t1756533147 * value)
	{
		___listContainer_10 = value;
		Il2CppCodeGenWriteBarrier((&___listContainer_10), value);
	}

	inline static int32_t get_offset_of_drawerScene_11() { return static_cast<int32_t>(offsetof(DrawerHandler_t321192673, ___drawerScene_11)); }
	inline DrawerScene_t240065691 * get_drawerScene_11() const { return ___drawerScene_11; }
	inline DrawerScene_t240065691 ** get_address_of_drawerScene_11() { return &___drawerScene_11; }
	inline void set_drawerScene_11(DrawerScene_t240065691 * value)
	{
		___drawerScene_11 = value;
		Il2CppCodeGenWriteBarrier((&___drawerScene_11), value);
	}

	inline static int32_t get_offset_of__prefabGenerator_12() { return static_cast<int32_t>(offsetof(DrawerHandler_t321192673, ____prefabGenerator_12)); }
	inline RuntimeObject* get__prefabGenerator_12() const { return ____prefabGenerator_12; }
	inline RuntimeObject** get_address_of__prefabGenerator_12() { return &____prefabGenerator_12; }
	inline void set__prefabGenerator_12(RuntimeObject* value)
	{
		____prefabGenerator_12 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_12), value);
	}

	inline static int32_t get_offset_of_pathToUIDrawerButtonGraphics_13() { return static_cast<int32_t>(offsetof(DrawerHandler_t321192673, ___pathToUIDrawerButtonGraphics_13)); }
	inline String_t* get_pathToUIDrawerButtonGraphics_13() const { return ___pathToUIDrawerButtonGraphics_13; }
	inline String_t** get_address_of_pathToUIDrawerButtonGraphics_13() { return &___pathToUIDrawerButtonGraphics_13; }
	inline void set_pathToUIDrawerButtonGraphics_13(String_t* value)
	{
		___pathToUIDrawerButtonGraphics_13 = value;
		Il2CppCodeGenWriteBarrier((&___pathToUIDrawerButtonGraphics_13), value);
	}

	inline static int32_t get_offset_of_buttonTop_14() { return static_cast<int32_t>(offsetof(DrawerHandler_t321192673, ___buttonTop_14)); }
	inline Sprite_t309593783 * get_buttonTop_14() const { return ___buttonTop_14; }
	inline Sprite_t309593783 ** get_address_of_buttonTop_14() { return &___buttonTop_14; }
	inline void set_buttonTop_14(Sprite_t309593783 * value)
	{
		___buttonTop_14 = value;
		Il2CppCodeGenWriteBarrier((&___buttonTop_14), value);
	}

	inline static int32_t get_offset_of_buttonMid_15() { return static_cast<int32_t>(offsetof(DrawerHandler_t321192673, ___buttonMid_15)); }
	inline Sprite_t309593783 * get_buttonMid_15() const { return ___buttonMid_15; }
	inline Sprite_t309593783 ** get_address_of_buttonMid_15() { return &___buttonMid_15; }
	inline void set_buttonMid_15(Sprite_t309593783 * value)
	{
		___buttonMid_15 = value;
		Il2CppCodeGenWriteBarrier((&___buttonMid_15), value);
	}

	inline static int32_t get_offset_of_buttonBottom_16() { return static_cast<int32_t>(offsetof(DrawerHandler_t321192673, ___buttonBottom_16)); }
	inline Sprite_t309593783 * get_buttonBottom_16() const { return ___buttonBottom_16; }
	inline Sprite_t309593783 ** get_address_of_buttonBottom_16() { return &___buttonBottom_16; }
	inline void set_buttonBottom_16(Sprite_t309593783 * value)
	{
		___buttonBottom_16 = value;
		Il2CppCodeGenWriteBarrier((&___buttonBottom_16), value);
	}
};

struct DrawerHandler_t321192673_StaticFields
{
public:
	// System.Action`1<UnityEngine.GameObject> DrawerHandler::<>f__am$cache0
	Action_1_t1558332529 * ___U3CU3Ef__amU24cache0_17;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_17() { return static_cast<int32_t>(offsetof(DrawerHandler_t321192673_StaticFields, ___U3CU3Ef__amU24cache0_17)); }
	inline Action_1_t1558332529 * get_U3CU3Ef__amU24cache0_17() const { return ___U3CU3Ef__amU24cache0_17; }
	inline Action_1_t1558332529 ** get_address_of_U3CU3Ef__amU24cache0_17() { return &___U3CU3Ef__amU24cache0_17; }
	inline void set_U3CU3Ef__amU24cache0_17(Action_1_t1558332529 * value)
	{
		___U3CU3Ef__amU24cache0_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWERHANDLER_T321192673_H
#ifndef DELETEFROMLIST_T465902211_H
#define DELETEFROMLIST_T465902211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeleteFromList
struct  DeleteFromList_t465902211  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button DeleteFromList::yourButton
	Button_t2872111280 * ___yourButton_2;

public:
	inline static int32_t get_offset_of_yourButton_2() { return static_cast<int32_t>(offsetof(DeleteFromList_t465902211, ___yourButton_2)); }
	inline Button_t2872111280 * get_yourButton_2() const { return ___yourButton_2; }
	inline Button_t2872111280 ** get_address_of_yourButton_2() { return &___yourButton_2; }
	inline void set_yourButton_2(Button_t2872111280 * value)
	{
		___yourButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___yourButton_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETEFROMLIST_T465902211_H
#ifndef ROTATEBUTTON_T1854395031_H
#define ROTATEBUTTON_T1854395031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.RotateButton
struct  RotateButton_t1854395031  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 Library.Code.UI.RotateButton::rotation
	int32_t ___rotation_2;
	// System.Int32 Library.Code.UI.RotateButton::value
	int32_t ___value_3;
	// UnityEngine.GameObject Library.Code.UI.RotateButton::model
	GameObject_t1756533147 * ___model_4;
	// System.Boolean Library.Code.UI.RotateButton::hold
	bool ___hold_5;

public:
	inline static int32_t get_offset_of_rotation_2() { return static_cast<int32_t>(offsetof(RotateButton_t1854395031, ___rotation_2)); }
	inline int32_t get_rotation_2() const { return ___rotation_2; }
	inline int32_t* get_address_of_rotation_2() { return &___rotation_2; }
	inline void set_rotation_2(int32_t value)
	{
		___rotation_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(RotateButton_t1854395031, ___value_3)); }
	inline int32_t get_value_3() const { return ___value_3; }
	inline int32_t* get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(int32_t value)
	{
		___value_3 = value;
	}

	inline static int32_t get_offset_of_model_4() { return static_cast<int32_t>(offsetof(RotateButton_t1854395031, ___model_4)); }
	inline GameObject_t1756533147 * get_model_4() const { return ___model_4; }
	inline GameObject_t1756533147 ** get_address_of_model_4() { return &___model_4; }
	inline void set_model_4(GameObject_t1756533147 * value)
	{
		___model_4 = value;
		Il2CppCodeGenWriteBarrier((&___model_4), value);
	}

	inline static int32_t get_offset_of_hold_5() { return static_cast<int32_t>(offsetof(RotateButton_t1854395031, ___hold_5)); }
	inline bool get_hold_5() const { return ___hold_5; }
	inline bool* get_address_of_hold_5() { return &___hold_5; }
	inline void set_hold_5(bool value)
	{
		___hold_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEBUTTON_T1854395031_H
#ifndef DOASYNCIMP_T1177308585_H
#define DOASYNCIMP_T1177308585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Utils.Async.DoAsyncImp
struct  DoAsyncImp_t1177308585  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOASYNCIMP_T1177308585_H
#ifndef DRAWERSCENE_T240065691_H
#define DRAWERSCENE_T240065691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrawerScene
struct  DrawerScene_t240065691  : public MonoBehaviour_t1158329972
{
public:
	// DrawerHandler DrawerScene::drawerHandler
	DrawerHandler_t321192673 * ___drawerHandler_2;
	// Library.Code.DI.Prefab.PrefabProvider DrawerScene::_prefabProvider
	PrefabProvider_t1422780085 * ____prefabProvider_3;
	// Library.Code.DI.GroupinModel.GroupingModelProvider DrawerScene::_groupingModelProvider
	GroupingModelProvider_t2551172685 * ____groupingModelProvider_4;
	// Library.Code.DI.MessageTempaltes.MessageTemplatesProvider DrawerScene::_messageTemplatesProvider
	MessageTemplatesProvider_t2923081561 * ____messageTemplatesProvider_5;
	// Library.Code.DI.Message.MessageProvider DrawerScene::_messageProvider
	MessageProvider_t618924372 * ____messageProvider_6;
	// Library.Code.DI.DepedencyProvider DrawerScene::depedencyProvider
	RuntimeObject* ___depedencyProvider_7;
	// System.String DrawerScene::pathToUIPrefabs
	String_t* ___pathToUIPrefabs_8;
	// UnityEngine.GameObject DrawerScene::elementColorsScene
	GameObject_t1756533147 * ___elementColorsScene_9;
	// UnityEngine.GameObject DrawerScene::remarkScene
	GameObject_t1756533147 * ___remarkScene_10;
	// UnityEngine.GameObject DrawerScene::checklistScene
	GameObject_t1756533147 * ___checklistScene_11;
	// UnityEngine.GameObject DrawerScene::messagelistScene
	GameObject_t1756533147 * ___messagelistScene_12;
	// UnityEngine.GameObject DrawerScene::modelelementScene
	GameObject_t1756533147 * ___modelelementScene_13;
	// UnityEngine.GameObject DrawerScene::currentStatusScene
	GameObject_t1756533147 * ___currentStatusScene_14;
	// UnityEngine.GameObject DrawerScene::photoPopupScene
	GameObject_t1756533147 * ___photoPopupScene_15;
	// UnityEngine.GameObject DrawerScene::listContainer
	GameObject_t1756533147 * ___listContainer_16;
	// UnityEngine.GameObject DrawerScene::canvasContainer
	GameObject_t1756533147 * ___canvasContainer_17;
	// Library.Code.UI.PrefabGenerator.PrefabGenerator DrawerScene::_prefabGenerator
	RuntimeObject* ____prefabGenerator_18;
	// System.Boolean DrawerScene::elementLoaded
	bool ___elementLoaded_19;
	// UnityEngine.GameObject DrawerScene::footer
	GameObject_t1756533147 * ___footer_20;
	// Library.Code.Communication.Communication DrawerScene::comm
	Communication_t424466906 * ___comm_21;
	// Library.Code.DI.PopUp.PopUpShower DrawerScene::popUpShower
	PopUpShower_t3186557280 * ___popUpShower_22;

public:
	inline static int32_t get_offset_of_drawerHandler_2() { return static_cast<int32_t>(offsetof(DrawerScene_t240065691, ___drawerHandler_2)); }
	inline DrawerHandler_t321192673 * get_drawerHandler_2() const { return ___drawerHandler_2; }
	inline DrawerHandler_t321192673 ** get_address_of_drawerHandler_2() { return &___drawerHandler_2; }
	inline void set_drawerHandler_2(DrawerHandler_t321192673 * value)
	{
		___drawerHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___drawerHandler_2), value);
	}

	inline static int32_t get_offset_of__prefabProvider_3() { return static_cast<int32_t>(offsetof(DrawerScene_t240065691, ____prefabProvider_3)); }
	inline PrefabProvider_t1422780085 * get__prefabProvider_3() const { return ____prefabProvider_3; }
	inline PrefabProvider_t1422780085 ** get_address_of__prefabProvider_3() { return &____prefabProvider_3; }
	inline void set__prefabProvider_3(PrefabProvider_t1422780085 * value)
	{
		____prefabProvider_3 = value;
		Il2CppCodeGenWriteBarrier((&____prefabProvider_3), value);
	}

	inline static int32_t get_offset_of__groupingModelProvider_4() { return static_cast<int32_t>(offsetof(DrawerScene_t240065691, ____groupingModelProvider_4)); }
	inline GroupingModelProvider_t2551172685 * get__groupingModelProvider_4() const { return ____groupingModelProvider_4; }
	inline GroupingModelProvider_t2551172685 ** get_address_of__groupingModelProvider_4() { return &____groupingModelProvider_4; }
	inline void set__groupingModelProvider_4(GroupingModelProvider_t2551172685 * value)
	{
		____groupingModelProvider_4 = value;
		Il2CppCodeGenWriteBarrier((&____groupingModelProvider_4), value);
	}

	inline static int32_t get_offset_of__messageTemplatesProvider_5() { return static_cast<int32_t>(offsetof(DrawerScene_t240065691, ____messageTemplatesProvider_5)); }
	inline MessageTemplatesProvider_t2923081561 * get__messageTemplatesProvider_5() const { return ____messageTemplatesProvider_5; }
	inline MessageTemplatesProvider_t2923081561 ** get_address_of__messageTemplatesProvider_5() { return &____messageTemplatesProvider_5; }
	inline void set__messageTemplatesProvider_5(MessageTemplatesProvider_t2923081561 * value)
	{
		____messageTemplatesProvider_5 = value;
		Il2CppCodeGenWriteBarrier((&____messageTemplatesProvider_5), value);
	}

	inline static int32_t get_offset_of__messageProvider_6() { return static_cast<int32_t>(offsetof(DrawerScene_t240065691, ____messageProvider_6)); }
	inline MessageProvider_t618924372 * get__messageProvider_6() const { return ____messageProvider_6; }
	inline MessageProvider_t618924372 ** get_address_of__messageProvider_6() { return &____messageProvider_6; }
	inline void set__messageProvider_6(MessageProvider_t618924372 * value)
	{
		____messageProvider_6 = value;
		Il2CppCodeGenWriteBarrier((&____messageProvider_6), value);
	}

	inline static int32_t get_offset_of_depedencyProvider_7() { return static_cast<int32_t>(offsetof(DrawerScene_t240065691, ___depedencyProvider_7)); }
	inline RuntimeObject* get_depedencyProvider_7() const { return ___depedencyProvider_7; }
	inline RuntimeObject** get_address_of_depedencyProvider_7() { return &___depedencyProvider_7; }
	inline void set_depedencyProvider_7(RuntimeObject* value)
	{
		___depedencyProvider_7 = value;
		Il2CppCodeGenWriteBarrier((&___depedencyProvider_7), value);
	}

	inline static int32_t get_offset_of_pathToUIPrefabs_8() { return static_cast<int32_t>(offsetof(DrawerScene_t240065691, ___pathToUIPrefabs_8)); }
	inline String_t* get_pathToUIPrefabs_8() const { return ___pathToUIPrefabs_8; }
	inline String_t** get_address_of_pathToUIPrefabs_8() { return &___pathToUIPrefabs_8; }
	inline void set_pathToUIPrefabs_8(String_t* value)
	{
		___pathToUIPrefabs_8 = value;
		Il2CppCodeGenWriteBarrier((&___pathToUIPrefabs_8), value);
	}

	inline static int32_t get_offset_of_elementColorsScene_9() { return static_cast<int32_t>(offsetof(DrawerScene_t240065691, ___elementColorsScene_9)); }
	inline GameObject_t1756533147 * get_elementColorsScene_9() const { return ___elementColorsScene_9; }
	inline GameObject_t1756533147 ** get_address_of_elementColorsScene_9() { return &___elementColorsScene_9; }
	inline void set_elementColorsScene_9(GameObject_t1756533147 * value)
	{
		___elementColorsScene_9 = value;
		Il2CppCodeGenWriteBarrier((&___elementColorsScene_9), value);
	}

	inline static int32_t get_offset_of_remarkScene_10() { return static_cast<int32_t>(offsetof(DrawerScene_t240065691, ___remarkScene_10)); }
	inline GameObject_t1756533147 * get_remarkScene_10() const { return ___remarkScene_10; }
	inline GameObject_t1756533147 ** get_address_of_remarkScene_10() { return &___remarkScene_10; }
	inline void set_remarkScene_10(GameObject_t1756533147 * value)
	{
		___remarkScene_10 = value;
		Il2CppCodeGenWriteBarrier((&___remarkScene_10), value);
	}

	inline static int32_t get_offset_of_checklistScene_11() { return static_cast<int32_t>(offsetof(DrawerScene_t240065691, ___checklistScene_11)); }
	inline GameObject_t1756533147 * get_checklistScene_11() const { return ___checklistScene_11; }
	inline GameObject_t1756533147 ** get_address_of_checklistScene_11() { return &___checklistScene_11; }
	inline void set_checklistScene_11(GameObject_t1756533147 * value)
	{
		___checklistScene_11 = value;
		Il2CppCodeGenWriteBarrier((&___checklistScene_11), value);
	}

	inline static int32_t get_offset_of_messagelistScene_12() { return static_cast<int32_t>(offsetof(DrawerScene_t240065691, ___messagelistScene_12)); }
	inline GameObject_t1756533147 * get_messagelistScene_12() const { return ___messagelistScene_12; }
	inline GameObject_t1756533147 ** get_address_of_messagelistScene_12() { return &___messagelistScene_12; }
	inline void set_messagelistScene_12(GameObject_t1756533147 * value)
	{
		___messagelistScene_12 = value;
		Il2CppCodeGenWriteBarrier((&___messagelistScene_12), value);
	}

	inline static int32_t get_offset_of_modelelementScene_13() { return static_cast<int32_t>(offsetof(DrawerScene_t240065691, ___modelelementScene_13)); }
	inline GameObject_t1756533147 * get_modelelementScene_13() const { return ___modelelementScene_13; }
	inline GameObject_t1756533147 ** get_address_of_modelelementScene_13() { return &___modelelementScene_13; }
	inline void set_modelelementScene_13(GameObject_t1756533147 * value)
	{
		___modelelementScene_13 = value;
		Il2CppCodeGenWriteBarrier((&___modelelementScene_13), value);
	}

	inline static int32_t get_offset_of_currentStatusScene_14() { return static_cast<int32_t>(offsetof(DrawerScene_t240065691, ___currentStatusScene_14)); }
	inline GameObject_t1756533147 * get_currentStatusScene_14() const { return ___currentStatusScene_14; }
	inline GameObject_t1756533147 ** get_address_of_currentStatusScene_14() { return &___currentStatusScene_14; }
	inline void set_currentStatusScene_14(GameObject_t1756533147 * value)
	{
		___currentStatusScene_14 = value;
		Il2CppCodeGenWriteBarrier((&___currentStatusScene_14), value);
	}

	inline static int32_t get_offset_of_photoPopupScene_15() { return static_cast<int32_t>(offsetof(DrawerScene_t240065691, ___photoPopupScene_15)); }
	inline GameObject_t1756533147 * get_photoPopupScene_15() const { return ___photoPopupScene_15; }
	inline GameObject_t1756533147 ** get_address_of_photoPopupScene_15() { return &___photoPopupScene_15; }
	inline void set_photoPopupScene_15(GameObject_t1756533147 * value)
	{
		___photoPopupScene_15 = value;
		Il2CppCodeGenWriteBarrier((&___photoPopupScene_15), value);
	}

	inline static int32_t get_offset_of_listContainer_16() { return static_cast<int32_t>(offsetof(DrawerScene_t240065691, ___listContainer_16)); }
	inline GameObject_t1756533147 * get_listContainer_16() const { return ___listContainer_16; }
	inline GameObject_t1756533147 ** get_address_of_listContainer_16() { return &___listContainer_16; }
	inline void set_listContainer_16(GameObject_t1756533147 * value)
	{
		___listContainer_16 = value;
		Il2CppCodeGenWriteBarrier((&___listContainer_16), value);
	}

	inline static int32_t get_offset_of_canvasContainer_17() { return static_cast<int32_t>(offsetof(DrawerScene_t240065691, ___canvasContainer_17)); }
	inline GameObject_t1756533147 * get_canvasContainer_17() const { return ___canvasContainer_17; }
	inline GameObject_t1756533147 ** get_address_of_canvasContainer_17() { return &___canvasContainer_17; }
	inline void set_canvasContainer_17(GameObject_t1756533147 * value)
	{
		___canvasContainer_17 = value;
		Il2CppCodeGenWriteBarrier((&___canvasContainer_17), value);
	}

	inline static int32_t get_offset_of__prefabGenerator_18() { return static_cast<int32_t>(offsetof(DrawerScene_t240065691, ____prefabGenerator_18)); }
	inline RuntimeObject* get__prefabGenerator_18() const { return ____prefabGenerator_18; }
	inline RuntimeObject** get_address_of__prefabGenerator_18() { return &____prefabGenerator_18; }
	inline void set__prefabGenerator_18(RuntimeObject* value)
	{
		____prefabGenerator_18 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_18), value);
	}

	inline static int32_t get_offset_of_elementLoaded_19() { return static_cast<int32_t>(offsetof(DrawerScene_t240065691, ___elementLoaded_19)); }
	inline bool get_elementLoaded_19() const { return ___elementLoaded_19; }
	inline bool* get_address_of_elementLoaded_19() { return &___elementLoaded_19; }
	inline void set_elementLoaded_19(bool value)
	{
		___elementLoaded_19 = value;
	}

	inline static int32_t get_offset_of_footer_20() { return static_cast<int32_t>(offsetof(DrawerScene_t240065691, ___footer_20)); }
	inline GameObject_t1756533147 * get_footer_20() const { return ___footer_20; }
	inline GameObject_t1756533147 ** get_address_of_footer_20() { return &___footer_20; }
	inline void set_footer_20(GameObject_t1756533147 * value)
	{
		___footer_20 = value;
		Il2CppCodeGenWriteBarrier((&___footer_20), value);
	}

	inline static int32_t get_offset_of_comm_21() { return static_cast<int32_t>(offsetof(DrawerScene_t240065691, ___comm_21)); }
	inline Communication_t424466906 * get_comm_21() const { return ___comm_21; }
	inline Communication_t424466906 ** get_address_of_comm_21() { return &___comm_21; }
	inline void set_comm_21(Communication_t424466906 * value)
	{
		___comm_21 = value;
		Il2CppCodeGenWriteBarrier((&___comm_21), value);
	}

	inline static int32_t get_offset_of_popUpShower_22() { return static_cast<int32_t>(offsetof(DrawerScene_t240065691, ___popUpShower_22)); }
	inline PopUpShower_t3186557280 * get_popUpShower_22() const { return ___popUpShower_22; }
	inline PopUpShower_t3186557280 ** get_address_of_popUpShower_22() { return &___popUpShower_22; }
	inline void set_popUpShower_22(PopUpShower_t3186557280 * value)
	{
		___popUpShower_22 = value;
		Il2CppCodeGenWriteBarrier((&___popUpShower_22), value);
	}
};

struct DrawerScene_t240065691_StaticFields
{
public:
	// LanguageStrings DrawerScene::languageStrings
	LanguageStrings_t1206686884 * ___languageStrings_23;

public:
	inline static int32_t get_offset_of_languageStrings_23() { return static_cast<int32_t>(offsetof(DrawerScene_t240065691_StaticFields, ___languageStrings_23)); }
	inline LanguageStrings_t1206686884 * get_languageStrings_23() const { return ___languageStrings_23; }
	inline LanguageStrings_t1206686884 ** get_address_of_languageStrings_23() { return &___languageStrings_23; }
	inline void set_languageStrings_23(LanguageStrings_t1206686884 * value)
	{
		___languageStrings_23 = value;
		Il2CppCodeGenWriteBarrier((&___languageStrings_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWERSCENE_T240065691_H
#ifndef DRAWERANIMATIONBUTTON_T1773246733_H
#define DRAWERANIMATIONBUTTON_T1773246733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DrawerAnimationButton
struct  DrawerAnimationButton_t1773246733  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Animator DrawerAnimationButton::anim
	Animator_t69676727 * ___anim_2;
	// System.Boolean DrawerAnimationButton::active
	bool ___active_3;

public:
	inline static int32_t get_offset_of_anim_2() { return static_cast<int32_t>(offsetof(DrawerAnimationButton_t1773246733, ___anim_2)); }
	inline Animator_t69676727 * get_anim_2() const { return ___anim_2; }
	inline Animator_t69676727 ** get_address_of_anim_2() { return &___anim_2; }
	inline void set_anim_2(Animator_t69676727 * value)
	{
		___anim_2 = value;
		Il2CppCodeGenWriteBarrier((&___anim_2), value);
	}

	inline static int32_t get_offset_of_active_3() { return static_cast<int32_t>(offsetof(DrawerAnimationButton_t1773246733, ___active_3)); }
	inline bool get_active_3() const { return ___active_3; }
	inline bool* get_address_of_active_3() { return &___active_3; }
	inline void set_active_3(bool value)
	{
		___active_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWERANIMATIONBUTTON_T1773246733_H
#ifndef MESSAGEOBSERVER_T1722898529_H
#define MESSAGEOBSERVER_T1722898529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageObserver
struct  MessageObserver_t1722898529  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEOBSERVER_T1722898529_H
#ifndef DISPATCHER_T3198616969_H
#define DISPATCHER_T3198616969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CI.HttpClient.Core.Dispatcher
struct  Dispatcher_t3198616969  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct Dispatcher_t3198616969_StaticFields
{
public:
	// System.Collections.Generic.Queue`1<System.Action> CI.HttpClient.Core.Dispatcher::_queue
	Queue_1_t3046128587 * ____queue_2;
	// System.Object CI.HttpClient.Core.Dispatcher::_lock
	RuntimeObject * ____lock_3;

public:
	inline static int32_t get_offset_of__queue_2() { return static_cast<int32_t>(offsetof(Dispatcher_t3198616969_StaticFields, ____queue_2)); }
	inline Queue_1_t3046128587 * get__queue_2() const { return ____queue_2; }
	inline Queue_1_t3046128587 ** get_address_of__queue_2() { return &____queue_2; }
	inline void set__queue_2(Queue_1_t3046128587 * value)
	{
		____queue_2 = value;
		Il2CppCodeGenWriteBarrier((&____queue_2), value);
	}

	inline static int32_t get_offset_of__lock_3() { return static_cast<int32_t>(offsetof(Dispatcher_t3198616969_StaticFields, ____lock_3)); }
	inline RuntimeObject * get__lock_3() const { return ____lock_3; }
	inline RuntimeObject ** get_address_of__lock_3() { return &____lock_3; }
	inline void set__lock_3(RuntimeObject * value)
	{
		____lock_3 = value;
		Il2CppCodeGenWriteBarrier((&____lock_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPATCHER_T3198616969_H
#ifndef GROUPINCOLORMOBILEIMP_T1767066759_H
#define GROUPINCOLORMOBILEIMP_T1767066759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI.GroupinColorMobileImp
struct  GroupinColorMobileImp_t1767066759  : public GroupinColor_t2175596517
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPINCOLORMOBILEIMP_T1767066759_H
#ifndef MESSAGETEMPLATESPROVIDER_T2923081561_H
#define MESSAGETEMPLATESPROVIDER_T2923081561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.MessageTempaltes.MessageTemplatesProvider
struct  MessageTemplatesProvider_t2923081561  : public ProviderWithApiFactory_1_t2940439303
{
public:
	// Library.Code.DI.MessageTempaltes.MessageTempaltesModule Library.Code.DI.MessageTempaltes.MessageTemplatesProvider::_messageTempaltesModule
	MessageTempaltesModule_t744814112 * ____messageTempaltesModule_2;

public:
	inline static int32_t get_offset_of__messageTempaltesModule_2() { return static_cast<int32_t>(offsetof(MessageTemplatesProvider_t2923081561, ____messageTempaltesModule_2)); }
	inline MessageTempaltesModule_t744814112 * get__messageTempaltesModule_2() const { return ____messageTempaltesModule_2; }
	inline MessageTempaltesModule_t744814112 ** get_address_of__messageTempaltesModule_2() { return &____messageTempaltesModule_2; }
	inline void set__messageTempaltesModule_2(MessageTempaltesModule_t744814112 * value)
	{
		____messageTempaltesModule_2 = value;
		Il2CppCodeGenWriteBarrier((&____messageTempaltesModule_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGETEMPLATESPROVIDER_T2923081561_H
#ifndef MESSAGESPROVIDERIMP_T2278660791_H
#define MESSAGESPROVIDERIMP_T2278660791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessagesProviderImp
struct  MessagesProviderImp_t2278660791  : public MessageProvider_t618924372
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGESPROVIDERIMP_T2278660791_H
#ifndef ELEMENTHANDLERPROVIDERIMP_T2037353801_H
#define ELEMENTHANDLERPROVIDERIMP_T2037353801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DI.ElementHandlerProviderImp
struct  ElementHandlerProviderImp_t2037353801  : public ElementHandlerProvider_t2338674629
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTHANDLERPROVIDERIMP_T2037353801_H
#ifndef ELEMENT3DPROVIERIMP_T2228562702_H
#define ELEMENT3DPROVIERIMP_T2228562702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DI.Element3dProvierImp
struct  Element3dProvierImp_t2228562702  : public Element3dProvider_t3146139312
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT3DPROVIERIMP_T2228562702_H
#ifndef GROUPINCOLORMOBILEIMPNEW_T3384506007_H
#define GROUPINCOLORMOBILEIMPNEW_T3384506007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI.GroupinColorMobileImpNew
struct  GroupinColorMobileImpNew_t3384506007  : public GroupinColorNew_t4212717807
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPINCOLORMOBILEIMPNEW_T3384506007_H
#ifndef PREFABSPROVIDERIMP_T1296656228_H
#define PREFABSPROVIDERIMP_T1296656228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PrefabsProviderImp
struct  PrefabsProviderImp_t1296656228  : public PrefabProvider_t1422780085
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABSPROVIDERIMP_T1296656228_H
#ifndef CHECKLISTPROVIDER_T3393423783_H
#define CHECKLISTPROVIDER_T3393423783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.Checklist.ChecklistProvider
struct  ChecklistProvider_t3393423783  : public ProviderWithApiFactory_1_t992121606
{
public:
	// Library.Code.DI.Checklist.ChecklistModule Library.Code.DI.Checklist.ChecklistProvider::_checklistModule
	ChecklistModule_t3684412132 * ____checklistModule_2;

public:
	inline static int32_t get_offset_of__checklistModule_2() { return static_cast<int32_t>(offsetof(ChecklistProvider_t3393423783, ____checklistModule_2)); }
	inline ChecklistModule_t3684412132 * get__checklistModule_2() const { return ____checklistModule_2; }
	inline ChecklistModule_t3684412132 ** get_address_of__checklistModule_2() { return &____checklistModule_2; }
	inline void set__checklistModule_2(ChecklistModule_t3684412132 * value)
	{
		____checklistModule_2 = value;
		Il2CppCodeGenWriteBarrier((&____checklistModule_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKLISTPROVIDER_T3393423783_H
#ifndef ELEMENTAPPROVEPROVIDER_T2738039706_H
#define ELEMENTAPPROVEPROVIDER_T2738039706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.Elements.ElementApproveProvider
struct  ElementApproveProvider_t2738039706  : public ProviderWithApiFactory_1_t1501685989
{
public:
	// Library.Code.DI.Elements.ElementApproveStatusModule Library.Code.DI.Elements.ElementApproveProvider::_elementApproveStatusModule
	ElementApproveStatusModule_t4176563737 * ____elementApproveStatusModule_2;

public:
	inline static int32_t get_offset_of__elementApproveStatusModule_2() { return static_cast<int32_t>(offsetof(ElementApproveProvider_t2738039706, ____elementApproveStatusModule_2)); }
	inline ElementApproveStatusModule_t4176563737 * get__elementApproveStatusModule_2() const { return ____elementApproveStatusModule_2; }
	inline ElementApproveStatusModule_t4176563737 ** get_address_of__elementApproveStatusModule_2() { return &____elementApproveStatusModule_2; }
	inline void set__elementApproveStatusModule_2(ElementApproveStatusModule_t4176563737 * value)
	{
		____elementApproveStatusModule_2 = value;
		Il2CppCodeGenWriteBarrier((&____elementApproveStatusModule_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTAPPROVEPROVIDER_T2738039706_H
#ifndef ELEMENTPROVIDER_T3018113545_H
#define ELEMENTPROVIDER_T3018113545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.Elements.ElementProvider
struct  ElementProvider_t3018113545  : public ProviderWithApiFactory_1_t4144576901
{
public:
	// Library.Code.DI.Elements.ElementFacadeModule Library.Code.DI.Elements.ElementProvider::_elementFacadeModule
	ElementFacadeModule_t1081315832 * ____elementFacadeModule_2;

public:
	inline static int32_t get_offset_of__elementFacadeModule_2() { return static_cast<int32_t>(offsetof(ElementProvider_t3018113545, ____elementFacadeModule_2)); }
	inline ElementFacadeModule_t1081315832 * get__elementFacadeModule_2() const { return ____elementFacadeModule_2; }
	inline ElementFacadeModule_t1081315832 ** get_address_of__elementFacadeModule_2() { return &____elementFacadeModule_2; }
	inline void set__elementFacadeModule_2(ElementFacadeModule_t1081315832 * value)
	{
		____elementFacadeModule_2 = value;
		Il2CppCodeGenWriteBarrier((&____elementFacadeModule_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTPROVIDER_T3018113545_H
#ifndef GROUPINGMODELPROVIDER_T2551172685_H
#define GROUPINGMODELPROVIDER_T2551172685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.GroupinModel.GroupingModelProvider
struct  GroupingModelProvider_t2551172685  : public ProviderWithApiFactory_1_t1428020601
{
public:
	// Library.Code.DI.GroupinModel.GroupingModelModule Library.Code.DI.GroupinModel.GroupingModelProvider::_groupingModelModule
	GroupingModelModule_t337983572 * ____groupingModelModule_2;

public:
	inline static int32_t get_offset_of__groupingModelModule_2() { return static_cast<int32_t>(offsetof(GroupingModelProvider_t2551172685, ____groupingModelModule_2)); }
	inline GroupingModelModule_t337983572 * get__groupingModelModule_2() const { return ____groupingModelModule_2; }
	inline GroupingModelModule_t337983572 ** get_address_of__groupingModelModule_2() { return &____groupingModelModule_2; }
	inline void set__groupingModelModule_2(GroupingModelModule_t337983572 * value)
	{
		____groupingModelModule_2 = value;
		Il2CppCodeGenWriteBarrier((&____groupingModelModule_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPINGMODELPROVIDER_T2551172685_H
#ifndef SAVEREMARKIMP_T1879814373_H
#define SAVEREMARKIMP_T1879814373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.Remark.SaveRemarkImp
struct  SaveRemarkImp_t1879814373  : public SaveRemark_t4091223213
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAVEREMARKIMP_T1879814373_H
#ifndef MODELPROVIDER_T1845401484_H
#define MODELPROVIDER_T1845401484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.Models.ModelProvider
struct  ModelProvider_t1845401484  : public ProviderWithApiFactory_1_t2855966632
{
public:
	// Library.Code.DI.Models.ModelFacadeModule Library.Code.DI.Models.ModelProvider::_modelFacadeModule
	ModelFacadeModule_t3252945753 * ____modelFacadeModule_2;

public:
	inline static int32_t get_offset_of__modelFacadeModule_2() { return static_cast<int32_t>(offsetof(ModelProvider_t1845401484, ____modelFacadeModule_2)); }
	inline ModelFacadeModule_t3252945753 * get__modelFacadeModule_2() const { return ____modelFacadeModule_2; }
	inline ModelFacadeModule_t3252945753 ** get_address_of__modelFacadeModule_2() { return &____modelFacadeModule_2; }
	inline void set__modelFacadeModule_2(ModelFacadeModule_t3252945753 * value)
	{
		____modelFacadeModule_2 = value;
		Il2CppCodeGenWriteBarrier((&____modelFacadeModule_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELPROVIDER_T1845401484_H
#ifndef CLOSEREMARKIMP_T4161074716_H
#define CLOSEREMARKIMP_T4161074716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.Remark.CloseRemarkImp
struct  CloseRemarkImp_t4161074716  : public CloseRemark_t2638852306
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSEREMARKIMP_T4161074716_H
#ifndef ELEMENTPROVIERIMP_T3781256465_H
#define ELEMENTPROVIERIMP_T3781256465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DI.ElementProvierImp
struct  ElementProvierImp_t3781256465  : public ElementProvider_t3018113545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTPROVIERIMP_T3781256465_H
#ifndef MESSAGETEMPLATEIMP_T2751049673_H
#define MESSAGETEMPLATEIMP_T2751049673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageTemplateImp
struct  MessageTemplateImp_t2751049673  : public MessageTemplatesProvider_t2923081561
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGETEMPLATEIMP_T2751049673_H
#ifndef MESSAGETEMPLATESPROVIDERIMP_T1105777579_H
#define MESSAGETEMPLATESPROVIDERIMP_T1105777579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MessageTemplatesProviderImp
struct  MessageTemplatesProviderImp_t1105777579  : public MessageTemplatesProvider_t2923081561
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGETEMPLATESPROVIDERIMP_T1105777579_H
#ifndef CHECKLISTPROVIERIMP_T3433478069_H
#define CHECKLISTPROVIERIMP_T3433478069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DI.ChecklistProvierImp
struct  ChecklistProvierImp_t3433478069  : public ChecklistProvider_t3393423783
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKLISTPROVIERIMP_T3433478069_H
#ifndef ELEMENTAPPROVEPROVIDEIMP_T3855677316_H
#define ELEMENTAPPROVEPROVIDEIMP_T3855677316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DI.ElementApproveProvideImp
struct  ElementApproveProvideImp_t3855677316  : public ElementApproveProvider_t2738039706
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTAPPROVEPROVIDEIMP_T3855677316_H
#ifndef GROUPIMGMODELPROVIERIMP_T2602305506_H
#define GROUPIMGMODELPROVIERIMP_T2602305506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GroupimgModelProvierImp
struct  GroupimgModelProvierImp_t2602305506  : public GroupingModelProvider_t2551172685
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPIMGMODELPROVIERIMP_T2602305506_H
#ifndef MODELPROVIDERIMP_T3014437250_H
#define MODELPROVIDERIMP_T3014437250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModelProviderImp
struct  ModelProviderImp_t3014437250  : public ModelProvider_t1845401484
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELPROVIDERIMP_T3014437250_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (PopUpStuff_t3426923330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2400[5] = 
{
	PopUpStuff_t3426923330::get_offset_of_title_0(),
	PopUpStuff_t3426923330::get_offset_of_text_1(),
	PopUpStuff_t3426923330::get_offset_of_okAction_2(),
	PopUpStuff_t3426923330::get_offset_of_cancelAction_3(),
	PopUpStuff_t3426923330::get_offset_of_type_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (WarningPopUp_t2981532944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2401[3] = 
{
	WarningPopUp_t2981532944::get_offset_of_okButton_2(),
	WarningPopUp_t2981532944::get_offset_of_titleText_3(),
	WarningPopUp_t2981532944::get_offset_of_infoText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (U3ChandlePopUpU3Ec__AnonStorey0_t3839772636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2402[1] = 
{
	U3ChandlePopUpU3Ec__AnonStorey0_t3839772636::get_offset_of_popUpStuff_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2404[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (PrefabGeneratorImp_t2478736189), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2406[2] = 
{
	PrefabGeneratorImp_t2478736189::get_offset_of__createInterface_0(),
	PrefabGeneratorImp_t2478736189::get_offset_of_pathToUIPrefabs_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (Prefabs_t3645706183)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2407[36] = 
{
	Prefabs_t3645706183::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (ProgressBar_t521705726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2408[2] = 
{
	ProgressBar_t521705726::get_offset_of_rectComponent_2(),
	ProgressBar_t521705726::get_offset_of_rotateSpeed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (CloseRemark_t2638852306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2409[2] = 
{
	CloseRemark_t2638852306::get_offset_of__remarkPanel_2(),
	CloseRemark_t2638852306::get_offset_of__shouldRemoveFiles_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (CloseRemarkImp_t4161074716), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (MessageListBuilderTwoPanels_t2629535697), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (NewRemarkHandlerMobile_t798103230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2412[5] = 
{
	NewRemarkHandlerMobile_t798103230::get_offset_of_communication_2(),
	NewRemarkHandlerMobile_t798103230::get_offset_of__currentCheckListItemFacade_3(),
	NewRemarkHandlerMobile_t798103230::get_offset_of_currentStatusScene_4(),
	NewRemarkHandlerMobile_t798103230::get_offset_of_currentItemButton_5(),
	NewRemarkHandlerMobile_t798103230::get_offset_of_forcedItemStauts_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (U3CinitializeSaveButtonU3Ec__AnonStorey0_t600697625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2413[4] = 
{
	U3CinitializeSaveButtonU3Ec__AnonStorey0_t600697625::get_offset_of_remarkTransform_0(),
	U3CinitializeSaveButtonU3Ec__AnonStorey0_t600697625::get_offset_of_depedencyProvider_1(),
	U3CinitializeSaveButtonU3Ec__AnonStorey0_t600697625::get_offset_of_inputField_2(),
	U3CinitializeSaveButtonU3Ec__AnonStorey0_t600697625::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (U3CinitializCatchPhotoU3Ec__AnonStorey1_t1231633819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2414[4] = 
{
	U3CinitializCatchPhotoU3Ec__AnonStorey1_t1231633819::get_offset_of_holder_0(),
	U3CinitializCatchPhotoU3Ec__AnonStorey1_t1231633819::get_offset_of_takePhoto_1(),
	U3CinitializCatchPhotoU3Ec__AnonStorey1_t1231633819::get_offset_of_takeAudio_2(),
	U3CinitializCatchPhotoU3Ec__AnonStorey1_t1231633819::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (RemarkHandler_t1175353018), -1, sizeof(RemarkHandler_t1175353018_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2415[1] = 
{
	RemarkHandler_t1175353018_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (RemarkHandlerMobile_t1067069532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2416[4] = 
{
	RemarkHandlerMobile_t1067069532::get_offset_of_communication_2(),
	RemarkHandlerMobile_t1067069532::get_offset_of__currentCheckListItemFacade_3(),
	RemarkHandlerMobile_t1067069532::get_offset_of_currentStatusScene_4(),
	RemarkHandlerMobile_t1067069532::get_offset_of_currentItemButton_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (U3CinitializeSaveButtonU3Ec__AnonStorey0_t3054089613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2417[2] = 
{
	U3CinitializeSaveButtonU3Ec__AnonStorey0_t3054089613::get_offset_of_depedencyProvider_0(),
	U3CinitializeSaveButtonU3Ec__AnonStorey0_t3054089613::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (U3CinitializCatchPhotoU3Ec__AnonStorey1_t471598695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2418[3] = 
{
	U3CinitializCatchPhotoU3Ec__AnonStorey1_t471598695::get_offset_of_labelPhoto_0(),
	U3CinitializCatchPhotoU3Ec__AnonStorey1_t471598695::get_offset_of_holder_1(),
	U3CinitializCatchPhotoU3Ec__AnonStorey1_t471598695::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (RemarkHandlerMobileTwoPanels_t1827359049), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2419[5] = 
{
	RemarkHandlerMobileTwoPanels_t1827359049::get_offset_of_communication_2(),
	RemarkHandlerMobileTwoPanels_t1827359049::get_offset_of__currentCheckListItemFacade_3(),
	RemarkHandlerMobileTwoPanels_t1827359049::get_offset_of_currentStatusScene_4(),
	RemarkHandlerMobileTwoPanels_t1827359049::get_offset_of_currentItemButton_5(),
	RemarkHandlerMobileTwoPanels_t1827359049::get_offset_of_forcedItemStauts_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (U3CinitializeSaveButtonU3Ec__AnonStorey0_t1415142742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2420[4] = 
{
	U3CinitializeSaveButtonU3Ec__AnonStorey0_t1415142742::get_offset_of_remarkTransform_0(),
	U3CinitializeSaveButtonU3Ec__AnonStorey0_t1415142742::get_offset_of_depedencyProvider_1(),
	U3CinitializeSaveButtonU3Ec__AnonStorey0_t1415142742::get_offset_of_inputField_2(),
	U3CinitializeSaveButtonU3Ec__AnonStorey0_t1415142742::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (U3CinitializeCatchPhotoU3Ec__AnonStorey1_t3556158843), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2421[4] = 
{
	U3CinitializeCatchPhotoU3Ec__AnonStorey1_t3556158843::get_offset_of_holder_0(),
	U3CinitializeCatchPhotoU3Ec__AnonStorey1_t3556158843::get_offset_of_takePhoto_1(),
	U3CinitializeCatchPhotoU3Ec__AnonStorey1_t3556158843::get_offset_of_takeAudio_2(),
	U3CinitializeCatchPhotoU3Ec__AnonStorey1_t3556158843::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (SaveRemark_t4091223213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2422[10] = 
{
	SaveRemark_t4091223213::get_offset_of__messagesFacade_2(),
	SaveRemark_t4091223213::get_offset_of__cameraPositionProvider_3(),
	SaveRemark_t4091223213::get_offset_of__inputField_4(),
	SaveRemark_t4091223213::get_offset_of__element3DFacade_5(),
	SaveRemark_t4091223213::get_offset_of__currentElement_6(),
	SaveRemark_t4091223213::get_offset_of__currentCheckListItemFacade_7(),
	SaveRemark_t4091223213::get_offset_of__pathHolder_8(),
	SaveRemark_t4091223213::get_offset_of_started_9(),
	SaveRemark_t4091223213::get_offset_of_onSaveAction_10(),
	SaveRemark_t4091223213::get_offset_of_currentItemStatus_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (SaveRemarkImp_t1879814373), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (RotateButton_t1854395031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2424[4] = 
{
	RotateButton_t1854395031::get_offset_of_rotation_2(),
	RotateButton_t1854395031::get_offset_of_value_3(),
	RotateButton_t1854395031::get_offset_of_model_4(),
	RotateButton_t1854395031::get_offset_of_hold_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (TextColor_t3858664002), -1, sizeof(TextColor_t3858664002_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2425[8] = 
{
	TextColor_t3858664002_StaticFields::get_offset_of_textPrimaryColor_0(),
	TextColor_t3858664002_StaticFields::get_offset_of_textSecondaryColor_1(),
	TextColor_t3858664002_StaticFields::get_offset_of_textAccentColor_2(),
	TextColor_t3858664002_StaticFields::get_offset_of_textTetriaryColor_3(),
	TextColor_t3858664002_StaticFields::get_offset_of_testTextPrimaryColor_4(),
	TextColor_t3858664002_StaticFields::get_offset_of_testTextSecondaryColor_5(),
	TextColor_t3858664002_StaticFields::get_offset_of_testTextAccentColor_6(),
	TextColor_t3858664002_StaticFields::get_offset_of_testTextTetriaryColor_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (DoAsyncImp_t1177308585), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (FileReaderImpl_t2415093035), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (ReturnAssetBundleForElement_t2016413217), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (WebRequest_t921143439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2432[1] = 
{
	WebRequest_t921143439::get_offset_of__dataNetworkInfoProvider_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (HeaderClass_t2331629453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2434[2] = 
{
	HeaderClass_t2331629453::get_offset_of_header_0(),
	HeaderClass_t2331629453::get_offset_of_headerValue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (WebRequestType_t625788535)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2435[5] = 
{
	WebRequestType_t625788535::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (WebRequestBuilder_t3983605310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2436[4] = 
{
	WebRequestBuilder_t3983605310::get_offset_of_webRequestType_0(),
	WebRequestBuilder_t3983605310::get_offset_of__dataNetworkInfoProvider_1(),
	WebRequestBuilder_t3983605310::get_offset_of_url_2(),
	WebRequestBuilder_t3983605310::get_offset_of_data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (U3CretrunUnityWebRequestU3Ec__AnonStorey0_t3849400577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2437[1] = 
{
	U3CretrunUnityWebRequestU3Ec__AnonStorey0_t3849400577::get_offset_of_request_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (NoUrlSetted_t1586831947), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (NoHttpMethodSetException_t721769271), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (Dispatcher_t3198616969), -1, sizeof(Dispatcher_t3198616969_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2440[2] = 
{
	Dispatcher_t3198616969_StaticFields::get_offset_of__queue_2(),
	Dispatcher_t3198616969_StaticFields::get_offset_of__lock_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (ContentReadAction_t1342170461)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2442[4] = 
{
	ContentReadAction_t1342170461::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (HttpAction_t943117648)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2443[6] = 
{
	HttpAction_t943117648::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (HttpCompletionOption_t1897188955)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2444[3] = 
{
	HttpCompletionOption_t1897188955::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (ExampleSceneManagerController_t4160392891), -1, sizeof(ExampleSceneManagerController_t4160392891_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2445[10] = 
{
	ExampleSceneManagerController_t4160392891::get_offset_of_LeftText_2(),
	ExampleSceneManagerController_t4160392891::get_offset_of_RightText_3(),
	ExampleSceneManagerController_t4160392891::get_offset_of_ProgressSlider_4(),
	ExampleSceneManagerController_t4160392891_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
	ExampleSceneManagerController_t4160392891_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_6(),
	ExampleSceneManagerController_t4160392891_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_7(),
	ExampleSceneManagerController_t4160392891_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_8(),
	ExampleSceneManagerController_t4160392891_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_9(),
	ExampleSceneManagerController_t4160392891_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_10(),
	ExampleSceneManagerController_t4160392891_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (HttpBase_t4034084527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2446[3] = 
{
	HttpBase_t4034084527::get_offset_of__request_0(),
	HttpBase_t4034084527::get_offset_of__response_1(),
	HttpBase_t4034084527::get_offset_of__dispatcher_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (U3CRaiseUploadStatusCallbackU3Ec__AnonStorey0_t3210131969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2447[4] = 
{
	U3CRaiseUploadStatusCallbackU3Ec__AnonStorey0_t3210131969::get_offset_of_uploadStatusCallback_0(),
	U3CRaiseUploadStatusCallbackU3Ec__AnonStorey0_t3210131969::get_offset_of_contentLength_1(),
	U3CRaiseUploadStatusCallbackU3Ec__AnonStorey0_t3210131969::get_offset_of_contentUploadedThisRound_2(),
	U3CRaiseUploadStatusCallbackU3Ec__AnonStorey0_t3210131969::get_offset_of_totalContentUploaded_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2448[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2449[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (HttpDelete_t3529924879), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (HttpGet_t2212980058), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (HttpPatch_t3849283608), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (HttpPost_t2219433260), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (HttpPut_t653802679), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (HttpClient_t2113786063), -1, sizeof(HttpClient_t2113786063_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2455[19] = 
{
	0,
	0,
	0,
	0,
	HttpClient_t2113786063::get_offset_of_U3CDownloadBlockSizeU3Ek__BackingField_4(),
	HttpClient_t2113786063::get_offset_of_U3CUploadBlockSizeU3Ek__BackingField_5(),
	HttpClient_t2113786063::get_offset_of_U3CTimeoutU3Ek__BackingField_6(),
	HttpClient_t2113786063::get_offset_of_U3CReadWriteTimeoutU3Ek__BackingField_7(),
	HttpClient_t2113786063::get_offset_of_U3CCacheU3Ek__BackingField_8(),
	HttpClient_t2113786063::get_offset_of_U3CCertificatesU3Ek__BackingField_9(),
	HttpClient_t2113786063::get_offset_of_U3CCookiesU3Ek__BackingField_10(),
	HttpClient_t2113786063::get_offset_of_U3CCredentialsU3Ek__BackingField_11(),
	HttpClient_t2113786063::get_offset_of_U3CKeepAliveU3Ek__BackingField_12(),
	HttpClient_t2113786063::get_offset_of_U3CHeadersU3Ek__BackingField_13(),
	HttpClient_t2113786063::get_offset_of_U3CCustomHeadersU3Ek__BackingField_14(),
	HttpClient_t2113786063::get_offset_of_U3CProxyU3Ek__BackingField_15(),
	HttpClient_t2113786063::get_offset_of__requests_16(),
	HttpClient_t2113786063::get_offset_of__lock_17(),
	HttpClient_t2113786063_StaticFields::get_offset_of__dispatcher_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (U3CDeleteU3Ec__AnonStorey0_t1672730659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2456[3] = 
{
	U3CDeleteU3Ec__AnonStorey0_t1672730659::get_offset_of_uri_0(),
	U3CDeleteU3Ec__AnonStorey0_t1672730659::get_offset_of_responseCallback_1(),
	U3CDeleteU3Ec__AnonStorey0_t1672730659::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (U3CDeleteU3Ec__AnonStorey1_t1672730660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2457[4] = 
{
	U3CDeleteU3Ec__AnonStorey1_t1672730660::get_offset_of_uri_0(),
	U3CDeleteU3Ec__AnonStorey1_t1672730660::get_offset_of_completionOption_1(),
	U3CDeleteU3Ec__AnonStorey1_t1672730660::get_offset_of_responseCallback_2(),
	U3CDeleteU3Ec__AnonStorey1_t1672730660::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (U3CGetStringU3Ec__AnonStorey2_t2072622603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2458[3] = 
{
	U3CGetStringU3Ec__AnonStorey2_t2072622603::get_offset_of_uri_0(),
	U3CGetStringU3Ec__AnonStorey2_t2072622603::get_offset_of_responseCallback_1(),
	U3CGetStringU3Ec__AnonStorey2_t2072622603::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (U3CGetByteArrayU3Ec__AnonStorey3_t424213474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2459[4] = 
{
	U3CGetByteArrayU3Ec__AnonStorey3_t424213474::get_offset_of_uri_0(),
	U3CGetByteArrayU3Ec__AnonStorey3_t424213474::get_offset_of_completionOption_1(),
	U3CGetByteArrayU3Ec__AnonStorey3_t424213474::get_offset_of_responseCallback_2(),
	U3CGetByteArrayU3Ec__AnonStorey3_t424213474::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (U3CPatchU3Ec__AnonStorey4_t2473826542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2460[5] = 
{
	U3CPatchU3Ec__AnonStorey4_t2473826542::get_offset_of_uri_0(),
	U3CPatchU3Ec__AnonStorey4_t2473826542::get_offset_of_content_1(),
	U3CPatchU3Ec__AnonStorey4_t2473826542::get_offset_of_responseCallback_2(),
	U3CPatchU3Ec__AnonStorey4_t2473826542::get_offset_of_uploadStatusCallback_3(),
	U3CPatchU3Ec__AnonStorey4_t2473826542::get_offset_of_U24this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (U3CPatchU3Ec__AnonStorey5_t907742601), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2461[6] = 
{
	U3CPatchU3Ec__AnonStorey5_t907742601::get_offset_of_uri_0(),
	U3CPatchU3Ec__AnonStorey5_t907742601::get_offset_of_content_1(),
	U3CPatchU3Ec__AnonStorey5_t907742601::get_offset_of_completionOption_2(),
	U3CPatchU3Ec__AnonStorey5_t907742601::get_offset_of_responseCallback_3(),
	U3CPatchU3Ec__AnonStorey5_t907742601::get_offset_of_uploadStatusCallback_4(),
	U3CPatchU3Ec__AnonStorey5_t907742601::get_offset_of_U24this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (U3CPostU3Ec__AnonStorey6_t1971601810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2462[5] = 
{
	U3CPostU3Ec__AnonStorey6_t1971601810::get_offset_of_uri_0(),
	U3CPostU3Ec__AnonStorey6_t1971601810::get_offset_of_content_1(),
	U3CPostU3Ec__AnonStorey6_t1971601810::get_offset_of_responseCallback_2(),
	U3CPostU3Ec__AnonStorey6_t1971601810::get_offset_of_uploadStatusCallback_3(),
	U3CPostU3Ec__AnonStorey6_t1971601810::get_offset_of_U24this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (U3CPostU3Ec__AnonStorey7_t1971601809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2463[6] = 
{
	U3CPostU3Ec__AnonStorey7_t1971601809::get_offset_of_uri_0(),
	U3CPostU3Ec__AnonStorey7_t1971601809::get_offset_of_content_1(),
	U3CPostU3Ec__AnonStorey7_t1971601809::get_offset_of_completionOption_2(),
	U3CPostU3Ec__AnonStorey7_t1971601809::get_offset_of_responseCallback_3(),
	U3CPostU3Ec__AnonStorey7_t1971601809::get_offset_of_uploadStatusCallback_4(),
	U3CPostU3Ec__AnonStorey7_t1971601809::get_offset_of_U24this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (U3CPutU3Ec__AnonStorey8_t4219266303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2464[5] = 
{
	U3CPutU3Ec__AnonStorey8_t4219266303::get_offset_of_uri_0(),
	U3CPutU3Ec__AnonStorey8_t4219266303::get_offset_of_content_1(),
	U3CPutU3Ec__AnonStorey8_t4219266303::get_offset_of_responseCallback_2(),
	U3CPutU3Ec__AnonStorey8_t4219266303::get_offset_of_uploadStatusCallback_3(),
	U3CPutU3Ec__AnonStorey8_t4219266303::get_offset_of_U24this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { sizeof (U3CPutU3Ec__AnonStorey9_t1490382948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2465[6] = 
{
	U3CPutU3Ec__AnonStorey9_t1490382948::get_offset_of_uri_0(),
	U3CPutU3Ec__AnonStorey9_t1490382948::get_offset_of_content_1(),
	U3CPutU3Ec__AnonStorey9_t1490382948::get_offset_of_completionOption_2(),
	U3CPutU3Ec__AnonStorey9_t1490382948::get_offset_of_responseCallback_3(),
	U3CPutU3Ec__AnonStorey9_t1490382948::get_offset_of_uploadStatusCallback_4(),
	U3CPutU3Ec__AnonStorey9_t1490382948::get_offset_of_U24this_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2466[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (ByteArrayContent_t1139747420), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2467[2] = 
{
	ByteArrayContent_t1139747420::get_offset_of__content_0(),
	ByteArrayContent_t1139747420::get_offset_of_U3CHeadersU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (FormUrlEncodedContent_t50233144), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2468[3] = 
{
	FormUrlEncodedContent_t50233144::get_offset_of__nameValueCollection_0(),
	FormUrlEncodedContent_t50233144::get_offset_of__serialisedContent_1(),
	FormUrlEncodedContent_t50233144::get_offset_of_U3CHeadersU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (MultipartContent_t115045161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2470[8] = 
{
	0,
	MultipartContent_t115045161::get_offset_of__content_1(),
	MultipartContent_t115045161::get_offset_of__boundary_2(),
	MultipartContent_t115045161::get_offset_of__contentLength_3(),
	MultipartContent_t115045161::get_offset_of_U3CBoundaryStartBytesU3Ek__BackingField_4(),
	MultipartContent_t115045161::get_offset_of_U3CBoundaryEndBytesU3Ek__BackingField_5(),
	MultipartContent_t115045161::get_offset_of_U3CCRLFBytesU3Ek__BackingField_6(),
	MultipartContent_t115045161::get_offset_of_U3CHeadersU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (MultipartFormDataContent_t1375345943), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (StreamContent_t2445532195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2472[2] = 
{
	StreamContent_t2445532195::get_offset_of__stream_0(),
	StreamContent_t2445532195::get_offset_of_U3CHeadersU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { sizeof (StringContent_t615964002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2473[5] = 
{
	0,
	StringContent_t615964002::get_offset_of__content_1(),
	StringContent_t615964002::get_offset_of__encoding_2(),
	StringContent_t615964002::get_offset_of__serialisedContent_3(),
	StringContent_t615964002::get_offset_of_U3CHeadersU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2474[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { sizeof (UploadStatusMessage_t4187164922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2475[3] = 
{
	UploadStatusMessage_t4187164922::get_offset_of_U3CContentLengthU3Ek__BackingField_0(),
	UploadStatusMessage_t4187164922::get_offset_of_U3CTotalContentUploadedU3Ek__BackingField_1(),
	UploadStatusMessage_t4187164922::get_offset_of_U3CContentUploadedThisRoundU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (DeleteFromList_t465902211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2476[1] = 
{
	DeleteFromList_t465902211::get_offset_of_yourButton_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (ChecklistProvierImp_t3433478069), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (Element3dProvierImp_t2228562702), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (ElementApproveProvideImp_t3855677316), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (ElementHandlerProviderImp_t2037353801), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (ElementProvierImp_t3781256465), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { sizeof (GroupimgModelProvierImp_t2602305506), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (MessagesProviderImp_t2278660791), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (MessageTemplatesProviderImp_t1105777579), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (ModelProviderImp_t3014437250), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (PrefabsProviderImp_t1296656228), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (DrawerHandler_t321192673), -1, sizeof(DrawerHandler_t321192673_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2487[16] = 
{
	DrawerHandler_t321192673::get_offset_of_buttonContainer_2(),
	DrawerHandler_t321192673::get_offset_of_buttonPrefab_3(),
	DrawerHandler_t321192673::get_offset_of_buttonRemark_4(),
	DrawerHandler_t321192673::get_offset_of_buttonCheckList_5(),
	DrawerHandler_t321192673::get_offset_of_buttonElementColors_6(),
	DrawerHandler_t321192673::get_offset_of_buttons_7(),
	DrawerHandler_t321192673::get_offset_of_buttomMessages_8(),
	DrawerHandler_t321192673::get_offset_of_pathToUIPrefabDrawerButtons_9(),
	DrawerHandler_t321192673::get_offset_of_listContainer_10(),
	DrawerHandler_t321192673::get_offset_of_drawerScene_11(),
	DrawerHandler_t321192673::get_offset_of__prefabGenerator_12(),
	DrawerHandler_t321192673::get_offset_of_pathToUIDrawerButtonGraphics_13(),
	DrawerHandler_t321192673::get_offset_of_buttonTop_14(),
	DrawerHandler_t321192673::get_offset_of_buttonMid_15(),
	DrawerHandler_t321192673::get_offset_of_buttonBottom_16(),
	DrawerHandler_t321192673_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (U3CInstantiateDrawerButtonU3Ec__AnonStorey0_t2380289698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2488[2] = 
{
	U3CInstantiateDrawerButtonU3Ec__AnonStorey0_t2380289698::get_offset_of_objectInScene_0(),
	U3CInstantiateDrawerButtonU3Ec__AnonStorey0_t2380289698::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (U3CInstantiateDrawerButtonNewU3Ec__AnonStorey1_t2226637809), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2489[2] = 
{
	U3CInstantiateDrawerButtonNewU3Ec__AnonStorey1_t2226637809::get_offset_of_objectInScene_0(),
	U3CInstantiateDrawerButtonNewU3Ec__AnonStorey1_t2226637809::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (U3CInstantiateRemarkButtonU3Ec__AnonStorey2_t2468962027), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2490[2] = 
{
	U3CInstantiateRemarkButtonU3Ec__AnonStorey2_t2468962027::get_offset_of_objectInScene_0(),
	U3CInstantiateRemarkButtonU3Ec__AnonStorey2_t2468962027::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (DrawerScene_t240065691), -1, sizeof(DrawerScene_t240065691_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2491[22] = 
{
	DrawerScene_t240065691::get_offset_of_drawerHandler_2(),
	DrawerScene_t240065691::get_offset_of__prefabProvider_3(),
	DrawerScene_t240065691::get_offset_of__groupingModelProvider_4(),
	DrawerScene_t240065691::get_offset_of__messageTemplatesProvider_5(),
	DrawerScene_t240065691::get_offset_of__messageProvider_6(),
	DrawerScene_t240065691::get_offset_of_depedencyProvider_7(),
	DrawerScene_t240065691::get_offset_of_pathToUIPrefabs_8(),
	DrawerScene_t240065691::get_offset_of_elementColorsScene_9(),
	DrawerScene_t240065691::get_offset_of_remarkScene_10(),
	DrawerScene_t240065691::get_offset_of_checklistScene_11(),
	DrawerScene_t240065691::get_offset_of_messagelistScene_12(),
	DrawerScene_t240065691::get_offset_of_modelelementScene_13(),
	DrawerScene_t240065691::get_offset_of_currentStatusScene_14(),
	DrawerScene_t240065691::get_offset_of_photoPopupScene_15(),
	DrawerScene_t240065691::get_offset_of_listContainer_16(),
	DrawerScene_t240065691::get_offset_of_canvasContainer_17(),
	DrawerScene_t240065691::get_offset_of__prefabGenerator_18(),
	DrawerScene_t240065691::get_offset_of_elementLoaded_19(),
	DrawerScene_t240065691::get_offset_of_footer_20(),
	DrawerScene_t240065691::get_offset_of_comm_21(),
	DrawerScene_t240065691::get_offset_of_popUpShower_22(),
	DrawerScene_t240065691_StaticFields::get_offset_of_languageStrings_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (MoveObjectHandler_t509454862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2492[5] = 
{
	MoveObjectHandler_t509454862::get_offset_of_single_2(),
	MoveObjectHandler_t509454862::get_offset_of_clock_3(),
	MoveObjectHandler_t509454862::get_offset_of_antiClock_4(),
	MoveObjectHandler_t509454862::get_offset_of_up_5(),
	MoveObjectHandler_t509454862::get_offset_of_down_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { sizeof (MessageObserver_t1722898529), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (MessageTemplateImp_t2751049673), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (DrawerAnimationButton_t1773246733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2495[2] = 
{
	DrawerAnimationButton_t1773246733::get_offset_of_anim_2(),
	DrawerAnimationButton_t1773246733::get_offset_of_active_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (GroupinColorMobileImp_t1767066759), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (GroupinColorMobileImpNew_t3384506007), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (MessageListBuilderImp_t1204586794), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { sizeof (Benchmark01_t2768175604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2499[10] = 
{
	Benchmark01_t2768175604::get_offset_of_BenchmarkType_2(),
	Benchmark01_t2768175604::get_offset_of_TMProFont_3(),
	Benchmark01_t2768175604::get_offset_of_TextMeshFont_4(),
	Benchmark01_t2768175604::get_offset_of_m_textMeshPro_5(),
	Benchmark01_t2768175604::get_offset_of_m_textContainer_6(),
	Benchmark01_t2768175604::get_offset_of_m_textMesh_7(),
	0,
	0,
	Benchmark01_t2768175604::get_offset_of_m_material01_10(),
	Benchmark01_t2768175604::get_offset_of_m_material02_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
