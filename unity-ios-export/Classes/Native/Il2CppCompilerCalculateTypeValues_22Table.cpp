﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// Library.Code.Domain.Entities.Element
struct Element_t2276588008;
// Library.Code.Facades.Callback
struct Callback_t1820017589;
// Library.Code.Facades.Elements.ElementFacadeImpl
struct ElementFacadeImpl_t1898847230;
// System.Collections.Generic.List`1<Library.Code.Domain.Entities.ModelGroup>
struct List_1_t2603308778;
// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.ModelGroup>>
struct ResponseWrapper_1_t4098269842;
// System.Collections.Generic.Dictionary`2<Library.Code.Domain.Entities.ModelGroup,System.Collections.Generic.List`1<UnityEngine.GameObject>>
struct Dictionary_2_t4078280220;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// Library.Code.Facades.ModelGrouping.ModelGroupDtoFactory
struct ModelGroupDtoFactory_t474877319;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Library.Code.Facades.ModelGrouping.ModelGroupDtoFactory/<createDtos>c__AnonStorey0
struct U3CcreateDtosU3Ec__AnonStorey0_t827342538;
// System.Collections.Generic.List`1<Library.Code.Facades.Observer`1<Library.Code.Domain.Dtos.ModelGroupDto>>
struct List_1_t3710614623;
// Library.Code.Networking.GroupModel.GroupingModelApi
struct GroupingModelApi_t3365654244;
// Library.Code.Utils.Async.DoAsync
struct DoAsync_t850966475;
// System.Collections.Generic.List`1<Library.Code.Domain.Dtos.ModelGroupDto>
struct List_1_t1177375679;
// Library.Code.Utils.Platforms.ReturnForPlatform`2<System.String,Library.Code.Domain.Entities.Element>
struct ReturnForPlatform_2_t501990968;
// UnityEngine.WWW
struct WWW_t2919945039;
// Library.Code.Networking.ResponseWrapper`1<Library.Code.Domain.Dtos.ElementWithAssetBundle>
struct ResponseWrapper_1_t3479396734;
// Library.Code.Networking._3dElement.Download3dElementImp
struct Download3dElementImp_t2560998171;
// Library.Code.Domain.Entities.Model
struct Model_t2163629551;
// Library.Code.Facades.Models.ModelFacadeImpl
struct ModelFacadeImpl_t3221237801;
// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model>
struct List_1_t1532750683;
// Library.Code.Persistance.FileJsonPersistance`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model>>
struct FileJsonPersistance_1_t649922390;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model>>
struct UnityAction_1_t2899336434;
// Library.Code.Facades.Models.ModelsPersistanceImpl
struct ModelsPersistanceImpl_t1155557687;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// Library.Code.Parsers.JsonParser`1<Library.Code.Domain.Entities.Checklist.ItemStatus>
struct JsonParser_1_t3602540230;
// Library.Code.WebRequest.WebRequestBuilder
struct WebRequestBuilder_t3983605310;
// Library.Code.Parsers.JsonParser`1<Library.Code.Domain.Entities.ApproveElementWrapper>
struct JsonParser_1_t3346973389;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t254341728;
// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.ApproveElementResponse>>
struct ResponseWrapper_1_t3629854712;
// Library.Code.Networking.Elements.ElementStatusApiImpl
struct ElementStatusApiImpl_t2005129180;
// Library.Code.Domain.Entities.ApproveElementResponse
struct ApproveElementResponse_t2765772516;
// Library.Code.Networking.ResponseWrapper`1<Library.Code.Domain.Entities.Element>
struct ResponseWrapper_1_t3771549072;
// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Element>>
struct ResponseWrapper_1_t3140670204;
// Library.Code.Networking.Elements.ElementApiImp
struct ElementApiImp_t2310790088;
// Library.Code.Domain.Entities.Checklist.ItemStatus
struct ItemStatus_t1378751697;
// UnityEngine.Events.UnityAction`1<Library.Code.Networking.FailReason>
struct UnityAction_1_t1842893265;
// Library.Code.Networking.Checklist.ChangeCheckListItemStatusApiImp
struct ChangeCheckListItemStatusApiImp_t3896993653;
// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Checklist.CheckList>
struct List_1_t3501169210;
// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Checklist.CheckList>>
struct ResponseWrapper_1_t701162978;
// Library.Code.Parsers.JsonParser`1<Library.Code.Domain.Entities.Checklist.CheckListResponse>
struct JsonParser_1_t1585561634;
// Library.Code.Networking.Checklist.LoadChecklistForElementApiImp
struct LoadChecklistForElementApiImp_t310732585;
// Library.Code.Parsers.JsonParser`1<Library.Code.Domain.Entities.ElementsEmbedded>
struct JsonParser_1_t3978576236;
// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Element>
struct List_1_t1645709140;
// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Message>
struct List_1_t583975089;
// Library.Code.Networking.Files.SendFileApi
struct SendFileApi_t2830678900;
// Library.Code.Utils.FileReader.FileReader
struct FileReader_t575612575;
// Library.Code.Networking.ResponseWrapper`1<System.Int64>
struct ResponseWrapper_1_t2404039101;
// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Message>>
struct ResponseWrapper_1_t2078936153;
// Library.Code.Persistance.FileJsonPersistance`1<Library.Code.Domain.Dtos.MessageListWrapper>
struct FileJsonPersistance_1_t961952247;
// Library.Code.Persistance.FileJsonPersistance`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Element>>
struct FileJsonPersistance_1_t762880847;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Element>>
struct UnityAction_1_t3012294891;
// Library.Code.Facades.Elements.ElementPersistanceImpl
struct ElementPersistanceImpl_t1641319881;
// Library.Code.Domain.Dtos.MessageListWrapper
struct MessageListWrapper_t1844780540;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Message>>
struct UnityAction_1_t1950560840;
// Library.Code.Facades.Messages.MessagePersistance
struct MessagePersistance_t1067416742;
// Library.Code.Networking.Message.GroupCreatorApi
struct GroupCreatorApi_t1994779875;
// Library.Code.Networking.Message.SendBatchMessageApi
struct SendBatchMessageApi_t4086852703;
// Library.Code.Facades.Messages.MessagesFacade
struct MessagesFacade_t3479558102;
// Library.Code.Facades.Files.SendFileFacade
struct SendFileFacade_t1187763752;
// System.Collections.Generic.List`1<Library.Code.Domain.Entities.MessageTemplate>
struct List_1_t2162674683;
// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.MessageTemplate>>
struct ResponseWrapper_1_t3657635747;
// System.Collections.Generic.List`1<Library.Code.Facades.Observer`1<Library.Code.Domain.Entities.MessageTemplate>>
struct List_1_t400946331;
// Library.Code.Networking.MessageTemplate.MessageTemplatesApi
struct MessageTemplatesApi_t583105650;
// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model>>
struct ResponseWrapper_1_t3027711747;
// Library.Code.Facades.PersistanceFacade`1<Library.Code.Domain.Entities.Model>
struct PersistanceFacade_1_t473761224;
// System.Collections.Generic.List`1<Library.Code.Facades.Observer`1<Library.Code.Domain.Entities.Model>>
struct List_1_t4065989627;
// Library.Code.Networking.Models.ModelApi
struct ModelApi_t498632979;
// Library.Code.Facades.Messages.MessagesFacadeImpl
struct MessagesFacadeImpl_t154288538;
// Library.Code.Domain.Entities.Message
struct Message_t1214853957;
// System.Collections.Generic.List`1<Library.Code.Facades.Observer`1<Library.Code.Domain.Entities.Message>>
struct List_1_t3117214033;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Void
struct Void_t1841601450;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// Library.Code.Facades.PathHolder.PathHolderImp
struct PathHolderImp_t857450797;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CREMOVEU3EC__ANONSTOREY1_T2493958417_H
#define U3CREMOVEU3EC__ANONSTOREY1_T2493958417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Elements.ElementFacadeImpl/<remove>c__AnonStorey1
struct  U3CremoveU3Ec__AnonStorey1_t2493958417  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Element Library.Code.Facades.Elements.ElementFacadeImpl/<remove>c__AnonStorey1::models
	Element_t2276588008 * ___models_0;
	// Library.Code.Facades.Callback Library.Code.Facades.Elements.ElementFacadeImpl/<remove>c__AnonStorey1::persistCallback
	RuntimeObject* ___persistCallback_1;
	// Library.Code.Facades.Elements.ElementFacadeImpl Library.Code.Facades.Elements.ElementFacadeImpl/<remove>c__AnonStorey1::$this
	ElementFacadeImpl_t1898847230 * ___U24this_2;

public:
	inline static int32_t get_offset_of_models_0() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__AnonStorey1_t2493958417, ___models_0)); }
	inline Element_t2276588008 * get_models_0() const { return ___models_0; }
	inline Element_t2276588008 ** get_address_of_models_0() { return &___models_0; }
	inline void set_models_0(Element_t2276588008 * value)
	{
		___models_0 = value;
		Il2CppCodeGenWriteBarrier((&___models_0), value);
	}

	inline static int32_t get_offset_of_persistCallback_1() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__AnonStorey1_t2493958417, ___persistCallback_1)); }
	inline RuntimeObject* get_persistCallback_1() const { return ___persistCallback_1; }
	inline RuntimeObject** get_address_of_persistCallback_1() { return &___persistCallback_1; }
	inline void set_persistCallback_1(RuntimeObject* value)
	{
		___persistCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___persistCallback_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__AnonStorey1_t2493958417, ___U24this_2)); }
	inline ElementFacadeImpl_t1898847230 * get_U24this_2() const { return ___U24this_2; }
	inline ElementFacadeImpl_t1898847230 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ElementFacadeImpl_t1898847230 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVEU3EC__ANONSTOREY1_T2493958417_H
#ifndef U3CLOADMODELGROUPINGDATAU3EC__ITERATOR0_T1419613339_H
#define U3CLOADMODELGROUPINGDATAU3EC__ITERATOR0_T1419613339_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.ModelGrouping.FakeGroupinModelApi/<loadModelGroupingData>c__Iterator0
struct  U3CloadModelGroupingDataU3Ec__Iterator0_t1419613339  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.ModelGroup> Library.Code.Facades.ModelGrouping.FakeGroupinModelApi/<loadModelGroupingData>c__Iterator0::<list>__0
	List_1_t2603308778 * ___U3ClistU3E__0_0;
	// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.ModelGroup>> Library.Code.Facades.ModelGrouping.FakeGroupinModelApi/<loadModelGroupingData>c__Iterator0::reposneWrapper
	RuntimeObject* ___reposneWrapper_1;
	// System.Object Library.Code.Facades.ModelGrouping.FakeGroupinModelApi/<loadModelGroupingData>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Library.Code.Facades.ModelGrouping.FakeGroupinModelApi/<loadModelGroupingData>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Library.Code.Facades.ModelGrouping.FakeGroupinModelApi/<loadModelGroupingData>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3ClistU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadModelGroupingDataU3Ec__Iterator0_t1419613339, ___U3ClistU3E__0_0)); }
	inline List_1_t2603308778 * get_U3ClistU3E__0_0() const { return ___U3ClistU3E__0_0; }
	inline List_1_t2603308778 ** get_address_of_U3ClistU3E__0_0() { return &___U3ClistU3E__0_0; }
	inline void set_U3ClistU3E__0_0(List_1_t2603308778 * value)
	{
		___U3ClistU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClistU3E__0_0), value);
	}

	inline static int32_t get_offset_of_reposneWrapper_1() { return static_cast<int32_t>(offsetof(U3CloadModelGroupingDataU3Ec__Iterator0_t1419613339, ___reposneWrapper_1)); }
	inline RuntimeObject* get_reposneWrapper_1() const { return ___reposneWrapper_1; }
	inline RuntimeObject** get_address_of_reposneWrapper_1() { return &___reposneWrapper_1; }
	inline void set_reposneWrapper_1(RuntimeObject* value)
	{
		___reposneWrapper_1 = value;
		Il2CppCodeGenWriteBarrier((&___reposneWrapper_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CloadModelGroupingDataU3Ec__Iterator0_t1419613339, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CloadModelGroupingDataU3Ec__Iterator0_t1419613339, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CloadModelGroupingDataU3Ec__Iterator0_t1419613339, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADMODELGROUPINGDATAU3EC__ITERATOR0_T1419613339_H
#ifndef MODELGROUPDTOFACTORY_T474877319_H
#define MODELGROUPDTOFACTORY_T474877319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.ModelGrouping.ModelGroupDtoFactory
struct  ModelGroupDtoFactory_t474877319  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELGROUPDTOFACTORY_T474877319_H
#ifndef U3CCREATEDTOSU3EC__ANONSTOREY0_T827342538_H
#define U3CCREATEDTOSU3EC__ANONSTOREY0_T827342538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.ModelGrouping.ModelGroupDtoFactory/<createDtos>c__AnonStorey0
struct  U3CcreateDtosU3Ec__AnonStorey0_t827342538  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.ModelGroup> Library.Code.Facades.ModelGrouping.ModelGroupDtoFactory/<createDtos>c__AnonStorey0::groups
	List_1_t2603308778 * ___groups_0;
	// System.Collections.Generic.Dictionary`2<Library.Code.Domain.Entities.ModelGroup,System.Collections.Generic.List`1<UnityEngine.GameObject>> Library.Code.Facades.ModelGrouping.ModelGroupDtoFactory/<createDtos>c__AnonStorey0::map
	Dictionary_2_t4078280220 * ___map_1;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Library.Code.Facades.ModelGrouping.ModelGroupDtoFactory/<createDtos>c__AnonStorey0::modelsCopy
	List_1_t1125654279 * ___modelsCopy_2;
	// Library.Code.Facades.ModelGrouping.ModelGroupDtoFactory Library.Code.Facades.ModelGrouping.ModelGroupDtoFactory/<createDtos>c__AnonStorey0::$this
	ModelGroupDtoFactory_t474877319 * ___U24this_3;

public:
	inline static int32_t get_offset_of_groups_0() { return static_cast<int32_t>(offsetof(U3CcreateDtosU3Ec__AnonStorey0_t827342538, ___groups_0)); }
	inline List_1_t2603308778 * get_groups_0() const { return ___groups_0; }
	inline List_1_t2603308778 ** get_address_of_groups_0() { return &___groups_0; }
	inline void set_groups_0(List_1_t2603308778 * value)
	{
		___groups_0 = value;
		Il2CppCodeGenWriteBarrier((&___groups_0), value);
	}

	inline static int32_t get_offset_of_map_1() { return static_cast<int32_t>(offsetof(U3CcreateDtosU3Ec__AnonStorey0_t827342538, ___map_1)); }
	inline Dictionary_2_t4078280220 * get_map_1() const { return ___map_1; }
	inline Dictionary_2_t4078280220 ** get_address_of_map_1() { return &___map_1; }
	inline void set_map_1(Dictionary_2_t4078280220 * value)
	{
		___map_1 = value;
		Il2CppCodeGenWriteBarrier((&___map_1), value);
	}

	inline static int32_t get_offset_of_modelsCopy_2() { return static_cast<int32_t>(offsetof(U3CcreateDtosU3Ec__AnonStorey0_t827342538, ___modelsCopy_2)); }
	inline List_1_t1125654279 * get_modelsCopy_2() const { return ___modelsCopy_2; }
	inline List_1_t1125654279 ** get_address_of_modelsCopy_2() { return &___modelsCopy_2; }
	inline void set_modelsCopy_2(List_1_t1125654279 * value)
	{
		___modelsCopy_2 = value;
		Il2CppCodeGenWriteBarrier((&___modelsCopy_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CcreateDtosU3Ec__AnonStorey0_t827342538, ___U24this_3)); }
	inline ModelGroupDtoFactory_t474877319 * get_U24this_3() const { return ___U24this_3; }
	inline ModelGroupDtoFactory_t474877319 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(ModelGroupDtoFactory_t474877319 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATEDTOSU3EC__ANONSTOREY0_T827342538_H
#ifndef U3CCREATEDTOSU3EC__ANONSTOREY1_T1243368241_H
#define U3CCREATEDTOSU3EC__ANONSTOREY1_T1243368241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.ModelGrouping.ModelGroupDtoFactory/<createDtos>c__AnonStorey0/<createDtos>c__AnonStorey1
struct  U3CcreateDtosU3Ec__AnonStorey1_t1243368241  : public RuntimeObject
{
public:
	// System.String Library.Code.Facades.ModelGrouping.ModelGroupDtoFactory/<createDtos>c__AnonStorey0/<createDtos>c__AnonStorey1::gameObjName
	String_t* ___gameObjName_0;
	// UnityEngine.GameObject Library.Code.Facades.ModelGrouping.ModelGroupDtoFactory/<createDtos>c__AnonStorey0/<createDtos>c__AnonStorey1::gameObject
	GameObject_t1756533147 * ___gameObject_1;
	// Library.Code.Facades.ModelGrouping.ModelGroupDtoFactory/<createDtos>c__AnonStorey0 Library.Code.Facades.ModelGrouping.ModelGroupDtoFactory/<createDtos>c__AnonStorey0/<createDtos>c__AnonStorey1::<>f__ref$0
	U3CcreateDtosU3Ec__AnonStorey0_t827342538 * ___U3CU3Ef__refU240_2;

public:
	inline static int32_t get_offset_of_gameObjName_0() { return static_cast<int32_t>(offsetof(U3CcreateDtosU3Ec__AnonStorey1_t1243368241, ___gameObjName_0)); }
	inline String_t* get_gameObjName_0() const { return ___gameObjName_0; }
	inline String_t** get_address_of_gameObjName_0() { return &___gameObjName_0; }
	inline void set_gameObjName_0(String_t* value)
	{
		___gameObjName_0 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjName_0), value);
	}

	inline static int32_t get_offset_of_gameObject_1() { return static_cast<int32_t>(offsetof(U3CcreateDtosU3Ec__AnonStorey1_t1243368241, ___gameObject_1)); }
	inline GameObject_t1756533147 * get_gameObject_1() const { return ___gameObject_1; }
	inline GameObject_t1756533147 ** get_address_of_gameObject_1() { return &___gameObject_1; }
	inline void set_gameObject_1(GameObject_t1756533147 * value)
	{
		___gameObject_1 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_2() { return static_cast<int32_t>(offsetof(U3CcreateDtosU3Ec__AnonStorey1_t1243368241, ___U3CU3Ef__refU240_2)); }
	inline U3CcreateDtosU3Ec__AnonStorey0_t827342538 * get_U3CU3Ef__refU240_2() const { return ___U3CU3Ef__refU240_2; }
	inline U3CcreateDtosU3Ec__AnonStorey0_t827342538 ** get_address_of_U3CU3Ef__refU240_2() { return &___U3CU3Ef__refU240_2; }
	inline void set_U3CU3Ef__refU240_2(U3CcreateDtosU3Ec__AnonStorey0_t827342538 * value)
	{
		___U3CU3Ef__refU240_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATEDTOSU3EC__ANONSTOREY1_T1243368241_H
#ifndef MODELGROUPFACADEIMP_T1471023694_H
#define MODELGROUPFACADEIMP_T1471023694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.ModelGrouping.ModelGroupFacadeImp
struct  ModelGroupFacadeImp_t1471023694  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Library.Code.Facades.ModelGrouping.ModelGroupFacadeImp::model
	GameObject_t1756533147 * ___model_0;
	// System.Collections.Generic.List`1<Library.Code.Facades.Observer`1<Library.Code.Domain.Dtos.ModelGroupDto>> Library.Code.Facades.ModelGrouping.ModelGroupFacadeImp::_observers
	List_1_t3710614623 * ____observers_1;
	// Library.Code.Networking.GroupModel.GroupingModelApi Library.Code.Facades.ModelGrouping.ModelGroupFacadeImp::_groupingModelApi
	RuntimeObject* ____groupingModelApi_2;
	// Library.Code.Utils.Async.DoAsync Library.Code.Facades.ModelGrouping.ModelGroupFacadeImp::_async
	RuntimeObject* ____async_3;
	// System.Collections.Generic.List`1<Library.Code.Domain.Dtos.ModelGroupDto> Library.Code.Facades.ModelGrouping.ModelGroupFacadeImp::_modelGroupDtos
	List_1_t1177375679 * ____modelGroupDtos_4;
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.ModelGroup> Library.Code.Facades.ModelGrouping.ModelGroupFacadeImp::_modelGroups
	List_1_t2603308778 * ____modelGroups_5;

public:
	inline static int32_t get_offset_of_model_0() { return static_cast<int32_t>(offsetof(ModelGroupFacadeImp_t1471023694, ___model_0)); }
	inline GameObject_t1756533147 * get_model_0() const { return ___model_0; }
	inline GameObject_t1756533147 ** get_address_of_model_0() { return &___model_0; }
	inline void set_model_0(GameObject_t1756533147 * value)
	{
		___model_0 = value;
		Il2CppCodeGenWriteBarrier((&___model_0), value);
	}

	inline static int32_t get_offset_of__observers_1() { return static_cast<int32_t>(offsetof(ModelGroupFacadeImp_t1471023694, ____observers_1)); }
	inline List_1_t3710614623 * get__observers_1() const { return ____observers_1; }
	inline List_1_t3710614623 ** get_address_of__observers_1() { return &____observers_1; }
	inline void set__observers_1(List_1_t3710614623 * value)
	{
		____observers_1 = value;
		Il2CppCodeGenWriteBarrier((&____observers_1), value);
	}

	inline static int32_t get_offset_of__groupingModelApi_2() { return static_cast<int32_t>(offsetof(ModelGroupFacadeImp_t1471023694, ____groupingModelApi_2)); }
	inline RuntimeObject* get__groupingModelApi_2() const { return ____groupingModelApi_2; }
	inline RuntimeObject** get_address_of__groupingModelApi_2() { return &____groupingModelApi_2; }
	inline void set__groupingModelApi_2(RuntimeObject* value)
	{
		____groupingModelApi_2 = value;
		Il2CppCodeGenWriteBarrier((&____groupingModelApi_2), value);
	}

	inline static int32_t get_offset_of__async_3() { return static_cast<int32_t>(offsetof(ModelGroupFacadeImp_t1471023694, ____async_3)); }
	inline RuntimeObject* get__async_3() const { return ____async_3; }
	inline RuntimeObject** get_address_of__async_3() { return &____async_3; }
	inline void set__async_3(RuntimeObject* value)
	{
		____async_3 = value;
		Il2CppCodeGenWriteBarrier((&____async_3), value);
	}

	inline static int32_t get_offset_of__modelGroupDtos_4() { return static_cast<int32_t>(offsetof(ModelGroupFacadeImp_t1471023694, ____modelGroupDtos_4)); }
	inline List_1_t1177375679 * get__modelGroupDtos_4() const { return ____modelGroupDtos_4; }
	inline List_1_t1177375679 ** get_address_of__modelGroupDtos_4() { return &____modelGroupDtos_4; }
	inline void set__modelGroupDtos_4(List_1_t1177375679 * value)
	{
		____modelGroupDtos_4 = value;
		Il2CppCodeGenWriteBarrier((&____modelGroupDtos_4), value);
	}

	inline static int32_t get_offset_of__modelGroups_5() { return static_cast<int32_t>(offsetof(ModelGroupFacadeImp_t1471023694, ____modelGroups_5)); }
	inline List_1_t2603308778 * get__modelGroups_5() const { return ____modelGroups_5; }
	inline List_1_t2603308778 ** get_address_of__modelGroups_5() { return &____modelGroups_5; }
	inline void set__modelGroups_5(List_1_t2603308778 * value)
	{
		____modelGroups_5 = value;
		Il2CppCodeGenWriteBarrier((&____modelGroups_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELGROUPFACADEIMP_T1471023694_H
#ifndef DOWNLOAD3DELEMENTIMP_T2560998171_H
#define DOWNLOAD3DELEMENTIMP_T2560998171_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking._3dElement.Download3dElementImp
struct  Download3dElementImp_t2560998171  : public RuntimeObject
{
public:
	// Library.Code.Utils.Platforms.ReturnForPlatform`2<System.String,Library.Code.Domain.Entities.Element> Library.Code.Networking._3dElement.Download3dElementImp::returnAssetBundle
	RuntimeObject* ___returnAssetBundle_0;

public:
	inline static int32_t get_offset_of_returnAssetBundle_0() { return static_cast<int32_t>(offsetof(Download3dElementImp_t2560998171, ___returnAssetBundle_0)); }
	inline RuntimeObject* get_returnAssetBundle_0() const { return ___returnAssetBundle_0; }
	inline RuntimeObject** get_address_of_returnAssetBundle_0() { return &___returnAssetBundle_0; }
	inline void set_returnAssetBundle_0(RuntimeObject* value)
	{
		___returnAssetBundle_0 = value;
		Il2CppCodeGenWriteBarrier((&___returnAssetBundle_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOAD3DELEMENTIMP_T2560998171_H
#ifndef U3CDOWNLOADMODELU3EC__ITERATOR0_T2709715940_H
#define U3CDOWNLOADMODELU3EC__ITERATOR0_T2709715940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking._3dElement.Download3dElementImp/<downloadModel>c__Iterator0
struct  U3CdownloadModelU3Ec__Iterator0_t2709715940  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Element Library.Code.Networking._3dElement.Download3dElementImp/<downloadModel>c__Iterator0::element
	Element_t2276588008 * ___element_0;
	// System.String Library.Code.Networking._3dElement.Download3dElementImp/<downloadModel>c__Iterator0::<url>__0
	String_t* ___U3CurlU3E__0_1;
	// UnityEngine.WWW Library.Code.Networking._3dElement.Download3dElementImp/<downloadModel>c__Iterator0::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_2;
	// Library.Code.Networking.ResponseWrapper`1<Library.Code.Domain.Dtos.ElementWithAssetBundle> Library.Code.Networking._3dElement.Download3dElementImp/<downloadModel>c__Iterator0::wrapper
	RuntimeObject* ___wrapper_3;
	// Library.Code.Networking._3dElement.Download3dElementImp Library.Code.Networking._3dElement.Download3dElementImp/<downloadModel>c__Iterator0::$this
	Download3dElementImp_t2560998171 * ___U24this_4;
	// System.Object Library.Code.Networking._3dElement.Download3dElementImp/<downloadModel>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Library.Code.Networking._3dElement.Download3dElementImp/<downloadModel>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 Library.Code.Networking._3dElement.Download3dElementImp/<downloadModel>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_element_0() { return static_cast<int32_t>(offsetof(U3CdownloadModelU3Ec__Iterator0_t2709715940, ___element_0)); }
	inline Element_t2276588008 * get_element_0() const { return ___element_0; }
	inline Element_t2276588008 ** get_address_of_element_0() { return &___element_0; }
	inline void set_element_0(Element_t2276588008 * value)
	{
		___element_0 = value;
		Il2CppCodeGenWriteBarrier((&___element_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3E__0_1() { return static_cast<int32_t>(offsetof(U3CdownloadModelU3Ec__Iterator0_t2709715940, ___U3CurlU3E__0_1)); }
	inline String_t* get_U3CurlU3E__0_1() const { return ___U3CurlU3E__0_1; }
	inline String_t** get_address_of_U3CurlU3E__0_1() { return &___U3CurlU3E__0_1; }
	inline void set_U3CurlU3E__0_1(String_t* value)
	{
		___U3CurlU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_2() { return static_cast<int32_t>(offsetof(U3CdownloadModelU3Ec__Iterator0_t2709715940, ___U3CwwwU3E__0_2)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_2() const { return ___U3CwwwU3E__0_2; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_2() { return &___U3CwwwU3E__0_2; }
	inline void set_U3CwwwU3E__0_2(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_2), value);
	}

	inline static int32_t get_offset_of_wrapper_3() { return static_cast<int32_t>(offsetof(U3CdownloadModelU3Ec__Iterator0_t2709715940, ___wrapper_3)); }
	inline RuntimeObject* get_wrapper_3() const { return ___wrapper_3; }
	inline RuntimeObject** get_address_of_wrapper_3() { return &___wrapper_3; }
	inline void set_wrapper_3(RuntimeObject* value)
	{
		___wrapper_3 = value;
		Il2CppCodeGenWriteBarrier((&___wrapper_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CdownloadModelU3Ec__Iterator0_t2709715940, ___U24this_4)); }
	inline Download3dElementImp_t2560998171 * get_U24this_4() const { return ___U24this_4; }
	inline Download3dElementImp_t2560998171 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(Download3dElementImp_t2560998171 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CdownloadModelU3Ec__Iterator0_t2709715940, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CdownloadModelU3Ec__Iterator0_t2709715940, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CdownloadModelU3Ec__Iterator0_t2709715940, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOWNLOADMODELU3EC__ITERATOR0_T2709715940_H
#ifndef FAKEGROUPINMODELAPI_T2519811264_H
#define FAKEGROUPINMODELAPI_T2519811264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.ModelGrouping.FakeGroupinModelApi
struct  FakeGroupinModelApi_t2519811264  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEGROUPINMODELAPI_T2519811264_H
#ifndef U3CREMOVEU3EC__ANONSTOREY1_T2484859181_H
#define U3CREMOVEU3EC__ANONSTOREY1_T2484859181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Models.ModelFacadeImpl/<remove>c__AnonStorey1
struct  U3CremoveU3Ec__AnonStorey1_t2484859181  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Model Library.Code.Facades.Models.ModelFacadeImpl/<remove>c__AnonStorey1::models
	Model_t2163629551 * ___models_0;
	// Library.Code.Facades.Callback Library.Code.Facades.Models.ModelFacadeImpl/<remove>c__AnonStorey1::persistCallback
	RuntimeObject* ___persistCallback_1;
	// Library.Code.Facades.Models.ModelFacadeImpl Library.Code.Facades.Models.ModelFacadeImpl/<remove>c__AnonStorey1::$this
	ModelFacadeImpl_t3221237801 * ___U24this_2;

public:
	inline static int32_t get_offset_of_models_0() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__AnonStorey1_t2484859181, ___models_0)); }
	inline Model_t2163629551 * get_models_0() const { return ___models_0; }
	inline Model_t2163629551 ** get_address_of_models_0() { return &___models_0; }
	inline void set_models_0(Model_t2163629551 * value)
	{
		___models_0 = value;
		Il2CppCodeGenWriteBarrier((&___models_0), value);
	}

	inline static int32_t get_offset_of_persistCallback_1() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__AnonStorey1_t2484859181, ___persistCallback_1)); }
	inline RuntimeObject* get_persistCallback_1() const { return ___persistCallback_1; }
	inline RuntimeObject** get_address_of_persistCallback_1() { return &___persistCallback_1; }
	inline void set_persistCallback_1(RuntimeObject* value)
	{
		___persistCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___persistCallback_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__AnonStorey1_t2484859181, ___U24this_2)); }
	inline ModelFacadeImpl_t3221237801 * get_U24this_2() const { return ___U24this_2; }
	inline ModelFacadeImpl_t3221237801 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ModelFacadeImpl_t3221237801 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVEU3EC__ANONSTOREY1_T2484859181_H
#ifndef U3CGETU3EC__ANONSTOREY2_T241217034_H
#define U3CGETU3EC__ANONSTOREY2_T241217034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Models.ModelFacadeImpl/<get>c__AnonStorey2
struct  U3CgetU3Ec__AnonStorey2_t241217034  : public RuntimeObject
{
public:
	// Library.Code.Facades.Callback Library.Code.Facades.Models.ModelFacadeImpl/<get>c__AnonStorey2::persistCallback
	RuntimeObject* ___persistCallback_0;

public:
	inline static int32_t get_offset_of_persistCallback_0() { return static_cast<int32_t>(offsetof(U3CgetU3Ec__AnonStorey2_t241217034, ___persistCallback_0)); }
	inline RuntimeObject* get_persistCallback_0() const { return ___persistCallback_0; }
	inline RuntimeObject** get_address_of_persistCallback_0() { return &___persistCallback_0; }
	inline void set_persistCallback_0(RuntimeObject* value)
	{
		___persistCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___persistCallback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETU3EC__ANONSTOREY2_T241217034_H
#ifndef U3CUPDATEOBSERVERSU3EC__ANONSTOREY3_T193582475_H
#define U3CUPDATEOBSERVERSU3EC__ANONSTOREY3_T193582475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Models.ModelFacadeImpl/<updateObservers>c__AnonStorey3
struct  U3CupdateObserversU3Ec__AnonStorey3_t193582475  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model> Library.Code.Facades.Models.ModelFacadeImpl/<updateObservers>c__AnonStorey3::messages
	List_1_t1532750683 * ___messages_0;

public:
	inline static int32_t get_offset_of_messages_0() { return static_cast<int32_t>(offsetof(U3CupdateObserversU3Ec__AnonStorey3_t193582475, ___messages_0)); }
	inline List_1_t1532750683 * get_messages_0() const { return ___messages_0; }
	inline List_1_t1532750683 ** get_address_of_messages_0() { return &___messages_0; }
	inline void set_messages_0(List_1_t1532750683 * value)
	{
		___messages_0 = value;
		Il2CppCodeGenWriteBarrier((&___messages_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEOBSERVERSU3EC__ANONSTOREY3_T193582475_H
#ifndef MODELSPERSISTANCEIMPL_T1155557687_H
#define MODELSPERSISTANCEIMPL_T1155557687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Models.ModelsPersistanceImpl
struct  ModelsPersistanceImpl_t1155557687  : public RuntimeObject
{
public:
	// System.String Library.Code.Facades.Models.ModelsPersistanceImpl::_filename
	String_t* ____filename_0;
	// Library.Code.Persistance.FileJsonPersistance`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model>> Library.Code.Facades.Models.ModelsPersistanceImpl::filePresistance
	FileJsonPersistance_1_t649922390 * ___filePresistance_1;
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model> Library.Code.Facades.Models.ModelsPersistanceImpl::_models
	List_1_t1532750683 * ____models_2;

public:
	inline static int32_t get_offset_of__filename_0() { return static_cast<int32_t>(offsetof(ModelsPersistanceImpl_t1155557687, ____filename_0)); }
	inline String_t* get__filename_0() const { return ____filename_0; }
	inline String_t** get_address_of__filename_0() { return &____filename_0; }
	inline void set__filename_0(String_t* value)
	{
		____filename_0 = value;
		Il2CppCodeGenWriteBarrier((&____filename_0), value);
	}

	inline static int32_t get_offset_of_filePresistance_1() { return static_cast<int32_t>(offsetof(ModelsPersistanceImpl_t1155557687, ___filePresistance_1)); }
	inline FileJsonPersistance_1_t649922390 * get_filePresistance_1() const { return ___filePresistance_1; }
	inline FileJsonPersistance_1_t649922390 ** get_address_of_filePresistance_1() { return &___filePresistance_1; }
	inline void set_filePresistance_1(FileJsonPersistance_1_t649922390 * value)
	{
		___filePresistance_1 = value;
		Il2CppCodeGenWriteBarrier((&___filePresistance_1), value);
	}

	inline static int32_t get_offset_of__models_2() { return static_cast<int32_t>(offsetof(ModelsPersistanceImpl_t1155557687, ____models_2)); }
	inline List_1_t1532750683 * get__models_2() const { return ____models_2; }
	inline List_1_t1532750683 ** get_address_of__models_2() { return &____models_2; }
	inline void set__models_2(List_1_t1532750683 * value)
	{
		____models_2 = value;
		Il2CppCodeGenWriteBarrier((&____models_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELSPERSISTANCEIMPL_T1155557687_H
#ifndef U3CGETALLU3EC__ITERATOR0_T1222501571_H
#define U3CGETALLU3EC__ITERATOR0_T1222501571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Models.ModelsPersistanceImpl/<getAll>c__Iterator0
struct  U3CgetAllU3Ec__Iterator0_t1222501571  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model> Library.Code.Facades.Models.ModelsPersistanceImpl/<getAll>c__Iterator0::<list>__0
	List_1_t1532750683 * ___U3ClistU3E__0_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model>> Library.Code.Facades.Models.ModelsPersistanceImpl/<getAll>c__Iterator0::success
	UnityAction_1_t2899336434 * ___success_1;
	// Library.Code.Facades.Models.ModelsPersistanceImpl Library.Code.Facades.Models.ModelsPersistanceImpl/<getAll>c__Iterator0::$this
	ModelsPersistanceImpl_t1155557687 * ___U24this_2;
	// System.Object Library.Code.Facades.Models.ModelsPersistanceImpl/<getAll>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Library.Code.Facades.Models.ModelsPersistanceImpl/<getAll>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Library.Code.Facades.Models.ModelsPersistanceImpl/<getAll>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3ClistU3E__0_0() { return static_cast<int32_t>(offsetof(U3CgetAllU3Ec__Iterator0_t1222501571, ___U3ClistU3E__0_0)); }
	inline List_1_t1532750683 * get_U3ClistU3E__0_0() const { return ___U3ClistU3E__0_0; }
	inline List_1_t1532750683 ** get_address_of_U3ClistU3E__0_0() { return &___U3ClistU3E__0_0; }
	inline void set_U3ClistU3E__0_0(List_1_t1532750683 * value)
	{
		___U3ClistU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClistU3E__0_0), value);
	}

	inline static int32_t get_offset_of_success_1() { return static_cast<int32_t>(offsetof(U3CgetAllU3Ec__Iterator0_t1222501571, ___success_1)); }
	inline UnityAction_1_t2899336434 * get_success_1() const { return ___success_1; }
	inline UnityAction_1_t2899336434 ** get_address_of_success_1() { return &___success_1; }
	inline void set_success_1(UnityAction_1_t2899336434 * value)
	{
		___success_1 = value;
		Il2CppCodeGenWriteBarrier((&___success_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CgetAllU3Ec__Iterator0_t1222501571, ___U24this_2)); }
	inline ModelsPersistanceImpl_t1155557687 * get_U24this_2() const { return ___U24this_2; }
	inline ModelsPersistanceImpl_t1155557687 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ModelsPersistanceImpl_t1155557687 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CgetAllU3Ec__Iterator0_t1222501571, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CgetAllU3Ec__Iterator0_t1222501571, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CgetAllU3Ec__Iterator0_t1222501571, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLU3EC__ITERATOR0_T1222501571_H
#ifndef U3CADDU3EC__ITERATOR1_T690995996_H
#define U3CADDU3EC__ITERATOR1_T690995996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Models.ModelsPersistanceImpl/<add>c__Iterator1
struct  U3CaddU3Ec__Iterator1_t690995996  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Model Library.Code.Facades.Models.ModelsPersistanceImpl/<add>c__Iterator1::message
	Model_t2163629551 * ___message_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model>> Library.Code.Facades.Models.ModelsPersistanceImpl/<add>c__Iterator1::success
	UnityAction_1_t2899336434 * ___success_1;
	// Library.Code.Facades.Models.ModelsPersistanceImpl Library.Code.Facades.Models.ModelsPersistanceImpl/<add>c__Iterator1::$this
	ModelsPersistanceImpl_t1155557687 * ___U24this_2;
	// System.Object Library.Code.Facades.Models.ModelsPersistanceImpl/<add>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Library.Code.Facades.Models.ModelsPersistanceImpl/<add>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 Library.Code.Facades.Models.ModelsPersistanceImpl/<add>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_message_0() { return static_cast<int32_t>(offsetof(U3CaddU3Ec__Iterator1_t690995996, ___message_0)); }
	inline Model_t2163629551 * get_message_0() const { return ___message_0; }
	inline Model_t2163629551 ** get_address_of_message_0() { return &___message_0; }
	inline void set_message_0(Model_t2163629551 * value)
	{
		___message_0 = value;
		Il2CppCodeGenWriteBarrier((&___message_0), value);
	}

	inline static int32_t get_offset_of_success_1() { return static_cast<int32_t>(offsetof(U3CaddU3Ec__Iterator1_t690995996, ___success_1)); }
	inline UnityAction_1_t2899336434 * get_success_1() const { return ___success_1; }
	inline UnityAction_1_t2899336434 ** get_address_of_success_1() { return &___success_1; }
	inline void set_success_1(UnityAction_1_t2899336434 * value)
	{
		___success_1 = value;
		Il2CppCodeGenWriteBarrier((&___success_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CaddU3Ec__Iterator1_t690995996, ___U24this_2)); }
	inline ModelsPersistanceImpl_t1155557687 * get_U24this_2() const { return ___U24this_2; }
	inline ModelsPersistanceImpl_t1155557687 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ModelsPersistanceImpl_t1155557687 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CaddU3Ec__Iterator1_t690995996, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CaddU3Ec__Iterator1_t690995996, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CaddU3Ec__Iterator1_t690995996, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDU3EC__ITERATOR1_T690995996_H
#ifndef U3CREMOVEU3EC__ITERATOR2_T1286003366_H
#define U3CREMOVEU3EC__ITERATOR2_T1286003366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Models.ModelsPersistanceImpl/<remove>c__Iterator2
struct  U3CremoveU3Ec__Iterator2_t1286003366  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Model Library.Code.Facades.Models.ModelsPersistanceImpl/<remove>c__Iterator2::message
	Model_t2163629551 * ___message_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model>> Library.Code.Facades.Models.ModelsPersistanceImpl/<remove>c__Iterator2::success
	UnityAction_1_t2899336434 * ___success_1;
	// Library.Code.Facades.Models.ModelsPersistanceImpl Library.Code.Facades.Models.ModelsPersistanceImpl/<remove>c__Iterator2::$this
	ModelsPersistanceImpl_t1155557687 * ___U24this_2;
	// System.Object Library.Code.Facades.Models.ModelsPersistanceImpl/<remove>c__Iterator2::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Library.Code.Facades.Models.ModelsPersistanceImpl/<remove>c__Iterator2::$disposing
	bool ___U24disposing_4;
	// System.Int32 Library.Code.Facades.Models.ModelsPersistanceImpl/<remove>c__Iterator2::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_message_0() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__Iterator2_t1286003366, ___message_0)); }
	inline Model_t2163629551 * get_message_0() const { return ___message_0; }
	inline Model_t2163629551 ** get_address_of_message_0() { return &___message_0; }
	inline void set_message_0(Model_t2163629551 * value)
	{
		___message_0 = value;
		Il2CppCodeGenWriteBarrier((&___message_0), value);
	}

	inline static int32_t get_offset_of_success_1() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__Iterator2_t1286003366, ___success_1)); }
	inline UnityAction_1_t2899336434 * get_success_1() const { return ___success_1; }
	inline UnityAction_1_t2899336434 ** get_address_of_success_1() { return &___success_1; }
	inline void set_success_1(UnityAction_1_t2899336434 * value)
	{
		___success_1 = value;
		Il2CppCodeGenWriteBarrier((&___success_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__Iterator2_t1286003366, ___U24this_2)); }
	inline ModelsPersistanceImpl_t1155557687 * get_U24this_2() const { return ___U24this_2; }
	inline ModelsPersistanceImpl_t1155557687 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ModelsPersistanceImpl_t1155557687 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__Iterator2_t1286003366, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__Iterator2_t1286003366, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__Iterator2_t1286003366, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVEU3EC__ITERATOR2_T1286003366_H
#ifndef U3CCLEARU3EC__ITERATOR3_T1383450968_H
#define U3CCLEARU3EC__ITERATOR3_T1383450968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Models.ModelsPersistanceImpl/<clear>c__Iterator3
struct  U3CclearU3Ec__Iterator3_t1383450968  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityAction Library.Code.Facades.Models.ModelsPersistanceImpl/<clear>c__Iterator3::success
	UnityAction_t4025899511 * ___success_0;
	// Library.Code.Facades.Models.ModelsPersistanceImpl Library.Code.Facades.Models.ModelsPersistanceImpl/<clear>c__Iterator3::$this
	ModelsPersistanceImpl_t1155557687 * ___U24this_1;
	// System.Object Library.Code.Facades.Models.ModelsPersistanceImpl/<clear>c__Iterator3::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Library.Code.Facades.Models.ModelsPersistanceImpl/<clear>c__Iterator3::$disposing
	bool ___U24disposing_3;
	// System.Int32 Library.Code.Facades.Models.ModelsPersistanceImpl/<clear>c__Iterator3::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_success_0() { return static_cast<int32_t>(offsetof(U3CclearU3Ec__Iterator3_t1383450968, ___success_0)); }
	inline UnityAction_t4025899511 * get_success_0() const { return ___success_0; }
	inline UnityAction_t4025899511 ** get_address_of_success_0() { return &___success_0; }
	inline void set_success_0(UnityAction_t4025899511 * value)
	{
		___success_0 = value;
		Il2CppCodeGenWriteBarrier((&___success_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CclearU3Ec__Iterator3_t1383450968, ___U24this_1)); }
	inline ModelsPersistanceImpl_t1155557687 * get_U24this_1() const { return ___U24this_1; }
	inline ModelsPersistanceImpl_t1155557687 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ModelsPersistanceImpl_t1155557687 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CclearU3Ec__Iterator3_t1383450968, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CclearU3Ec__Iterator3_t1383450968, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CclearU3Ec__Iterator3_t1383450968, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLEARU3EC__ITERATOR3_T1383450968_H
#ifndef CHANGECHECKLISTITEMSTATUSAPIIMP_T3896993653_H
#define CHANGECHECKLISTITEMSTATUSAPIIMP_T3896993653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Checklist.ChangeCheckListItemStatusApiImp
struct  ChangeCheckListItemStatusApiImp_t3896993653  : public RuntimeObject
{
public:
	// System.String Library.Code.Networking.Checklist.ChangeCheckListItemStatusApiImp::ulr
	String_t* ___ulr_0;
	// Library.Code.Parsers.JsonParser`1<Library.Code.Domain.Entities.Checklist.ItemStatus> Library.Code.Networking.Checklist.ChangeCheckListItemStatusApiImp::_parser
	RuntimeObject* ____parser_1;
	// Library.Code.WebRequest.WebRequestBuilder Library.Code.Networking.Checklist.ChangeCheckListItemStatusApiImp::_requestBuilder
	WebRequestBuilder_t3983605310 * ____requestBuilder_2;

public:
	inline static int32_t get_offset_of_ulr_0() { return static_cast<int32_t>(offsetof(ChangeCheckListItemStatusApiImp_t3896993653, ___ulr_0)); }
	inline String_t* get_ulr_0() const { return ___ulr_0; }
	inline String_t** get_address_of_ulr_0() { return &___ulr_0; }
	inline void set_ulr_0(String_t* value)
	{
		___ulr_0 = value;
		Il2CppCodeGenWriteBarrier((&___ulr_0), value);
	}

	inline static int32_t get_offset_of__parser_1() { return static_cast<int32_t>(offsetof(ChangeCheckListItemStatusApiImp_t3896993653, ____parser_1)); }
	inline RuntimeObject* get__parser_1() const { return ____parser_1; }
	inline RuntimeObject** get_address_of__parser_1() { return &____parser_1; }
	inline void set__parser_1(RuntimeObject* value)
	{
		____parser_1 = value;
		Il2CppCodeGenWriteBarrier((&____parser_1), value);
	}

	inline static int32_t get_offset_of__requestBuilder_2() { return static_cast<int32_t>(offsetof(ChangeCheckListItemStatusApiImp_t3896993653, ____requestBuilder_2)); }
	inline WebRequestBuilder_t3983605310 * get__requestBuilder_2() const { return ____requestBuilder_2; }
	inline WebRequestBuilder_t3983605310 ** get_address_of__requestBuilder_2() { return &____requestBuilder_2; }
	inline void set__requestBuilder_2(WebRequestBuilder_t3983605310 * value)
	{
		____requestBuilder_2 = value;
		Il2CppCodeGenWriteBarrier((&____requestBuilder_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANGECHECKLISTITEMSTATUSAPIIMP_T3896993653_H
#ifndef ELEMENTSTATUSAPIIMPL_T2005129180_H
#define ELEMENTSTATUSAPIIMPL_T2005129180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Elements.ElementStatusApiImpl
struct  ElementStatusApiImpl_t2005129180  : public RuntimeObject
{
public:
	// System.String Library.Code.Networking.Elements.ElementStatusApiImpl::url
	String_t* ___url_0;
	// Library.Code.Parsers.JsonParser`1<Library.Code.Domain.Entities.ApproveElementWrapper> Library.Code.Networking.Elements.ElementStatusApiImpl::parser
	RuntimeObject* ___parser_1;
	// Library.Code.WebRequest.WebRequestBuilder Library.Code.Networking.Elements.ElementStatusApiImpl::_builder
	WebRequestBuilder_t3983605310 * ____builder_2;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(ElementStatusApiImpl_t2005129180, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_parser_1() { return static_cast<int32_t>(offsetof(ElementStatusApiImpl_t2005129180, ___parser_1)); }
	inline RuntimeObject* get_parser_1() const { return ___parser_1; }
	inline RuntimeObject** get_address_of_parser_1() { return &___parser_1; }
	inline void set_parser_1(RuntimeObject* value)
	{
		___parser_1 = value;
		Il2CppCodeGenWriteBarrier((&___parser_1), value);
	}

	inline static int32_t get_offset_of__builder_2() { return static_cast<int32_t>(offsetof(ElementStatusApiImpl_t2005129180, ____builder_2)); }
	inline WebRequestBuilder_t3983605310 * get__builder_2() const { return ____builder_2; }
	inline WebRequestBuilder_t3983605310 ** get_address_of__builder_2() { return &____builder_2; }
	inline void set__builder_2(WebRequestBuilder_t3983605310 * value)
	{
		____builder_2 = value;
		Il2CppCodeGenWriteBarrier((&____builder_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTSTATUSAPIIMPL_T2005129180_H
#ifndef U3CAPPROVEELEMENTSTATUSU3EC__ITERATOR0_T4133233964_H
#define U3CAPPROVEELEMENTSTATUSU3EC__ITERATOR0_T4133233964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Elements.ElementStatusApiImpl/<approveElementStatus>c__Iterator0
struct  U3CapproveElementStatusU3Ec__Iterator0_t4133233964  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Element Library.Code.Networking.Elements.ElementStatusApiImpl/<approveElementStatus>c__Iterator0::element
	Element_t2276588008 * ___element_0;
	// System.String Library.Code.Networking.Elements.ElementStatusApiImpl/<approveElementStatus>c__Iterator0::<urll>__0
	String_t* ___U3CurllU3E__0_1;
	// UnityEngine.Networking.UnityWebRequest Library.Code.Networking.Elements.ElementStatusApiImpl/<approveElementStatus>c__Iterator0::<request>__0
	UnityWebRequest_t254341728 * ___U3CrequestU3E__0_2;
	// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.ApproveElementResponse>> Library.Code.Networking.Elements.ElementStatusApiImpl/<approveElementStatus>c__Iterator0::wrapper
	RuntimeObject* ___wrapper_3;
	// Library.Code.Networking.Elements.ElementStatusApiImpl Library.Code.Networking.Elements.ElementStatusApiImpl/<approveElementStatus>c__Iterator0::$this
	ElementStatusApiImpl_t2005129180 * ___U24this_4;
	// System.Object Library.Code.Networking.Elements.ElementStatusApiImpl/<approveElementStatus>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Library.Code.Networking.Elements.ElementStatusApiImpl/<approveElementStatus>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 Library.Code.Networking.Elements.ElementStatusApiImpl/<approveElementStatus>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_element_0() { return static_cast<int32_t>(offsetof(U3CapproveElementStatusU3Ec__Iterator0_t4133233964, ___element_0)); }
	inline Element_t2276588008 * get_element_0() const { return ___element_0; }
	inline Element_t2276588008 ** get_address_of_element_0() { return &___element_0; }
	inline void set_element_0(Element_t2276588008 * value)
	{
		___element_0 = value;
		Il2CppCodeGenWriteBarrier((&___element_0), value);
	}

	inline static int32_t get_offset_of_U3CurllU3E__0_1() { return static_cast<int32_t>(offsetof(U3CapproveElementStatusU3Ec__Iterator0_t4133233964, ___U3CurllU3E__0_1)); }
	inline String_t* get_U3CurllU3E__0_1() const { return ___U3CurllU3E__0_1; }
	inline String_t** get_address_of_U3CurllU3E__0_1() { return &___U3CurllU3E__0_1; }
	inline void set_U3CurllU3E__0_1(String_t* value)
	{
		___U3CurllU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurllU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CapproveElementStatusU3Ec__Iterator0_t4133233964, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t254341728 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t254341728 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t254341728 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_wrapper_3() { return static_cast<int32_t>(offsetof(U3CapproveElementStatusU3Ec__Iterator0_t4133233964, ___wrapper_3)); }
	inline RuntimeObject* get_wrapper_3() const { return ___wrapper_3; }
	inline RuntimeObject** get_address_of_wrapper_3() { return &___wrapper_3; }
	inline void set_wrapper_3(RuntimeObject* value)
	{
		___wrapper_3 = value;
		Il2CppCodeGenWriteBarrier((&___wrapper_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CapproveElementStatusU3Ec__Iterator0_t4133233964, ___U24this_4)); }
	inline ElementStatusApiImpl_t2005129180 * get_U24this_4() const { return ___U24this_4; }
	inline ElementStatusApiImpl_t2005129180 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(ElementStatusApiImpl_t2005129180 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CapproveElementStatusU3Ec__Iterator0_t4133233964, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CapproveElementStatusU3Ec__Iterator0_t4133233964, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CapproveElementStatusU3Ec__Iterator0_t4133233964, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAPPROVEELEMENTSTATUSU3EC__ITERATOR0_T4133233964_H
#ifndef U3CSETCURRENTSTATUSU3EC__ITERATOR1_T1596340271_H
#define U3CSETCURRENTSTATUSU3EC__ITERATOR1_T1596340271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Elements.ElementStatusApiImpl/<setCurrentStatus>c__Iterator1
struct  U3CsetCurrentStatusU3Ec__Iterator1_t1596340271  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Element Library.Code.Networking.Elements.ElementStatusApiImpl/<setCurrentStatus>c__Iterator1::element
	Element_t2276588008 * ___element_0;
	// Library.Code.Domain.Entities.ApproveElementResponse Library.Code.Networking.Elements.ElementStatusApiImpl/<setCurrentStatus>c__Iterator1::status
	ApproveElementResponse_t2765772516 * ___status_1;
	// System.String Library.Code.Networking.Elements.ElementStatusApiImpl/<setCurrentStatus>c__Iterator1::<urll>__0
	String_t* ___U3CurllU3E__0_2;
	// UnityEngine.Networking.UnityWebRequest Library.Code.Networking.Elements.ElementStatusApiImpl/<setCurrentStatus>c__Iterator1::<request>__0
	UnityWebRequest_t254341728 * ___U3CrequestU3E__0_3;
	// Library.Code.Networking.ResponseWrapper`1<Library.Code.Domain.Entities.Element> Library.Code.Networking.Elements.ElementStatusApiImpl/<setCurrentStatus>c__Iterator1::wrapper
	RuntimeObject* ___wrapper_4;
	// Library.Code.Networking.Elements.ElementStatusApiImpl Library.Code.Networking.Elements.ElementStatusApiImpl/<setCurrentStatus>c__Iterator1::$this
	ElementStatusApiImpl_t2005129180 * ___U24this_5;
	// System.Object Library.Code.Networking.Elements.ElementStatusApiImpl/<setCurrentStatus>c__Iterator1::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean Library.Code.Networking.Elements.ElementStatusApiImpl/<setCurrentStatus>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 Library.Code.Networking.Elements.ElementStatusApiImpl/<setCurrentStatus>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_element_0() { return static_cast<int32_t>(offsetof(U3CsetCurrentStatusU3Ec__Iterator1_t1596340271, ___element_0)); }
	inline Element_t2276588008 * get_element_0() const { return ___element_0; }
	inline Element_t2276588008 ** get_address_of_element_0() { return &___element_0; }
	inline void set_element_0(Element_t2276588008 * value)
	{
		___element_0 = value;
		Il2CppCodeGenWriteBarrier((&___element_0), value);
	}

	inline static int32_t get_offset_of_status_1() { return static_cast<int32_t>(offsetof(U3CsetCurrentStatusU3Ec__Iterator1_t1596340271, ___status_1)); }
	inline ApproveElementResponse_t2765772516 * get_status_1() const { return ___status_1; }
	inline ApproveElementResponse_t2765772516 ** get_address_of_status_1() { return &___status_1; }
	inline void set_status_1(ApproveElementResponse_t2765772516 * value)
	{
		___status_1 = value;
		Il2CppCodeGenWriteBarrier((&___status_1), value);
	}

	inline static int32_t get_offset_of_U3CurllU3E__0_2() { return static_cast<int32_t>(offsetof(U3CsetCurrentStatusU3Ec__Iterator1_t1596340271, ___U3CurllU3E__0_2)); }
	inline String_t* get_U3CurllU3E__0_2() const { return ___U3CurllU3E__0_2; }
	inline String_t** get_address_of_U3CurllU3E__0_2() { return &___U3CurllU3E__0_2; }
	inline void set_U3CurllU3E__0_2(String_t* value)
	{
		___U3CurllU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurllU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CsetCurrentStatusU3Ec__Iterator1_t1596340271, ___U3CrequestU3E__0_3)); }
	inline UnityWebRequest_t254341728 * get_U3CrequestU3E__0_3() const { return ___U3CrequestU3E__0_3; }
	inline UnityWebRequest_t254341728 ** get_address_of_U3CrequestU3E__0_3() { return &___U3CrequestU3E__0_3; }
	inline void set_U3CrequestU3E__0_3(UnityWebRequest_t254341728 * value)
	{
		___U3CrequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_3), value);
	}

	inline static int32_t get_offset_of_wrapper_4() { return static_cast<int32_t>(offsetof(U3CsetCurrentStatusU3Ec__Iterator1_t1596340271, ___wrapper_4)); }
	inline RuntimeObject* get_wrapper_4() const { return ___wrapper_4; }
	inline RuntimeObject** get_address_of_wrapper_4() { return &___wrapper_4; }
	inline void set_wrapper_4(RuntimeObject* value)
	{
		___wrapper_4 = value;
		Il2CppCodeGenWriteBarrier((&___wrapper_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CsetCurrentStatusU3Ec__Iterator1_t1596340271, ___U24this_5)); }
	inline ElementStatusApiImpl_t2005129180 * get_U24this_5() const { return ___U24this_5; }
	inline ElementStatusApiImpl_t2005129180 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(ElementStatusApiImpl_t2005129180 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CsetCurrentStatusU3Ec__Iterator1_t1596340271, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CsetCurrentStatusU3Ec__Iterator1_t1596340271, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CsetCurrentStatusU3Ec__Iterator1_t1596340271, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETCURRENTSTATUSU3EC__ITERATOR1_T1596340271_H
#ifndef FAKEELEMENTSTATUSAPI_T3580592613_H
#define FAKEELEMENTSTATUSAPI_T3580592613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Elements.FakeElementStatusApi
struct  FakeElementStatusApi_t3580592613  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEELEMENTSTATUSAPI_T3580592613_H
#ifndef U3CAPPROVEELEMENTSTATUSU3EC__ITERATOR0_T1302570149_H
#define U3CAPPROVEELEMENTSTATUSU3EC__ITERATOR0_T1302570149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Elements.FakeElementStatusApi/<approveElementStatus>c__Iterator0
struct  U3CapproveElementStatusU3Ec__Iterator0_t1302570149  : public RuntimeObject
{
public:
	// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.ApproveElementResponse>> Library.Code.Networking.Elements.FakeElementStatusApi/<approveElementStatus>c__Iterator0::wrapper
	RuntimeObject* ___wrapper_0;
	// System.Object Library.Code.Networking.Elements.FakeElementStatusApi/<approveElementStatus>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Library.Code.Networking.Elements.FakeElementStatusApi/<approveElementStatus>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Library.Code.Networking.Elements.FakeElementStatusApi/<approveElementStatus>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_wrapper_0() { return static_cast<int32_t>(offsetof(U3CapproveElementStatusU3Ec__Iterator0_t1302570149, ___wrapper_0)); }
	inline RuntimeObject* get_wrapper_0() const { return ___wrapper_0; }
	inline RuntimeObject** get_address_of_wrapper_0() { return &___wrapper_0; }
	inline void set_wrapper_0(RuntimeObject* value)
	{
		___wrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&___wrapper_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CapproveElementStatusU3Ec__Iterator0_t1302570149, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CapproveElementStatusU3Ec__Iterator0_t1302570149, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CapproveElementStatusU3Ec__Iterator0_t1302570149, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAPPROVEELEMENTSTATUSU3EC__ITERATOR0_T1302570149_H
#ifndef U3CSETCURRENTSTATUSU3EC__ITERATOR1_T3340147720_H
#define U3CSETCURRENTSTATUSU3EC__ITERATOR1_T3340147720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Elements.FakeElementStatusApi/<setCurrentStatus>c__Iterator1
struct  U3CsetCurrentStatusU3Ec__Iterator1_t3340147720  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.ApproveElementResponse Library.Code.Networking.Elements.FakeElementStatusApi/<setCurrentStatus>c__Iterator1::status
	ApproveElementResponse_t2765772516 * ___status_0;
	// Library.Code.Domain.Entities.Element Library.Code.Networking.Elements.FakeElementStatusApi/<setCurrentStatus>c__Iterator1::element
	Element_t2276588008 * ___element_1;
	// Library.Code.Networking.ResponseWrapper`1<Library.Code.Domain.Entities.Element> Library.Code.Networking.Elements.FakeElementStatusApi/<setCurrentStatus>c__Iterator1::wrapper
	RuntimeObject* ___wrapper_2;
	// System.Object Library.Code.Networking.Elements.FakeElementStatusApi/<setCurrentStatus>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Library.Code.Networking.Elements.FakeElementStatusApi/<setCurrentStatus>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 Library.Code.Networking.Elements.FakeElementStatusApi/<setCurrentStatus>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_status_0() { return static_cast<int32_t>(offsetof(U3CsetCurrentStatusU3Ec__Iterator1_t3340147720, ___status_0)); }
	inline ApproveElementResponse_t2765772516 * get_status_0() const { return ___status_0; }
	inline ApproveElementResponse_t2765772516 ** get_address_of_status_0() { return &___status_0; }
	inline void set_status_0(ApproveElementResponse_t2765772516 * value)
	{
		___status_0 = value;
		Il2CppCodeGenWriteBarrier((&___status_0), value);
	}

	inline static int32_t get_offset_of_element_1() { return static_cast<int32_t>(offsetof(U3CsetCurrentStatusU3Ec__Iterator1_t3340147720, ___element_1)); }
	inline Element_t2276588008 * get_element_1() const { return ___element_1; }
	inline Element_t2276588008 ** get_address_of_element_1() { return &___element_1; }
	inline void set_element_1(Element_t2276588008 * value)
	{
		___element_1 = value;
		Il2CppCodeGenWriteBarrier((&___element_1), value);
	}

	inline static int32_t get_offset_of_wrapper_2() { return static_cast<int32_t>(offsetof(U3CsetCurrentStatusU3Ec__Iterator1_t3340147720, ___wrapper_2)); }
	inline RuntimeObject* get_wrapper_2() const { return ___wrapper_2; }
	inline RuntimeObject** get_address_of_wrapper_2() { return &___wrapper_2; }
	inline void set_wrapper_2(RuntimeObject* value)
	{
		___wrapper_2 = value;
		Il2CppCodeGenWriteBarrier((&___wrapper_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CsetCurrentStatusU3Ec__Iterator1_t3340147720, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CsetCurrentStatusU3Ec__Iterator1_t3340147720, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CsetCurrentStatusU3Ec__Iterator1_t3340147720, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETCURRENTSTATUSU3EC__ITERATOR1_T3340147720_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef U3CLOADELEMENTSU3EC__ITERATOR0_T594275620_H
#define U3CLOADELEMENTSU3EC__ITERATOR0_T594275620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Elements.ElementApiImp/<loadElements>c__Iterator0
struct  U3CloadElementsU3Ec__Iterator0_t594275620  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest Library.Code.Networking.Elements.ElementApiImp/<loadElements>c__Iterator0::<request>__0
	UnityWebRequest_t254341728 * ___U3CrequestU3E__0_0;
	// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Element>> Library.Code.Networking.Elements.ElementApiImp/<loadElements>c__Iterator0::reposneWrapper
	RuntimeObject* ___reposneWrapper_1;
	// Library.Code.Networking.Elements.ElementApiImp Library.Code.Networking.Elements.ElementApiImp/<loadElements>c__Iterator0::$this
	ElementApiImp_t2310790088 * ___U24this_2;
	// System.Object Library.Code.Networking.Elements.ElementApiImp/<loadElements>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Library.Code.Networking.Elements.ElementApiImp/<loadElements>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Library.Code.Networking.Elements.ElementApiImp/<loadElements>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t594275620, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t254341728 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t254341728 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t254341728 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_reposneWrapper_1() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t594275620, ___reposneWrapper_1)); }
	inline RuntimeObject* get_reposneWrapper_1() const { return ___reposneWrapper_1; }
	inline RuntimeObject** get_address_of_reposneWrapper_1() { return &___reposneWrapper_1; }
	inline void set_reposneWrapper_1(RuntimeObject* value)
	{
		___reposneWrapper_1 = value;
		Il2CppCodeGenWriteBarrier((&___reposneWrapper_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t594275620, ___U24this_2)); }
	inline ElementApiImp_t2310790088 * get_U24this_2() const { return ___U24this_2; }
	inline ElementApiImp_t2310790088 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ElementApiImp_t2310790088 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t594275620, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t594275620, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t594275620, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADELEMENTSU3EC__ITERATOR0_T594275620_H
#ifndef U3CCHANGEITEMSTATUSU3EC__ITERATOR0_T1382815528_H
#define U3CCHANGEITEMSTATUSU3EC__ITERATOR0_T1382815528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Checklist.ChangeCheckListItemStatusApiImp/<changeItemStatus>c__Iterator0
struct  U3CchangeItemStatusU3Ec__Iterator0_t1382815528  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Checklist.ItemStatus Library.Code.Networking.Checklist.ChangeCheckListItemStatusApiImp/<changeItemStatus>c__Iterator0::itemStatus
	ItemStatus_t1378751697 * ___itemStatus_0;
	// System.Int64 Library.Code.Networking.Checklist.ChangeCheckListItemStatusApiImp/<changeItemStatus>c__Iterator0::elementId
	int64_t ___elementId_1;
	// UnityEngine.Networking.UnityWebRequest Library.Code.Networking.Checklist.ChangeCheckListItemStatusApiImp/<changeItemStatus>c__Iterator0::<request>__0
	UnityWebRequest_t254341728 * ___U3CrequestU3E__0_2;
	// UnityEngine.Events.UnityAction`1<Library.Code.Networking.FailReason> Library.Code.Networking.Checklist.ChangeCheckListItemStatusApiImp/<changeItemStatus>c__Iterator0::fail
	UnityAction_1_t1842893265 * ___fail_3;
	// UnityEngine.Events.UnityAction Library.Code.Networking.Checklist.ChangeCheckListItemStatusApiImp/<changeItemStatus>c__Iterator0::success
	UnityAction_t4025899511 * ___success_4;
	// Library.Code.Networking.Checklist.ChangeCheckListItemStatusApiImp Library.Code.Networking.Checklist.ChangeCheckListItemStatusApiImp/<changeItemStatus>c__Iterator0::$this
	ChangeCheckListItemStatusApiImp_t3896993653 * ___U24this_5;
	// System.Object Library.Code.Networking.Checklist.ChangeCheckListItemStatusApiImp/<changeItemStatus>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean Library.Code.Networking.Checklist.ChangeCheckListItemStatusApiImp/<changeItemStatus>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 Library.Code.Networking.Checklist.ChangeCheckListItemStatusApiImp/<changeItemStatus>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_itemStatus_0() { return static_cast<int32_t>(offsetof(U3CchangeItemStatusU3Ec__Iterator0_t1382815528, ___itemStatus_0)); }
	inline ItemStatus_t1378751697 * get_itemStatus_0() const { return ___itemStatus_0; }
	inline ItemStatus_t1378751697 ** get_address_of_itemStatus_0() { return &___itemStatus_0; }
	inline void set_itemStatus_0(ItemStatus_t1378751697 * value)
	{
		___itemStatus_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStatus_0), value);
	}

	inline static int32_t get_offset_of_elementId_1() { return static_cast<int32_t>(offsetof(U3CchangeItemStatusU3Ec__Iterator0_t1382815528, ___elementId_1)); }
	inline int64_t get_elementId_1() const { return ___elementId_1; }
	inline int64_t* get_address_of_elementId_1() { return &___elementId_1; }
	inline void set_elementId_1(int64_t value)
	{
		___elementId_1 = value;
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CchangeItemStatusU3Ec__Iterator0_t1382815528, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t254341728 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t254341728 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t254341728 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_fail_3() { return static_cast<int32_t>(offsetof(U3CchangeItemStatusU3Ec__Iterator0_t1382815528, ___fail_3)); }
	inline UnityAction_1_t1842893265 * get_fail_3() const { return ___fail_3; }
	inline UnityAction_1_t1842893265 ** get_address_of_fail_3() { return &___fail_3; }
	inline void set_fail_3(UnityAction_1_t1842893265 * value)
	{
		___fail_3 = value;
		Il2CppCodeGenWriteBarrier((&___fail_3), value);
	}

	inline static int32_t get_offset_of_success_4() { return static_cast<int32_t>(offsetof(U3CchangeItemStatusU3Ec__Iterator0_t1382815528, ___success_4)); }
	inline UnityAction_t4025899511 * get_success_4() const { return ___success_4; }
	inline UnityAction_t4025899511 ** get_address_of_success_4() { return &___success_4; }
	inline void set_success_4(UnityAction_t4025899511 * value)
	{
		___success_4 = value;
		Il2CppCodeGenWriteBarrier((&___success_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CchangeItemStatusU3Ec__Iterator0_t1382815528, ___U24this_5)); }
	inline ChangeCheckListItemStatusApiImp_t3896993653 * get_U24this_5() const { return ___U24this_5; }
	inline ChangeCheckListItemStatusApiImp_t3896993653 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(ChangeCheckListItemStatusApiImp_t3896993653 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CchangeItemStatusU3Ec__Iterator0_t1382815528, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CchangeItemStatusU3Ec__Iterator0_t1382815528, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CchangeItemStatusU3Ec__Iterator0_t1382815528, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHANGEITEMSTATUSU3EC__ITERATOR0_T1382815528_H
#ifndef FAKECHANGECHECKLISTITEMSTATUSAPI_T3817536300_H
#define FAKECHANGECHECKLISTITEMSTATUSAPI_T3817536300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Checklist.FakeChangeChecklistItemStatusApi
struct  FakeChangeChecklistItemStatusApi_t3817536300  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKECHANGECHECKLISTITEMSTATUSAPI_T3817536300_H
#ifndef U3CCHANGEITEMSTATUSU3EC__ITERATOR0_T3672242215_H
#define U3CCHANGEITEMSTATUSU3EC__ITERATOR0_T3672242215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Checklist.FakeChangeChecklistItemStatusApi/<changeItemStatus>c__Iterator0
struct  U3CchangeItemStatusU3Ec__Iterator0_t3672242215  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityAction Library.Code.Networking.Checklist.FakeChangeChecklistItemStatusApi/<changeItemStatus>c__Iterator0::success
	UnityAction_t4025899511 * ___success_0;
	// System.Object Library.Code.Networking.Checklist.FakeChangeChecklistItemStatusApi/<changeItemStatus>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Library.Code.Networking.Checklist.FakeChangeChecklistItemStatusApi/<changeItemStatus>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Library.Code.Networking.Checklist.FakeChangeChecklistItemStatusApi/<changeItemStatus>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_success_0() { return static_cast<int32_t>(offsetof(U3CchangeItemStatusU3Ec__Iterator0_t3672242215, ___success_0)); }
	inline UnityAction_t4025899511 * get_success_0() const { return ___success_0; }
	inline UnityAction_t4025899511 ** get_address_of_success_0() { return &___success_0; }
	inline void set_success_0(UnityAction_t4025899511 * value)
	{
		___success_0 = value;
		Il2CppCodeGenWriteBarrier((&___success_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CchangeItemStatusU3Ec__Iterator0_t3672242215, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CchangeItemStatusU3Ec__Iterator0_t3672242215, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CchangeItemStatusU3Ec__Iterator0_t3672242215, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHANGEITEMSTATUSU3EC__ITERATOR0_T3672242215_H
#ifndef FAKELOADCHECKLISTFORELEMENTAPI_T3867601896_H
#define FAKELOADCHECKLISTFORELEMENTAPI_T3867601896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Checklist.FakeLoadChecklistForElementApi
struct  FakeLoadChecklistForElementApi_t3867601896  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKELOADCHECKLISTFORELEMENTAPI_T3867601896_H
#ifndef U3CLOADELEMENTSU3EC__ITERATOR0_T2255347523_H
#define U3CLOADELEMENTSU3EC__ITERATOR0_T2255347523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Checklist.FakeLoadChecklistForElementApi/<loadElements>c__Iterator0
struct  U3CloadElementsU3Ec__Iterator0_t2255347523  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Checklist.CheckList> Library.Code.Networking.Checklist.FakeLoadChecklistForElementApi/<loadElements>c__Iterator0::<list>__0
	List_1_t3501169210 * ___U3ClistU3E__0_0;
	// Library.Code.Domain.Entities.Element Library.Code.Networking.Checklist.FakeLoadChecklistForElementApi/<loadElements>c__Iterator0::element
	Element_t2276588008 * ___element_1;
	// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Checklist.CheckList>> Library.Code.Networking.Checklist.FakeLoadChecklistForElementApi/<loadElements>c__Iterator0::reposneWrapper
	RuntimeObject* ___reposneWrapper_2;
	// System.Object Library.Code.Networking.Checklist.FakeLoadChecklistForElementApi/<loadElements>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Library.Code.Networking.Checklist.FakeLoadChecklistForElementApi/<loadElements>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Library.Code.Networking.Checklist.FakeLoadChecklistForElementApi/<loadElements>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3ClistU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t2255347523, ___U3ClistU3E__0_0)); }
	inline List_1_t3501169210 * get_U3ClistU3E__0_0() const { return ___U3ClistU3E__0_0; }
	inline List_1_t3501169210 ** get_address_of_U3ClistU3E__0_0() { return &___U3ClistU3E__0_0; }
	inline void set_U3ClistU3E__0_0(List_1_t3501169210 * value)
	{
		___U3ClistU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClistU3E__0_0), value);
	}

	inline static int32_t get_offset_of_element_1() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t2255347523, ___element_1)); }
	inline Element_t2276588008 * get_element_1() const { return ___element_1; }
	inline Element_t2276588008 ** get_address_of_element_1() { return &___element_1; }
	inline void set_element_1(Element_t2276588008 * value)
	{
		___element_1 = value;
		Il2CppCodeGenWriteBarrier((&___element_1), value);
	}

	inline static int32_t get_offset_of_reposneWrapper_2() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t2255347523, ___reposneWrapper_2)); }
	inline RuntimeObject* get_reposneWrapper_2() const { return ___reposneWrapper_2; }
	inline RuntimeObject** get_address_of_reposneWrapper_2() { return &___reposneWrapper_2; }
	inline void set_reposneWrapper_2(RuntimeObject* value)
	{
		___reposneWrapper_2 = value;
		Il2CppCodeGenWriteBarrier((&___reposneWrapper_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t2255347523, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t2255347523, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t2255347523, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADELEMENTSU3EC__ITERATOR0_T2255347523_H
#ifndef LOADCHECKLISTFORELEMENTAPIIMP_T310732585_H
#define LOADCHECKLISTFORELEMENTAPIIMP_T310732585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Checklist.LoadChecklistForElementApiImp
struct  LoadChecklistForElementApiImp_t310732585  : public RuntimeObject
{
public:
	// Library.Code.Parsers.JsonParser`1<Library.Code.Domain.Entities.Checklist.CheckListResponse> Library.Code.Networking.Checklist.LoadChecklistForElementApiImp::_parser
	RuntimeObject* ____parser_0;
	// System.String Library.Code.Networking.Checklist.LoadChecklistForElementApiImp::url
	String_t* ___url_1;
	// Library.Code.WebRequest.WebRequestBuilder Library.Code.Networking.Checklist.LoadChecklistForElementApiImp::_builder
	WebRequestBuilder_t3983605310 * ____builder_2;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(LoadChecklistForElementApiImp_t310732585, ____parser_0)); }
	inline RuntimeObject* get__parser_0() const { return ____parser_0; }
	inline RuntimeObject** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(RuntimeObject* value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((&____parser_0), value);
	}

	inline static int32_t get_offset_of_url_1() { return static_cast<int32_t>(offsetof(LoadChecklistForElementApiImp_t310732585, ___url_1)); }
	inline String_t* get_url_1() const { return ___url_1; }
	inline String_t** get_address_of_url_1() { return &___url_1; }
	inline void set_url_1(String_t* value)
	{
		___url_1 = value;
		Il2CppCodeGenWriteBarrier((&___url_1), value);
	}

	inline static int32_t get_offset_of__builder_2() { return static_cast<int32_t>(offsetof(LoadChecklistForElementApiImp_t310732585, ____builder_2)); }
	inline WebRequestBuilder_t3983605310 * get__builder_2() const { return ____builder_2; }
	inline WebRequestBuilder_t3983605310 ** get_address_of__builder_2() { return &____builder_2; }
	inline void set__builder_2(WebRequestBuilder_t3983605310 * value)
	{
		____builder_2 = value;
		Il2CppCodeGenWriteBarrier((&____builder_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADCHECKLISTFORELEMENTAPIIMP_T310732585_H
#ifndef U3CLOADELEMENTSU3EC__ITERATOR0_T354627748_H
#define U3CLOADELEMENTSU3EC__ITERATOR0_T354627748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Checklist.LoadChecklistForElementApiImp/<loadElements>c__Iterator0
struct  U3CloadElementsU3Ec__Iterator0_t354627748  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Element Library.Code.Networking.Checklist.LoadChecklistForElementApiImp/<loadElements>c__Iterator0::element
	Element_t2276588008 * ___element_0;
	// UnityEngine.Networking.UnityWebRequest Library.Code.Networking.Checklist.LoadChecklistForElementApiImp/<loadElements>c__Iterator0::<request>__0
	UnityWebRequest_t254341728 * ___U3CrequestU3E__0_1;
	// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Checklist.CheckList>> Library.Code.Networking.Checklist.LoadChecklistForElementApiImp/<loadElements>c__Iterator0::reposneWrapper
	RuntimeObject* ___reposneWrapper_2;
	// Library.Code.Networking.Checklist.LoadChecklistForElementApiImp Library.Code.Networking.Checklist.LoadChecklistForElementApiImp/<loadElements>c__Iterator0::$this
	LoadChecklistForElementApiImp_t310732585 * ___U24this_3;
	// System.Object Library.Code.Networking.Checklist.LoadChecklistForElementApiImp/<loadElements>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Library.Code.Networking.Checklist.LoadChecklistForElementApiImp/<loadElements>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Library.Code.Networking.Checklist.LoadChecklistForElementApiImp/<loadElements>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_element_0() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t354627748, ___element_0)); }
	inline Element_t2276588008 * get_element_0() const { return ___element_0; }
	inline Element_t2276588008 ** get_address_of_element_0() { return &___element_0; }
	inline void set_element_0(Element_t2276588008 * value)
	{
		___element_0 = value;
		Il2CppCodeGenWriteBarrier((&___element_0), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t354627748, ___U3CrequestU3E__0_1)); }
	inline UnityWebRequest_t254341728 * get_U3CrequestU3E__0_1() const { return ___U3CrequestU3E__0_1; }
	inline UnityWebRequest_t254341728 ** get_address_of_U3CrequestU3E__0_1() { return &___U3CrequestU3E__0_1; }
	inline void set_U3CrequestU3E__0_1(UnityWebRequest_t254341728 * value)
	{
		___U3CrequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_1), value);
	}

	inline static int32_t get_offset_of_reposneWrapper_2() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t354627748, ___reposneWrapper_2)); }
	inline RuntimeObject* get_reposneWrapper_2() const { return ___reposneWrapper_2; }
	inline RuntimeObject** get_address_of_reposneWrapper_2() { return &___reposneWrapper_2; }
	inline void set_reposneWrapper_2(RuntimeObject* value)
	{
		___reposneWrapper_2 = value;
		Il2CppCodeGenWriteBarrier((&___reposneWrapper_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t354627748, ___U24this_3)); }
	inline LoadChecklistForElementApiImp_t310732585 * get_U24this_3() const { return ___U24this_3; }
	inline LoadChecklistForElementApiImp_t310732585 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(LoadChecklistForElementApiImp_t310732585 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t354627748, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t354627748, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t354627748, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADELEMENTSU3EC__ITERATOR0_T354627748_H
#ifndef ELEMENTAPIIMP_T2310790088_H
#define ELEMENTAPIIMP_T2310790088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Elements.ElementApiImp
struct  ElementApiImp_t2310790088  : public RuntimeObject
{
public:
	// Library.Code.Parsers.JsonParser`1<Library.Code.Domain.Entities.ElementsEmbedded> Library.Code.Networking.Elements.ElementApiImp::_parser
	RuntimeObject* ____parser_0;
	// System.String Library.Code.Networking.Elements.ElementApiImp::url
	String_t* ___url_1;
	// Library.Code.WebRequest.WebRequestBuilder Library.Code.Networking.Elements.ElementApiImp::_builder
	WebRequestBuilder_t3983605310 * ____builder_2;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(ElementApiImp_t2310790088, ____parser_0)); }
	inline RuntimeObject* get__parser_0() const { return ____parser_0; }
	inline RuntimeObject** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(RuntimeObject* value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((&____parser_0), value);
	}

	inline static int32_t get_offset_of_url_1() { return static_cast<int32_t>(offsetof(ElementApiImp_t2310790088, ___url_1)); }
	inline String_t* get_url_1() const { return ___url_1; }
	inline String_t** get_address_of_url_1() { return &___url_1; }
	inline void set_url_1(String_t* value)
	{
		___url_1 = value;
		Il2CppCodeGenWriteBarrier((&___url_1), value);
	}

	inline static int32_t get_offset_of__builder_2() { return static_cast<int32_t>(offsetof(ElementApiImp_t2310790088, ____builder_2)); }
	inline WebRequestBuilder_t3983605310 * get__builder_2() const { return ____builder_2; }
	inline WebRequestBuilder_t3983605310 ** get_address_of__builder_2() { return &____builder_2; }
	inline void set__builder_2(WebRequestBuilder_t3983605310 * value)
	{
		____builder_2 = value;
		Il2CppCodeGenWriteBarrier((&____builder_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTAPIIMP_T2310790088_H
#ifndef U3CSAVEU3EC__ANONSTOREY0_T2000402439_H
#define U3CSAVEU3EC__ANONSTOREY0_T2000402439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Models.ModelFacadeImpl/<save>c__AnonStorey0
struct  U3CsaveU3Ec__AnonStorey0_t2000402439  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Model Library.Code.Facades.Models.ModelFacadeImpl/<save>c__AnonStorey0::models
	Model_t2163629551 * ___models_0;
	// Library.Code.Facades.Callback Library.Code.Facades.Models.ModelFacadeImpl/<save>c__AnonStorey0::persistCallback
	RuntimeObject* ___persistCallback_1;
	// Library.Code.Facades.Models.ModelFacadeImpl Library.Code.Facades.Models.ModelFacadeImpl/<save>c__AnonStorey0::$this
	ModelFacadeImpl_t3221237801 * ___U24this_2;

public:
	inline static int32_t get_offset_of_models_0() { return static_cast<int32_t>(offsetof(U3CsaveU3Ec__AnonStorey0_t2000402439, ___models_0)); }
	inline Model_t2163629551 * get_models_0() const { return ___models_0; }
	inline Model_t2163629551 ** get_address_of_models_0() { return &___models_0; }
	inline void set_models_0(Model_t2163629551 * value)
	{
		___models_0 = value;
		Il2CppCodeGenWriteBarrier((&___models_0), value);
	}

	inline static int32_t get_offset_of_persistCallback_1() { return static_cast<int32_t>(offsetof(U3CsaveU3Ec__AnonStorey0_t2000402439, ___persistCallback_1)); }
	inline RuntimeObject* get_persistCallback_1() const { return ___persistCallback_1; }
	inline RuntimeObject** get_address_of_persistCallback_1() { return &___persistCallback_1; }
	inline void set_persistCallback_1(RuntimeObject* value)
	{
		___persistCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___persistCallback_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CsaveU3Ec__AnonStorey0_t2000402439, ___U24this_2)); }
	inline ModelFacadeImpl_t3221237801 * get_U24this_2() const { return ___U24this_2; }
	inline ModelFacadeImpl_t3221237801 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ModelFacadeImpl_t3221237801 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSAVEU3EC__ANONSTOREY0_T2000402439_H
#ifndef U3CLOADELEMENTSU3EC__ITERATOR0_T2433942208_H
#define U3CLOADELEMENTSU3EC__ITERATOR0_T2433942208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Elements.FakeElementApi/<loadElements>c__Iterator0
struct  U3CloadElementsU3Ec__Iterator0_t2433942208  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Element> Library.Code.Facades.Elements.FakeElementApi/<loadElements>c__Iterator0::<fakeElements>__0
	List_1_t1645709140 * ___U3CfakeElementsU3E__0_0;
	// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Element>> Library.Code.Facades.Elements.FakeElementApi/<loadElements>c__Iterator0::reposneWrapper
	RuntimeObject* ___reposneWrapper_1;
	// System.Object Library.Code.Facades.Elements.FakeElementApi/<loadElements>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Library.Code.Facades.Elements.FakeElementApi/<loadElements>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Library.Code.Facades.Elements.FakeElementApi/<loadElements>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CfakeElementsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t2433942208, ___U3CfakeElementsU3E__0_0)); }
	inline List_1_t1645709140 * get_U3CfakeElementsU3E__0_0() const { return ___U3CfakeElementsU3E__0_0; }
	inline List_1_t1645709140 ** get_address_of_U3CfakeElementsU3E__0_0() { return &___U3CfakeElementsU3E__0_0; }
	inline void set_U3CfakeElementsU3E__0_0(List_1_t1645709140 * value)
	{
		___U3CfakeElementsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfakeElementsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_reposneWrapper_1() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t2433942208, ___reposneWrapper_1)); }
	inline RuntimeObject* get_reposneWrapper_1() const { return ___reposneWrapper_1; }
	inline RuntimeObject** get_address_of_reposneWrapper_1() { return &___reposneWrapper_1; }
	inline void set_reposneWrapper_1(RuntimeObject* value)
	{
		___reposneWrapper_1 = value;
		Il2CppCodeGenWriteBarrier((&___reposneWrapper_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t2433942208, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t2433942208, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CloadElementsU3Ec__Iterator0_t2433942208, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADELEMENTSU3EC__ITERATOR0_T2433942208_H
#ifndef FAKESENDFILEAPI_T117775171_H
#define FAKESENDFILEAPI_T117775171_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Files.FakeSendFileApi
struct  FakeSendFileApi_t117775171  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKESENDFILEAPI_T117775171_H
#ifndef SENDFILEFACADEIMPL_T1613402490_H
#define SENDFILEFACADEIMPL_T1613402490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Files.SendFileFacadeImpl
struct  SendFileFacadeImpl_t1613402490  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Message> Library.Code.Facades.Files.SendFileFacadeImpl::messages
	List_1_t583975089 * ___messages_0;
	// Library.Code.Networking.Files.SendFileApi Library.Code.Facades.Files.SendFileFacadeImpl::sendFileApi
	RuntimeObject* ___sendFileApi_1;
	// Library.Code.Utils.FileReader.FileReader Library.Code.Facades.Files.SendFileFacadeImpl::fileReader
	RuntimeObject* ___fileReader_2;
	// Library.Code.Utils.Async.DoAsync Library.Code.Facades.Files.SendFileFacadeImpl::doAsync
	RuntimeObject* ___doAsync_3;

public:
	inline static int32_t get_offset_of_messages_0() { return static_cast<int32_t>(offsetof(SendFileFacadeImpl_t1613402490, ___messages_0)); }
	inline List_1_t583975089 * get_messages_0() const { return ___messages_0; }
	inline List_1_t583975089 ** get_address_of_messages_0() { return &___messages_0; }
	inline void set_messages_0(List_1_t583975089 * value)
	{
		___messages_0 = value;
		Il2CppCodeGenWriteBarrier((&___messages_0), value);
	}

	inline static int32_t get_offset_of_sendFileApi_1() { return static_cast<int32_t>(offsetof(SendFileFacadeImpl_t1613402490, ___sendFileApi_1)); }
	inline RuntimeObject* get_sendFileApi_1() const { return ___sendFileApi_1; }
	inline RuntimeObject** get_address_of_sendFileApi_1() { return &___sendFileApi_1; }
	inline void set_sendFileApi_1(RuntimeObject* value)
	{
		___sendFileApi_1 = value;
		Il2CppCodeGenWriteBarrier((&___sendFileApi_1), value);
	}

	inline static int32_t get_offset_of_fileReader_2() { return static_cast<int32_t>(offsetof(SendFileFacadeImpl_t1613402490, ___fileReader_2)); }
	inline RuntimeObject* get_fileReader_2() const { return ___fileReader_2; }
	inline RuntimeObject** get_address_of_fileReader_2() { return &___fileReader_2; }
	inline void set_fileReader_2(RuntimeObject* value)
	{
		___fileReader_2 = value;
		Il2CppCodeGenWriteBarrier((&___fileReader_2), value);
	}

	inline static int32_t get_offset_of_doAsync_3() { return static_cast<int32_t>(offsetof(SendFileFacadeImpl_t1613402490, ___doAsync_3)); }
	inline RuntimeObject* get_doAsync_3() const { return ___doAsync_3; }
	inline RuntimeObject** get_address_of_doAsync_3() { return &___doAsync_3; }
	inline void set_doAsync_3(RuntimeObject* value)
	{
		___doAsync_3 = value;
		Il2CppCodeGenWriteBarrier((&___doAsync_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDFILEFACADEIMPL_T1613402490_H
#ifndef FAKEGROUPCREATION_T2089367363_H
#define FAKEGROUPCREATION_T2089367363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Messages.FakeGroupCreation
struct  FakeGroupCreation_t2089367363  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEGROUPCREATION_T2089367363_H
#ifndef U3CCREATEGROUPU3EC__ITERATOR0_T621978403_H
#define U3CCREATEGROUPU3EC__ITERATOR0_T621978403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Messages.FakeGroupCreation/<createGroup>c__Iterator0
struct  U3CcreateGroupU3Ec__Iterator0_t621978403  : public RuntimeObject
{
public:
	// Library.Code.Networking.ResponseWrapper`1<System.Int64> Library.Code.Facades.Messages.FakeGroupCreation/<createGroup>c__Iterator0::callback
	RuntimeObject* ___callback_0;
	// System.Object Library.Code.Facades.Messages.FakeGroupCreation/<createGroup>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Library.Code.Facades.Messages.FakeGroupCreation/<createGroup>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Library.Code.Facades.Messages.FakeGroupCreation/<createGroup>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CcreateGroupU3Ec__Iterator0_t621978403, ___callback_0)); }
	inline RuntimeObject* get_callback_0() const { return ___callback_0; }
	inline RuntimeObject** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(RuntimeObject* value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CcreateGroupU3Ec__Iterator0_t621978403, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CcreateGroupU3Ec__Iterator0_t621978403, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CcreateGroupU3Ec__Iterator0_t621978403, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATEGROUPU3EC__ITERATOR0_T621978403_H
#ifndef FAKESENDBATCHMESSAGES_T2772032319_H
#define FAKESENDBATCHMESSAGES_T2772032319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Messages.FakeSendBatchMessages
struct  FakeSendBatchMessages_t2772032319  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKESENDBATCHMESSAGES_T2772032319_H
#ifndef U3CSENDBATCHMESSAGESU3EC__ITERATOR0_T863552472_H
#define U3CSENDBATCHMESSAGESU3EC__ITERATOR0_T863552472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Messages.FakeSendBatchMessages/<sendBatchMessages>c__Iterator0
struct  U3CsendBatchMessagesU3Ec__Iterator0_t863552472  : public RuntimeObject
{
public:
	// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Message>> Library.Code.Facades.Messages.FakeSendBatchMessages/<sendBatchMessages>c__Iterator0::callback
	RuntimeObject* ___callback_0;
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Message> Library.Code.Facades.Messages.FakeSendBatchMessages/<sendBatchMessages>c__Iterator0::messages
	List_1_t583975089 * ___messages_1;
	// System.Object Library.Code.Facades.Messages.FakeSendBatchMessages/<sendBatchMessages>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Library.Code.Facades.Messages.FakeSendBatchMessages/<sendBatchMessages>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Library.Code.Facades.Messages.FakeSendBatchMessages/<sendBatchMessages>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CsendBatchMessagesU3Ec__Iterator0_t863552472, ___callback_0)); }
	inline RuntimeObject* get_callback_0() const { return ___callback_0; }
	inline RuntimeObject** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(RuntimeObject* value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_messages_1() { return static_cast<int32_t>(offsetof(U3CsendBatchMessagesU3Ec__Iterator0_t863552472, ___messages_1)); }
	inline List_1_t583975089 * get_messages_1() const { return ___messages_1; }
	inline List_1_t583975089 ** get_address_of_messages_1() { return &___messages_1; }
	inline void set_messages_1(List_1_t583975089 * value)
	{
		___messages_1 = value;
		Il2CppCodeGenWriteBarrier((&___messages_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CsendBatchMessagesU3Ec__Iterator0_t863552472, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CsendBatchMessagesU3Ec__Iterator0_t863552472, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CsendBatchMessagesU3Ec__Iterator0_t863552472, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDBATCHMESSAGESU3EC__ITERATOR0_T863552472_H
#ifndef MESSAGEPERSISTANCE_T1067416742_H
#define MESSAGEPERSISTANCE_T1067416742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Messages.MessagePersistance
struct  MessagePersistance_t1067416742  : public RuntimeObject
{
public:
	// System.String Library.Code.Facades.Messages.MessagePersistance::_filename
	String_t* ____filename_0;
	// Library.Code.Persistance.FileJsonPersistance`1<Library.Code.Domain.Dtos.MessageListWrapper> Library.Code.Facades.Messages.MessagePersistance::filePresistance
	FileJsonPersistance_1_t961952247 * ___filePresistance_1;
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Message> Library.Code.Facades.Messages.MessagePersistance::_messages
	List_1_t583975089 * ____messages_2;

public:
	inline static int32_t get_offset_of__filename_0() { return static_cast<int32_t>(offsetof(MessagePersistance_t1067416742, ____filename_0)); }
	inline String_t* get__filename_0() const { return ____filename_0; }
	inline String_t** get_address_of__filename_0() { return &____filename_0; }
	inline void set__filename_0(String_t* value)
	{
		____filename_0 = value;
		Il2CppCodeGenWriteBarrier((&____filename_0), value);
	}

	inline static int32_t get_offset_of_filePresistance_1() { return static_cast<int32_t>(offsetof(MessagePersistance_t1067416742, ___filePresistance_1)); }
	inline FileJsonPersistance_1_t961952247 * get_filePresistance_1() const { return ___filePresistance_1; }
	inline FileJsonPersistance_1_t961952247 ** get_address_of_filePresistance_1() { return &___filePresistance_1; }
	inline void set_filePresistance_1(FileJsonPersistance_1_t961952247 * value)
	{
		___filePresistance_1 = value;
		Il2CppCodeGenWriteBarrier((&___filePresistance_1), value);
	}

	inline static int32_t get_offset_of__messages_2() { return static_cast<int32_t>(offsetof(MessagePersistance_t1067416742, ____messages_2)); }
	inline List_1_t583975089 * get__messages_2() const { return ____messages_2; }
	inline List_1_t583975089 ** get_address_of__messages_2() { return &____messages_2; }
	inline void set__messages_2(List_1_t583975089 * value)
	{
		____messages_2 = value;
		Il2CppCodeGenWriteBarrier((&____messages_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEPERSISTANCE_T1067416742_H
#ifndef FAKEELEMENTAPI_T1095413803_H
#define FAKEELEMENTAPI_T1095413803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Elements.FakeElementApi
struct  FakeElementApi_t1095413803  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEELEMENTAPI_T1095413803_H
#ifndef U3CGETU3EC__ANONSTOREY2_T1130253552_H
#define U3CGETU3EC__ANONSTOREY2_T1130253552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Elements.ElementFacadeImpl/<get>c__AnonStorey2
struct  U3CgetU3Ec__AnonStorey2_t1130253552  : public RuntimeObject
{
public:
	// Library.Code.Facades.Callback Library.Code.Facades.Elements.ElementFacadeImpl/<get>c__AnonStorey2::persistCallback
	RuntimeObject* ___persistCallback_0;

public:
	inline static int32_t get_offset_of_persistCallback_0() { return static_cast<int32_t>(offsetof(U3CgetU3Ec__AnonStorey2_t1130253552, ___persistCallback_0)); }
	inline RuntimeObject* get_persistCallback_0() const { return ___persistCallback_0; }
	inline RuntimeObject** get_address_of_persistCallback_0() { return &___persistCallback_0; }
	inline void set_persistCallback_0(RuntimeObject* value)
	{
		___persistCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___persistCallback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETU3EC__ANONSTOREY2_T1130253552_H
#ifndef U3CLOADU3EC__ANONSTOREY3_T3334096223_H
#define U3CLOADU3EC__ANONSTOREY3_T3334096223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Elements.ElementFacadeImpl/<load>c__AnonStorey3
struct  U3CloadU3Ec__AnonStorey3_t3334096223  : public RuntimeObject
{
public:
	// System.Int64 Library.Code.Facades.Elements.ElementFacadeImpl/<load>c__AnonStorey3::id
	int64_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CloadU3Ec__AnonStorey3_t3334096223, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADU3EC__ANONSTOREY3_T3334096223_H
#ifndef U3CUPDATEOBSERVERSU3EC__ANONSTOREY4_T3475232138_H
#define U3CUPDATEOBSERVERSU3EC__ANONSTOREY4_T3475232138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Elements.ElementFacadeImpl/<updateObservers>c__AnonStorey4
struct  U3CupdateObserversU3Ec__AnonStorey4_t3475232138  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Element> Library.Code.Facades.Elements.ElementFacadeImpl/<updateObservers>c__AnonStorey4::messages
	List_1_t1645709140 * ___messages_0;

public:
	inline static int32_t get_offset_of_messages_0() { return static_cast<int32_t>(offsetof(U3CupdateObserversU3Ec__AnonStorey4_t3475232138, ___messages_0)); }
	inline List_1_t1645709140 * get_messages_0() const { return ___messages_0; }
	inline List_1_t1645709140 ** get_address_of_messages_0() { return &___messages_0; }
	inline void set_messages_0(List_1_t1645709140 * value)
	{
		___messages_0 = value;
		Il2CppCodeGenWriteBarrier((&___messages_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEOBSERVERSU3EC__ANONSTOREY4_T3475232138_H
#ifndef U3CLOADFORMODELU3EC__ANONSTOREY5_T2869201201_H
#define U3CLOADFORMODELU3EC__ANONSTOREY5_T2869201201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Elements.ElementFacadeImpl/<loadForModel>c__AnonStorey5
struct  U3CloadForModelU3Ec__AnonStorey5_t2869201201  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Model Library.Code.Facades.Elements.ElementFacadeImpl/<loadForModel>c__AnonStorey5::model
	Model_t2163629551 * ___model_0;

public:
	inline static int32_t get_offset_of_model_0() { return static_cast<int32_t>(offsetof(U3CloadForModelU3Ec__AnonStorey5_t2869201201, ___model_0)); }
	inline Model_t2163629551 * get_model_0() const { return ___model_0; }
	inline Model_t2163629551 ** get_address_of_model_0() { return &___model_0; }
	inline void set_model_0(Model_t2163629551 * value)
	{
		___model_0 = value;
		Il2CppCodeGenWriteBarrier((&___model_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADFORMODELU3EC__ANONSTOREY5_T2869201201_H
#ifndef ELEMENTPERSISTANCEIMPL_T1641319881_H
#define ELEMENTPERSISTANCEIMPL_T1641319881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Elements.ElementPersistanceImpl
struct  ElementPersistanceImpl_t1641319881  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Element> Library.Code.Facades.Elements.ElementPersistanceImpl::_elements
	List_1_t1645709140 * ____elements_0;
	// Library.Code.Persistance.FileJsonPersistance`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Element>> Library.Code.Facades.Elements.ElementPersistanceImpl::filePresistance
	FileJsonPersistance_1_t762880847 * ___filePresistance_1;
	// System.String Library.Code.Facades.Elements.ElementPersistanceImpl::_filename
	String_t* ____filename_2;

public:
	inline static int32_t get_offset_of__elements_0() { return static_cast<int32_t>(offsetof(ElementPersistanceImpl_t1641319881, ____elements_0)); }
	inline List_1_t1645709140 * get__elements_0() const { return ____elements_0; }
	inline List_1_t1645709140 ** get_address_of__elements_0() { return &____elements_0; }
	inline void set__elements_0(List_1_t1645709140 * value)
	{
		____elements_0 = value;
		Il2CppCodeGenWriteBarrier((&____elements_0), value);
	}

	inline static int32_t get_offset_of_filePresistance_1() { return static_cast<int32_t>(offsetof(ElementPersistanceImpl_t1641319881, ___filePresistance_1)); }
	inline FileJsonPersistance_1_t762880847 * get_filePresistance_1() const { return ___filePresistance_1; }
	inline FileJsonPersistance_1_t762880847 ** get_address_of_filePresistance_1() { return &___filePresistance_1; }
	inline void set_filePresistance_1(FileJsonPersistance_1_t762880847 * value)
	{
		___filePresistance_1 = value;
		Il2CppCodeGenWriteBarrier((&___filePresistance_1), value);
	}

	inline static int32_t get_offset_of__filename_2() { return static_cast<int32_t>(offsetof(ElementPersistanceImpl_t1641319881, ____filename_2)); }
	inline String_t* get__filename_2() const { return ____filename_2; }
	inline String_t** get_address_of__filename_2() { return &____filename_2; }
	inline void set__filename_2(String_t* value)
	{
		____filename_2 = value;
		Il2CppCodeGenWriteBarrier((&____filename_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTPERSISTANCEIMPL_T1641319881_H
#ifndef U3CGETALLU3EC__ITERATOR0_T3007678366_H
#define U3CGETALLU3EC__ITERATOR0_T3007678366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Elements.ElementPersistanceImpl/<getAll>c__Iterator0
struct  U3CgetAllU3Ec__Iterator0_t3007678366  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Element> Library.Code.Facades.Elements.ElementPersistanceImpl/<getAll>c__Iterator0::<list>__0
	List_1_t1645709140 * ___U3ClistU3E__0_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Element>> Library.Code.Facades.Elements.ElementPersistanceImpl/<getAll>c__Iterator0::success
	UnityAction_1_t3012294891 * ___success_1;
	// Library.Code.Facades.Elements.ElementPersistanceImpl Library.Code.Facades.Elements.ElementPersistanceImpl/<getAll>c__Iterator0::$this
	ElementPersistanceImpl_t1641319881 * ___U24this_2;
	// System.Object Library.Code.Facades.Elements.ElementPersistanceImpl/<getAll>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Library.Code.Facades.Elements.ElementPersistanceImpl/<getAll>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Library.Code.Facades.Elements.ElementPersistanceImpl/<getAll>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3ClistU3E__0_0() { return static_cast<int32_t>(offsetof(U3CgetAllU3Ec__Iterator0_t3007678366, ___U3ClistU3E__0_0)); }
	inline List_1_t1645709140 * get_U3ClistU3E__0_0() const { return ___U3ClistU3E__0_0; }
	inline List_1_t1645709140 ** get_address_of_U3ClistU3E__0_0() { return &___U3ClistU3E__0_0; }
	inline void set_U3ClistU3E__0_0(List_1_t1645709140 * value)
	{
		___U3ClistU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClistU3E__0_0), value);
	}

	inline static int32_t get_offset_of_success_1() { return static_cast<int32_t>(offsetof(U3CgetAllU3Ec__Iterator0_t3007678366, ___success_1)); }
	inline UnityAction_1_t3012294891 * get_success_1() const { return ___success_1; }
	inline UnityAction_1_t3012294891 ** get_address_of_success_1() { return &___success_1; }
	inline void set_success_1(UnityAction_1_t3012294891 * value)
	{
		___success_1 = value;
		Il2CppCodeGenWriteBarrier((&___success_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CgetAllU3Ec__Iterator0_t3007678366, ___U24this_2)); }
	inline ElementPersistanceImpl_t1641319881 * get_U24this_2() const { return ___U24this_2; }
	inline ElementPersistanceImpl_t1641319881 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ElementPersistanceImpl_t1641319881 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CgetAllU3Ec__Iterator0_t3007678366, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CgetAllU3Ec__Iterator0_t3007678366, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CgetAllU3Ec__Iterator0_t3007678366, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLU3EC__ITERATOR0_T3007678366_H
#ifndef U3CADDU3EC__ITERATOR1_T2643060007_H
#define U3CADDU3EC__ITERATOR1_T2643060007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Elements.ElementPersistanceImpl/<add>c__Iterator1
struct  U3CaddU3Ec__Iterator1_t2643060007  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Element Library.Code.Facades.Elements.ElementPersistanceImpl/<add>c__Iterator1::message
	Element_t2276588008 * ___message_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Element>> Library.Code.Facades.Elements.ElementPersistanceImpl/<add>c__Iterator1::success
	UnityAction_1_t3012294891 * ___success_1;
	// Library.Code.Facades.Elements.ElementPersistanceImpl Library.Code.Facades.Elements.ElementPersistanceImpl/<add>c__Iterator1::$this
	ElementPersistanceImpl_t1641319881 * ___U24this_2;
	// System.Object Library.Code.Facades.Elements.ElementPersistanceImpl/<add>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Library.Code.Facades.Elements.ElementPersistanceImpl/<add>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 Library.Code.Facades.Elements.ElementPersistanceImpl/<add>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_message_0() { return static_cast<int32_t>(offsetof(U3CaddU3Ec__Iterator1_t2643060007, ___message_0)); }
	inline Element_t2276588008 * get_message_0() const { return ___message_0; }
	inline Element_t2276588008 ** get_address_of_message_0() { return &___message_0; }
	inline void set_message_0(Element_t2276588008 * value)
	{
		___message_0 = value;
		Il2CppCodeGenWriteBarrier((&___message_0), value);
	}

	inline static int32_t get_offset_of_success_1() { return static_cast<int32_t>(offsetof(U3CaddU3Ec__Iterator1_t2643060007, ___success_1)); }
	inline UnityAction_1_t3012294891 * get_success_1() const { return ___success_1; }
	inline UnityAction_1_t3012294891 ** get_address_of_success_1() { return &___success_1; }
	inline void set_success_1(UnityAction_1_t3012294891 * value)
	{
		___success_1 = value;
		Il2CppCodeGenWriteBarrier((&___success_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CaddU3Ec__Iterator1_t2643060007, ___U24this_2)); }
	inline ElementPersistanceImpl_t1641319881 * get_U24this_2() const { return ___U24this_2; }
	inline ElementPersistanceImpl_t1641319881 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ElementPersistanceImpl_t1641319881 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CaddU3Ec__Iterator1_t2643060007, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CaddU3Ec__Iterator1_t2643060007, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CaddU3Ec__Iterator1_t2643060007, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDU3EC__ITERATOR1_T2643060007_H
#ifndef U3CREMOVEU3EC__ITERATOR2_T1746054447_H
#define U3CREMOVEU3EC__ITERATOR2_T1746054447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Elements.ElementPersistanceImpl/<remove>c__Iterator2
struct  U3CremoveU3Ec__Iterator2_t1746054447  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Element Library.Code.Facades.Elements.ElementPersistanceImpl/<remove>c__Iterator2::message
	Element_t2276588008 * ___message_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Element>> Library.Code.Facades.Elements.ElementPersistanceImpl/<remove>c__Iterator2::success
	UnityAction_1_t3012294891 * ___success_1;
	// Library.Code.Facades.Elements.ElementPersistanceImpl Library.Code.Facades.Elements.ElementPersistanceImpl/<remove>c__Iterator2::$this
	ElementPersistanceImpl_t1641319881 * ___U24this_2;
	// System.Object Library.Code.Facades.Elements.ElementPersistanceImpl/<remove>c__Iterator2::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Library.Code.Facades.Elements.ElementPersistanceImpl/<remove>c__Iterator2::$disposing
	bool ___U24disposing_4;
	// System.Int32 Library.Code.Facades.Elements.ElementPersistanceImpl/<remove>c__Iterator2::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_message_0() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__Iterator2_t1746054447, ___message_0)); }
	inline Element_t2276588008 * get_message_0() const { return ___message_0; }
	inline Element_t2276588008 ** get_address_of_message_0() { return &___message_0; }
	inline void set_message_0(Element_t2276588008 * value)
	{
		___message_0 = value;
		Il2CppCodeGenWriteBarrier((&___message_0), value);
	}

	inline static int32_t get_offset_of_success_1() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__Iterator2_t1746054447, ___success_1)); }
	inline UnityAction_1_t3012294891 * get_success_1() const { return ___success_1; }
	inline UnityAction_1_t3012294891 ** get_address_of_success_1() { return &___success_1; }
	inline void set_success_1(UnityAction_1_t3012294891 * value)
	{
		___success_1 = value;
		Il2CppCodeGenWriteBarrier((&___success_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__Iterator2_t1746054447, ___U24this_2)); }
	inline ElementPersistanceImpl_t1641319881 * get_U24this_2() const { return ___U24this_2; }
	inline ElementPersistanceImpl_t1641319881 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ElementPersistanceImpl_t1641319881 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__Iterator2_t1746054447, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__Iterator2_t1746054447, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__Iterator2_t1746054447, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVEU3EC__ITERATOR2_T1746054447_H
#ifndef U3CCLEARU3EC__ITERATOR3_T4110304517_H
#define U3CCLEARU3EC__ITERATOR3_T4110304517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Elements.ElementPersistanceImpl/<clear>c__Iterator3
struct  U3CclearU3Ec__Iterator3_t4110304517  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityAction Library.Code.Facades.Elements.ElementPersistanceImpl/<clear>c__Iterator3::success
	UnityAction_t4025899511 * ___success_0;
	// Library.Code.Facades.Elements.ElementPersistanceImpl Library.Code.Facades.Elements.ElementPersistanceImpl/<clear>c__Iterator3::$this
	ElementPersistanceImpl_t1641319881 * ___U24this_1;
	// System.Object Library.Code.Facades.Elements.ElementPersistanceImpl/<clear>c__Iterator3::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Library.Code.Facades.Elements.ElementPersistanceImpl/<clear>c__Iterator3::$disposing
	bool ___U24disposing_3;
	// System.Int32 Library.Code.Facades.Elements.ElementPersistanceImpl/<clear>c__Iterator3::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_success_0() { return static_cast<int32_t>(offsetof(U3CclearU3Ec__Iterator3_t4110304517, ___success_0)); }
	inline UnityAction_t4025899511 * get_success_0() const { return ___success_0; }
	inline UnityAction_t4025899511 ** get_address_of_success_0() { return &___success_0; }
	inline void set_success_0(UnityAction_t4025899511 * value)
	{
		___success_0 = value;
		Il2CppCodeGenWriteBarrier((&___success_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CclearU3Ec__Iterator3_t4110304517, ___U24this_1)); }
	inline ElementPersistanceImpl_t1641319881 * get_U24this_1() const { return ___U24this_1; }
	inline ElementPersistanceImpl_t1641319881 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ElementPersistanceImpl_t1641319881 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CclearU3Ec__Iterator3_t4110304517, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CclearU3Ec__Iterator3_t4110304517, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CclearU3Ec__Iterator3_t4110304517, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLEARU3EC__ITERATOR3_T4110304517_H
#ifndef U3CGETALLU3EC__ITERATOR0_T1970345158_H
#define U3CGETALLU3EC__ITERATOR0_T1970345158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Messages.MessagePersistance/<getAll>c__Iterator0
struct  U3CgetAllU3Ec__Iterator0_t1970345158  : public RuntimeObject
{
public:
	// Library.Code.Domain.Dtos.MessageListWrapper Library.Code.Facades.Messages.MessagePersistance/<getAll>c__Iterator0::<list>__0
	MessageListWrapper_t1844780540 * ___U3ClistU3E__0_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Message>> Library.Code.Facades.Messages.MessagePersistance/<getAll>c__Iterator0::success
	UnityAction_1_t1950560840 * ___success_1;
	// Library.Code.Facades.Messages.MessagePersistance Library.Code.Facades.Messages.MessagePersistance/<getAll>c__Iterator0::$this
	MessagePersistance_t1067416742 * ___U24this_2;
	// System.Object Library.Code.Facades.Messages.MessagePersistance/<getAll>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Library.Code.Facades.Messages.MessagePersistance/<getAll>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Library.Code.Facades.Messages.MessagePersistance/<getAll>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3ClistU3E__0_0() { return static_cast<int32_t>(offsetof(U3CgetAllU3Ec__Iterator0_t1970345158, ___U3ClistU3E__0_0)); }
	inline MessageListWrapper_t1844780540 * get_U3ClistU3E__0_0() const { return ___U3ClistU3E__0_0; }
	inline MessageListWrapper_t1844780540 ** get_address_of_U3ClistU3E__0_0() { return &___U3ClistU3E__0_0; }
	inline void set_U3ClistU3E__0_0(MessageListWrapper_t1844780540 * value)
	{
		___U3ClistU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClistU3E__0_0), value);
	}

	inline static int32_t get_offset_of_success_1() { return static_cast<int32_t>(offsetof(U3CgetAllU3Ec__Iterator0_t1970345158, ___success_1)); }
	inline UnityAction_1_t1950560840 * get_success_1() const { return ___success_1; }
	inline UnityAction_1_t1950560840 ** get_address_of_success_1() { return &___success_1; }
	inline void set_success_1(UnityAction_1_t1950560840 * value)
	{
		___success_1 = value;
		Il2CppCodeGenWriteBarrier((&___success_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CgetAllU3Ec__Iterator0_t1970345158, ___U24this_2)); }
	inline MessagePersistance_t1067416742 * get_U24this_2() const { return ___U24this_2; }
	inline MessagePersistance_t1067416742 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(MessagePersistance_t1067416742 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CgetAllU3Ec__Iterator0_t1970345158, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CgetAllU3Ec__Iterator0_t1970345158, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CgetAllU3Ec__Iterator0_t1970345158, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLU3EC__ITERATOR0_T1970345158_H
#ifndef U3CUPDATEOBSERVERSU3EC__ANONSTOREY4_T2571633295_H
#define U3CUPDATEOBSERVERSU3EC__ANONSTOREY4_T2571633295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Messages.MessagesFacadeImpl/<updateObservers>c__AnonStorey4
struct  U3CupdateObserversU3Ec__AnonStorey4_t2571633295  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Message> Library.Code.Facades.Messages.MessagesFacadeImpl/<updateObservers>c__AnonStorey4::messages
	List_1_t583975089 * ___messages_0;

public:
	inline static int32_t get_offset_of_messages_0() { return static_cast<int32_t>(offsetof(U3CupdateObserversU3Ec__AnonStorey4_t2571633295, ___messages_0)); }
	inline List_1_t583975089 * get_messages_0() const { return ___messages_0; }
	inline List_1_t583975089 ** get_address_of_messages_0() { return &___messages_0; }
	inline void set_messages_0(List_1_t583975089 * value)
	{
		___messages_0 = value;
		Il2CppCodeGenWriteBarrier((&___messages_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEOBSERVERSU3EC__ANONSTOREY4_T2571633295_H
#ifndef SENDMESSAGEFACADEIMP_T702032987_H
#define SENDMESSAGEFACADEIMP_T702032987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Messages.SendMessageFacadeImp
struct  SendMessageFacadeImp_t702032987  : public RuntimeObject
{
public:
	// Library.Code.Networking.Message.GroupCreatorApi Library.Code.Facades.Messages.SendMessageFacadeImp::_groupCreator
	RuntimeObject* ____groupCreator_0;
	// Library.Code.Networking.Message.SendBatchMessageApi Library.Code.Facades.Messages.SendMessageFacadeImp::_batchMessageSender
	RuntimeObject* ____batchMessageSender_1;
	// Library.Code.Facades.Messages.MessagesFacade Library.Code.Facades.Messages.SendMessageFacadeImp::_messagesFacade
	RuntimeObject* ____messagesFacade_2;
	// Library.Code.Utils.Async.DoAsync Library.Code.Facades.Messages.SendMessageFacadeImp::_async
	RuntimeObject* ____async_3;
	// Library.Code.Facades.Files.SendFileFacade Library.Code.Facades.Messages.SendMessageFacadeImp::_fileFacade
	RuntimeObject* ____fileFacade_4;
	// System.Int64 Library.Code.Facades.Messages.SendMessageFacadeImp::groupId
	int64_t ___groupId_5;
	// Library.Code.Facades.Callback Library.Code.Facades.Messages.SendMessageFacadeImp::_callback
	RuntimeObject* ____callback_6;

public:
	inline static int32_t get_offset_of__groupCreator_0() { return static_cast<int32_t>(offsetof(SendMessageFacadeImp_t702032987, ____groupCreator_0)); }
	inline RuntimeObject* get__groupCreator_0() const { return ____groupCreator_0; }
	inline RuntimeObject** get_address_of__groupCreator_0() { return &____groupCreator_0; }
	inline void set__groupCreator_0(RuntimeObject* value)
	{
		____groupCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&____groupCreator_0), value);
	}

	inline static int32_t get_offset_of__batchMessageSender_1() { return static_cast<int32_t>(offsetof(SendMessageFacadeImp_t702032987, ____batchMessageSender_1)); }
	inline RuntimeObject* get__batchMessageSender_1() const { return ____batchMessageSender_1; }
	inline RuntimeObject** get_address_of__batchMessageSender_1() { return &____batchMessageSender_1; }
	inline void set__batchMessageSender_1(RuntimeObject* value)
	{
		____batchMessageSender_1 = value;
		Il2CppCodeGenWriteBarrier((&____batchMessageSender_1), value);
	}

	inline static int32_t get_offset_of__messagesFacade_2() { return static_cast<int32_t>(offsetof(SendMessageFacadeImp_t702032987, ____messagesFacade_2)); }
	inline RuntimeObject* get__messagesFacade_2() const { return ____messagesFacade_2; }
	inline RuntimeObject** get_address_of__messagesFacade_2() { return &____messagesFacade_2; }
	inline void set__messagesFacade_2(RuntimeObject* value)
	{
		____messagesFacade_2 = value;
		Il2CppCodeGenWriteBarrier((&____messagesFacade_2), value);
	}

	inline static int32_t get_offset_of__async_3() { return static_cast<int32_t>(offsetof(SendMessageFacadeImp_t702032987, ____async_3)); }
	inline RuntimeObject* get__async_3() const { return ____async_3; }
	inline RuntimeObject** get_address_of__async_3() { return &____async_3; }
	inline void set__async_3(RuntimeObject* value)
	{
		____async_3 = value;
		Il2CppCodeGenWriteBarrier((&____async_3), value);
	}

	inline static int32_t get_offset_of__fileFacade_4() { return static_cast<int32_t>(offsetof(SendMessageFacadeImp_t702032987, ____fileFacade_4)); }
	inline RuntimeObject* get__fileFacade_4() const { return ____fileFacade_4; }
	inline RuntimeObject** get_address_of__fileFacade_4() { return &____fileFacade_4; }
	inline void set__fileFacade_4(RuntimeObject* value)
	{
		____fileFacade_4 = value;
		Il2CppCodeGenWriteBarrier((&____fileFacade_4), value);
	}

	inline static int32_t get_offset_of_groupId_5() { return static_cast<int32_t>(offsetof(SendMessageFacadeImp_t702032987, ___groupId_5)); }
	inline int64_t get_groupId_5() const { return ___groupId_5; }
	inline int64_t* get_address_of_groupId_5() { return &___groupId_5; }
	inline void set_groupId_5(int64_t value)
	{
		___groupId_5 = value;
	}

	inline static int32_t get_offset_of__callback_6() { return static_cast<int32_t>(offsetof(SendMessageFacadeImp_t702032987, ____callback_6)); }
	inline RuntimeObject* get__callback_6() const { return ____callback_6; }
	inline RuntimeObject** get_address_of__callback_6() { return &____callback_6; }
	inline void set__callback_6(RuntimeObject* value)
	{
		____callback_6 = value;
		Il2CppCodeGenWriteBarrier((&____callback_6), value);
	}
};

struct SendMessageFacadeImp_t702032987_StaticFields
{
public:
	// UnityEngine.Events.UnityAction Library.Code.Facades.Messages.SendMessageFacadeImp::<>f__am$cache0
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(SendMessageFacadeImp_t702032987_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDMESSAGEFACADEIMP_T702032987_H
#ifndef FAKEMESSAGETEMPLATESAPI_T2375628161_H
#define FAKEMESSAGETEMPLATESAPI_T2375628161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.MessageTemplates.FakeMessageTemplatesApi
struct  FakeMessageTemplatesApi_t2375628161  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEMESSAGETEMPLATESAPI_T2375628161_H
#ifndef U3CLOADMESSAGESTEMPLATESU3EC__ITERATOR0_T1966591915_H
#define U3CLOADMESSAGESTEMPLATESU3EC__ITERATOR0_T1966591915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.MessageTemplates.FakeMessageTemplatesApi/<loadMessagesTemplates>c__Iterator0
struct  U3CloadMessagesTemplatesU3Ec__Iterator0_t1966591915  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.MessageTemplate> Library.Code.Facades.MessageTemplates.FakeMessageTemplatesApi/<loadMessagesTemplates>c__Iterator0::<list>__0
	List_1_t2162674683 * ___U3ClistU3E__0_0;
	// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.MessageTemplate>> Library.Code.Facades.MessageTemplates.FakeMessageTemplatesApi/<loadMessagesTemplates>c__Iterator0::reposneWrapper
	RuntimeObject* ___reposneWrapper_1;
	// System.Object Library.Code.Facades.MessageTemplates.FakeMessageTemplatesApi/<loadMessagesTemplates>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Library.Code.Facades.MessageTemplates.FakeMessageTemplatesApi/<loadMessagesTemplates>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Library.Code.Facades.MessageTemplates.FakeMessageTemplatesApi/<loadMessagesTemplates>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3ClistU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadMessagesTemplatesU3Ec__Iterator0_t1966591915, ___U3ClistU3E__0_0)); }
	inline List_1_t2162674683 * get_U3ClistU3E__0_0() const { return ___U3ClistU3E__0_0; }
	inline List_1_t2162674683 ** get_address_of_U3ClistU3E__0_0() { return &___U3ClistU3E__0_0; }
	inline void set_U3ClistU3E__0_0(List_1_t2162674683 * value)
	{
		___U3ClistU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClistU3E__0_0), value);
	}

	inline static int32_t get_offset_of_reposneWrapper_1() { return static_cast<int32_t>(offsetof(U3CloadMessagesTemplatesU3Ec__Iterator0_t1966591915, ___reposneWrapper_1)); }
	inline RuntimeObject* get_reposneWrapper_1() const { return ___reposneWrapper_1; }
	inline RuntimeObject** get_address_of_reposneWrapper_1() { return &___reposneWrapper_1; }
	inline void set_reposneWrapper_1(RuntimeObject* value)
	{
		___reposneWrapper_1 = value;
		Il2CppCodeGenWriteBarrier((&___reposneWrapper_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CloadMessagesTemplatesU3Ec__Iterator0_t1966591915, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CloadMessagesTemplatesU3Ec__Iterator0_t1966591915, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CloadMessagesTemplatesU3Ec__Iterator0_t1966591915, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADMESSAGESTEMPLATESU3EC__ITERATOR0_T1966591915_H
#ifndef MESSAGETEMPLATESFACADEIMP_T2809804482_H
#define MESSAGETEMPLATESFACADEIMP_T2809804482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.MessageTemplates.MessageTemplatesFacadeImp
struct  MessageTemplatesFacadeImp_t2809804482  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Facades.Observer`1<Library.Code.Domain.Entities.MessageTemplate>> Library.Code.Facades.MessageTemplates.MessageTemplatesFacadeImp::_observers
	List_1_t400946331 * ____observers_0;
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.MessageTemplate> Library.Code.Facades.MessageTemplates.MessageTemplatesFacadeImp::_messageTemplates
	List_1_t2162674683 * ____messageTemplates_1;
	// Library.Code.Networking.MessageTemplate.MessageTemplatesApi Library.Code.Facades.MessageTemplates.MessageTemplatesFacadeImp::messagesApi
	RuntimeObject* ___messagesApi_2;
	// Library.Code.Utils.Async.DoAsync Library.Code.Facades.MessageTemplates.MessageTemplatesFacadeImp::_doAsync
	RuntimeObject* ____doAsync_3;
	// Library.Code.Facades.Callback Library.Code.Facades.MessageTemplates.MessageTemplatesFacadeImp::_callback
	RuntimeObject* ____callback_4;

public:
	inline static int32_t get_offset_of__observers_0() { return static_cast<int32_t>(offsetof(MessageTemplatesFacadeImp_t2809804482, ____observers_0)); }
	inline List_1_t400946331 * get__observers_0() const { return ____observers_0; }
	inline List_1_t400946331 ** get_address_of__observers_0() { return &____observers_0; }
	inline void set__observers_0(List_1_t400946331 * value)
	{
		____observers_0 = value;
		Il2CppCodeGenWriteBarrier((&____observers_0), value);
	}

	inline static int32_t get_offset_of__messageTemplates_1() { return static_cast<int32_t>(offsetof(MessageTemplatesFacadeImp_t2809804482, ____messageTemplates_1)); }
	inline List_1_t2162674683 * get__messageTemplates_1() const { return ____messageTemplates_1; }
	inline List_1_t2162674683 ** get_address_of__messageTemplates_1() { return &____messageTemplates_1; }
	inline void set__messageTemplates_1(List_1_t2162674683 * value)
	{
		____messageTemplates_1 = value;
		Il2CppCodeGenWriteBarrier((&____messageTemplates_1), value);
	}

	inline static int32_t get_offset_of_messagesApi_2() { return static_cast<int32_t>(offsetof(MessageTemplatesFacadeImp_t2809804482, ___messagesApi_2)); }
	inline RuntimeObject* get_messagesApi_2() const { return ___messagesApi_2; }
	inline RuntimeObject** get_address_of_messagesApi_2() { return &___messagesApi_2; }
	inline void set_messagesApi_2(RuntimeObject* value)
	{
		___messagesApi_2 = value;
		Il2CppCodeGenWriteBarrier((&___messagesApi_2), value);
	}

	inline static int32_t get_offset_of__doAsync_3() { return static_cast<int32_t>(offsetof(MessageTemplatesFacadeImp_t2809804482, ____doAsync_3)); }
	inline RuntimeObject* get__doAsync_3() const { return ____doAsync_3; }
	inline RuntimeObject** get_address_of__doAsync_3() { return &____doAsync_3; }
	inline void set__doAsync_3(RuntimeObject* value)
	{
		____doAsync_3 = value;
		Il2CppCodeGenWriteBarrier((&____doAsync_3), value);
	}

	inline static int32_t get_offset_of__callback_4() { return static_cast<int32_t>(offsetof(MessageTemplatesFacadeImp_t2809804482, ____callback_4)); }
	inline RuntimeObject* get__callback_4() const { return ____callback_4; }
	inline RuntimeObject** get_address_of__callback_4() { return &____callback_4; }
	inline void set__callback_4(RuntimeObject* value)
	{
		____callback_4 = value;
		Il2CppCodeGenWriteBarrier((&____callback_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGETEMPLATESFACADEIMP_T2809804482_H
#ifndef FAKEMODELAPI_T2762945876_H
#define FAKEMODELAPI_T2762945876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Models.FakeModelApi
struct  FakeModelApi_t2762945876  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEMODELAPI_T2762945876_H
#ifndef U3CLIBRARY_CODE_NETWORKING_MODELS_MODELAPI_LOADMODELSU3EC__ITERATOR0_T1215701439_H
#define U3CLIBRARY_CODE_NETWORKING_MODELS_MODELAPI_LOADMODELSU3EC__ITERATOR0_T1215701439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Models.FakeModelApi/<Library_Code_Networking_Models_ModelApi_loadModels>c__Iterator0
struct  U3CLibrary_Code_Networking_Models_ModelApi_loadModelsU3Ec__Iterator0_t1215701439  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model> Library.Code.Facades.Models.FakeModelApi/<Library_Code_Networking_Models_ModelApi_loadModels>c__Iterator0::<fakeModels>__0
	List_1_t1532750683 * ___U3CfakeModelsU3E__0_0;
	// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model>> Library.Code.Facades.Models.FakeModelApi/<Library_Code_Networking_Models_ModelApi_loadModels>c__Iterator0::reposneWrapper
	RuntimeObject* ___reposneWrapper_1;
	// System.Object Library.Code.Facades.Models.FakeModelApi/<Library_Code_Networking_Models_ModelApi_loadModels>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Library.Code.Facades.Models.FakeModelApi/<Library_Code_Networking_Models_ModelApi_loadModels>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 Library.Code.Facades.Models.FakeModelApi/<Library_Code_Networking_Models_ModelApi_loadModels>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CfakeModelsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLibrary_Code_Networking_Models_ModelApi_loadModelsU3Ec__Iterator0_t1215701439, ___U3CfakeModelsU3E__0_0)); }
	inline List_1_t1532750683 * get_U3CfakeModelsU3E__0_0() const { return ___U3CfakeModelsU3E__0_0; }
	inline List_1_t1532750683 ** get_address_of_U3CfakeModelsU3E__0_0() { return &___U3CfakeModelsU3E__0_0; }
	inline void set_U3CfakeModelsU3E__0_0(List_1_t1532750683 * value)
	{
		___U3CfakeModelsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfakeModelsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_reposneWrapper_1() { return static_cast<int32_t>(offsetof(U3CLibrary_Code_Networking_Models_ModelApi_loadModelsU3Ec__Iterator0_t1215701439, ___reposneWrapper_1)); }
	inline RuntimeObject* get_reposneWrapper_1() const { return ___reposneWrapper_1; }
	inline RuntimeObject** get_address_of_reposneWrapper_1() { return &___reposneWrapper_1; }
	inline void set_reposneWrapper_1(RuntimeObject* value)
	{
		___reposneWrapper_1 = value;
		Il2CppCodeGenWriteBarrier((&___reposneWrapper_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CLibrary_Code_Networking_Models_ModelApi_loadModelsU3Ec__Iterator0_t1215701439, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CLibrary_Code_Networking_Models_ModelApi_loadModelsU3Ec__Iterator0_t1215701439, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CLibrary_Code_Networking_Models_ModelApi_loadModelsU3Ec__Iterator0_t1215701439, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLIBRARY_CODE_NETWORKING_MODELS_MODELAPI_LOADMODELSU3EC__ITERATOR0_T1215701439_H
#ifndef MODELFACADEIMPL_T3221237801_H
#define MODELFACADEIMPL_T3221237801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Models.ModelFacadeImpl
struct  ModelFacadeImpl_t3221237801  : public RuntimeObject
{
public:
	// Library.Code.Facades.PersistanceFacade`1<Library.Code.Domain.Entities.Model> Library.Code.Facades.Models.ModelFacadeImpl::_modelPersistance
	RuntimeObject* ____modelPersistance_0;
	// Library.Code.Utils.Async.DoAsync Library.Code.Facades.Models.ModelFacadeImpl::doAsync
	RuntimeObject* ___doAsync_1;
	// System.Collections.Generic.List`1<Library.Code.Facades.Observer`1<Library.Code.Domain.Entities.Model>> Library.Code.Facades.Models.ModelFacadeImpl::_observers
	List_1_t4065989627 * ____observers_2;
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model> Library.Code.Facades.Models.ModelFacadeImpl::_models
	List_1_t1532750683 * ____models_3;
	// Library.Code.Networking.Models.ModelApi Library.Code.Facades.Models.ModelFacadeImpl::modelApi
	RuntimeObject* ___modelApi_4;

public:
	inline static int32_t get_offset_of__modelPersistance_0() { return static_cast<int32_t>(offsetof(ModelFacadeImpl_t3221237801, ____modelPersistance_0)); }
	inline RuntimeObject* get__modelPersistance_0() const { return ____modelPersistance_0; }
	inline RuntimeObject** get_address_of__modelPersistance_0() { return &____modelPersistance_0; }
	inline void set__modelPersistance_0(RuntimeObject* value)
	{
		____modelPersistance_0 = value;
		Il2CppCodeGenWriteBarrier((&____modelPersistance_0), value);
	}

	inline static int32_t get_offset_of_doAsync_1() { return static_cast<int32_t>(offsetof(ModelFacadeImpl_t3221237801, ___doAsync_1)); }
	inline RuntimeObject* get_doAsync_1() const { return ___doAsync_1; }
	inline RuntimeObject** get_address_of_doAsync_1() { return &___doAsync_1; }
	inline void set_doAsync_1(RuntimeObject* value)
	{
		___doAsync_1 = value;
		Il2CppCodeGenWriteBarrier((&___doAsync_1), value);
	}

	inline static int32_t get_offset_of__observers_2() { return static_cast<int32_t>(offsetof(ModelFacadeImpl_t3221237801, ____observers_2)); }
	inline List_1_t4065989627 * get__observers_2() const { return ____observers_2; }
	inline List_1_t4065989627 ** get_address_of__observers_2() { return &____observers_2; }
	inline void set__observers_2(List_1_t4065989627 * value)
	{
		____observers_2 = value;
		Il2CppCodeGenWriteBarrier((&____observers_2), value);
	}

	inline static int32_t get_offset_of__models_3() { return static_cast<int32_t>(offsetof(ModelFacadeImpl_t3221237801, ____models_3)); }
	inline List_1_t1532750683 * get__models_3() const { return ____models_3; }
	inline List_1_t1532750683 ** get_address_of__models_3() { return &____models_3; }
	inline void set__models_3(List_1_t1532750683 * value)
	{
		____models_3 = value;
		Il2CppCodeGenWriteBarrier((&____models_3), value);
	}

	inline static int32_t get_offset_of_modelApi_4() { return static_cast<int32_t>(offsetof(ModelFacadeImpl_t3221237801, ___modelApi_4)); }
	inline RuntimeObject* get_modelApi_4() const { return ___modelApi_4; }
	inline RuntimeObject** get_address_of_modelApi_4() { return &___modelApi_4; }
	inline void set_modelApi_4(RuntimeObject* value)
	{
		___modelApi_4 = value;
		Il2CppCodeGenWriteBarrier((&___modelApi_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELFACADEIMPL_T3221237801_H
#ifndef U3CCLEARU3EC__ANONSTOREY3_T1249885919_H
#define U3CCLEARU3EC__ANONSTOREY3_T1249885919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Messages.MessagesFacadeImpl/<clear>c__AnonStorey3
struct  U3CclearU3Ec__AnonStorey3_t1249885919  : public RuntimeObject
{
public:
	// Library.Code.Facades.Callback Library.Code.Facades.Messages.MessagesFacadeImpl/<clear>c__AnonStorey3::callback
	RuntimeObject* ___callback_0;
	// Library.Code.Facades.Messages.MessagesFacadeImpl Library.Code.Facades.Messages.MessagesFacadeImpl/<clear>c__AnonStorey3::$this
	MessagesFacadeImpl_t154288538 * ___U24this_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CclearU3Ec__AnonStorey3_t1249885919, ___callback_0)); }
	inline RuntimeObject* get_callback_0() const { return ___callback_0; }
	inline RuntimeObject** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(RuntimeObject* value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CclearU3Ec__AnonStorey3_t1249885919, ___U24this_1)); }
	inline MessagesFacadeImpl_t154288538 * get_U24this_1() const { return ___U24this_1; }
	inline MessagesFacadeImpl_t154288538 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(MessagesFacadeImpl_t154288538 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLEARU3EC__ANONSTOREY3_T1249885919_H
#ifndef U3CADDU3EC__ITERATOR1_T3880396359_H
#define U3CADDU3EC__ITERATOR1_T3880396359_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Messages.MessagePersistance/<add>c__Iterator1
struct  U3CaddU3Ec__Iterator1_t3880396359  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Message Library.Code.Facades.Messages.MessagePersistance/<add>c__Iterator1::message
	Message_t1214853957 * ___message_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Message>> Library.Code.Facades.Messages.MessagePersistance/<add>c__Iterator1::success
	UnityAction_1_t1950560840 * ___success_1;
	// Library.Code.Facades.Messages.MessagePersistance Library.Code.Facades.Messages.MessagePersistance/<add>c__Iterator1::$this
	MessagePersistance_t1067416742 * ___U24this_2;
	// System.Object Library.Code.Facades.Messages.MessagePersistance/<add>c__Iterator1::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Library.Code.Facades.Messages.MessagePersistance/<add>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 Library.Code.Facades.Messages.MessagePersistance/<add>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_message_0() { return static_cast<int32_t>(offsetof(U3CaddU3Ec__Iterator1_t3880396359, ___message_0)); }
	inline Message_t1214853957 * get_message_0() const { return ___message_0; }
	inline Message_t1214853957 ** get_address_of_message_0() { return &___message_0; }
	inline void set_message_0(Message_t1214853957 * value)
	{
		___message_0 = value;
		Il2CppCodeGenWriteBarrier((&___message_0), value);
	}

	inline static int32_t get_offset_of_success_1() { return static_cast<int32_t>(offsetof(U3CaddU3Ec__Iterator1_t3880396359, ___success_1)); }
	inline UnityAction_1_t1950560840 * get_success_1() const { return ___success_1; }
	inline UnityAction_1_t1950560840 ** get_address_of_success_1() { return &___success_1; }
	inline void set_success_1(UnityAction_1_t1950560840 * value)
	{
		___success_1 = value;
		Il2CppCodeGenWriteBarrier((&___success_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CaddU3Ec__Iterator1_t3880396359, ___U24this_2)); }
	inline MessagePersistance_t1067416742 * get_U24this_2() const { return ___U24this_2; }
	inline MessagePersistance_t1067416742 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(MessagePersistance_t1067416742 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CaddU3Ec__Iterator1_t3880396359, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CaddU3Ec__Iterator1_t3880396359, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CaddU3Ec__Iterator1_t3880396359, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDU3EC__ITERATOR1_T3880396359_H
#ifndef U3CREMOVEU3EC__ITERATOR2_T3988214839_H
#define U3CREMOVEU3EC__ITERATOR2_T3988214839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Messages.MessagePersistance/<remove>c__Iterator2
struct  U3CremoveU3Ec__Iterator2_t3988214839  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Message Library.Code.Facades.Messages.MessagePersistance/<remove>c__Iterator2::message
	Message_t1214853957 * ___message_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Message>> Library.Code.Facades.Messages.MessagePersistance/<remove>c__Iterator2::success
	UnityAction_1_t1950560840 * ___success_1;
	// Library.Code.Facades.Messages.MessagePersistance Library.Code.Facades.Messages.MessagePersistance/<remove>c__Iterator2::$this
	MessagePersistance_t1067416742 * ___U24this_2;
	// System.Object Library.Code.Facades.Messages.MessagePersistance/<remove>c__Iterator2::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Library.Code.Facades.Messages.MessagePersistance/<remove>c__Iterator2::$disposing
	bool ___U24disposing_4;
	// System.Int32 Library.Code.Facades.Messages.MessagePersistance/<remove>c__Iterator2::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_message_0() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__Iterator2_t3988214839, ___message_0)); }
	inline Message_t1214853957 * get_message_0() const { return ___message_0; }
	inline Message_t1214853957 ** get_address_of_message_0() { return &___message_0; }
	inline void set_message_0(Message_t1214853957 * value)
	{
		___message_0 = value;
		Il2CppCodeGenWriteBarrier((&___message_0), value);
	}

	inline static int32_t get_offset_of_success_1() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__Iterator2_t3988214839, ___success_1)); }
	inline UnityAction_1_t1950560840 * get_success_1() const { return ___success_1; }
	inline UnityAction_1_t1950560840 ** get_address_of_success_1() { return &___success_1; }
	inline void set_success_1(UnityAction_1_t1950560840 * value)
	{
		___success_1 = value;
		Il2CppCodeGenWriteBarrier((&___success_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__Iterator2_t3988214839, ___U24this_2)); }
	inline MessagePersistance_t1067416742 * get_U24this_2() const { return ___U24this_2; }
	inline MessagePersistance_t1067416742 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(MessagePersistance_t1067416742 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__Iterator2_t3988214839, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__Iterator2_t3988214839, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__Iterator2_t3988214839, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVEU3EC__ITERATOR2_T3988214839_H
#ifndef U3CCLEARU3EC__ITERATOR3_T2460459301_H
#define U3CCLEARU3EC__ITERATOR3_T2460459301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Messages.MessagePersistance/<clear>c__Iterator3
struct  U3CclearU3Ec__Iterator3_t2460459301  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityAction Library.Code.Facades.Messages.MessagePersistance/<clear>c__Iterator3::success
	UnityAction_t4025899511 * ___success_0;
	// Library.Code.Facades.Messages.MessagePersistance Library.Code.Facades.Messages.MessagePersistance/<clear>c__Iterator3::$this
	MessagePersistance_t1067416742 * ___U24this_1;
	// System.Object Library.Code.Facades.Messages.MessagePersistance/<clear>c__Iterator3::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Library.Code.Facades.Messages.MessagePersistance/<clear>c__Iterator3::$disposing
	bool ___U24disposing_3;
	// System.Int32 Library.Code.Facades.Messages.MessagePersistance/<clear>c__Iterator3::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_success_0() { return static_cast<int32_t>(offsetof(U3CclearU3Ec__Iterator3_t2460459301, ___success_0)); }
	inline UnityAction_t4025899511 * get_success_0() const { return ___success_0; }
	inline UnityAction_t4025899511 ** get_address_of_success_0() { return &___success_0; }
	inline void set_success_0(UnityAction_t4025899511 * value)
	{
		___success_0 = value;
		Il2CppCodeGenWriteBarrier((&___success_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CclearU3Ec__Iterator3_t2460459301, ___U24this_1)); }
	inline MessagePersistance_t1067416742 * get_U24this_1() const { return ___U24this_1; }
	inline MessagePersistance_t1067416742 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(MessagePersistance_t1067416742 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CclearU3Ec__Iterator3_t2460459301, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CclearU3Ec__Iterator3_t2460459301, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CclearU3Ec__Iterator3_t2460459301, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLEARU3EC__ITERATOR3_T2460459301_H
#ifndef MESSAGESFACADEIMPL_T154288538_H
#define MESSAGESFACADEIMPL_T154288538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Messages.MessagesFacadeImpl
struct  MessagesFacadeImpl_t154288538  : public RuntimeObject
{
public:
	// Library.Code.Facades.Messages.MessagePersistance Library.Code.Facades.Messages.MessagesFacadeImpl::_messagePersistance
	MessagePersistance_t1067416742 * ____messagePersistance_0;
	// Library.Code.Utils.Async.DoAsync Library.Code.Facades.Messages.MessagesFacadeImpl::_async
	RuntimeObject* ____async_1;
	// System.Collections.Generic.List`1<Library.Code.Facades.Observer`1<Library.Code.Domain.Entities.Message>> Library.Code.Facades.Messages.MessagesFacadeImpl::_observers
	List_1_t3117214033 * ____observers_2;
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Message> Library.Code.Facades.Messages.MessagesFacadeImpl::_messages
	List_1_t583975089 * ____messages_3;

public:
	inline static int32_t get_offset_of__messagePersistance_0() { return static_cast<int32_t>(offsetof(MessagesFacadeImpl_t154288538, ____messagePersistance_0)); }
	inline MessagePersistance_t1067416742 * get__messagePersistance_0() const { return ____messagePersistance_0; }
	inline MessagePersistance_t1067416742 ** get_address_of__messagePersistance_0() { return &____messagePersistance_0; }
	inline void set__messagePersistance_0(MessagePersistance_t1067416742 * value)
	{
		____messagePersistance_0 = value;
		Il2CppCodeGenWriteBarrier((&____messagePersistance_0), value);
	}

	inline static int32_t get_offset_of__async_1() { return static_cast<int32_t>(offsetof(MessagesFacadeImpl_t154288538, ____async_1)); }
	inline RuntimeObject* get__async_1() const { return ____async_1; }
	inline RuntimeObject** get_address_of__async_1() { return &____async_1; }
	inline void set__async_1(RuntimeObject* value)
	{
		____async_1 = value;
		Il2CppCodeGenWriteBarrier((&____async_1), value);
	}

	inline static int32_t get_offset_of__observers_2() { return static_cast<int32_t>(offsetof(MessagesFacadeImpl_t154288538, ____observers_2)); }
	inline List_1_t3117214033 * get__observers_2() const { return ____observers_2; }
	inline List_1_t3117214033 ** get_address_of__observers_2() { return &____observers_2; }
	inline void set__observers_2(List_1_t3117214033 * value)
	{
		____observers_2 = value;
		Il2CppCodeGenWriteBarrier((&____observers_2), value);
	}

	inline static int32_t get_offset_of__messages_3() { return static_cast<int32_t>(offsetof(MessagesFacadeImpl_t154288538, ____messages_3)); }
	inline List_1_t583975089 * get__messages_3() const { return ____messages_3; }
	inline List_1_t583975089 ** get_address_of__messages_3() { return &____messages_3; }
	inline void set__messages_3(List_1_t583975089 * value)
	{
		____messages_3 = value;
		Il2CppCodeGenWriteBarrier((&____messages_3), value);
	}
};

struct MessagesFacadeImpl_t154288538_StaticFields
{
public:
	// UnityEngine.Events.UnityAction Library.Code.Facades.Messages.MessagesFacadeImpl::<>f__am$cache0
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache0_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(MessagesFacadeImpl_t154288538_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGESFACADEIMPL_T154288538_H
#ifndef U3CSAVEU3EC__ANONSTOREY0_T568912870_H
#define U3CSAVEU3EC__ANONSTOREY0_T568912870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Messages.MessagesFacadeImpl/<save>c__AnonStorey0
struct  U3CsaveU3Ec__AnonStorey0_t568912870  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Message Library.Code.Facades.Messages.MessagesFacadeImpl/<save>c__AnonStorey0::message
	Message_t1214853957 * ___message_0;
	// Library.Code.Facades.Callback Library.Code.Facades.Messages.MessagesFacadeImpl/<save>c__AnonStorey0::persistCallback
	RuntimeObject* ___persistCallback_1;
	// Library.Code.Facades.Messages.MessagesFacadeImpl Library.Code.Facades.Messages.MessagesFacadeImpl/<save>c__AnonStorey0::$this
	MessagesFacadeImpl_t154288538 * ___U24this_2;

public:
	inline static int32_t get_offset_of_message_0() { return static_cast<int32_t>(offsetof(U3CsaveU3Ec__AnonStorey0_t568912870, ___message_0)); }
	inline Message_t1214853957 * get_message_0() const { return ___message_0; }
	inline Message_t1214853957 ** get_address_of_message_0() { return &___message_0; }
	inline void set_message_0(Message_t1214853957 * value)
	{
		___message_0 = value;
		Il2CppCodeGenWriteBarrier((&___message_0), value);
	}

	inline static int32_t get_offset_of_persistCallback_1() { return static_cast<int32_t>(offsetof(U3CsaveU3Ec__AnonStorey0_t568912870, ___persistCallback_1)); }
	inline RuntimeObject* get_persistCallback_1() const { return ___persistCallback_1; }
	inline RuntimeObject** get_address_of_persistCallback_1() { return &___persistCallback_1; }
	inline void set_persistCallback_1(RuntimeObject* value)
	{
		___persistCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___persistCallback_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CsaveU3Ec__AnonStorey0_t568912870, ___U24this_2)); }
	inline MessagesFacadeImpl_t154288538 * get_U24this_2() const { return ___U24this_2; }
	inline MessagesFacadeImpl_t154288538 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(MessagesFacadeImpl_t154288538 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSAVEU3EC__ANONSTOREY0_T568912870_H
#ifndef U3CREMOVEU3EC__ANONSTOREY1_T3209414258_H
#define U3CREMOVEU3EC__ANONSTOREY1_T3209414258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Messages.MessagesFacadeImpl/<remove>c__AnonStorey1
struct  U3CremoveU3Ec__AnonStorey1_t3209414258  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Message Library.Code.Facades.Messages.MessagesFacadeImpl/<remove>c__AnonStorey1::message
	Message_t1214853957 * ___message_0;
	// Library.Code.Facades.Callback Library.Code.Facades.Messages.MessagesFacadeImpl/<remove>c__AnonStorey1::persistCallback
	RuntimeObject* ___persistCallback_1;
	// Library.Code.Facades.Messages.MessagesFacadeImpl Library.Code.Facades.Messages.MessagesFacadeImpl/<remove>c__AnonStorey1::$this
	MessagesFacadeImpl_t154288538 * ___U24this_2;

public:
	inline static int32_t get_offset_of_message_0() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__AnonStorey1_t3209414258, ___message_0)); }
	inline Message_t1214853957 * get_message_0() const { return ___message_0; }
	inline Message_t1214853957 ** get_address_of_message_0() { return &___message_0; }
	inline void set_message_0(Message_t1214853957 * value)
	{
		___message_0 = value;
		Il2CppCodeGenWriteBarrier((&___message_0), value);
	}

	inline static int32_t get_offset_of_persistCallback_1() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__AnonStorey1_t3209414258, ___persistCallback_1)); }
	inline RuntimeObject* get_persistCallback_1() const { return ___persistCallback_1; }
	inline RuntimeObject** get_address_of_persistCallback_1() { return &___persistCallback_1; }
	inline void set_persistCallback_1(RuntimeObject* value)
	{
		___persistCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___persistCallback_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CremoveU3Ec__AnonStorey1_t3209414258, ___U24this_2)); }
	inline MessagesFacadeImpl_t154288538 * get_U24this_2() const { return ___U24this_2; }
	inline MessagesFacadeImpl_t154288538 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(MessagesFacadeImpl_t154288538 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVEU3EC__ANONSTOREY1_T3209414258_H
#ifndef U3CGETU3EC__ANONSTOREY2_T2085224001_H
#define U3CGETU3EC__ANONSTOREY2_T2085224001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Messages.MessagesFacadeImpl/<get>c__AnonStorey2
struct  U3CgetU3Ec__AnonStorey2_t2085224001  : public RuntimeObject
{
public:
	// Library.Code.Facades.Callback Library.Code.Facades.Messages.MessagesFacadeImpl/<get>c__AnonStorey2::persistCallback
	RuntimeObject* ___persistCallback_0;
	// Library.Code.Facades.Messages.MessagesFacadeImpl Library.Code.Facades.Messages.MessagesFacadeImpl/<get>c__AnonStorey2::$this
	MessagesFacadeImpl_t154288538 * ___U24this_1;

public:
	inline static int32_t get_offset_of_persistCallback_0() { return static_cast<int32_t>(offsetof(U3CgetU3Ec__AnonStorey2_t2085224001, ___persistCallback_0)); }
	inline RuntimeObject* get_persistCallback_0() const { return ___persistCallback_0; }
	inline RuntimeObject** get_address_of_persistCallback_0() { return &___persistCallback_0; }
	inline void set_persistCallback_0(RuntimeObject* value)
	{
		___persistCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___persistCallback_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CgetU3Ec__AnonStorey2_t2085224001, ___U24this_1)); }
	inline MessagesFacadeImpl_t154288538 * get_U24this_1() const { return ___U24this_1; }
	inline MessagesFacadeImpl_t154288538 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(MessagesFacadeImpl_t154288538 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETU3EC__ANONSTOREY2_T2085224001_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef FAILENUMS_T680706100_H
#define FAILENUMS_T680706100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.FailEnums
struct  FailEnums_t680706100 
{
public:
	// System.Int32 Library.Code.Networking.FailEnums::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FailEnums_t680706100, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAILENUMS_T680706100_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef FAILREASON_T476307514_H
#define FAILREASON_T476307514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.FailReason
struct  FailReason_t476307514  : public RuntimeObject
{
public:
	// System.String Library.Code.Networking.FailReason::failReason
	String_t* ___failReason_0;
	// Library.Code.Networking.FailEnums Library.Code.Networking.FailReason::enumReason
	int32_t ___enumReason_1;

public:
	inline static int32_t get_offset_of_failReason_0() { return static_cast<int32_t>(offsetof(FailReason_t476307514, ___failReason_0)); }
	inline String_t* get_failReason_0() const { return ___failReason_0; }
	inline String_t** get_address_of_failReason_0() { return &___failReason_0; }
	inline void set_failReason_0(String_t* value)
	{
		___failReason_0 = value;
		Il2CppCodeGenWriteBarrier((&___failReason_0), value);
	}

	inline static int32_t get_offset_of_enumReason_1() { return static_cast<int32_t>(offsetof(FailReason_t476307514, ___enumReason_1)); }
	inline int32_t get_enumReason_1() const { return ___enumReason_1; }
	inline int32_t* get_address_of_enumReason_1() { return &___enumReason_1; }
	inline void set_enumReason_1(int32_t value)
	{
		___enumReason_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAILREASON_T476307514_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef PATHHOLDERIMP_T857450797_H
#define PATHHOLDERIMP_T857450797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.PathHolder.PathHolderImp
struct  PathHolderImp_t857450797  : public MonoBehaviour_t1158329972
{
public:
	// System.String Library.Code.Facades.PathHolder.PathHolderImp::filePath
	String_t* ___filePath_2;
	// System.Collections.Generic.List`1<System.String> Library.Code.Facades.PathHolder.PathHolderImp::_paths
	List_1_t1398341365 * ____paths_3;
	// System.String Library.Code.Facades.PathHolder.PathHolderImp::recordPath
	String_t* ___recordPath_4;

public:
	inline static int32_t get_offset_of_filePath_2() { return static_cast<int32_t>(offsetof(PathHolderImp_t857450797, ___filePath_2)); }
	inline String_t* get_filePath_2() const { return ___filePath_2; }
	inline String_t** get_address_of_filePath_2() { return &___filePath_2; }
	inline void set_filePath_2(String_t* value)
	{
		___filePath_2 = value;
		Il2CppCodeGenWriteBarrier((&___filePath_2), value);
	}

	inline static int32_t get_offset_of__paths_3() { return static_cast<int32_t>(offsetof(PathHolderImp_t857450797, ____paths_3)); }
	inline List_1_t1398341365 * get__paths_3() const { return ____paths_3; }
	inline List_1_t1398341365 ** get_address_of__paths_3() { return &____paths_3; }
	inline void set__paths_3(List_1_t1398341365 * value)
	{
		____paths_3 = value;
		Il2CppCodeGenWriteBarrier((&____paths_3), value);
	}

	inline static int32_t get_offset_of_recordPath_4() { return static_cast<int32_t>(offsetof(PathHolderImp_t857450797, ___recordPath_4)); }
	inline String_t* get_recordPath_4() const { return ___recordPath_4; }
	inline String_t** get_address_of_recordPath_4() { return &___recordPath_4; }
	inline void set_recordPath_4(String_t* value)
	{
		___recordPath_4 = value;
		Il2CppCodeGenWriteBarrier((&___recordPath_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHHOLDERIMP_T857450797_H
#ifndef SNAPSHOTPERSISTANCEPROVIDER_T3366466112_H
#define SNAPSHOTPERSISTANCEPROVIDER_T3366466112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.PathHolder.SnapshotPersistanceProvider
struct  SnapshotPersistanceProvider_t3366466112  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Facades.PathHolder.PathHolderImp Library.Code.Facades.PathHolder.SnapshotPersistanceProvider::snapshotPersistance
	PathHolderImp_t857450797 * ___snapshotPersistance_2;

public:
	inline static int32_t get_offset_of_snapshotPersistance_2() { return static_cast<int32_t>(offsetof(SnapshotPersistanceProvider_t3366466112, ___snapshotPersistance_2)); }
	inline PathHolderImp_t857450797 * get_snapshotPersistance_2() const { return ___snapshotPersistance_2; }
	inline PathHolderImp_t857450797 ** get_address_of_snapshotPersistance_2() { return &___snapshotPersistance_2; }
	inline void set_snapshotPersistance_2(PathHolderImp_t857450797 * value)
	{
		___snapshotPersistance_2 = value;
		Il2CppCodeGenWriteBarrier((&___snapshotPersistance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SNAPSHOTPERSISTANCEPROVIDER_T3366466112_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (U3CremoveU3Ec__AnonStorey1_t2493958417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2200[3] = 
{
	U3CremoveU3Ec__AnonStorey1_t2493958417::get_offset_of_models_0(),
	U3CremoveU3Ec__AnonStorey1_t2493958417::get_offset_of_persistCallback_1(),
	U3CremoveU3Ec__AnonStorey1_t2493958417::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (U3CgetU3Ec__AnonStorey2_t1130253552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2201[1] = 
{
	U3CgetU3Ec__AnonStorey2_t1130253552::get_offset_of_persistCallback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (U3CloadU3Ec__AnonStorey3_t3334096223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2202[1] = 
{
	U3CloadU3Ec__AnonStorey3_t3334096223::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (U3CupdateObserversU3Ec__AnonStorey4_t3475232138), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2203[1] = 
{
	U3CupdateObserversU3Ec__AnonStorey4_t3475232138::get_offset_of_messages_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (U3CloadForModelU3Ec__AnonStorey5_t2869201201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2204[1] = 
{
	U3CloadForModelU3Ec__AnonStorey5_t2869201201::get_offset_of_model_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (ElementPersistanceImpl_t1641319881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2205[3] = 
{
	ElementPersistanceImpl_t1641319881::get_offset_of__elements_0(),
	ElementPersistanceImpl_t1641319881::get_offset_of_filePresistance_1(),
	ElementPersistanceImpl_t1641319881::get_offset_of__filename_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (U3CgetAllU3Ec__Iterator0_t3007678366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2206[6] = 
{
	U3CgetAllU3Ec__Iterator0_t3007678366::get_offset_of_U3ClistU3E__0_0(),
	U3CgetAllU3Ec__Iterator0_t3007678366::get_offset_of_success_1(),
	U3CgetAllU3Ec__Iterator0_t3007678366::get_offset_of_U24this_2(),
	U3CgetAllU3Ec__Iterator0_t3007678366::get_offset_of_U24current_3(),
	U3CgetAllU3Ec__Iterator0_t3007678366::get_offset_of_U24disposing_4(),
	U3CgetAllU3Ec__Iterator0_t3007678366::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (U3CaddU3Ec__Iterator1_t2643060007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2207[6] = 
{
	U3CaddU3Ec__Iterator1_t2643060007::get_offset_of_message_0(),
	U3CaddU3Ec__Iterator1_t2643060007::get_offset_of_success_1(),
	U3CaddU3Ec__Iterator1_t2643060007::get_offset_of_U24this_2(),
	U3CaddU3Ec__Iterator1_t2643060007::get_offset_of_U24current_3(),
	U3CaddU3Ec__Iterator1_t2643060007::get_offset_of_U24disposing_4(),
	U3CaddU3Ec__Iterator1_t2643060007::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (U3CremoveU3Ec__Iterator2_t1746054447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2208[6] = 
{
	U3CremoveU3Ec__Iterator2_t1746054447::get_offset_of_message_0(),
	U3CremoveU3Ec__Iterator2_t1746054447::get_offset_of_success_1(),
	U3CremoveU3Ec__Iterator2_t1746054447::get_offset_of_U24this_2(),
	U3CremoveU3Ec__Iterator2_t1746054447::get_offset_of_U24current_3(),
	U3CremoveU3Ec__Iterator2_t1746054447::get_offset_of_U24disposing_4(),
	U3CremoveU3Ec__Iterator2_t1746054447::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (U3CclearU3Ec__Iterator3_t4110304517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2209[5] = 
{
	U3CclearU3Ec__Iterator3_t4110304517::get_offset_of_success_0(),
	U3CclearU3Ec__Iterator3_t4110304517::get_offset_of_U24this_1(),
	U3CclearU3Ec__Iterator3_t4110304517::get_offset_of_U24current_2(),
	U3CclearU3Ec__Iterator3_t4110304517::get_offset_of_U24disposing_3(),
	U3CclearU3Ec__Iterator3_t4110304517::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (FakeElementApi_t1095413803), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (U3CloadElementsU3Ec__Iterator0_t2433942208), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2211[5] = 
{
	U3CloadElementsU3Ec__Iterator0_t2433942208::get_offset_of_U3CfakeElementsU3E__0_0(),
	U3CloadElementsU3Ec__Iterator0_t2433942208::get_offset_of_reposneWrapper_1(),
	U3CloadElementsU3Ec__Iterator0_t2433942208::get_offset_of_U24current_2(),
	U3CloadElementsU3Ec__Iterator0_t2433942208::get_offset_of_U24disposing_3(),
	U3CloadElementsU3Ec__Iterator0_t2433942208::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (FakeSendFileApi_t117775171), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (SendFileFacadeImpl_t1613402490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2214[4] = 
{
	SendFileFacadeImpl_t1613402490::get_offset_of_messages_0(),
	SendFileFacadeImpl_t1613402490::get_offset_of_sendFileApi_1(),
	SendFileFacadeImpl_t1613402490::get_offset_of_fileReader_2(),
	SendFileFacadeImpl_t1613402490::get_offset_of_doAsync_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (FakeGroupCreation_t2089367363), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (U3CcreateGroupU3Ec__Iterator0_t621978403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2223[4] = 
{
	U3CcreateGroupU3Ec__Iterator0_t621978403::get_offset_of_callback_0(),
	U3CcreateGroupU3Ec__Iterator0_t621978403::get_offset_of_U24current_1(),
	U3CcreateGroupU3Ec__Iterator0_t621978403::get_offset_of_U24disposing_2(),
	U3CcreateGroupU3Ec__Iterator0_t621978403::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (FakeSendBatchMessages_t2772032319), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (U3CsendBatchMessagesU3Ec__Iterator0_t863552472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2225[5] = 
{
	U3CsendBatchMessagesU3Ec__Iterator0_t863552472::get_offset_of_callback_0(),
	U3CsendBatchMessagesU3Ec__Iterator0_t863552472::get_offset_of_messages_1(),
	U3CsendBatchMessagesU3Ec__Iterator0_t863552472::get_offset_of_U24current_2(),
	U3CsendBatchMessagesU3Ec__Iterator0_t863552472::get_offset_of_U24disposing_3(),
	U3CsendBatchMessagesU3Ec__Iterator0_t863552472::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (MessagePersistance_t1067416742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2226[3] = 
{
	MessagePersistance_t1067416742::get_offset_of__filename_0(),
	MessagePersistance_t1067416742::get_offset_of_filePresistance_1(),
	MessagePersistance_t1067416742::get_offset_of__messages_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (U3CgetAllU3Ec__Iterator0_t1970345158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2227[6] = 
{
	U3CgetAllU3Ec__Iterator0_t1970345158::get_offset_of_U3ClistU3E__0_0(),
	U3CgetAllU3Ec__Iterator0_t1970345158::get_offset_of_success_1(),
	U3CgetAllU3Ec__Iterator0_t1970345158::get_offset_of_U24this_2(),
	U3CgetAllU3Ec__Iterator0_t1970345158::get_offset_of_U24current_3(),
	U3CgetAllU3Ec__Iterator0_t1970345158::get_offset_of_U24disposing_4(),
	U3CgetAllU3Ec__Iterator0_t1970345158::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (U3CaddU3Ec__Iterator1_t3880396359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2228[6] = 
{
	U3CaddU3Ec__Iterator1_t3880396359::get_offset_of_message_0(),
	U3CaddU3Ec__Iterator1_t3880396359::get_offset_of_success_1(),
	U3CaddU3Ec__Iterator1_t3880396359::get_offset_of_U24this_2(),
	U3CaddU3Ec__Iterator1_t3880396359::get_offset_of_U24current_3(),
	U3CaddU3Ec__Iterator1_t3880396359::get_offset_of_U24disposing_4(),
	U3CaddU3Ec__Iterator1_t3880396359::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (U3CremoveU3Ec__Iterator2_t3988214839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2229[6] = 
{
	U3CremoveU3Ec__Iterator2_t3988214839::get_offset_of_message_0(),
	U3CremoveU3Ec__Iterator2_t3988214839::get_offset_of_success_1(),
	U3CremoveU3Ec__Iterator2_t3988214839::get_offset_of_U24this_2(),
	U3CremoveU3Ec__Iterator2_t3988214839::get_offset_of_U24current_3(),
	U3CremoveU3Ec__Iterator2_t3988214839::get_offset_of_U24disposing_4(),
	U3CremoveU3Ec__Iterator2_t3988214839::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (U3CclearU3Ec__Iterator3_t2460459301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[5] = 
{
	U3CclearU3Ec__Iterator3_t2460459301::get_offset_of_success_0(),
	U3CclearU3Ec__Iterator3_t2460459301::get_offset_of_U24this_1(),
	U3CclearU3Ec__Iterator3_t2460459301::get_offset_of_U24current_2(),
	U3CclearU3Ec__Iterator3_t2460459301::get_offset_of_U24disposing_3(),
	U3CclearU3Ec__Iterator3_t2460459301::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (MessagesFacadeImpl_t154288538), -1, sizeof(MessagesFacadeImpl_t154288538_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2232[5] = 
{
	MessagesFacadeImpl_t154288538::get_offset_of__messagePersistance_0(),
	MessagesFacadeImpl_t154288538::get_offset_of__async_1(),
	MessagesFacadeImpl_t154288538::get_offset_of__observers_2(),
	MessagesFacadeImpl_t154288538::get_offset_of__messages_3(),
	MessagesFacadeImpl_t154288538_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (U3CsaveU3Ec__AnonStorey0_t568912870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2233[3] = 
{
	U3CsaveU3Ec__AnonStorey0_t568912870::get_offset_of_message_0(),
	U3CsaveU3Ec__AnonStorey0_t568912870::get_offset_of_persistCallback_1(),
	U3CsaveU3Ec__AnonStorey0_t568912870::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (U3CremoveU3Ec__AnonStorey1_t3209414258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2234[3] = 
{
	U3CremoveU3Ec__AnonStorey1_t3209414258::get_offset_of_message_0(),
	U3CremoveU3Ec__AnonStorey1_t3209414258::get_offset_of_persistCallback_1(),
	U3CremoveU3Ec__AnonStorey1_t3209414258::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (U3CgetU3Ec__AnonStorey2_t2085224001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2235[2] = 
{
	U3CgetU3Ec__AnonStorey2_t2085224001::get_offset_of_persistCallback_0(),
	U3CgetU3Ec__AnonStorey2_t2085224001::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (U3CclearU3Ec__AnonStorey3_t1249885919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2236[2] = 
{
	U3CclearU3Ec__AnonStorey3_t1249885919::get_offset_of_callback_0(),
	U3CclearU3Ec__AnonStorey3_t1249885919::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (U3CupdateObserversU3Ec__AnonStorey4_t2571633295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2237[1] = 
{
	U3CupdateObserversU3Ec__AnonStorey4_t2571633295::get_offset_of_messages_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (SendMessageFacadeImp_t702032987), -1, sizeof(SendMessageFacadeImp_t702032987_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2239[8] = 
{
	SendMessageFacadeImp_t702032987::get_offset_of__groupCreator_0(),
	SendMessageFacadeImp_t702032987::get_offset_of__batchMessageSender_1(),
	SendMessageFacadeImp_t702032987::get_offset_of__messagesFacade_2(),
	SendMessageFacadeImp_t702032987::get_offset_of__async_3(),
	SendMessageFacadeImp_t702032987::get_offset_of__fileFacade_4(),
	SendMessageFacadeImp_t702032987::get_offset_of_groupId_5(),
	SendMessageFacadeImp_t702032987::get_offset_of__callback_6(),
	SendMessageFacadeImp_t702032987_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (FakeMessageTemplatesApi_t2375628161), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (U3CloadMessagesTemplatesU3Ec__Iterator0_t1966591915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2241[5] = 
{
	U3CloadMessagesTemplatesU3Ec__Iterator0_t1966591915::get_offset_of_U3ClistU3E__0_0(),
	U3CloadMessagesTemplatesU3Ec__Iterator0_t1966591915::get_offset_of_reposneWrapper_1(),
	U3CloadMessagesTemplatesU3Ec__Iterator0_t1966591915::get_offset_of_U24current_2(),
	U3CloadMessagesTemplatesU3Ec__Iterator0_t1966591915::get_offset_of_U24disposing_3(),
	U3CloadMessagesTemplatesU3Ec__Iterator0_t1966591915::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (MessageTemplatesFacadeImp_t2809804482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2243[5] = 
{
	MessageTemplatesFacadeImp_t2809804482::get_offset_of__observers_0(),
	MessageTemplatesFacadeImp_t2809804482::get_offset_of__messageTemplates_1(),
	MessageTemplatesFacadeImp_t2809804482::get_offset_of_messagesApi_2(),
	MessageTemplatesFacadeImp_t2809804482::get_offset_of__doAsync_3(),
	MessageTemplatesFacadeImp_t2809804482::get_offset_of__callback_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (FakeModelApi_t2762945876), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (U3CLibrary_Code_Networking_Models_ModelApi_loadModelsU3Ec__Iterator0_t1215701439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2245[5] = 
{
	U3CLibrary_Code_Networking_Models_ModelApi_loadModelsU3Ec__Iterator0_t1215701439::get_offset_of_U3CfakeModelsU3E__0_0(),
	U3CLibrary_Code_Networking_Models_ModelApi_loadModelsU3Ec__Iterator0_t1215701439::get_offset_of_reposneWrapper_1(),
	U3CLibrary_Code_Networking_Models_ModelApi_loadModelsU3Ec__Iterator0_t1215701439::get_offset_of_U24current_2(),
	U3CLibrary_Code_Networking_Models_ModelApi_loadModelsU3Ec__Iterator0_t1215701439::get_offset_of_U24disposing_3(),
	U3CLibrary_Code_Networking_Models_ModelApi_loadModelsU3Ec__Iterator0_t1215701439::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (ModelFacadeImpl_t3221237801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2247[5] = 
{
	ModelFacadeImpl_t3221237801::get_offset_of__modelPersistance_0(),
	ModelFacadeImpl_t3221237801::get_offset_of_doAsync_1(),
	ModelFacadeImpl_t3221237801::get_offset_of__observers_2(),
	ModelFacadeImpl_t3221237801::get_offset_of__models_3(),
	ModelFacadeImpl_t3221237801::get_offset_of_modelApi_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (U3CsaveU3Ec__AnonStorey0_t2000402439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2248[3] = 
{
	U3CsaveU3Ec__AnonStorey0_t2000402439::get_offset_of_models_0(),
	U3CsaveU3Ec__AnonStorey0_t2000402439::get_offset_of_persistCallback_1(),
	U3CsaveU3Ec__AnonStorey0_t2000402439::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (U3CremoveU3Ec__AnonStorey1_t2484859181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2249[3] = 
{
	U3CremoveU3Ec__AnonStorey1_t2484859181::get_offset_of_models_0(),
	U3CremoveU3Ec__AnonStorey1_t2484859181::get_offset_of_persistCallback_1(),
	U3CremoveU3Ec__AnonStorey1_t2484859181::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (U3CgetU3Ec__AnonStorey2_t241217034), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2250[1] = 
{
	U3CgetU3Ec__AnonStorey2_t241217034::get_offset_of_persistCallback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (U3CupdateObserversU3Ec__AnonStorey3_t193582475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2251[1] = 
{
	U3CupdateObserversU3Ec__AnonStorey3_t193582475::get_offset_of_messages_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (ModelsPersistanceImpl_t1155557687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2252[3] = 
{
	ModelsPersistanceImpl_t1155557687::get_offset_of__filename_0(),
	ModelsPersistanceImpl_t1155557687::get_offset_of_filePresistance_1(),
	ModelsPersistanceImpl_t1155557687::get_offset_of__models_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (U3CgetAllU3Ec__Iterator0_t1222501571), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2253[6] = 
{
	U3CgetAllU3Ec__Iterator0_t1222501571::get_offset_of_U3ClistU3E__0_0(),
	U3CgetAllU3Ec__Iterator0_t1222501571::get_offset_of_success_1(),
	U3CgetAllU3Ec__Iterator0_t1222501571::get_offset_of_U24this_2(),
	U3CgetAllU3Ec__Iterator0_t1222501571::get_offset_of_U24current_3(),
	U3CgetAllU3Ec__Iterator0_t1222501571::get_offset_of_U24disposing_4(),
	U3CgetAllU3Ec__Iterator0_t1222501571::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (U3CaddU3Ec__Iterator1_t690995996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2254[6] = 
{
	U3CaddU3Ec__Iterator1_t690995996::get_offset_of_message_0(),
	U3CaddU3Ec__Iterator1_t690995996::get_offset_of_success_1(),
	U3CaddU3Ec__Iterator1_t690995996::get_offset_of_U24this_2(),
	U3CaddU3Ec__Iterator1_t690995996::get_offset_of_U24current_3(),
	U3CaddU3Ec__Iterator1_t690995996::get_offset_of_U24disposing_4(),
	U3CaddU3Ec__Iterator1_t690995996::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (U3CremoveU3Ec__Iterator2_t1286003366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2255[6] = 
{
	U3CremoveU3Ec__Iterator2_t1286003366::get_offset_of_message_0(),
	U3CremoveU3Ec__Iterator2_t1286003366::get_offset_of_success_1(),
	U3CremoveU3Ec__Iterator2_t1286003366::get_offset_of_U24this_2(),
	U3CremoveU3Ec__Iterator2_t1286003366::get_offset_of_U24current_3(),
	U3CremoveU3Ec__Iterator2_t1286003366::get_offset_of_U24disposing_4(),
	U3CremoveU3Ec__Iterator2_t1286003366::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (U3CclearU3Ec__Iterator3_t1383450968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2256[5] = 
{
	U3CclearU3Ec__Iterator3_t1383450968::get_offset_of_success_0(),
	U3CclearU3Ec__Iterator3_t1383450968::get_offset_of_U24this_1(),
	U3CclearU3Ec__Iterator3_t1383450968::get_offset_of_U24current_2(),
	U3CclearU3Ec__Iterator3_t1383450968::get_offset_of_U24disposing_3(),
	U3CclearU3Ec__Iterator3_t1383450968::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (FakeGroupinModelApi_t2519811264), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (U3CloadModelGroupingDataU3Ec__Iterator0_t1419613339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2258[5] = 
{
	U3CloadModelGroupingDataU3Ec__Iterator0_t1419613339::get_offset_of_U3ClistU3E__0_0(),
	U3CloadModelGroupingDataU3Ec__Iterator0_t1419613339::get_offset_of_reposneWrapper_1(),
	U3CloadModelGroupingDataU3Ec__Iterator0_t1419613339::get_offset_of_U24current_2(),
	U3CloadModelGroupingDataU3Ec__Iterator0_t1419613339::get_offset_of_U24disposing_3(),
	U3CloadModelGroupingDataU3Ec__Iterator0_t1419613339::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (ModelGroupDtoFactory_t474877319), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (U3CcreateDtosU3Ec__AnonStorey0_t827342538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2260[4] = 
{
	U3CcreateDtosU3Ec__AnonStorey0_t827342538::get_offset_of_groups_0(),
	U3CcreateDtosU3Ec__AnonStorey0_t827342538::get_offset_of_map_1(),
	U3CcreateDtosU3Ec__AnonStorey0_t827342538::get_offset_of_modelsCopy_2(),
	U3CcreateDtosU3Ec__AnonStorey0_t827342538::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (U3CcreateDtosU3Ec__AnonStorey1_t1243368241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2261[3] = 
{
	U3CcreateDtosU3Ec__AnonStorey1_t1243368241::get_offset_of_gameObjName_0(),
	U3CcreateDtosU3Ec__AnonStorey1_t1243368241::get_offset_of_gameObject_1(),
	U3CcreateDtosU3Ec__AnonStorey1_t1243368241::get_offset_of_U3CU3Ef__refU240_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (ModelGroupFacadeImp_t1471023694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2263[6] = 
{
	ModelGroupFacadeImp_t1471023694::get_offset_of_model_0(),
	ModelGroupFacadeImp_t1471023694::get_offset_of__observers_1(),
	ModelGroupFacadeImp_t1471023694::get_offset_of__groupingModelApi_2(),
	ModelGroupFacadeImp_t1471023694::get_offset_of__async_3(),
	ModelGroupFacadeImp_t1471023694::get_offset_of__modelGroupDtos_4(),
	ModelGroupFacadeImp_t1471023694::get_offset_of__modelGroups_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (PathHolderImp_t857450797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2270[3] = 
{
	PathHolderImp_t857450797::get_offset_of_filePath_2(),
	PathHolderImp_t857450797::get_offset_of__paths_3(),
	PathHolderImp_t857450797::get_offset_of_recordPath_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (SnapshotPersistanceProvider_t3366466112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2271[1] = 
{
	SnapshotPersistanceProvider_t3366466112::get_offset_of_snapshotPersistance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (Download3dElementImp_t2560998171), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2274[1] = 
{
	Download3dElementImp_t2560998171::get_offset_of_returnAssetBundle_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (U3CdownloadModelU3Ec__Iterator0_t2709715940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[8] = 
{
	U3CdownloadModelU3Ec__Iterator0_t2709715940::get_offset_of_element_0(),
	U3CdownloadModelU3Ec__Iterator0_t2709715940::get_offset_of_U3CurlU3E__0_1(),
	U3CdownloadModelU3Ec__Iterator0_t2709715940::get_offset_of_U3CwwwU3E__0_2(),
	U3CdownloadModelU3Ec__Iterator0_t2709715940::get_offset_of_wrapper_3(),
	U3CdownloadModelU3Ec__Iterator0_t2709715940::get_offset_of_U24this_4(),
	U3CdownloadModelU3Ec__Iterator0_t2709715940::get_offset_of_U24current_5(),
	U3CdownloadModelU3Ec__Iterator0_t2709715940::get_offset_of_U24disposing_6(),
	U3CdownloadModelU3Ec__Iterator0_t2709715940::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (ChangeCheckListItemStatusApiImp_t3896993653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2279[3] = 
{
	ChangeCheckListItemStatusApiImp_t3896993653::get_offset_of_ulr_0(),
	ChangeCheckListItemStatusApiImp_t3896993653::get_offset_of__parser_1(),
	ChangeCheckListItemStatusApiImp_t3896993653::get_offset_of__requestBuilder_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (U3CchangeItemStatusU3Ec__Iterator0_t1382815528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2280[9] = 
{
	U3CchangeItemStatusU3Ec__Iterator0_t1382815528::get_offset_of_itemStatus_0(),
	U3CchangeItemStatusU3Ec__Iterator0_t1382815528::get_offset_of_elementId_1(),
	U3CchangeItemStatusU3Ec__Iterator0_t1382815528::get_offset_of_U3CrequestU3E__0_2(),
	U3CchangeItemStatusU3Ec__Iterator0_t1382815528::get_offset_of_fail_3(),
	U3CchangeItemStatusU3Ec__Iterator0_t1382815528::get_offset_of_success_4(),
	U3CchangeItemStatusU3Ec__Iterator0_t1382815528::get_offset_of_U24this_5(),
	U3CchangeItemStatusU3Ec__Iterator0_t1382815528::get_offset_of_U24current_6(),
	U3CchangeItemStatusU3Ec__Iterator0_t1382815528::get_offset_of_U24disposing_7(),
	U3CchangeItemStatusU3Ec__Iterator0_t1382815528::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (FakeChangeChecklistItemStatusApi_t3817536300), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (U3CchangeItemStatusU3Ec__Iterator0_t3672242215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2282[4] = 
{
	U3CchangeItemStatusU3Ec__Iterator0_t3672242215::get_offset_of_success_0(),
	U3CchangeItemStatusU3Ec__Iterator0_t3672242215::get_offset_of_U24current_1(),
	U3CchangeItemStatusU3Ec__Iterator0_t3672242215::get_offset_of_U24disposing_2(),
	U3CchangeItemStatusU3Ec__Iterator0_t3672242215::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (FakeLoadChecklistForElementApi_t3867601896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (U3CloadElementsU3Ec__Iterator0_t2255347523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2284[6] = 
{
	U3CloadElementsU3Ec__Iterator0_t2255347523::get_offset_of_U3ClistU3E__0_0(),
	U3CloadElementsU3Ec__Iterator0_t2255347523::get_offset_of_element_1(),
	U3CloadElementsU3Ec__Iterator0_t2255347523::get_offset_of_reposneWrapper_2(),
	U3CloadElementsU3Ec__Iterator0_t2255347523::get_offset_of_U24current_3(),
	U3CloadElementsU3Ec__Iterator0_t2255347523::get_offset_of_U24disposing_4(),
	U3CloadElementsU3Ec__Iterator0_t2255347523::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (LoadChecklistForElementApiImp_t310732585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2286[3] = 
{
	LoadChecklistForElementApiImp_t310732585::get_offset_of__parser_0(),
	LoadChecklistForElementApiImp_t310732585::get_offset_of_url_1(),
	LoadChecklistForElementApiImp_t310732585::get_offset_of__builder_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (U3CloadElementsU3Ec__Iterator0_t354627748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2287[7] = 
{
	U3CloadElementsU3Ec__Iterator0_t354627748::get_offset_of_element_0(),
	U3CloadElementsU3Ec__Iterator0_t354627748::get_offset_of_U3CrequestU3E__0_1(),
	U3CloadElementsU3Ec__Iterator0_t354627748::get_offset_of_reposneWrapper_2(),
	U3CloadElementsU3Ec__Iterator0_t354627748::get_offset_of_U24this_3(),
	U3CloadElementsU3Ec__Iterator0_t354627748::get_offset_of_U24current_4(),
	U3CloadElementsU3Ec__Iterator0_t354627748::get_offset_of_U24disposing_5(),
	U3CloadElementsU3Ec__Iterator0_t354627748::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (ElementApiImp_t2310790088), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2289[3] = 
{
	ElementApiImp_t2310790088::get_offset_of__parser_0(),
	ElementApiImp_t2310790088::get_offset_of_url_1(),
	ElementApiImp_t2310790088::get_offset_of__builder_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (U3CloadElementsU3Ec__Iterator0_t594275620), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2290[6] = 
{
	U3CloadElementsU3Ec__Iterator0_t594275620::get_offset_of_U3CrequestU3E__0_0(),
	U3CloadElementsU3Ec__Iterator0_t594275620::get_offset_of_reposneWrapper_1(),
	U3CloadElementsU3Ec__Iterator0_t594275620::get_offset_of_U24this_2(),
	U3CloadElementsU3Ec__Iterator0_t594275620::get_offset_of_U24current_3(),
	U3CloadElementsU3Ec__Iterator0_t594275620::get_offset_of_U24disposing_4(),
	U3CloadElementsU3Ec__Iterator0_t594275620::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (ElementStatusApiImpl_t2005129180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2292[3] = 
{
	ElementStatusApiImpl_t2005129180::get_offset_of_url_0(),
	ElementStatusApiImpl_t2005129180::get_offset_of_parser_1(),
	ElementStatusApiImpl_t2005129180::get_offset_of__builder_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (U3CapproveElementStatusU3Ec__Iterator0_t4133233964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2293[8] = 
{
	U3CapproveElementStatusU3Ec__Iterator0_t4133233964::get_offset_of_element_0(),
	U3CapproveElementStatusU3Ec__Iterator0_t4133233964::get_offset_of_U3CurllU3E__0_1(),
	U3CapproveElementStatusU3Ec__Iterator0_t4133233964::get_offset_of_U3CrequestU3E__0_2(),
	U3CapproveElementStatusU3Ec__Iterator0_t4133233964::get_offset_of_wrapper_3(),
	U3CapproveElementStatusU3Ec__Iterator0_t4133233964::get_offset_of_U24this_4(),
	U3CapproveElementStatusU3Ec__Iterator0_t4133233964::get_offset_of_U24current_5(),
	U3CapproveElementStatusU3Ec__Iterator0_t4133233964::get_offset_of_U24disposing_6(),
	U3CapproveElementStatusU3Ec__Iterator0_t4133233964::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (U3CsetCurrentStatusU3Ec__Iterator1_t1596340271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2294[9] = 
{
	U3CsetCurrentStatusU3Ec__Iterator1_t1596340271::get_offset_of_element_0(),
	U3CsetCurrentStatusU3Ec__Iterator1_t1596340271::get_offset_of_status_1(),
	U3CsetCurrentStatusU3Ec__Iterator1_t1596340271::get_offset_of_U3CurllU3E__0_2(),
	U3CsetCurrentStatusU3Ec__Iterator1_t1596340271::get_offset_of_U3CrequestU3E__0_3(),
	U3CsetCurrentStatusU3Ec__Iterator1_t1596340271::get_offset_of_wrapper_4(),
	U3CsetCurrentStatusU3Ec__Iterator1_t1596340271::get_offset_of_U24this_5(),
	U3CsetCurrentStatusU3Ec__Iterator1_t1596340271::get_offset_of_U24current_6(),
	U3CsetCurrentStatusU3Ec__Iterator1_t1596340271::get_offset_of_U24disposing_7(),
	U3CsetCurrentStatusU3Ec__Iterator1_t1596340271::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (FakeElementStatusApi_t3580592613), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (U3CapproveElementStatusU3Ec__Iterator0_t1302570149), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2296[4] = 
{
	U3CapproveElementStatusU3Ec__Iterator0_t1302570149::get_offset_of_wrapper_0(),
	U3CapproveElementStatusU3Ec__Iterator0_t1302570149::get_offset_of_U24current_1(),
	U3CapproveElementStatusU3Ec__Iterator0_t1302570149::get_offset_of_U24disposing_2(),
	U3CapproveElementStatusU3Ec__Iterator0_t1302570149::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (U3CsetCurrentStatusU3Ec__Iterator1_t3340147720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2297[6] = 
{
	U3CsetCurrentStatusU3Ec__Iterator1_t3340147720::get_offset_of_status_0(),
	U3CsetCurrentStatusU3Ec__Iterator1_t3340147720::get_offset_of_element_1(),
	U3CsetCurrentStatusU3Ec__Iterator1_t3340147720::get_offset_of_wrapper_2(),
	U3CsetCurrentStatusU3Ec__Iterator1_t3340147720::get_offset_of_U24current_3(),
	U3CsetCurrentStatusU3Ec__Iterator1_t3340147720::get_offset_of_U24disposing_4(),
	U3CsetCurrentStatusU3Ec__Iterator1_t3340147720::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (FailReason_t476307514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2298[2] = 
{
	FailReason_t476307514::get_offset_of_failReason_0(),
	FailReason_t476307514::get_offset_of_enumReason_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (FailEnums_t680706100)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2299[2] = 
{
	FailEnums_t680706100::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
