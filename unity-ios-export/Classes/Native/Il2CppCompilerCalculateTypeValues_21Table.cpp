﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Element>
struct List_1_t1645709140;
// Library.Code.Domain.Dtos.CameraPositionDto
struct CameraPositionDto_t1507864167;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Library.Code.Facades.Elements.ElementFacade
struct ElementFacade_t512443804;
// Library.Code.Networking._3dElement.DownloadElement3d
struct DownloadElement3d_t3227939199;
// Library.Code.UI.PrefabGenerator.CreateInterface
struct CreateInterface_t1919332287;
// Library.Code.Domain.Dtos.ElementWithObject
struct ElementWithObject_t2362593585;
// Library.Code.Utils.Async.DoAsync
struct DoAsync_t850966475;
// System.Collections.Generic.List`1<Library.Code.Facades.ObserverSingle`1<Library.Code.Domain.Dtos.ElementWithObject>>
struct List_1_t3385257581;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// System.Collections.Generic.List`1<Library.Code.Domain.Entities.MessageTemplate>
struct List_1_t2162674683;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Int64>
struct List_1_t278199169;
// Library.Code.Domain.Entities.DefaultElementsEmbedded
struct DefaultElementsEmbedded_t250135042;
// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Checklist.ItemStatus>
struct List_1_t747872829;
// Library.Code.Domain.Entities.Checklist.ElementStatus
struct ElementStatus_t2221316768;
// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Checklist.CheckList>
struct List_1_t3501169210;
// Library.Code.Networking.Checklist.LoadChecklistForElementApi
struct LoadChecklistForElementApi_t2929755249;
// Library.Code.Networking.Checklist.ChangeChecklistItemStatusApi
struct ChangeChecklistItemStatusApi_t1689193757;
// System.Collections.Generic.List`1<Library.Code.Facades.ObserverSingle`1<Library.Code.Domain.Entities.Checklist.CheckList>>
struct List_1_t859744778;
// Library.Code.Domain.Entities.Element
struct Element_t2276588008;
// Library.Code.Domain.Entities.Checklist.ItemStatus
struct ItemStatus_t1378751697;
// Library.Code.Domain.Entities.DefaultModelEmbedded
struct DefaultModelEmbedded_t2860339312;
// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model>
struct List_1_t1532750683;
// System.Collections.Generic.List`1<Library.Code.Domain.Entities.PrefixColor.PrefixColor>
struct List_1_t949058795;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Message>
struct List_1_t583975089;
// Library.Code.Domain.Entities.DefaultMessageEmbadded
struct DefaultMessageEmbadded_t2732967408;
// Library.Code.Facades.Messages.SendMessageFacade
struct SendMessageFacade_t1083868397;
// Library.Code.Facades.MessageTemplates.MessageTemplatesFacade
struct MessageTemplatesFacade_t4086936850;
// Library.Code.Facades.Callback
struct Callback_t1820017589;
// Library.Code.Facades.Elements.ElementFacadeImpl
struct ElementFacadeImpl_t1898847230;
// Library.Code.Facades.Models.ModelsPersistanceImpl
struct ModelsPersistanceImpl_t1155557687;
// Library.Code.Facades.Models.ModelFacade
struct ModelFacade_t1760456667;
// Library.Code.Facades.PersistanceFacade`1<Library.Code.Domain.Entities.Element>
struct PersistanceFacade_1_t586719681;
// Library.Code.Networking.Elements.ElementApi
struct ElementApi_t1787243248;
// System.Collections.Generic.List`1<Library.Code.Facades.Observer`1<Library.Code.Domain.Entities.Element>>
struct List_1_t4178948084;
// UnityEngine.Events.UnityAction`1<Library.Code.Domain.Entities.Element>
struct UnityAction_1_t3643173759;
// Library.Code.Domain.Entities.Model
struct Model_t2163629551;
// Library.Code.Networking.Elements.ElementStatusApi
struct ElementStatusApi_t3439319632;
// Library.Code.Facades._3dElementDownload.Element3dFacade
struct Element3dFacade_t3552582347;
// UnityEngine.Transform
struct Transform_t3275118058;
// Library.Code.UI.PrefabGenerator.PrefabGenerator
struct PrefabGenerator_t3099279183;
// Library.Code.DI.PopUp.PopUpShower
struct PopUpShower_t3186557280;
// System.Action
struct Action_t3226471752;
// Library.Code.Facades.ModelGrouping.ModelGroupFacade
struct ModelGroupFacade_t1233541942;
// Library.Code.Facades.CheckListF.CheckListFacade
struct CheckListFacade_t3860198914;
// Library.Code.Facades.Messages.MessagePersistance
struct MessagePersistance_t1067416742;
// Library.Code.Facades.Messages.MessagesFacade
struct MessagesFacade_t3479558102;
// Library.Code.Facades.Elements.ElementApproveStateFacade
struct ElementApproveStateFacade_t2685088950;
// Library.Code.Facades.Elements.ElementPersistanceImpl
struct ElementPersistanceImpl_t1641319881;
// Library.Code.Facades.CheckPoints.CheckPointsFacade
struct CheckPointsFacade_t481555209;
// Library.Code.Facades.ElementHandler.ElementHandlerFacade
struct ElementHandlerFacade_t2821299992;
// System.Collections.Generic.List`1<Library.Code.Facades.Observer`1<Library.Code.Domain.Dtos.CheckPointInfo>>
struct List_1_t956163168;
// System.Collections.Generic.List`1<Library.Code.Domain.Dtos.CheckPointInfo>
struct List_1_t2717891520;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// Library.Code.Domain.Dtos.CheckPointInfo
struct CheckPointInfo_t3348770388;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Collections.Generic.List`1<Library.Code.Domain.Entities.ApproveElementResponse>
struct List_1_t2134893648;
// System.Collections.Generic.List`1<Library.Code.Domain.Entities.PrefixColor.ColorParts>
struct List_1_t4163807915;
// Library.Code.DI.NetworkConfig
struct NetworkConfig_t3187165974;
// System.Collections.Generic.List`1<Library.Code.WebRequest.HeaderClass>
struct List_1_t1700750585;
// Library.Code.WebRequest.HeaderClass
struct HeaderClass_t2331629453;
// Library.Code.Domain.Entities.Checklist.CheckList
struct CheckList_t4132048078;
// System.Collections.Generic.List`1<Library.Code.Facades.ObserverSingle`1<Library.Code.Domain.Entities.Checklist.ItemStatus>>
struct List_1_t2401415693;
// Library.Code.UI.PopUp.PopUpStuff
struct PopUpStuff_t3426923330;
// UnityEngine.AssetBundle
struct AssetBundle_t2054978754;
// Library.Code.Facades.Files.SendFileFacade
struct SendFileFacade_t1187763752;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Void
struct Void_t1841601450;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// System.Collections.Generic.IEqualityComparer`1<Library.Code.Domain.Entities.ModelGroup>
struct IEqualityComparer_1_t2446820424;
// Library.Code.DI.Element3d.Element3dModule
struct Element3dModule_t2912470317;
// Library.Code.DI.Elements.ElementProvider
struct ElementProvider_t3018113545;
// Library.Code.DI.Element3d.CheckpointModule
struct CheckpointModule_t504654608;
// Library.Code.DI.Checklist.ChecklistProvider
struct ChecklistProvider_t3393423783;
// Library.Code.DI.Element3d.Element3dProvider
struct Element3dProvider_t3146139312;
// Library.Code.DI.GroupinModel.GroupingModelProvider
struct GroupingModelProvider_t2551172685;
// Library.Code.DI.ElementHandler.ElementHandlerModule
struct ElementHandlerModule_t3201733362;
// Library.Code.Facades.Camera.CameraFacadeImp
struct CameraFacadeImp_t1649784751;
// Library.Code.DI.Message.MessageFacadeModule
struct MessageFacadeModule_t1147318505;
// Library.Code.DI.Message.SendMessageModule
struct SendMessageModule_t690017393;
// Library.Code.DI.SendFile.SendFileModule
struct SendFileModule_t3843131166;
// Library.Code.DI.Elements.ElementFacadeModule
struct ElementFacadeModule_t1081315832;
// Library.Code.DI.GroupinModel.GroupingModelModule
struct GroupingModelModule_t337983572;
// Library.Code.DI.MessageTempaltes.MessageTempaltesModule
struct MessageTempaltesModule_t744814112;
// Library.Code.DI.Models.ModelFacadeModule
struct ModelFacadeModule_t3252945753;
// Library.Code.DI.Elements.ElementApproveStatusModule
struct ElementApproveStatusModule_t4176563737;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef HOLOFILETYPECONVERTER_T3247504072_H
#define HOLOFILETYPECONVERTER_T3247504072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.Enums.HoloFileTypeConverter
struct  HoloFileTypeConverter_t3247504072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLOFILETYPECONVERTER_T3247504072_H
#ifndef DEFAULTELEMENTSEMBEDDED_T250135042_H
#define DEFAULTELEMENTSEMBEDDED_T250135042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.DefaultElementsEmbedded
struct  DefaultElementsEmbedded_t250135042  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Element> Library.Code.Domain.Entities.DefaultElementsEmbedded::elements
	List_1_t1645709140 * ___elements_0;

public:
	inline static int32_t get_offset_of_elements_0() { return static_cast<int32_t>(offsetof(DefaultElementsEmbedded_t250135042, ___elements_0)); }
	inline List_1_t1645709140 * get_elements_0() const { return ___elements_0; }
	inline List_1_t1645709140 ** get_address_of_elements_0() { return &___elements_0; }
	inline void set_elements_0(List_1_t1645709140 * value)
	{
		___elements_0 = value;
		Il2CppCodeGenWriteBarrier((&___elements_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTELEMENTSEMBEDDED_T250135042_H
#ifndef CAMERAFACADEIMP_T1649784751_H
#define CAMERAFACADEIMP_T1649784751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Camera.CameraFacadeImp
struct  CameraFacadeImp_t1649784751  : public RuntimeObject
{
public:
	// Library.Code.Domain.Dtos.CameraPositionDto Library.Code.Facades.Camera.CameraFacadeImp::_cameraPositionDto
	CameraPositionDto_t1507864167 * ____cameraPositionDto_0;
	// UnityEngine.GameObject Library.Code.Facades.Camera.CameraFacadeImp::loadedObject
	GameObject_t1756533147 * ___loadedObject_1;

public:
	inline static int32_t get_offset_of__cameraPositionDto_0() { return static_cast<int32_t>(offsetof(CameraFacadeImp_t1649784751, ____cameraPositionDto_0)); }
	inline CameraPositionDto_t1507864167 * get__cameraPositionDto_0() const { return ____cameraPositionDto_0; }
	inline CameraPositionDto_t1507864167 ** get_address_of__cameraPositionDto_0() { return &____cameraPositionDto_0; }
	inline void set__cameraPositionDto_0(CameraPositionDto_t1507864167 * value)
	{
		____cameraPositionDto_0 = value;
		Il2CppCodeGenWriteBarrier((&____cameraPositionDto_0), value);
	}

	inline static int32_t get_offset_of_loadedObject_1() { return static_cast<int32_t>(offsetof(CameraFacadeImp_t1649784751, ___loadedObject_1)); }
	inline GameObject_t1756533147 * get_loadedObject_1() const { return ___loadedObject_1; }
	inline GameObject_t1756533147 ** get_address_of_loadedObject_1() { return &___loadedObject_1; }
	inline void set_loadedObject_1(GameObject_t1756533147 * value)
	{
		___loadedObject_1 = value;
		Il2CppCodeGenWriteBarrier((&___loadedObject_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAFACADEIMP_T1649784751_H
#ifndef STATECONVERTER_T4104478257_H
#define STATECONVERTER_T4104478257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.Enums.StateConverter
struct  StateConverter_t4104478257  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATECONVERTER_T4104478257_H
#ifndef ELEMENT3DFACADEIMP_T2774445145_H
#define ELEMENT3DFACADEIMP_T2774445145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades._3dElementDownload.Element3dFacadeImp
struct  Element3dFacadeImp_t2774445145  : public RuntimeObject
{
public:
	// Library.Code.Facades.Elements.ElementFacade Library.Code.Facades._3dElementDownload.Element3dFacadeImp::_elementFacade
	RuntimeObject* ____elementFacade_0;
	// Library.Code.Networking._3dElement.DownloadElement3d Library.Code.Facades._3dElementDownload.Element3dFacadeImp::_downloadElement3D
	RuntimeObject* ____downloadElement3D_1;
	// Library.Code.UI.PrefabGenerator.CreateInterface Library.Code.Facades._3dElementDownload.Element3dFacadeImp::_createInterface
	RuntimeObject* ____createInterface_2;
	// Library.Code.Domain.Dtos.ElementWithObject Library.Code.Facades._3dElementDownload.Element3dFacadeImp::_currentElement
	ElementWithObject_t2362593585 * ____currentElement_3;
	// Library.Code.Utils.Async.DoAsync Library.Code.Facades._3dElementDownload.Element3dFacadeImp::_async
	RuntimeObject* ____async_4;
	// System.Collections.Generic.List`1<Library.Code.Facades.ObserverSingle`1<Library.Code.Domain.Dtos.ElementWithObject>> Library.Code.Facades._3dElementDownload.Element3dFacadeImp::_observers
	List_1_t3385257581 * ____observers_5;
	// System.Int32 Library.Code.Facades._3dElementDownload.Element3dFacadeImp::loadingScale
	int32_t ___loadingScale_6;

public:
	inline static int32_t get_offset_of__elementFacade_0() { return static_cast<int32_t>(offsetof(Element3dFacadeImp_t2774445145, ____elementFacade_0)); }
	inline RuntimeObject* get__elementFacade_0() const { return ____elementFacade_0; }
	inline RuntimeObject** get_address_of__elementFacade_0() { return &____elementFacade_0; }
	inline void set__elementFacade_0(RuntimeObject* value)
	{
		____elementFacade_0 = value;
		Il2CppCodeGenWriteBarrier((&____elementFacade_0), value);
	}

	inline static int32_t get_offset_of__downloadElement3D_1() { return static_cast<int32_t>(offsetof(Element3dFacadeImp_t2774445145, ____downloadElement3D_1)); }
	inline RuntimeObject* get__downloadElement3D_1() const { return ____downloadElement3D_1; }
	inline RuntimeObject** get_address_of__downloadElement3D_1() { return &____downloadElement3D_1; }
	inline void set__downloadElement3D_1(RuntimeObject* value)
	{
		____downloadElement3D_1 = value;
		Il2CppCodeGenWriteBarrier((&____downloadElement3D_1), value);
	}

	inline static int32_t get_offset_of__createInterface_2() { return static_cast<int32_t>(offsetof(Element3dFacadeImp_t2774445145, ____createInterface_2)); }
	inline RuntimeObject* get__createInterface_2() const { return ____createInterface_2; }
	inline RuntimeObject** get_address_of__createInterface_2() { return &____createInterface_2; }
	inline void set__createInterface_2(RuntimeObject* value)
	{
		____createInterface_2 = value;
		Il2CppCodeGenWriteBarrier((&____createInterface_2), value);
	}

	inline static int32_t get_offset_of__currentElement_3() { return static_cast<int32_t>(offsetof(Element3dFacadeImp_t2774445145, ____currentElement_3)); }
	inline ElementWithObject_t2362593585 * get__currentElement_3() const { return ____currentElement_3; }
	inline ElementWithObject_t2362593585 ** get_address_of__currentElement_3() { return &____currentElement_3; }
	inline void set__currentElement_3(ElementWithObject_t2362593585 * value)
	{
		____currentElement_3 = value;
		Il2CppCodeGenWriteBarrier((&____currentElement_3), value);
	}

	inline static int32_t get_offset_of__async_4() { return static_cast<int32_t>(offsetof(Element3dFacadeImp_t2774445145, ____async_4)); }
	inline RuntimeObject* get__async_4() const { return ____async_4; }
	inline RuntimeObject** get_address_of__async_4() { return &____async_4; }
	inline void set__async_4(RuntimeObject* value)
	{
		____async_4 = value;
		Il2CppCodeGenWriteBarrier((&____async_4), value);
	}

	inline static int32_t get_offset_of__observers_5() { return static_cast<int32_t>(offsetof(Element3dFacadeImp_t2774445145, ____observers_5)); }
	inline List_1_t3385257581 * get__observers_5() const { return ____observers_5; }
	inline List_1_t3385257581 ** get_address_of__observers_5() { return &____observers_5; }
	inline void set__observers_5(List_1_t3385257581 * value)
	{
		____observers_5 = value;
		Il2CppCodeGenWriteBarrier((&____observers_5), value);
	}

	inline static int32_t get_offset_of_loadingScale_6() { return static_cast<int32_t>(offsetof(Element3dFacadeImp_t2774445145, ___loadingScale_6)); }
	inline int32_t get_loadingScale_6() const { return ___loadingScale_6; }
	inline int32_t* get_address_of_loadingScale_6() { return &___loadingScale_6; }
	inline void set_loadingScale_6(int32_t value)
	{
		___loadingScale_6 = value;
	}
};

struct Element3dFacadeImp_t2774445145_StaticFields
{
public:
	// UnityEngine.Events.UnityAction Library.Code.Facades._3dElementDownload.Element3dFacadeImp::<>f__am$cache0
	UnityAction_t4025899511 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(Element3dFacadeImp_t2774445145_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline UnityAction_t4025899511 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline UnityAction_t4025899511 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(UnityAction_t4025899511 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT3DFACADEIMP_T2774445145_H
#ifndef TEMPLATELIST_T3112186284_H
#define TEMPLATELIST_T3112186284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.TemplateList
struct  TemplateList_t3112186284  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.MessageTemplate> Library.Code.Domain.Entities.TemplateList::templates
	List_1_t2162674683 * ___templates_0;

public:
	inline static int32_t get_offset_of_templates_0() { return static_cast<int32_t>(offsetof(TemplateList_t3112186284, ___templates_0)); }
	inline List_1_t2162674683 * get_templates_0() const { return ___templates_0; }
	inline List_1_t2162674683 ** get_address_of_templates_0() { return &___templates_0; }
	inline void set_templates_0(List_1_t2162674683 * value)
	{
		___templates_0 = value;
		Il2CppCodeGenWriteBarrier((&___templates_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEMPLATELIST_T3112186284_H
#ifndef GROUPCREATE_T2003352421_H
#define GROUPCREATE_T2003352421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.GroupCreate
struct  GroupCreate_t2003352421  : public RuntimeObject
{
public:
	// System.Int64 Library.Code.Domain.Entities.GroupCreate::id
	int64_t ___id_0;
	// System.String Library.Code.Domain.Entities.GroupCreate::name
	String_t* ___name_1;
	// System.Collections.Generic.List`1<System.Int64> Library.Code.Domain.Entities.GroupCreate::messageIds
	List_1_t278199169 * ___messageIds_2;
	// System.Int64 Library.Code.Domain.Entities.GroupCreate::elementId
	int64_t ___elementId_3;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(GroupCreate_t2003352421, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(GroupCreate_t2003352421, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_messageIds_2() { return static_cast<int32_t>(offsetof(GroupCreate_t2003352421, ___messageIds_2)); }
	inline List_1_t278199169 * get_messageIds_2() const { return ___messageIds_2; }
	inline List_1_t278199169 ** get_address_of_messageIds_2() { return &___messageIds_2; }
	inline void set_messageIds_2(List_1_t278199169 * value)
	{
		___messageIds_2 = value;
		Il2CppCodeGenWriteBarrier((&___messageIds_2), value);
	}

	inline static int32_t get_offset_of_elementId_3() { return static_cast<int32_t>(offsetof(GroupCreate_t2003352421, ___elementId_3)); }
	inline int64_t get_elementId_3() const { return ___elementId_3; }
	inline int64_t* get_address_of_elementId_3() { return &___elementId_3; }
	inline void set_elementId_3(int64_t value)
	{
		___elementId_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPCREATE_T2003352421_H
#ifndef GROUPCREATERESPONSE_T2325302190_H
#define GROUPCREATERESPONSE_T2325302190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.GroupCreateResponse
struct  GroupCreateResponse_t2325302190  : public RuntimeObject
{
public:
	// System.Int64 Library.Code.Domain.Entities.GroupCreateResponse::id
	int64_t ___id_0;
	// System.String Library.Code.Domain.Entities.GroupCreateResponse::name
	String_t* ___name_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(GroupCreateResponse_t2325302190, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(GroupCreateResponse_t2325302190, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPCREATERESPONSE_T2325302190_H
#ifndef ELEMENTSEMBEDDED_T1754787703_H
#define ELEMENTSEMBEDDED_T1754787703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.ElementsEmbedded
struct  ElementsEmbedded_t1754787703  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.DefaultElementsEmbedded Library.Code.Domain.Entities.ElementsEmbedded::_embedded
	DefaultElementsEmbedded_t250135042 * ____embedded_0;

public:
	inline static int32_t get_offset_of__embedded_0() { return static_cast<int32_t>(offsetof(ElementsEmbedded_t1754787703, ____embedded_0)); }
	inline DefaultElementsEmbedded_t250135042 * get__embedded_0() const { return ____embedded_0; }
	inline DefaultElementsEmbedded_t250135042 ** get_address_of__embedded_0() { return &____embedded_0; }
	inline void set__embedded_0(DefaultElementsEmbedded_t250135042 * value)
	{
		____embedded_0 = value;
		Il2CppCodeGenWriteBarrier((&____embedded_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTSEMBEDDED_T1754787703_H
#ifndef CHECKLIST_T4132048078_H
#define CHECKLIST_T4132048078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.Checklist.CheckList
struct  CheckList_t4132048078  : public RuntimeObject
{
public:
	// System.Int64 Library.Code.Domain.Entities.Checklist.CheckList::id
	int64_t ___id_0;
	// System.String Library.Code.Domain.Entities.Checklist.CheckList::name
	String_t* ___name_1;
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Checklist.ItemStatus> Library.Code.Domain.Entities.Checklist.CheckList::checklistItems
	List_1_t747872829 * ___checklistItems_2;
	// Library.Code.Domain.Entities.Checklist.ElementStatus Library.Code.Domain.Entities.Checklist.CheckList::elementStatus
	ElementStatus_t2221316768 * ___elementStatus_3;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(CheckList_t4132048078, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(CheckList_t4132048078, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_checklistItems_2() { return static_cast<int32_t>(offsetof(CheckList_t4132048078, ___checklistItems_2)); }
	inline List_1_t747872829 * get_checklistItems_2() const { return ___checklistItems_2; }
	inline List_1_t747872829 ** get_address_of_checklistItems_2() { return &___checklistItems_2; }
	inline void set_checklistItems_2(List_1_t747872829 * value)
	{
		___checklistItems_2 = value;
		Il2CppCodeGenWriteBarrier((&___checklistItems_2), value);
	}

	inline static int32_t get_offset_of_elementStatus_3() { return static_cast<int32_t>(offsetof(CheckList_t4132048078, ___elementStatus_3)); }
	inline ElementStatus_t2221316768 * get_elementStatus_3() const { return ___elementStatus_3; }
	inline ElementStatus_t2221316768 ** get_address_of_elementStatus_3() { return &___elementStatus_3; }
	inline void set_elementStatus_3(ElementStatus_t2221316768 * value)
	{
		___elementStatus_3 = value;
		Il2CppCodeGenWriteBarrier((&___elementStatus_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKLIST_T4132048078_H
#ifndef CHECKLISTRESPONSE_T3656740397_H
#define CHECKLISTRESPONSE_T3656740397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.Checklist.CheckListResponse
struct  CheckListResponse_t3656740397  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Checklist.CheckList> Library.Code.Domain.Entities.Checklist.CheckListResponse::checklists
	List_1_t3501169210 * ___checklists_0;

public:
	inline static int32_t get_offset_of_checklists_0() { return static_cast<int32_t>(offsetof(CheckListResponse_t3656740397, ___checklists_0)); }
	inline List_1_t3501169210 * get_checklists_0() const { return ___checklists_0; }
	inline List_1_t3501169210 ** get_address_of_checklists_0() { return &___checklists_0; }
	inline void set_checklists_0(List_1_t3501169210 * value)
	{
		___checklists_0 = value;
		Il2CppCodeGenWriteBarrier((&___checklists_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKLISTRESPONSE_T3656740397_H
#ifndef ELEMENTSTATUS_T2221316768_H
#define ELEMENTSTATUS_T2221316768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.Checklist.ElementStatus
struct  ElementStatus_t2221316768  : public RuntimeObject
{
public:
	// System.Int64 Library.Code.Domain.Entities.Checklist.ElementStatus::id
	int64_t ___id_0;
	// System.String Library.Code.Domain.Entities.Checklist.ElementStatus::name
	String_t* ___name_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ElementStatus_t2221316768, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(ElementStatus_t2221316768, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTSTATUS_T2221316768_H
#ifndef ITEMSTATUS_T1378751697_H
#define ITEMSTATUS_T1378751697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.Checklist.ItemStatus
struct  ItemStatus_t1378751697  : public RuntimeObject
{
public:
	// System.Int64 Library.Code.Domain.Entities.Checklist.ItemStatus::id
	int64_t ___id_0;
	// System.String Library.Code.Domain.Entities.Checklist.ItemStatus::content
	String_t* ___content_1;
	// System.Int32 Library.Code.Domain.Entities.Checklist.ItemStatus::order
	int32_t ___order_2;
	// System.String Library.Code.Domain.Entities.Checklist.ItemStatus::status
	String_t* ___status_3;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ItemStatus_t1378751697, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(ItemStatus_t1378751697, ___content_1)); }
	inline String_t* get_content_1() const { return ___content_1; }
	inline String_t** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(String_t* value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_1), value);
	}

	inline static int32_t get_offset_of_order_2() { return static_cast<int32_t>(offsetof(ItemStatus_t1378751697, ___order_2)); }
	inline int32_t get_order_2() const { return ___order_2; }
	inline int32_t* get_address_of_order_2() { return &___order_2; }
	inline void set_order_2(int32_t value)
	{
		___order_2 = value;
	}

	inline static int32_t get_offset_of_status_3() { return static_cast<int32_t>(offsetof(ItemStatus_t1378751697, ___status_3)); }
	inline String_t* get_status_3() const { return ___status_3; }
	inline String_t** get_address_of_status_3() { return &___status_3; }
	inline void set_status_3(String_t* value)
	{
		___status_3 = value;
		Il2CppCodeGenWriteBarrier((&___status_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMSTATUS_T1378751697_H
#ifndef CHECKLISTFACADEIMP_T2146505562_H
#define CHECKLISTFACADEIMP_T2146505562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.CheckListF.CheckListFacadeImp
struct  CheckListFacadeImp_t2146505562  : public RuntimeObject
{
public:
	// Library.Code.Utils.Async.DoAsync Library.Code.Facades.CheckListF.CheckListFacadeImp::_async
	RuntimeObject* ____async_0;
	// Library.Code.Networking.Checklist.LoadChecklistForElementApi Library.Code.Facades.CheckListF.CheckListFacadeImp::_checklistForElementApi
	RuntimeObject* ____checklistForElementApi_1;
	// Library.Code.Networking.Checklist.ChangeChecklistItemStatusApi Library.Code.Facades.CheckListF.CheckListFacadeImp::_changeChecklistItemStatusApi
	RuntimeObject* ____changeChecklistItemStatusApi_2;
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Checklist.CheckList> Library.Code.Facades.CheckListF.CheckListFacadeImp::_checkLists
	List_1_t3501169210 * ____checkLists_3;
	// System.Collections.Generic.List`1<Library.Code.Facades.ObserverSingle`1<Library.Code.Domain.Entities.Checklist.CheckList>> Library.Code.Facades.CheckListF.CheckListFacadeImp::_observers
	List_1_t859744778 * ____observers_4;
	// Library.Code.Domain.Entities.Element Library.Code.Facades.CheckListF.CheckListFacadeImp::currentElement
	Element_t2276588008 * ___currentElement_5;

public:
	inline static int32_t get_offset_of__async_0() { return static_cast<int32_t>(offsetof(CheckListFacadeImp_t2146505562, ____async_0)); }
	inline RuntimeObject* get__async_0() const { return ____async_0; }
	inline RuntimeObject** get_address_of__async_0() { return &____async_0; }
	inline void set__async_0(RuntimeObject* value)
	{
		____async_0 = value;
		Il2CppCodeGenWriteBarrier((&____async_0), value);
	}

	inline static int32_t get_offset_of__checklistForElementApi_1() { return static_cast<int32_t>(offsetof(CheckListFacadeImp_t2146505562, ____checklistForElementApi_1)); }
	inline RuntimeObject* get__checklistForElementApi_1() const { return ____checklistForElementApi_1; }
	inline RuntimeObject** get_address_of__checklistForElementApi_1() { return &____checklistForElementApi_1; }
	inline void set__checklistForElementApi_1(RuntimeObject* value)
	{
		____checklistForElementApi_1 = value;
		Il2CppCodeGenWriteBarrier((&____checklistForElementApi_1), value);
	}

	inline static int32_t get_offset_of__changeChecklistItemStatusApi_2() { return static_cast<int32_t>(offsetof(CheckListFacadeImp_t2146505562, ____changeChecklistItemStatusApi_2)); }
	inline RuntimeObject* get__changeChecklistItemStatusApi_2() const { return ____changeChecklistItemStatusApi_2; }
	inline RuntimeObject** get_address_of__changeChecklistItemStatusApi_2() { return &____changeChecklistItemStatusApi_2; }
	inline void set__changeChecklistItemStatusApi_2(RuntimeObject* value)
	{
		____changeChecklistItemStatusApi_2 = value;
		Il2CppCodeGenWriteBarrier((&____changeChecklistItemStatusApi_2), value);
	}

	inline static int32_t get_offset_of__checkLists_3() { return static_cast<int32_t>(offsetof(CheckListFacadeImp_t2146505562, ____checkLists_3)); }
	inline List_1_t3501169210 * get__checkLists_3() const { return ____checkLists_3; }
	inline List_1_t3501169210 ** get_address_of__checkLists_3() { return &____checkLists_3; }
	inline void set__checkLists_3(List_1_t3501169210 * value)
	{
		____checkLists_3 = value;
		Il2CppCodeGenWriteBarrier((&____checkLists_3), value);
	}

	inline static int32_t get_offset_of__observers_4() { return static_cast<int32_t>(offsetof(CheckListFacadeImp_t2146505562, ____observers_4)); }
	inline List_1_t859744778 * get__observers_4() const { return ____observers_4; }
	inline List_1_t859744778 ** get_address_of__observers_4() { return &____observers_4; }
	inline void set__observers_4(List_1_t859744778 * value)
	{
		____observers_4 = value;
		Il2CppCodeGenWriteBarrier((&____observers_4), value);
	}

	inline static int32_t get_offset_of_currentElement_5() { return static_cast<int32_t>(offsetof(CheckListFacadeImp_t2146505562, ___currentElement_5)); }
	inline Element_t2276588008 * get_currentElement_5() const { return ___currentElement_5; }
	inline Element_t2276588008 ** get_address_of_currentElement_5() { return &___currentElement_5; }
	inline void set_currentElement_5(Element_t2276588008 * value)
	{
		___currentElement_5 = value;
		Il2CppCodeGenWriteBarrier((&___currentElement_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKLISTFACADEIMP_T2146505562_H
#ifndef ITEMSTATUSDTO_T3640770036_H
#define ITEMSTATUSDTO_T3640770036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.Checklist.ItemStatusDto
struct  ItemStatusDto_t3640770036  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Checklist.ItemStatus Library.Code.Domain.Entities.Checklist.ItemStatusDto::itemStatus
	ItemStatus_t1378751697 * ___itemStatus_0;
	// System.Boolean Library.Code.Domain.Entities.Checklist.ItemStatusDto::isCurrent
	bool ___isCurrent_1;

public:
	inline static int32_t get_offset_of_itemStatus_0() { return static_cast<int32_t>(offsetof(ItemStatusDto_t3640770036, ___itemStatus_0)); }
	inline ItemStatus_t1378751697 * get_itemStatus_0() const { return ___itemStatus_0; }
	inline ItemStatus_t1378751697 ** get_address_of_itemStatus_0() { return &___itemStatus_0; }
	inline void set_itemStatus_0(ItemStatus_t1378751697 * value)
	{
		___itemStatus_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStatus_0), value);
	}

	inline static int32_t get_offset_of_isCurrent_1() { return static_cast<int32_t>(offsetof(ItemStatusDto_t3640770036, ___isCurrent_1)); }
	inline bool get_isCurrent_1() const { return ___isCurrent_1; }
	inline bool* get_address_of_isCurrent_1() { return &___isCurrent_1; }
	inline void set_isCurrent_1(bool value)
	{
		___isCurrent_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMSTATUSDTO_T3640770036_H
#ifndef ELEMENT_T2276588008_H
#define ELEMENT_T2276588008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.Element
struct  Element_t2276588008  : public RuntimeObject
{
public:
	// System.Int64 Library.Code.Domain.Entities.Element::id
	int64_t ___id_0;
	// System.String Library.Code.Domain.Entities.Element::name
	String_t* ___name_1;
	// System.Int64 Library.Code.Domain.Entities.Element::designerId
	int64_t ___designerId_2;
	// System.Boolean Library.Code.Domain.Entities.Element::completed
	bool ___completed_3;
	// System.String Library.Code.Domain.Entities.Element::createdAt
	String_t* ___createdAt_4;
	// System.Int64 Library.Code.Domain.Entities.Element::modelId
	int64_t ___modelId_5;
	// System.String Library.Code.Domain.Entities.Element::currentFileUrl
	String_t* ___currentFileUrl_6;
	// System.String Library.Code.Domain.Entities.Element::currentFileAndroid
	String_t* ___currentFileAndroid_7;
	// System.String Library.Code.Domain.Entities.Element::currentFileIOS
	String_t* ___currentFileIOS_8;
	// System.String Library.Code.Domain.Entities.Element::currentFileHololens
	String_t* ___currentFileHololens_9;
	// System.String Library.Code.Domain.Entities.Element::status
	String_t* ___status_10;
	// System.String Library.Code.Domain.Entities.Element::oldStatus
	String_t* ___oldStatus_11;
	// System.String Library.Code.Domain.Entities.Element::qrPath
	String_t* ___qrPath_12;
	// System.Int64 Library.Code.Domain.Entities.Element::statusId
	int64_t ___statusId_13;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(Element_t2276588008, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(Element_t2276588008, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_designerId_2() { return static_cast<int32_t>(offsetof(Element_t2276588008, ___designerId_2)); }
	inline int64_t get_designerId_2() const { return ___designerId_2; }
	inline int64_t* get_address_of_designerId_2() { return &___designerId_2; }
	inline void set_designerId_2(int64_t value)
	{
		___designerId_2 = value;
	}

	inline static int32_t get_offset_of_completed_3() { return static_cast<int32_t>(offsetof(Element_t2276588008, ___completed_3)); }
	inline bool get_completed_3() const { return ___completed_3; }
	inline bool* get_address_of_completed_3() { return &___completed_3; }
	inline void set_completed_3(bool value)
	{
		___completed_3 = value;
	}

	inline static int32_t get_offset_of_createdAt_4() { return static_cast<int32_t>(offsetof(Element_t2276588008, ___createdAt_4)); }
	inline String_t* get_createdAt_4() const { return ___createdAt_4; }
	inline String_t** get_address_of_createdAt_4() { return &___createdAt_4; }
	inline void set_createdAt_4(String_t* value)
	{
		___createdAt_4 = value;
		Il2CppCodeGenWriteBarrier((&___createdAt_4), value);
	}

	inline static int32_t get_offset_of_modelId_5() { return static_cast<int32_t>(offsetof(Element_t2276588008, ___modelId_5)); }
	inline int64_t get_modelId_5() const { return ___modelId_5; }
	inline int64_t* get_address_of_modelId_5() { return &___modelId_5; }
	inline void set_modelId_5(int64_t value)
	{
		___modelId_5 = value;
	}

	inline static int32_t get_offset_of_currentFileUrl_6() { return static_cast<int32_t>(offsetof(Element_t2276588008, ___currentFileUrl_6)); }
	inline String_t* get_currentFileUrl_6() const { return ___currentFileUrl_6; }
	inline String_t** get_address_of_currentFileUrl_6() { return &___currentFileUrl_6; }
	inline void set_currentFileUrl_6(String_t* value)
	{
		___currentFileUrl_6 = value;
		Il2CppCodeGenWriteBarrier((&___currentFileUrl_6), value);
	}

	inline static int32_t get_offset_of_currentFileAndroid_7() { return static_cast<int32_t>(offsetof(Element_t2276588008, ___currentFileAndroid_7)); }
	inline String_t* get_currentFileAndroid_7() const { return ___currentFileAndroid_7; }
	inline String_t** get_address_of_currentFileAndroid_7() { return &___currentFileAndroid_7; }
	inline void set_currentFileAndroid_7(String_t* value)
	{
		___currentFileAndroid_7 = value;
		Il2CppCodeGenWriteBarrier((&___currentFileAndroid_7), value);
	}

	inline static int32_t get_offset_of_currentFileIOS_8() { return static_cast<int32_t>(offsetof(Element_t2276588008, ___currentFileIOS_8)); }
	inline String_t* get_currentFileIOS_8() const { return ___currentFileIOS_8; }
	inline String_t** get_address_of_currentFileIOS_8() { return &___currentFileIOS_8; }
	inline void set_currentFileIOS_8(String_t* value)
	{
		___currentFileIOS_8 = value;
		Il2CppCodeGenWriteBarrier((&___currentFileIOS_8), value);
	}

	inline static int32_t get_offset_of_currentFileHololens_9() { return static_cast<int32_t>(offsetof(Element_t2276588008, ___currentFileHololens_9)); }
	inline String_t* get_currentFileHololens_9() const { return ___currentFileHololens_9; }
	inline String_t** get_address_of_currentFileHololens_9() { return &___currentFileHololens_9; }
	inline void set_currentFileHololens_9(String_t* value)
	{
		___currentFileHololens_9 = value;
		Il2CppCodeGenWriteBarrier((&___currentFileHololens_9), value);
	}

	inline static int32_t get_offset_of_status_10() { return static_cast<int32_t>(offsetof(Element_t2276588008, ___status_10)); }
	inline String_t* get_status_10() const { return ___status_10; }
	inline String_t** get_address_of_status_10() { return &___status_10; }
	inline void set_status_10(String_t* value)
	{
		___status_10 = value;
		Il2CppCodeGenWriteBarrier((&___status_10), value);
	}

	inline static int32_t get_offset_of_oldStatus_11() { return static_cast<int32_t>(offsetof(Element_t2276588008, ___oldStatus_11)); }
	inline String_t* get_oldStatus_11() const { return ___oldStatus_11; }
	inline String_t** get_address_of_oldStatus_11() { return &___oldStatus_11; }
	inline void set_oldStatus_11(String_t* value)
	{
		___oldStatus_11 = value;
		Il2CppCodeGenWriteBarrier((&___oldStatus_11), value);
	}

	inline static int32_t get_offset_of_qrPath_12() { return static_cast<int32_t>(offsetof(Element_t2276588008, ___qrPath_12)); }
	inline String_t* get_qrPath_12() const { return ___qrPath_12; }
	inline String_t** get_address_of_qrPath_12() { return &___qrPath_12; }
	inline void set_qrPath_12(String_t* value)
	{
		___qrPath_12 = value;
		Il2CppCodeGenWriteBarrier((&___qrPath_12), value);
	}

	inline static int32_t get_offset_of_statusId_13() { return static_cast<int32_t>(offsetof(Element_t2276588008, ___statusId_13)); }
	inline int64_t get_statusId_13() const { return ___statusId_13; }
	inline int64_t* get_address_of_statusId_13() { return &___statusId_13; }
	inline void set_statusId_13(int64_t value)
	{
		___statusId_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT_T2276588008_H
#ifndef ELEMENTLIST_T1752951218_H
#define ELEMENTLIST_T1752951218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.ElementList
struct  ElementList_t1752951218  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Element> Library.Code.Domain.Entities.ElementList::elementsList
	List_1_t1645709140 * ___elementsList_0;

public:
	inline static int32_t get_offset_of_elementsList_0() { return static_cast<int32_t>(offsetof(ElementList_t1752951218, ___elementsList_0)); }
	inline List_1_t1645709140 * get_elementsList_0() const { return ___elementsList_0; }
	inline List_1_t1645709140 ** get_address_of_elementsList_0() { return &___elementsList_0; }
	inline void set_elementsList_0(List_1_t1645709140 * value)
	{
		___elementsList_0 = value;
		Il2CppCodeGenWriteBarrier((&___elementsList_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTLIST_T1752951218_H
#ifndef GROUPCREATE1_T1731656564_H
#define GROUPCREATE1_T1731656564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.GroupCreate1
struct  GroupCreate1_t1731656564  : public RuntimeObject
{
public:
	// System.String Library.Code.Domain.Entities.GroupCreate1::element
	String_t* ___element_0;
	// System.String Library.Code.Domain.Entities.GroupCreate1::name
	String_t* ___name_1;

public:
	inline static int32_t get_offset_of_element_0() { return static_cast<int32_t>(offsetof(GroupCreate1_t1731656564, ___element_0)); }
	inline String_t* get_element_0() const { return ___element_0; }
	inline String_t** get_address_of_element_0() { return &___element_0; }
	inline void set_element_0(String_t* value)
	{
		___element_0 = value;
		Il2CppCodeGenWriteBarrier((&___element_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(GroupCreate1_t1731656564, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPCREATE1_T1731656564_H
#ifndef MODEL_T2163629551_H
#define MODEL_T2163629551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.Model
struct  Model_t2163629551  : public RuntimeObject
{
public:
	// System.Int64 Library.Code.Domain.Entities.Model::id
	int64_t ___id_0;
	// System.String Library.Code.Domain.Entities.Model::name
	String_t* ___name_1;
	// System.Int64 Library.Code.Domain.Entities.Model::companyId
	int64_t ___companyId_2;
	// System.String Library.Code.Domain.Entities.Model::createdAt
	String_t* ___createdAt_3;
	// System.String Library.Code.Domain.Entities.Model::dueDate
	String_t* ___dueDate_4;
	// System.String Library.Code.Domain.Entities.Model::imageUrl
	String_t* ___imageUrl_5;
	// System.String Library.Code.Domain.Entities.Model::client
	String_t* ___client_6;
	// System.String Library.Code.Domain.Entities.Model::description
	String_t* ___description_7;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(Model_t2163629551, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(Model_t2163629551, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_companyId_2() { return static_cast<int32_t>(offsetof(Model_t2163629551, ___companyId_2)); }
	inline int64_t get_companyId_2() const { return ___companyId_2; }
	inline int64_t* get_address_of_companyId_2() { return &___companyId_2; }
	inline void set_companyId_2(int64_t value)
	{
		___companyId_2 = value;
	}

	inline static int32_t get_offset_of_createdAt_3() { return static_cast<int32_t>(offsetof(Model_t2163629551, ___createdAt_3)); }
	inline String_t* get_createdAt_3() const { return ___createdAt_3; }
	inline String_t** get_address_of_createdAt_3() { return &___createdAt_3; }
	inline void set_createdAt_3(String_t* value)
	{
		___createdAt_3 = value;
		Il2CppCodeGenWriteBarrier((&___createdAt_3), value);
	}

	inline static int32_t get_offset_of_dueDate_4() { return static_cast<int32_t>(offsetof(Model_t2163629551, ___dueDate_4)); }
	inline String_t* get_dueDate_4() const { return ___dueDate_4; }
	inline String_t** get_address_of_dueDate_4() { return &___dueDate_4; }
	inline void set_dueDate_4(String_t* value)
	{
		___dueDate_4 = value;
		Il2CppCodeGenWriteBarrier((&___dueDate_4), value);
	}

	inline static int32_t get_offset_of_imageUrl_5() { return static_cast<int32_t>(offsetof(Model_t2163629551, ___imageUrl_5)); }
	inline String_t* get_imageUrl_5() const { return ___imageUrl_5; }
	inline String_t** get_address_of_imageUrl_5() { return &___imageUrl_5; }
	inline void set_imageUrl_5(String_t* value)
	{
		___imageUrl_5 = value;
		Il2CppCodeGenWriteBarrier((&___imageUrl_5), value);
	}

	inline static int32_t get_offset_of_client_6() { return static_cast<int32_t>(offsetof(Model_t2163629551, ___client_6)); }
	inline String_t* get_client_6() const { return ___client_6; }
	inline String_t** get_address_of_client_6() { return &___client_6; }
	inline void set_client_6(String_t* value)
	{
		___client_6 = value;
		Il2CppCodeGenWriteBarrier((&___client_6), value);
	}

	inline static int32_t get_offset_of_description_7() { return static_cast<int32_t>(offsetof(Model_t2163629551, ___description_7)); }
	inline String_t* get_description_7() const { return ___description_7; }
	inline String_t** get_address_of_description_7() { return &___description_7; }
	inline void set_description_7(String_t* value)
	{
		___description_7 = value;
		Il2CppCodeGenWriteBarrier((&___description_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODEL_T2163629551_H
#ifndef MODELEMBEDDED_T574489313_H
#define MODELEMBEDDED_T574489313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.ModelEmbedded
struct  ModelEmbedded_t574489313  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.DefaultModelEmbedded Library.Code.Domain.Entities.ModelEmbedded::_embedded
	DefaultModelEmbedded_t2860339312 * ____embedded_0;

public:
	inline static int32_t get_offset_of__embedded_0() { return static_cast<int32_t>(offsetof(ModelEmbedded_t574489313, ____embedded_0)); }
	inline DefaultModelEmbedded_t2860339312 * get__embedded_0() const { return ____embedded_0; }
	inline DefaultModelEmbedded_t2860339312 ** get_address_of__embedded_0() { return &____embedded_0; }
	inline void set__embedded_0(DefaultModelEmbedded_t2860339312 * value)
	{
		____embedded_0 = value;
		Il2CppCodeGenWriteBarrier((&____embedded_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELEMBEDDED_T574489313_H
#ifndef DEFAULTMODELEMBEDDED_T2860339312_H
#define DEFAULTMODELEMBEDDED_T2860339312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.DefaultModelEmbedded
struct  DefaultModelEmbedded_t2860339312  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model> Library.Code.Domain.Entities.DefaultModelEmbedded::models
	List_1_t1532750683 * ___models_0;

public:
	inline static int32_t get_offset_of_models_0() { return static_cast<int32_t>(offsetof(DefaultModelEmbedded_t2860339312, ___models_0)); }
	inline List_1_t1532750683 * get_models_0() const { return ___models_0; }
	inline List_1_t1532750683 ** get_address_of_models_0() { return &___models_0; }
	inline void set_models_0(List_1_t1532750683 * value)
	{
		___models_0 = value;
		Il2CppCodeGenWriteBarrier((&___models_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTMODELEMBEDDED_T2860339312_H
#ifndef COLORPARTS_T499719487_H
#define COLORPARTS_T499719487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.PrefixColor.ColorParts
struct  ColorParts_t499719487  : public RuntimeObject
{
public:
	// System.Int64 Library.Code.Domain.Entities.PrefixColor.ColorParts::id
	int64_t ___id_0;
	// System.String Library.Code.Domain.Entities.PrefixColor.ColorParts::prefix
	String_t* ___prefix_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ColorParts_t499719487, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_prefix_1() { return static_cast<int32_t>(offsetof(ColorParts_t499719487, ___prefix_1)); }
	inline String_t* get_prefix_1() const { return ___prefix_1; }
	inline String_t** get_address_of_prefix_1() { return &___prefix_1; }
	inline void set_prefix_1(String_t* value)
	{
		___prefix_1 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPARTS_T499719487_H
#ifndef COLORPREFIXESNAMEEQUALITYCOMPARER_T3918632335_H
#define COLORPREFIXESNAMEEQUALITYCOMPARER_T3918632335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.ModelGroup/ColorPrefixesNameEqualityComparer
struct  ColorPrefixesNameEqualityComparer_t3918632335  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPREFIXESNAMEEQUALITYCOMPARER_T3918632335_H
#ifndef MODELSLIST_T4161403536_H
#define MODELSLIST_T4161403536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.ModelsList
struct  ModelsList_t4161403536  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model> Library.Code.Domain.Entities.ModelsList::modelsList
	List_1_t1532750683 * ___modelsList_0;

public:
	inline static int32_t get_offset_of_modelsList_0() { return static_cast<int32_t>(offsetof(ModelsList_t4161403536, ___modelsList_0)); }
	inline List_1_t1532750683 * get_modelsList_0() const { return ___modelsList_0; }
	inline List_1_t1532750683 ** get_address_of_modelsList_0() { return &___modelsList_0; }
	inline void set_modelsList_0(List_1_t1532750683 * value)
	{
		___modelsList_0 = value;
		Il2CppCodeGenWriteBarrier((&___modelsList_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELSLIST_T4161403536_H
#ifndef PREFIXCOLORRESPONSE_T106821790_H
#define PREFIXCOLORRESPONSE_T106821790_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.PrefixColor.PrefixColorResponse
struct  PrefixColorResponse_t106821790  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.PrefixColor.PrefixColor> Library.Code.Domain.Entities.PrefixColor.PrefixColorResponse::list
	List_1_t949058795 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(PrefixColorResponse_t106821790, ___list_0)); }
	inline List_1_t949058795 * get_list_0() const { return ___list_0; }
	inline List_1_t949058795 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t949058795 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFIXCOLORRESPONSE_T106821790_H
#ifndef DEFAULTMESSAGEEMBADDED_T2732967408_H
#define DEFAULTMESSAGEEMBADDED_T2732967408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.DefaultMessageEmbadded
struct  DefaultMessageEmbadded_t2732967408  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.MessageTemplate> Library.Code.Domain.Entities.DefaultMessageEmbadded::defaultMessages
	List_1_t2162674683 * ___defaultMessages_0;

public:
	inline static int32_t get_offset_of_defaultMessages_0() { return static_cast<int32_t>(offsetof(DefaultMessageEmbadded_t2732967408, ___defaultMessages_0)); }
	inline List_1_t2162674683 * get_defaultMessages_0() const { return ___defaultMessages_0; }
	inline List_1_t2162674683 ** get_address_of_defaultMessages_0() { return &___defaultMessages_0; }
	inline void set_defaultMessages_0(List_1_t2162674683 * value)
	{
		___defaultMessages_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultMessages_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTMESSAGEEMBADDED_T2732967408_H
#ifndef GROUPCREATE2_T1731656567_H
#define GROUPCREATE2_T1731656567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.GroupCreate2
struct  GroupCreate2_t1731656567  : public RuntimeObject
{
public:
	// System.Int64 Library.Code.Domain.Entities.GroupCreate2::element
	int64_t ___element_0;
	// System.String Library.Code.Domain.Entities.GroupCreate2::name
	String_t* ___name_1;

public:
	inline static int32_t get_offset_of_element_0() { return static_cast<int32_t>(offsetof(GroupCreate2_t1731656567, ___element_0)); }
	inline int64_t get_element_0() const { return ___element_0; }
	inline int64_t* get_address_of_element_0() { return &___element_0; }
	inline void set_element_0(int64_t value)
	{
		___element_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(GroupCreate2_t1731656567, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPCREATE2_T1731656567_H
#ifndef HOLOFILE_T1957467228_H
#define HOLOFILE_T1957467228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.HoloFile
struct  HoloFile_t1957467228  : public RuntimeObject
{
public:
	// System.String Library.Code.Domain.Entities.HoloFile::element
	String_t* ___element_0;
	// System.String Library.Code.Domain.Entities.HoloFile::path
	String_t* ___path_1;
	// System.String Library.Code.Domain.Entities.HoloFile::type
	String_t* ___type_2;
	// System.String Library.Code.Domain.Entities.HoloFile::conversionStatus
	String_t* ___conversionStatus_3;
	// System.String Library.Code.Domain.Entities.HoloFile::conversionMsg
	String_t* ___conversionMsg_4;

public:
	inline static int32_t get_offset_of_element_0() { return static_cast<int32_t>(offsetof(HoloFile_t1957467228, ___element_0)); }
	inline String_t* get_element_0() const { return ___element_0; }
	inline String_t** get_address_of_element_0() { return &___element_0; }
	inline void set_element_0(String_t* value)
	{
		___element_0 = value;
		Il2CppCodeGenWriteBarrier((&___element_0), value);
	}

	inline static int32_t get_offset_of_path_1() { return static_cast<int32_t>(offsetof(HoloFile_t1957467228, ___path_1)); }
	inline String_t* get_path_1() const { return ___path_1; }
	inline String_t** get_address_of_path_1() { return &___path_1; }
	inline void set_path_1(String_t* value)
	{
		___path_1 = value;
		Il2CppCodeGenWriteBarrier((&___path_1), value);
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(HoloFile_t1957467228, ___type_2)); }
	inline String_t* get_type_2() const { return ___type_2; }
	inline String_t** get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(String_t* value)
	{
		___type_2 = value;
		Il2CppCodeGenWriteBarrier((&___type_2), value);
	}

	inline static int32_t get_offset_of_conversionStatus_3() { return static_cast<int32_t>(offsetof(HoloFile_t1957467228, ___conversionStatus_3)); }
	inline String_t* get_conversionStatus_3() const { return ___conversionStatus_3; }
	inline String_t** get_address_of_conversionStatus_3() { return &___conversionStatus_3; }
	inline void set_conversionStatus_3(String_t* value)
	{
		___conversionStatus_3 = value;
		Il2CppCodeGenWriteBarrier((&___conversionStatus_3), value);
	}

	inline static int32_t get_offset_of_conversionMsg_4() { return static_cast<int32_t>(offsetof(HoloFile_t1957467228, ___conversionMsg_4)); }
	inline String_t* get_conversionMsg_4() const { return ___conversionMsg_4; }
	inline String_t** get_address_of_conversionMsg_4() { return &___conversionMsg_4; }
	inline void set_conversionMsg_4(String_t* value)
	{
		___conversionMsg_4 = value;
		Il2CppCodeGenWriteBarrier((&___conversionMsg_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLOFILE_T1957467228_H
#ifndef QRCODE_T296287656_H
#define QRCODE_T296287656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.QRCode
struct  QRCode_t296287656  : public RuntimeObject
{
public:
	// System.String Library.Code.Domain.Entities.QRCode::scannedString
	String_t* ___scannedString_0;

public:
	inline static int32_t get_offset_of_scannedString_0() { return static_cast<int32_t>(offsetof(QRCode_t296287656, ___scannedString_0)); }
	inline String_t* get_scannedString_0() const { return ___scannedString_0; }
	inline String_t** get_address_of_scannedString_0() { return &___scannedString_0; }
	inline void set_scannedString_0(String_t* value)
	{
		___scannedString_0 = value;
		Il2CppCodeGenWriteBarrier((&___scannedString_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QRCODE_T296287656_H
#ifndef HOLOFILETYPECONVERTER_T2088768576_H
#define HOLOFILETYPECONVERTER_T2088768576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.HoloFileTypeConverter
struct  HoloFileTypeConverter_t2088768576  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLOFILETYPECONVERTER_T2088768576_H
#ifndef MESSAGE_T1214853957_H
#define MESSAGE_T1214853957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.Message
struct  Message_t1214853957  : public RuntimeObject
{
public:
	// System.String Library.Code.Domain.Entities.Message::element
	String_t* ___element_0;
	// System.Int64 Library.Code.Domain.Entities.Message::elementId
	int64_t ___elementId_1;
	// System.String Library.Code.Domain.Entities.Message::content
	String_t* ___content_2;
	// System.Boolean Library.Code.Domain.Entities.Message::error
	bool ___error_3;
	// System.String Library.Code.Domain.Entities.Message::checkpoint
	String_t* ___checkpoint_4;
	// System.Int64 Library.Code.Domain.Entities.Message::checklistItemId
	int64_t ___checklistItemId_5;
	// System.Single Library.Code.Domain.Entities.Message::cameraXPos
	float ___cameraXPos_6;
	// System.Single Library.Code.Domain.Entities.Message::cameraYPos
	float ___cameraYPos_7;
	// System.Single Library.Code.Domain.Entities.Message::cameraZPos
	float ___cameraZPos_8;
	// System.Single Library.Code.Domain.Entities.Message::cameraXRot
	float ___cameraXRot_9;
	// System.Single Library.Code.Domain.Entities.Message::cameraYRot
	float ___cameraYRot_10;
	// System.Single Library.Code.Domain.Entities.Message::cameraZRot
	float ___cameraZRot_11;
	// System.Single Library.Code.Domain.Entities.Message::modelXPos
	float ___modelXPos_12;
	// System.Single Library.Code.Domain.Entities.Message::modelYPos
	float ___modelYPos_13;
	// System.Single Library.Code.Domain.Entities.Message::modelZPos
	float ___modelZPos_14;
	// System.Single Library.Code.Domain.Entities.Message::modelXRot
	float ___modelXRot_15;
	// System.Single Library.Code.Domain.Entities.Message::modelYRot
	float ___modelYRot_16;
	// System.Single Library.Code.Domain.Entities.Message::modelZRot
	float ___modelZRot_17;
	// System.Int64 Library.Code.Domain.Entities.Message::groupId
	int64_t ___groupId_18;
	// System.Int64 Library.Code.Domain.Entities.Message::messageId
	int64_t ___messageId_19;
	// System.String Library.Code.Domain.Entities.Message::snapshotPath
	String_t* ___snapshotPath_20;
	// System.Collections.Generic.List`1<System.String> Library.Code.Domain.Entities.Message::paths
	List_1_t1398341365 * ___paths_21;
	// System.String Library.Code.Domain.Entities.Message::audioPath
	String_t* ___audioPath_22;

public:
	inline static int32_t get_offset_of_element_0() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___element_0)); }
	inline String_t* get_element_0() const { return ___element_0; }
	inline String_t** get_address_of_element_0() { return &___element_0; }
	inline void set_element_0(String_t* value)
	{
		___element_0 = value;
		Il2CppCodeGenWriteBarrier((&___element_0), value);
	}

	inline static int32_t get_offset_of_elementId_1() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___elementId_1)); }
	inline int64_t get_elementId_1() const { return ___elementId_1; }
	inline int64_t* get_address_of_elementId_1() { return &___elementId_1; }
	inline void set_elementId_1(int64_t value)
	{
		___elementId_1 = value;
	}

	inline static int32_t get_offset_of_content_2() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___content_2)); }
	inline String_t* get_content_2() const { return ___content_2; }
	inline String_t** get_address_of_content_2() { return &___content_2; }
	inline void set_content_2(String_t* value)
	{
		___content_2 = value;
		Il2CppCodeGenWriteBarrier((&___content_2), value);
	}

	inline static int32_t get_offset_of_error_3() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___error_3)); }
	inline bool get_error_3() const { return ___error_3; }
	inline bool* get_address_of_error_3() { return &___error_3; }
	inline void set_error_3(bool value)
	{
		___error_3 = value;
	}

	inline static int32_t get_offset_of_checkpoint_4() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___checkpoint_4)); }
	inline String_t* get_checkpoint_4() const { return ___checkpoint_4; }
	inline String_t** get_address_of_checkpoint_4() { return &___checkpoint_4; }
	inline void set_checkpoint_4(String_t* value)
	{
		___checkpoint_4 = value;
		Il2CppCodeGenWriteBarrier((&___checkpoint_4), value);
	}

	inline static int32_t get_offset_of_checklistItemId_5() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___checklistItemId_5)); }
	inline int64_t get_checklistItemId_5() const { return ___checklistItemId_5; }
	inline int64_t* get_address_of_checklistItemId_5() { return &___checklistItemId_5; }
	inline void set_checklistItemId_5(int64_t value)
	{
		___checklistItemId_5 = value;
	}

	inline static int32_t get_offset_of_cameraXPos_6() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___cameraXPos_6)); }
	inline float get_cameraXPos_6() const { return ___cameraXPos_6; }
	inline float* get_address_of_cameraXPos_6() { return &___cameraXPos_6; }
	inline void set_cameraXPos_6(float value)
	{
		___cameraXPos_6 = value;
	}

	inline static int32_t get_offset_of_cameraYPos_7() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___cameraYPos_7)); }
	inline float get_cameraYPos_7() const { return ___cameraYPos_7; }
	inline float* get_address_of_cameraYPos_7() { return &___cameraYPos_7; }
	inline void set_cameraYPos_7(float value)
	{
		___cameraYPos_7 = value;
	}

	inline static int32_t get_offset_of_cameraZPos_8() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___cameraZPos_8)); }
	inline float get_cameraZPos_8() const { return ___cameraZPos_8; }
	inline float* get_address_of_cameraZPos_8() { return &___cameraZPos_8; }
	inline void set_cameraZPos_8(float value)
	{
		___cameraZPos_8 = value;
	}

	inline static int32_t get_offset_of_cameraXRot_9() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___cameraXRot_9)); }
	inline float get_cameraXRot_9() const { return ___cameraXRot_9; }
	inline float* get_address_of_cameraXRot_9() { return &___cameraXRot_9; }
	inline void set_cameraXRot_9(float value)
	{
		___cameraXRot_9 = value;
	}

	inline static int32_t get_offset_of_cameraYRot_10() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___cameraYRot_10)); }
	inline float get_cameraYRot_10() const { return ___cameraYRot_10; }
	inline float* get_address_of_cameraYRot_10() { return &___cameraYRot_10; }
	inline void set_cameraYRot_10(float value)
	{
		___cameraYRot_10 = value;
	}

	inline static int32_t get_offset_of_cameraZRot_11() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___cameraZRot_11)); }
	inline float get_cameraZRot_11() const { return ___cameraZRot_11; }
	inline float* get_address_of_cameraZRot_11() { return &___cameraZRot_11; }
	inline void set_cameraZRot_11(float value)
	{
		___cameraZRot_11 = value;
	}

	inline static int32_t get_offset_of_modelXPos_12() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___modelXPos_12)); }
	inline float get_modelXPos_12() const { return ___modelXPos_12; }
	inline float* get_address_of_modelXPos_12() { return &___modelXPos_12; }
	inline void set_modelXPos_12(float value)
	{
		___modelXPos_12 = value;
	}

	inline static int32_t get_offset_of_modelYPos_13() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___modelYPos_13)); }
	inline float get_modelYPos_13() const { return ___modelYPos_13; }
	inline float* get_address_of_modelYPos_13() { return &___modelYPos_13; }
	inline void set_modelYPos_13(float value)
	{
		___modelYPos_13 = value;
	}

	inline static int32_t get_offset_of_modelZPos_14() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___modelZPos_14)); }
	inline float get_modelZPos_14() const { return ___modelZPos_14; }
	inline float* get_address_of_modelZPos_14() { return &___modelZPos_14; }
	inline void set_modelZPos_14(float value)
	{
		___modelZPos_14 = value;
	}

	inline static int32_t get_offset_of_modelXRot_15() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___modelXRot_15)); }
	inline float get_modelXRot_15() const { return ___modelXRot_15; }
	inline float* get_address_of_modelXRot_15() { return &___modelXRot_15; }
	inline void set_modelXRot_15(float value)
	{
		___modelXRot_15 = value;
	}

	inline static int32_t get_offset_of_modelYRot_16() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___modelYRot_16)); }
	inline float get_modelYRot_16() const { return ___modelYRot_16; }
	inline float* get_address_of_modelYRot_16() { return &___modelYRot_16; }
	inline void set_modelYRot_16(float value)
	{
		___modelYRot_16 = value;
	}

	inline static int32_t get_offset_of_modelZRot_17() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___modelZRot_17)); }
	inline float get_modelZRot_17() const { return ___modelZRot_17; }
	inline float* get_address_of_modelZRot_17() { return &___modelZRot_17; }
	inline void set_modelZRot_17(float value)
	{
		___modelZRot_17 = value;
	}

	inline static int32_t get_offset_of_groupId_18() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___groupId_18)); }
	inline int64_t get_groupId_18() const { return ___groupId_18; }
	inline int64_t* get_address_of_groupId_18() { return &___groupId_18; }
	inline void set_groupId_18(int64_t value)
	{
		___groupId_18 = value;
	}

	inline static int32_t get_offset_of_messageId_19() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___messageId_19)); }
	inline int64_t get_messageId_19() const { return ___messageId_19; }
	inline int64_t* get_address_of_messageId_19() { return &___messageId_19; }
	inline void set_messageId_19(int64_t value)
	{
		___messageId_19 = value;
	}

	inline static int32_t get_offset_of_snapshotPath_20() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___snapshotPath_20)); }
	inline String_t* get_snapshotPath_20() const { return ___snapshotPath_20; }
	inline String_t** get_address_of_snapshotPath_20() { return &___snapshotPath_20; }
	inline void set_snapshotPath_20(String_t* value)
	{
		___snapshotPath_20 = value;
		Il2CppCodeGenWriteBarrier((&___snapshotPath_20), value);
	}

	inline static int32_t get_offset_of_paths_21() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___paths_21)); }
	inline List_1_t1398341365 * get_paths_21() const { return ___paths_21; }
	inline List_1_t1398341365 ** get_address_of_paths_21() { return &___paths_21; }
	inline void set_paths_21(List_1_t1398341365 * value)
	{
		___paths_21 = value;
		Il2CppCodeGenWriteBarrier((&___paths_21), value);
	}

	inline static int32_t get_offset_of_audioPath_22() { return static_cast<int32_t>(offsetof(Message_t1214853957, ___audioPath_22)); }
	inline String_t* get_audioPath_22() const { return ___audioPath_22; }
	inline String_t** get_address_of_audioPath_22() { return &___audioPath_22; }
	inline void set_audioPath_22(String_t* value)
	{
		___audioPath_22 = value;
		Il2CppCodeGenWriteBarrier((&___audioPath_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGE_T1214853957_H
#ifndef MESSAGELIST_T401268481_H
#define MESSAGELIST_T401268481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.MessageList
struct  MessageList_t401268481  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Message> Library.Code.Domain.Entities.MessageList::messagesList
	List_1_t583975089 * ___messagesList_0;

public:
	inline static int32_t get_offset_of_messagesList_0() { return static_cast<int32_t>(offsetof(MessageList_t401268481, ___messagesList_0)); }
	inline List_1_t583975089 * get_messagesList_0() const { return ___messagesList_0; }
	inline List_1_t583975089 ** get_address_of_messagesList_0() { return &___messagesList_0; }
	inline void set_messagesList_0(List_1_t583975089 * value)
	{
		___messagesList_0 = value;
		Il2CppCodeGenWriteBarrier((&___messagesList_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGELIST_T401268481_H
#ifndef MESSAGETEMPLATE_T2793553551_H
#define MESSAGETEMPLATE_T2793553551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.MessageTemplate
struct  MessageTemplate_t2793553551  : public RuntimeObject
{
public:
	// System.String Library.Code.Domain.Entities.MessageTemplate::content
	String_t* ___content_0;
	// System.Int32 Library.Code.Domain.Entities.MessageTemplate::companyId
	int32_t ___companyId_1;

public:
	inline static int32_t get_offset_of_content_0() { return static_cast<int32_t>(offsetof(MessageTemplate_t2793553551, ___content_0)); }
	inline String_t* get_content_0() const { return ___content_0; }
	inline String_t** get_address_of_content_0() { return &___content_0; }
	inline void set_content_0(String_t* value)
	{
		___content_0 = value;
		Il2CppCodeGenWriteBarrier((&___content_0), value);
	}

	inline static int32_t get_offset_of_companyId_1() { return static_cast<int32_t>(offsetof(MessageTemplate_t2793553551, ___companyId_1)); }
	inline int32_t get_companyId_1() const { return ___companyId_1; }
	inline int32_t* get_address_of_companyId_1() { return &___companyId_1; }
	inline void set_companyId_1(int32_t value)
	{
		___companyId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGETEMPLATE_T2793553551_H
#ifndef MESSAGETEMPLATEEMBEDDED_T613025749_H
#define MESSAGETEMPLATEEMBEDDED_T613025749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.MessageTemplateEmbedded
struct  MessageTemplateEmbedded_t613025749  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.DefaultMessageEmbadded Library.Code.Domain.Entities.MessageTemplateEmbedded::_embedded
	DefaultMessageEmbadded_t2732967408 * ____embedded_0;

public:
	inline static int32_t get_offset_of__embedded_0() { return static_cast<int32_t>(offsetof(MessageTemplateEmbedded_t613025749, ____embedded_0)); }
	inline DefaultMessageEmbadded_t2732967408 * get__embedded_0() const { return ____embedded_0; }
	inline DefaultMessageEmbadded_t2732967408 ** get_address_of__embedded_0() { return &____embedded_0; }
	inline void set__embedded_0(DefaultMessageEmbadded_t2732967408 * value)
	{
		____embedded_0 = value;
		Il2CppCodeGenWriteBarrier((&____embedded_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGETEMPLATEEMBEDDED_T613025749_H
#ifndef BATCHMESSAGESRESPONSES_T304281498_H
#define BATCHMESSAGESRESPONSES_T304281498_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.BatchMessagesResponses
struct  BatchMessagesResponses_t304281498  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Int64> Library.Code.Domain.Entities.BatchMessagesResponses::messageIds
	List_1_t278199169 * ___messageIds_0;

public:
	inline static int32_t get_offset_of_messageIds_0() { return static_cast<int32_t>(offsetof(BatchMessagesResponses_t304281498, ___messageIds_0)); }
	inline List_1_t278199169 * get_messageIds_0() const { return ___messageIds_0; }
	inline List_1_t278199169 ** get_address_of_messageIds_0() { return &___messageIds_0; }
	inline void set_messageIds_0(List_1_t278199169 * value)
	{
		___messageIds_0 = value;
		Il2CppCodeGenWriteBarrier((&___messageIds_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATCHMESSAGESRESPONSES_T304281498_H
#ifndef SENDMESSAGEMODULE_T690017393_H
#define SENDMESSAGEMODULE_T690017393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.Message.SendMessageModule
struct  SendMessageModule_t690017393  : public RuntimeObject
{
public:
	// Library.Code.Facades.Messages.SendMessageFacade Library.Code.DI.Message.SendMessageModule::_sendMessageFacade
	RuntimeObject* ____sendMessageFacade_0;

public:
	inline static int32_t get_offset_of__sendMessageFacade_0() { return static_cast<int32_t>(offsetof(SendMessageModule_t690017393, ____sendMessageFacade_0)); }
	inline RuntimeObject* get__sendMessageFacade_0() const { return ____sendMessageFacade_0; }
	inline RuntimeObject** get_address_of__sendMessageFacade_0() { return &____sendMessageFacade_0; }
	inline void set__sendMessageFacade_0(RuntimeObject* value)
	{
		____sendMessageFacade_0 = value;
		Il2CppCodeGenWriteBarrier((&____sendMessageFacade_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDMESSAGEMODULE_T690017393_H
#ifndef MESSAGETEMPALTESMODULE_T744814112_H
#define MESSAGETEMPALTESMODULE_T744814112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.MessageTempaltes.MessageTempaltesModule
struct  MessageTempaltesModule_t744814112  : public RuntimeObject
{
public:
	// Library.Code.Facades.MessageTemplates.MessageTemplatesFacade Library.Code.DI.MessageTempaltes.MessageTempaltesModule::_messageTemplatesFacade
	RuntimeObject* ____messageTemplatesFacade_0;

public:
	inline static int32_t get_offset_of__messageTemplatesFacade_0() { return static_cast<int32_t>(offsetof(MessageTempaltesModule_t744814112, ____messageTemplatesFacade_0)); }
	inline RuntimeObject* get__messageTemplatesFacade_0() const { return ____messageTemplatesFacade_0; }
	inline RuntimeObject** get_address_of__messageTemplatesFacade_0() { return &____messageTemplatesFacade_0; }
	inline void set__messageTemplatesFacade_0(RuntimeObject* value)
	{
		____messageTemplatesFacade_0 = value;
		Il2CppCodeGenWriteBarrier((&____messageTemplatesFacade_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGETEMPALTESMODULE_T744814112_H
#ifndef U3CSAVEU3EC__ANONSTOREY0_T2624472547_H
#define U3CSAVEU3EC__ANONSTOREY0_T2624472547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Elements.ElementFacadeImpl/<save>c__AnonStorey0
struct  U3CsaveU3Ec__AnonStorey0_t2624472547  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Element Library.Code.Facades.Elements.ElementFacadeImpl/<save>c__AnonStorey0::models
	Element_t2276588008 * ___models_0;
	// Library.Code.Facades.Callback Library.Code.Facades.Elements.ElementFacadeImpl/<save>c__AnonStorey0::persistCallback
	RuntimeObject* ___persistCallback_1;
	// Library.Code.Facades.Elements.ElementFacadeImpl Library.Code.Facades.Elements.ElementFacadeImpl/<save>c__AnonStorey0::$this
	ElementFacadeImpl_t1898847230 * ___U24this_2;

public:
	inline static int32_t get_offset_of_models_0() { return static_cast<int32_t>(offsetof(U3CsaveU3Ec__AnonStorey0_t2624472547, ___models_0)); }
	inline Element_t2276588008 * get_models_0() const { return ___models_0; }
	inline Element_t2276588008 ** get_address_of_models_0() { return &___models_0; }
	inline void set_models_0(Element_t2276588008 * value)
	{
		___models_0 = value;
		Il2CppCodeGenWriteBarrier((&___models_0), value);
	}

	inline static int32_t get_offset_of_persistCallback_1() { return static_cast<int32_t>(offsetof(U3CsaveU3Ec__AnonStorey0_t2624472547, ___persistCallback_1)); }
	inline RuntimeObject* get_persistCallback_1() const { return ___persistCallback_1; }
	inline RuntimeObject** get_address_of_persistCallback_1() { return &___persistCallback_1; }
	inline void set_persistCallback_1(RuntimeObject* value)
	{
		___persistCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___persistCallback_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CsaveU3Ec__AnonStorey0_t2624472547, ___U24this_2)); }
	inline ElementFacadeImpl_t1898847230 * get_U24this_2() const { return ___U24this_2; }
	inline ElementFacadeImpl_t1898847230 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ElementFacadeImpl_t1898847230 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSAVEU3EC__ANONSTOREY0_T2624472547_H
#ifndef MODELFACADEMODULE_T3252945753_H
#define MODELFACADEMODULE_T3252945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.Models.ModelFacadeModule
struct  ModelFacadeModule_t3252945753  : public RuntimeObject
{
public:
	// Library.Code.Facades.Models.ModelsPersistanceImpl Library.Code.DI.Models.ModelFacadeModule::_modelPersistance
	ModelsPersistanceImpl_t1155557687 * ____modelPersistance_0;
	// Library.Code.Facades.Models.ModelFacade Library.Code.DI.Models.ModelFacadeModule::_modelFacade
	RuntimeObject* ____modelFacade_1;

public:
	inline static int32_t get_offset_of__modelPersistance_0() { return static_cast<int32_t>(offsetof(ModelFacadeModule_t3252945753, ____modelPersistance_0)); }
	inline ModelsPersistanceImpl_t1155557687 * get__modelPersistance_0() const { return ____modelPersistance_0; }
	inline ModelsPersistanceImpl_t1155557687 ** get_address_of__modelPersistance_0() { return &____modelPersistance_0; }
	inline void set__modelPersistance_0(ModelsPersistanceImpl_t1155557687 * value)
	{
		____modelPersistance_0 = value;
		Il2CppCodeGenWriteBarrier((&____modelPersistance_0), value);
	}

	inline static int32_t get_offset_of__modelFacade_1() { return static_cast<int32_t>(offsetof(ModelFacadeModule_t3252945753, ____modelFacade_1)); }
	inline RuntimeObject* get__modelFacade_1() const { return ____modelFacade_1; }
	inline RuntimeObject** get_address_of__modelFacade_1() { return &____modelFacade_1; }
	inline void set__modelFacade_1(RuntimeObject* value)
	{
		____modelFacade_1 = value;
		Il2CppCodeGenWriteBarrier((&____modelFacade_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELFACADEMODULE_T3252945753_H
#ifndef ELEMENTFACADEIMPL_T1898847230_H
#define ELEMENTFACADEIMPL_T1898847230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Elements.ElementFacadeImpl
struct  ElementFacadeImpl_t1898847230  : public RuntimeObject
{
public:
	// Library.Code.Utils.Async.DoAsync Library.Code.Facades.Elements.ElementFacadeImpl::doAsync
	RuntimeObject* ___doAsync_0;
	// Library.Code.Facades.PersistanceFacade`1<Library.Code.Domain.Entities.Element> Library.Code.Facades.Elements.ElementFacadeImpl::_elementPersistance
	RuntimeObject* ____elementPersistance_1;
	// Library.Code.Networking.Elements.ElementApi Library.Code.Facades.Elements.ElementFacadeImpl::_elementApi
	RuntimeObject* ____elementApi_2;
	// System.Collections.Generic.List`1<Library.Code.Facades.Observer`1<Library.Code.Domain.Entities.Element>> Library.Code.Facades.Elements.ElementFacadeImpl::_observers
	List_1_t4178948084 * ____observers_3;
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Element> Library.Code.Facades.Elements.ElementFacadeImpl::_elements
	List_1_t1645709140 * ____elements_4;
	// UnityEngine.Events.UnityAction`1<Library.Code.Domain.Entities.Element> Library.Code.Facades.Elements.ElementFacadeImpl::successFindElementById
	UnityAction_1_t3643173759 * ___successFindElementById_5;
	// System.Int64 Library.Code.Facades.Elements.ElementFacadeImpl::wantedId
	int64_t ___wantedId_6;
	// Library.Code.Domain.Entities.Model Library.Code.Facades.Elements.ElementFacadeImpl::_modelForLoading
	Model_t2163629551 * ____modelForLoading_7;

public:
	inline static int32_t get_offset_of_doAsync_0() { return static_cast<int32_t>(offsetof(ElementFacadeImpl_t1898847230, ___doAsync_0)); }
	inline RuntimeObject* get_doAsync_0() const { return ___doAsync_0; }
	inline RuntimeObject** get_address_of_doAsync_0() { return &___doAsync_0; }
	inline void set_doAsync_0(RuntimeObject* value)
	{
		___doAsync_0 = value;
		Il2CppCodeGenWriteBarrier((&___doAsync_0), value);
	}

	inline static int32_t get_offset_of__elementPersistance_1() { return static_cast<int32_t>(offsetof(ElementFacadeImpl_t1898847230, ____elementPersistance_1)); }
	inline RuntimeObject* get__elementPersistance_1() const { return ____elementPersistance_1; }
	inline RuntimeObject** get_address_of__elementPersistance_1() { return &____elementPersistance_1; }
	inline void set__elementPersistance_1(RuntimeObject* value)
	{
		____elementPersistance_1 = value;
		Il2CppCodeGenWriteBarrier((&____elementPersistance_1), value);
	}

	inline static int32_t get_offset_of__elementApi_2() { return static_cast<int32_t>(offsetof(ElementFacadeImpl_t1898847230, ____elementApi_2)); }
	inline RuntimeObject* get__elementApi_2() const { return ____elementApi_2; }
	inline RuntimeObject** get_address_of__elementApi_2() { return &____elementApi_2; }
	inline void set__elementApi_2(RuntimeObject* value)
	{
		____elementApi_2 = value;
		Il2CppCodeGenWriteBarrier((&____elementApi_2), value);
	}

	inline static int32_t get_offset_of__observers_3() { return static_cast<int32_t>(offsetof(ElementFacadeImpl_t1898847230, ____observers_3)); }
	inline List_1_t4178948084 * get__observers_3() const { return ____observers_3; }
	inline List_1_t4178948084 ** get_address_of__observers_3() { return &____observers_3; }
	inline void set__observers_3(List_1_t4178948084 * value)
	{
		____observers_3 = value;
		Il2CppCodeGenWriteBarrier((&____observers_3), value);
	}

	inline static int32_t get_offset_of__elements_4() { return static_cast<int32_t>(offsetof(ElementFacadeImpl_t1898847230, ____elements_4)); }
	inline List_1_t1645709140 * get__elements_4() const { return ____elements_4; }
	inline List_1_t1645709140 ** get_address_of__elements_4() { return &____elements_4; }
	inline void set__elements_4(List_1_t1645709140 * value)
	{
		____elements_4 = value;
		Il2CppCodeGenWriteBarrier((&____elements_4), value);
	}

	inline static int32_t get_offset_of_successFindElementById_5() { return static_cast<int32_t>(offsetof(ElementFacadeImpl_t1898847230, ___successFindElementById_5)); }
	inline UnityAction_1_t3643173759 * get_successFindElementById_5() const { return ___successFindElementById_5; }
	inline UnityAction_1_t3643173759 ** get_address_of_successFindElementById_5() { return &___successFindElementById_5; }
	inline void set_successFindElementById_5(UnityAction_1_t3643173759 * value)
	{
		___successFindElementById_5 = value;
		Il2CppCodeGenWriteBarrier((&___successFindElementById_5), value);
	}

	inline static int32_t get_offset_of_wantedId_6() { return static_cast<int32_t>(offsetof(ElementFacadeImpl_t1898847230, ___wantedId_6)); }
	inline int64_t get_wantedId_6() const { return ___wantedId_6; }
	inline int64_t* get_address_of_wantedId_6() { return &___wantedId_6; }
	inline void set_wantedId_6(int64_t value)
	{
		___wantedId_6 = value;
	}

	inline static int32_t get_offset_of__modelForLoading_7() { return static_cast<int32_t>(offsetof(ElementFacadeImpl_t1898847230, ____modelForLoading_7)); }
	inline Model_t2163629551 * get__modelForLoading_7() const { return ____modelForLoading_7; }
	inline Model_t2163629551 ** get_address_of__modelForLoading_7() { return &____modelForLoading_7; }
	inline void set__modelForLoading_7(Model_t2163629551 * value)
	{
		____modelForLoading_7 = value;
		Il2CppCodeGenWriteBarrier((&____modelForLoading_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTFACADEIMPL_T1898847230_H
#ifndef ELEMENTAPPROVESTATEFACADEIMP_T2478314832_H
#define ELEMENTAPPROVESTATEFACADEIMP_T2478314832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Elements.ElementApproveStateFacadeImp
struct  ElementApproveStateFacadeImp_t2478314832  : public RuntimeObject
{
public:
	// Library.Code.Networking.Elements.ElementStatusApi Library.Code.Facades.Elements.ElementApproveStateFacadeImp::_elementStatusApi
	RuntimeObject* ____elementStatusApi_0;
	// Library.Code.Facades._3dElementDownload.Element3dFacade Library.Code.Facades.Elements.ElementApproveStateFacadeImp::_element3DFacade
	RuntimeObject* ____element3DFacade_1;
	// UnityEngine.Transform Library.Code.Facades.Elements.ElementApproveStateFacadeImp::popUpTransform
	Transform_t3275118058 * ___popUpTransform_2;
	// Library.Code.UI.PrefabGenerator.PrefabGenerator Library.Code.Facades.Elements.ElementApproveStateFacadeImp::prefabGenerator
	RuntimeObject* ___prefabGenerator_3;
	// Library.Code.Domain.Entities.Element Library.Code.Facades.Elements.ElementApproveStateFacadeImp::currentElement
	Element_t2276588008 * ___currentElement_4;
	// Library.Code.Utils.Async.DoAsync Library.Code.Facades.Elements.ElementApproveStateFacadeImp::_async
	RuntimeObject* ____async_5;
	// Library.Code.DI.PopUp.PopUpShower Library.Code.Facades.Elements.ElementApproveStateFacadeImp::_popUpShower
	PopUpShower_t3186557280 * ____popUpShower_6;
	// UnityEngine.GameObject Library.Code.Facades.Elements.ElementApproveStateFacadeImp::popUp
	GameObject_t1756533147 * ___popUp_7;

public:
	inline static int32_t get_offset_of__elementStatusApi_0() { return static_cast<int32_t>(offsetof(ElementApproveStateFacadeImp_t2478314832, ____elementStatusApi_0)); }
	inline RuntimeObject* get__elementStatusApi_0() const { return ____elementStatusApi_0; }
	inline RuntimeObject** get_address_of__elementStatusApi_0() { return &____elementStatusApi_0; }
	inline void set__elementStatusApi_0(RuntimeObject* value)
	{
		____elementStatusApi_0 = value;
		Il2CppCodeGenWriteBarrier((&____elementStatusApi_0), value);
	}

	inline static int32_t get_offset_of__element3DFacade_1() { return static_cast<int32_t>(offsetof(ElementApproveStateFacadeImp_t2478314832, ____element3DFacade_1)); }
	inline RuntimeObject* get__element3DFacade_1() const { return ____element3DFacade_1; }
	inline RuntimeObject** get_address_of__element3DFacade_1() { return &____element3DFacade_1; }
	inline void set__element3DFacade_1(RuntimeObject* value)
	{
		____element3DFacade_1 = value;
		Il2CppCodeGenWriteBarrier((&____element3DFacade_1), value);
	}

	inline static int32_t get_offset_of_popUpTransform_2() { return static_cast<int32_t>(offsetof(ElementApproveStateFacadeImp_t2478314832, ___popUpTransform_2)); }
	inline Transform_t3275118058 * get_popUpTransform_2() const { return ___popUpTransform_2; }
	inline Transform_t3275118058 ** get_address_of_popUpTransform_2() { return &___popUpTransform_2; }
	inline void set_popUpTransform_2(Transform_t3275118058 * value)
	{
		___popUpTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___popUpTransform_2), value);
	}

	inline static int32_t get_offset_of_prefabGenerator_3() { return static_cast<int32_t>(offsetof(ElementApproveStateFacadeImp_t2478314832, ___prefabGenerator_3)); }
	inline RuntimeObject* get_prefabGenerator_3() const { return ___prefabGenerator_3; }
	inline RuntimeObject** get_address_of_prefabGenerator_3() { return &___prefabGenerator_3; }
	inline void set_prefabGenerator_3(RuntimeObject* value)
	{
		___prefabGenerator_3 = value;
		Il2CppCodeGenWriteBarrier((&___prefabGenerator_3), value);
	}

	inline static int32_t get_offset_of_currentElement_4() { return static_cast<int32_t>(offsetof(ElementApproveStateFacadeImp_t2478314832, ___currentElement_4)); }
	inline Element_t2276588008 * get_currentElement_4() const { return ___currentElement_4; }
	inline Element_t2276588008 ** get_address_of_currentElement_4() { return &___currentElement_4; }
	inline void set_currentElement_4(Element_t2276588008 * value)
	{
		___currentElement_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentElement_4), value);
	}

	inline static int32_t get_offset_of__async_5() { return static_cast<int32_t>(offsetof(ElementApproveStateFacadeImp_t2478314832, ____async_5)); }
	inline RuntimeObject* get__async_5() const { return ____async_5; }
	inline RuntimeObject** get_address_of__async_5() { return &____async_5; }
	inline void set__async_5(RuntimeObject* value)
	{
		____async_5 = value;
		Il2CppCodeGenWriteBarrier((&____async_5), value);
	}

	inline static int32_t get_offset_of__popUpShower_6() { return static_cast<int32_t>(offsetof(ElementApproveStateFacadeImp_t2478314832, ____popUpShower_6)); }
	inline PopUpShower_t3186557280 * get__popUpShower_6() const { return ____popUpShower_6; }
	inline PopUpShower_t3186557280 ** get_address_of__popUpShower_6() { return &____popUpShower_6; }
	inline void set__popUpShower_6(PopUpShower_t3186557280 * value)
	{
		____popUpShower_6 = value;
		Il2CppCodeGenWriteBarrier((&____popUpShower_6), value);
	}

	inline static int32_t get_offset_of_popUp_7() { return static_cast<int32_t>(offsetof(ElementApproveStateFacadeImp_t2478314832, ___popUp_7)); }
	inline GameObject_t1756533147 * get_popUp_7() const { return ___popUp_7; }
	inline GameObject_t1756533147 ** get_address_of_popUp_7() { return &___popUp_7; }
	inline void set_popUp_7(GameObject_t1756533147 * value)
	{
		___popUp_7 = value;
		Il2CppCodeGenWriteBarrier((&___popUp_7), value);
	}
};

struct ElementApproveStateFacadeImp_t2478314832_StaticFields
{
public:
	// System.Action Library.Code.Facades.Elements.ElementApproveStateFacadeImp::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_8() { return static_cast<int32_t>(offsetof(ElementApproveStateFacadeImp_t2478314832_StaticFields, ___U3CU3Ef__amU24cache0_8)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_8() const { return ___U3CU3Ef__amU24cache0_8; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_8() { return &___U3CU3Ef__amU24cache0_8; }
	inline void set_U3CU3Ef__amU24cache0_8(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTAPPROVESTATEFACADEIMP_T2478314832_H
#ifndef ELEMENTHANDLERFACADEIMP_T258577600_H
#define ELEMENTHANDLERFACADEIMP_T258577600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.ElementHandler.ElementHandlerFacadeImp
struct  ElementHandlerFacadeImp_t258577600  : public RuntimeObject
{
public:
	// Library.Code.Facades.ModelGrouping.ModelGroupFacade Library.Code.Facades.ElementHandler.ElementHandlerFacadeImp::modelGroupFacade
	RuntimeObject* ___modelGroupFacade_0;
	// Library.Code.Facades._3dElementDownload.Element3dFacade Library.Code.Facades.ElementHandler.ElementHandlerFacadeImp::_dFacade
	RuntimeObject* ____dFacade_1;
	// Library.Code.Utils.Async.DoAsync Library.Code.Facades.ElementHandler.ElementHandlerFacadeImp::_async
	RuntimeObject* ____async_2;
	// Library.Code.Facades.CheckListF.CheckListFacade Library.Code.Facades.ElementHandler.ElementHandlerFacadeImp::_checkListFacade
	RuntimeObject* ____checkListFacade_3;
	// Library.Code.Domain.Dtos.ElementWithObject Library.Code.Facades.ElementHandler.ElementHandlerFacadeImp::lastLoadedModel
	ElementWithObject_t2362593585 * ___lastLoadedModel_4;

public:
	inline static int32_t get_offset_of_modelGroupFacade_0() { return static_cast<int32_t>(offsetof(ElementHandlerFacadeImp_t258577600, ___modelGroupFacade_0)); }
	inline RuntimeObject* get_modelGroupFacade_0() const { return ___modelGroupFacade_0; }
	inline RuntimeObject** get_address_of_modelGroupFacade_0() { return &___modelGroupFacade_0; }
	inline void set_modelGroupFacade_0(RuntimeObject* value)
	{
		___modelGroupFacade_0 = value;
		Il2CppCodeGenWriteBarrier((&___modelGroupFacade_0), value);
	}

	inline static int32_t get_offset_of__dFacade_1() { return static_cast<int32_t>(offsetof(ElementHandlerFacadeImp_t258577600, ____dFacade_1)); }
	inline RuntimeObject* get__dFacade_1() const { return ____dFacade_1; }
	inline RuntimeObject** get_address_of__dFacade_1() { return &____dFacade_1; }
	inline void set__dFacade_1(RuntimeObject* value)
	{
		____dFacade_1 = value;
		Il2CppCodeGenWriteBarrier((&____dFacade_1), value);
	}

	inline static int32_t get_offset_of__async_2() { return static_cast<int32_t>(offsetof(ElementHandlerFacadeImp_t258577600, ____async_2)); }
	inline RuntimeObject* get__async_2() const { return ____async_2; }
	inline RuntimeObject** get_address_of__async_2() { return &____async_2; }
	inline void set__async_2(RuntimeObject* value)
	{
		____async_2 = value;
		Il2CppCodeGenWriteBarrier((&____async_2), value);
	}

	inline static int32_t get_offset_of__checkListFacade_3() { return static_cast<int32_t>(offsetof(ElementHandlerFacadeImp_t258577600, ____checkListFacade_3)); }
	inline RuntimeObject* get__checkListFacade_3() const { return ____checkListFacade_3; }
	inline RuntimeObject** get_address_of__checkListFacade_3() { return &____checkListFacade_3; }
	inline void set__checkListFacade_3(RuntimeObject* value)
	{
		____checkListFacade_3 = value;
		Il2CppCodeGenWriteBarrier((&____checkListFacade_3), value);
	}

	inline static int32_t get_offset_of_lastLoadedModel_4() { return static_cast<int32_t>(offsetof(ElementHandlerFacadeImp_t258577600, ___lastLoadedModel_4)); }
	inline ElementWithObject_t2362593585 * get_lastLoadedModel_4() const { return ___lastLoadedModel_4; }
	inline ElementWithObject_t2362593585 ** get_address_of_lastLoadedModel_4() { return &___lastLoadedModel_4; }
	inline void set_lastLoadedModel_4(ElementWithObject_t2362593585 * value)
	{
		___lastLoadedModel_4 = value;
		Il2CppCodeGenWriteBarrier((&___lastLoadedModel_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTHANDLERFACADEIMP_T258577600_H
#ifndef MESSAGEFACADEMODULE_T1147318505_H
#define MESSAGEFACADEMODULE_T1147318505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.Message.MessageFacadeModule
struct  MessageFacadeModule_t1147318505  : public RuntimeObject
{
public:
	// Library.Code.Facades.Messages.MessagePersistance Library.Code.DI.Message.MessageFacadeModule::_messagePersistance
	MessagePersistance_t1067416742 * ____messagePersistance_0;
	// Library.Code.Facades.Messages.MessagesFacade Library.Code.DI.Message.MessageFacadeModule::_messagesFacade
	RuntimeObject* ____messagesFacade_1;

public:
	inline static int32_t get_offset_of__messagePersistance_0() { return static_cast<int32_t>(offsetof(MessageFacadeModule_t1147318505, ____messagePersistance_0)); }
	inline MessagePersistance_t1067416742 * get__messagePersistance_0() const { return ____messagePersistance_0; }
	inline MessagePersistance_t1067416742 ** get_address_of__messagePersistance_0() { return &____messagePersistance_0; }
	inline void set__messagePersistance_0(MessagePersistance_t1067416742 * value)
	{
		____messagePersistance_0 = value;
		Il2CppCodeGenWriteBarrier((&____messagePersistance_0), value);
	}

	inline static int32_t get_offset_of__messagesFacade_1() { return static_cast<int32_t>(offsetof(MessageFacadeModule_t1147318505, ____messagesFacade_1)); }
	inline RuntimeObject* get__messagesFacade_1() const { return ____messagesFacade_1; }
	inline RuntimeObject** get_address_of__messagesFacade_1() { return &____messagesFacade_1; }
	inline void set__messagesFacade_1(RuntimeObject* value)
	{
		____messagesFacade_1 = value;
		Il2CppCodeGenWriteBarrier((&____messagesFacade_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEFACADEMODULE_T1147318505_H
#ifndef ELEMENTAPPROVESTATUSMODULE_T4176563737_H
#define ELEMENTAPPROVESTATUSMODULE_T4176563737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.Elements.ElementApproveStatusModule
struct  ElementApproveStatusModule_t4176563737  : public RuntimeObject
{
public:
	// Library.Code.Facades.Elements.ElementApproveStateFacade Library.Code.DI.Elements.ElementApproveStatusModule::_approveStateFacade
	RuntimeObject* ____approveStateFacade_0;

public:
	inline static int32_t get_offset_of__approveStateFacade_0() { return static_cast<int32_t>(offsetof(ElementApproveStatusModule_t4176563737, ____approveStateFacade_0)); }
	inline RuntimeObject* get__approveStateFacade_0() const { return ____approveStateFacade_0; }
	inline RuntimeObject** get_address_of__approveStateFacade_0() { return &____approveStateFacade_0; }
	inline void set__approveStateFacade_0(RuntimeObject* value)
	{
		____approveStateFacade_0 = value;
		Il2CppCodeGenWriteBarrier((&____approveStateFacade_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTAPPROVESTATUSMODULE_T4176563737_H
#ifndef ELEMENTFACADEMODULE_T1081315832_H
#define ELEMENTFACADEMODULE_T1081315832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.Elements.ElementFacadeModule
struct  ElementFacadeModule_t1081315832  : public RuntimeObject
{
public:
	// Library.Code.Facades.Elements.ElementFacade Library.Code.DI.Elements.ElementFacadeModule::_elementFacade
	RuntimeObject* ____elementFacade_0;
	// Library.Code.Facades.Elements.ElementPersistanceImpl Library.Code.DI.Elements.ElementFacadeModule::_elementPersistance
	ElementPersistanceImpl_t1641319881 * ____elementPersistance_1;

public:
	inline static int32_t get_offset_of__elementFacade_0() { return static_cast<int32_t>(offsetof(ElementFacadeModule_t1081315832, ____elementFacade_0)); }
	inline RuntimeObject* get__elementFacade_0() const { return ____elementFacade_0; }
	inline RuntimeObject** get_address_of__elementFacade_0() { return &____elementFacade_0; }
	inline void set__elementFacade_0(RuntimeObject* value)
	{
		____elementFacade_0 = value;
		Il2CppCodeGenWriteBarrier((&____elementFacade_0), value);
	}

	inline static int32_t get_offset_of__elementPersistance_1() { return static_cast<int32_t>(offsetof(ElementFacadeModule_t1081315832, ____elementPersistance_1)); }
	inline ElementPersistanceImpl_t1641319881 * get__elementPersistance_1() const { return ____elementPersistance_1; }
	inline ElementPersistanceImpl_t1641319881 ** get_address_of__elementPersistance_1() { return &____elementPersistance_1; }
	inline void set__elementPersistance_1(ElementPersistanceImpl_t1641319881 * value)
	{
		____elementPersistance_1 = value;
		Il2CppCodeGenWriteBarrier((&____elementPersistance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTFACADEMODULE_T1081315832_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef CHECKPOINTMODULE_T504654608_H
#define CHECKPOINTMODULE_T504654608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.Element3d.CheckpointModule
struct  CheckpointModule_t504654608  : public RuntimeObject
{
public:
	// Library.Code.Facades.CheckPoints.CheckPointsFacade Library.Code.DI.Element3d.CheckpointModule::checkPointFacade
	RuntimeObject* ___checkPointFacade_0;

public:
	inline static int32_t get_offset_of_checkPointFacade_0() { return static_cast<int32_t>(offsetof(CheckpointModule_t504654608, ___checkPointFacade_0)); }
	inline RuntimeObject* get_checkPointFacade_0() const { return ___checkPointFacade_0; }
	inline RuntimeObject** get_address_of_checkPointFacade_0() { return &___checkPointFacade_0; }
	inline void set_checkPointFacade_0(RuntimeObject* value)
	{
		___checkPointFacade_0 = value;
		Il2CppCodeGenWriteBarrier((&___checkPointFacade_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKPOINTMODULE_T504654608_H
#ifndef ELEMENT3DMODULE_T2912470317_H
#define ELEMENT3DMODULE_T2912470317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.Element3d.Element3dModule
struct  Element3dModule_t2912470317  : public RuntimeObject
{
public:
	// Library.Code.Facades._3dElementDownload.Element3dFacade Library.Code.DI.Element3d.Element3dModule::_dFacade
	RuntimeObject* ____dFacade_0;

public:
	inline static int32_t get_offset_of__dFacade_0() { return static_cast<int32_t>(offsetof(Element3dModule_t2912470317, ____dFacade_0)); }
	inline RuntimeObject* get__dFacade_0() const { return ____dFacade_0; }
	inline RuntimeObject** get_address_of__dFacade_0() { return &____dFacade_0; }
	inline void set__dFacade_0(RuntimeObject* value)
	{
		____dFacade_0 = value;
		Il2CppCodeGenWriteBarrier((&____dFacade_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT3DMODULE_T2912470317_H
#ifndef ELEMENTHANDLERMODULE_T3201733362_H
#define ELEMENTHANDLERMODULE_T3201733362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.ElementHandler.ElementHandlerModule
struct  ElementHandlerModule_t3201733362  : public RuntimeObject
{
public:
	// Library.Code.Facades.ElementHandler.ElementHandlerFacade Library.Code.DI.ElementHandler.ElementHandlerModule::_elementHandlerModule
	RuntimeObject* ____elementHandlerModule_0;

public:
	inline static int32_t get_offset_of__elementHandlerModule_0() { return static_cast<int32_t>(offsetof(ElementHandlerModule_t3201733362, ____elementHandlerModule_0)); }
	inline RuntimeObject* get__elementHandlerModule_0() const { return ____elementHandlerModule_0; }
	inline RuntimeObject** get_address_of__elementHandlerModule_0() { return &____elementHandlerModule_0; }
	inline void set__elementHandlerModule_0(RuntimeObject* value)
	{
		____elementHandlerModule_0 = value;
		Il2CppCodeGenWriteBarrier((&____elementHandlerModule_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTHANDLERMODULE_T3201733362_H
#ifndef GROUPINGMODELMODULE_T337983572_H
#define GROUPINGMODELMODULE_T337983572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.GroupinModel.GroupingModelModule
struct  GroupingModelModule_t337983572  : public RuntimeObject
{
public:
	// Library.Code.Facades.ModelGrouping.ModelGroupFacade Library.Code.DI.GroupinModel.GroupingModelModule::_facade
	RuntimeObject* ____facade_0;

public:
	inline static int32_t get_offset_of__facade_0() { return static_cast<int32_t>(offsetof(GroupingModelModule_t337983572, ____facade_0)); }
	inline RuntimeObject* get__facade_0() const { return ____facade_0; }
	inline RuntimeObject** get_address_of__facade_0() { return &____facade_0; }
	inline void set__facade_0(RuntimeObject* value)
	{
		____facade_0 = value;
		Il2CppCodeGenWriteBarrier((&____facade_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPINGMODELMODULE_T337983572_H
#ifndef CHECKPOINTFACADEIMP_T1228426044_H
#define CHECKPOINTFACADEIMP_T1228426044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.CheckPoints.CheckPointFacadeImp
struct  CheckPointFacadeImp_t1228426044  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Facades.Observer`1<Library.Code.Domain.Dtos.CheckPointInfo>> Library.Code.Facades.CheckPoints.CheckPointFacadeImp::_list
	List_1_t956163168 * ____list_1;
	// System.Collections.Generic.List`1<Library.Code.Domain.Dtos.CheckPointInfo> Library.Code.Facades.CheckPoints.CheckPointFacadeImp::_checkPointInfos
	List_1_t2717891520 * ____checkPointInfos_2;
	// Library.Code.Facades._3dElementDownload.Element3dFacade Library.Code.Facades.CheckPoints.CheckPointFacadeImp::_dFacade
	RuntimeObject* ____dFacade_3;

public:
	inline static int32_t get_offset_of__list_1() { return static_cast<int32_t>(offsetof(CheckPointFacadeImp_t1228426044, ____list_1)); }
	inline List_1_t956163168 * get__list_1() const { return ____list_1; }
	inline List_1_t956163168 ** get_address_of__list_1() { return &____list_1; }
	inline void set__list_1(List_1_t956163168 * value)
	{
		____list_1 = value;
		Il2CppCodeGenWriteBarrier((&____list_1), value);
	}

	inline static int32_t get_offset_of__checkPointInfos_2() { return static_cast<int32_t>(offsetof(CheckPointFacadeImp_t1228426044, ____checkPointInfos_2)); }
	inline List_1_t2717891520 * get__checkPointInfos_2() const { return ____checkPointInfos_2; }
	inline List_1_t2717891520 ** get_address_of__checkPointInfos_2() { return &____checkPointInfos_2; }
	inline void set__checkPointInfos_2(List_1_t2717891520 * value)
	{
		____checkPointInfos_2 = value;
		Il2CppCodeGenWriteBarrier((&____checkPointInfos_2), value);
	}

	inline static int32_t get_offset_of__dFacade_3() { return static_cast<int32_t>(offsetof(CheckPointFacadeImp_t1228426044, ____dFacade_3)); }
	inline RuntimeObject* get__dFacade_3() const { return ____dFacade_3; }
	inline RuntimeObject** get_address_of__dFacade_3() { return &____dFacade_3; }
	inline void set__dFacade_3(RuntimeObject* value)
	{
		____dFacade_3 = value;
		Il2CppCodeGenWriteBarrier((&____dFacade_3), value);
	}
};

struct CheckPointFacadeImp_t1228426044_StaticFields
{
public:
	// System.String Library.Code.Facades.CheckPoints.CheckPointFacadeImp::CHECKPOINT_PREFIX
	String_t* ___CHECKPOINT_PREFIX_0;

public:
	inline static int32_t get_offset_of_CHECKPOINT_PREFIX_0() { return static_cast<int32_t>(offsetof(CheckPointFacadeImp_t1228426044_StaticFields, ___CHECKPOINT_PREFIX_0)); }
	inline String_t* get_CHECKPOINT_PREFIX_0() const { return ___CHECKPOINT_PREFIX_0; }
	inline String_t** get_address_of_CHECKPOINT_PREFIX_0() { return &___CHECKPOINT_PREFIX_0; }
	inline void set_CHECKPOINT_PREFIX_0(String_t* value)
	{
		___CHECKPOINT_PREFIX_0 = value;
		Il2CppCodeGenWriteBarrier((&___CHECKPOINT_PREFIX_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKPOINTFACADEIMP_T1228426044_H
#ifndef FILEARRAYDTO_T2141896262_H
#define FILEARRAYDTO_T2141896262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Dtos.FileArrayDto
struct  FileArrayDto_t2141896262  : public RuntimeObject
{
public:
	// System.Byte[] Library.Code.Domain.Dtos.FileArrayDto::bytes
	ByteU5BU5D_t3397334013* ___bytes_0;
	// System.String Library.Code.Domain.Dtos.FileArrayDto::filename
	String_t* ___filename_1;

public:
	inline static int32_t get_offset_of_bytes_0() { return static_cast<int32_t>(offsetof(FileArrayDto_t2141896262, ___bytes_0)); }
	inline ByteU5BU5D_t3397334013* get_bytes_0() const { return ___bytes_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_bytes_0() { return &___bytes_0; }
	inline void set_bytes_0(ByteU5BU5D_t3397334013* value)
	{
		___bytes_0 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_0), value);
	}

	inline static int32_t get_offset_of_filename_1() { return static_cast<int32_t>(offsetof(FileArrayDto_t2141896262, ___filename_1)); }
	inline String_t* get_filename_1() const { return ___filename_1; }
	inline String_t** get_address_of_filename_1() { return &___filename_1; }
	inline void set_filename_1(String_t* value)
	{
		___filename_1 = value;
		Il2CppCodeGenWriteBarrier((&___filename_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEARRAYDTO_T2141896262_H
#ifndef ITEMSTATUSWITHCHECKPOINT_T1657770941_H
#define ITEMSTATUSWITHCHECKPOINT_T1657770941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Dtos.ItemStatusWithCheckpoint
struct  ItemStatusWithCheckpoint_t1657770941  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Checklist.ItemStatus Library.Code.Domain.Dtos.ItemStatusWithCheckpoint::itemStatus
	ItemStatus_t1378751697 * ___itemStatus_0;
	// Library.Code.Domain.Dtos.CheckPointInfo Library.Code.Domain.Dtos.ItemStatusWithCheckpoint::CheckPointInfo
	CheckPointInfo_t3348770388 * ___CheckPointInfo_1;

public:
	inline static int32_t get_offset_of_itemStatus_0() { return static_cast<int32_t>(offsetof(ItemStatusWithCheckpoint_t1657770941, ___itemStatus_0)); }
	inline ItemStatus_t1378751697 * get_itemStatus_0() const { return ___itemStatus_0; }
	inline ItemStatus_t1378751697 ** get_address_of_itemStatus_0() { return &___itemStatus_0; }
	inline void set_itemStatus_0(ItemStatus_t1378751697 * value)
	{
		___itemStatus_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStatus_0), value);
	}

	inline static int32_t get_offset_of_CheckPointInfo_1() { return static_cast<int32_t>(offsetof(ItemStatusWithCheckpoint_t1657770941, ___CheckPointInfo_1)); }
	inline CheckPointInfo_t3348770388 * get_CheckPointInfo_1() const { return ___CheckPointInfo_1; }
	inline CheckPointInfo_t3348770388 ** get_address_of_CheckPointInfo_1() { return &___CheckPointInfo_1; }
	inline void set_CheckPointInfo_1(CheckPointInfo_t3348770388 * value)
	{
		___CheckPointInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___CheckPointInfo_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMSTATUSWITHCHECKPOINT_T1657770941_H
#ifndef MESSAGELISTWRAPPER_T1844780540_H
#define MESSAGELISTWRAPPER_T1844780540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Dtos.MessageListWrapper
struct  MessageListWrapper_t1844780540  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Message> Library.Code.Domain.Dtos.MessageListWrapper::list
	List_1_t583975089 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(MessageListWrapper_t1844780540, ___list_0)); }
	inline List_1_t583975089 * get_list_0() const { return ___list_0; }
	inline List_1_t583975089 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t583975089 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGELISTWRAPPER_T1844780540_H
#ifndef U3CCHANGEITEMSTATUSU3EC__ANONSTOREY0_T205953564_H
#define U3CCHANGEITEMSTATUSU3EC__ANONSTOREY0_T205953564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.CheckListF.CheckListFacadeImp/<changeItemStatus>c__AnonStorey0
struct  U3CchangeItemStatusU3Ec__AnonStorey0_t205953564  : public RuntimeObject
{
public:
	// System.Action`1<System.Boolean> Library.Code.Facades.CheckListF.CheckListFacadeImp/<changeItemStatus>c__AnonStorey0::success
	Action_1_t3627374100 * ___success_0;

public:
	inline static int32_t get_offset_of_success_0() { return static_cast<int32_t>(offsetof(U3CchangeItemStatusU3Ec__AnonStorey0_t205953564, ___success_0)); }
	inline Action_1_t3627374100 * get_success_0() const { return ___success_0; }
	inline Action_1_t3627374100 ** get_address_of_success_0() { return &___success_0; }
	inline void set_success_0(Action_1_t3627374100 * value)
	{
		___success_0 = value;
		Il2CppCodeGenWriteBarrier((&___success_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHANGEITEMSTATUSU3EC__ANONSTOREY0_T205953564_H
#ifndef APPROVEELEMENTRESPONSE_T2765772516_H
#define APPROVEELEMENTRESPONSE_T2765772516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.ApproveElementResponse
struct  ApproveElementResponse_t2765772516  : public RuntimeObject
{
public:
	// System.Int64 Library.Code.Domain.Entities.ApproveElementResponse::id
	int64_t ___id_0;
	// System.String Library.Code.Domain.Entities.ApproveElementResponse::name
	String_t* ___name_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ApproveElementResponse_t2765772516, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(ApproveElementResponse_t2765772516, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPROVEELEMENTRESPONSE_T2765772516_H
#ifndef APPROVEELEMENTWRAPPER_T1123184856_H
#define APPROVEELEMENTWRAPPER_T1123184856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.ApproveElementWrapper
struct  ApproveElementWrapper_t1123184856  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.ApproveElementResponse> Library.Code.Domain.Entities.ApproveElementWrapper::list
	List_1_t2134893648 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(ApproveElementWrapper_t1123184856, ___list_0)); }
	inline List_1_t2134893648 * get_list_0() const { return ___list_0; }
	inline List_1_t2134893648 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t2134893648 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPROVEELEMENTWRAPPER_T1123184856_H
#ifndef BATCHMESSAGES_T2405623082_H
#define BATCHMESSAGES_T2405623082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.BatchMessages
struct  BatchMessages_t2405623082  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Message> Library.Code.Domain.Entities.BatchMessages::messages
	List_1_t583975089 * ___messages_0;

public:
	inline static int32_t get_offset_of_messages_0() { return static_cast<int32_t>(offsetof(BatchMessages_t2405623082, ___messages_0)); }
	inline List_1_t583975089 * get_messages_0() const { return ___messages_0; }
	inline List_1_t583975089 ** get_address_of_messages_0() { return &___messages_0; }
	inline void set_messages_0(List_1_t583975089 * value)
	{
		___messages_0 = value;
		Il2CppCodeGenWriteBarrier((&___messages_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATCHMESSAGES_T2405623082_H
#ifndef PREFIXCOLOR_T1579937663_H
#define PREFIXCOLOR_T1579937663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.PrefixColor.PrefixColor
struct  PrefixColor_t1579937663  : public RuntimeObject
{
public:
	// System.Int64 Library.Code.Domain.Entities.PrefixColor.PrefixColor::id
	int64_t ___id_0;
	// System.String Library.Code.Domain.Entities.PrefixColor.PrefixColor::name
	String_t* ___name_1;
	// System.String Library.Code.Domain.Entities.PrefixColor.PrefixColor::color
	String_t* ___color_2;
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.PrefixColor.ColorParts> Library.Code.Domain.Entities.PrefixColor.PrefixColor::parts
	List_1_t4163807915 * ___parts_3;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(PrefixColor_t1579937663, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(PrefixColor_t1579937663, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_color_2() { return static_cast<int32_t>(offsetof(PrefixColor_t1579937663, ___color_2)); }
	inline String_t* get_color_2() const { return ___color_2; }
	inline String_t** get_address_of_color_2() { return &___color_2; }
	inline void set_color_2(String_t* value)
	{
		___color_2 = value;
		Il2CppCodeGenWriteBarrier((&___color_2), value);
	}

	inline static int32_t get_offset_of_parts_3() { return static_cast<int32_t>(offsetof(PrefixColor_t1579937663, ___parts_3)); }
	inline List_1_t4163807915 * get_parts_3() const { return ___parts_3; }
	inline List_1_t4163807915 ** get_address_of_parts_3() { return &___parts_3; }
	inline void set_parts_3(List_1_t4163807915 * value)
	{
		___parts_3 = value;
		Il2CppCodeGenWriteBarrier((&___parts_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFIXCOLOR_T1579937663_H
#ifndef USERDATANETWORKINFOPROVIDERIMP_T767684310_H
#define USERDATANETWORKINFOPROVIDERIMP_T767684310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.UserDataProvider.UserDataNetworkInfoProviderImp
struct  UserDataNetworkInfoProviderImp_t767684310  : public RuntimeObject
{
public:
	// Library.Code.DI.NetworkConfig Library.Code.DI.UserDataProvider.UserDataNetworkInfoProviderImp::_config
	NetworkConfig_t3187165974 * ____config_0;
	// System.Collections.Generic.List`1<Library.Code.WebRequest.HeaderClass> Library.Code.DI.UserDataProvider.UserDataNetworkInfoProviderImp::headers
	List_1_t1700750585 * ___headers_1;
	// Library.Code.WebRequest.HeaderClass Library.Code.DI.UserDataProvider.UserDataNetworkInfoProviderImp::_contentHeader
	HeaderClass_t2331629453 * ____contentHeader_2;

public:
	inline static int32_t get_offset_of__config_0() { return static_cast<int32_t>(offsetof(UserDataNetworkInfoProviderImp_t767684310, ____config_0)); }
	inline NetworkConfig_t3187165974 * get__config_0() const { return ____config_0; }
	inline NetworkConfig_t3187165974 ** get_address_of__config_0() { return &____config_0; }
	inline void set__config_0(NetworkConfig_t3187165974 * value)
	{
		____config_0 = value;
		Il2CppCodeGenWriteBarrier((&____config_0), value);
	}

	inline static int32_t get_offset_of_headers_1() { return static_cast<int32_t>(offsetof(UserDataNetworkInfoProviderImp_t767684310, ___headers_1)); }
	inline List_1_t1700750585 * get_headers_1() const { return ___headers_1; }
	inline List_1_t1700750585 ** get_address_of_headers_1() { return &___headers_1; }
	inline void set_headers_1(List_1_t1700750585 * value)
	{
		___headers_1 = value;
		Il2CppCodeGenWriteBarrier((&___headers_1), value);
	}

	inline static int32_t get_offset_of__contentHeader_2() { return static_cast<int32_t>(offsetof(UserDataNetworkInfoProviderImp_t767684310, ____contentHeader_2)); }
	inline HeaderClass_t2331629453 * get__contentHeader_2() const { return ____contentHeader_2; }
	inline HeaderClass_t2331629453 ** get_address_of__contentHeader_2() { return &____contentHeader_2; }
	inline void set__contentHeader_2(HeaderClass_t2331629453 * value)
	{
		____contentHeader_2 = value;
		Il2CppCodeGenWriteBarrier((&____contentHeader_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERDATANETWORKINFOPROVIDERIMP_T767684310_H
#ifndef U3CGETCHECKLISTU3EC__ANONSTOREY1_T1420208296_H
#define U3CGETCHECKLISTU3EC__ANONSTOREY1_T1420208296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.CheckListF.CheckListFacadeImp/<getChecklist>c__AnonStorey1
struct  U3CgetChecklistU3Ec__AnonStorey1_t1420208296  : public RuntimeObject
{
public:
	// System.Int64 Library.Code.Facades.CheckListF.CheckListFacadeImp/<getChecklist>c__AnonStorey1::status
	int64_t ___status_0;
	// Library.Code.Domain.Entities.Checklist.CheckList Library.Code.Facades.CheckListF.CheckListFacadeImp/<getChecklist>c__AnonStorey1::returnedC
	CheckList_t4132048078 * ___returnedC_1;

public:
	inline static int32_t get_offset_of_status_0() { return static_cast<int32_t>(offsetof(U3CgetChecklistU3Ec__AnonStorey1_t1420208296, ___status_0)); }
	inline int64_t get_status_0() const { return ___status_0; }
	inline int64_t* get_address_of_status_0() { return &___status_0; }
	inline void set_status_0(int64_t value)
	{
		___status_0 = value;
	}

	inline static int32_t get_offset_of_returnedC_1() { return static_cast<int32_t>(offsetof(U3CgetChecklistU3Ec__AnonStorey1_t1420208296, ___returnedC_1)); }
	inline CheckList_t4132048078 * get_returnedC_1() const { return ___returnedC_1; }
	inline CheckList_t4132048078 ** get_address_of_returnedC_1() { return &___returnedC_1; }
	inline void set_returnedC_1(CheckList_t4132048078 * value)
	{
		___returnedC_1 = value;
		Il2CppCodeGenWriteBarrier((&___returnedC_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETCHECKLISTU3EC__ANONSTOREY1_T1420208296_H
#ifndef CURRENTCHECKLISTITEMFACADEIMP_T1377343062_H
#define CURRENTCHECKLISTITEMFACADEIMP_T1377343062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.CheckListF.CurrentChecklistItemFacadeImp
struct  CurrentChecklistItemFacadeImp_t1377343062  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Checklist.ItemStatus Library.Code.Facades.CheckListF.CurrentChecklistItemFacadeImp::currentItemStatus
	ItemStatus_t1378751697 * ___currentItemStatus_0;
	// System.Collections.Generic.List`1<Library.Code.Facades.ObserverSingle`1<Library.Code.Domain.Entities.Checklist.ItemStatus>> Library.Code.Facades.CheckListF.CurrentChecklistItemFacadeImp::_observerSingles
	List_1_t2401415693 * ____observerSingles_1;

public:
	inline static int32_t get_offset_of_currentItemStatus_0() { return static_cast<int32_t>(offsetof(CurrentChecklistItemFacadeImp_t1377343062, ___currentItemStatus_0)); }
	inline ItemStatus_t1378751697 * get_currentItemStatus_0() const { return ___currentItemStatus_0; }
	inline ItemStatus_t1378751697 ** get_address_of_currentItemStatus_0() { return &___currentItemStatus_0; }
	inline void set_currentItemStatus_0(ItemStatus_t1378751697 * value)
	{
		___currentItemStatus_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentItemStatus_0), value);
	}

	inline static int32_t get_offset_of__observerSingles_1() { return static_cast<int32_t>(offsetof(CurrentChecklistItemFacadeImp_t1377343062, ____observerSingles_1)); }
	inline List_1_t2401415693 * get__observerSingles_1() const { return ____observerSingles_1; }
	inline List_1_t2401415693 ** get_address_of__observerSingles_1() { return &____observerSingles_1; }
	inline void set__observerSingles_1(List_1_t2401415693 * value)
	{
		____observerSingles_1 = value;
		Il2CppCodeGenWriteBarrier((&____observerSingles_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENTCHECKLISTITEMFACADEIMP_T1377343062_H
#ifndef CHECKPOINTINFO_T3348770388_H
#define CHECKPOINTINFO_T3348770388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Dtos.CheckPointInfo
struct  CheckPointInfo_t3348770388  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Library.Code.Domain.Dtos.CheckPointInfo::checkpoint
	GameObject_t1756533147 * ___checkpoint_0;
	// System.String Library.Code.Domain.Dtos.CheckPointInfo::checkpointName
	String_t* ___checkpointName_1;

public:
	inline static int32_t get_offset_of_checkpoint_0() { return static_cast<int32_t>(offsetof(CheckPointInfo_t3348770388, ___checkpoint_0)); }
	inline GameObject_t1756533147 * get_checkpoint_0() const { return ___checkpoint_0; }
	inline GameObject_t1756533147 ** get_address_of_checkpoint_0() { return &___checkpoint_0; }
	inline void set_checkpoint_0(GameObject_t1756533147 * value)
	{
		___checkpoint_0 = value;
		Il2CppCodeGenWriteBarrier((&___checkpoint_0), value);
	}

	inline static int32_t get_offset_of_checkpointName_1() { return static_cast<int32_t>(offsetof(CheckPointInfo_t3348770388, ___checkpointName_1)); }
	inline String_t* get_checkpointName_1() const { return ___checkpointName_1; }
	inline String_t** get_address_of_checkpointName_1() { return &___checkpointName_1; }
	inline void set_checkpointName_1(String_t* value)
	{
		___checkpointName_1 = value;
		Il2CppCodeGenWriteBarrier((&___checkpointName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKPOINTINFO_T3348770388_H
#ifndef U3CSHOWPOPUPU3EC__ANONSTOREY0_T1848632180_H
#define U3CSHOWPOPUPU3EC__ANONSTOREY0_T1848632180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.PopUp.PopUpShower/<showPopup>c__AnonStorey0
struct  U3CshowPopupU3Ec__AnonStorey0_t1848632180  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Library.Code.DI.PopUp.PopUpShower/<showPopup>c__AnonStorey0::popUpGameObject
	GameObject_t1756533147 * ___popUpGameObject_0;
	// Library.Code.UI.PopUp.PopUpStuff Library.Code.DI.PopUp.PopUpShower/<showPopup>c__AnonStorey0::stuff
	PopUpStuff_t3426923330 * ___stuff_1;

public:
	inline static int32_t get_offset_of_popUpGameObject_0() { return static_cast<int32_t>(offsetof(U3CshowPopupU3Ec__AnonStorey0_t1848632180, ___popUpGameObject_0)); }
	inline GameObject_t1756533147 * get_popUpGameObject_0() const { return ___popUpGameObject_0; }
	inline GameObject_t1756533147 ** get_address_of_popUpGameObject_0() { return &___popUpGameObject_0; }
	inline void set_popUpGameObject_0(GameObject_t1756533147 * value)
	{
		___popUpGameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___popUpGameObject_0), value);
	}

	inline static int32_t get_offset_of_stuff_1() { return static_cast<int32_t>(offsetof(U3CshowPopupU3Ec__AnonStorey0_t1848632180, ___stuff_1)); }
	inline PopUpStuff_t3426923330 * get_stuff_1() const { return ___stuff_1; }
	inline PopUpStuff_t3426923330 ** get_address_of_stuff_1() { return &___stuff_1; }
	inline void set_stuff_1(PopUpStuff_t3426923330 * value)
	{
		___stuff_1 = value;
		Il2CppCodeGenWriteBarrier((&___stuff_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWPOPUPU3EC__ANONSTOREY0_T1848632180_H
#ifndef ELEMENTWITHASSETBUNDLE_T1984435670_H
#define ELEMENTWITHASSETBUNDLE_T1984435670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Dtos.ElementWithAssetBundle
struct  ElementWithAssetBundle_t1984435670  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Element Library.Code.Domain.Dtos.ElementWithAssetBundle::element
	Element_t2276588008 * ___element_0;
	// UnityEngine.AssetBundle Library.Code.Domain.Dtos.ElementWithAssetBundle::assetBundle
	AssetBundle_t2054978754 * ___assetBundle_1;

public:
	inline static int32_t get_offset_of_element_0() { return static_cast<int32_t>(offsetof(ElementWithAssetBundle_t1984435670, ___element_0)); }
	inline Element_t2276588008 * get_element_0() const { return ___element_0; }
	inline Element_t2276588008 ** get_address_of_element_0() { return &___element_0; }
	inline void set_element_0(Element_t2276588008 * value)
	{
		___element_0 = value;
		Il2CppCodeGenWriteBarrier((&___element_0), value);
	}

	inline static int32_t get_offset_of_assetBundle_1() { return static_cast<int32_t>(offsetof(ElementWithAssetBundle_t1984435670, ___assetBundle_1)); }
	inline AssetBundle_t2054978754 * get_assetBundle_1() const { return ___assetBundle_1; }
	inline AssetBundle_t2054978754 ** get_address_of_assetBundle_1() { return &___assetBundle_1; }
	inline void set_assetBundle_1(AssetBundle_t2054978754 * value)
	{
		___assetBundle_1 = value;
		Il2CppCodeGenWriteBarrier((&___assetBundle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTWITHASSETBUNDLE_T1984435670_H
#ifndef ELEMENTWITHOBJECT_T2362593585_H
#define ELEMENTWITHOBJECT_T2362593585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Dtos.ElementWithObject
struct  ElementWithObject_t2362593585  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Library.Code.Domain.Dtos.ElementWithObject::element3d
	GameObject_t1756533147 * ___element3d_0;
	// Library.Code.Domain.Entities.Element Library.Code.Domain.Dtos.ElementWithObject::element
	Element_t2276588008 * ___element_1;

public:
	inline static int32_t get_offset_of_element3d_0() { return static_cast<int32_t>(offsetof(ElementWithObject_t2362593585, ___element3d_0)); }
	inline GameObject_t1756533147 * get_element3d_0() const { return ___element3d_0; }
	inline GameObject_t1756533147 ** get_address_of_element3d_0() { return &___element3d_0; }
	inline void set_element3d_0(GameObject_t1756533147 * value)
	{
		___element3d_0 = value;
		Il2CppCodeGenWriteBarrier((&___element3d_0), value);
	}

	inline static int32_t get_offset_of_element_1() { return static_cast<int32_t>(offsetof(ElementWithObject_t2362593585, ___element_1)); }
	inline Element_t2276588008 * get_element_1() const { return ___element_1; }
	inline Element_t2276588008 ** get_address_of_element_1() { return &___element_1; }
	inline void set_element_1(Element_t2276588008 * value)
	{
		___element_1 = value;
		Il2CppCodeGenWriteBarrier((&___element_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTWITHOBJECT_T2362593585_H
#ifndef SENDFILEMODULE_T3843131166_H
#define SENDFILEMODULE_T3843131166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.SendFile.SendFileModule
struct  SendFileModule_t3843131166  : public RuntimeObject
{
public:
	// Library.Code.Facades.Files.SendFileFacade Library.Code.DI.SendFile.SendFileModule::sendFileFacade
	RuntimeObject* ___sendFileFacade_0;

public:
	inline static int32_t get_offset_of_sendFileFacade_0() { return static_cast<int32_t>(offsetof(SendFileModule_t3843131166, ___sendFileFacade_0)); }
	inline RuntimeObject* get_sendFileFacade_0() const { return ___sendFileFacade_0; }
	inline RuntimeObject** get_address_of_sendFileFacade_0() { return &___sendFileFacade_0; }
	inline void set_sendFileFacade_0(RuntimeObject* value)
	{
		___sendFileFacade_0 = value;
		Il2CppCodeGenWriteBarrier((&___sendFileFacade_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDFILEMODULE_T3843131166_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef CAMERAPOSITIONDTO_T1507864167_H
#define CAMERAPOSITIONDTO_T1507864167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Dtos.CameraPositionDto
struct  CameraPositionDto_t1507864167  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Library.Code.Domain.Dtos.CameraPositionDto::camPos
	Vector3_t2243707580  ___camPos_0;
	// UnityEngine.Vector3 Library.Code.Domain.Dtos.CameraPositionDto::camRot
	Vector3_t2243707580  ___camRot_1;
	// UnityEngine.Vector3 Library.Code.Domain.Dtos.CameraPositionDto::modPos
	Vector3_t2243707580  ___modPos_2;
	// UnityEngine.Vector3 Library.Code.Domain.Dtos.CameraPositionDto::modRot
	Vector3_t2243707580  ___modRot_3;

public:
	inline static int32_t get_offset_of_camPos_0() { return static_cast<int32_t>(offsetof(CameraPositionDto_t1507864167, ___camPos_0)); }
	inline Vector3_t2243707580  get_camPos_0() const { return ___camPos_0; }
	inline Vector3_t2243707580 * get_address_of_camPos_0() { return &___camPos_0; }
	inline void set_camPos_0(Vector3_t2243707580  value)
	{
		___camPos_0 = value;
	}

	inline static int32_t get_offset_of_camRot_1() { return static_cast<int32_t>(offsetof(CameraPositionDto_t1507864167, ___camRot_1)); }
	inline Vector3_t2243707580  get_camRot_1() const { return ___camRot_1; }
	inline Vector3_t2243707580 * get_address_of_camRot_1() { return &___camRot_1; }
	inline void set_camRot_1(Vector3_t2243707580  value)
	{
		___camRot_1 = value;
	}

	inline static int32_t get_offset_of_modPos_2() { return static_cast<int32_t>(offsetof(CameraPositionDto_t1507864167, ___modPos_2)); }
	inline Vector3_t2243707580  get_modPos_2() const { return ___modPos_2; }
	inline Vector3_t2243707580 * get_address_of_modPos_2() { return &___modPos_2; }
	inline void set_modPos_2(Vector3_t2243707580  value)
	{
		___modPos_2 = value;
	}

	inline static int32_t get_offset_of_modRot_3() { return static_cast<int32_t>(offsetof(CameraPositionDto_t1507864167, ___modRot_3)); }
	inline Vector3_t2243707580  get_modRot_3() const { return ___modRot_3; }
	inline Vector3_t2243707580 * get_address_of_modRot_3() { return &___modRot_3; }
	inline void set_modRot_3(Vector3_t2243707580  value)
	{
		___modRot_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAPOSITIONDTO_T1507864167_H
#ifndef MODELGROUPDTO_T1808254547_H
#define MODELGROUPDTO_T1808254547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Dtos.ModelGroupDto
struct  ModelGroupDto_t1808254547  : public RuntimeObject
{
public:
	// System.Int32 Library.Code.Domain.Dtos.ModelGroupDto::size
	int32_t ___size_0;
	// UnityEngine.Color Library.Code.Domain.Dtos.ModelGroupDto::_color
	Color_t2020392075  ____color_1;
	// System.String Library.Code.Domain.Dtos.ModelGroupDto::_name
	String_t* ____name_2;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Library.Code.Domain.Dtos.ModelGroupDto::_objects
	List_1_t1125654279 * ____objects_3;
	// System.Boolean Library.Code.Domain.Dtos.ModelGroupDto::visible
	bool ___visible_4;

public:
	inline static int32_t get_offset_of_size_0() { return static_cast<int32_t>(offsetof(ModelGroupDto_t1808254547, ___size_0)); }
	inline int32_t get_size_0() const { return ___size_0; }
	inline int32_t* get_address_of_size_0() { return &___size_0; }
	inline void set_size_0(int32_t value)
	{
		___size_0 = value;
	}

	inline static int32_t get_offset_of__color_1() { return static_cast<int32_t>(offsetof(ModelGroupDto_t1808254547, ____color_1)); }
	inline Color_t2020392075  get__color_1() const { return ____color_1; }
	inline Color_t2020392075 * get_address_of__color_1() { return &____color_1; }
	inline void set__color_1(Color_t2020392075  value)
	{
		____color_1 = value;
	}

	inline static int32_t get_offset_of__name_2() { return static_cast<int32_t>(offsetof(ModelGroupDto_t1808254547, ____name_2)); }
	inline String_t* get__name_2() const { return ____name_2; }
	inline String_t** get_address_of__name_2() { return &____name_2; }
	inline void set__name_2(String_t* value)
	{
		____name_2 = value;
		Il2CppCodeGenWriteBarrier((&____name_2), value);
	}

	inline static int32_t get_offset_of__objects_3() { return static_cast<int32_t>(offsetof(ModelGroupDto_t1808254547, ____objects_3)); }
	inline List_1_t1125654279 * get__objects_3() const { return ____objects_3; }
	inline List_1_t1125654279 ** get_address_of__objects_3() { return &____objects_3; }
	inline void set__objects_3(List_1_t1125654279 * value)
	{
		____objects_3 = value;
		Il2CppCodeGenWriteBarrier((&____objects_3), value);
	}

	inline static int32_t get_offset_of_visible_4() { return static_cast<int32_t>(offsetof(ModelGroupDto_t1808254547, ___visible_4)); }
	inline bool get_visible_4() const { return ___visible_4; }
	inline bool* get_address_of_visible_4() { return &___visible_4; }
	inline void set_visible_4(bool value)
	{
		___visible_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELGROUPDTO_T1808254547_H
#ifndef CHECLISTITEMSTATUS_T94742076_H
#define CHECLISTITEMSTATUS_T94742076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.Checklist.CheclistItemStatus
struct  CheclistItemStatus_t94742076 
{
public:
	// System.Int32 Library.Code.Domain.Entities.Checklist.CheclistItemStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CheclistItemStatus_t94742076, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECLISTITEMSTATUS_T94742076_H
#ifndef NETWORKMODE_T4181639873_H
#define NETWORKMODE_T4181639873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.NetworkConfig/NetworkMode
struct  NetworkMode_t4181639873 
{
public:
	// System.Int32 Library.Code.DI.NetworkConfig/NetworkMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NetworkMode_t4181639873, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKMODE_T4181639873_H
#ifndef ELEMENTSTATE_T2285175491_H
#define ELEMENTSTATE_T2285175491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.Enums.ElementState
struct  ElementState_t2285175491 
{
public:
	// System.Int32 Library.Code.Domain.Entities.Enums.ElementState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ElementState_t2285175491, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTSTATE_T2285175491_H
#ifndef HOLOFILETYPE_T513177464_H
#define HOLOFILETYPE_T513177464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.Enums.HoloFileType
struct  HoloFileType_t513177464 
{
public:
	// System.Int32 Library.Code.Domain.Entities.Enums.HoloFileType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HoloFileType_t513177464, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLOFILETYPE_T513177464_H
#ifndef HOLOFILETYPE_T3239551668_H
#define HOLOFILETYPE_T3239551668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.HoloFileType
struct  HoloFileType_t3239551668 
{
public:
	// System.Int32 Library.Code.Domain.Entities.HoloFileType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HoloFileType_t3239551668, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLOFILETYPE_T3239551668_H
#ifndef MODELGROUP_T3234187646_H
#define MODELGROUP_T3234187646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Entities.ModelGroup
struct  ModelGroup_t3234187646  : public RuntimeObject
{
public:
	// UnityEngine.Color Library.Code.Domain.Entities.ModelGroup::color
	Color_t2020392075  ___color_0;
	// System.Collections.Generic.List`1<System.String> Library.Code.Domain.Entities.ModelGroup::prefixes
	List_1_t1398341365 * ___prefixes_1;
	// System.String Library.Code.Domain.Entities.ModelGroup::name
	String_t* ___name_2;

public:
	inline static int32_t get_offset_of_color_0() { return static_cast<int32_t>(offsetof(ModelGroup_t3234187646, ___color_0)); }
	inline Color_t2020392075  get_color_0() const { return ___color_0; }
	inline Color_t2020392075 * get_address_of_color_0() { return &___color_0; }
	inline void set_color_0(Color_t2020392075  value)
	{
		___color_0 = value;
	}

	inline static int32_t get_offset_of_prefixes_1() { return static_cast<int32_t>(offsetof(ModelGroup_t3234187646, ___prefixes_1)); }
	inline List_1_t1398341365 * get_prefixes_1() const { return ___prefixes_1; }
	inline List_1_t1398341365 ** get_address_of_prefixes_1() { return &___prefixes_1; }
	inline void set_prefixes_1(List_1_t1398341365 * value)
	{
		___prefixes_1 = value;
		Il2CppCodeGenWriteBarrier((&___prefixes_1), value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(ModelGroup_t3234187646, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}
};

struct ModelGroup_t3234187646_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<Library.Code.Domain.Entities.ModelGroup> Library.Code.Domain.Entities.ModelGroup::ColorPrefixesNameComparerInstance
	RuntimeObject* ___ColorPrefixesNameComparerInstance_3;

public:
	inline static int32_t get_offset_of_ColorPrefixesNameComparerInstance_3() { return static_cast<int32_t>(offsetof(ModelGroup_t3234187646_StaticFields, ___ColorPrefixesNameComparerInstance_3)); }
	inline RuntimeObject* get_ColorPrefixesNameComparerInstance_3() const { return ___ColorPrefixesNameComparerInstance_3; }
	inline RuntimeObject** get_address_of_ColorPrefixesNameComparerInstance_3() { return &___ColorPrefixesNameComparerInstance_3; }
	inline void set_ColorPrefixesNameComparerInstance_3(RuntimeObject* value)
	{
		___ColorPrefixesNameComparerInstance_3 = value;
		Il2CppCodeGenWriteBarrier((&___ColorPrefixesNameComparerInstance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELGROUP_T3234187646_H
#ifndef NETWORKCONFIG_T3187165974_H
#define NETWORKCONFIG_T3187165974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.NetworkConfig
struct  NetworkConfig_t3187165974  : public RuntimeObject
{
public:
	// System.String Library.Code.DI.NetworkConfig::localWojtek
	String_t* ___localWojtek_0;
	// System.String Library.Code.DI.NetworkConfig::testBaseUrl
	String_t* ___testBaseUrl_1;
	// System.String Library.Code.DI.NetworkConfig::prodBaseUrl
	String_t* ___prodBaseUrl_2;
	// Library.Code.DI.NetworkConfig/NetworkMode Library.Code.DI.NetworkConfig::mode
	int32_t ___mode_3;

public:
	inline static int32_t get_offset_of_localWojtek_0() { return static_cast<int32_t>(offsetof(NetworkConfig_t3187165974, ___localWojtek_0)); }
	inline String_t* get_localWojtek_0() const { return ___localWojtek_0; }
	inline String_t** get_address_of_localWojtek_0() { return &___localWojtek_0; }
	inline void set_localWojtek_0(String_t* value)
	{
		___localWojtek_0 = value;
		Il2CppCodeGenWriteBarrier((&___localWojtek_0), value);
	}

	inline static int32_t get_offset_of_testBaseUrl_1() { return static_cast<int32_t>(offsetof(NetworkConfig_t3187165974, ___testBaseUrl_1)); }
	inline String_t* get_testBaseUrl_1() const { return ___testBaseUrl_1; }
	inline String_t** get_address_of_testBaseUrl_1() { return &___testBaseUrl_1; }
	inline void set_testBaseUrl_1(String_t* value)
	{
		___testBaseUrl_1 = value;
		Il2CppCodeGenWriteBarrier((&___testBaseUrl_1), value);
	}

	inline static int32_t get_offset_of_prodBaseUrl_2() { return static_cast<int32_t>(offsetof(NetworkConfig_t3187165974, ___prodBaseUrl_2)); }
	inline String_t* get_prodBaseUrl_2() const { return ___prodBaseUrl_2; }
	inline String_t** get_address_of_prodBaseUrl_2() { return &___prodBaseUrl_2; }
	inline void set_prodBaseUrl_2(String_t* value)
	{
		___prodBaseUrl_2 = value;
		Il2CppCodeGenWriteBarrier((&___prodBaseUrl_2), value);
	}

	inline static int32_t get_offset_of_mode_3() { return static_cast<int32_t>(offsetof(NetworkConfig_t3187165974, ___mode_3)); }
	inline int32_t get_mode_3() const { return ___mode_3; }
	inline int32_t* get_address_of_mode_3() { return &___mode_3; }
	inline void set_mode_3(int32_t value)
	{
		___mode_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCONFIG_T3187165974_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef ELEMENT3DPROVIDER_T3146139312_H
#define ELEMENT3DPROVIDER_T3146139312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.Element3d.Element3dProvider
struct  Element3dProvider_t3146139312  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Networking._3dElement.DownloadElement3d Library.Code.DI.Element3d.Element3dProvider::_downloadElement3D
	RuntimeObject* ____downloadElement3D_2;
	// Library.Code.DI.Element3d.Element3dModule Library.Code.DI.Element3d.Element3dProvider::_element3DModule
	Element3dModule_t2912470317 * ____element3DModule_3;
	// Library.Code.DI.Elements.ElementProvider Library.Code.DI.Element3d.Element3dProvider::_elementProvider
	ElementProvider_t3018113545 * ____elementProvider_4;
	// Library.Code.DI.Element3d.CheckpointModule Library.Code.DI.Element3d.Element3dProvider::_checkpointModule
	CheckpointModule_t504654608 * ____checkpointModule_5;

public:
	inline static int32_t get_offset_of__downloadElement3D_2() { return static_cast<int32_t>(offsetof(Element3dProvider_t3146139312, ____downloadElement3D_2)); }
	inline RuntimeObject* get__downloadElement3D_2() const { return ____downloadElement3D_2; }
	inline RuntimeObject** get_address_of__downloadElement3D_2() { return &____downloadElement3D_2; }
	inline void set__downloadElement3D_2(RuntimeObject* value)
	{
		____downloadElement3D_2 = value;
		Il2CppCodeGenWriteBarrier((&____downloadElement3D_2), value);
	}

	inline static int32_t get_offset_of__element3DModule_3() { return static_cast<int32_t>(offsetof(Element3dProvider_t3146139312, ____element3DModule_3)); }
	inline Element3dModule_t2912470317 * get__element3DModule_3() const { return ____element3DModule_3; }
	inline Element3dModule_t2912470317 ** get_address_of__element3DModule_3() { return &____element3DModule_3; }
	inline void set__element3DModule_3(Element3dModule_t2912470317 * value)
	{
		____element3DModule_3 = value;
		Il2CppCodeGenWriteBarrier((&____element3DModule_3), value);
	}

	inline static int32_t get_offset_of__elementProvider_4() { return static_cast<int32_t>(offsetof(Element3dProvider_t3146139312, ____elementProvider_4)); }
	inline ElementProvider_t3018113545 * get__elementProvider_4() const { return ____elementProvider_4; }
	inline ElementProvider_t3018113545 ** get_address_of__elementProvider_4() { return &____elementProvider_4; }
	inline void set__elementProvider_4(ElementProvider_t3018113545 * value)
	{
		____elementProvider_4 = value;
		Il2CppCodeGenWriteBarrier((&____elementProvider_4), value);
	}

	inline static int32_t get_offset_of__checkpointModule_5() { return static_cast<int32_t>(offsetof(Element3dProvider_t3146139312, ____checkpointModule_5)); }
	inline CheckpointModule_t504654608 * get__checkpointModule_5() const { return ____checkpointModule_5; }
	inline CheckpointModule_t504654608 ** get_address_of__checkpointModule_5() { return &____checkpointModule_5; }
	inline void set__checkpointModule_5(CheckpointModule_t504654608 * value)
	{
		____checkpointModule_5 = value;
		Il2CppCodeGenWriteBarrier((&____checkpointModule_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENT3DPROVIDER_T3146139312_H
#ifndef ELEMENTHANDLERPROVIDER_T2338674629_H
#define ELEMENTHANDLERPROVIDER_T2338674629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.ElementHandler.ElementHandlerProvider
struct  ElementHandlerProvider_t2338674629  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.DI.Checklist.ChecklistProvider Library.Code.DI.ElementHandler.ElementHandlerProvider::_checklistProvider
	ChecklistProvider_t3393423783 * ____checklistProvider_2;
	// Library.Code.DI.Element3d.Element3dProvider Library.Code.DI.ElementHandler.ElementHandlerProvider::_element3DProvider
	Element3dProvider_t3146139312 * ____element3DProvider_3;
	// Library.Code.DI.GroupinModel.GroupingModelProvider Library.Code.DI.ElementHandler.ElementHandlerProvider::_modelGroupFacade
	GroupingModelProvider_t2551172685 * ____modelGroupFacade_4;
	// Library.Code.DI.ElementHandler.ElementHandlerModule Library.Code.DI.ElementHandler.ElementHandlerProvider::_elementHandlerModule
	ElementHandlerModule_t3201733362 * ____elementHandlerModule_5;

public:
	inline static int32_t get_offset_of__checklistProvider_2() { return static_cast<int32_t>(offsetof(ElementHandlerProvider_t2338674629, ____checklistProvider_2)); }
	inline ChecklistProvider_t3393423783 * get__checklistProvider_2() const { return ____checklistProvider_2; }
	inline ChecklistProvider_t3393423783 ** get_address_of__checklistProvider_2() { return &____checklistProvider_2; }
	inline void set__checklistProvider_2(ChecklistProvider_t3393423783 * value)
	{
		____checklistProvider_2 = value;
		Il2CppCodeGenWriteBarrier((&____checklistProvider_2), value);
	}

	inline static int32_t get_offset_of__element3DProvider_3() { return static_cast<int32_t>(offsetof(ElementHandlerProvider_t2338674629, ____element3DProvider_3)); }
	inline Element3dProvider_t3146139312 * get__element3DProvider_3() const { return ____element3DProvider_3; }
	inline Element3dProvider_t3146139312 ** get_address_of__element3DProvider_3() { return &____element3DProvider_3; }
	inline void set__element3DProvider_3(Element3dProvider_t3146139312 * value)
	{
		____element3DProvider_3 = value;
		Il2CppCodeGenWriteBarrier((&____element3DProvider_3), value);
	}

	inline static int32_t get_offset_of__modelGroupFacade_4() { return static_cast<int32_t>(offsetof(ElementHandlerProvider_t2338674629, ____modelGroupFacade_4)); }
	inline GroupingModelProvider_t2551172685 * get__modelGroupFacade_4() const { return ____modelGroupFacade_4; }
	inline GroupingModelProvider_t2551172685 ** get_address_of__modelGroupFacade_4() { return &____modelGroupFacade_4; }
	inline void set__modelGroupFacade_4(GroupingModelProvider_t2551172685 * value)
	{
		____modelGroupFacade_4 = value;
		Il2CppCodeGenWriteBarrier((&____modelGroupFacade_4), value);
	}

	inline static int32_t get_offset_of__elementHandlerModule_5() { return static_cast<int32_t>(offsetof(ElementHandlerProvider_t2338674629, ____elementHandlerModule_5)); }
	inline ElementHandlerModule_t3201733362 * get__elementHandlerModule_5() const { return ____elementHandlerModule_5; }
	inline ElementHandlerModule_t3201733362 ** get_address_of__elementHandlerModule_5() { return &____elementHandlerModule_5; }
	inline void set__elementHandlerModule_5(ElementHandlerModule_t3201733362 * value)
	{
		____elementHandlerModule_5 = value;
		Il2CppCodeGenWriteBarrier((&____elementHandlerModule_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTHANDLERPROVIDER_T2338674629_H
#ifndef PROVIDERWITHAPIFACTORY_1_T2855966632_H
#define PROVIDERWITHAPIFACTORY_1_T2855966632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.ProviderWithApiFactory`1<Library.Code.Networking.Models.ModelApi>
struct  ProviderWithApiFactory_1_t2855966632  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERWITHAPIFACTORY_1_T2855966632_H
#ifndef PROVIDERWITHAPIFACTORY_1_T2940439303_H
#define PROVIDERWITHAPIFACTORY_1_T2940439303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.ProviderWithApiFactory`1<Library.Code.Networking.MessageTemplate.MessageTemplatesApi>
struct  ProviderWithApiFactory_1_t2940439303  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERWITHAPIFACTORY_1_T2940439303_H
#ifndef PREFABPROVIDER_T1422780085_H
#define PREFABPROVIDER_T1422780085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.Prefab.PrefabProvider
struct  PrefabProvider_t1422780085  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.UI.PrefabGenerator.PrefabGenerator Library.Code.DI.Prefab.PrefabProvider::_prefabGenerator
	RuntimeObject* ____prefabGenerator_2;

public:
	inline static int32_t get_offset_of__prefabGenerator_2() { return static_cast<int32_t>(offsetof(PrefabProvider_t1422780085, ____prefabGenerator_2)); }
	inline RuntimeObject* get__prefabGenerator_2() const { return ____prefabGenerator_2; }
	inline RuntimeObject** get_address_of__prefabGenerator_2() { return &____prefabGenerator_2; }
	inline void set__prefabGenerator_2(RuntimeObject* value)
	{
		____prefabGenerator_2 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABPROVIDER_T1422780085_H
#ifndef CAMERAPROVIDER_T364748060_H
#define CAMERAPROVIDER_T364748060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Facades.Camera.CameraProvider
struct  CameraProvider_t364748060  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Facades._3dElementDownload.Element3dFacade Library.Code.Facades.Camera.CameraProvider::element3dFacade
	RuntimeObject* ___element3dFacade_2;
	// Library.Code.Facades.Camera.CameraFacadeImp Library.Code.Facades.Camera.CameraProvider::_cameraFacadeImp
	CameraFacadeImp_t1649784751 * ____cameraFacadeImp_3;

public:
	inline static int32_t get_offset_of_element3dFacade_2() { return static_cast<int32_t>(offsetof(CameraProvider_t364748060, ___element3dFacade_2)); }
	inline RuntimeObject* get_element3dFacade_2() const { return ___element3dFacade_2; }
	inline RuntimeObject** get_address_of_element3dFacade_2() { return &___element3dFacade_2; }
	inline void set_element3dFacade_2(RuntimeObject* value)
	{
		___element3dFacade_2 = value;
		Il2CppCodeGenWriteBarrier((&___element3dFacade_2), value);
	}

	inline static int32_t get_offset_of__cameraFacadeImp_3() { return static_cast<int32_t>(offsetof(CameraProvider_t364748060, ____cameraFacadeImp_3)); }
	inline CameraFacadeImp_t1649784751 * get__cameraFacadeImp_3() const { return ____cameraFacadeImp_3; }
	inline CameraFacadeImp_t1649784751 ** get_address_of__cameraFacadeImp_3() { return &____cameraFacadeImp_3; }
	inline void set__cameraFacadeImp_3(CameraFacadeImp_t1649784751 * value)
	{
		____cameraFacadeImp_3 = value;
		Il2CppCodeGenWriteBarrier((&____cameraFacadeImp_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAPROVIDER_T364748060_H
#ifndef PROVIDERWITHAPIFACTORY_1_T1501685989_H
#define PROVIDERWITHAPIFACTORY_1_T1501685989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.ProviderWithApiFactory`1<Library.Code.Networking.Elements.ElementStatusApi>
struct  ProviderWithApiFactory_1_t1501685989  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERWITHAPIFACTORY_1_T1501685989_H
#ifndef MESSAGEPROVIDER_T618924372_H
#define MESSAGEPROVIDER_T618924372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.Message.MessageProvider
struct  MessageProvider_t618924372  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.DI.Message.MessageFacadeModule Library.Code.DI.Message.MessageProvider::_messageFacadeModule
	MessageFacadeModule_t1147318505 * ____messageFacadeModule_2;
	// Library.Code.DI.Message.SendMessageModule Library.Code.DI.Message.MessageProvider::_sendMessageModule
	SendMessageModule_t690017393 * ____sendMessageModule_3;
	// Library.Code.DI.SendFile.SendFileModule Library.Code.DI.Message.MessageProvider::_sendFileModule
	SendFileModule_t3843131166 * ____sendFileModule_4;

public:
	inline static int32_t get_offset_of__messageFacadeModule_2() { return static_cast<int32_t>(offsetof(MessageProvider_t618924372, ____messageFacadeModule_2)); }
	inline MessageFacadeModule_t1147318505 * get__messageFacadeModule_2() const { return ____messageFacadeModule_2; }
	inline MessageFacadeModule_t1147318505 ** get_address_of__messageFacadeModule_2() { return &____messageFacadeModule_2; }
	inline void set__messageFacadeModule_2(MessageFacadeModule_t1147318505 * value)
	{
		____messageFacadeModule_2 = value;
		Il2CppCodeGenWriteBarrier((&____messageFacadeModule_2), value);
	}

	inline static int32_t get_offset_of__sendMessageModule_3() { return static_cast<int32_t>(offsetof(MessageProvider_t618924372, ____sendMessageModule_3)); }
	inline SendMessageModule_t690017393 * get__sendMessageModule_3() const { return ____sendMessageModule_3; }
	inline SendMessageModule_t690017393 ** get_address_of__sendMessageModule_3() { return &____sendMessageModule_3; }
	inline void set__sendMessageModule_3(SendMessageModule_t690017393 * value)
	{
		____sendMessageModule_3 = value;
		Il2CppCodeGenWriteBarrier((&____sendMessageModule_3), value);
	}

	inline static int32_t get_offset_of__sendFileModule_4() { return static_cast<int32_t>(offsetof(MessageProvider_t618924372, ____sendFileModule_4)); }
	inline SendFileModule_t3843131166 * get__sendFileModule_4() const { return ____sendFileModule_4; }
	inline SendFileModule_t3843131166 ** get_address_of__sendFileModule_4() { return &____sendFileModule_4; }
	inline void set__sendFileModule_4(SendFileModule_t3843131166 * value)
	{
		____sendFileModule_4 = value;
		Il2CppCodeGenWriteBarrier((&____sendFileModule_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEPROVIDER_T618924372_H
#ifndef PROVIDERWITHAPIFACTORY_1_T4144576901_H
#define PROVIDERWITHAPIFACTORY_1_T4144576901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.ProviderWithApiFactory`1<Library.Code.Networking.Elements.ElementApi>
struct  ProviderWithApiFactory_1_t4144576901  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERWITHAPIFACTORY_1_T4144576901_H
#ifndef POPUPSHOWER_T3186557280_H
#define POPUPSHOWER_T3186557280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.PopUp.PopUpShower
struct  PopUpShower_t3186557280  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform Library.Code.DI.PopUp.PopUpShower::canvasTrans
	Transform_t3275118058 * ___canvasTrans_2;

public:
	inline static int32_t get_offset_of_canvasTrans_2() { return static_cast<int32_t>(offsetof(PopUpShower_t3186557280, ___canvasTrans_2)); }
	inline Transform_t3275118058 * get_canvasTrans_2() const { return ___canvasTrans_2; }
	inline Transform_t3275118058 ** get_address_of_canvasTrans_2() { return &___canvasTrans_2; }
	inline void set_canvasTrans_2(Transform_t3275118058 * value)
	{
		___canvasTrans_2 = value;
		Il2CppCodeGenWriteBarrier((&___canvasTrans_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPUPSHOWER_T3186557280_H
#ifndef PROVIDERWITHAPIFACTORY_1_T1428020601_H
#define PROVIDERWITHAPIFACTORY_1_T1428020601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.ProviderWithApiFactory`1<Library.Code.Networking.GroupModel.GroupingModelApi>
struct  ProviderWithApiFactory_1_t1428020601  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERWITHAPIFACTORY_1_T1428020601_H
#ifndef ELEMENTPROVIDER_T3018113545_H
#define ELEMENTPROVIDER_T3018113545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.Elements.ElementProvider
struct  ElementProvider_t3018113545  : public ProviderWithApiFactory_1_t4144576901
{
public:
	// Library.Code.DI.Elements.ElementFacadeModule Library.Code.DI.Elements.ElementProvider::_elementFacadeModule
	ElementFacadeModule_t1081315832 * ____elementFacadeModule_2;

public:
	inline static int32_t get_offset_of__elementFacadeModule_2() { return static_cast<int32_t>(offsetof(ElementProvider_t3018113545, ____elementFacadeModule_2)); }
	inline ElementFacadeModule_t1081315832 * get__elementFacadeModule_2() const { return ____elementFacadeModule_2; }
	inline ElementFacadeModule_t1081315832 ** get_address_of__elementFacadeModule_2() { return &____elementFacadeModule_2; }
	inline void set__elementFacadeModule_2(ElementFacadeModule_t1081315832 * value)
	{
		____elementFacadeModule_2 = value;
		Il2CppCodeGenWriteBarrier((&____elementFacadeModule_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTPROVIDER_T3018113545_H
#ifndef GROUPINGMODELPROVIDER_T2551172685_H
#define GROUPINGMODELPROVIDER_T2551172685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.GroupinModel.GroupingModelProvider
struct  GroupingModelProvider_t2551172685  : public ProviderWithApiFactory_1_t1428020601
{
public:
	// Library.Code.DI.GroupinModel.GroupingModelModule Library.Code.DI.GroupinModel.GroupingModelProvider::_groupingModelModule
	GroupingModelModule_t337983572 * ____groupingModelModule_2;

public:
	inline static int32_t get_offset_of__groupingModelModule_2() { return static_cast<int32_t>(offsetof(GroupingModelProvider_t2551172685, ____groupingModelModule_2)); }
	inline GroupingModelModule_t337983572 * get__groupingModelModule_2() const { return ____groupingModelModule_2; }
	inline GroupingModelModule_t337983572 ** get_address_of__groupingModelModule_2() { return &____groupingModelModule_2; }
	inline void set__groupingModelModule_2(GroupingModelModule_t337983572 * value)
	{
		____groupingModelModule_2 = value;
		Il2CppCodeGenWriteBarrier((&____groupingModelModule_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPINGMODELPROVIDER_T2551172685_H
#ifndef MESSAGETEMPLATESPROVIDER_T2923081561_H
#define MESSAGETEMPLATESPROVIDER_T2923081561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.MessageTempaltes.MessageTemplatesProvider
struct  MessageTemplatesProvider_t2923081561  : public ProviderWithApiFactory_1_t2940439303
{
public:
	// Library.Code.DI.MessageTempaltes.MessageTempaltesModule Library.Code.DI.MessageTempaltes.MessageTemplatesProvider::_messageTempaltesModule
	MessageTempaltesModule_t744814112 * ____messageTempaltesModule_2;

public:
	inline static int32_t get_offset_of__messageTempaltesModule_2() { return static_cast<int32_t>(offsetof(MessageTemplatesProvider_t2923081561, ____messageTempaltesModule_2)); }
	inline MessageTempaltesModule_t744814112 * get__messageTempaltesModule_2() const { return ____messageTempaltesModule_2; }
	inline MessageTempaltesModule_t744814112 ** get_address_of__messageTempaltesModule_2() { return &____messageTempaltesModule_2; }
	inline void set__messageTempaltesModule_2(MessageTempaltesModule_t744814112 * value)
	{
		____messageTempaltesModule_2 = value;
		Il2CppCodeGenWriteBarrier((&____messageTempaltesModule_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGETEMPLATESPROVIDER_T2923081561_H
#ifndef MODELPROVIDER_T1845401484_H
#define MODELPROVIDER_T1845401484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.Models.ModelProvider
struct  ModelProvider_t1845401484  : public ProviderWithApiFactory_1_t2855966632
{
public:
	// Library.Code.DI.Models.ModelFacadeModule Library.Code.DI.Models.ModelProvider::_modelFacadeModule
	ModelFacadeModule_t3252945753 * ____modelFacadeModule_2;

public:
	inline static int32_t get_offset_of__modelFacadeModule_2() { return static_cast<int32_t>(offsetof(ModelProvider_t1845401484, ____modelFacadeModule_2)); }
	inline ModelFacadeModule_t3252945753 * get__modelFacadeModule_2() const { return ____modelFacadeModule_2; }
	inline ModelFacadeModule_t3252945753 ** get_address_of__modelFacadeModule_2() { return &____modelFacadeModule_2; }
	inline void set__modelFacadeModule_2(ModelFacadeModule_t3252945753 * value)
	{
		____modelFacadeModule_2 = value;
		Il2CppCodeGenWriteBarrier((&____modelFacadeModule_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELPROVIDER_T1845401484_H
#ifndef ELEMENTAPPROVEPROVIDER_T2738039706_H
#define ELEMENTAPPROVEPROVIDER_T2738039706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.DI.Elements.ElementApproveProvider
struct  ElementApproveProvider_t2738039706  : public ProviderWithApiFactory_1_t1501685989
{
public:
	// Library.Code.DI.Elements.ElementApproveStatusModule Library.Code.DI.Elements.ElementApproveProvider::_elementApproveStatusModule
	ElementApproveStatusModule_t4176563737 * ____elementApproveStatusModule_2;

public:
	inline static int32_t get_offset_of__elementApproveStatusModule_2() { return static_cast<int32_t>(offsetof(ElementApproveProvider_t2738039706, ____elementApproveStatusModule_2)); }
	inline ElementApproveStatusModule_t4176563737 * get__elementApproveStatusModule_2() const { return ____elementApproveStatusModule_2; }
	inline ElementApproveStatusModule_t4176563737 ** get_address_of__elementApproveStatusModule_2() { return &____elementApproveStatusModule_2; }
	inline void set__elementApproveStatusModule_2(ElementApproveStatusModule_t4176563737 * value)
	{
		____elementApproveStatusModule_2 = value;
		Il2CppCodeGenWriteBarrier((&____elementApproveStatusModule_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTAPPROVEPROVIDER_T2738039706_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (ElementApproveProvider_t2738039706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2100[1] = 
{
	ElementApproveProvider_t2738039706::get_offset_of__elementApproveStatusModule_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (ElementApproveStatusModule_t4176563737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2101[1] = 
{
	ElementApproveStatusModule_t4176563737::get_offset_of__approveStateFacade_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (ElementFacadeModule_t1081315832), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2102[2] = 
{
	ElementFacadeModule_t1081315832::get_offset_of__elementFacade_0(),
	ElementFacadeModule_t1081315832::get_offset_of__elementPersistance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (ElementProvider_t3018113545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2103[1] = 
{
	ElementProvider_t3018113545::get_offset_of__elementFacadeModule_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (CheckpointModule_t504654608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2104[1] = 
{
	CheckpointModule_t504654608::get_offset_of_checkPointFacade_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (Element3dModule_t2912470317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2105[1] = 
{
	Element3dModule_t2912470317::get_offset_of__dFacade_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (Element3dProvider_t3146139312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2106[4] = 
{
	Element3dProvider_t3146139312::get_offset_of__downloadElement3D_2(),
	Element3dProvider_t3146139312::get_offset_of__element3DModule_3(),
	Element3dProvider_t3146139312::get_offset_of__elementProvider_4(),
	Element3dProvider_t3146139312::get_offset_of__checkpointModule_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (ElementHandlerModule_t3201733362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2107[1] = 
{
	ElementHandlerModule_t3201733362::get_offset_of__elementHandlerModule_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (ElementHandlerProvider_t2338674629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2108[4] = 
{
	ElementHandlerProvider_t2338674629::get_offset_of__checklistProvider_2(),
	ElementHandlerProvider_t2338674629::get_offset_of__element3DProvider_3(),
	ElementHandlerProvider_t2338674629::get_offset_of__modelGroupFacade_4(),
	ElementHandlerProvider_t2338674629::get_offset_of__elementHandlerModule_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (GroupingModelModule_t337983572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2109[1] = 
{
	GroupingModelModule_t337983572::get_offset_of__facade_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (GroupingModelProvider_t2551172685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2110[1] = 
{
	GroupingModelProvider_t2551172685::get_offset_of__groupingModelModule_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (MessageFacadeModule_t1147318505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2111[2] = 
{
	MessageFacadeModule_t1147318505::get_offset_of__messagePersistance_0(),
	MessageFacadeModule_t1147318505::get_offset_of__messagesFacade_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (MessageProvider_t618924372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2112[3] = 
{
	MessageProvider_t618924372::get_offset_of__messageFacadeModule_2(),
	MessageProvider_t618924372::get_offset_of__sendMessageModule_3(),
	MessageProvider_t618924372::get_offset_of__sendFileModule_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (SendMessageModule_t690017393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2113[1] = 
{
	SendMessageModule_t690017393::get_offset_of__sendMessageFacade_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (MessageTempaltesModule_t744814112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2114[1] = 
{
	MessageTempaltesModule_t744814112::get_offset_of__messageTemplatesFacade_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (MessageTemplatesProvider_t2923081561), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2115[1] = 
{
	MessageTemplatesProvider_t2923081561::get_offset_of__messageTempaltesModule_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (ModelFacadeModule_t3252945753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2116[2] = 
{
	ModelFacadeModule_t3252945753::get_offset_of__modelPersistance_0(),
	ModelFacadeModule_t3252945753::get_offset_of__modelFacade_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (ModelProvider_t1845401484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2117[1] = 
{
	ModelProvider_t1845401484::get_offset_of__modelFacadeModule_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (NetworkConfig_t3187165974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2118[4] = 
{
	NetworkConfig_t3187165974::get_offset_of_localWojtek_0(),
	NetworkConfig_t3187165974::get_offset_of_testBaseUrl_1(),
	NetworkConfig_t3187165974::get_offset_of_prodBaseUrl_2(),
	NetworkConfig_t3187165974::get_offset_of_mode_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (NetworkMode_t4181639873)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2119[3] = 
{
	NetworkMode_t4181639873::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (PopUpShower_t3186557280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2120[1] = 
{
	PopUpShower_t3186557280::get_offset_of_canvasTrans_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (U3CshowPopupU3Ec__AnonStorey0_t1848632180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2121[2] = 
{
	U3CshowPopupU3Ec__AnonStorey0_t1848632180::get_offset_of_popUpGameObject_0(),
	U3CshowPopupU3Ec__AnonStorey0_t1848632180::get_offset_of_stuff_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (PrefabProvider_t1422780085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2122[1] = 
{
	PrefabProvider_t1422780085::get_offset_of__prefabGenerator_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (SendFileModule_t3843131166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2124[1] = 
{
	SendFileModule_t3843131166::get_offset_of_sendFileFacade_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (UserDataNetworkInfoProviderImp_t767684310), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2125[3] = 
{
	UserDataNetworkInfoProviderImp_t767684310::get_offset_of__config_0(),
	UserDataNetworkInfoProviderImp_t767684310::get_offset_of_headers_1(),
	UserDataNetworkInfoProviderImp_t767684310::get_offset_of__contentHeader_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (CameraPositionDto_t1507864167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[4] = 
{
	CameraPositionDto_t1507864167::get_offset_of_camPos_0(),
	CameraPositionDto_t1507864167::get_offset_of_camRot_1(),
	CameraPositionDto_t1507864167::get_offset_of_modPos_2(),
	CameraPositionDto_t1507864167::get_offset_of_modRot_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (CheckPointInfo_t3348770388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2127[2] = 
{
	CheckPointInfo_t3348770388::get_offset_of_checkpoint_0(),
	CheckPointInfo_t3348770388::get_offset_of_checkpointName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (ElementWithAssetBundle_t1984435670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2128[2] = 
{
	ElementWithAssetBundle_t1984435670::get_offset_of_element_0(),
	ElementWithAssetBundle_t1984435670::get_offset_of_assetBundle_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (ElementWithObject_t2362593585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2129[2] = 
{
	ElementWithObject_t2362593585::get_offset_of_element3d_0(),
	ElementWithObject_t2362593585::get_offset_of_element_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (FileArrayDto_t2141896262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2130[2] = 
{
	FileArrayDto_t2141896262::get_offset_of_bytes_0(),
	FileArrayDto_t2141896262::get_offset_of_filename_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (ItemStatusWithCheckpoint_t1657770941), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2131[2] = 
{
	ItemStatusWithCheckpoint_t1657770941::get_offset_of_itemStatus_0(),
	ItemStatusWithCheckpoint_t1657770941::get_offset_of_CheckPointInfo_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (MessageListWrapper_t1844780540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2132[1] = 
{
	MessageListWrapper_t1844780540::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (ModelGroupDto_t1808254547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2133[5] = 
{
	ModelGroupDto_t1808254547::get_offset_of_size_0(),
	ModelGroupDto_t1808254547::get_offset_of__color_1(),
	ModelGroupDto_t1808254547::get_offset_of__name_2(),
	ModelGroupDto_t1808254547::get_offset_of__objects_3(),
	ModelGroupDto_t1808254547::get_offset_of_visible_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (ApproveElementResponse_t2765772516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2134[2] = 
{
	ApproveElementResponse_t2765772516::get_offset_of_id_0(),
	ApproveElementResponse_t2765772516::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (ApproveElementWrapper_t1123184856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2135[1] = 
{
	ApproveElementWrapper_t1123184856::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (BatchMessages_t2405623082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2136[1] = 
{
	BatchMessages_t2405623082::get_offset_of_messages_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (BatchMessagesResponses_t304281498), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2137[1] = 
{
	BatchMessagesResponses_t304281498::get_offset_of_messageIds_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (CheckList_t4132048078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2138[4] = 
{
	CheckList_t4132048078::get_offset_of_id_0(),
	CheckList_t4132048078::get_offset_of_name_1(),
	CheckList_t4132048078::get_offset_of_checklistItems_2(),
	CheckList_t4132048078::get_offset_of_elementStatus_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (CheckListResponse_t3656740397), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2139[1] = 
{
	CheckListResponse_t3656740397::get_offset_of_checklists_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (ElementStatus_t2221316768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2140[2] = 
{
	ElementStatus_t2221316768::get_offset_of_id_0(),
	ElementStatus_t2221316768::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (ItemStatus_t1378751697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2141[4] = 
{
	ItemStatus_t1378751697::get_offset_of_id_0(),
	ItemStatus_t1378751697::get_offset_of_content_1(),
	ItemStatus_t1378751697::get_offset_of_order_2(),
	ItemStatus_t1378751697::get_offset_of_status_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (CheclistItemStatus_t94742076)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2142[4] = 
{
	CheclistItemStatus_t94742076::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (ItemStatusDto_t3640770036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2143[2] = 
{
	ItemStatusDto_t3640770036::get_offset_of_itemStatus_0(),
	ItemStatusDto_t3640770036::get_offset_of_isCurrent_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (Element_t2276588008), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2144[14] = 
{
	Element_t2276588008::get_offset_of_id_0(),
	Element_t2276588008::get_offset_of_name_1(),
	Element_t2276588008::get_offset_of_designerId_2(),
	Element_t2276588008::get_offset_of_completed_3(),
	Element_t2276588008::get_offset_of_createdAt_4(),
	Element_t2276588008::get_offset_of_modelId_5(),
	Element_t2276588008::get_offset_of_currentFileUrl_6(),
	Element_t2276588008::get_offset_of_currentFileAndroid_7(),
	Element_t2276588008::get_offset_of_currentFileIOS_8(),
	Element_t2276588008::get_offset_of_currentFileHololens_9(),
	Element_t2276588008::get_offset_of_status_10(),
	Element_t2276588008::get_offset_of_oldStatus_11(),
	Element_t2276588008::get_offset_of_qrPath_12(),
	Element_t2276588008::get_offset_of_statusId_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (ElementList_t1752951218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2145[1] = 
{
	ElementList_t1752951218::get_offset_of_elementsList_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (ElementsEmbedded_t1754787703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2146[1] = 
{
	ElementsEmbedded_t1754787703::get_offset_of__embedded_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (DefaultElementsEmbedded_t250135042), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2147[1] = 
{
	DefaultElementsEmbedded_t250135042::get_offset_of_elements_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (ElementState_t2285175491)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2148[8] = 
{
	ElementState_t2285175491::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (StateConverter_t4104478257), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (HoloFileType_t513177464)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2150[4] = 
{
	HoloFileType_t513177464::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (HoloFileTypeConverter_t3247504072), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (GroupCreate_t2003352421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2152[4] = 
{
	GroupCreate_t2003352421::get_offset_of_id_0(),
	GroupCreate_t2003352421::get_offset_of_name_1(),
	GroupCreate_t2003352421::get_offset_of_messageIds_2(),
	GroupCreate_t2003352421::get_offset_of_elementId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (GroupCreateResponse_t2325302190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2153[2] = 
{
	GroupCreateResponse_t2325302190::get_offset_of_id_0(),
	GroupCreateResponse_t2325302190::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (GroupCreate1_t1731656564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2154[2] = 
{
	GroupCreate1_t1731656564::get_offset_of_element_0(),
	GroupCreate1_t1731656564::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (GroupCreate2_t1731656567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2155[2] = 
{
	GroupCreate2_t1731656567::get_offset_of_element_0(),
	GroupCreate2_t1731656567::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (HoloFile_t1957467228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2156[5] = 
{
	HoloFile_t1957467228::get_offset_of_element_0(),
	HoloFile_t1957467228::get_offset_of_path_1(),
	HoloFile_t1957467228::get_offset_of_type_2(),
	HoloFile_t1957467228::get_offset_of_conversionStatus_3(),
	HoloFile_t1957467228::get_offset_of_conversionMsg_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (HoloFileType_t3239551668)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2157[4] = 
{
	HoloFileType_t3239551668::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (HoloFileTypeConverter_t2088768576), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (Message_t1214853957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2159[23] = 
{
	Message_t1214853957::get_offset_of_element_0(),
	Message_t1214853957::get_offset_of_elementId_1(),
	Message_t1214853957::get_offset_of_content_2(),
	Message_t1214853957::get_offset_of_error_3(),
	Message_t1214853957::get_offset_of_checkpoint_4(),
	Message_t1214853957::get_offset_of_checklistItemId_5(),
	Message_t1214853957::get_offset_of_cameraXPos_6(),
	Message_t1214853957::get_offset_of_cameraYPos_7(),
	Message_t1214853957::get_offset_of_cameraZPos_8(),
	Message_t1214853957::get_offset_of_cameraXRot_9(),
	Message_t1214853957::get_offset_of_cameraYRot_10(),
	Message_t1214853957::get_offset_of_cameraZRot_11(),
	Message_t1214853957::get_offset_of_modelXPos_12(),
	Message_t1214853957::get_offset_of_modelYPos_13(),
	Message_t1214853957::get_offset_of_modelZPos_14(),
	Message_t1214853957::get_offset_of_modelXRot_15(),
	Message_t1214853957::get_offset_of_modelYRot_16(),
	Message_t1214853957::get_offset_of_modelZRot_17(),
	Message_t1214853957::get_offset_of_groupId_18(),
	Message_t1214853957::get_offset_of_messageId_19(),
	Message_t1214853957::get_offset_of_snapshotPath_20(),
	Message_t1214853957::get_offset_of_paths_21(),
	Message_t1214853957::get_offset_of_audioPath_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (MessageList_t401268481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2160[1] = 
{
	MessageList_t401268481::get_offset_of_messagesList_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (MessageTemplate_t2793553551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2161[2] = 
{
	MessageTemplate_t2793553551::get_offset_of_content_0(),
	MessageTemplate_t2793553551::get_offset_of_companyId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (MessageTemplateEmbedded_t613025749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2162[1] = 
{
	MessageTemplateEmbedded_t613025749::get_offset_of__embedded_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (DefaultMessageEmbadded_t2732967408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2163[1] = 
{
	DefaultMessageEmbadded_t2732967408::get_offset_of_defaultMessages_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (Model_t2163629551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2164[8] = 
{
	Model_t2163629551::get_offset_of_id_0(),
	Model_t2163629551::get_offset_of_name_1(),
	Model_t2163629551::get_offset_of_companyId_2(),
	Model_t2163629551::get_offset_of_createdAt_3(),
	Model_t2163629551::get_offset_of_dueDate_4(),
	Model_t2163629551::get_offset_of_imageUrl_5(),
	Model_t2163629551::get_offset_of_client_6(),
	Model_t2163629551::get_offset_of_description_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (ModelEmbedded_t574489313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2165[1] = 
{
	ModelEmbedded_t574489313::get_offset_of__embedded_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (DefaultModelEmbedded_t2860339312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2166[1] = 
{
	DefaultModelEmbedded_t2860339312::get_offset_of_models_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (ModelGroup_t3234187646), -1, sizeof(ModelGroup_t3234187646_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2167[4] = 
{
	ModelGroup_t3234187646::get_offset_of_color_0(),
	ModelGroup_t3234187646::get_offset_of_prefixes_1(),
	ModelGroup_t3234187646::get_offset_of_name_2(),
	ModelGroup_t3234187646_StaticFields::get_offset_of_ColorPrefixesNameComparerInstance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (ColorPrefixesNameEqualityComparer_t3918632335), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (ModelsList_t4161403536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2169[1] = 
{
	ModelsList_t4161403536::get_offset_of_modelsList_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (PrefixColorResponse_t106821790), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[1] = 
{
	PrefixColorResponse_t106821790::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (PrefixColor_t1579937663), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2171[4] = 
{
	PrefixColor_t1579937663::get_offset_of_id_0(),
	PrefixColor_t1579937663::get_offset_of_name_1(),
	PrefixColor_t1579937663::get_offset_of_color_2(),
	PrefixColor_t1579937663::get_offset_of_parts_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (ColorParts_t499719487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2172[2] = 
{
	ColorParts_t499719487::get_offset_of_id_0(),
	ColorParts_t499719487::get_offset_of_prefix_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (QRCode_t296287656), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2173[1] = 
{
	QRCode_t296287656::get_offset_of_scannedString_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (TemplateList_t3112186284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2174[1] = 
{
	TemplateList_t3112186284::get_offset_of_templates_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (Element3dFacadeImp_t2774445145), -1, sizeof(Element3dFacadeImp_t2774445145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2176[8] = 
{
	Element3dFacadeImp_t2774445145::get_offset_of__elementFacade_0(),
	Element3dFacadeImp_t2774445145::get_offset_of__downloadElement3D_1(),
	Element3dFacadeImp_t2774445145::get_offset_of__createInterface_2(),
	Element3dFacadeImp_t2774445145::get_offset_of__currentElement_3(),
	Element3dFacadeImp_t2774445145::get_offset_of__async_4(),
	Element3dFacadeImp_t2774445145::get_offset_of__observers_5(),
	Element3dFacadeImp_t2774445145::get_offset_of_loadingScale_6(),
	Element3dFacadeImp_t2774445145_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (CameraFacadeImp_t1649784751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2183[2] = 
{
	CameraFacadeImp_t1649784751::get_offset_of__cameraPositionDto_0(),
	CameraFacadeImp_t1649784751::get_offset_of_loadedObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (CameraProvider_t364748060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2184[2] = 
{
	CameraProvider_t364748060::get_offset_of_element3dFacade_2(),
	CameraProvider_t364748060::get_offset_of__cameraFacadeImp_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (CheckListFacadeImp_t2146505562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2186[6] = 
{
	CheckListFacadeImp_t2146505562::get_offset_of__async_0(),
	CheckListFacadeImp_t2146505562::get_offset_of__checklistForElementApi_1(),
	CheckListFacadeImp_t2146505562::get_offset_of__changeChecklistItemStatusApi_2(),
	CheckListFacadeImp_t2146505562::get_offset_of__checkLists_3(),
	CheckListFacadeImp_t2146505562::get_offset_of__observers_4(),
	CheckListFacadeImp_t2146505562::get_offset_of_currentElement_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (U3CchangeItemStatusU3Ec__AnonStorey0_t205953564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2187[1] = 
{
	U3CchangeItemStatusU3Ec__AnonStorey0_t205953564::get_offset_of_success_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (U3CgetChecklistU3Ec__AnonStorey1_t1420208296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2188[2] = 
{
	U3CgetChecklistU3Ec__AnonStorey1_t1420208296::get_offset_of_status_0(),
	U3CgetChecklistU3Ec__AnonStorey1_t1420208296::get_offset_of_returnedC_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (CurrentChecklistItemFacadeImp_t1377343062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2190[2] = 
{
	CurrentChecklistItemFacadeImp_t1377343062::get_offset_of_currentItemStatus_0(),
	CurrentChecklistItemFacadeImp_t1377343062::get_offset_of__observerSingles_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (CheckPointFacadeImp_t1228426044), -1, sizeof(CheckPointFacadeImp_t1228426044_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2191[4] = 
{
	CheckPointFacadeImp_t1228426044_StaticFields::get_offset_of_CHECKPOINT_PREFIX_0(),
	CheckPointFacadeImp_t1228426044::get_offset_of__list_1(),
	CheckPointFacadeImp_t1228426044::get_offset_of__checkPointInfos_2(),
	CheckPointFacadeImp_t1228426044::get_offset_of__dFacade_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (ElementHandlerFacadeImp_t258577600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2194[5] = 
{
	ElementHandlerFacadeImp_t258577600::get_offset_of_modelGroupFacade_0(),
	ElementHandlerFacadeImp_t258577600::get_offset_of__dFacade_1(),
	ElementHandlerFacadeImp_t258577600::get_offset_of__async_2(),
	ElementHandlerFacadeImp_t258577600::get_offset_of__checkListFacade_3(),
	ElementHandlerFacadeImp_t258577600::get_offset_of_lastLoadedModel_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (ElementApproveStateFacadeImp_t2478314832), -1, sizeof(ElementApproveStateFacadeImp_t2478314832_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2196[9] = 
{
	ElementApproveStateFacadeImp_t2478314832::get_offset_of__elementStatusApi_0(),
	ElementApproveStateFacadeImp_t2478314832::get_offset_of__element3DFacade_1(),
	ElementApproveStateFacadeImp_t2478314832::get_offset_of_popUpTransform_2(),
	ElementApproveStateFacadeImp_t2478314832::get_offset_of_prefabGenerator_3(),
	ElementApproveStateFacadeImp_t2478314832::get_offset_of_currentElement_4(),
	ElementApproveStateFacadeImp_t2478314832::get_offset_of__async_5(),
	ElementApproveStateFacadeImp_t2478314832::get_offset_of__popUpShower_6(),
	ElementApproveStateFacadeImp_t2478314832::get_offset_of_popUp_7(),
	ElementApproveStateFacadeImp_t2478314832_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (ElementFacadeImpl_t1898847230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2198[8] = 
{
	ElementFacadeImpl_t1898847230::get_offset_of_doAsync_0(),
	ElementFacadeImpl_t1898847230::get_offset_of__elementPersistance_1(),
	ElementFacadeImpl_t1898847230::get_offset_of__elementApi_2(),
	ElementFacadeImpl_t1898847230::get_offset_of__observers_3(),
	ElementFacadeImpl_t1898847230::get_offset_of__elements_4(),
	ElementFacadeImpl_t1898847230::get_offset_of_successFindElementById_5(),
	ElementFacadeImpl_t1898847230::get_offset_of_wantedId_6(),
	ElementFacadeImpl_t1898847230::get_offset_of__modelForLoading_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (U3CsaveU3Ec__AnonStorey0_t2624472547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2199[3] = 
{
	U3CsaveU3Ec__AnonStorey0_t2624472547::get_offset_of_models_0(),
	U3CsaveU3Ec__AnonStorey0_t2624472547::get_offset_of_persistCallback_1(),
	U3CsaveU3Ec__AnonStorey0_t2624472547::get_offset_of_U24this_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
