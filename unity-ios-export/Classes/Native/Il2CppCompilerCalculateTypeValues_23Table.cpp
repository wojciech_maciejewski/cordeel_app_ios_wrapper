﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// CI.HttpClient.HttpClient
struct HttpClient_t2113786063;
// System.String
struct String_t;
// Library.Code.WebRequest.UserDataNetworkInfoProvider
struct UserDataNetworkInfoProvider_t529481330;
// Library.Code.Domain.Entities.ApproveElementResponse
struct ApproveElementResponse_t2765772516;
// Library.Code.UI.ElementStatus.ElementNewStatusChoos
struct ElementNewStatusChoos_t3073096760;
// System.Action`1<Library.Code.UI.ElementStatus.AproveElementDto>
struct Action_1_t490573446;
// Library.Code.UI.ElementStatus.ApproveElementRow
struct ApproveElementRow_t3948802595;
// UnityEngine.Transform
struct Transform_t3275118058;
// Library.Code.UI.ChooseModel.ModelObserverNew
struct ModelObserverNew_t1299566747;
// Library.Code.UI.ChooseModel.ModelObserver
struct ModelObserver_t1172265923;
// ModelElementObserver
struct ModelElementObserver_t2747689887;
// Library.Code.UI.ChooseModel.ElementsObserverNew
struct ElementsObserverNew_t2308744357;
// Library.Code.UI.ChooseModel.ElementsObserver
struct ElementsObserver_t3382904053;
// Library.Code.Domain.Entities.Checklist.ItemStatusDto
struct ItemStatusDto_t3640770036;
// UnityEngine.Events.UnityAction`1<Library.Code.Domain.Entities.Checklist.ItemStatusDto>
struct UnityAction_1_t712388491;
// Library.Code.UI.ChecklistUI.ItemStatusRow
struct ItemStatusRow_t2790536877;
// Library.Code.UI.ElementStatus.AproveElementDto
struct AproveElementDto_t688774064;
// Library.Code.UI.ElementStatus.ElementNewStatusChoos/<addRows>c__AnonStorey0
struct U3CaddRowsU3Ec__AnonStorey0_t2717169914;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.Dictionary`2<Library.Code.UI.PopUp.PopUps,Library.Code.UI.PopUp.PopUpHandler>
struct Dictionary_2_t1112370778;
// Library.Code.UI.PopUp.PopUpStuff
struct PopUpStuff_t3426923330;
// Library.Code.UI.MessageTemplates.Remark
struct Remark_t2047647426;
// Library.Code.Domain.Entities.Checklist.ItemStatus
struct ItemStatus_t1378751697;
// Library.Code.UI.ChecklistUI.ChecklistUI
struct ChecklistUI_t3492366448;
// Library.Code.Domain.Dtos.ItemStatusWithCheckpoint
struct ItemStatusWithCheckpoint_t1657770941;
// Library.Code.UI.ChecklistUI.ChecklistMobileUINew/<ActionOnCheckChange>c__AnonStorey0
struct U3CActionOnCheckChangeU3Ec__AnonStorey0_t4060093481;
// System.Action
struct Action_t3226471752;
// Library.Code.Parsers.JsonParser`2<Library.Code.Domain.Entities.GroupCreate2,Library.Code.Domain.Entities.GroupCreate>
struct JsonParser_2_t693764146;
// Library.Code.WebRequest.WebRequestBuilder
struct WebRequestBuilder_t3983605310;
// Library.Code.UI.ChecklistUI.ChecklistMobileUINew
struct ChecklistMobileUINew_t1060043268;
// Library.Code.Domain.Entities.GroupCreate
struct GroupCreate_t2003352421;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t254341728;
// Library.Code.Networking.ResponseWrapper`1<System.Int64>
struct ResponseWrapper_1_t2404039101;
// Library.Code.Networking.Message.GroupCreatorApiImp
struct GroupCreatorApiImp_t3941306685;
// Library.Code.Parsers.JsonParser`2<Library.Code.Domain.Entities.BatchMessages,Library.Code.Domain.Entities.BatchMessagesResponses>
struct JsonParser_2_t3092672136;
// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Message>
struct List_1_t583975089;
// Library.Code.Domain.Entities.BatchMessages
struct BatchMessages_t2405623082;
// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Message>>
struct ResponseWrapper_1_t2078936153;
// Library.Code.Networking.Message.SendBatchMessageApiImp
struct SendBatchMessageApiImp_t1721429217;
// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model>>
struct ResponseWrapper_1_t3027711747;
// Library.Code.Networking.Models.ModelApiImp
struct ModelApiImp_t398605299;
// Library.Code.Parsers.JsonParser`1<Library.Code.Domain.Entities.MessageTemplateEmbedded>
struct JsonParser_1_t2836814282;
// Library.Code.Parsers.JsonParser`1<Library.Code.Domain.Entities.ModelEmbedded>
struct JsonParser_1_t2798277846;
// Library.Code.Facades.Callback
struct Callback_t1820017589;
// Library.Code.Networking.Files.SendFileApiImpl
struct SendFileApiImpl_t545224088;
// Library.Code.Parsers.JsonParser`1<Library.Code.Domain.Entities.PrefixColor.PrefixColorResponse>
struct JsonParser_1_t2330610323;
// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.ModelGroup>>
struct ResponseWrapper_1_t4098269842;
// Library.Code.Networking.GroupModel.GroupingModelApiImp
struct GroupingModelApiImp_t1621005666;
// System.Collections.Generic.List`1<Library.Code.Domain.Entities.ModelGroup>
struct List_1_t2603308778;
// Library.Code.Networking.GroupModel.GroupingModelApiImp/<loadModelGroupingData>c__Iterator0
struct U3CloadModelGroupingDataU3Ec__Iterator0_t580313732;
// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.MessageTemplate>>
struct ResponseWrapper_1_t3657635747;
// Library.Code.Networking.MessageTemplate.MessageTemplateApiImp
struct MessageTemplateApiImp_t700868765;
// Library.Code.Networking.Message.GroupCreatorApiHoloImp
struct GroupCreatorApiHoloImp_t1244970665;
// Library.Code.Parsers.JsonParser`2<Library.Code.Domain.Entities.GroupCreate1,Library.Code.Domain.Entities.GroupCreateResponse>
struct JsonParser_2_t3852117994;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Void
struct Void_t1841601450;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Text
struct Text_t356221433;
// Library.Code.Facades.ElementHandler.ElementHandlerFacade
struct ElementHandlerFacade_t2821299992;
// Library.Code.UI.PrefabGenerator.PrefabGenerator
struct PrefabGenerator_t3099279183;
// Library.Code.Facades.Elements.ElementFacade
struct ElementFacade_t512443804;
// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Element>
struct List_1_t1645709140;
// Library.Code.DI.PopUp.PopUpShower
struct PopUpShower_t3186557280;
// Library.Code.Facades._3dElementDownload.Element3dFacade
struct Element3dFacade_t3552582347;
// Library.Code.UI.ChooseModel.ModelInterface
struct ModelInterface_t515034102;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;
// UnityEngine.Events.UnityAction`1<Library.Code.Domain.Dtos.ItemStatusWithCheckpoint>
struct UnityAction_1_t3024356692;
// UnityEngine.UI.Image
struct Image_t2042527209;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t934157183;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// Library.Code.Facades.Messages.MessagesFacade
struct MessagesFacade_t3479558102;
// Library.Code.Domain.Entities.Message
struct Message_t1214853957;
// System.Collections.Generic.List`1<PhotoHistory>
struct List_1_t3591931956;
// System.Action`3<UnityEngine.Sprite,System.String,System.String>
struct Action_3_t2024507103;
// UnityEngine.Camera
struct Camera_t189460977;
// Library.Code.Domain.Entities.MessageTemplate
struct MessageTemplate_t2793553551;
// Library.Code.UI.MessageTemplates.FillUpWithMessageTemplate
struct FillUpWithMessageTemplate_t2347927757;
// Library.Code.Facades.Models.ModelFacade
struct ModelFacade_t1760456667;
// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model>
struct List_1_t1532750683;
// Library.Code.Facades.CheckListF.CurrentCheckListItemFacade
struct CurrentCheckListItemFacade_t552048566;
// Library.Code.Facades.CheckListF.CheckListFacade
struct CheckListFacade_t3860198914;
// System.Collections.Generic.List`1<Library.Code.UI.ChecklistUI.ItemStatusRow>
struct List_1_t2159658009;
// Library.Code.Facades.CheckPoints.CheckPointsFacade
struct CheckPointsFacade_t481555209;
// Library.Code.Facades.Elements.ElementApproveStateFacade
struct ElementApproveStateFacade_t2685088950;
// Library.Code.Domain.Entities.Checklist.CheckList
struct CheckList_t4132048078;
// System.Collections.Generic.List`1<Library.Code.Domain.Dtos.CheckPointInfo>
struct List_1_t2717891520;
// Library.Code.UI.ChooseModel.ElementRowHandlerDelegate
struct ElementRowHandlerDelegate_t1173495887;
// Library.Code.Domain.Entities.Element
struct Element_t2276588008;
// Library.Code.UI.PopUp.PopUpsFactory
struct PopUpsFactory_t1333276925;
// Library.Code.UI.ChooseModel.ElementsInterface
struct ElementsInterface_t199981478;
// TMPro.TMP_Dropdown
struct TMP_Dropdown_t1768193147;
// Library.Code.UI.ChooseModel.ModelRowHandlerDelegate
struct ModelRowHandlerDelegate_t2338206932;
// Library.Code.Domain.Entities.Model
struct Model_t2163629551;
// Library.Code.Facades.ModelGrouping.ModelGroupFacade
struct ModelGroupFacade_t1233541942;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t2719087314;
// Library.Code.Domain.Dtos.ModelGroupDto
struct ModelGroupDto_t1808254547;
// System.Collections.Generic.List`1<Library.Code.UI.ElementStatus.ApproveElementRow>
struct List_1_t3317923727;
// System.Action`1<Library.Code.Domain.Entities.ApproveElementResponse>
struct Action_1_t2567571898;
// PhotoPopup
struct PhotoPopup_t4101423206;
// UnityEngine.UI.InputField
struct InputField_t1631627530;
// Library.Code.Facades.MessageTemplates.MessageTemplatesFacade
struct MessageTemplatesFacade_t4086936850;
// Library.Code.Facades.Messages.SendMessageFacade
struct SendMessageFacade_t1083868397;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef SENDFILEAPIIMPL_T545224088_H
#define SENDFILEAPIIMPL_T545224088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Files.SendFileApiImpl
struct  SendFileApiImpl_t545224088  : public RuntimeObject
{
public:
	// CI.HttpClient.HttpClient Library.Code.Networking.Files.SendFileApiImpl::client
	HttpClient_t2113786063 * ___client_0;
	// System.String Library.Code.Networking.Files.SendFileApiImpl::url
	String_t* ___url_1;
	// Library.Code.WebRequest.UserDataNetworkInfoProvider Library.Code.Networking.Files.SendFileApiImpl::_dataNetworkInfoProvider
	RuntimeObject* ____dataNetworkInfoProvider_2;

public:
	inline static int32_t get_offset_of_client_0() { return static_cast<int32_t>(offsetof(SendFileApiImpl_t545224088, ___client_0)); }
	inline HttpClient_t2113786063 * get_client_0() const { return ___client_0; }
	inline HttpClient_t2113786063 ** get_address_of_client_0() { return &___client_0; }
	inline void set_client_0(HttpClient_t2113786063 * value)
	{
		___client_0 = value;
		Il2CppCodeGenWriteBarrier((&___client_0), value);
	}

	inline static int32_t get_offset_of_url_1() { return static_cast<int32_t>(offsetof(SendFileApiImpl_t545224088, ___url_1)); }
	inline String_t* get_url_1() const { return ___url_1; }
	inline String_t** get_address_of_url_1() { return &___url_1; }
	inline void set_url_1(String_t* value)
	{
		___url_1 = value;
		Il2CppCodeGenWriteBarrier((&___url_1), value);
	}

	inline static int32_t get_offset_of__dataNetworkInfoProvider_2() { return static_cast<int32_t>(offsetof(SendFileApiImpl_t545224088, ____dataNetworkInfoProvider_2)); }
	inline RuntimeObject* get__dataNetworkInfoProvider_2() const { return ____dataNetworkInfoProvider_2; }
	inline RuntimeObject** get_address_of__dataNetworkInfoProvider_2() { return &____dataNetworkInfoProvider_2; }
	inline void set__dataNetworkInfoProvider_2(RuntimeObject* value)
	{
		____dataNetworkInfoProvider_2 = value;
		Il2CppCodeGenWriteBarrier((&____dataNetworkInfoProvider_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDFILEAPIIMPL_T545224088_H
#ifndef U3CADDROWSU3EC__ANONSTOREY0_T2717169914_H
#define U3CADDROWSU3EC__ANONSTOREY0_T2717169914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ElementStatus.ElementNewStatusChoos/<addRows>c__AnonStorey0
struct  U3CaddRowsU3Ec__AnonStorey0_t2717169914  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.ApproveElementResponse Library.Code.UI.ElementStatus.ElementNewStatusChoos/<addRows>c__AnonStorey0::dto
	ApproveElementResponse_t2765772516 * ___dto_0;
	// Library.Code.UI.ElementStatus.ElementNewStatusChoos Library.Code.UI.ElementStatus.ElementNewStatusChoos/<addRows>c__AnonStorey0::$this
	ElementNewStatusChoos_t3073096760 * ___U24this_1;

public:
	inline static int32_t get_offset_of_dto_0() { return static_cast<int32_t>(offsetof(U3CaddRowsU3Ec__AnonStorey0_t2717169914, ___dto_0)); }
	inline ApproveElementResponse_t2765772516 * get_dto_0() const { return ___dto_0; }
	inline ApproveElementResponse_t2765772516 ** get_address_of_dto_0() { return &___dto_0; }
	inline void set_dto_0(ApproveElementResponse_t2765772516 * value)
	{
		___dto_0 = value;
		Il2CppCodeGenWriteBarrier((&___dto_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CaddRowsU3Ec__AnonStorey0_t2717169914, ___U24this_1)); }
	inline ElementNewStatusChoos_t3073096760 * get_U24this_1() const { return ___U24this_1; }
	inline ElementNewStatusChoos_t3073096760 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ElementNewStatusChoos_t3073096760 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDROWSU3EC__ANONSTOREY0_T2717169914_H
#ifndef U3CADDLISTENERTOBUTTONU3EC__ANONSTOREY0_T962888137_H
#define U3CADDLISTENERTOBUTTONU3EC__ANONSTOREY0_T962888137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ElementStatus.ApproveElementRow/<addListenerToButton>c__AnonStorey0
struct  U3CaddListenerToButtonU3Ec__AnonStorey0_t962888137  : public RuntimeObject
{
public:
	// System.Action`1<Library.Code.UI.ElementStatus.AproveElementDto> Library.Code.UI.ElementStatus.ApproveElementRow/<addListenerToButton>c__AnonStorey0::action
	Action_1_t490573446 * ___action_0;
	// Library.Code.UI.ElementStatus.ApproveElementRow Library.Code.UI.ElementStatus.ApproveElementRow/<addListenerToButton>c__AnonStorey0::$this
	ApproveElementRow_t3948802595 * ___U24this_1;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CaddListenerToButtonU3Ec__AnonStorey0_t962888137, ___action_0)); }
	inline Action_1_t490573446 * get_action_0() const { return ___action_0; }
	inline Action_1_t490573446 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t490573446 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CaddListenerToButtonU3Ec__AnonStorey0_t962888137, ___U24this_1)); }
	inline ApproveElementRow_t3948802595 * get_U24this_1() const { return ___U24this_1; }
	inline ApproveElementRow_t3948802595 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ApproveElementRow_t3948802595 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDLISTENERTOBUTTONU3EC__ANONSTOREY0_T962888137_H
#ifndef U3CADDMODELSU3EC__ANONSTOREY0_T2567043122_H
#define U3CADDMODELSU3EC__ANONSTOREY0_T2567043122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ChooseModel.ModelObserverNew/<addModels>c__AnonStorey0
struct  U3CaddModelsU3Ec__AnonStorey0_t2567043122  : public RuntimeObject
{
public:
	// UnityEngine.Transform Library.Code.UI.ChooseModel.ModelObserverNew/<addModels>c__AnonStorey0::contentTransform
	Transform_t3275118058 * ___contentTransform_0;
	// Library.Code.UI.ChooseModel.ModelObserverNew Library.Code.UI.ChooseModel.ModelObserverNew/<addModels>c__AnonStorey0::$this
	ModelObserverNew_t1299566747 * ___U24this_1;

public:
	inline static int32_t get_offset_of_contentTransform_0() { return static_cast<int32_t>(offsetof(U3CaddModelsU3Ec__AnonStorey0_t2567043122, ___contentTransform_0)); }
	inline Transform_t3275118058 * get_contentTransform_0() const { return ___contentTransform_0; }
	inline Transform_t3275118058 ** get_address_of_contentTransform_0() { return &___contentTransform_0; }
	inline void set_contentTransform_0(Transform_t3275118058 * value)
	{
		___contentTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___contentTransform_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CaddModelsU3Ec__AnonStorey0_t2567043122, ___U24this_1)); }
	inline ModelObserverNew_t1299566747 * get_U24this_1() const { return ___U24this_1; }
	inline ModelObserverNew_t1299566747 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ModelObserverNew_t1299566747 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDMODELSU3EC__ANONSTOREY0_T2567043122_H
#ifndef U3CADDMODELSU3EC__ANONSTOREY0_T3035878898_H
#define U3CADDMODELSU3EC__ANONSTOREY0_T3035878898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ChooseModel.ModelObserver/<addModels>c__AnonStorey0
struct  U3CaddModelsU3Ec__AnonStorey0_t3035878898  : public RuntimeObject
{
public:
	// UnityEngine.Transform Library.Code.UI.ChooseModel.ModelObserver/<addModels>c__AnonStorey0::contentTransform
	Transform_t3275118058 * ___contentTransform_0;
	// Library.Code.UI.ChooseModel.ModelObserver Library.Code.UI.ChooseModel.ModelObserver/<addModels>c__AnonStorey0::$this
	ModelObserver_t1172265923 * ___U24this_1;

public:
	inline static int32_t get_offset_of_contentTransform_0() { return static_cast<int32_t>(offsetof(U3CaddModelsU3Ec__AnonStorey0_t3035878898, ___contentTransform_0)); }
	inline Transform_t3275118058 * get_contentTransform_0() const { return ___contentTransform_0; }
	inline Transform_t3275118058 ** get_address_of_contentTransform_0() { return &___contentTransform_0; }
	inline void set_contentTransform_0(Transform_t3275118058 * value)
	{
		___contentTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___contentTransform_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CaddModelsU3Ec__AnonStorey0_t3035878898, ___U24this_1)); }
	inline ModelObserver_t1172265923 * get_U24this_1() const { return ___U24this_1; }
	inline ModelObserver_t1172265923 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ModelObserver_t1172265923 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDMODELSU3EC__ANONSTOREY0_T3035878898_H
#ifndef U3CUPDATEOBSERVERU3EC__ANONSTOREY1_T2240497877_H
#define U3CUPDATEOBSERVERU3EC__ANONSTOREY1_T2240497877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModelElementObserver/<updateObserver>c__AnonStorey1
struct  U3CupdateObserverU3Ec__AnonStorey1_t2240497877  : public RuntimeObject
{
public:
	// UnityEngine.Transform ModelElementObserver/<updateObserver>c__AnonStorey1::contentTransform
	Transform_t3275118058 * ___contentTransform_0;
	// ModelElementObserver ModelElementObserver/<updateObserver>c__AnonStorey1::$this
	ModelElementObserver_t2747689887 * ___U24this_1;

public:
	inline static int32_t get_offset_of_contentTransform_0() { return static_cast<int32_t>(offsetof(U3CupdateObserverU3Ec__AnonStorey1_t2240497877, ___contentTransform_0)); }
	inline Transform_t3275118058 * get_contentTransform_0() const { return ___contentTransform_0; }
	inline Transform_t3275118058 ** get_address_of_contentTransform_0() { return &___contentTransform_0; }
	inline void set_contentTransform_0(Transform_t3275118058 * value)
	{
		___contentTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___contentTransform_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CupdateObserverU3Ec__AnonStorey1_t2240497877, ___U24this_1)); }
	inline ModelElementObserver_t2747689887 * get_U24this_1() const { return ___U24this_1; }
	inline ModelElementObserver_t2747689887 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ModelElementObserver_t2747689887 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEOBSERVERU3EC__ANONSTOREY1_T2240497877_H
#ifndef U3CUPDATEOBSERVERU3EC__ANONSTOREY0_T2240497878_H
#define U3CUPDATEOBSERVERU3EC__ANONSTOREY0_T2240497878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModelElementObserver/<updateObserver>c__AnonStorey0
struct  U3CupdateObserverU3Ec__AnonStorey0_t2240497878  : public RuntimeObject
{
public:
	// UnityEngine.Transform ModelElementObserver/<updateObserver>c__AnonStorey0::contentTransform
	Transform_t3275118058 * ___contentTransform_0;
	// ModelElementObserver ModelElementObserver/<updateObserver>c__AnonStorey0::$this
	ModelElementObserver_t2747689887 * ___U24this_1;

public:
	inline static int32_t get_offset_of_contentTransform_0() { return static_cast<int32_t>(offsetof(U3CupdateObserverU3Ec__AnonStorey0_t2240497878, ___contentTransform_0)); }
	inline Transform_t3275118058 * get_contentTransform_0() const { return ___contentTransform_0; }
	inline Transform_t3275118058 ** get_address_of_contentTransform_0() { return &___contentTransform_0; }
	inline void set_contentTransform_0(Transform_t3275118058 * value)
	{
		___contentTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___contentTransform_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CupdateObserverU3Ec__AnonStorey0_t2240497878, ___U24this_1)); }
	inline ModelElementObserver_t2747689887 * get_U24this_1() const { return ___U24this_1; }
	inline ModelElementObserver_t2747689887 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ModelElementObserver_t2747689887 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEOBSERVERU3EC__ANONSTOREY0_T2240497878_H
#ifndef U3CADDELEMENTSU3EC__ANONSTOREY0_T2713806289_H
#define U3CADDELEMENTSU3EC__ANONSTOREY0_T2713806289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ChooseModel.ElementsObserverNew/<addElements>c__AnonStorey0
struct  U3CaddElementsU3Ec__AnonStorey0_t2713806289  : public RuntimeObject
{
public:
	// UnityEngine.Transform Library.Code.UI.ChooseModel.ElementsObserverNew/<addElements>c__AnonStorey0::contentTransform
	Transform_t3275118058 * ___contentTransform_0;
	// Library.Code.UI.ChooseModel.ElementsObserverNew Library.Code.UI.ChooseModel.ElementsObserverNew/<addElements>c__AnonStorey0::$this
	ElementsObserverNew_t2308744357 * ___U24this_1;

public:
	inline static int32_t get_offset_of_contentTransform_0() { return static_cast<int32_t>(offsetof(U3CaddElementsU3Ec__AnonStorey0_t2713806289, ___contentTransform_0)); }
	inline Transform_t3275118058 * get_contentTransform_0() const { return ___contentTransform_0; }
	inline Transform_t3275118058 ** get_address_of_contentTransform_0() { return &___contentTransform_0; }
	inline void set_contentTransform_0(Transform_t3275118058 * value)
	{
		___contentTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___contentTransform_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CaddElementsU3Ec__AnonStorey0_t2713806289, ___U24this_1)); }
	inline ElementsObserverNew_t2308744357 * get_U24this_1() const { return ___U24this_1; }
	inline ElementsObserverNew_t2308744357 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ElementsObserverNew_t2308744357 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDELEMENTSU3EC__ANONSTOREY0_T2713806289_H
#ifndef U3CADDELEMENTSU3EC__ANONSTOREY0_T2043651465_H
#define U3CADDELEMENTSU3EC__ANONSTOREY0_T2043651465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ChooseModel.ElementsObserver/<addElements>c__AnonStorey0
struct  U3CaddElementsU3Ec__AnonStorey0_t2043651465  : public RuntimeObject
{
public:
	// UnityEngine.Transform Library.Code.UI.ChooseModel.ElementsObserver/<addElements>c__AnonStorey0::contentTransform
	Transform_t3275118058 * ___contentTransform_0;
	// Library.Code.UI.ChooseModel.ElementsObserver Library.Code.UI.ChooseModel.ElementsObserver/<addElements>c__AnonStorey0::$this
	ElementsObserver_t3382904053 * ___U24this_1;

public:
	inline static int32_t get_offset_of_contentTransform_0() { return static_cast<int32_t>(offsetof(U3CaddElementsU3Ec__AnonStorey0_t2043651465, ___contentTransform_0)); }
	inline Transform_t3275118058 * get_contentTransform_0() const { return ___contentTransform_0; }
	inline Transform_t3275118058 ** get_address_of_contentTransform_0() { return &___contentTransform_0; }
	inline void set_contentTransform_0(Transform_t3275118058 * value)
	{
		___contentTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___contentTransform_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CaddElementsU3Ec__AnonStorey0_t2043651465, ___U24this_1)); }
	inline ElementsObserver_t3382904053 * get_U24this_1() const { return ___U24this_1; }
	inline ElementsObserver_t3382904053 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ElementsObserver_t3382904053 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDELEMENTSU3EC__ANONSTOREY0_T2043651465_H
#ifndef U3CSETITEMSTATUSU3EC__ANONSTOREY0_T1847096850_H
#define U3CSETITEMSTATUSU3EC__ANONSTOREY0_T1847096850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ChecklistUI.ItemStatusRow/<setItemStatus>c__AnonStorey0
struct  U3CsetItemStatusU3Ec__AnonStorey0_t1847096850  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Checklist.ItemStatusDto Library.Code.UI.ChecklistUI.ItemStatusRow/<setItemStatus>c__AnonStorey0::itemStatusDto
	ItemStatusDto_t3640770036 * ___itemStatusDto_0;
	// UnityEngine.Events.UnityAction`1<Library.Code.Domain.Entities.Checklist.ItemStatusDto> Library.Code.UI.ChecklistUI.ItemStatusRow/<setItemStatus>c__AnonStorey0::actionOnClick
	UnityAction_1_t712388491 * ___actionOnClick_1;
	// Library.Code.UI.ChecklistUI.ItemStatusRow Library.Code.UI.ChecklistUI.ItemStatusRow/<setItemStatus>c__AnonStorey0::$this
	ItemStatusRow_t2790536877 * ___U24this_2;

public:
	inline static int32_t get_offset_of_itemStatusDto_0() { return static_cast<int32_t>(offsetof(U3CsetItemStatusU3Ec__AnonStorey0_t1847096850, ___itemStatusDto_0)); }
	inline ItemStatusDto_t3640770036 * get_itemStatusDto_0() const { return ___itemStatusDto_0; }
	inline ItemStatusDto_t3640770036 ** get_address_of_itemStatusDto_0() { return &___itemStatusDto_0; }
	inline void set_itemStatusDto_0(ItemStatusDto_t3640770036 * value)
	{
		___itemStatusDto_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStatusDto_0), value);
	}

	inline static int32_t get_offset_of_actionOnClick_1() { return static_cast<int32_t>(offsetof(U3CsetItemStatusU3Ec__AnonStorey0_t1847096850, ___actionOnClick_1)); }
	inline UnityAction_1_t712388491 * get_actionOnClick_1() const { return ___actionOnClick_1; }
	inline UnityAction_1_t712388491 ** get_address_of_actionOnClick_1() { return &___actionOnClick_1; }
	inline void set_actionOnClick_1(UnityAction_1_t712388491 * value)
	{
		___actionOnClick_1 = value;
		Il2CppCodeGenWriteBarrier((&___actionOnClick_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CsetItemStatusU3Ec__AnonStorey0_t1847096850, ___U24this_2)); }
	inline ItemStatusRow_t2790536877 * get_U24this_2() const { return ___U24this_2; }
	inline ItemStatusRow_t2790536877 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ItemStatusRow_t2790536877 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETITEMSTATUSU3EC__ANONSTOREY0_T1847096850_H
#ifndef U3CADDROWSU3EC__ANONSTOREY1_T2365418681_H
#define U3CADDROWSU3EC__ANONSTOREY1_T2365418681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ElementStatus.ElementNewStatusChoos/<addRows>c__AnonStorey0/<addRows>c__AnonStorey1
struct  U3CaddRowsU3Ec__AnonStorey1_t2365418681  : public RuntimeObject
{
public:
	// Library.Code.UI.ElementStatus.AproveElementDto Library.Code.UI.ElementStatus.ElementNewStatusChoos/<addRows>c__AnonStorey0/<addRows>c__AnonStorey1::it
	AproveElementDto_t688774064 * ___it_0;
	// Library.Code.UI.ElementStatus.ElementNewStatusChoos/<addRows>c__AnonStorey0 Library.Code.UI.ElementStatus.ElementNewStatusChoos/<addRows>c__AnonStorey0/<addRows>c__AnonStorey1::<>f__ref$0
	U3CaddRowsU3Ec__AnonStorey0_t2717169914 * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of_it_0() { return static_cast<int32_t>(offsetof(U3CaddRowsU3Ec__AnonStorey1_t2365418681, ___it_0)); }
	inline AproveElementDto_t688774064 * get_it_0() const { return ___it_0; }
	inline AproveElementDto_t688774064 ** get_address_of_it_0() { return &___it_0; }
	inline void set_it_0(AproveElementDto_t688774064 * value)
	{
		___it_0 = value;
		Il2CppCodeGenWriteBarrier((&___it_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CaddRowsU3Ec__AnonStorey1_t2365418681, ___U3CU3Ef__refU240_1)); }
	inline U3CaddRowsU3Ec__AnonStorey0_t2717169914 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CaddRowsU3Ec__AnonStorey0_t2717169914 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CaddRowsU3Ec__AnonStorey0_t2717169914 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDROWSU3EC__ANONSTOREY1_T2365418681_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef SANDBOXPOPUPFACTORY_T63848479_H
#define SANDBOXPOPUPFACTORY_T63848479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.PopUp.SandboxPopUpFactory
struct  SandboxPopUpFactory_t63848479  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Library.Code.UI.PopUp.SandboxPopUpFactory::_gameObject
	GameObject_t1756533147 * ____gameObject_0;
	// System.Collections.Generic.Dictionary`2<Library.Code.UI.PopUp.PopUps,Library.Code.UI.PopUp.PopUpHandler> Library.Code.UI.PopUp.SandboxPopUpFactory::popUpHandlers
	Dictionary_2_t1112370778 * ___popUpHandlers_1;

public:
	inline static int32_t get_offset_of__gameObject_0() { return static_cast<int32_t>(offsetof(SandboxPopUpFactory_t63848479, ____gameObject_0)); }
	inline GameObject_t1756533147 * get__gameObject_0() const { return ____gameObject_0; }
	inline GameObject_t1756533147 ** get_address_of__gameObject_0() { return &____gameObject_0; }
	inline void set__gameObject_0(GameObject_t1756533147 * value)
	{
		____gameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&____gameObject_0), value);
	}

	inline static int32_t get_offset_of_popUpHandlers_1() { return static_cast<int32_t>(offsetof(SandboxPopUpFactory_t63848479, ___popUpHandlers_1)); }
	inline Dictionary_2_t1112370778 * get_popUpHandlers_1() const { return ___popUpHandlers_1; }
	inline Dictionary_2_t1112370778 ** get_address_of_popUpHandlers_1() { return &___popUpHandlers_1; }
	inline void set_popUpHandlers_1(Dictionary_2_t1112370778 * value)
	{
		___popUpHandlers_1 = value;
		Il2CppCodeGenWriteBarrier((&___popUpHandlers_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SANDBOXPOPUPFACTORY_T63848479_H
#ifndef POPUPHANDLER_T3865200444_H
#define POPUPHANDLER_T3865200444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.PopUp.PopUpHandler
struct  PopUpHandler_t3865200444  : public RuntimeObject
{
public:
	// UnityEngine.Transform Library.Code.UI.PopUp.PopUpHandler::backgroundTransform
	Transform_t3275118058 * ___backgroundTransform_0;
	// UnityEngine.GameObject Library.Code.UI.PopUp.PopUpHandler::gameObject
	GameObject_t1756533147 * ___gameObject_1;

public:
	inline static int32_t get_offset_of_backgroundTransform_0() { return static_cast<int32_t>(offsetof(PopUpHandler_t3865200444, ___backgroundTransform_0)); }
	inline Transform_t3275118058 * get_backgroundTransform_0() const { return ___backgroundTransform_0; }
	inline Transform_t3275118058 ** get_address_of_backgroundTransform_0() { return &___backgroundTransform_0; }
	inline void set_backgroundTransform_0(Transform_t3275118058 * value)
	{
		___backgroundTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundTransform_0), value);
	}

	inline static int32_t get_offset_of_gameObject_1() { return static_cast<int32_t>(offsetof(PopUpHandler_t3865200444, ___gameObject_1)); }
	inline GameObject_t1756533147 * get_gameObject_1() const { return ___gameObject_1; }
	inline GameObject_t1756533147 ** get_address_of_gameObject_1() { return &___gameObject_1; }
	inline void set_gameObject_1(GameObject_t1756533147 * value)
	{
		___gameObject_1 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPUPHANDLER_T3865200444_H
#ifndef U3CHANDLEPOPUPU3EC__ANONSTOREY0_T2649938024_H
#define U3CHANDLEPOPUPU3EC__ANONSTOREY0_T2649938024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.PopUp.AcceptPopUp/<handlePopUp>c__AnonStorey0
struct  U3ChandlePopUpU3Ec__AnonStorey0_t2649938024  : public RuntimeObject
{
public:
	// Library.Code.UI.PopUp.PopUpStuff Library.Code.UI.PopUp.AcceptPopUp/<handlePopUp>c__AnonStorey0::popUpStuff
	PopUpStuff_t3426923330 * ___popUpStuff_0;

public:
	inline static int32_t get_offset_of_popUpStuff_0() { return static_cast<int32_t>(offsetof(U3ChandlePopUpU3Ec__AnonStorey0_t2649938024, ___popUpStuff_0)); }
	inline PopUpStuff_t3426923330 * get_popUpStuff_0() const { return ___popUpStuff_0; }
	inline PopUpStuff_t3426923330 ** get_address_of_popUpStuff_0() { return &___popUpStuff_0; }
	inline void set_popUpStuff_0(PopUpStuff_t3426923330 * value)
	{
		___popUpStuff_0 = value;
		Il2CppCodeGenWriteBarrier((&___popUpStuff_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHANDLEPOPUPU3EC__ANONSTOREY0_T2649938024_H
#ifndef U3CANCHORACTIVATEU3EC__ITERATOR0_T1705974996_H
#define U3CANCHORACTIVATEU3EC__ITERATOR0_T1705974996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MessageTemplates.Remark/<AnchorActivate>c__Iterator0
struct  U3CAnchorActivateU3Ec__Iterator0_t1705974996  : public RuntimeObject
{
public:
	// Library.Code.UI.MessageTemplates.Remark Library.Code.UI.MessageTemplates.Remark/<AnchorActivate>c__Iterator0::$this
	Remark_t2047647426 * ___U24this_0;
	// System.Object Library.Code.UI.MessageTemplates.Remark/<AnchorActivate>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Library.Code.UI.MessageTemplates.Remark/<AnchorActivate>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Library.Code.UI.MessageTemplates.Remark/<AnchorActivate>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CAnchorActivateU3Ec__Iterator0_t1705974996, ___U24this_0)); }
	inline Remark_t2047647426 * get_U24this_0() const { return ___U24this_0; }
	inline Remark_t2047647426 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Remark_t2047647426 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CAnchorActivateU3Ec__Iterator0_t1705974996, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CAnchorActivateU3Ec__Iterator0_t1705974996, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CAnchorActivateU3Ec__Iterator0_t1705974996, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANCHORACTIVATEU3EC__ITERATOR0_T1705974996_H
#ifndef NEWMESSAGELISTBUILDER_T2354248106_H
#define NEWMESSAGELISTBUILDER_T2354248106_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MessagesList.New.NewMessageListBuilder
struct  NewMessageListBuilder_t2354248106  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWMESSAGELISTBUILDER_T2354248106_H
#ifndef MESSAGELISTBUILDERSAVEDREMARKS_T2061646460_H
#define MESSAGELISTBUILDERSAVEDREMARKS_T2061646460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MessagesList.New.MessageListBuilderSavedRemarks
struct  MessageListBuilderSavedRemarks_t2061646460  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGELISTBUILDERSAVEDREMARKS_T2061646460_H
#ifndef MESSAGELISTBUILDER_T1028067940_H
#define MESSAGELISTBUILDER_T1028067940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MessagesList.MessageListBuilder
struct  MessageListBuilder_t1028067940  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGELISTBUILDER_T1028067940_H
#ifndef LANGUAGESTRINGS_T1206686884_H
#define LANGUAGESTRINGS_T1206686884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LanguageStrings
struct  LanguageStrings_t1206686884  : public RuntimeObject
{
public:
	// System.String LanguageStrings::menuButtonChecklist
	String_t* ___menuButtonChecklist_0;
	// System.String LanguageStrings::menuButtonLayers
	String_t* ___menuButtonLayers_1;
	// System.String LanguageStrings::menuButtonAddRemarks
	String_t* ___menuButtonAddRemarks_2;
	// System.String LanguageStrings::menuButtonSavedRemarks
	String_t* ___menuButtonSavedRemarks_3;
	// System.String LanguageStrings::menuButtonSelectModelElement
	String_t* ___menuButtonSelectModelElement_4;
	// System.String LanguageStrings::checklistAcceptState
	String_t* ___checklistAcceptState_5;
	// System.String LanguageStrings::checklistUntouchedState
	String_t* ___checklistUntouchedState_6;
	// System.String LanguageStrings::checklistRejectState
	String_t* ___checklistRejectState_7;
	// System.String LanguageStrings::checklistApproveButton
	String_t* ___checklistApproveButton_8;
	// System.String LanguageStrings::layersLabel
	String_t* ___layersLabel_9;
	// System.String LanguageStrings::addRemarksLabel
	String_t* ___addRemarksLabel_10;
	// System.String LanguageStrings::addRemarksCancelButton
	String_t* ___addRemarksCancelButton_11;
	// System.String LanguageStrings::addRemarksSaveButton
	String_t* ___addRemarksSaveButton_12;
	// System.String LanguageStrings::addRemarksEmptyChecklistButton
	String_t* ___addRemarksEmptyChecklistButton_13;
	// System.String LanguageStrings::savedRemarksLabel
	String_t* ___savedRemarksLabel_14;
	// System.String LanguageStrings::savedRemarksUploadButton
	String_t* ___savedRemarksUploadButton_15;
	// System.String LanguageStrings::selectMELabel
	String_t* ___selectMELabel_16;
	// System.String LanguageStrings::selectMESearchModels
	String_t* ___selectMESearchModels_17;
	// System.String LanguageStrings::selectMESearchElements
	String_t* ___selectMESearchElements_18;
	// System.String LanguageStrings::photoPopupDescription
	String_t* ___photoPopupDescription_19;
	// System.String LanguageStrings::photoPopupPhotoHistory
	String_t* ___photoPopupPhotoHistory_20;
	// System.String LanguageStrings::photoPopupDeleteButton
	String_t* ___photoPopupDeleteButton_21;
	// System.String LanguageStrings::photoPopupAddButton
	String_t* ___photoPopupAddButton_22;
	// System.String LanguageStrings::modelLoadingTitle
	String_t* ___modelLoadingTitle_23;
	// System.String LanguageStrings::modelLoadingLoading
	String_t* ___modelLoadingLoading_24;
	// System.String LanguageStrings::modelLoadingSuccess
	String_t* ___modelLoadingSuccess_25;
	// System.String LanguageStrings::modelLoadingSuccessMessage
	String_t* ___modelLoadingSuccessMessage_26;
	// System.String LanguageStrings::modelLoadingPartialSuccess
	String_t* ___modelLoadingPartialSuccess_27;
	// System.String LanguageStrings::modelLoadingPartialSuccessMessage
	String_t* ___modelLoadingPartialSuccessMessage_28;
	// System.String LanguageStrings::uploadMessagesTitle
	String_t* ___uploadMessagesTitle_29;
	// System.String LanguageStrings::uploadMessages
	String_t* ___uploadMessages_30;
	// System.String LanguageStrings::uploadMessagesFailure
	String_t* ___uploadMessagesFailure_31;
	// System.String LanguageStrings::approveStateTitle
	String_t* ___approveStateTitle_32;
	// System.String LanguageStrings::approveStateQuestion
	String_t* ___approveStateQuestion_33;
	// System.String LanguageStrings::approveStateStatusTitle
	String_t* ___approveStateStatusTitle_34;
	// System.String LanguageStrings::approveStateStatusChanging
	String_t* ___approveStateStatusChanging_35;
	// System.String LanguageStrings::approveStateStatusChangeFailure
	String_t* ___approveStateStatusChangeFailure_36;
	// System.String LanguageStrings::approveStateStatusChangeFailureReason
	String_t* ___approveStateStatusChangeFailureReason_37;
	// System.String LanguageStrings::approveStateRemarkSaveFailure
	String_t* ___approveStateRemarkSaveFailure_38;
	// System.String LanguageStrings::statusChangeTitle
	String_t* ___statusChangeTitle_39;
	// System.String LanguageStrings::statusChange
	String_t* ___statusChange_40;
	// System.String LanguageStrings::statusChangeFailure
	String_t* ___statusChangeFailure_41;
	// System.String LanguageStrings::statusChooseLabel
	String_t* ___statusChooseLabel_42;
	// System.String LanguageStrings::statusChooseOKButton
	String_t* ___statusChooseOKButton_43;
	// System.String LanguageStrings::statusChooseCancelButton
	String_t* ___statusChooseCancelButton_44;
	// System.String LanguageStrings::deleteMessageTitle
	String_t* ___deleteMessageTitle_45;
	// System.String LanguageStrings::deleteMessageQuestion
	String_t* ___deleteMessageQuestion_46;

public:
	inline static int32_t get_offset_of_menuButtonChecklist_0() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___menuButtonChecklist_0)); }
	inline String_t* get_menuButtonChecklist_0() const { return ___menuButtonChecklist_0; }
	inline String_t** get_address_of_menuButtonChecklist_0() { return &___menuButtonChecklist_0; }
	inline void set_menuButtonChecklist_0(String_t* value)
	{
		___menuButtonChecklist_0 = value;
		Il2CppCodeGenWriteBarrier((&___menuButtonChecklist_0), value);
	}

	inline static int32_t get_offset_of_menuButtonLayers_1() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___menuButtonLayers_1)); }
	inline String_t* get_menuButtonLayers_1() const { return ___menuButtonLayers_1; }
	inline String_t** get_address_of_menuButtonLayers_1() { return &___menuButtonLayers_1; }
	inline void set_menuButtonLayers_1(String_t* value)
	{
		___menuButtonLayers_1 = value;
		Il2CppCodeGenWriteBarrier((&___menuButtonLayers_1), value);
	}

	inline static int32_t get_offset_of_menuButtonAddRemarks_2() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___menuButtonAddRemarks_2)); }
	inline String_t* get_menuButtonAddRemarks_2() const { return ___menuButtonAddRemarks_2; }
	inline String_t** get_address_of_menuButtonAddRemarks_2() { return &___menuButtonAddRemarks_2; }
	inline void set_menuButtonAddRemarks_2(String_t* value)
	{
		___menuButtonAddRemarks_2 = value;
		Il2CppCodeGenWriteBarrier((&___menuButtonAddRemarks_2), value);
	}

	inline static int32_t get_offset_of_menuButtonSavedRemarks_3() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___menuButtonSavedRemarks_3)); }
	inline String_t* get_menuButtonSavedRemarks_3() const { return ___menuButtonSavedRemarks_3; }
	inline String_t** get_address_of_menuButtonSavedRemarks_3() { return &___menuButtonSavedRemarks_3; }
	inline void set_menuButtonSavedRemarks_3(String_t* value)
	{
		___menuButtonSavedRemarks_3 = value;
		Il2CppCodeGenWriteBarrier((&___menuButtonSavedRemarks_3), value);
	}

	inline static int32_t get_offset_of_menuButtonSelectModelElement_4() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___menuButtonSelectModelElement_4)); }
	inline String_t* get_menuButtonSelectModelElement_4() const { return ___menuButtonSelectModelElement_4; }
	inline String_t** get_address_of_menuButtonSelectModelElement_4() { return &___menuButtonSelectModelElement_4; }
	inline void set_menuButtonSelectModelElement_4(String_t* value)
	{
		___menuButtonSelectModelElement_4 = value;
		Il2CppCodeGenWriteBarrier((&___menuButtonSelectModelElement_4), value);
	}

	inline static int32_t get_offset_of_checklistAcceptState_5() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___checklistAcceptState_5)); }
	inline String_t* get_checklistAcceptState_5() const { return ___checklistAcceptState_5; }
	inline String_t** get_address_of_checklistAcceptState_5() { return &___checklistAcceptState_5; }
	inline void set_checklistAcceptState_5(String_t* value)
	{
		___checklistAcceptState_5 = value;
		Il2CppCodeGenWriteBarrier((&___checklistAcceptState_5), value);
	}

	inline static int32_t get_offset_of_checklistUntouchedState_6() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___checklistUntouchedState_6)); }
	inline String_t* get_checklistUntouchedState_6() const { return ___checklistUntouchedState_6; }
	inline String_t** get_address_of_checklistUntouchedState_6() { return &___checklistUntouchedState_6; }
	inline void set_checklistUntouchedState_6(String_t* value)
	{
		___checklistUntouchedState_6 = value;
		Il2CppCodeGenWriteBarrier((&___checklistUntouchedState_6), value);
	}

	inline static int32_t get_offset_of_checklistRejectState_7() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___checklistRejectState_7)); }
	inline String_t* get_checklistRejectState_7() const { return ___checklistRejectState_7; }
	inline String_t** get_address_of_checklistRejectState_7() { return &___checklistRejectState_7; }
	inline void set_checklistRejectState_7(String_t* value)
	{
		___checklistRejectState_7 = value;
		Il2CppCodeGenWriteBarrier((&___checklistRejectState_7), value);
	}

	inline static int32_t get_offset_of_checklistApproveButton_8() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___checklistApproveButton_8)); }
	inline String_t* get_checklistApproveButton_8() const { return ___checklistApproveButton_8; }
	inline String_t** get_address_of_checklistApproveButton_8() { return &___checklistApproveButton_8; }
	inline void set_checklistApproveButton_8(String_t* value)
	{
		___checklistApproveButton_8 = value;
		Il2CppCodeGenWriteBarrier((&___checklistApproveButton_8), value);
	}

	inline static int32_t get_offset_of_layersLabel_9() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___layersLabel_9)); }
	inline String_t* get_layersLabel_9() const { return ___layersLabel_9; }
	inline String_t** get_address_of_layersLabel_9() { return &___layersLabel_9; }
	inline void set_layersLabel_9(String_t* value)
	{
		___layersLabel_9 = value;
		Il2CppCodeGenWriteBarrier((&___layersLabel_9), value);
	}

	inline static int32_t get_offset_of_addRemarksLabel_10() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___addRemarksLabel_10)); }
	inline String_t* get_addRemarksLabel_10() const { return ___addRemarksLabel_10; }
	inline String_t** get_address_of_addRemarksLabel_10() { return &___addRemarksLabel_10; }
	inline void set_addRemarksLabel_10(String_t* value)
	{
		___addRemarksLabel_10 = value;
		Il2CppCodeGenWriteBarrier((&___addRemarksLabel_10), value);
	}

	inline static int32_t get_offset_of_addRemarksCancelButton_11() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___addRemarksCancelButton_11)); }
	inline String_t* get_addRemarksCancelButton_11() const { return ___addRemarksCancelButton_11; }
	inline String_t** get_address_of_addRemarksCancelButton_11() { return &___addRemarksCancelButton_11; }
	inline void set_addRemarksCancelButton_11(String_t* value)
	{
		___addRemarksCancelButton_11 = value;
		Il2CppCodeGenWriteBarrier((&___addRemarksCancelButton_11), value);
	}

	inline static int32_t get_offset_of_addRemarksSaveButton_12() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___addRemarksSaveButton_12)); }
	inline String_t* get_addRemarksSaveButton_12() const { return ___addRemarksSaveButton_12; }
	inline String_t** get_address_of_addRemarksSaveButton_12() { return &___addRemarksSaveButton_12; }
	inline void set_addRemarksSaveButton_12(String_t* value)
	{
		___addRemarksSaveButton_12 = value;
		Il2CppCodeGenWriteBarrier((&___addRemarksSaveButton_12), value);
	}

	inline static int32_t get_offset_of_addRemarksEmptyChecklistButton_13() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___addRemarksEmptyChecklistButton_13)); }
	inline String_t* get_addRemarksEmptyChecklistButton_13() const { return ___addRemarksEmptyChecklistButton_13; }
	inline String_t** get_address_of_addRemarksEmptyChecklistButton_13() { return &___addRemarksEmptyChecklistButton_13; }
	inline void set_addRemarksEmptyChecklistButton_13(String_t* value)
	{
		___addRemarksEmptyChecklistButton_13 = value;
		Il2CppCodeGenWriteBarrier((&___addRemarksEmptyChecklistButton_13), value);
	}

	inline static int32_t get_offset_of_savedRemarksLabel_14() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___savedRemarksLabel_14)); }
	inline String_t* get_savedRemarksLabel_14() const { return ___savedRemarksLabel_14; }
	inline String_t** get_address_of_savedRemarksLabel_14() { return &___savedRemarksLabel_14; }
	inline void set_savedRemarksLabel_14(String_t* value)
	{
		___savedRemarksLabel_14 = value;
		Il2CppCodeGenWriteBarrier((&___savedRemarksLabel_14), value);
	}

	inline static int32_t get_offset_of_savedRemarksUploadButton_15() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___savedRemarksUploadButton_15)); }
	inline String_t* get_savedRemarksUploadButton_15() const { return ___savedRemarksUploadButton_15; }
	inline String_t** get_address_of_savedRemarksUploadButton_15() { return &___savedRemarksUploadButton_15; }
	inline void set_savedRemarksUploadButton_15(String_t* value)
	{
		___savedRemarksUploadButton_15 = value;
		Il2CppCodeGenWriteBarrier((&___savedRemarksUploadButton_15), value);
	}

	inline static int32_t get_offset_of_selectMELabel_16() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___selectMELabel_16)); }
	inline String_t* get_selectMELabel_16() const { return ___selectMELabel_16; }
	inline String_t** get_address_of_selectMELabel_16() { return &___selectMELabel_16; }
	inline void set_selectMELabel_16(String_t* value)
	{
		___selectMELabel_16 = value;
		Il2CppCodeGenWriteBarrier((&___selectMELabel_16), value);
	}

	inline static int32_t get_offset_of_selectMESearchModels_17() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___selectMESearchModels_17)); }
	inline String_t* get_selectMESearchModels_17() const { return ___selectMESearchModels_17; }
	inline String_t** get_address_of_selectMESearchModels_17() { return &___selectMESearchModels_17; }
	inline void set_selectMESearchModels_17(String_t* value)
	{
		___selectMESearchModels_17 = value;
		Il2CppCodeGenWriteBarrier((&___selectMESearchModels_17), value);
	}

	inline static int32_t get_offset_of_selectMESearchElements_18() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___selectMESearchElements_18)); }
	inline String_t* get_selectMESearchElements_18() const { return ___selectMESearchElements_18; }
	inline String_t** get_address_of_selectMESearchElements_18() { return &___selectMESearchElements_18; }
	inline void set_selectMESearchElements_18(String_t* value)
	{
		___selectMESearchElements_18 = value;
		Il2CppCodeGenWriteBarrier((&___selectMESearchElements_18), value);
	}

	inline static int32_t get_offset_of_photoPopupDescription_19() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___photoPopupDescription_19)); }
	inline String_t* get_photoPopupDescription_19() const { return ___photoPopupDescription_19; }
	inline String_t** get_address_of_photoPopupDescription_19() { return &___photoPopupDescription_19; }
	inline void set_photoPopupDescription_19(String_t* value)
	{
		___photoPopupDescription_19 = value;
		Il2CppCodeGenWriteBarrier((&___photoPopupDescription_19), value);
	}

	inline static int32_t get_offset_of_photoPopupPhotoHistory_20() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___photoPopupPhotoHistory_20)); }
	inline String_t* get_photoPopupPhotoHistory_20() const { return ___photoPopupPhotoHistory_20; }
	inline String_t** get_address_of_photoPopupPhotoHistory_20() { return &___photoPopupPhotoHistory_20; }
	inline void set_photoPopupPhotoHistory_20(String_t* value)
	{
		___photoPopupPhotoHistory_20 = value;
		Il2CppCodeGenWriteBarrier((&___photoPopupPhotoHistory_20), value);
	}

	inline static int32_t get_offset_of_photoPopupDeleteButton_21() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___photoPopupDeleteButton_21)); }
	inline String_t* get_photoPopupDeleteButton_21() const { return ___photoPopupDeleteButton_21; }
	inline String_t** get_address_of_photoPopupDeleteButton_21() { return &___photoPopupDeleteButton_21; }
	inline void set_photoPopupDeleteButton_21(String_t* value)
	{
		___photoPopupDeleteButton_21 = value;
		Il2CppCodeGenWriteBarrier((&___photoPopupDeleteButton_21), value);
	}

	inline static int32_t get_offset_of_photoPopupAddButton_22() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___photoPopupAddButton_22)); }
	inline String_t* get_photoPopupAddButton_22() const { return ___photoPopupAddButton_22; }
	inline String_t** get_address_of_photoPopupAddButton_22() { return &___photoPopupAddButton_22; }
	inline void set_photoPopupAddButton_22(String_t* value)
	{
		___photoPopupAddButton_22 = value;
		Il2CppCodeGenWriteBarrier((&___photoPopupAddButton_22), value);
	}

	inline static int32_t get_offset_of_modelLoadingTitle_23() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___modelLoadingTitle_23)); }
	inline String_t* get_modelLoadingTitle_23() const { return ___modelLoadingTitle_23; }
	inline String_t** get_address_of_modelLoadingTitle_23() { return &___modelLoadingTitle_23; }
	inline void set_modelLoadingTitle_23(String_t* value)
	{
		___modelLoadingTitle_23 = value;
		Il2CppCodeGenWriteBarrier((&___modelLoadingTitle_23), value);
	}

	inline static int32_t get_offset_of_modelLoadingLoading_24() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___modelLoadingLoading_24)); }
	inline String_t* get_modelLoadingLoading_24() const { return ___modelLoadingLoading_24; }
	inline String_t** get_address_of_modelLoadingLoading_24() { return &___modelLoadingLoading_24; }
	inline void set_modelLoadingLoading_24(String_t* value)
	{
		___modelLoadingLoading_24 = value;
		Il2CppCodeGenWriteBarrier((&___modelLoadingLoading_24), value);
	}

	inline static int32_t get_offset_of_modelLoadingSuccess_25() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___modelLoadingSuccess_25)); }
	inline String_t* get_modelLoadingSuccess_25() const { return ___modelLoadingSuccess_25; }
	inline String_t** get_address_of_modelLoadingSuccess_25() { return &___modelLoadingSuccess_25; }
	inline void set_modelLoadingSuccess_25(String_t* value)
	{
		___modelLoadingSuccess_25 = value;
		Il2CppCodeGenWriteBarrier((&___modelLoadingSuccess_25), value);
	}

	inline static int32_t get_offset_of_modelLoadingSuccessMessage_26() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___modelLoadingSuccessMessage_26)); }
	inline String_t* get_modelLoadingSuccessMessage_26() const { return ___modelLoadingSuccessMessage_26; }
	inline String_t** get_address_of_modelLoadingSuccessMessage_26() { return &___modelLoadingSuccessMessage_26; }
	inline void set_modelLoadingSuccessMessage_26(String_t* value)
	{
		___modelLoadingSuccessMessage_26 = value;
		Il2CppCodeGenWriteBarrier((&___modelLoadingSuccessMessage_26), value);
	}

	inline static int32_t get_offset_of_modelLoadingPartialSuccess_27() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___modelLoadingPartialSuccess_27)); }
	inline String_t* get_modelLoadingPartialSuccess_27() const { return ___modelLoadingPartialSuccess_27; }
	inline String_t** get_address_of_modelLoadingPartialSuccess_27() { return &___modelLoadingPartialSuccess_27; }
	inline void set_modelLoadingPartialSuccess_27(String_t* value)
	{
		___modelLoadingPartialSuccess_27 = value;
		Il2CppCodeGenWriteBarrier((&___modelLoadingPartialSuccess_27), value);
	}

	inline static int32_t get_offset_of_modelLoadingPartialSuccessMessage_28() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___modelLoadingPartialSuccessMessage_28)); }
	inline String_t* get_modelLoadingPartialSuccessMessage_28() const { return ___modelLoadingPartialSuccessMessage_28; }
	inline String_t** get_address_of_modelLoadingPartialSuccessMessage_28() { return &___modelLoadingPartialSuccessMessage_28; }
	inline void set_modelLoadingPartialSuccessMessage_28(String_t* value)
	{
		___modelLoadingPartialSuccessMessage_28 = value;
		Il2CppCodeGenWriteBarrier((&___modelLoadingPartialSuccessMessage_28), value);
	}

	inline static int32_t get_offset_of_uploadMessagesTitle_29() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___uploadMessagesTitle_29)); }
	inline String_t* get_uploadMessagesTitle_29() const { return ___uploadMessagesTitle_29; }
	inline String_t** get_address_of_uploadMessagesTitle_29() { return &___uploadMessagesTitle_29; }
	inline void set_uploadMessagesTitle_29(String_t* value)
	{
		___uploadMessagesTitle_29 = value;
		Il2CppCodeGenWriteBarrier((&___uploadMessagesTitle_29), value);
	}

	inline static int32_t get_offset_of_uploadMessages_30() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___uploadMessages_30)); }
	inline String_t* get_uploadMessages_30() const { return ___uploadMessages_30; }
	inline String_t** get_address_of_uploadMessages_30() { return &___uploadMessages_30; }
	inline void set_uploadMessages_30(String_t* value)
	{
		___uploadMessages_30 = value;
		Il2CppCodeGenWriteBarrier((&___uploadMessages_30), value);
	}

	inline static int32_t get_offset_of_uploadMessagesFailure_31() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___uploadMessagesFailure_31)); }
	inline String_t* get_uploadMessagesFailure_31() const { return ___uploadMessagesFailure_31; }
	inline String_t** get_address_of_uploadMessagesFailure_31() { return &___uploadMessagesFailure_31; }
	inline void set_uploadMessagesFailure_31(String_t* value)
	{
		___uploadMessagesFailure_31 = value;
		Il2CppCodeGenWriteBarrier((&___uploadMessagesFailure_31), value);
	}

	inline static int32_t get_offset_of_approveStateTitle_32() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___approveStateTitle_32)); }
	inline String_t* get_approveStateTitle_32() const { return ___approveStateTitle_32; }
	inline String_t** get_address_of_approveStateTitle_32() { return &___approveStateTitle_32; }
	inline void set_approveStateTitle_32(String_t* value)
	{
		___approveStateTitle_32 = value;
		Il2CppCodeGenWriteBarrier((&___approveStateTitle_32), value);
	}

	inline static int32_t get_offset_of_approveStateQuestion_33() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___approveStateQuestion_33)); }
	inline String_t* get_approveStateQuestion_33() const { return ___approveStateQuestion_33; }
	inline String_t** get_address_of_approveStateQuestion_33() { return &___approveStateQuestion_33; }
	inline void set_approveStateQuestion_33(String_t* value)
	{
		___approveStateQuestion_33 = value;
		Il2CppCodeGenWriteBarrier((&___approveStateQuestion_33), value);
	}

	inline static int32_t get_offset_of_approveStateStatusTitle_34() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___approveStateStatusTitle_34)); }
	inline String_t* get_approveStateStatusTitle_34() const { return ___approveStateStatusTitle_34; }
	inline String_t** get_address_of_approveStateStatusTitle_34() { return &___approveStateStatusTitle_34; }
	inline void set_approveStateStatusTitle_34(String_t* value)
	{
		___approveStateStatusTitle_34 = value;
		Il2CppCodeGenWriteBarrier((&___approveStateStatusTitle_34), value);
	}

	inline static int32_t get_offset_of_approveStateStatusChanging_35() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___approveStateStatusChanging_35)); }
	inline String_t* get_approveStateStatusChanging_35() const { return ___approveStateStatusChanging_35; }
	inline String_t** get_address_of_approveStateStatusChanging_35() { return &___approveStateStatusChanging_35; }
	inline void set_approveStateStatusChanging_35(String_t* value)
	{
		___approveStateStatusChanging_35 = value;
		Il2CppCodeGenWriteBarrier((&___approveStateStatusChanging_35), value);
	}

	inline static int32_t get_offset_of_approveStateStatusChangeFailure_36() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___approveStateStatusChangeFailure_36)); }
	inline String_t* get_approveStateStatusChangeFailure_36() const { return ___approveStateStatusChangeFailure_36; }
	inline String_t** get_address_of_approveStateStatusChangeFailure_36() { return &___approveStateStatusChangeFailure_36; }
	inline void set_approveStateStatusChangeFailure_36(String_t* value)
	{
		___approveStateStatusChangeFailure_36 = value;
		Il2CppCodeGenWriteBarrier((&___approveStateStatusChangeFailure_36), value);
	}

	inline static int32_t get_offset_of_approveStateStatusChangeFailureReason_37() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___approveStateStatusChangeFailureReason_37)); }
	inline String_t* get_approveStateStatusChangeFailureReason_37() const { return ___approveStateStatusChangeFailureReason_37; }
	inline String_t** get_address_of_approveStateStatusChangeFailureReason_37() { return &___approveStateStatusChangeFailureReason_37; }
	inline void set_approveStateStatusChangeFailureReason_37(String_t* value)
	{
		___approveStateStatusChangeFailureReason_37 = value;
		Il2CppCodeGenWriteBarrier((&___approveStateStatusChangeFailureReason_37), value);
	}

	inline static int32_t get_offset_of_approveStateRemarkSaveFailure_38() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___approveStateRemarkSaveFailure_38)); }
	inline String_t* get_approveStateRemarkSaveFailure_38() const { return ___approveStateRemarkSaveFailure_38; }
	inline String_t** get_address_of_approveStateRemarkSaveFailure_38() { return &___approveStateRemarkSaveFailure_38; }
	inline void set_approveStateRemarkSaveFailure_38(String_t* value)
	{
		___approveStateRemarkSaveFailure_38 = value;
		Il2CppCodeGenWriteBarrier((&___approveStateRemarkSaveFailure_38), value);
	}

	inline static int32_t get_offset_of_statusChangeTitle_39() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___statusChangeTitle_39)); }
	inline String_t* get_statusChangeTitle_39() const { return ___statusChangeTitle_39; }
	inline String_t** get_address_of_statusChangeTitle_39() { return &___statusChangeTitle_39; }
	inline void set_statusChangeTitle_39(String_t* value)
	{
		___statusChangeTitle_39 = value;
		Il2CppCodeGenWriteBarrier((&___statusChangeTitle_39), value);
	}

	inline static int32_t get_offset_of_statusChange_40() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___statusChange_40)); }
	inline String_t* get_statusChange_40() const { return ___statusChange_40; }
	inline String_t** get_address_of_statusChange_40() { return &___statusChange_40; }
	inline void set_statusChange_40(String_t* value)
	{
		___statusChange_40 = value;
		Il2CppCodeGenWriteBarrier((&___statusChange_40), value);
	}

	inline static int32_t get_offset_of_statusChangeFailure_41() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___statusChangeFailure_41)); }
	inline String_t* get_statusChangeFailure_41() const { return ___statusChangeFailure_41; }
	inline String_t** get_address_of_statusChangeFailure_41() { return &___statusChangeFailure_41; }
	inline void set_statusChangeFailure_41(String_t* value)
	{
		___statusChangeFailure_41 = value;
		Il2CppCodeGenWriteBarrier((&___statusChangeFailure_41), value);
	}

	inline static int32_t get_offset_of_statusChooseLabel_42() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___statusChooseLabel_42)); }
	inline String_t* get_statusChooseLabel_42() const { return ___statusChooseLabel_42; }
	inline String_t** get_address_of_statusChooseLabel_42() { return &___statusChooseLabel_42; }
	inline void set_statusChooseLabel_42(String_t* value)
	{
		___statusChooseLabel_42 = value;
		Il2CppCodeGenWriteBarrier((&___statusChooseLabel_42), value);
	}

	inline static int32_t get_offset_of_statusChooseOKButton_43() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___statusChooseOKButton_43)); }
	inline String_t* get_statusChooseOKButton_43() const { return ___statusChooseOKButton_43; }
	inline String_t** get_address_of_statusChooseOKButton_43() { return &___statusChooseOKButton_43; }
	inline void set_statusChooseOKButton_43(String_t* value)
	{
		___statusChooseOKButton_43 = value;
		Il2CppCodeGenWriteBarrier((&___statusChooseOKButton_43), value);
	}

	inline static int32_t get_offset_of_statusChooseCancelButton_44() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___statusChooseCancelButton_44)); }
	inline String_t* get_statusChooseCancelButton_44() const { return ___statusChooseCancelButton_44; }
	inline String_t** get_address_of_statusChooseCancelButton_44() { return &___statusChooseCancelButton_44; }
	inline void set_statusChooseCancelButton_44(String_t* value)
	{
		___statusChooseCancelButton_44 = value;
		Il2CppCodeGenWriteBarrier((&___statusChooseCancelButton_44), value);
	}

	inline static int32_t get_offset_of_deleteMessageTitle_45() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___deleteMessageTitle_45)); }
	inline String_t* get_deleteMessageTitle_45() const { return ___deleteMessageTitle_45; }
	inline String_t** get_address_of_deleteMessageTitle_45() { return &___deleteMessageTitle_45; }
	inline void set_deleteMessageTitle_45(String_t* value)
	{
		___deleteMessageTitle_45 = value;
		Il2CppCodeGenWriteBarrier((&___deleteMessageTitle_45), value);
	}

	inline static int32_t get_offset_of_deleteMessageQuestion_46() { return static_cast<int32_t>(offsetof(LanguageStrings_t1206686884, ___deleteMessageQuestion_46)); }
	inline String_t* get_deleteMessageQuestion_46() const { return ___deleteMessageQuestion_46; }
	inline String_t** get_address_of_deleteMessageQuestion_46() { return &___deleteMessageQuestion_46; }
	inline void set_deleteMessageQuestion_46(String_t* value)
	{
		___deleteMessageQuestion_46 = value;
		Il2CppCodeGenWriteBarrier((&___deleteMessageQuestion_46), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LANGUAGESTRINGS_T1206686884_H
#ifndef APROVEELEMENTDTO_T688774064_H
#define APROVEELEMENTDTO_T688774064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ElementStatus.AproveElementDto
struct  AproveElementDto_t688774064  : public RuntimeObject
{
public:
	// System.Boolean Library.Code.UI.ElementStatus.AproveElementDto::check
	bool ___check_0;
	// Library.Code.Domain.Entities.ApproveElementResponse Library.Code.UI.ElementStatus.AproveElementDto::element
	ApproveElementResponse_t2765772516 * ___element_1;

public:
	inline static int32_t get_offset_of_check_0() { return static_cast<int32_t>(offsetof(AproveElementDto_t688774064, ___check_0)); }
	inline bool get_check_0() const { return ___check_0; }
	inline bool* get_address_of_check_0() { return &___check_0; }
	inline void set_check_0(bool value)
	{
		___check_0 = value;
	}

	inline static int32_t get_offset_of_element_1() { return static_cast<int32_t>(offsetof(AproveElementDto_t688774064, ___element_1)); }
	inline ApproveElementResponse_t2765772516 * get_element_1() const { return ___element_1; }
	inline ApproveElementResponse_t2765772516 ** get_address_of_element_1() { return &___element_1; }
	inline void set_element_1(ApproveElementResponse_t2765772516 * value)
	{
		___element_1 = value;
		Il2CppCodeGenWriteBarrier((&___element_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APROVEELEMENTDTO_T688774064_H
#ifndef U3CONEVENTU3EC__ANONSTOREY0_T2580920443_H
#define U3CONEVENTU3EC__ANONSTOREY0_T2580920443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ChecklistUI.ChecklistUI/<OnEvent>c__AnonStorey0
struct  U3COnEventU3Ec__AnonStorey0_t2580920443  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Checklist.ItemStatus Library.Code.UI.ChecklistUI.ChecklistUI/<OnEvent>c__AnonStorey0::itemStatus
	ItemStatus_t1378751697 * ___itemStatus_0;
	// Library.Code.UI.ChecklistUI.ChecklistUI Library.Code.UI.ChecklistUI.ChecklistUI/<OnEvent>c__AnonStorey0::$this
	ChecklistUI_t3492366448 * ___U24this_1;

public:
	inline static int32_t get_offset_of_itemStatus_0() { return static_cast<int32_t>(offsetof(U3COnEventU3Ec__AnonStorey0_t2580920443, ___itemStatus_0)); }
	inline ItemStatus_t1378751697 * get_itemStatus_0() const { return ___itemStatus_0; }
	inline ItemStatus_t1378751697 ** get_address_of_itemStatus_0() { return &___itemStatus_0; }
	inline void set_itemStatus_0(ItemStatus_t1378751697 * value)
	{
		___itemStatus_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStatus_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3COnEventU3Ec__AnonStorey0_t2580920443, ___U24this_1)); }
	inline ChecklistUI_t3492366448 * get_U24this_1() const { return ___U24this_1; }
	inline ChecklistUI_t3492366448 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ChecklistUI_t3492366448 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONEVENTU3EC__ANONSTOREY0_T2580920443_H
#ifndef U3CACTIONONCHECKCHANGEU3EC__ANONSTOREY1_T1708225949_H
#define U3CACTIONONCHECKCHANGEU3EC__ANONSTOREY1_T1708225949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ChecklistUI.ChecklistMobileUINew/<ActionOnCheckChange>c__AnonStorey0/<ActionOnCheckChange>c__AnonStorey1
struct  U3CActionOnCheckChangeU3Ec__AnonStorey1_t1708225949  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Checklist.ItemStatus Library.Code.UI.ChecklistUI.ChecklistMobileUINew/<ActionOnCheckChange>c__AnonStorey0/<ActionOnCheckChange>c__AnonStorey1::status
	ItemStatus_t1378751697 * ___status_0;
	// Library.Code.Domain.Dtos.ItemStatusWithCheckpoint Library.Code.UI.ChecklistUI.ChecklistMobileUINew/<ActionOnCheckChange>c__AnonStorey0/<ActionOnCheckChange>c__AnonStorey1::iitemStatus
	ItemStatusWithCheckpoint_t1657770941 * ___iitemStatus_1;
	// Library.Code.UI.ChecklistUI.ChecklistMobileUINew/<ActionOnCheckChange>c__AnonStorey0 Library.Code.UI.ChecklistUI.ChecklistMobileUINew/<ActionOnCheckChange>c__AnonStorey0/<ActionOnCheckChange>c__AnonStorey1::<>f__ref$0
	U3CActionOnCheckChangeU3Ec__AnonStorey0_t4060093481 * ___U3CU3Ef__refU240_2;

public:
	inline static int32_t get_offset_of_status_0() { return static_cast<int32_t>(offsetof(U3CActionOnCheckChangeU3Ec__AnonStorey1_t1708225949, ___status_0)); }
	inline ItemStatus_t1378751697 * get_status_0() const { return ___status_0; }
	inline ItemStatus_t1378751697 ** get_address_of_status_0() { return &___status_0; }
	inline void set_status_0(ItemStatus_t1378751697 * value)
	{
		___status_0 = value;
		Il2CppCodeGenWriteBarrier((&___status_0), value);
	}

	inline static int32_t get_offset_of_iitemStatus_1() { return static_cast<int32_t>(offsetof(U3CActionOnCheckChangeU3Ec__AnonStorey1_t1708225949, ___iitemStatus_1)); }
	inline ItemStatusWithCheckpoint_t1657770941 * get_iitemStatus_1() const { return ___iitemStatus_1; }
	inline ItemStatusWithCheckpoint_t1657770941 ** get_address_of_iitemStatus_1() { return &___iitemStatus_1; }
	inline void set_iitemStatus_1(ItemStatusWithCheckpoint_t1657770941 * value)
	{
		___iitemStatus_1 = value;
		Il2CppCodeGenWriteBarrier((&___iitemStatus_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_2() { return static_cast<int32_t>(offsetof(U3CActionOnCheckChangeU3Ec__AnonStorey1_t1708225949, ___U3CU3Ef__refU240_2)); }
	inline U3CActionOnCheckChangeU3Ec__AnonStorey0_t4060093481 * get_U3CU3Ef__refU240_2() const { return ___U3CU3Ef__refU240_2; }
	inline U3CActionOnCheckChangeU3Ec__AnonStorey0_t4060093481 ** get_address_of_U3CU3Ef__refU240_2() { return &___U3CU3Ef__refU240_2; }
	inline void set_U3CU3Ef__refU240_2(U3CActionOnCheckChangeU3Ec__AnonStorey0_t4060093481 * value)
	{
		___U3CU3Ef__refU240_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_2), value);
	}
};

struct U3CActionOnCheckChangeU3Ec__AnonStorey1_t1708225949_StaticFields
{
public:
	// System.Action Library.Code.UI.ChecklistUI.ChecklistMobileUINew/<ActionOnCheckChange>c__AnonStorey0/<ActionOnCheckChange>c__AnonStorey1::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(U3CActionOnCheckChangeU3Ec__AnonStorey1_t1708225949_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CACTIONONCHECKCHANGEU3EC__ANONSTOREY1_T1708225949_H
#ifndef GROUPCREATORAPIIMP_T3941306685_H
#define GROUPCREATORAPIIMP_T3941306685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Message.GroupCreatorApiImp
struct  GroupCreatorApiImp_t3941306685  : public RuntimeObject
{
public:
	// Library.Code.Parsers.JsonParser`2<Library.Code.Domain.Entities.GroupCreate2,Library.Code.Domain.Entities.GroupCreate> Library.Code.Networking.Message.GroupCreatorApiImp::_parser
	RuntimeObject* ____parser_0;
	// System.String Library.Code.Networking.Message.GroupCreatorApiImp::ulr
	String_t* ___ulr_1;
	// Library.Code.WebRequest.WebRequestBuilder Library.Code.Networking.Message.GroupCreatorApiImp::_builder
	WebRequestBuilder_t3983605310 * ____builder_2;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(GroupCreatorApiImp_t3941306685, ____parser_0)); }
	inline RuntimeObject* get__parser_0() const { return ____parser_0; }
	inline RuntimeObject** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(RuntimeObject* value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((&____parser_0), value);
	}

	inline static int32_t get_offset_of_ulr_1() { return static_cast<int32_t>(offsetof(GroupCreatorApiImp_t3941306685, ___ulr_1)); }
	inline String_t* get_ulr_1() const { return ___ulr_1; }
	inline String_t** get_address_of_ulr_1() { return &___ulr_1; }
	inline void set_ulr_1(String_t* value)
	{
		___ulr_1 = value;
		Il2CppCodeGenWriteBarrier((&___ulr_1), value);
	}

	inline static int32_t get_offset_of__builder_2() { return static_cast<int32_t>(offsetof(GroupCreatorApiImp_t3941306685, ____builder_2)); }
	inline WebRequestBuilder_t3983605310 * get__builder_2() const { return ____builder_2; }
	inline WebRequestBuilder_t3983605310 ** get_address_of__builder_2() { return &____builder_2; }
	inline void set__builder_2(WebRequestBuilder_t3983605310 * value)
	{
		____builder_2 = value;
		Il2CppCodeGenWriteBarrier((&____builder_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPCREATORAPIIMP_T3941306685_H
#ifndef U3CACTIONONCHECKCHANGEU3EC__ANONSTOREY0_T4060093481_H
#define U3CACTIONONCHECKCHANGEU3EC__ANONSTOREY0_T4060093481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ChecklistUI.ChecklistMobileUINew/<ActionOnCheckChange>c__AnonStorey0
struct  U3CActionOnCheckChangeU3Ec__AnonStorey0_t4060093481  : public RuntimeObject
{
public:
	// Library.Code.Domain.Dtos.ItemStatusWithCheckpoint Library.Code.UI.ChecklistUI.ChecklistMobileUINew/<ActionOnCheckChange>c__AnonStorey0::itemStatus
	ItemStatusWithCheckpoint_t1657770941 * ___itemStatus_0;
	// Library.Code.UI.ChecklistUI.ChecklistMobileUINew Library.Code.UI.ChecklistUI.ChecklistMobileUINew/<ActionOnCheckChange>c__AnonStorey0::$this
	ChecklistMobileUINew_t1060043268 * ___U24this_1;

public:
	inline static int32_t get_offset_of_itemStatus_0() { return static_cast<int32_t>(offsetof(U3CActionOnCheckChangeU3Ec__AnonStorey0_t4060093481, ___itemStatus_0)); }
	inline ItemStatusWithCheckpoint_t1657770941 * get_itemStatus_0() const { return ___itemStatus_0; }
	inline ItemStatusWithCheckpoint_t1657770941 ** get_address_of_itemStatus_0() { return &___itemStatus_0; }
	inline void set_itemStatus_0(ItemStatusWithCheckpoint_t1657770941 * value)
	{
		___itemStatus_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStatus_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CActionOnCheckChangeU3Ec__AnonStorey0_t4060093481, ___U24this_1)); }
	inline ChecklistMobileUINew_t1060043268 * get_U24this_1() const { return ___U24this_1; }
	inline ChecklistMobileUINew_t1060043268 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ChecklistMobileUINew_t1060043268 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CACTIONONCHECKCHANGEU3EC__ANONSTOREY0_T4060093481_H
#ifndef U3CCREATEGROUPU3EC__ITERATOR0_T3097045949_H
#define U3CCREATEGROUPU3EC__ITERATOR0_T3097045949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Message.GroupCreatorApiImp/<createGroup>c__Iterator0
struct  U3CcreateGroupU3Ec__Iterator0_t3097045949  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.GroupCreate Library.Code.Networking.Message.GroupCreatorApiImp/<createGroup>c__Iterator0::create
	GroupCreate_t2003352421 * ___create_0;
	// System.String Library.Code.Networking.Message.GroupCreatorApiImp/<createGroup>c__Iterator0::<json>__0
	String_t* ___U3CjsonU3E__0_1;
	// UnityEngine.Networking.UnityWebRequest Library.Code.Networking.Message.GroupCreatorApiImp/<createGroup>c__Iterator0::<request>__0
	UnityWebRequest_t254341728 * ___U3CrequestU3E__0_2;
	// Library.Code.Networking.ResponseWrapper`1<System.Int64> Library.Code.Networking.Message.GroupCreatorApiImp/<createGroup>c__Iterator0::callback
	RuntimeObject* ___callback_3;
	// Library.Code.Networking.Message.GroupCreatorApiImp Library.Code.Networking.Message.GroupCreatorApiImp/<createGroup>c__Iterator0::$this
	GroupCreatorApiImp_t3941306685 * ___U24this_4;
	// System.Object Library.Code.Networking.Message.GroupCreatorApiImp/<createGroup>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Library.Code.Networking.Message.GroupCreatorApiImp/<createGroup>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 Library.Code.Networking.Message.GroupCreatorApiImp/<createGroup>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_create_0() { return static_cast<int32_t>(offsetof(U3CcreateGroupU3Ec__Iterator0_t3097045949, ___create_0)); }
	inline GroupCreate_t2003352421 * get_create_0() const { return ___create_0; }
	inline GroupCreate_t2003352421 ** get_address_of_create_0() { return &___create_0; }
	inline void set_create_0(GroupCreate_t2003352421 * value)
	{
		___create_0 = value;
		Il2CppCodeGenWriteBarrier((&___create_0), value);
	}

	inline static int32_t get_offset_of_U3CjsonU3E__0_1() { return static_cast<int32_t>(offsetof(U3CcreateGroupU3Ec__Iterator0_t3097045949, ___U3CjsonU3E__0_1)); }
	inline String_t* get_U3CjsonU3E__0_1() const { return ___U3CjsonU3E__0_1; }
	inline String_t** get_address_of_U3CjsonU3E__0_1() { return &___U3CjsonU3E__0_1; }
	inline void set_U3CjsonU3E__0_1(String_t* value)
	{
		___U3CjsonU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CcreateGroupU3Ec__Iterator0_t3097045949, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t254341728 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t254341728 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t254341728 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(U3CcreateGroupU3Ec__Iterator0_t3097045949, ___callback_3)); }
	inline RuntimeObject* get_callback_3() const { return ___callback_3; }
	inline RuntimeObject** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(RuntimeObject* value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___callback_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CcreateGroupU3Ec__Iterator0_t3097045949, ___U24this_4)); }
	inline GroupCreatorApiImp_t3941306685 * get_U24this_4() const { return ___U24this_4; }
	inline GroupCreatorApiImp_t3941306685 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(GroupCreatorApiImp_t3941306685 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CcreateGroupU3Ec__Iterator0_t3097045949, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CcreateGroupU3Ec__Iterator0_t3097045949, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CcreateGroupU3Ec__Iterator0_t3097045949, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATEGROUPU3EC__ITERATOR0_T3097045949_H
#ifndef SENDBATCHMESSAGEAPIIMP_T1721429217_H
#define SENDBATCHMESSAGEAPIIMP_T1721429217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Message.SendBatchMessageApiImp
struct  SendBatchMessageApiImp_t1721429217  : public RuntimeObject
{
public:
	// Library.Code.Parsers.JsonParser`2<Library.Code.Domain.Entities.BatchMessages,Library.Code.Domain.Entities.BatchMessagesResponses> Library.Code.Networking.Message.SendBatchMessageApiImp::_parser
	RuntimeObject* ____parser_0;
	// System.String Library.Code.Networking.Message.SendBatchMessageApiImp::ulr
	String_t* ___ulr_1;
	// Library.Code.WebRequest.WebRequestBuilder Library.Code.Networking.Message.SendBatchMessageApiImp::_builder
	WebRequestBuilder_t3983605310 * ____builder_2;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(SendBatchMessageApiImp_t1721429217, ____parser_0)); }
	inline RuntimeObject* get__parser_0() const { return ____parser_0; }
	inline RuntimeObject** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(RuntimeObject* value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((&____parser_0), value);
	}

	inline static int32_t get_offset_of_ulr_1() { return static_cast<int32_t>(offsetof(SendBatchMessageApiImp_t1721429217, ___ulr_1)); }
	inline String_t* get_ulr_1() const { return ___ulr_1; }
	inline String_t** get_address_of_ulr_1() { return &___ulr_1; }
	inline void set_ulr_1(String_t* value)
	{
		___ulr_1 = value;
		Il2CppCodeGenWriteBarrier((&___ulr_1), value);
	}

	inline static int32_t get_offset_of__builder_2() { return static_cast<int32_t>(offsetof(SendBatchMessageApiImp_t1721429217, ____builder_2)); }
	inline WebRequestBuilder_t3983605310 * get__builder_2() const { return ____builder_2; }
	inline WebRequestBuilder_t3983605310 ** get_address_of__builder_2() { return &____builder_2; }
	inline void set__builder_2(WebRequestBuilder_t3983605310 * value)
	{
		____builder_2 = value;
		Il2CppCodeGenWriteBarrier((&____builder_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDBATCHMESSAGEAPIIMP_T1721429217_H
#ifndef U3CSENDBATCHMESSAGESU3EC__ITERATOR0_T575326176_H
#define U3CSENDBATCHMESSAGESU3EC__ITERATOR0_T575326176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Message.SendBatchMessageApiImp/<sendBatchMessages>c__Iterator0
struct  U3CsendBatchMessagesU3Ec__Iterator0_t575326176  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Message> Library.Code.Networking.Message.SendBatchMessageApiImp/<sendBatchMessages>c__Iterator0::messages
	List_1_t583975089 * ___messages_0;
	// Library.Code.Domain.Entities.BatchMessages Library.Code.Networking.Message.SendBatchMessageApiImp/<sendBatchMessages>c__Iterator0::<batch>__0
	BatchMessages_t2405623082 * ___U3CbatchU3E__0_1;
	// System.String Library.Code.Networking.Message.SendBatchMessageApiImp/<sendBatchMessages>c__Iterator0::<json>__0
	String_t* ___U3CjsonU3E__0_2;
	// UnityEngine.Networking.UnityWebRequest Library.Code.Networking.Message.SendBatchMessageApiImp/<sendBatchMessages>c__Iterator0::<request>__0
	UnityWebRequest_t254341728 * ___U3CrequestU3E__0_3;
	// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Message>> Library.Code.Networking.Message.SendBatchMessageApiImp/<sendBatchMessages>c__Iterator0::callback
	RuntimeObject* ___callback_4;
	// Library.Code.Networking.Message.SendBatchMessageApiImp Library.Code.Networking.Message.SendBatchMessageApiImp/<sendBatchMessages>c__Iterator0::$this
	SendBatchMessageApiImp_t1721429217 * ___U24this_5;
	// System.Object Library.Code.Networking.Message.SendBatchMessageApiImp/<sendBatchMessages>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean Library.Code.Networking.Message.SendBatchMessageApiImp/<sendBatchMessages>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 Library.Code.Networking.Message.SendBatchMessageApiImp/<sendBatchMessages>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_messages_0() { return static_cast<int32_t>(offsetof(U3CsendBatchMessagesU3Ec__Iterator0_t575326176, ___messages_0)); }
	inline List_1_t583975089 * get_messages_0() const { return ___messages_0; }
	inline List_1_t583975089 ** get_address_of_messages_0() { return &___messages_0; }
	inline void set_messages_0(List_1_t583975089 * value)
	{
		___messages_0 = value;
		Il2CppCodeGenWriteBarrier((&___messages_0), value);
	}

	inline static int32_t get_offset_of_U3CbatchU3E__0_1() { return static_cast<int32_t>(offsetof(U3CsendBatchMessagesU3Ec__Iterator0_t575326176, ___U3CbatchU3E__0_1)); }
	inline BatchMessages_t2405623082 * get_U3CbatchU3E__0_1() const { return ___U3CbatchU3E__0_1; }
	inline BatchMessages_t2405623082 ** get_address_of_U3CbatchU3E__0_1() { return &___U3CbatchU3E__0_1; }
	inline void set_U3CbatchU3E__0_1(BatchMessages_t2405623082 * value)
	{
		___U3CbatchU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbatchU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CjsonU3E__0_2() { return static_cast<int32_t>(offsetof(U3CsendBatchMessagesU3Ec__Iterator0_t575326176, ___U3CjsonU3E__0_2)); }
	inline String_t* get_U3CjsonU3E__0_2() const { return ___U3CjsonU3E__0_2; }
	inline String_t** get_address_of_U3CjsonU3E__0_2() { return &___U3CjsonU3E__0_2; }
	inline void set_U3CjsonU3E__0_2(String_t* value)
	{
		___U3CjsonU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CsendBatchMessagesU3Ec__Iterator0_t575326176, ___U3CrequestU3E__0_3)); }
	inline UnityWebRequest_t254341728 * get_U3CrequestU3E__0_3() const { return ___U3CrequestU3E__0_3; }
	inline UnityWebRequest_t254341728 ** get_address_of_U3CrequestU3E__0_3() { return &___U3CrequestU3E__0_3; }
	inline void set_U3CrequestU3E__0_3(UnityWebRequest_t254341728 * value)
	{
		___U3CrequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CsendBatchMessagesU3Ec__Iterator0_t575326176, ___callback_4)); }
	inline RuntimeObject* get_callback_4() const { return ___callback_4; }
	inline RuntimeObject** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(RuntimeObject* value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CsendBatchMessagesU3Ec__Iterator0_t575326176, ___U24this_5)); }
	inline SendBatchMessageApiImp_t1721429217 * get_U24this_5() const { return ___U24this_5; }
	inline SendBatchMessageApiImp_t1721429217 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(SendBatchMessageApiImp_t1721429217 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CsendBatchMessagesU3Ec__Iterator0_t575326176, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CsendBatchMessagesU3Ec__Iterator0_t575326176, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CsendBatchMessagesU3Ec__Iterator0_t575326176, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDBATCHMESSAGESU3EC__ITERATOR0_T575326176_H
#ifndef U3CLOADMODELSU3EC__ITERATOR0_T3694394883_H
#define U3CLOADMODELSU3EC__ITERATOR0_T3694394883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Models.ModelApiImp/<loadModels>c__Iterator0
struct  U3CloadModelsU3Ec__Iterator0_t3694394883  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest Library.Code.Networking.Models.ModelApiImp/<loadModels>c__Iterator0::<request>__0
	UnityWebRequest_t254341728 * ___U3CrequestU3E__0_0;
	// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model>> Library.Code.Networking.Models.ModelApiImp/<loadModels>c__Iterator0::reposneWrapper
	RuntimeObject* ___reposneWrapper_1;
	// Library.Code.Networking.Models.ModelApiImp Library.Code.Networking.Models.ModelApiImp/<loadModels>c__Iterator0::$this
	ModelApiImp_t398605299 * ___U24this_2;
	// System.Object Library.Code.Networking.Models.ModelApiImp/<loadModels>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Library.Code.Networking.Models.ModelApiImp/<loadModels>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Library.Code.Networking.Models.ModelApiImp/<loadModels>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadModelsU3Ec__Iterator0_t3694394883, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t254341728 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t254341728 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t254341728 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_reposneWrapper_1() { return static_cast<int32_t>(offsetof(U3CloadModelsU3Ec__Iterator0_t3694394883, ___reposneWrapper_1)); }
	inline RuntimeObject* get_reposneWrapper_1() const { return ___reposneWrapper_1; }
	inline RuntimeObject** get_address_of_reposneWrapper_1() { return &___reposneWrapper_1; }
	inline void set_reposneWrapper_1(RuntimeObject* value)
	{
		___reposneWrapper_1 = value;
		Il2CppCodeGenWriteBarrier((&___reposneWrapper_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CloadModelsU3Ec__Iterator0_t3694394883, ___U24this_2)); }
	inline ModelApiImp_t398605299 * get_U24this_2() const { return ___U24this_2; }
	inline ModelApiImp_t398605299 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ModelApiImp_t398605299 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CloadModelsU3Ec__Iterator0_t3694394883, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CloadModelsU3Ec__Iterator0_t3694394883, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CloadModelsU3Ec__Iterator0_t3694394883, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADMODELSU3EC__ITERATOR0_T3694394883_H
#ifndef MESSAGETEMPLATEAPIIMP_T700868765_H
#define MESSAGETEMPLATEAPIIMP_T700868765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.MessageTemplate.MessageTemplateApiImp
struct  MessageTemplateApiImp_t700868765  : public RuntimeObject
{
public:
	// Library.Code.Parsers.JsonParser`1<Library.Code.Domain.Entities.MessageTemplateEmbedded> Library.Code.Networking.MessageTemplate.MessageTemplateApiImp::_parser
	RuntimeObject* ____parser_0;
	// System.String Library.Code.Networking.MessageTemplate.MessageTemplateApiImp::ulr
	String_t* ___ulr_1;
	// Library.Code.WebRequest.WebRequestBuilder Library.Code.Networking.MessageTemplate.MessageTemplateApiImp::_builder
	WebRequestBuilder_t3983605310 * ____builder_2;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(MessageTemplateApiImp_t700868765, ____parser_0)); }
	inline RuntimeObject* get__parser_0() const { return ____parser_0; }
	inline RuntimeObject** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(RuntimeObject* value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((&____parser_0), value);
	}

	inline static int32_t get_offset_of_ulr_1() { return static_cast<int32_t>(offsetof(MessageTemplateApiImp_t700868765, ___ulr_1)); }
	inline String_t* get_ulr_1() const { return ___ulr_1; }
	inline String_t** get_address_of_ulr_1() { return &___ulr_1; }
	inline void set_ulr_1(String_t* value)
	{
		___ulr_1 = value;
		Il2CppCodeGenWriteBarrier((&___ulr_1), value);
	}

	inline static int32_t get_offset_of__builder_2() { return static_cast<int32_t>(offsetof(MessageTemplateApiImp_t700868765, ____builder_2)); }
	inline WebRequestBuilder_t3983605310 * get__builder_2() const { return ____builder_2; }
	inline WebRequestBuilder_t3983605310 ** get_address_of__builder_2() { return &____builder_2; }
	inline void set__builder_2(WebRequestBuilder_t3983605310 * value)
	{
		____builder_2 = value;
		Il2CppCodeGenWriteBarrier((&____builder_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGETEMPLATEAPIIMP_T700868765_H
#ifndef MODELAPIIMP_T398605299_H
#define MODELAPIIMP_T398605299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Models.ModelApiImp
struct  ModelApiImp_t398605299  : public RuntimeObject
{
public:
	// Library.Code.Parsers.JsonParser`1<Library.Code.Domain.Entities.ModelEmbedded> Library.Code.Networking.Models.ModelApiImp::_parser
	RuntimeObject* ____parser_0;
	// System.String Library.Code.Networking.Models.ModelApiImp::url
	String_t* ___url_1;
	// Library.Code.WebRequest.WebRequestBuilder Library.Code.Networking.Models.ModelApiImp::_builder
	WebRequestBuilder_t3983605310 * ____builder_2;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(ModelApiImp_t398605299, ____parser_0)); }
	inline RuntimeObject* get__parser_0() const { return ____parser_0; }
	inline RuntimeObject** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(RuntimeObject* value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((&____parser_0), value);
	}

	inline static int32_t get_offset_of_url_1() { return static_cast<int32_t>(offsetof(ModelApiImp_t398605299, ___url_1)); }
	inline String_t* get_url_1() const { return ___url_1; }
	inline String_t** get_address_of_url_1() { return &___url_1; }
	inline void set_url_1(String_t* value)
	{
		___url_1 = value;
		Il2CppCodeGenWriteBarrier((&___url_1), value);
	}

	inline static int32_t get_offset_of__builder_2() { return static_cast<int32_t>(offsetof(ModelApiImp_t398605299, ____builder_2)); }
	inline WebRequestBuilder_t3983605310 * get__builder_2() const { return ____builder_2; }
	inline WebRequestBuilder_t3983605310 ** get_address_of__builder_2() { return &____builder_2; }
	inline void set__builder_2(WebRequestBuilder_t3983605310 * value)
	{
		____builder_2 = value;
		Il2CppCodeGenWriteBarrier((&____builder_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELAPIIMP_T398605299_H
#ifndef U3CONEVENTU3EC__ANONSTOREY2_T649407929_H
#define U3CONEVENTU3EC__ANONSTOREY2_T649407929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ChecklistUI.ChecklistMobileUINew/<OnEvent>c__AnonStorey2
struct  U3COnEventU3Ec__AnonStorey2_t649407929  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.Checklist.ItemStatus Library.Code.UI.ChecklistUI.ChecklistMobileUINew/<OnEvent>c__AnonStorey2::itemStatus
	ItemStatus_t1378751697 * ___itemStatus_0;
	// Library.Code.UI.ChecklistUI.ChecklistMobileUINew Library.Code.UI.ChecklistUI.ChecklistMobileUINew/<OnEvent>c__AnonStorey2::$this
	ChecklistMobileUINew_t1060043268 * ___U24this_1;

public:
	inline static int32_t get_offset_of_itemStatus_0() { return static_cast<int32_t>(offsetof(U3COnEventU3Ec__AnonStorey2_t649407929, ___itemStatus_0)); }
	inline ItemStatus_t1378751697 * get_itemStatus_0() const { return ___itemStatus_0; }
	inline ItemStatus_t1378751697 ** get_address_of_itemStatus_0() { return &___itemStatus_0; }
	inline void set_itemStatus_0(ItemStatus_t1378751697 * value)
	{
		___itemStatus_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStatus_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3COnEventU3Ec__AnonStorey2_t649407929, ___U24this_1)); }
	inline ChecklistMobileUINew_t1060043268 * get_U24this_1() const { return ___U24this_1; }
	inline ChecklistMobileUINew_t1060043268 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ChecklistMobileUINew_t1060043268 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

struct U3COnEventU3Ec__AnonStorey2_t649407929_StaticFields
{
public:
	// System.Action Library.Code.UI.ChecklistUI.ChecklistMobileUINew/<OnEvent>c__AnonStorey2::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_2;
	// System.Action Library.Code.UI.ChecklistUI.ChecklistMobileUINew/<OnEvent>c__AnonStorey2::<>f__am$cache1
	Action_t3226471752 * ___U3CU3Ef__amU24cache1_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(U3COnEventU3Ec__AnonStorey2_t649407929_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(U3COnEventU3Ec__AnonStorey2_t649407929_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CONEVENTU3EC__ANONSTOREY2_T649407929_H
#ifndef U3CSENDFILESU3EC__ANONSTOREY0_T1170884102_H
#define U3CSENDFILESU3EC__ANONSTOREY0_T1170884102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Files.SendFileApiImpl/<sendFiles>c__AnonStorey0
struct  U3CsendFilesU3Ec__AnonStorey0_t1170884102  : public RuntimeObject
{
public:
	// System.String Library.Code.Networking.Files.SendFileApiImpl/<sendFiles>c__AnonStorey0::uri
	String_t* ___uri_0;
	// Library.Code.Facades.Callback Library.Code.Networking.Files.SendFileApiImpl/<sendFiles>c__AnonStorey0::callback
	RuntimeObject* ___callback_1;
	// Library.Code.Networking.Files.SendFileApiImpl Library.Code.Networking.Files.SendFileApiImpl/<sendFiles>c__AnonStorey0::$this
	SendFileApiImpl_t545224088 * ___U24this_2;

public:
	inline static int32_t get_offset_of_uri_0() { return static_cast<int32_t>(offsetof(U3CsendFilesU3Ec__AnonStorey0_t1170884102, ___uri_0)); }
	inline String_t* get_uri_0() const { return ___uri_0; }
	inline String_t** get_address_of_uri_0() { return &___uri_0; }
	inline void set_uri_0(String_t* value)
	{
		___uri_0 = value;
		Il2CppCodeGenWriteBarrier((&___uri_0), value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(U3CsendFilesU3Ec__AnonStorey0_t1170884102, ___callback_1)); }
	inline RuntimeObject* get_callback_1() const { return ___callback_1; }
	inline RuntimeObject** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(RuntimeObject* value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CsendFilesU3Ec__AnonStorey0_t1170884102, ___U24this_2)); }
	inline SendFileApiImpl_t545224088 * get_U24this_2() const { return ___U24this_2; }
	inline SendFileApiImpl_t545224088 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(SendFileApiImpl_t545224088 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDFILESU3EC__ANONSTOREY0_T1170884102_H
#ifndef GROUPINGMODELAPIIMP_T1621005666_H
#define GROUPINGMODELAPIIMP_T1621005666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.GroupModel.GroupingModelApiImp
struct  GroupingModelApiImp_t1621005666  : public RuntimeObject
{
public:
	// Library.Code.Parsers.JsonParser`1<Library.Code.Domain.Entities.PrefixColor.PrefixColorResponse> Library.Code.Networking.GroupModel.GroupingModelApiImp::_jsonParser
	RuntimeObject* ____jsonParser_0;
	// System.String Library.Code.Networking.GroupModel.GroupingModelApiImp::url
	String_t* ___url_1;
	// Library.Code.WebRequest.WebRequestBuilder Library.Code.Networking.GroupModel.GroupingModelApiImp::_builder
	WebRequestBuilder_t3983605310 * ____builder_2;

public:
	inline static int32_t get_offset_of__jsonParser_0() { return static_cast<int32_t>(offsetof(GroupingModelApiImp_t1621005666, ____jsonParser_0)); }
	inline RuntimeObject* get__jsonParser_0() const { return ____jsonParser_0; }
	inline RuntimeObject** get_address_of__jsonParser_0() { return &____jsonParser_0; }
	inline void set__jsonParser_0(RuntimeObject* value)
	{
		____jsonParser_0 = value;
		Il2CppCodeGenWriteBarrier((&____jsonParser_0), value);
	}

	inline static int32_t get_offset_of_url_1() { return static_cast<int32_t>(offsetof(GroupingModelApiImp_t1621005666, ___url_1)); }
	inline String_t* get_url_1() const { return ___url_1; }
	inline String_t** get_address_of_url_1() { return &___url_1; }
	inline void set_url_1(String_t* value)
	{
		___url_1 = value;
		Il2CppCodeGenWriteBarrier((&___url_1), value);
	}

	inline static int32_t get_offset_of__builder_2() { return static_cast<int32_t>(offsetof(GroupingModelApiImp_t1621005666, ____builder_2)); }
	inline WebRequestBuilder_t3983605310 * get__builder_2() const { return ____builder_2; }
	inline WebRequestBuilder_t3983605310 ** get_address_of__builder_2() { return &____builder_2; }
	inline void set__builder_2(WebRequestBuilder_t3983605310 * value)
	{
		____builder_2 = value;
		Il2CppCodeGenWriteBarrier((&____builder_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPINGMODELAPIIMP_T1621005666_H
#ifndef U3CLOADMODELGROUPINGDATAU3EC__ITERATOR0_T580313732_H
#define U3CLOADMODELGROUPINGDATAU3EC__ITERATOR0_T580313732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.GroupModel.GroupingModelApiImp/<loadModelGroupingData>c__Iterator0
struct  U3CloadModelGroupingDataU3Ec__Iterator0_t580313732  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest Library.Code.Networking.GroupModel.GroupingModelApiImp/<loadModelGroupingData>c__Iterator0::<request>__0
	UnityWebRequest_t254341728 * ___U3CrequestU3E__0_0;
	// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.ModelGroup>> Library.Code.Networking.GroupModel.GroupingModelApiImp/<loadModelGroupingData>c__Iterator0::reposneWrapper
	RuntimeObject* ___reposneWrapper_1;
	// Library.Code.Networking.GroupModel.GroupingModelApiImp Library.Code.Networking.GroupModel.GroupingModelApiImp/<loadModelGroupingData>c__Iterator0::$this
	GroupingModelApiImp_t1621005666 * ___U24this_2;
	// System.Object Library.Code.Networking.GroupModel.GroupingModelApiImp/<loadModelGroupingData>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Library.Code.Networking.GroupModel.GroupingModelApiImp/<loadModelGroupingData>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Library.Code.Networking.GroupModel.GroupingModelApiImp/<loadModelGroupingData>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadModelGroupingDataU3Ec__Iterator0_t580313732, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t254341728 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t254341728 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t254341728 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_reposneWrapper_1() { return static_cast<int32_t>(offsetof(U3CloadModelGroupingDataU3Ec__Iterator0_t580313732, ___reposneWrapper_1)); }
	inline RuntimeObject* get_reposneWrapper_1() const { return ___reposneWrapper_1; }
	inline RuntimeObject** get_address_of_reposneWrapper_1() { return &___reposneWrapper_1; }
	inline void set_reposneWrapper_1(RuntimeObject* value)
	{
		___reposneWrapper_1 = value;
		Il2CppCodeGenWriteBarrier((&___reposneWrapper_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CloadModelGroupingDataU3Ec__Iterator0_t580313732, ___U24this_2)); }
	inline GroupingModelApiImp_t1621005666 * get_U24this_2() const { return ___U24this_2; }
	inline GroupingModelApiImp_t1621005666 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(GroupingModelApiImp_t1621005666 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CloadModelGroupingDataU3Ec__Iterator0_t580313732, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CloadModelGroupingDataU3Ec__Iterator0_t580313732, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CloadModelGroupingDataU3Ec__Iterator0_t580313732, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADMODELGROUPINGDATAU3EC__ITERATOR0_T580313732_H
#ifndef U3CLOADMODELGROUPINGDATAU3EC__ANONSTOREY1_T3430150173_H
#define U3CLOADMODELGROUPINGDATAU3EC__ANONSTOREY1_T3430150173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.GroupModel.GroupingModelApiImp/<loadModelGroupingData>c__Iterator0/<loadModelGroupingData>c__AnonStorey1
struct  U3CloadModelGroupingDataU3Ec__AnonStorey1_t3430150173  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.ModelGroup> Library.Code.Networking.GroupModel.GroupingModelApiImp/<loadModelGroupingData>c__Iterator0/<loadModelGroupingData>c__AnonStorey1::list
	List_1_t2603308778 * ___list_0;
	// Library.Code.Networking.GroupModel.GroupingModelApiImp/<loadModelGroupingData>c__Iterator0 Library.Code.Networking.GroupModel.GroupingModelApiImp/<loadModelGroupingData>c__Iterator0/<loadModelGroupingData>c__AnonStorey1::<>f__ref$0
	U3CloadModelGroupingDataU3Ec__Iterator0_t580313732 * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(U3CloadModelGroupingDataU3Ec__AnonStorey1_t3430150173, ___list_0)); }
	inline List_1_t2603308778 * get_list_0() const { return ___list_0; }
	inline List_1_t2603308778 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t2603308778 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CloadModelGroupingDataU3Ec__AnonStorey1_t3430150173, ___U3CU3Ef__refU240_1)); }
	inline U3CloadModelGroupingDataU3Ec__Iterator0_t580313732 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CloadModelGroupingDataU3Ec__Iterator0_t580313732 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CloadModelGroupingDataU3Ec__Iterator0_t580313732 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADMODELGROUPINGDATAU3EC__ANONSTOREY1_T3430150173_H
#ifndef U3CLOADMESSAGESTEMPLATESU3EC__ITERATOR0_T130507051_H
#define U3CLOADMESSAGESTEMPLATESU3EC__ITERATOR0_T130507051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.MessageTemplate.MessageTemplateApiImp/<loadMessagesTemplates>c__Iterator0
struct  U3CloadMessagesTemplatesU3Ec__Iterator0_t130507051  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest Library.Code.Networking.MessageTemplate.MessageTemplateApiImp/<loadMessagesTemplates>c__Iterator0::<request>__0
	UnityWebRequest_t254341728 * ___U3CrequestU3E__0_0;
	// Library.Code.Networking.ResponseWrapper`1<System.Collections.Generic.List`1<Library.Code.Domain.Entities.MessageTemplate>> Library.Code.Networking.MessageTemplate.MessageTemplateApiImp/<loadMessagesTemplates>c__Iterator0::reposneWrapper
	RuntimeObject* ___reposneWrapper_1;
	// Library.Code.Networking.MessageTemplate.MessageTemplateApiImp Library.Code.Networking.MessageTemplate.MessageTemplateApiImp/<loadMessagesTemplates>c__Iterator0::$this
	MessageTemplateApiImp_t700868765 * ___U24this_2;
	// System.Object Library.Code.Networking.MessageTemplate.MessageTemplateApiImp/<loadMessagesTemplates>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Library.Code.Networking.MessageTemplate.MessageTemplateApiImp/<loadMessagesTemplates>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Library.Code.Networking.MessageTemplate.MessageTemplateApiImp/<loadMessagesTemplates>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadMessagesTemplatesU3Ec__Iterator0_t130507051, ___U3CrequestU3E__0_0)); }
	inline UnityWebRequest_t254341728 * get_U3CrequestU3E__0_0() const { return ___U3CrequestU3E__0_0; }
	inline UnityWebRequest_t254341728 ** get_address_of_U3CrequestU3E__0_0() { return &___U3CrequestU3E__0_0; }
	inline void set_U3CrequestU3E__0_0(UnityWebRequest_t254341728 * value)
	{
		___U3CrequestU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_0), value);
	}

	inline static int32_t get_offset_of_reposneWrapper_1() { return static_cast<int32_t>(offsetof(U3CloadMessagesTemplatesU3Ec__Iterator0_t130507051, ___reposneWrapper_1)); }
	inline RuntimeObject* get_reposneWrapper_1() const { return ___reposneWrapper_1; }
	inline RuntimeObject** get_address_of_reposneWrapper_1() { return &___reposneWrapper_1; }
	inline void set_reposneWrapper_1(RuntimeObject* value)
	{
		___reposneWrapper_1 = value;
		Il2CppCodeGenWriteBarrier((&___reposneWrapper_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CloadMessagesTemplatesU3Ec__Iterator0_t130507051, ___U24this_2)); }
	inline MessageTemplateApiImp_t700868765 * get_U24this_2() const { return ___U24this_2; }
	inline MessageTemplateApiImp_t700868765 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(MessageTemplateApiImp_t700868765 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CloadMessagesTemplatesU3Ec__Iterator0_t130507051, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CloadMessagesTemplatesU3Ec__Iterator0_t130507051, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CloadMessagesTemplatesU3Ec__Iterator0_t130507051, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADMESSAGESTEMPLATESU3EC__ITERATOR0_T130507051_H
#ifndef U3CCREATEGROUPU3EC__ITERATOR0_T3134932817_H
#define U3CCREATEGROUPU3EC__ITERATOR0_T3134932817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Message.GroupCreatorApiHoloImp/<createGroup>c__Iterator0
struct  U3CcreateGroupU3Ec__Iterator0_t3134932817  : public RuntimeObject
{
public:
	// Library.Code.Domain.Entities.GroupCreate Library.Code.Networking.Message.GroupCreatorApiHoloImp/<createGroup>c__Iterator0::create
	GroupCreate_t2003352421 * ___create_0;
	// System.String Library.Code.Networking.Message.GroupCreatorApiHoloImp/<createGroup>c__Iterator0::<json>__0
	String_t* ___U3CjsonU3E__0_1;
	// UnityEngine.Networking.UnityWebRequest Library.Code.Networking.Message.GroupCreatorApiHoloImp/<createGroup>c__Iterator0::<request>__0
	UnityWebRequest_t254341728 * ___U3CrequestU3E__0_2;
	// Library.Code.Networking.ResponseWrapper`1<System.Int64> Library.Code.Networking.Message.GroupCreatorApiHoloImp/<createGroup>c__Iterator0::callback
	RuntimeObject* ___callback_3;
	// Library.Code.Networking.Message.GroupCreatorApiHoloImp Library.Code.Networking.Message.GroupCreatorApiHoloImp/<createGroup>c__Iterator0::$this
	GroupCreatorApiHoloImp_t1244970665 * ___U24this_4;
	// System.Object Library.Code.Networking.Message.GroupCreatorApiHoloImp/<createGroup>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Library.Code.Networking.Message.GroupCreatorApiHoloImp/<createGroup>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 Library.Code.Networking.Message.GroupCreatorApiHoloImp/<createGroup>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_create_0() { return static_cast<int32_t>(offsetof(U3CcreateGroupU3Ec__Iterator0_t3134932817, ___create_0)); }
	inline GroupCreate_t2003352421 * get_create_0() const { return ___create_0; }
	inline GroupCreate_t2003352421 ** get_address_of_create_0() { return &___create_0; }
	inline void set_create_0(GroupCreate_t2003352421 * value)
	{
		___create_0 = value;
		Il2CppCodeGenWriteBarrier((&___create_0), value);
	}

	inline static int32_t get_offset_of_U3CjsonU3E__0_1() { return static_cast<int32_t>(offsetof(U3CcreateGroupU3Ec__Iterator0_t3134932817, ___U3CjsonU3E__0_1)); }
	inline String_t* get_U3CjsonU3E__0_1() const { return ___U3CjsonU3E__0_1; }
	inline String_t** get_address_of_U3CjsonU3E__0_1() { return &___U3CjsonU3E__0_1; }
	inline void set_U3CjsonU3E__0_1(String_t* value)
	{
		___U3CjsonU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CrequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CcreateGroupU3Ec__Iterator0_t3134932817, ___U3CrequestU3E__0_2)); }
	inline UnityWebRequest_t254341728 * get_U3CrequestU3E__0_2() const { return ___U3CrequestU3E__0_2; }
	inline UnityWebRequest_t254341728 ** get_address_of_U3CrequestU3E__0_2() { return &___U3CrequestU3E__0_2; }
	inline void set_U3CrequestU3E__0_2(UnityWebRequest_t254341728 * value)
	{
		___U3CrequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__0_2), value);
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(U3CcreateGroupU3Ec__Iterator0_t3134932817, ___callback_3)); }
	inline RuntimeObject* get_callback_3() const { return ___callback_3; }
	inline RuntimeObject** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(RuntimeObject* value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___callback_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CcreateGroupU3Ec__Iterator0_t3134932817, ___U24this_4)); }
	inline GroupCreatorApiHoloImp_t1244970665 * get_U24this_4() const { return ___U24this_4; }
	inline GroupCreatorApiHoloImp_t1244970665 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(GroupCreatorApiHoloImp_t1244970665 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CcreateGroupU3Ec__Iterator0_t3134932817, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CcreateGroupU3Ec__Iterator0_t3134932817, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CcreateGroupU3Ec__Iterator0_t3134932817, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATEGROUPU3EC__ITERATOR0_T3134932817_H
#ifndef GROUPCREATORAPIHOLOIMP_T1244970665_H
#define GROUPCREATORAPIHOLOIMP_T1244970665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Networking.Message.GroupCreatorApiHoloImp
struct  GroupCreatorApiHoloImp_t1244970665  : public RuntimeObject
{
public:
	// Library.Code.Parsers.JsonParser`2<Library.Code.Domain.Entities.GroupCreate1,Library.Code.Domain.Entities.GroupCreateResponse> Library.Code.Networking.Message.GroupCreatorApiHoloImp::_parser
	RuntimeObject* ____parser_0;
	// System.String Library.Code.Networking.Message.GroupCreatorApiHoloImp::ulr
	String_t* ___ulr_1;
	// Library.Code.WebRequest.WebRequestBuilder Library.Code.Networking.Message.GroupCreatorApiHoloImp::_builder
	WebRequestBuilder_t3983605310 * ____builder_2;

public:
	inline static int32_t get_offset_of__parser_0() { return static_cast<int32_t>(offsetof(GroupCreatorApiHoloImp_t1244970665, ____parser_0)); }
	inline RuntimeObject* get__parser_0() const { return ____parser_0; }
	inline RuntimeObject** get_address_of__parser_0() { return &____parser_0; }
	inline void set__parser_0(RuntimeObject* value)
	{
		____parser_0 = value;
		Il2CppCodeGenWriteBarrier((&____parser_0), value);
	}

	inline static int32_t get_offset_of_ulr_1() { return static_cast<int32_t>(offsetof(GroupCreatorApiHoloImp_t1244970665, ___ulr_1)); }
	inline String_t* get_ulr_1() const { return ___ulr_1; }
	inline String_t** get_address_of_ulr_1() { return &___ulr_1; }
	inline void set_ulr_1(String_t* value)
	{
		___ulr_1 = value;
		Il2CppCodeGenWriteBarrier((&___ulr_1), value);
	}

	inline static int32_t get_offset_of__builder_2() { return static_cast<int32_t>(offsetof(GroupCreatorApiHoloImp_t1244970665, ____builder_2)); }
	inline WebRequestBuilder_t3983605310 * get__builder_2() const { return ____builder_2; }
	inline WebRequestBuilder_t3983605310 ** get_address_of__builder_2() { return &____builder_2; }
	inline void set__builder_2(WebRequestBuilder_t3983605310 * value)
	{
		____builder_2 = value;
		Il2CppCodeGenWriteBarrier((&____builder_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPCREATORAPIHOLOIMP_T1244970665_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ACCEPTPOPUP_T454786204_H
#define ACCEPTPOPUP_T454786204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.PopUp.AcceptPopUp
struct  AcceptPopUp_t454786204  : public PopUpHandler_t3865200444
{
public:
	// UnityEngine.UI.Button Library.Code.UI.PopUp.AcceptPopUp::okButton
	Button_t2872111280 * ___okButton_2;
	// UnityEngine.UI.Button Library.Code.UI.PopUp.AcceptPopUp::cancelButton
	Button_t2872111280 * ___cancelButton_3;
	// UnityEngine.UI.Text Library.Code.UI.PopUp.AcceptPopUp::titleText
	Text_t356221433 * ___titleText_4;
	// UnityEngine.UI.Text Library.Code.UI.PopUp.AcceptPopUp::infoText
	Text_t356221433 * ___infoText_5;

public:
	inline static int32_t get_offset_of_okButton_2() { return static_cast<int32_t>(offsetof(AcceptPopUp_t454786204, ___okButton_2)); }
	inline Button_t2872111280 * get_okButton_2() const { return ___okButton_2; }
	inline Button_t2872111280 ** get_address_of_okButton_2() { return &___okButton_2; }
	inline void set_okButton_2(Button_t2872111280 * value)
	{
		___okButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___okButton_2), value);
	}

	inline static int32_t get_offset_of_cancelButton_3() { return static_cast<int32_t>(offsetof(AcceptPopUp_t454786204, ___cancelButton_3)); }
	inline Button_t2872111280 * get_cancelButton_3() const { return ___cancelButton_3; }
	inline Button_t2872111280 ** get_address_of_cancelButton_3() { return &___cancelButton_3; }
	inline void set_cancelButton_3(Button_t2872111280 * value)
	{
		___cancelButton_3 = value;
		Il2CppCodeGenWriteBarrier((&___cancelButton_3), value);
	}

	inline static int32_t get_offset_of_titleText_4() { return static_cast<int32_t>(offsetof(AcceptPopUp_t454786204, ___titleText_4)); }
	inline Text_t356221433 * get_titleText_4() const { return ___titleText_4; }
	inline Text_t356221433 ** get_address_of_titleText_4() { return &___titleText_4; }
	inline void set_titleText_4(Text_t356221433 * value)
	{
		___titleText_4 = value;
		Il2CppCodeGenWriteBarrier((&___titleText_4), value);
	}

	inline static int32_t get_offset_of_infoText_5() { return static_cast<int32_t>(offsetof(AcceptPopUp_t454786204, ___infoText_5)); }
	inline Text_t356221433 * get_infoText_5() const { return ___infoText_5; }
	inline Text_t356221433 ** get_address_of_infoText_5() { return &___infoText_5; }
	inline void set_infoText_5(Text_t356221433 * value)
	{
		___infoText_5 = value;
		Il2CppCodeGenWriteBarrier((&___infoText_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCEPTPOPUP_T454786204_H
#ifndef LOADINGPOPUP_T194048676_H
#define LOADINGPOPUP_T194048676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.PopUp.LoadingPopUp
struct  LoadingPopUp_t194048676  : public PopUpHandler_t3865200444
{
public:
	// UnityEngine.UI.Text Library.Code.UI.PopUp.LoadingPopUp::titleText
	Text_t356221433 * ___titleText_2;
	// UnityEngine.UI.Text Library.Code.UI.PopUp.LoadingPopUp::infoText
	Text_t356221433 * ___infoText_3;

public:
	inline static int32_t get_offset_of_titleText_2() { return static_cast<int32_t>(offsetof(LoadingPopUp_t194048676, ___titleText_2)); }
	inline Text_t356221433 * get_titleText_2() const { return ___titleText_2; }
	inline Text_t356221433 ** get_address_of_titleText_2() { return &___titleText_2; }
	inline void set_titleText_2(Text_t356221433 * value)
	{
		___titleText_2 = value;
		Il2CppCodeGenWriteBarrier((&___titleText_2), value);
	}

	inline static int32_t get_offset_of_infoText_3() { return static_cast<int32_t>(offsetof(LoadingPopUp_t194048676, ___infoText_3)); }
	inline Text_t356221433 * get_infoText_3() const { return ___infoText_3; }
	inline Text_t356221433 ** get_address_of_infoText_3() { return &___infoText_3; }
	inline void set_infoText_3(Text_t356221433 * value)
	{
		___infoText_3 = value;
		Il2CppCodeGenWriteBarrier((&___infoText_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADINGPOPUP_T194048676_H
#ifndef ENGLISHSTRINGS_T2792403630_H
#define ENGLISHSTRINGS_T2792403630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnglishStrings
struct  EnglishStrings_t2792403630  : public LanguageStrings_t1206686884
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENGLISHSTRINGS_T2792403630_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef POPUPS_T1014109545_H
#define POPUPS_T1014109545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.PopUp.PopUps
struct  PopUps_t1014109545 
{
public:
	// System.Int32 Library.Code.UI.PopUp.PopUps::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PopUps_t1014109545, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPUPS_T1014109545_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef ELEMENTSOBSERVERNEW_T2308744357_H
#define ELEMENTSOBSERVERNEW_T2308744357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ChooseModel.ElementsObserverNew
struct  ElementsObserverNew_t2308744357  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Facades.ElementHandler.ElementHandlerFacade Library.Code.UI.ChooseModel.ElementsObserverNew::_elementHandlerFacade
	RuntimeObject* ____elementHandlerFacade_2;
	// Library.Code.UI.PrefabGenerator.PrefabGenerator Library.Code.UI.ChooseModel.ElementsObserverNew::_prefabGenerator
	RuntimeObject* ____prefabGenerator_3;
	// Library.Code.Facades.Elements.ElementFacade Library.Code.UI.ChooseModel.ElementsObserverNew::_elementFacade
	RuntimeObject* ____elementFacade_4;
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Element> Library.Code.UI.ChooseModel.ElementsObserverNew::_elements
	List_1_t1645709140 * ____elements_5;
	// UnityEngine.UI.Button Library.Code.UI.ChooseModel.ElementsObserverNew::backButton
	Button_t2872111280 * ___backButton_6;
	// Library.Code.DI.PopUp.PopUpShower Library.Code.UI.ChooseModel.ElementsObserverNew::_showerPopUps
	PopUpShower_t3186557280 * ____showerPopUps_7;
	// Library.Code.Facades._3dElementDownload.Element3dFacade Library.Code.UI.ChooseModel.ElementsObserverNew::element3DFacade
	RuntimeObject* ___element3DFacade_8;
	// Library.Code.UI.ChooseModel.ModelInterface Library.Code.UI.ChooseModel.ElementsObserverNew::_modelInterface
	RuntimeObject* ____modelInterface_9;
	// System.Int64 Library.Code.UI.ChooseModel.ElementsObserverNew::lasteElementLoaded
	int64_t ___lasteElementLoaded_10;
	// UnityEngine.GameObject Library.Code.UI.ChooseModel.ElementsObserverNew::loadingPopUp
	GameObject_t1756533147 * ___loadingPopUp_11;

public:
	inline static int32_t get_offset_of__elementHandlerFacade_2() { return static_cast<int32_t>(offsetof(ElementsObserverNew_t2308744357, ____elementHandlerFacade_2)); }
	inline RuntimeObject* get__elementHandlerFacade_2() const { return ____elementHandlerFacade_2; }
	inline RuntimeObject** get_address_of__elementHandlerFacade_2() { return &____elementHandlerFacade_2; }
	inline void set__elementHandlerFacade_2(RuntimeObject* value)
	{
		____elementHandlerFacade_2 = value;
		Il2CppCodeGenWriteBarrier((&____elementHandlerFacade_2), value);
	}

	inline static int32_t get_offset_of__prefabGenerator_3() { return static_cast<int32_t>(offsetof(ElementsObserverNew_t2308744357, ____prefabGenerator_3)); }
	inline RuntimeObject* get__prefabGenerator_3() const { return ____prefabGenerator_3; }
	inline RuntimeObject** get_address_of__prefabGenerator_3() { return &____prefabGenerator_3; }
	inline void set__prefabGenerator_3(RuntimeObject* value)
	{
		____prefabGenerator_3 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_3), value);
	}

	inline static int32_t get_offset_of__elementFacade_4() { return static_cast<int32_t>(offsetof(ElementsObserverNew_t2308744357, ____elementFacade_4)); }
	inline RuntimeObject* get__elementFacade_4() const { return ____elementFacade_4; }
	inline RuntimeObject** get_address_of__elementFacade_4() { return &____elementFacade_4; }
	inline void set__elementFacade_4(RuntimeObject* value)
	{
		____elementFacade_4 = value;
		Il2CppCodeGenWriteBarrier((&____elementFacade_4), value);
	}

	inline static int32_t get_offset_of__elements_5() { return static_cast<int32_t>(offsetof(ElementsObserverNew_t2308744357, ____elements_5)); }
	inline List_1_t1645709140 * get__elements_5() const { return ____elements_5; }
	inline List_1_t1645709140 ** get_address_of__elements_5() { return &____elements_5; }
	inline void set__elements_5(List_1_t1645709140 * value)
	{
		____elements_5 = value;
		Il2CppCodeGenWriteBarrier((&____elements_5), value);
	}

	inline static int32_t get_offset_of_backButton_6() { return static_cast<int32_t>(offsetof(ElementsObserverNew_t2308744357, ___backButton_6)); }
	inline Button_t2872111280 * get_backButton_6() const { return ___backButton_6; }
	inline Button_t2872111280 ** get_address_of_backButton_6() { return &___backButton_6; }
	inline void set_backButton_6(Button_t2872111280 * value)
	{
		___backButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___backButton_6), value);
	}

	inline static int32_t get_offset_of__showerPopUps_7() { return static_cast<int32_t>(offsetof(ElementsObserverNew_t2308744357, ____showerPopUps_7)); }
	inline PopUpShower_t3186557280 * get__showerPopUps_7() const { return ____showerPopUps_7; }
	inline PopUpShower_t3186557280 ** get_address_of__showerPopUps_7() { return &____showerPopUps_7; }
	inline void set__showerPopUps_7(PopUpShower_t3186557280 * value)
	{
		____showerPopUps_7 = value;
		Il2CppCodeGenWriteBarrier((&____showerPopUps_7), value);
	}

	inline static int32_t get_offset_of_element3DFacade_8() { return static_cast<int32_t>(offsetof(ElementsObserverNew_t2308744357, ___element3DFacade_8)); }
	inline RuntimeObject* get_element3DFacade_8() const { return ___element3DFacade_8; }
	inline RuntimeObject** get_address_of_element3DFacade_8() { return &___element3DFacade_8; }
	inline void set_element3DFacade_8(RuntimeObject* value)
	{
		___element3DFacade_8 = value;
		Il2CppCodeGenWriteBarrier((&___element3DFacade_8), value);
	}

	inline static int32_t get_offset_of__modelInterface_9() { return static_cast<int32_t>(offsetof(ElementsObserverNew_t2308744357, ____modelInterface_9)); }
	inline RuntimeObject* get__modelInterface_9() const { return ____modelInterface_9; }
	inline RuntimeObject** get_address_of__modelInterface_9() { return &____modelInterface_9; }
	inline void set__modelInterface_9(RuntimeObject* value)
	{
		____modelInterface_9 = value;
		Il2CppCodeGenWriteBarrier((&____modelInterface_9), value);
	}

	inline static int32_t get_offset_of_lasteElementLoaded_10() { return static_cast<int32_t>(offsetof(ElementsObserverNew_t2308744357, ___lasteElementLoaded_10)); }
	inline int64_t get_lasteElementLoaded_10() const { return ___lasteElementLoaded_10; }
	inline int64_t* get_address_of_lasteElementLoaded_10() { return &___lasteElementLoaded_10; }
	inline void set_lasteElementLoaded_10(int64_t value)
	{
		___lasteElementLoaded_10 = value;
	}

	inline static int32_t get_offset_of_loadingPopUp_11() { return static_cast<int32_t>(offsetof(ElementsObserverNew_t2308744357, ___loadingPopUp_11)); }
	inline GameObject_t1756533147 * get_loadingPopUp_11() const { return ___loadingPopUp_11; }
	inline GameObject_t1756533147 ** get_address_of_loadingPopUp_11() { return &___loadingPopUp_11; }
	inline void set_loadingPopUp_11(GameObject_t1756533147 * value)
	{
		___loadingPopUp_11 = value;
		Il2CppCodeGenWriteBarrier((&___loadingPopUp_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTSOBSERVERNEW_T2308744357_H
#ifndef CHECKLISTROW_T2468600002_H
#define CHECKLISTROW_T2468600002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ChecklistUI.CheckListRow
struct  CheckListRow_t2468600002  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Toggle Library.Code.UI.ChecklistUI.CheckListRow::checkboxAccept
	Toggle_t3976754468 * ___checkboxAccept_2;
	// UnityEngine.UI.Toggle Library.Code.UI.ChecklistUI.CheckListRow::checkboxUntouched
	Toggle_t3976754468 * ___checkboxUntouched_3;
	// UnityEngine.UI.Toggle Library.Code.UI.ChecklistUI.CheckListRow::checkboxRejected
	Toggle_t3976754468 * ___checkboxRejected_4;
	// UnityEngine.UI.Text Library.Code.UI.ChecklistUI.CheckListRow::text
	Text_t356221433 * ___text_5;
	// System.Boolean Library.Code.UI.ChecklistUI.CheckListRow::isAwake
	bool ___isAwake_6;
	// Library.Code.Domain.Entities.Checklist.ItemStatus Library.Code.UI.ChecklistUI.CheckListRow::itemStatus
	ItemStatus_t1378751697 * ___itemStatus_7;
	// UnityEngine.Events.UnityAction`1<Library.Code.Domain.Dtos.ItemStatusWithCheckpoint> Library.Code.UI.ChecklistUI.CheckListRow::actionOnCheckChange
	UnityAction_1_t3024356692 * ___actionOnCheckChange_8;
	// Library.Code.Domain.Dtos.ItemStatusWithCheckpoint Library.Code.UI.ChecklistUI.CheckListRow::itemStatusWithCheckpoint
	ItemStatusWithCheckpoint_t1657770941 * ___itemStatusWithCheckpoint_9;

public:
	inline static int32_t get_offset_of_checkboxAccept_2() { return static_cast<int32_t>(offsetof(CheckListRow_t2468600002, ___checkboxAccept_2)); }
	inline Toggle_t3976754468 * get_checkboxAccept_2() const { return ___checkboxAccept_2; }
	inline Toggle_t3976754468 ** get_address_of_checkboxAccept_2() { return &___checkboxAccept_2; }
	inline void set_checkboxAccept_2(Toggle_t3976754468 * value)
	{
		___checkboxAccept_2 = value;
		Il2CppCodeGenWriteBarrier((&___checkboxAccept_2), value);
	}

	inline static int32_t get_offset_of_checkboxUntouched_3() { return static_cast<int32_t>(offsetof(CheckListRow_t2468600002, ___checkboxUntouched_3)); }
	inline Toggle_t3976754468 * get_checkboxUntouched_3() const { return ___checkboxUntouched_3; }
	inline Toggle_t3976754468 ** get_address_of_checkboxUntouched_3() { return &___checkboxUntouched_3; }
	inline void set_checkboxUntouched_3(Toggle_t3976754468 * value)
	{
		___checkboxUntouched_3 = value;
		Il2CppCodeGenWriteBarrier((&___checkboxUntouched_3), value);
	}

	inline static int32_t get_offset_of_checkboxRejected_4() { return static_cast<int32_t>(offsetof(CheckListRow_t2468600002, ___checkboxRejected_4)); }
	inline Toggle_t3976754468 * get_checkboxRejected_4() const { return ___checkboxRejected_4; }
	inline Toggle_t3976754468 ** get_address_of_checkboxRejected_4() { return &___checkboxRejected_4; }
	inline void set_checkboxRejected_4(Toggle_t3976754468 * value)
	{
		___checkboxRejected_4 = value;
		Il2CppCodeGenWriteBarrier((&___checkboxRejected_4), value);
	}

	inline static int32_t get_offset_of_text_5() { return static_cast<int32_t>(offsetof(CheckListRow_t2468600002, ___text_5)); }
	inline Text_t356221433 * get_text_5() const { return ___text_5; }
	inline Text_t356221433 ** get_address_of_text_5() { return &___text_5; }
	inline void set_text_5(Text_t356221433 * value)
	{
		___text_5 = value;
		Il2CppCodeGenWriteBarrier((&___text_5), value);
	}

	inline static int32_t get_offset_of_isAwake_6() { return static_cast<int32_t>(offsetof(CheckListRow_t2468600002, ___isAwake_6)); }
	inline bool get_isAwake_6() const { return ___isAwake_6; }
	inline bool* get_address_of_isAwake_6() { return &___isAwake_6; }
	inline void set_isAwake_6(bool value)
	{
		___isAwake_6 = value;
	}

	inline static int32_t get_offset_of_itemStatus_7() { return static_cast<int32_t>(offsetof(CheckListRow_t2468600002, ___itemStatus_7)); }
	inline ItemStatus_t1378751697 * get_itemStatus_7() const { return ___itemStatus_7; }
	inline ItemStatus_t1378751697 ** get_address_of_itemStatus_7() { return &___itemStatus_7; }
	inline void set_itemStatus_7(ItemStatus_t1378751697 * value)
	{
		___itemStatus_7 = value;
		Il2CppCodeGenWriteBarrier((&___itemStatus_7), value);
	}

	inline static int32_t get_offset_of_actionOnCheckChange_8() { return static_cast<int32_t>(offsetof(CheckListRow_t2468600002, ___actionOnCheckChange_8)); }
	inline UnityAction_1_t3024356692 * get_actionOnCheckChange_8() const { return ___actionOnCheckChange_8; }
	inline UnityAction_1_t3024356692 ** get_address_of_actionOnCheckChange_8() { return &___actionOnCheckChange_8; }
	inline void set_actionOnCheckChange_8(UnityAction_1_t3024356692 * value)
	{
		___actionOnCheckChange_8 = value;
		Il2CppCodeGenWriteBarrier((&___actionOnCheckChange_8), value);
	}

	inline static int32_t get_offset_of_itemStatusWithCheckpoint_9() { return static_cast<int32_t>(offsetof(CheckListRow_t2468600002, ___itemStatusWithCheckpoint_9)); }
	inline ItemStatusWithCheckpoint_t1657770941 * get_itemStatusWithCheckpoint_9() const { return ___itemStatusWithCheckpoint_9; }
	inline ItemStatusWithCheckpoint_t1657770941 ** get_address_of_itemStatusWithCheckpoint_9() { return &___itemStatusWithCheckpoint_9; }
	inline void set_itemStatusWithCheckpoint_9(ItemStatusWithCheckpoint_t1657770941 * value)
	{
		___itemStatusWithCheckpoint_9 = value;
		Il2CppCodeGenWriteBarrier((&___itemStatusWithCheckpoint_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKLISTROW_T2468600002_H
#ifndef PHOTOPOPUP_T4101423206_H
#define PHOTOPOPUP_T4101423206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotoPopup
struct  PhotoPopup_t4101423206  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button PhotoPopup::closeButton
	Button_t2872111280 * ___closeButton_2;
	// UnityEngine.UI.Image PhotoPopup::photoImage
	Image_t2042527209 * ___photoImage_3;
	// TMPro.TextMeshProUGUI PhotoPopup::imageDateTime
	TextMeshProUGUI_t934157183 * ___imageDateTime_4;
	// TMPro.TextMeshProUGUI PhotoPopup::mainFileName
	TextMeshProUGUI_t934157183 * ___mainFileName_5;
	// TMPro.TextMeshProUGUI PhotoPopup::descriptionText
	TextMeshProUGUI_t934157183 * ___descriptionText_6;
	// UnityEngine.RectTransform PhotoPopup::scrollContents
	RectTransform_t3349966182 * ___scrollContents_7;
	// UnityEngine.CanvasGroup PhotoPopup::canvasGroup
	CanvasGroup_t3296560743 * ___canvasGroup_8;
	// UnityEngine.UI.Button PhotoPopup::deleteButton
	Button_t2872111280 * ___deleteButton_9;
	// Library.Code.Facades.Messages.MessagesFacade PhotoPopup::_messagesFacade
	RuntimeObject* ____messagesFacade_10;
	// Library.Code.Domain.Entities.Message PhotoPopup::currentMessage
	Message_t1214853957 * ___currentMessage_11;
	// Library.Code.DI.PopUp.PopUpShower PhotoPopup::_popUpShower
	PopUpShower_t3186557280 * ____popUpShower_12;
	// Library.Code.UI.PrefabGenerator.PrefabGenerator PhotoPopup::prefabGenerator
	RuntimeObject* ___prefabGenerator_13;
	// System.Collections.Generic.List`1<PhotoHistory> PhotoPopup::existingPhotoHistory
	List_1_t3591931956 * ___existingPhotoHistory_14;

public:
	inline static int32_t get_offset_of_closeButton_2() { return static_cast<int32_t>(offsetof(PhotoPopup_t4101423206, ___closeButton_2)); }
	inline Button_t2872111280 * get_closeButton_2() const { return ___closeButton_2; }
	inline Button_t2872111280 ** get_address_of_closeButton_2() { return &___closeButton_2; }
	inline void set_closeButton_2(Button_t2872111280 * value)
	{
		___closeButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___closeButton_2), value);
	}

	inline static int32_t get_offset_of_photoImage_3() { return static_cast<int32_t>(offsetof(PhotoPopup_t4101423206, ___photoImage_3)); }
	inline Image_t2042527209 * get_photoImage_3() const { return ___photoImage_3; }
	inline Image_t2042527209 ** get_address_of_photoImage_3() { return &___photoImage_3; }
	inline void set_photoImage_3(Image_t2042527209 * value)
	{
		___photoImage_3 = value;
		Il2CppCodeGenWriteBarrier((&___photoImage_3), value);
	}

	inline static int32_t get_offset_of_imageDateTime_4() { return static_cast<int32_t>(offsetof(PhotoPopup_t4101423206, ___imageDateTime_4)); }
	inline TextMeshProUGUI_t934157183 * get_imageDateTime_4() const { return ___imageDateTime_4; }
	inline TextMeshProUGUI_t934157183 ** get_address_of_imageDateTime_4() { return &___imageDateTime_4; }
	inline void set_imageDateTime_4(TextMeshProUGUI_t934157183 * value)
	{
		___imageDateTime_4 = value;
		Il2CppCodeGenWriteBarrier((&___imageDateTime_4), value);
	}

	inline static int32_t get_offset_of_mainFileName_5() { return static_cast<int32_t>(offsetof(PhotoPopup_t4101423206, ___mainFileName_5)); }
	inline TextMeshProUGUI_t934157183 * get_mainFileName_5() const { return ___mainFileName_5; }
	inline TextMeshProUGUI_t934157183 ** get_address_of_mainFileName_5() { return &___mainFileName_5; }
	inline void set_mainFileName_5(TextMeshProUGUI_t934157183 * value)
	{
		___mainFileName_5 = value;
		Il2CppCodeGenWriteBarrier((&___mainFileName_5), value);
	}

	inline static int32_t get_offset_of_descriptionText_6() { return static_cast<int32_t>(offsetof(PhotoPopup_t4101423206, ___descriptionText_6)); }
	inline TextMeshProUGUI_t934157183 * get_descriptionText_6() const { return ___descriptionText_6; }
	inline TextMeshProUGUI_t934157183 ** get_address_of_descriptionText_6() { return &___descriptionText_6; }
	inline void set_descriptionText_6(TextMeshProUGUI_t934157183 * value)
	{
		___descriptionText_6 = value;
		Il2CppCodeGenWriteBarrier((&___descriptionText_6), value);
	}

	inline static int32_t get_offset_of_scrollContents_7() { return static_cast<int32_t>(offsetof(PhotoPopup_t4101423206, ___scrollContents_7)); }
	inline RectTransform_t3349966182 * get_scrollContents_7() const { return ___scrollContents_7; }
	inline RectTransform_t3349966182 ** get_address_of_scrollContents_7() { return &___scrollContents_7; }
	inline void set_scrollContents_7(RectTransform_t3349966182 * value)
	{
		___scrollContents_7 = value;
		Il2CppCodeGenWriteBarrier((&___scrollContents_7), value);
	}

	inline static int32_t get_offset_of_canvasGroup_8() { return static_cast<int32_t>(offsetof(PhotoPopup_t4101423206, ___canvasGroup_8)); }
	inline CanvasGroup_t3296560743 * get_canvasGroup_8() const { return ___canvasGroup_8; }
	inline CanvasGroup_t3296560743 ** get_address_of_canvasGroup_8() { return &___canvasGroup_8; }
	inline void set_canvasGroup_8(CanvasGroup_t3296560743 * value)
	{
		___canvasGroup_8 = value;
		Il2CppCodeGenWriteBarrier((&___canvasGroup_8), value);
	}

	inline static int32_t get_offset_of_deleteButton_9() { return static_cast<int32_t>(offsetof(PhotoPopup_t4101423206, ___deleteButton_9)); }
	inline Button_t2872111280 * get_deleteButton_9() const { return ___deleteButton_9; }
	inline Button_t2872111280 ** get_address_of_deleteButton_9() { return &___deleteButton_9; }
	inline void set_deleteButton_9(Button_t2872111280 * value)
	{
		___deleteButton_9 = value;
		Il2CppCodeGenWriteBarrier((&___deleteButton_9), value);
	}

	inline static int32_t get_offset_of__messagesFacade_10() { return static_cast<int32_t>(offsetof(PhotoPopup_t4101423206, ____messagesFacade_10)); }
	inline RuntimeObject* get__messagesFacade_10() const { return ____messagesFacade_10; }
	inline RuntimeObject** get_address_of__messagesFacade_10() { return &____messagesFacade_10; }
	inline void set__messagesFacade_10(RuntimeObject* value)
	{
		____messagesFacade_10 = value;
		Il2CppCodeGenWriteBarrier((&____messagesFacade_10), value);
	}

	inline static int32_t get_offset_of_currentMessage_11() { return static_cast<int32_t>(offsetof(PhotoPopup_t4101423206, ___currentMessage_11)); }
	inline Message_t1214853957 * get_currentMessage_11() const { return ___currentMessage_11; }
	inline Message_t1214853957 ** get_address_of_currentMessage_11() { return &___currentMessage_11; }
	inline void set_currentMessage_11(Message_t1214853957 * value)
	{
		___currentMessage_11 = value;
		Il2CppCodeGenWriteBarrier((&___currentMessage_11), value);
	}

	inline static int32_t get_offset_of__popUpShower_12() { return static_cast<int32_t>(offsetof(PhotoPopup_t4101423206, ____popUpShower_12)); }
	inline PopUpShower_t3186557280 * get__popUpShower_12() const { return ____popUpShower_12; }
	inline PopUpShower_t3186557280 ** get_address_of__popUpShower_12() { return &____popUpShower_12; }
	inline void set__popUpShower_12(PopUpShower_t3186557280 * value)
	{
		____popUpShower_12 = value;
		Il2CppCodeGenWriteBarrier((&____popUpShower_12), value);
	}

	inline static int32_t get_offset_of_prefabGenerator_13() { return static_cast<int32_t>(offsetof(PhotoPopup_t4101423206, ___prefabGenerator_13)); }
	inline RuntimeObject* get_prefabGenerator_13() const { return ___prefabGenerator_13; }
	inline RuntimeObject** get_address_of_prefabGenerator_13() { return &___prefabGenerator_13; }
	inline void set_prefabGenerator_13(RuntimeObject* value)
	{
		___prefabGenerator_13 = value;
		Il2CppCodeGenWriteBarrier((&___prefabGenerator_13), value);
	}

	inline static int32_t get_offset_of_existingPhotoHistory_14() { return static_cast<int32_t>(offsetof(PhotoPopup_t4101423206, ___existingPhotoHistory_14)); }
	inline List_1_t3591931956 * get_existingPhotoHistory_14() const { return ___existingPhotoHistory_14; }
	inline List_1_t3591931956 ** get_address_of_existingPhotoHistory_14() { return &___existingPhotoHistory_14; }
	inline void set_existingPhotoHistory_14(List_1_t3591931956 * value)
	{
		___existingPhotoHistory_14 = value;
		Il2CppCodeGenWriteBarrier((&___existingPhotoHistory_14), value);
	}
};

struct PhotoPopup_t4101423206_StaticFields
{
public:
	// System.Action PhotoPopup::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_15() { return static_cast<int32_t>(offsetof(PhotoPopup_t4101423206_StaticFields, ___U3CU3Ef__amU24cache0_15)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_15() const { return ___U3CU3Ef__amU24cache0_15; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_15() { return &___U3CU3Ef__amU24cache0_15; }
	inline void set_U3CU3Ef__amU24cache0_15(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTOPOPUP_T4101423206_H
#ifndef PHOTOHISTORY_T4222810824_H
#define PHOTOHISTORY_T4222810824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotoHistory
struct  PhotoHistory_t4222810824  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image PhotoHistory::thumbnail
	Image_t2042527209 * ___thumbnail_2;
	// TMPro.TextMeshProUGUI PhotoHistory::date
	TextMeshProUGUI_t934157183 * ___date_3;
	// TMPro.TextMeshProUGUI PhotoHistory::filename
	TextMeshProUGUI_t934157183 * ___filename_4;
	// UnityEngine.RectTransform PhotoHistory::rectTransform
	RectTransform_t3349966182 * ___rectTransform_5;
	// System.Action`3<UnityEngine.Sprite,System.String,System.String> PhotoHistory::onClick
	Action_3_t2024507103 * ___onClick_6;

public:
	inline static int32_t get_offset_of_thumbnail_2() { return static_cast<int32_t>(offsetof(PhotoHistory_t4222810824, ___thumbnail_2)); }
	inline Image_t2042527209 * get_thumbnail_2() const { return ___thumbnail_2; }
	inline Image_t2042527209 ** get_address_of_thumbnail_2() { return &___thumbnail_2; }
	inline void set_thumbnail_2(Image_t2042527209 * value)
	{
		___thumbnail_2 = value;
		Il2CppCodeGenWriteBarrier((&___thumbnail_2), value);
	}

	inline static int32_t get_offset_of_date_3() { return static_cast<int32_t>(offsetof(PhotoHistory_t4222810824, ___date_3)); }
	inline TextMeshProUGUI_t934157183 * get_date_3() const { return ___date_3; }
	inline TextMeshProUGUI_t934157183 ** get_address_of_date_3() { return &___date_3; }
	inline void set_date_3(TextMeshProUGUI_t934157183 * value)
	{
		___date_3 = value;
		Il2CppCodeGenWriteBarrier((&___date_3), value);
	}

	inline static int32_t get_offset_of_filename_4() { return static_cast<int32_t>(offsetof(PhotoHistory_t4222810824, ___filename_4)); }
	inline TextMeshProUGUI_t934157183 * get_filename_4() const { return ___filename_4; }
	inline TextMeshProUGUI_t934157183 ** get_address_of_filename_4() { return &___filename_4; }
	inline void set_filename_4(TextMeshProUGUI_t934157183 * value)
	{
		___filename_4 = value;
		Il2CppCodeGenWriteBarrier((&___filename_4), value);
	}

	inline static int32_t get_offset_of_rectTransform_5() { return static_cast<int32_t>(offsetof(PhotoHistory_t4222810824, ___rectTransform_5)); }
	inline RectTransform_t3349966182 * get_rectTransform_5() const { return ___rectTransform_5; }
	inline RectTransform_t3349966182 ** get_address_of_rectTransform_5() { return &___rectTransform_5; }
	inline void set_rectTransform_5(RectTransform_t3349966182 * value)
	{
		___rectTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_5), value);
	}

	inline static int32_t get_offset_of_onClick_6() { return static_cast<int32_t>(offsetof(PhotoHistory_t4222810824, ___onClick_6)); }
	inline Action_3_t2024507103 * get_onClick_6() const { return ___onClick_6; }
	inline Action_3_t2024507103 ** get_address_of_onClick_6() { return &___onClick_6; }
	inline void set_onClick_6(Action_3_t2024507103 * value)
	{
		___onClick_6 = value;
		Il2CppCodeGenWriteBarrier((&___onClick_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHOTOHISTORY_T4222810824_H
#ifndef MOVEELEMENT_T2302326803_H
#define MOVEELEMENT_T2302326803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MoveElement
struct  MoveElement_t2302326803  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 Library.Code.UI.MoveElement::index
	int32_t ___index_2;
	// UnityEngine.UI.Toggle Library.Code.UI.MoveElement::move
	Toggle_t3976754468 * ___move_3;
	// System.Single Library.Code.UI.MoveElement::speed
	float ___speed_4;
	// System.Single Library.Code.UI.MoveElement::tempX
	float ___tempX_5;
	// System.Boolean Library.Code.UI.MoveElement::isloaded
	bool ___isloaded_6;
	// System.Single Library.Code.UI.MoveElement::rotX
	float ___rotX_7;
	// UnityEngine.GameObject Library.Code.UI.MoveElement::gObject
	GameObject_t1756533147 * ___gObject_8;
	// System.Int32 Library.Code.UI.MoveElement::tiltAngle
	int32_t ___tiltAngle_9;
	// UnityEngine.Camera Library.Code.UI.MoveElement::selectedCamera
	Camera_t189460977 * ___selectedCamera_10;
	// System.Single Library.Code.UI.MoveElement::perspectiveZoomSpeed
	float ___perspectiveZoomSpeed_11;
	// System.Single Library.Code.UI.MoveElement::orthoZoomSpeed
	float ___orthoZoomSpeed_12;
	// System.Single Library.Code.UI.MoveElement::dist
	float ___dist_13;
	// System.Boolean Library.Code.UI.MoveElement::dragging
	bool ___dragging_14;
	// UnityEngine.Transform Library.Code.UI.MoveElement::toDrag
	Transform_t3275118058 * ___toDrag_15;
	// UnityEngine.Vector3 Library.Code.UI.MoveElement::offset
	Vector3_t2243707580  ___offset_16;

public:
	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(MoveElement_t2302326803, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_move_3() { return static_cast<int32_t>(offsetof(MoveElement_t2302326803, ___move_3)); }
	inline Toggle_t3976754468 * get_move_3() const { return ___move_3; }
	inline Toggle_t3976754468 ** get_address_of_move_3() { return &___move_3; }
	inline void set_move_3(Toggle_t3976754468 * value)
	{
		___move_3 = value;
		Il2CppCodeGenWriteBarrier((&___move_3), value);
	}

	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(MoveElement_t2302326803, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_tempX_5() { return static_cast<int32_t>(offsetof(MoveElement_t2302326803, ___tempX_5)); }
	inline float get_tempX_5() const { return ___tempX_5; }
	inline float* get_address_of_tempX_5() { return &___tempX_5; }
	inline void set_tempX_5(float value)
	{
		___tempX_5 = value;
	}

	inline static int32_t get_offset_of_isloaded_6() { return static_cast<int32_t>(offsetof(MoveElement_t2302326803, ___isloaded_6)); }
	inline bool get_isloaded_6() const { return ___isloaded_6; }
	inline bool* get_address_of_isloaded_6() { return &___isloaded_6; }
	inline void set_isloaded_6(bool value)
	{
		___isloaded_6 = value;
	}

	inline static int32_t get_offset_of_rotX_7() { return static_cast<int32_t>(offsetof(MoveElement_t2302326803, ___rotX_7)); }
	inline float get_rotX_7() const { return ___rotX_7; }
	inline float* get_address_of_rotX_7() { return &___rotX_7; }
	inline void set_rotX_7(float value)
	{
		___rotX_7 = value;
	}

	inline static int32_t get_offset_of_gObject_8() { return static_cast<int32_t>(offsetof(MoveElement_t2302326803, ___gObject_8)); }
	inline GameObject_t1756533147 * get_gObject_8() const { return ___gObject_8; }
	inline GameObject_t1756533147 ** get_address_of_gObject_8() { return &___gObject_8; }
	inline void set_gObject_8(GameObject_t1756533147 * value)
	{
		___gObject_8 = value;
		Il2CppCodeGenWriteBarrier((&___gObject_8), value);
	}

	inline static int32_t get_offset_of_tiltAngle_9() { return static_cast<int32_t>(offsetof(MoveElement_t2302326803, ___tiltAngle_9)); }
	inline int32_t get_tiltAngle_9() const { return ___tiltAngle_9; }
	inline int32_t* get_address_of_tiltAngle_9() { return &___tiltAngle_9; }
	inline void set_tiltAngle_9(int32_t value)
	{
		___tiltAngle_9 = value;
	}

	inline static int32_t get_offset_of_selectedCamera_10() { return static_cast<int32_t>(offsetof(MoveElement_t2302326803, ___selectedCamera_10)); }
	inline Camera_t189460977 * get_selectedCamera_10() const { return ___selectedCamera_10; }
	inline Camera_t189460977 ** get_address_of_selectedCamera_10() { return &___selectedCamera_10; }
	inline void set_selectedCamera_10(Camera_t189460977 * value)
	{
		___selectedCamera_10 = value;
		Il2CppCodeGenWriteBarrier((&___selectedCamera_10), value);
	}

	inline static int32_t get_offset_of_perspectiveZoomSpeed_11() { return static_cast<int32_t>(offsetof(MoveElement_t2302326803, ___perspectiveZoomSpeed_11)); }
	inline float get_perspectiveZoomSpeed_11() const { return ___perspectiveZoomSpeed_11; }
	inline float* get_address_of_perspectiveZoomSpeed_11() { return &___perspectiveZoomSpeed_11; }
	inline void set_perspectiveZoomSpeed_11(float value)
	{
		___perspectiveZoomSpeed_11 = value;
	}

	inline static int32_t get_offset_of_orthoZoomSpeed_12() { return static_cast<int32_t>(offsetof(MoveElement_t2302326803, ___orthoZoomSpeed_12)); }
	inline float get_orthoZoomSpeed_12() const { return ___orthoZoomSpeed_12; }
	inline float* get_address_of_orthoZoomSpeed_12() { return &___orthoZoomSpeed_12; }
	inline void set_orthoZoomSpeed_12(float value)
	{
		___orthoZoomSpeed_12 = value;
	}

	inline static int32_t get_offset_of_dist_13() { return static_cast<int32_t>(offsetof(MoveElement_t2302326803, ___dist_13)); }
	inline float get_dist_13() const { return ___dist_13; }
	inline float* get_address_of_dist_13() { return &___dist_13; }
	inline void set_dist_13(float value)
	{
		___dist_13 = value;
	}

	inline static int32_t get_offset_of_dragging_14() { return static_cast<int32_t>(offsetof(MoveElement_t2302326803, ___dragging_14)); }
	inline bool get_dragging_14() const { return ___dragging_14; }
	inline bool* get_address_of_dragging_14() { return &___dragging_14; }
	inline void set_dragging_14(bool value)
	{
		___dragging_14 = value;
	}

	inline static int32_t get_offset_of_toDrag_15() { return static_cast<int32_t>(offsetof(MoveElement_t2302326803, ___toDrag_15)); }
	inline Transform_t3275118058 * get_toDrag_15() const { return ___toDrag_15; }
	inline Transform_t3275118058 ** get_address_of_toDrag_15() { return &___toDrag_15; }
	inline void set_toDrag_15(Transform_t3275118058 * value)
	{
		___toDrag_15 = value;
		Il2CppCodeGenWriteBarrier((&___toDrag_15), value);
	}

	inline static int32_t get_offset_of_offset_16() { return static_cast<int32_t>(offsetof(MoveElement_t2302326803, ___offset_16)); }
	inline Vector3_t2243707580  get_offset_16() const { return ___offset_16; }
	inline Vector3_t2243707580 * get_address_of_offset_16() { return &___offset_16; }
	inline void set_offset_16(Vector3_t2243707580  value)
	{
		___offset_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEELEMENT_T2302326803_H
#ifndef TEMPLATEROWHANDLER_T1160348300_H
#define TEMPLATEROWHANDLER_T1160348300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MessageTemplates.TemplateRowHandler
struct  TemplateRowHandler_t1160348300  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Domain.Entities.MessageTemplate Library.Code.UI.MessageTemplates.TemplateRowHandler::_messageTemplate
	MessageTemplate_t2793553551 * ____messageTemplate_2;
	// Library.Code.UI.MessageTemplates.FillUpWithMessageTemplate Library.Code.UI.MessageTemplates.TemplateRowHandler::_FillUpWithMessageTemplate
	RuntimeObject* ____FillUpWithMessageTemplate_3;
	// UnityEngine.UI.Button Library.Code.UI.MessageTemplates.TemplateRowHandler::text
	Button_t2872111280 * ___text_4;

public:
	inline static int32_t get_offset_of__messageTemplate_2() { return static_cast<int32_t>(offsetof(TemplateRowHandler_t1160348300, ____messageTemplate_2)); }
	inline MessageTemplate_t2793553551 * get__messageTemplate_2() const { return ____messageTemplate_2; }
	inline MessageTemplate_t2793553551 ** get_address_of__messageTemplate_2() { return &____messageTemplate_2; }
	inline void set__messageTemplate_2(MessageTemplate_t2793553551 * value)
	{
		____messageTemplate_2 = value;
		Il2CppCodeGenWriteBarrier((&____messageTemplate_2), value);
	}

	inline static int32_t get_offset_of__FillUpWithMessageTemplate_3() { return static_cast<int32_t>(offsetof(TemplateRowHandler_t1160348300, ____FillUpWithMessageTemplate_3)); }
	inline RuntimeObject* get__FillUpWithMessageTemplate_3() const { return ____FillUpWithMessageTemplate_3; }
	inline RuntimeObject** get_address_of__FillUpWithMessageTemplate_3() { return &____FillUpWithMessageTemplate_3; }
	inline void set__FillUpWithMessageTemplate_3(RuntimeObject* value)
	{
		____FillUpWithMessageTemplate_3 = value;
		Il2CppCodeGenWriteBarrier((&____FillUpWithMessageTemplate_3), value);
	}

	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(TemplateRowHandler_t1160348300, ___text_4)); }
	inline Button_t2872111280 * get_text_4() const { return ___text_4; }
	inline Button_t2872111280 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(Button_t2872111280 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEMPLATEROWHANDLER_T1160348300_H
#ifndef MODELELEMENTOBSERVER_T2747689887_H
#define MODELELEMENTOBSERVER_T2747689887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModelElementObserver
struct  ModelElementObserver_t2747689887  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Facades.Models.ModelFacade ModelElementObserver::_modelFacade
	RuntimeObject* ____modelFacade_2;
	// Library.Code.UI.PrefabGenerator.PrefabGenerator ModelElementObserver::_prefabGenerator
	RuntimeObject* ____prefabGenerator_3;
	// Library.Code.Facades.Elements.ElementFacade ModelElementObserver::_elementFacade
	RuntimeObject* ____elementFacade_4;
	// Library.Code.Facades._3dElementDownload.Element3dFacade ModelElementObserver::_element3dFacade
	RuntimeObject* ____element3dFacade_5;
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model> ModelElementObserver::_models
	List_1_t1532750683 * ____models_6;
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Element> ModelElementObserver::_elements
	List_1_t1645709140 * ____elements_7;
	// System.Boolean ModelElementObserver::modelIsChoosen
	bool ___modelIsChoosen_8;
	// System.Boolean ModelElementObserver::started
	bool ___started_9;

public:
	inline static int32_t get_offset_of__modelFacade_2() { return static_cast<int32_t>(offsetof(ModelElementObserver_t2747689887, ____modelFacade_2)); }
	inline RuntimeObject* get__modelFacade_2() const { return ____modelFacade_2; }
	inline RuntimeObject** get_address_of__modelFacade_2() { return &____modelFacade_2; }
	inline void set__modelFacade_2(RuntimeObject* value)
	{
		____modelFacade_2 = value;
		Il2CppCodeGenWriteBarrier((&____modelFacade_2), value);
	}

	inline static int32_t get_offset_of__prefabGenerator_3() { return static_cast<int32_t>(offsetof(ModelElementObserver_t2747689887, ____prefabGenerator_3)); }
	inline RuntimeObject* get__prefabGenerator_3() const { return ____prefabGenerator_3; }
	inline RuntimeObject** get_address_of__prefabGenerator_3() { return &____prefabGenerator_3; }
	inline void set__prefabGenerator_3(RuntimeObject* value)
	{
		____prefabGenerator_3 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_3), value);
	}

	inline static int32_t get_offset_of__elementFacade_4() { return static_cast<int32_t>(offsetof(ModelElementObserver_t2747689887, ____elementFacade_4)); }
	inline RuntimeObject* get__elementFacade_4() const { return ____elementFacade_4; }
	inline RuntimeObject** get_address_of__elementFacade_4() { return &____elementFacade_4; }
	inline void set__elementFacade_4(RuntimeObject* value)
	{
		____elementFacade_4 = value;
		Il2CppCodeGenWriteBarrier((&____elementFacade_4), value);
	}

	inline static int32_t get_offset_of__element3dFacade_5() { return static_cast<int32_t>(offsetof(ModelElementObserver_t2747689887, ____element3dFacade_5)); }
	inline RuntimeObject* get__element3dFacade_5() const { return ____element3dFacade_5; }
	inline RuntimeObject** get_address_of__element3dFacade_5() { return &____element3dFacade_5; }
	inline void set__element3dFacade_5(RuntimeObject* value)
	{
		____element3dFacade_5 = value;
		Il2CppCodeGenWriteBarrier((&____element3dFacade_5), value);
	}

	inline static int32_t get_offset_of__models_6() { return static_cast<int32_t>(offsetof(ModelElementObserver_t2747689887, ____models_6)); }
	inline List_1_t1532750683 * get__models_6() const { return ____models_6; }
	inline List_1_t1532750683 ** get_address_of__models_6() { return &____models_6; }
	inline void set__models_6(List_1_t1532750683 * value)
	{
		____models_6 = value;
		Il2CppCodeGenWriteBarrier((&____models_6), value);
	}

	inline static int32_t get_offset_of__elements_7() { return static_cast<int32_t>(offsetof(ModelElementObserver_t2747689887, ____elements_7)); }
	inline List_1_t1645709140 * get__elements_7() const { return ____elements_7; }
	inline List_1_t1645709140 ** get_address_of__elements_7() { return &____elements_7; }
	inline void set__elements_7(List_1_t1645709140 * value)
	{
		____elements_7 = value;
		Il2CppCodeGenWriteBarrier((&____elements_7), value);
	}

	inline static int32_t get_offset_of_modelIsChoosen_8() { return static_cast<int32_t>(offsetof(ModelElementObserver_t2747689887, ___modelIsChoosen_8)); }
	inline bool get_modelIsChoosen_8() const { return ___modelIsChoosen_8; }
	inline bool* get_address_of_modelIsChoosen_8() { return &___modelIsChoosen_8; }
	inline void set_modelIsChoosen_8(bool value)
	{
		___modelIsChoosen_8 = value;
	}

	inline static int32_t get_offset_of_started_9() { return static_cast<int32_t>(offsetof(ModelElementObserver_t2747689887, ___started_9)); }
	inline bool get_started_9() const { return ___started_9; }
	inline bool* get_address_of_started_9() { return &___started_9; }
	inline void set_started_9(bool value)
	{
		___started_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELELEMENTOBSERVER_T2747689887_H
#ifndef CHECKLISTROWNEW_T383576096_H
#define CHECKLISTROWNEW_T383576096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ChecklistUI.CheckListRowNew
struct  CheckListRowNew_t383576096  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Toggle Library.Code.UI.ChecklistUI.CheckListRowNew::checkboxAccept
	Toggle_t3976754468 * ___checkboxAccept_2;
	// UnityEngine.UI.Toggle Library.Code.UI.ChecklistUI.CheckListRowNew::checkboxRejected
	Toggle_t3976754468 * ___checkboxRejected_3;
	// TMPro.TextMeshProUGUI Library.Code.UI.ChecklistUI.CheckListRowNew::text
	TextMeshProUGUI_t934157183 * ___text_4;
	// System.Boolean Library.Code.UI.ChecklistUI.CheckListRowNew::isAwake
	bool ___isAwake_5;
	// Library.Code.Domain.Entities.Checklist.ItemStatus Library.Code.UI.ChecklistUI.CheckListRowNew::itemStatus
	ItemStatus_t1378751697 * ___itemStatus_6;
	// Library.Code.Domain.Dtos.ItemStatusWithCheckpoint Library.Code.UI.ChecklistUI.CheckListRowNew::_itemStatusWithCheckpoint
	ItemStatusWithCheckpoint_t1657770941 * ____itemStatusWithCheckpoint_7;
	// UnityEngine.Events.UnityAction`1<Library.Code.Domain.Dtos.ItemStatusWithCheckpoint> Library.Code.UI.ChecklistUI.CheckListRowNew::actionOnCheckChange
	UnityAction_1_t3024356692 * ___actionOnCheckChange_8;

public:
	inline static int32_t get_offset_of_checkboxAccept_2() { return static_cast<int32_t>(offsetof(CheckListRowNew_t383576096, ___checkboxAccept_2)); }
	inline Toggle_t3976754468 * get_checkboxAccept_2() const { return ___checkboxAccept_2; }
	inline Toggle_t3976754468 ** get_address_of_checkboxAccept_2() { return &___checkboxAccept_2; }
	inline void set_checkboxAccept_2(Toggle_t3976754468 * value)
	{
		___checkboxAccept_2 = value;
		Il2CppCodeGenWriteBarrier((&___checkboxAccept_2), value);
	}

	inline static int32_t get_offset_of_checkboxRejected_3() { return static_cast<int32_t>(offsetof(CheckListRowNew_t383576096, ___checkboxRejected_3)); }
	inline Toggle_t3976754468 * get_checkboxRejected_3() const { return ___checkboxRejected_3; }
	inline Toggle_t3976754468 ** get_address_of_checkboxRejected_3() { return &___checkboxRejected_3; }
	inline void set_checkboxRejected_3(Toggle_t3976754468 * value)
	{
		___checkboxRejected_3 = value;
		Il2CppCodeGenWriteBarrier((&___checkboxRejected_3), value);
	}

	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(CheckListRowNew_t383576096, ___text_4)); }
	inline TextMeshProUGUI_t934157183 * get_text_4() const { return ___text_4; }
	inline TextMeshProUGUI_t934157183 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(TextMeshProUGUI_t934157183 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}

	inline static int32_t get_offset_of_isAwake_5() { return static_cast<int32_t>(offsetof(CheckListRowNew_t383576096, ___isAwake_5)); }
	inline bool get_isAwake_5() const { return ___isAwake_5; }
	inline bool* get_address_of_isAwake_5() { return &___isAwake_5; }
	inline void set_isAwake_5(bool value)
	{
		___isAwake_5 = value;
	}

	inline static int32_t get_offset_of_itemStatus_6() { return static_cast<int32_t>(offsetof(CheckListRowNew_t383576096, ___itemStatus_6)); }
	inline ItemStatus_t1378751697 * get_itemStatus_6() const { return ___itemStatus_6; }
	inline ItemStatus_t1378751697 ** get_address_of_itemStatus_6() { return &___itemStatus_6; }
	inline void set_itemStatus_6(ItemStatus_t1378751697 * value)
	{
		___itemStatus_6 = value;
		Il2CppCodeGenWriteBarrier((&___itemStatus_6), value);
	}

	inline static int32_t get_offset_of__itemStatusWithCheckpoint_7() { return static_cast<int32_t>(offsetof(CheckListRowNew_t383576096, ____itemStatusWithCheckpoint_7)); }
	inline ItemStatusWithCheckpoint_t1657770941 * get__itemStatusWithCheckpoint_7() const { return ____itemStatusWithCheckpoint_7; }
	inline ItemStatusWithCheckpoint_t1657770941 ** get_address_of__itemStatusWithCheckpoint_7() { return &____itemStatusWithCheckpoint_7; }
	inline void set__itemStatusWithCheckpoint_7(ItemStatusWithCheckpoint_t1657770941 * value)
	{
		____itemStatusWithCheckpoint_7 = value;
		Il2CppCodeGenWriteBarrier((&____itemStatusWithCheckpoint_7), value);
	}

	inline static int32_t get_offset_of_actionOnCheckChange_8() { return static_cast<int32_t>(offsetof(CheckListRowNew_t383576096, ___actionOnCheckChange_8)); }
	inline UnityAction_1_t3024356692 * get_actionOnCheckChange_8() const { return ___actionOnCheckChange_8; }
	inline UnityAction_1_t3024356692 ** get_address_of_actionOnCheckChange_8() { return &___actionOnCheckChange_8; }
	inline void set_actionOnCheckChange_8(UnityAction_1_t3024356692 * value)
	{
		___actionOnCheckChange_8 = value;
		Il2CppCodeGenWriteBarrier((&___actionOnCheckChange_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKLISTROWNEW_T383576096_H
#ifndef CURRENTITEMSTATUSUI_T1638528844_H
#define CURRENTITEMSTATUSUI_T1638528844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ChecklistUI.CurrentItemStatusUI
struct  CurrentItemStatusUI_t1638528844  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.UI.PrefabGenerator.PrefabGenerator Library.Code.UI.ChecklistUI.CurrentItemStatusUI::_prefabGenerator
	RuntimeObject* ____prefabGenerator_2;
	// Library.Code.Facades.CheckListF.CurrentCheckListItemFacade Library.Code.UI.ChecklistUI.CurrentItemStatusUI::_currentCheckListItemFacade
	RuntimeObject* ____currentCheckListItemFacade_3;
	// Library.Code.Facades.CheckListF.CheckListFacade Library.Code.UI.ChecklistUI.CurrentItemStatusUI::_checkListFacade
	RuntimeObject* ____checkListFacade_4;
	// Library.Code.Domain.Entities.Checklist.ItemStatus Library.Code.UI.ChecklistUI.CurrentItemStatusUI::_currentItemStatus
	ItemStatus_t1378751697 * ____currentItemStatus_5;
	// System.Collections.Generic.List`1<Library.Code.UI.ChecklistUI.ItemStatusRow> Library.Code.UI.ChecklistUI.CurrentItemStatusUI::_itemStatusRows
	List_1_t2159658009 * ____itemStatusRows_6;
	// System.Boolean Library.Code.UI.ChecklistUI.CurrentItemStatusUI::started
	bool ___started_7;

public:
	inline static int32_t get_offset_of__prefabGenerator_2() { return static_cast<int32_t>(offsetof(CurrentItemStatusUI_t1638528844, ____prefabGenerator_2)); }
	inline RuntimeObject* get__prefabGenerator_2() const { return ____prefabGenerator_2; }
	inline RuntimeObject** get_address_of__prefabGenerator_2() { return &____prefabGenerator_2; }
	inline void set__prefabGenerator_2(RuntimeObject* value)
	{
		____prefabGenerator_2 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_2), value);
	}

	inline static int32_t get_offset_of__currentCheckListItemFacade_3() { return static_cast<int32_t>(offsetof(CurrentItemStatusUI_t1638528844, ____currentCheckListItemFacade_3)); }
	inline RuntimeObject* get__currentCheckListItemFacade_3() const { return ____currentCheckListItemFacade_3; }
	inline RuntimeObject** get_address_of__currentCheckListItemFacade_3() { return &____currentCheckListItemFacade_3; }
	inline void set__currentCheckListItemFacade_3(RuntimeObject* value)
	{
		____currentCheckListItemFacade_3 = value;
		Il2CppCodeGenWriteBarrier((&____currentCheckListItemFacade_3), value);
	}

	inline static int32_t get_offset_of__checkListFacade_4() { return static_cast<int32_t>(offsetof(CurrentItemStatusUI_t1638528844, ____checkListFacade_4)); }
	inline RuntimeObject* get__checkListFacade_4() const { return ____checkListFacade_4; }
	inline RuntimeObject** get_address_of__checkListFacade_4() { return &____checkListFacade_4; }
	inline void set__checkListFacade_4(RuntimeObject* value)
	{
		____checkListFacade_4 = value;
		Il2CppCodeGenWriteBarrier((&____checkListFacade_4), value);
	}

	inline static int32_t get_offset_of__currentItemStatus_5() { return static_cast<int32_t>(offsetof(CurrentItemStatusUI_t1638528844, ____currentItemStatus_5)); }
	inline ItemStatus_t1378751697 * get__currentItemStatus_5() const { return ____currentItemStatus_5; }
	inline ItemStatus_t1378751697 ** get_address_of__currentItemStatus_5() { return &____currentItemStatus_5; }
	inline void set__currentItemStatus_5(ItemStatus_t1378751697 * value)
	{
		____currentItemStatus_5 = value;
		Il2CppCodeGenWriteBarrier((&____currentItemStatus_5), value);
	}

	inline static int32_t get_offset_of__itemStatusRows_6() { return static_cast<int32_t>(offsetof(CurrentItemStatusUI_t1638528844, ____itemStatusRows_6)); }
	inline List_1_t2159658009 * get__itemStatusRows_6() const { return ____itemStatusRows_6; }
	inline List_1_t2159658009 ** get_address_of__itemStatusRows_6() { return &____itemStatusRows_6; }
	inline void set__itemStatusRows_6(List_1_t2159658009 * value)
	{
		____itemStatusRows_6 = value;
		Il2CppCodeGenWriteBarrier((&____itemStatusRows_6), value);
	}

	inline static int32_t get_offset_of_started_7() { return static_cast<int32_t>(offsetof(CurrentItemStatusUI_t1638528844, ___started_7)); }
	inline bool get_started_7() const { return ___started_7; }
	inline bool* get_address_of_started_7() { return &___started_7; }
	inline void set_started_7(bool value)
	{
		___started_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURRENTITEMSTATUSUI_T1638528844_H
#ifndef ITEMSTATUSROW_T2790536877_H
#define ITEMSTATUSROW_T2790536877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ChecklistUI.ItemStatusRow
struct  ItemStatusRow_t2790536877  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button Library.Code.UI.ChecklistUI.ItemStatusRow::button
	Button_t2872111280 * ___button_2;
	// Library.Code.Domain.Entities.Checklist.ItemStatusDto Library.Code.UI.ChecklistUI.ItemStatusRow::itemStatus
	ItemStatusDto_t3640770036 * ___itemStatus_3;

public:
	inline static int32_t get_offset_of_button_2() { return static_cast<int32_t>(offsetof(ItemStatusRow_t2790536877, ___button_2)); }
	inline Button_t2872111280 * get_button_2() const { return ___button_2; }
	inline Button_t2872111280 ** get_address_of_button_2() { return &___button_2; }
	inline void set_button_2(Button_t2872111280 * value)
	{
		___button_2 = value;
		Il2CppCodeGenWriteBarrier((&___button_2), value);
	}

	inline static int32_t get_offset_of_itemStatus_3() { return static_cast<int32_t>(offsetof(ItemStatusRow_t2790536877, ___itemStatus_3)); }
	inline ItemStatusDto_t3640770036 * get_itemStatus_3() const { return ___itemStatus_3; }
	inline ItemStatusDto_t3640770036 ** get_address_of_itemStatus_3() { return &___itemStatus_3; }
	inline void set_itemStatus_3(ItemStatusDto_t3640770036 * value)
	{
		___itemStatus_3 = value;
		Il2CppCodeGenWriteBarrier((&___itemStatus_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMSTATUSROW_T2790536877_H
#ifndef CHECKLISTUI_T3492366448_H
#define CHECKLISTUI_T3492366448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ChecklistUI.ChecklistUI
struct  ChecklistUI_t3492366448  : public MonoBehaviour_t1158329972
{
public:
	// System.String Library.Code.UI.ChecklistUI.ChecklistUI::POPUP_TITLE
	String_t* ___POPUP_TITLE_2;
	// Library.Code.Facades.CheckListF.CheckListFacade Library.Code.UI.ChecklistUI.ChecklistUI::_checkListFacade
	RuntimeObject* ____checkListFacade_3;
	// Library.Code.Facades.CheckPoints.CheckPointsFacade Library.Code.UI.ChecklistUI.ChecklistUI::_checkPointsFacade
	RuntimeObject* ____checkPointsFacade_4;
	// Library.Code.UI.PrefabGenerator.PrefabGenerator Library.Code.UI.ChecklistUI.ChecklistUI::_prefabGenerator
	RuntimeObject* ____prefabGenerator_5;
	// System.Boolean Library.Code.UI.ChecklistUI.ChecklistUI::started
	bool ___started_6;
	// UnityEngine.UI.Button Library.Code.UI.ChecklistUI.ChecklistUI::approveButton
	Button_t2872111280 * ___approveButton_7;
	// Library.Code.Facades.Elements.ElementApproveStateFacade Library.Code.UI.ChecklistUI.ChecklistUI::_elementApproveStateFacade
	RuntimeObject* ____elementApproveStateFacade_8;
	// Library.Code.DI.PopUp.PopUpShower Library.Code.UI.ChecklistUI.ChecklistUI::_popUpShower
	PopUpShower_t3186557280 * ____popUpShower_9;
	// UnityEngine.GameObject Library.Code.UI.ChecklistUI.ChecklistUI::popUp
	GameObject_t1756533147 * ___popUp_10;
	// System.Int32 Library.Code.UI.ChecklistUI.ChecklistUI::objectNumber
	int32_t ___objectNumber_11;
	// System.Boolean Library.Code.UI.ChecklistUI.ChecklistUI::isCheckpointsLoaded
	bool ___isCheckpointsLoaded_12;
	// System.Boolean Library.Code.UI.ChecklistUI.ChecklistUI::isChecklistLoaded
	bool ___isChecklistLoaded_13;
	// Library.Code.Domain.Entities.Checklist.CheckList Library.Code.UI.ChecklistUI.ChecklistUI::_loadedList
	CheckList_t4132048078 * ____loadedList_14;
	// System.Collections.Generic.List`1<Library.Code.Domain.Dtos.CheckPointInfo> Library.Code.UI.ChecklistUI.ChecklistUI::_checkPointInfosLoaded
	List_1_t2717891520 * ____checkPointInfosLoaded_15;

public:
	inline static int32_t get_offset_of_POPUP_TITLE_2() { return static_cast<int32_t>(offsetof(ChecklistUI_t3492366448, ___POPUP_TITLE_2)); }
	inline String_t* get_POPUP_TITLE_2() const { return ___POPUP_TITLE_2; }
	inline String_t** get_address_of_POPUP_TITLE_2() { return &___POPUP_TITLE_2; }
	inline void set_POPUP_TITLE_2(String_t* value)
	{
		___POPUP_TITLE_2 = value;
		Il2CppCodeGenWriteBarrier((&___POPUP_TITLE_2), value);
	}

	inline static int32_t get_offset_of__checkListFacade_3() { return static_cast<int32_t>(offsetof(ChecklistUI_t3492366448, ____checkListFacade_3)); }
	inline RuntimeObject* get__checkListFacade_3() const { return ____checkListFacade_3; }
	inline RuntimeObject** get_address_of__checkListFacade_3() { return &____checkListFacade_3; }
	inline void set__checkListFacade_3(RuntimeObject* value)
	{
		____checkListFacade_3 = value;
		Il2CppCodeGenWriteBarrier((&____checkListFacade_3), value);
	}

	inline static int32_t get_offset_of__checkPointsFacade_4() { return static_cast<int32_t>(offsetof(ChecklistUI_t3492366448, ____checkPointsFacade_4)); }
	inline RuntimeObject* get__checkPointsFacade_4() const { return ____checkPointsFacade_4; }
	inline RuntimeObject** get_address_of__checkPointsFacade_4() { return &____checkPointsFacade_4; }
	inline void set__checkPointsFacade_4(RuntimeObject* value)
	{
		____checkPointsFacade_4 = value;
		Il2CppCodeGenWriteBarrier((&____checkPointsFacade_4), value);
	}

	inline static int32_t get_offset_of__prefabGenerator_5() { return static_cast<int32_t>(offsetof(ChecklistUI_t3492366448, ____prefabGenerator_5)); }
	inline RuntimeObject* get__prefabGenerator_5() const { return ____prefabGenerator_5; }
	inline RuntimeObject** get_address_of__prefabGenerator_5() { return &____prefabGenerator_5; }
	inline void set__prefabGenerator_5(RuntimeObject* value)
	{
		____prefabGenerator_5 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_5), value);
	}

	inline static int32_t get_offset_of_started_6() { return static_cast<int32_t>(offsetof(ChecklistUI_t3492366448, ___started_6)); }
	inline bool get_started_6() const { return ___started_6; }
	inline bool* get_address_of_started_6() { return &___started_6; }
	inline void set_started_6(bool value)
	{
		___started_6 = value;
	}

	inline static int32_t get_offset_of_approveButton_7() { return static_cast<int32_t>(offsetof(ChecklistUI_t3492366448, ___approveButton_7)); }
	inline Button_t2872111280 * get_approveButton_7() const { return ___approveButton_7; }
	inline Button_t2872111280 ** get_address_of_approveButton_7() { return &___approveButton_7; }
	inline void set_approveButton_7(Button_t2872111280 * value)
	{
		___approveButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___approveButton_7), value);
	}

	inline static int32_t get_offset_of__elementApproveStateFacade_8() { return static_cast<int32_t>(offsetof(ChecklistUI_t3492366448, ____elementApproveStateFacade_8)); }
	inline RuntimeObject* get__elementApproveStateFacade_8() const { return ____elementApproveStateFacade_8; }
	inline RuntimeObject** get_address_of__elementApproveStateFacade_8() { return &____elementApproveStateFacade_8; }
	inline void set__elementApproveStateFacade_8(RuntimeObject* value)
	{
		____elementApproveStateFacade_8 = value;
		Il2CppCodeGenWriteBarrier((&____elementApproveStateFacade_8), value);
	}

	inline static int32_t get_offset_of__popUpShower_9() { return static_cast<int32_t>(offsetof(ChecklistUI_t3492366448, ____popUpShower_9)); }
	inline PopUpShower_t3186557280 * get__popUpShower_9() const { return ____popUpShower_9; }
	inline PopUpShower_t3186557280 ** get_address_of__popUpShower_9() { return &____popUpShower_9; }
	inline void set__popUpShower_9(PopUpShower_t3186557280 * value)
	{
		____popUpShower_9 = value;
		Il2CppCodeGenWriteBarrier((&____popUpShower_9), value);
	}

	inline static int32_t get_offset_of_popUp_10() { return static_cast<int32_t>(offsetof(ChecklistUI_t3492366448, ___popUp_10)); }
	inline GameObject_t1756533147 * get_popUp_10() const { return ___popUp_10; }
	inline GameObject_t1756533147 ** get_address_of_popUp_10() { return &___popUp_10; }
	inline void set_popUp_10(GameObject_t1756533147 * value)
	{
		___popUp_10 = value;
		Il2CppCodeGenWriteBarrier((&___popUp_10), value);
	}

	inline static int32_t get_offset_of_objectNumber_11() { return static_cast<int32_t>(offsetof(ChecklistUI_t3492366448, ___objectNumber_11)); }
	inline int32_t get_objectNumber_11() const { return ___objectNumber_11; }
	inline int32_t* get_address_of_objectNumber_11() { return &___objectNumber_11; }
	inline void set_objectNumber_11(int32_t value)
	{
		___objectNumber_11 = value;
	}

	inline static int32_t get_offset_of_isCheckpointsLoaded_12() { return static_cast<int32_t>(offsetof(ChecklistUI_t3492366448, ___isCheckpointsLoaded_12)); }
	inline bool get_isCheckpointsLoaded_12() const { return ___isCheckpointsLoaded_12; }
	inline bool* get_address_of_isCheckpointsLoaded_12() { return &___isCheckpointsLoaded_12; }
	inline void set_isCheckpointsLoaded_12(bool value)
	{
		___isCheckpointsLoaded_12 = value;
	}

	inline static int32_t get_offset_of_isChecklistLoaded_13() { return static_cast<int32_t>(offsetof(ChecklistUI_t3492366448, ___isChecklistLoaded_13)); }
	inline bool get_isChecklistLoaded_13() const { return ___isChecklistLoaded_13; }
	inline bool* get_address_of_isChecklistLoaded_13() { return &___isChecklistLoaded_13; }
	inline void set_isChecklistLoaded_13(bool value)
	{
		___isChecklistLoaded_13 = value;
	}

	inline static int32_t get_offset_of__loadedList_14() { return static_cast<int32_t>(offsetof(ChecklistUI_t3492366448, ____loadedList_14)); }
	inline CheckList_t4132048078 * get__loadedList_14() const { return ____loadedList_14; }
	inline CheckList_t4132048078 ** get_address_of__loadedList_14() { return &____loadedList_14; }
	inline void set__loadedList_14(CheckList_t4132048078 * value)
	{
		____loadedList_14 = value;
		Il2CppCodeGenWriteBarrier((&____loadedList_14), value);
	}

	inline static int32_t get_offset_of__checkPointInfosLoaded_15() { return static_cast<int32_t>(offsetof(ChecklistUI_t3492366448, ____checkPointInfosLoaded_15)); }
	inline List_1_t2717891520 * get__checkPointInfosLoaded_15() const { return ____checkPointInfosLoaded_15; }
	inline List_1_t2717891520 ** get_address_of__checkPointInfosLoaded_15() { return &____checkPointInfosLoaded_15; }
	inline void set__checkPointInfosLoaded_15(List_1_t2717891520 * value)
	{
		____checkPointInfosLoaded_15 = value;
		Il2CppCodeGenWriteBarrier((&____checkPointInfosLoaded_15), value);
	}
};

struct ChecklistUI_t3492366448_StaticFields
{
public:
	// UnityEngine.Events.UnityAction`1<Library.Code.Domain.Dtos.ItemStatusWithCheckpoint> Library.Code.UI.ChecklistUI.ChecklistUI::<>f__am$cache0
	UnityAction_1_t3024356692 * ___U3CU3Ef__amU24cache0_16;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_16() { return static_cast<int32_t>(offsetof(ChecklistUI_t3492366448_StaticFields, ___U3CU3Ef__amU24cache0_16)); }
	inline UnityAction_1_t3024356692 * get_U3CU3Ef__amU24cache0_16() const { return ___U3CU3Ef__amU24cache0_16; }
	inline UnityAction_1_t3024356692 ** get_address_of_U3CU3Ef__amU24cache0_16() { return &___U3CU3Ef__amU24cache0_16; }
	inline void set_U3CU3Ef__amU24cache0_16(UnityAction_1_t3024356692 * value)
	{
		___U3CU3Ef__amU24cache0_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKLISTUI_T3492366448_H
#ifndef ELEMENTROWHANDLER_T1682113530_H
#define ELEMENTROWHANDLER_T1682113530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ChooseModel.ElementRowHandler
struct  ElementRowHandler_t1682113530  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.UI.ChooseModel.ElementRowHandlerDelegate Library.Code.UI.ChooseModel.ElementRowHandler::_elementRowHandlerDelegate
	RuntimeObject* ____elementRowHandlerDelegate_2;
	// Library.Code.Domain.Entities.Element Library.Code.UI.ChooseModel.ElementRowHandler::_element
	Element_t2276588008 * ____element_3;
	// UnityEngine.UI.Button Library.Code.UI.ChooseModel.ElementRowHandler::text
	Button_t2872111280 * ___text_4;

public:
	inline static int32_t get_offset_of__elementRowHandlerDelegate_2() { return static_cast<int32_t>(offsetof(ElementRowHandler_t1682113530, ____elementRowHandlerDelegate_2)); }
	inline RuntimeObject* get__elementRowHandlerDelegate_2() const { return ____elementRowHandlerDelegate_2; }
	inline RuntimeObject** get_address_of__elementRowHandlerDelegate_2() { return &____elementRowHandlerDelegate_2; }
	inline void set__elementRowHandlerDelegate_2(RuntimeObject* value)
	{
		____elementRowHandlerDelegate_2 = value;
		Il2CppCodeGenWriteBarrier((&____elementRowHandlerDelegate_2), value);
	}

	inline static int32_t get_offset_of__element_3() { return static_cast<int32_t>(offsetof(ElementRowHandler_t1682113530, ____element_3)); }
	inline Element_t2276588008 * get__element_3() const { return ____element_3; }
	inline Element_t2276588008 ** get_address_of__element_3() { return &____element_3; }
	inline void set__element_3(Element_t2276588008 * value)
	{
		____element_3 = value;
		Il2CppCodeGenWriteBarrier((&____element_3), value);
	}

	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(ElementRowHandler_t1682113530, ___text_4)); }
	inline Button_t2872111280 * get_text_4() const { return ___text_4; }
	inline Button_t2872111280 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(Button_t2872111280 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTROWHANDLER_T1682113530_H
#ifndef ELEMENTROWHANDLERNEW_T1658168_H
#define ELEMENTROWHANDLERNEW_T1658168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ChooseModel.ElementRowHandlerNew
struct  ElementRowHandlerNew_t1658168  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.UI.ChooseModel.ElementRowHandlerDelegate Library.Code.UI.ChooseModel.ElementRowHandlerNew::_elementRowHandlerDelegate
	RuntimeObject* ____elementRowHandlerDelegate_2;
	// Library.Code.Domain.Entities.Element Library.Code.UI.ChooseModel.ElementRowHandlerNew::_element
	Element_t2276588008 * ____element_3;
	// UnityEngine.UI.Button Library.Code.UI.ChooseModel.ElementRowHandlerNew::text
	Button_t2872111280 * ___text_4;

public:
	inline static int32_t get_offset_of__elementRowHandlerDelegate_2() { return static_cast<int32_t>(offsetof(ElementRowHandlerNew_t1658168, ____elementRowHandlerDelegate_2)); }
	inline RuntimeObject* get__elementRowHandlerDelegate_2() const { return ____elementRowHandlerDelegate_2; }
	inline RuntimeObject** get_address_of__elementRowHandlerDelegate_2() { return &____elementRowHandlerDelegate_2; }
	inline void set__elementRowHandlerDelegate_2(RuntimeObject* value)
	{
		____elementRowHandlerDelegate_2 = value;
		Il2CppCodeGenWriteBarrier((&____elementRowHandlerDelegate_2), value);
	}

	inline static int32_t get_offset_of__element_3() { return static_cast<int32_t>(offsetof(ElementRowHandlerNew_t1658168, ____element_3)); }
	inline Element_t2276588008 * get__element_3() const { return ____element_3; }
	inline Element_t2276588008 ** get_address_of__element_3() { return &____element_3; }
	inline void set__element_3(Element_t2276588008 * value)
	{
		____element_3 = value;
		Il2CppCodeGenWriteBarrier((&____element_3), value);
	}

	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(ElementRowHandlerNew_t1658168, ___text_4)); }
	inline Button_t2872111280 * get_text_4() const { return ___text_4; }
	inline Button_t2872111280 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(Button_t2872111280 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTROWHANDLERNEW_T1658168_H
#ifndef POPUPSCRIPT_T4024301849_H
#define POPUPSCRIPT_T4024301849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.PopUp.PopUpScript
struct  PopUpScript_t4024301849  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.UI.PopUp.PopUpsFactory Library.Code.UI.PopUp.PopUpScript::popsUpsFactory
	RuntimeObject* ___popsUpsFactory_2;

public:
	inline static int32_t get_offset_of_popsUpsFactory_2() { return static_cast<int32_t>(offsetof(PopUpScript_t4024301849, ___popsUpsFactory_2)); }
	inline RuntimeObject* get_popsUpsFactory_2() const { return ___popsUpsFactory_2; }
	inline RuntimeObject** get_address_of_popsUpsFactory_2() { return &___popsUpsFactory_2; }
	inline void set_popsUpsFactory_2(RuntimeObject* value)
	{
		___popsUpsFactory_2 = value;
		Il2CppCodeGenWriteBarrier((&___popsUpsFactory_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPUPSCRIPT_T4024301849_H
#ifndef ELEMENTSOBSERVER_T3382904053_H
#define ELEMENTSOBSERVER_T3382904053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ChooseModel.ElementsObserver
struct  ElementsObserver_t3382904053  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Facades.ElementHandler.ElementHandlerFacade Library.Code.UI.ChooseModel.ElementsObserver::_elementHandlerFacade
	RuntimeObject* ____elementHandlerFacade_2;
	// Library.Code.UI.PrefabGenerator.PrefabGenerator Library.Code.UI.ChooseModel.ElementsObserver::_prefabGenerator
	RuntimeObject* ____prefabGenerator_3;
	// Library.Code.Facades.Elements.ElementFacade Library.Code.UI.ChooseModel.ElementsObserver::_elementFacade
	RuntimeObject* ____elementFacade_4;
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Element> Library.Code.UI.ChooseModel.ElementsObserver::_elements
	List_1_t1645709140 * ____elements_5;
	// UnityEngine.UI.Button Library.Code.UI.ChooseModel.ElementsObserver::backButton
	Button_t2872111280 * ___backButton_6;
	// Library.Code.DI.PopUp.PopUpShower Library.Code.UI.ChooseModel.ElementsObserver::_showerPopUps
	PopUpShower_t3186557280 * ____showerPopUps_7;
	// Library.Code.Facades._3dElementDownload.Element3dFacade Library.Code.UI.ChooseModel.ElementsObserver::element3DFacade
	RuntimeObject* ___element3DFacade_8;
	// Library.Code.UI.ChooseModel.ModelInterface Library.Code.UI.ChooseModel.ElementsObserver::_modelInterface
	RuntimeObject* ____modelInterface_9;
	// System.Int64 Library.Code.UI.ChooseModel.ElementsObserver::lasteElementLoaded
	int64_t ___lasteElementLoaded_10;
	// UnityEngine.GameObject Library.Code.UI.ChooseModel.ElementsObserver::loadingPopUp
	GameObject_t1756533147 * ___loadingPopUp_11;

public:
	inline static int32_t get_offset_of__elementHandlerFacade_2() { return static_cast<int32_t>(offsetof(ElementsObserver_t3382904053, ____elementHandlerFacade_2)); }
	inline RuntimeObject* get__elementHandlerFacade_2() const { return ____elementHandlerFacade_2; }
	inline RuntimeObject** get_address_of__elementHandlerFacade_2() { return &____elementHandlerFacade_2; }
	inline void set__elementHandlerFacade_2(RuntimeObject* value)
	{
		____elementHandlerFacade_2 = value;
		Il2CppCodeGenWriteBarrier((&____elementHandlerFacade_2), value);
	}

	inline static int32_t get_offset_of__prefabGenerator_3() { return static_cast<int32_t>(offsetof(ElementsObserver_t3382904053, ____prefabGenerator_3)); }
	inline RuntimeObject* get__prefabGenerator_3() const { return ____prefabGenerator_3; }
	inline RuntimeObject** get_address_of__prefabGenerator_3() { return &____prefabGenerator_3; }
	inline void set__prefabGenerator_3(RuntimeObject* value)
	{
		____prefabGenerator_3 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_3), value);
	}

	inline static int32_t get_offset_of__elementFacade_4() { return static_cast<int32_t>(offsetof(ElementsObserver_t3382904053, ____elementFacade_4)); }
	inline RuntimeObject* get__elementFacade_4() const { return ____elementFacade_4; }
	inline RuntimeObject** get_address_of__elementFacade_4() { return &____elementFacade_4; }
	inline void set__elementFacade_4(RuntimeObject* value)
	{
		____elementFacade_4 = value;
		Il2CppCodeGenWriteBarrier((&____elementFacade_4), value);
	}

	inline static int32_t get_offset_of__elements_5() { return static_cast<int32_t>(offsetof(ElementsObserver_t3382904053, ____elements_5)); }
	inline List_1_t1645709140 * get__elements_5() const { return ____elements_5; }
	inline List_1_t1645709140 ** get_address_of__elements_5() { return &____elements_5; }
	inline void set__elements_5(List_1_t1645709140 * value)
	{
		____elements_5 = value;
		Il2CppCodeGenWriteBarrier((&____elements_5), value);
	}

	inline static int32_t get_offset_of_backButton_6() { return static_cast<int32_t>(offsetof(ElementsObserver_t3382904053, ___backButton_6)); }
	inline Button_t2872111280 * get_backButton_6() const { return ___backButton_6; }
	inline Button_t2872111280 ** get_address_of_backButton_6() { return &___backButton_6; }
	inline void set_backButton_6(Button_t2872111280 * value)
	{
		___backButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___backButton_6), value);
	}

	inline static int32_t get_offset_of__showerPopUps_7() { return static_cast<int32_t>(offsetof(ElementsObserver_t3382904053, ____showerPopUps_7)); }
	inline PopUpShower_t3186557280 * get__showerPopUps_7() const { return ____showerPopUps_7; }
	inline PopUpShower_t3186557280 ** get_address_of__showerPopUps_7() { return &____showerPopUps_7; }
	inline void set__showerPopUps_7(PopUpShower_t3186557280 * value)
	{
		____showerPopUps_7 = value;
		Il2CppCodeGenWriteBarrier((&____showerPopUps_7), value);
	}

	inline static int32_t get_offset_of_element3DFacade_8() { return static_cast<int32_t>(offsetof(ElementsObserver_t3382904053, ___element3DFacade_8)); }
	inline RuntimeObject* get_element3DFacade_8() const { return ___element3DFacade_8; }
	inline RuntimeObject** get_address_of_element3DFacade_8() { return &___element3DFacade_8; }
	inline void set_element3DFacade_8(RuntimeObject* value)
	{
		___element3DFacade_8 = value;
		Il2CppCodeGenWriteBarrier((&___element3DFacade_8), value);
	}

	inline static int32_t get_offset_of__modelInterface_9() { return static_cast<int32_t>(offsetof(ElementsObserver_t3382904053, ____modelInterface_9)); }
	inline RuntimeObject* get__modelInterface_9() const { return ____modelInterface_9; }
	inline RuntimeObject** get_address_of__modelInterface_9() { return &____modelInterface_9; }
	inline void set__modelInterface_9(RuntimeObject* value)
	{
		____modelInterface_9 = value;
		Il2CppCodeGenWriteBarrier((&____modelInterface_9), value);
	}

	inline static int32_t get_offset_of_lasteElementLoaded_10() { return static_cast<int32_t>(offsetof(ElementsObserver_t3382904053, ___lasteElementLoaded_10)); }
	inline int64_t get_lasteElementLoaded_10() const { return ___lasteElementLoaded_10; }
	inline int64_t* get_address_of_lasteElementLoaded_10() { return &___lasteElementLoaded_10; }
	inline void set_lasteElementLoaded_10(int64_t value)
	{
		___lasteElementLoaded_10 = value;
	}

	inline static int32_t get_offset_of_loadingPopUp_11() { return static_cast<int32_t>(offsetof(ElementsObserver_t3382904053, ___loadingPopUp_11)); }
	inline GameObject_t1756533147 * get_loadingPopUp_11() const { return ___loadingPopUp_11; }
	inline GameObject_t1756533147 ** get_address_of_loadingPopUp_11() { return &___loadingPopUp_11; }
	inline void set_loadingPopUp_11(GameObject_t1756533147 * value)
	{
		___loadingPopUp_11 = value;
		Il2CppCodeGenWriteBarrier((&___loadingPopUp_11), value);
	}
};

struct ElementsObserver_t3382904053_StaticFields
{
public:
	// System.Action Library.Code.UI.ChooseModel.ElementsObserver::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_12;
	// System.Action Library.Code.UI.ChooseModel.ElementsObserver::<>f__am$cache1
	Action_t3226471752 * ___U3CU3Ef__amU24cache1_13;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_12() { return static_cast<int32_t>(offsetof(ElementsObserver_t3382904053_StaticFields, ___U3CU3Ef__amU24cache0_12)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_12() const { return ___U3CU3Ef__amU24cache0_12; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_12() { return &___U3CU3Ef__amU24cache0_12; }
	inline void set_U3CU3Ef__amU24cache0_12(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_13() { return static_cast<int32_t>(offsetof(ElementsObserver_t3382904053_StaticFields, ___U3CU3Ef__amU24cache1_13)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache1_13() const { return ___U3CU3Ef__amU24cache1_13; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache1_13() { return &___U3CU3Ef__amU24cache1_13; }
	inline void set_U3CU3Ef__amU24cache1_13(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache1_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTSOBSERVER_T3382904053_H
#ifndef REMARK_T2047647426_H
#define REMARK_T2047647426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MessageTemplates.Remark
struct  Remark_t2047647426  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button Library.Code.UI.MessageTemplates.Remark::button
	Button_t2872111280 * ___button_2;
	// UnityEngine.GameObject Library.Code.UI.MessageTemplates.Remark::remarkWindow
	GameObject_t1756533147 * ___remarkWindow_3;
	// UnityEngine.UI.Button Library.Code.UI.MessageTemplates.Remark::save
	Button_t2872111280 * ___save_4;
	// UnityEngine.UI.Button Library.Code.UI.MessageTemplates.Remark::close
	Button_t2872111280 * ___close_5;
	// System.Boolean Library.Code.UI.MessageTemplates.Remark::anchor
	bool ___anchor_6;

public:
	inline static int32_t get_offset_of_button_2() { return static_cast<int32_t>(offsetof(Remark_t2047647426, ___button_2)); }
	inline Button_t2872111280 * get_button_2() const { return ___button_2; }
	inline Button_t2872111280 ** get_address_of_button_2() { return &___button_2; }
	inline void set_button_2(Button_t2872111280 * value)
	{
		___button_2 = value;
		Il2CppCodeGenWriteBarrier((&___button_2), value);
	}

	inline static int32_t get_offset_of_remarkWindow_3() { return static_cast<int32_t>(offsetof(Remark_t2047647426, ___remarkWindow_3)); }
	inline GameObject_t1756533147 * get_remarkWindow_3() const { return ___remarkWindow_3; }
	inline GameObject_t1756533147 ** get_address_of_remarkWindow_3() { return &___remarkWindow_3; }
	inline void set_remarkWindow_3(GameObject_t1756533147 * value)
	{
		___remarkWindow_3 = value;
		Il2CppCodeGenWriteBarrier((&___remarkWindow_3), value);
	}

	inline static int32_t get_offset_of_save_4() { return static_cast<int32_t>(offsetof(Remark_t2047647426, ___save_4)); }
	inline Button_t2872111280 * get_save_4() const { return ___save_4; }
	inline Button_t2872111280 ** get_address_of_save_4() { return &___save_4; }
	inline void set_save_4(Button_t2872111280 * value)
	{
		___save_4 = value;
		Il2CppCodeGenWriteBarrier((&___save_4), value);
	}

	inline static int32_t get_offset_of_close_5() { return static_cast<int32_t>(offsetof(Remark_t2047647426, ___close_5)); }
	inline Button_t2872111280 * get_close_5() const { return ___close_5; }
	inline Button_t2872111280 ** get_address_of_close_5() { return &___close_5; }
	inline void set_close_5(Button_t2872111280 * value)
	{
		___close_5 = value;
		Il2CppCodeGenWriteBarrier((&___close_5), value);
	}

	inline static int32_t get_offset_of_anchor_6() { return static_cast<int32_t>(offsetof(Remark_t2047647426, ___anchor_6)); }
	inline bool get_anchor_6() const { return ___anchor_6; }
	inline bool* get_address_of_anchor_6() { return &___anchor_6; }
	inline void set_anchor_6(bool value)
	{
		___anchor_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMARK_T2047647426_H
#ifndef MODELOBSERVERNEW_T1299566747_H
#define MODELOBSERVERNEW_T1299566747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ChooseModel.ModelObserverNew
struct  ModelObserverNew_t1299566747  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Facades.Models.ModelFacade Library.Code.UI.ChooseModel.ModelObserverNew::_modelFacade
	RuntimeObject* ____modelFacade_2;
	// Library.Code.UI.PrefabGenerator.PrefabGenerator Library.Code.UI.ChooseModel.ModelObserverNew::_prefabGenerator
	RuntimeObject* ____prefabGenerator_3;
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model> Library.Code.UI.ChooseModel.ModelObserverNew::_models
	List_1_t1532750683 * ____models_4;
	// Library.Code.Facades.Elements.ElementFacade Library.Code.UI.ChooseModel.ModelObserverNew::_elementFacade
	RuntimeObject* ____elementFacade_5;
	// Library.Code.UI.ChooseModel.ElementsObserverNew Library.Code.UI.ChooseModel.ModelObserverNew::_elementsObserver
	ElementsObserverNew_t2308744357 * ____elementsObserver_6;
	// Library.Code.UI.ChooseModel.ElementsInterface Library.Code.UI.ChooseModel.ModelObserverNew::_elementsInterface
	RuntimeObject* ____elementsInterface_7;

public:
	inline static int32_t get_offset_of__modelFacade_2() { return static_cast<int32_t>(offsetof(ModelObserverNew_t1299566747, ____modelFacade_2)); }
	inline RuntimeObject* get__modelFacade_2() const { return ____modelFacade_2; }
	inline RuntimeObject** get_address_of__modelFacade_2() { return &____modelFacade_2; }
	inline void set__modelFacade_2(RuntimeObject* value)
	{
		____modelFacade_2 = value;
		Il2CppCodeGenWriteBarrier((&____modelFacade_2), value);
	}

	inline static int32_t get_offset_of__prefabGenerator_3() { return static_cast<int32_t>(offsetof(ModelObserverNew_t1299566747, ____prefabGenerator_3)); }
	inline RuntimeObject* get__prefabGenerator_3() const { return ____prefabGenerator_3; }
	inline RuntimeObject** get_address_of__prefabGenerator_3() { return &____prefabGenerator_3; }
	inline void set__prefabGenerator_3(RuntimeObject* value)
	{
		____prefabGenerator_3 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_3), value);
	}

	inline static int32_t get_offset_of__models_4() { return static_cast<int32_t>(offsetof(ModelObserverNew_t1299566747, ____models_4)); }
	inline List_1_t1532750683 * get__models_4() const { return ____models_4; }
	inline List_1_t1532750683 ** get_address_of__models_4() { return &____models_4; }
	inline void set__models_4(List_1_t1532750683 * value)
	{
		____models_4 = value;
		Il2CppCodeGenWriteBarrier((&____models_4), value);
	}

	inline static int32_t get_offset_of__elementFacade_5() { return static_cast<int32_t>(offsetof(ModelObserverNew_t1299566747, ____elementFacade_5)); }
	inline RuntimeObject* get__elementFacade_5() const { return ____elementFacade_5; }
	inline RuntimeObject** get_address_of__elementFacade_5() { return &____elementFacade_5; }
	inline void set__elementFacade_5(RuntimeObject* value)
	{
		____elementFacade_5 = value;
		Il2CppCodeGenWriteBarrier((&____elementFacade_5), value);
	}

	inline static int32_t get_offset_of__elementsObserver_6() { return static_cast<int32_t>(offsetof(ModelObserverNew_t1299566747, ____elementsObserver_6)); }
	inline ElementsObserverNew_t2308744357 * get__elementsObserver_6() const { return ____elementsObserver_6; }
	inline ElementsObserverNew_t2308744357 ** get_address_of__elementsObserver_6() { return &____elementsObserver_6; }
	inline void set__elementsObserver_6(ElementsObserverNew_t2308744357 * value)
	{
		____elementsObserver_6 = value;
		Il2CppCodeGenWriteBarrier((&____elementsObserver_6), value);
	}

	inline static int32_t get_offset_of__elementsInterface_7() { return static_cast<int32_t>(offsetof(ModelObserverNew_t1299566747, ____elementsInterface_7)); }
	inline RuntimeObject* get__elementsInterface_7() const { return ____elementsInterface_7; }
	inline RuntimeObject** get_address_of__elementsInterface_7() { return &____elementsInterface_7; }
	inline void set__elementsInterface_7(RuntimeObject* value)
	{
		____elementsInterface_7 = value;
		Il2CppCodeGenWriteBarrier((&____elementsInterface_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELOBSERVERNEW_T1299566747_H
#ifndef CHECKLISTMOBILEUINEW_T1060043268_H
#define CHECKLISTMOBILEUINEW_T1060043268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ChecklistUI.ChecklistMobileUINew
struct  ChecklistMobileUINew_t1060043268  : public MonoBehaviour_t1158329972
{
public:
	// System.String Library.Code.UI.ChecklistUI.ChecklistMobileUINew::POPUP_TITLE
	String_t* ___POPUP_TITLE_2;
	// Library.Code.Facades.CheckListF.CheckListFacade Library.Code.UI.ChecklistUI.ChecklistMobileUINew::_checkListFacade
	RuntimeObject* ____checkListFacade_3;
	// Library.Code.UI.PrefabGenerator.PrefabGenerator Library.Code.UI.ChecklistUI.ChecklistMobileUINew::_prefabGenerator
	RuntimeObject* ____prefabGenerator_4;
	// System.Boolean Library.Code.UI.ChecklistUI.ChecklistMobileUINew::started
	bool ___started_5;
	// UnityEngine.UI.Button Library.Code.UI.ChecklistUI.ChecklistMobileUINew::approveButton
	Button_t2872111280 * ___approveButton_6;
	// Library.Code.Facades.Elements.ElementApproveStateFacade Library.Code.UI.ChecklistUI.ChecklistMobileUINew::_elementApproveStateFacade
	RuntimeObject* ____elementApproveStateFacade_7;
	// Library.Code.DI.PopUp.PopUpShower Library.Code.UI.ChecklistUI.ChecklistMobileUINew::_popUpShower
	PopUpShower_t3186557280 * ____popUpShower_8;
	// TMPro.TMP_Dropdown Library.Code.UI.ChecklistUI.ChecklistMobileUINew::dropdown
	TMP_Dropdown_t1768193147 * ___dropdown_9;
	// UnityEngine.GameObject Library.Code.UI.ChecklistUI.ChecklistMobileUINew::popUp
	GameObject_t1756533147 * ___popUp_10;
	// System.Int32 Library.Code.UI.ChecklistUI.ChecklistMobileUINew::objectNumber
	int32_t ___objectNumber_11;

public:
	inline static int32_t get_offset_of_POPUP_TITLE_2() { return static_cast<int32_t>(offsetof(ChecklistMobileUINew_t1060043268, ___POPUP_TITLE_2)); }
	inline String_t* get_POPUP_TITLE_2() const { return ___POPUP_TITLE_2; }
	inline String_t** get_address_of_POPUP_TITLE_2() { return &___POPUP_TITLE_2; }
	inline void set_POPUP_TITLE_2(String_t* value)
	{
		___POPUP_TITLE_2 = value;
		Il2CppCodeGenWriteBarrier((&___POPUP_TITLE_2), value);
	}

	inline static int32_t get_offset_of__checkListFacade_3() { return static_cast<int32_t>(offsetof(ChecklistMobileUINew_t1060043268, ____checkListFacade_3)); }
	inline RuntimeObject* get__checkListFacade_3() const { return ____checkListFacade_3; }
	inline RuntimeObject** get_address_of__checkListFacade_3() { return &____checkListFacade_3; }
	inline void set__checkListFacade_3(RuntimeObject* value)
	{
		____checkListFacade_3 = value;
		Il2CppCodeGenWriteBarrier((&____checkListFacade_3), value);
	}

	inline static int32_t get_offset_of__prefabGenerator_4() { return static_cast<int32_t>(offsetof(ChecklistMobileUINew_t1060043268, ____prefabGenerator_4)); }
	inline RuntimeObject* get__prefabGenerator_4() const { return ____prefabGenerator_4; }
	inline RuntimeObject** get_address_of__prefabGenerator_4() { return &____prefabGenerator_4; }
	inline void set__prefabGenerator_4(RuntimeObject* value)
	{
		____prefabGenerator_4 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_4), value);
	}

	inline static int32_t get_offset_of_started_5() { return static_cast<int32_t>(offsetof(ChecklistMobileUINew_t1060043268, ___started_5)); }
	inline bool get_started_5() const { return ___started_5; }
	inline bool* get_address_of_started_5() { return &___started_5; }
	inline void set_started_5(bool value)
	{
		___started_5 = value;
	}

	inline static int32_t get_offset_of_approveButton_6() { return static_cast<int32_t>(offsetof(ChecklistMobileUINew_t1060043268, ___approveButton_6)); }
	inline Button_t2872111280 * get_approveButton_6() const { return ___approveButton_6; }
	inline Button_t2872111280 ** get_address_of_approveButton_6() { return &___approveButton_6; }
	inline void set_approveButton_6(Button_t2872111280 * value)
	{
		___approveButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___approveButton_6), value);
	}

	inline static int32_t get_offset_of__elementApproveStateFacade_7() { return static_cast<int32_t>(offsetof(ChecklistMobileUINew_t1060043268, ____elementApproveStateFacade_7)); }
	inline RuntimeObject* get__elementApproveStateFacade_7() const { return ____elementApproveStateFacade_7; }
	inline RuntimeObject** get_address_of__elementApproveStateFacade_7() { return &____elementApproveStateFacade_7; }
	inline void set__elementApproveStateFacade_7(RuntimeObject* value)
	{
		____elementApproveStateFacade_7 = value;
		Il2CppCodeGenWriteBarrier((&____elementApproveStateFacade_7), value);
	}

	inline static int32_t get_offset_of__popUpShower_8() { return static_cast<int32_t>(offsetof(ChecklistMobileUINew_t1060043268, ____popUpShower_8)); }
	inline PopUpShower_t3186557280 * get__popUpShower_8() const { return ____popUpShower_8; }
	inline PopUpShower_t3186557280 ** get_address_of__popUpShower_8() { return &____popUpShower_8; }
	inline void set__popUpShower_8(PopUpShower_t3186557280 * value)
	{
		____popUpShower_8 = value;
		Il2CppCodeGenWriteBarrier((&____popUpShower_8), value);
	}

	inline static int32_t get_offset_of_dropdown_9() { return static_cast<int32_t>(offsetof(ChecklistMobileUINew_t1060043268, ___dropdown_9)); }
	inline TMP_Dropdown_t1768193147 * get_dropdown_9() const { return ___dropdown_9; }
	inline TMP_Dropdown_t1768193147 ** get_address_of_dropdown_9() { return &___dropdown_9; }
	inline void set_dropdown_9(TMP_Dropdown_t1768193147 * value)
	{
		___dropdown_9 = value;
		Il2CppCodeGenWriteBarrier((&___dropdown_9), value);
	}

	inline static int32_t get_offset_of_popUp_10() { return static_cast<int32_t>(offsetof(ChecklistMobileUINew_t1060043268, ___popUp_10)); }
	inline GameObject_t1756533147 * get_popUp_10() const { return ___popUp_10; }
	inline GameObject_t1756533147 ** get_address_of_popUp_10() { return &___popUp_10; }
	inline void set_popUp_10(GameObject_t1756533147 * value)
	{
		___popUp_10 = value;
		Il2CppCodeGenWriteBarrier((&___popUp_10), value);
	}

	inline static int32_t get_offset_of_objectNumber_11() { return static_cast<int32_t>(offsetof(ChecklistMobileUINew_t1060043268, ___objectNumber_11)); }
	inline int32_t get_objectNumber_11() const { return ___objectNumber_11; }
	inline int32_t* get_address_of_objectNumber_11() { return &___objectNumber_11; }
	inline void set_objectNumber_11(int32_t value)
	{
		___objectNumber_11 = value;
	}
};

struct ChecklistMobileUINew_t1060043268_StaticFields
{
public:
	// System.Action Library.Code.UI.ChecklistUI.ChecklistMobileUINew::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_12() { return static_cast<int32_t>(offsetof(ChecklistMobileUINew_t1060043268_StaticFields, ___U3CU3Ef__amU24cache0_12)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_12() const { return ___U3CU3Ef__amU24cache0_12; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_12() { return &___U3CU3Ef__amU24cache0_12; }
	inline void set_U3CU3Ef__amU24cache0_12(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKLISTMOBILEUINEW_T1060043268_H
#ifndef MODELROWHANDLER_T2220072347_H
#define MODELROWHANDLER_T2220072347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MessageTemplates.ModelRowHandler
struct  ModelRowHandler_t2220072347  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.UI.ChooseModel.ModelRowHandlerDelegate Library.Code.UI.MessageTemplates.ModelRowHandler::_ModelRowHandlerDelegate
	RuntimeObject* ____ModelRowHandlerDelegate_2;
	// Library.Code.Domain.Entities.Model Library.Code.UI.MessageTemplates.ModelRowHandler::_model
	Model_t2163629551 * ____model_3;
	// Library.Code.Domain.Entities.Element Library.Code.UI.MessageTemplates.ModelRowHandler::_element
	Element_t2276588008 * ____element_4;
	// UnityEngine.UI.Button Library.Code.UI.MessageTemplates.ModelRowHandler::text
	Button_t2872111280 * ___text_5;

public:
	inline static int32_t get_offset_of__ModelRowHandlerDelegate_2() { return static_cast<int32_t>(offsetof(ModelRowHandler_t2220072347, ____ModelRowHandlerDelegate_2)); }
	inline RuntimeObject* get__ModelRowHandlerDelegate_2() const { return ____ModelRowHandlerDelegate_2; }
	inline RuntimeObject** get_address_of__ModelRowHandlerDelegate_2() { return &____ModelRowHandlerDelegate_2; }
	inline void set__ModelRowHandlerDelegate_2(RuntimeObject* value)
	{
		____ModelRowHandlerDelegate_2 = value;
		Il2CppCodeGenWriteBarrier((&____ModelRowHandlerDelegate_2), value);
	}

	inline static int32_t get_offset_of__model_3() { return static_cast<int32_t>(offsetof(ModelRowHandler_t2220072347, ____model_3)); }
	inline Model_t2163629551 * get__model_3() const { return ____model_3; }
	inline Model_t2163629551 ** get_address_of__model_3() { return &____model_3; }
	inline void set__model_3(Model_t2163629551 * value)
	{
		____model_3 = value;
		Il2CppCodeGenWriteBarrier((&____model_3), value);
	}

	inline static int32_t get_offset_of__element_4() { return static_cast<int32_t>(offsetof(ModelRowHandler_t2220072347, ____element_4)); }
	inline Element_t2276588008 * get__element_4() const { return ____element_4; }
	inline Element_t2276588008 ** get_address_of__element_4() { return &____element_4; }
	inline void set__element_4(Element_t2276588008 * value)
	{
		____element_4 = value;
		Il2CppCodeGenWriteBarrier((&____element_4), value);
	}

	inline static int32_t get_offset_of_text_5() { return static_cast<int32_t>(offsetof(ModelRowHandler_t2220072347, ___text_5)); }
	inline Button_t2872111280 * get_text_5() const { return ___text_5; }
	inline Button_t2872111280 ** get_address_of_text_5() { return &___text_5; }
	inline void set_text_5(Button_t2872111280 * value)
	{
		___text_5 = value;
		Il2CppCodeGenWriteBarrier((&___text_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELROWHANDLER_T2220072347_H
#ifndef MODELROWHANDLERNEW_T1567160379_H
#define MODELROWHANDLERNEW_T1567160379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MessageTemplates.ModelRowHandlerNew
struct  ModelRowHandlerNew_t1567160379  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.UI.ChooseModel.ModelRowHandlerDelegate Library.Code.UI.MessageTemplates.ModelRowHandlerNew::_ModelRowHandlerDelegate
	RuntimeObject* ____ModelRowHandlerDelegate_2;
	// Library.Code.Domain.Entities.Model Library.Code.UI.MessageTemplates.ModelRowHandlerNew::_model
	Model_t2163629551 * ____model_3;
	// Library.Code.Domain.Entities.Element Library.Code.UI.MessageTemplates.ModelRowHandlerNew::_element
	Element_t2276588008 * ____element_4;
	// UnityEngine.UI.Button Library.Code.UI.MessageTemplates.ModelRowHandlerNew::text
	Button_t2872111280 * ___text_5;

public:
	inline static int32_t get_offset_of__ModelRowHandlerDelegate_2() { return static_cast<int32_t>(offsetof(ModelRowHandlerNew_t1567160379, ____ModelRowHandlerDelegate_2)); }
	inline RuntimeObject* get__ModelRowHandlerDelegate_2() const { return ____ModelRowHandlerDelegate_2; }
	inline RuntimeObject** get_address_of__ModelRowHandlerDelegate_2() { return &____ModelRowHandlerDelegate_2; }
	inline void set__ModelRowHandlerDelegate_2(RuntimeObject* value)
	{
		____ModelRowHandlerDelegate_2 = value;
		Il2CppCodeGenWriteBarrier((&____ModelRowHandlerDelegate_2), value);
	}

	inline static int32_t get_offset_of__model_3() { return static_cast<int32_t>(offsetof(ModelRowHandlerNew_t1567160379, ____model_3)); }
	inline Model_t2163629551 * get__model_3() const { return ____model_3; }
	inline Model_t2163629551 ** get_address_of__model_3() { return &____model_3; }
	inline void set__model_3(Model_t2163629551 * value)
	{
		____model_3 = value;
		Il2CppCodeGenWriteBarrier((&____model_3), value);
	}

	inline static int32_t get_offset_of__element_4() { return static_cast<int32_t>(offsetof(ModelRowHandlerNew_t1567160379, ____element_4)); }
	inline Element_t2276588008 * get__element_4() const { return ____element_4; }
	inline Element_t2276588008 ** get_address_of__element_4() { return &____element_4; }
	inline void set__element_4(Element_t2276588008 * value)
	{
		____element_4 = value;
		Il2CppCodeGenWriteBarrier((&____element_4), value);
	}

	inline static int32_t get_offset_of_text_5() { return static_cast<int32_t>(offsetof(ModelRowHandlerNew_t1567160379, ___text_5)); }
	inline Button_t2872111280 * get_text_5() const { return ___text_5; }
	inline Button_t2872111280 ** get_address_of_text_5() { return &___text_5; }
	inline void set_text_5(Button_t2872111280 * value)
	{
		___text_5 = value;
		Il2CppCodeGenWriteBarrier((&___text_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELROWHANDLERNEW_T1567160379_H
#ifndef GROUPINCOLORNEW_T4212717807_H
#define GROUPINCOLORNEW_T4212717807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.UI.GroupinColorModel.GroupinColorNew
struct  GroupinColorNew_t4212717807  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Facades.ModelGrouping.ModelGroupFacade Library.UI.GroupinColorModel.GroupinColorNew::_modelGroupFacade
	RuntimeObject* ____modelGroupFacade_2;
	// Library.Code.UI.PrefabGenerator.PrefabGenerator Library.UI.GroupinColorModel.GroupinColorNew::_prefabGenerator
	RuntimeObject* ____prefabGenerator_3;
	// System.Boolean Library.UI.GroupinColorModel.GroupinColorNew::started
	bool ___started_4;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> Library.UI.GroupinColorModel.GroupinColorNew::existingItems
	List_1_t2719087314 * ___existingItems_5;
	// System.Single Library.UI.GroupinColorModel.GroupinColorNew::topMargin
	float ___topMargin_6;

public:
	inline static int32_t get_offset_of__modelGroupFacade_2() { return static_cast<int32_t>(offsetof(GroupinColorNew_t4212717807, ____modelGroupFacade_2)); }
	inline RuntimeObject* get__modelGroupFacade_2() const { return ____modelGroupFacade_2; }
	inline RuntimeObject** get_address_of__modelGroupFacade_2() { return &____modelGroupFacade_2; }
	inline void set__modelGroupFacade_2(RuntimeObject* value)
	{
		____modelGroupFacade_2 = value;
		Il2CppCodeGenWriteBarrier((&____modelGroupFacade_2), value);
	}

	inline static int32_t get_offset_of__prefabGenerator_3() { return static_cast<int32_t>(offsetof(GroupinColorNew_t4212717807, ____prefabGenerator_3)); }
	inline RuntimeObject* get__prefabGenerator_3() const { return ____prefabGenerator_3; }
	inline RuntimeObject** get_address_of__prefabGenerator_3() { return &____prefabGenerator_3; }
	inline void set__prefabGenerator_3(RuntimeObject* value)
	{
		____prefabGenerator_3 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_3), value);
	}

	inline static int32_t get_offset_of_started_4() { return static_cast<int32_t>(offsetof(GroupinColorNew_t4212717807, ___started_4)); }
	inline bool get_started_4() const { return ___started_4; }
	inline bool* get_address_of_started_4() { return &___started_4; }
	inline void set_started_4(bool value)
	{
		___started_4 = value;
	}

	inline static int32_t get_offset_of_existingItems_5() { return static_cast<int32_t>(offsetof(GroupinColorNew_t4212717807, ___existingItems_5)); }
	inline List_1_t2719087314 * get_existingItems_5() const { return ___existingItems_5; }
	inline List_1_t2719087314 ** get_address_of_existingItems_5() { return &___existingItems_5; }
	inline void set_existingItems_5(List_1_t2719087314 * value)
	{
		___existingItems_5 = value;
		Il2CppCodeGenWriteBarrier((&___existingItems_5), value);
	}

	inline static int32_t get_offset_of_topMargin_6() { return static_cast<int32_t>(offsetof(GroupinColorNew_t4212717807, ___topMargin_6)); }
	inline float get_topMargin_6() const { return ___topMargin_6; }
	inline float* get_address_of_topMargin_6() { return &___topMargin_6; }
	inline void set_topMargin_6(float value)
	{
		___topMargin_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPINCOLORNEW_T4212717807_H
#ifndef GROUPINCOLOR_T2175596517_H
#define GROUPINCOLOR_T2175596517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.UI.GroupinColorModel.GroupinColor
struct  GroupinColor_t2175596517  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Facades.ModelGrouping.ModelGroupFacade Library.UI.GroupinColorModel.GroupinColor::_modelGroupFacade
	RuntimeObject* ____modelGroupFacade_2;
	// Library.Code.UI.PrefabGenerator.PrefabGenerator Library.UI.GroupinColorModel.GroupinColor::_prefabGenerator
	RuntimeObject* ____prefabGenerator_3;
	// System.Boolean Library.UI.GroupinColorModel.GroupinColor::started
	bool ___started_4;

public:
	inline static int32_t get_offset_of__modelGroupFacade_2() { return static_cast<int32_t>(offsetof(GroupinColor_t2175596517, ____modelGroupFacade_2)); }
	inline RuntimeObject* get__modelGroupFacade_2() const { return ____modelGroupFacade_2; }
	inline RuntimeObject** get_address_of__modelGroupFacade_2() { return &____modelGroupFacade_2; }
	inline void set__modelGroupFacade_2(RuntimeObject* value)
	{
		____modelGroupFacade_2 = value;
		Il2CppCodeGenWriteBarrier((&____modelGroupFacade_2), value);
	}

	inline static int32_t get_offset_of__prefabGenerator_3() { return static_cast<int32_t>(offsetof(GroupinColor_t2175596517, ____prefabGenerator_3)); }
	inline RuntimeObject* get__prefabGenerator_3() const { return ____prefabGenerator_3; }
	inline RuntimeObject** get_address_of__prefabGenerator_3() { return &____prefabGenerator_3; }
	inline void set__prefabGenerator_3(RuntimeObject* value)
	{
		____prefabGenerator_3 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_3), value);
	}

	inline static int32_t get_offset_of_started_4() { return static_cast<int32_t>(offsetof(GroupinColor_t2175596517, ___started_4)); }
	inline bool get_started_4() const { return ___started_4; }
	inline bool* get_address_of_started_4() { return &___started_4; }
	inline void set_started_4(bool value)
	{
		___started_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPINCOLOR_T2175596517_H
#ifndef COLORROWHANDLERNEW_T1831333649_H
#define COLORROWHANDLERNEW_T1831333649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorRowHandlerNew
struct  ColorRowHandlerNew_t1831333649  : public MonoBehaviour_t1158329972
{
public:
	// TMPro.TextMeshProUGUI ColorRowHandlerNew::text
	TextMeshProUGUI_t934157183 * ___text_2;
	// UnityEngine.UI.Image ColorRowHandlerNew::color
	Image_t2042527209 * ___color_3;
	// Library.Code.Domain.Dtos.ModelGroupDto ColorRowHandlerNew::dto
	ModelGroupDto_t1808254547 * ___dto_4;
	// UnityEngine.GameObject ColorRowHandlerNew::onImage
	GameObject_t1756533147 * ___onImage_5;
	// UnityEngine.GameObject ColorRowHandlerNew::offImage
	GameObject_t1756533147 * ___offImage_6;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(ColorRowHandlerNew_t1831333649, ___text_2)); }
	inline TextMeshProUGUI_t934157183 * get_text_2() const { return ___text_2; }
	inline TextMeshProUGUI_t934157183 ** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(TextMeshProUGUI_t934157183 * value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier((&___text_2), value);
	}

	inline static int32_t get_offset_of_color_3() { return static_cast<int32_t>(offsetof(ColorRowHandlerNew_t1831333649, ___color_3)); }
	inline Image_t2042527209 * get_color_3() const { return ___color_3; }
	inline Image_t2042527209 ** get_address_of_color_3() { return &___color_3; }
	inline void set_color_3(Image_t2042527209 * value)
	{
		___color_3 = value;
		Il2CppCodeGenWriteBarrier((&___color_3), value);
	}

	inline static int32_t get_offset_of_dto_4() { return static_cast<int32_t>(offsetof(ColorRowHandlerNew_t1831333649, ___dto_4)); }
	inline ModelGroupDto_t1808254547 * get_dto_4() const { return ___dto_4; }
	inline ModelGroupDto_t1808254547 ** get_address_of_dto_4() { return &___dto_4; }
	inline void set_dto_4(ModelGroupDto_t1808254547 * value)
	{
		___dto_4 = value;
		Il2CppCodeGenWriteBarrier((&___dto_4), value);
	}

	inline static int32_t get_offset_of_onImage_5() { return static_cast<int32_t>(offsetof(ColorRowHandlerNew_t1831333649, ___onImage_5)); }
	inline GameObject_t1756533147 * get_onImage_5() const { return ___onImage_5; }
	inline GameObject_t1756533147 ** get_address_of_onImage_5() { return &___onImage_5; }
	inline void set_onImage_5(GameObject_t1756533147 * value)
	{
		___onImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___onImage_5), value);
	}

	inline static int32_t get_offset_of_offImage_6() { return static_cast<int32_t>(offsetof(ColorRowHandlerNew_t1831333649, ___offImage_6)); }
	inline GameObject_t1756533147 * get_offImage_6() const { return ___offImage_6; }
	inline GameObject_t1756533147 ** get_address_of_offImage_6() { return &___offImage_6; }
	inline void set_offImage_6(GameObject_t1756533147 * value)
	{
		___offImage_6 = value;
		Il2CppCodeGenWriteBarrier((&___offImage_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORROWHANDLERNEW_T1831333649_H
#ifndef COLORROWHANDLER_T2612092377_H
#define COLORROWHANDLER_T2612092377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColorRowHandler
struct  ColorRowHandler_t2612092377  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Toggle ColorRowHandler::checkbox
	Toggle_t3976754468 * ___checkbox_2;
	// TMPro.TextMeshProUGUI ColorRowHandler::text
	TextMeshProUGUI_t934157183 * ___text_3;
	// UnityEngine.UI.Image ColorRowHandler::color
	Image_t2042527209 * ___color_4;
	// Library.Code.Domain.Dtos.ModelGroupDto ColorRowHandler::dto
	ModelGroupDto_t1808254547 * ___dto_5;

public:
	inline static int32_t get_offset_of_checkbox_2() { return static_cast<int32_t>(offsetof(ColorRowHandler_t2612092377, ___checkbox_2)); }
	inline Toggle_t3976754468 * get_checkbox_2() const { return ___checkbox_2; }
	inline Toggle_t3976754468 ** get_address_of_checkbox_2() { return &___checkbox_2; }
	inline void set_checkbox_2(Toggle_t3976754468 * value)
	{
		___checkbox_2 = value;
		Il2CppCodeGenWriteBarrier((&___checkbox_2), value);
	}

	inline static int32_t get_offset_of_text_3() { return static_cast<int32_t>(offsetof(ColorRowHandler_t2612092377, ___text_3)); }
	inline TextMeshProUGUI_t934157183 * get_text_3() const { return ___text_3; }
	inline TextMeshProUGUI_t934157183 ** get_address_of_text_3() { return &___text_3; }
	inline void set_text_3(TextMeshProUGUI_t934157183 * value)
	{
		___text_3 = value;
		Il2CppCodeGenWriteBarrier((&___text_3), value);
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(ColorRowHandler_t2612092377, ___color_4)); }
	inline Image_t2042527209 * get_color_4() const { return ___color_4; }
	inline Image_t2042527209 ** get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Image_t2042527209 * value)
	{
		___color_4 = value;
		Il2CppCodeGenWriteBarrier((&___color_4), value);
	}

	inline static int32_t get_offset_of_dto_5() { return static_cast<int32_t>(offsetof(ColorRowHandler_t2612092377, ___dto_5)); }
	inline ModelGroupDto_t1808254547 * get_dto_5() const { return ___dto_5; }
	inline ModelGroupDto_t1808254547 ** get_address_of_dto_5() { return &___dto_5; }
	inline void set_dto_5(ModelGroupDto_t1808254547 * value)
	{
		___dto_5 = value;
		Il2CppCodeGenWriteBarrier((&___dto_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORROWHANDLER_T2612092377_H
#ifndef APPROVEELEMENTROW_T3948802595_H
#define APPROVEELEMENTROW_T3948802595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ElementStatus.ApproveElementRow
struct  ApproveElementRow_t3948802595  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button Library.Code.UI.ElementStatus.ApproveElementRow::button
	Button_t2872111280 * ___button_2;
	// Library.Code.UI.ElementStatus.AproveElementDto Library.Code.UI.ElementStatus.ApproveElementRow::itemStatus
	AproveElementDto_t688774064 * ___itemStatus_3;

public:
	inline static int32_t get_offset_of_button_2() { return static_cast<int32_t>(offsetof(ApproveElementRow_t3948802595, ___button_2)); }
	inline Button_t2872111280 * get_button_2() const { return ___button_2; }
	inline Button_t2872111280 ** get_address_of_button_2() { return &___button_2; }
	inline void set_button_2(Button_t2872111280 * value)
	{
		___button_2 = value;
		Il2CppCodeGenWriteBarrier((&___button_2), value);
	}

	inline static int32_t get_offset_of_itemStatus_3() { return static_cast<int32_t>(offsetof(ApproveElementRow_t3948802595, ___itemStatus_3)); }
	inline AproveElementDto_t688774064 * get_itemStatus_3() const { return ___itemStatus_3; }
	inline AproveElementDto_t688774064 ** get_address_of_itemStatus_3() { return &___itemStatus_3; }
	inline void set_itemStatus_3(AproveElementDto_t688774064 * value)
	{
		___itemStatus_3 = value;
		Il2CppCodeGenWriteBarrier((&___itemStatus_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPROVEELEMENTROW_T3948802595_H
#ifndef ELEMENTNEWSTATUSCHOOS_T3073096760_H
#define ELEMENTNEWSTATUSCHOOS_T3073096760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ElementStatus.ElementNewStatusChoos
struct  ElementNewStatusChoos_t3073096760  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<Library.Code.UI.ElementStatus.ApproveElementRow> Library.Code.UI.ElementStatus.ElementNewStatusChoos::_approveElements
	List_1_t3317923727 * ____approveElements_2;
	// Library.Code.UI.PrefabGenerator.PrefabGenerator Library.Code.UI.ElementStatus.ElementNewStatusChoos::_prefabGenerator
	RuntimeObject* ____prefabGenerator_3;
	// Library.Code.Domain.Entities.ApproveElementResponse Library.Code.UI.ElementStatus.ElementNewStatusChoos::_approveElementResponse
	ApproveElementResponse_t2765772516 * ____approveElementResponse_4;
	// UnityEngine.GameObject Library.Code.UI.ElementStatus.ElementNewStatusChoos::okButton
	GameObject_t1756533147 * ___okButton_5;
	// System.Action`1<Library.Code.Domain.Entities.ApproveElementResponse> Library.Code.UI.ElementStatus.ElementNewStatusChoos::choosenState
	Action_1_t2567571898 * ___choosenState_6;
	// System.Action Library.Code.UI.ElementStatus.ElementNewStatusChoos::cancel
	Action_t3226471752 * ___cancel_7;

public:
	inline static int32_t get_offset_of__approveElements_2() { return static_cast<int32_t>(offsetof(ElementNewStatusChoos_t3073096760, ____approveElements_2)); }
	inline List_1_t3317923727 * get__approveElements_2() const { return ____approveElements_2; }
	inline List_1_t3317923727 ** get_address_of__approveElements_2() { return &____approveElements_2; }
	inline void set__approveElements_2(List_1_t3317923727 * value)
	{
		____approveElements_2 = value;
		Il2CppCodeGenWriteBarrier((&____approveElements_2), value);
	}

	inline static int32_t get_offset_of__prefabGenerator_3() { return static_cast<int32_t>(offsetof(ElementNewStatusChoos_t3073096760, ____prefabGenerator_3)); }
	inline RuntimeObject* get__prefabGenerator_3() const { return ____prefabGenerator_3; }
	inline RuntimeObject** get_address_of__prefabGenerator_3() { return &____prefabGenerator_3; }
	inline void set__prefabGenerator_3(RuntimeObject* value)
	{
		____prefabGenerator_3 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_3), value);
	}

	inline static int32_t get_offset_of__approveElementResponse_4() { return static_cast<int32_t>(offsetof(ElementNewStatusChoos_t3073096760, ____approveElementResponse_4)); }
	inline ApproveElementResponse_t2765772516 * get__approveElementResponse_4() const { return ____approveElementResponse_4; }
	inline ApproveElementResponse_t2765772516 ** get_address_of__approveElementResponse_4() { return &____approveElementResponse_4; }
	inline void set__approveElementResponse_4(ApproveElementResponse_t2765772516 * value)
	{
		____approveElementResponse_4 = value;
		Il2CppCodeGenWriteBarrier((&____approveElementResponse_4), value);
	}

	inline static int32_t get_offset_of_okButton_5() { return static_cast<int32_t>(offsetof(ElementNewStatusChoos_t3073096760, ___okButton_5)); }
	inline GameObject_t1756533147 * get_okButton_5() const { return ___okButton_5; }
	inline GameObject_t1756533147 ** get_address_of_okButton_5() { return &___okButton_5; }
	inline void set_okButton_5(GameObject_t1756533147 * value)
	{
		___okButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___okButton_5), value);
	}

	inline static int32_t get_offset_of_choosenState_6() { return static_cast<int32_t>(offsetof(ElementNewStatusChoos_t3073096760, ___choosenState_6)); }
	inline Action_1_t2567571898 * get_choosenState_6() const { return ___choosenState_6; }
	inline Action_1_t2567571898 ** get_address_of_choosenState_6() { return &___choosenState_6; }
	inline void set_choosenState_6(Action_1_t2567571898 * value)
	{
		___choosenState_6 = value;
		Il2CppCodeGenWriteBarrier((&___choosenState_6), value);
	}

	inline static int32_t get_offset_of_cancel_7() { return static_cast<int32_t>(offsetof(ElementNewStatusChoos_t3073096760, ___cancel_7)); }
	inline Action_t3226471752 * get_cancel_7() const { return ___cancel_7; }
	inline Action_t3226471752 ** get_address_of_cancel_7() { return &___cancel_7; }
	inline void set_cancel_7(Action_t3226471752 * value)
	{
		___cancel_7 = value;
		Il2CppCodeGenWriteBarrier((&___cancel_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTNEWSTATUSCHOOS_T3073096760_H
#ifndef MESSAGELISTROW_T2364007661_H
#define MESSAGELISTROW_T2364007661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MessagesList.MessageListRow
struct  MessageListRow_t2364007661  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Facades.Messages.MessagesFacade Library.Code.UI.MessagesList.MessageListRow::_messagesFacade
	RuntimeObject* ____messagesFacade_2;
	// UnityEngine.UI.Button Library.Code.UI.MessagesList.MessageListRow::_button
	Button_t2872111280 * ____button_3;
	// UnityEngine.GameObject Library.Code.UI.MessagesList.MessageListRow::_recordPanel
	GameObject_t1756533147 * ____recordPanel_4;
	// UnityEngine.UI.Button Library.Code.UI.MessagesList.MessageListRow::_textButton
	Button_t2872111280 * ____textButton_5;
	// UnityEngine.UI.Text Library.Code.UI.MessagesList.MessageListRow::_text
	Text_t356221433 * ____text_6;
	// UnityEngine.UI.Text Library.Code.UI.MessagesList.MessageListRow::_rectordText
	Text_t356221433 * ____rectordText_7;
	// Library.Code.Domain.Entities.Message Library.Code.UI.MessagesList.MessageListRow::_message
	Message_t1214853957 * ____message_8;
	// PhotoPopup Library.Code.UI.MessagesList.MessageListRow::_photoPopup
	PhotoPopup_t4101423206 * ____photoPopup_9;
	// UnityEngine.CanvasGroup Library.Code.UI.MessagesList.MessageListRow::_photoPopupCanvasGroup
	CanvasGroup_t3296560743 * ____photoPopupCanvasGroup_10;
	// System.Boolean Library.Code.UI.MessagesList.MessageListRow::started
	bool ___started_11;

public:
	inline static int32_t get_offset_of__messagesFacade_2() { return static_cast<int32_t>(offsetof(MessageListRow_t2364007661, ____messagesFacade_2)); }
	inline RuntimeObject* get__messagesFacade_2() const { return ____messagesFacade_2; }
	inline RuntimeObject** get_address_of__messagesFacade_2() { return &____messagesFacade_2; }
	inline void set__messagesFacade_2(RuntimeObject* value)
	{
		____messagesFacade_2 = value;
		Il2CppCodeGenWriteBarrier((&____messagesFacade_2), value);
	}

	inline static int32_t get_offset_of__button_3() { return static_cast<int32_t>(offsetof(MessageListRow_t2364007661, ____button_3)); }
	inline Button_t2872111280 * get__button_3() const { return ____button_3; }
	inline Button_t2872111280 ** get_address_of__button_3() { return &____button_3; }
	inline void set__button_3(Button_t2872111280 * value)
	{
		____button_3 = value;
		Il2CppCodeGenWriteBarrier((&____button_3), value);
	}

	inline static int32_t get_offset_of__recordPanel_4() { return static_cast<int32_t>(offsetof(MessageListRow_t2364007661, ____recordPanel_4)); }
	inline GameObject_t1756533147 * get__recordPanel_4() const { return ____recordPanel_4; }
	inline GameObject_t1756533147 ** get_address_of__recordPanel_4() { return &____recordPanel_4; }
	inline void set__recordPanel_4(GameObject_t1756533147 * value)
	{
		____recordPanel_4 = value;
		Il2CppCodeGenWriteBarrier((&____recordPanel_4), value);
	}

	inline static int32_t get_offset_of__textButton_5() { return static_cast<int32_t>(offsetof(MessageListRow_t2364007661, ____textButton_5)); }
	inline Button_t2872111280 * get__textButton_5() const { return ____textButton_5; }
	inline Button_t2872111280 ** get_address_of__textButton_5() { return &____textButton_5; }
	inline void set__textButton_5(Button_t2872111280 * value)
	{
		____textButton_5 = value;
		Il2CppCodeGenWriteBarrier((&____textButton_5), value);
	}

	inline static int32_t get_offset_of__text_6() { return static_cast<int32_t>(offsetof(MessageListRow_t2364007661, ____text_6)); }
	inline Text_t356221433 * get__text_6() const { return ____text_6; }
	inline Text_t356221433 ** get_address_of__text_6() { return &____text_6; }
	inline void set__text_6(Text_t356221433 * value)
	{
		____text_6 = value;
		Il2CppCodeGenWriteBarrier((&____text_6), value);
	}

	inline static int32_t get_offset_of__rectordText_7() { return static_cast<int32_t>(offsetof(MessageListRow_t2364007661, ____rectordText_7)); }
	inline Text_t356221433 * get__rectordText_7() const { return ____rectordText_7; }
	inline Text_t356221433 ** get_address_of__rectordText_7() { return &____rectordText_7; }
	inline void set__rectordText_7(Text_t356221433 * value)
	{
		____rectordText_7 = value;
		Il2CppCodeGenWriteBarrier((&____rectordText_7), value);
	}

	inline static int32_t get_offset_of__message_8() { return static_cast<int32_t>(offsetof(MessageListRow_t2364007661, ____message_8)); }
	inline Message_t1214853957 * get__message_8() const { return ____message_8; }
	inline Message_t1214853957 ** get_address_of__message_8() { return &____message_8; }
	inline void set__message_8(Message_t1214853957 * value)
	{
		____message_8 = value;
		Il2CppCodeGenWriteBarrier((&____message_8), value);
	}

	inline static int32_t get_offset_of__photoPopup_9() { return static_cast<int32_t>(offsetof(MessageListRow_t2364007661, ____photoPopup_9)); }
	inline PhotoPopup_t4101423206 * get__photoPopup_9() const { return ____photoPopup_9; }
	inline PhotoPopup_t4101423206 ** get_address_of__photoPopup_9() { return &____photoPopup_9; }
	inline void set__photoPopup_9(PhotoPopup_t4101423206 * value)
	{
		____photoPopup_9 = value;
		Il2CppCodeGenWriteBarrier((&____photoPopup_9), value);
	}

	inline static int32_t get_offset_of__photoPopupCanvasGroup_10() { return static_cast<int32_t>(offsetof(MessageListRow_t2364007661, ____photoPopupCanvasGroup_10)); }
	inline CanvasGroup_t3296560743 * get__photoPopupCanvasGroup_10() const { return ____photoPopupCanvasGroup_10; }
	inline CanvasGroup_t3296560743 ** get_address_of__photoPopupCanvasGroup_10() { return &____photoPopupCanvasGroup_10; }
	inline void set__photoPopupCanvasGroup_10(CanvasGroup_t3296560743 * value)
	{
		____photoPopupCanvasGroup_10 = value;
		Il2CppCodeGenWriteBarrier((&____photoPopupCanvasGroup_10), value);
	}

	inline static int32_t get_offset_of_started_11() { return static_cast<int32_t>(offsetof(MessageListRow_t2364007661, ___started_11)); }
	inline bool get_started_11() const { return ___started_11; }
	inline bool* get_address_of_started_11() { return &___started_11; }
	inline void set_started_11(bool value)
	{
		___started_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGELISTROW_T2364007661_H
#ifndef OPENREMARK_BUTTON_T1024976505_H
#define OPENREMARK_BUTTON_T1024976505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MessageTemplates.OpenRemark_Button
struct  OpenRemark_Button_t1024976505  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Library.Code.UI.MessageTemplates.OpenRemark_Button::popUp
	GameObject_t1756533147 * ___popUp_2;

public:
	inline static int32_t get_offset_of_popUp_2() { return static_cast<int32_t>(offsetof(OpenRemark_Button_t1024976505, ___popUp_2)); }
	inline GameObject_t1756533147 * get_popUp_2() const { return ___popUp_2; }
	inline GameObject_t1756533147 ** get_address_of_popUp_2() { return &___popUp_2; }
	inline void set_popUp_2(GameObject_t1756533147 * value)
	{
		___popUp_2 = value;
		Il2CppCodeGenWriteBarrier((&___popUp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENREMARK_BUTTON_T1024976505_H
#ifndef LISTVIEWHANDLER_T424984033_H
#define LISTVIEWHANDLER_T424984033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MessageTemplates.ListViewHandler
struct  ListViewHandler_t424984033  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.InputField Library.Code.UI.MessageTemplates.ListViewHandler::textToBeFilledUpWithTemplate
	InputField_t1631627530 * ___textToBeFilledUpWithTemplate_2;
	// Library.Code.Facades.MessageTemplates.MessageTemplatesFacade Library.Code.UI.MessageTemplates.ListViewHandler::_messageTemplatesFacade
	RuntimeObject* ____messageTemplatesFacade_3;
	// Library.Code.UI.PrefabGenerator.PrefabGenerator Library.Code.UI.MessageTemplates.ListViewHandler::_prefabGenerator
	RuntimeObject* ____prefabGenerator_4;
	// System.Boolean Library.Code.UI.MessageTemplates.ListViewHandler::started
	bool ___started_5;

public:
	inline static int32_t get_offset_of_textToBeFilledUpWithTemplate_2() { return static_cast<int32_t>(offsetof(ListViewHandler_t424984033, ___textToBeFilledUpWithTemplate_2)); }
	inline InputField_t1631627530 * get_textToBeFilledUpWithTemplate_2() const { return ___textToBeFilledUpWithTemplate_2; }
	inline InputField_t1631627530 ** get_address_of_textToBeFilledUpWithTemplate_2() { return &___textToBeFilledUpWithTemplate_2; }
	inline void set_textToBeFilledUpWithTemplate_2(InputField_t1631627530 * value)
	{
		___textToBeFilledUpWithTemplate_2 = value;
		Il2CppCodeGenWriteBarrier((&___textToBeFilledUpWithTemplate_2), value);
	}

	inline static int32_t get_offset_of__messageTemplatesFacade_3() { return static_cast<int32_t>(offsetof(ListViewHandler_t424984033, ____messageTemplatesFacade_3)); }
	inline RuntimeObject* get__messageTemplatesFacade_3() const { return ____messageTemplatesFacade_3; }
	inline RuntimeObject** get_address_of__messageTemplatesFacade_3() { return &____messageTemplatesFacade_3; }
	inline void set__messageTemplatesFacade_3(RuntimeObject* value)
	{
		____messageTemplatesFacade_3 = value;
		Il2CppCodeGenWriteBarrier((&____messageTemplatesFacade_3), value);
	}

	inline static int32_t get_offset_of__prefabGenerator_4() { return static_cast<int32_t>(offsetof(ListViewHandler_t424984033, ____prefabGenerator_4)); }
	inline RuntimeObject* get__prefabGenerator_4() const { return ____prefabGenerator_4; }
	inline RuntimeObject** get_address_of__prefabGenerator_4() { return &____prefabGenerator_4; }
	inline void set__prefabGenerator_4(RuntimeObject* value)
	{
		____prefabGenerator_4 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_4), value);
	}

	inline static int32_t get_offset_of_started_5() { return static_cast<int32_t>(offsetof(ListViewHandler_t424984033, ___started_5)); }
	inline bool get_started_5() const { return ___started_5; }
	inline bool* get_address_of_started_5() { return &___started_5; }
	inline void set_started_5(bool value)
	{
		___started_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTVIEWHANDLER_T424984033_H
#ifndef DOSOMETHING_T3992512957_H
#define DOSOMETHING_T3992512957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MessageTemplates.DoSomething
struct  DoSomething_t3992512957  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOSOMETHING_T3992512957_H
#ifndef CLOSEREMARK_BUTTON_T3324403479_H
#define CLOSEREMARK_BUTTON_T3324403479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MessageTemplates.CloseRemark_Button
struct  CloseRemark_Button_t3324403479  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Library.Code.UI.MessageTemplates.CloseRemark_Button::popUp
	GameObject_t1756533147 * ___popUp_2;

public:
	inline static int32_t get_offset_of_popUp_2() { return static_cast<int32_t>(offsetof(CloseRemark_Button_t3324403479, ___popUp_2)); }
	inline GameObject_t1756533147 * get_popUp_2() const { return ___popUp_2; }
	inline GameObject_t1756533147 ** get_address_of_popUp_2() { return &___popUp_2; }
	inline void set_popUp_2(GameObject_t1756533147 * value)
	{
		___popUp_2 = value;
		Il2CppCodeGenWriteBarrier((&___popUp_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSEREMARK_BUTTON_T3324403479_H
#ifndef SENDMESSAGEBUTTONHANDLER_T618142915_H
#define SENDMESSAGEBUTTONHANDLER_T618142915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MessagesList.SendMessageButtonHandler
struct  SendMessageButtonHandler_t618142915  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Facades.Messages.SendMessageFacade Library.Code.UI.MessagesList.SendMessageButtonHandler::_sendMessageFacade
	RuntimeObject* ____sendMessageFacade_2;
	// Library.Code.Facades._3dElementDownload.Element3dFacade Library.Code.UI.MessagesList.SendMessageButtonHandler::_element3dFacade
	RuntimeObject* ____element3dFacade_3;
	// System.Boolean Library.Code.UI.MessagesList.SendMessageButtonHandler::started
	bool ___started_4;
	// System.Int64 Library.Code.UI.MessagesList.SendMessageButtonHandler::_currentElemntId
	int64_t ____currentElemntId_5;
	// Library.Code.DI.PopUp.PopUpShower Library.Code.UI.MessagesList.SendMessageButtonHandler::_popUpShower
	PopUpShower_t3186557280 * ____popUpShower_6;
	// UnityEngine.GameObject Library.Code.UI.MessagesList.SendMessageButtonHandler::popUpLoading
	GameObject_t1756533147 * ___popUpLoading_7;

public:
	inline static int32_t get_offset_of__sendMessageFacade_2() { return static_cast<int32_t>(offsetof(SendMessageButtonHandler_t618142915, ____sendMessageFacade_2)); }
	inline RuntimeObject* get__sendMessageFacade_2() const { return ____sendMessageFacade_2; }
	inline RuntimeObject** get_address_of__sendMessageFacade_2() { return &____sendMessageFacade_2; }
	inline void set__sendMessageFacade_2(RuntimeObject* value)
	{
		____sendMessageFacade_2 = value;
		Il2CppCodeGenWriteBarrier((&____sendMessageFacade_2), value);
	}

	inline static int32_t get_offset_of__element3dFacade_3() { return static_cast<int32_t>(offsetof(SendMessageButtonHandler_t618142915, ____element3dFacade_3)); }
	inline RuntimeObject* get__element3dFacade_3() const { return ____element3dFacade_3; }
	inline RuntimeObject** get_address_of__element3dFacade_3() { return &____element3dFacade_3; }
	inline void set__element3dFacade_3(RuntimeObject* value)
	{
		____element3dFacade_3 = value;
		Il2CppCodeGenWriteBarrier((&____element3dFacade_3), value);
	}

	inline static int32_t get_offset_of_started_4() { return static_cast<int32_t>(offsetof(SendMessageButtonHandler_t618142915, ___started_4)); }
	inline bool get_started_4() const { return ___started_4; }
	inline bool* get_address_of_started_4() { return &___started_4; }
	inline void set_started_4(bool value)
	{
		___started_4 = value;
	}

	inline static int32_t get_offset_of__currentElemntId_5() { return static_cast<int32_t>(offsetof(SendMessageButtonHandler_t618142915, ____currentElemntId_5)); }
	inline int64_t get__currentElemntId_5() const { return ____currentElemntId_5; }
	inline int64_t* get_address_of__currentElemntId_5() { return &____currentElemntId_5; }
	inline void set__currentElemntId_5(int64_t value)
	{
		____currentElemntId_5 = value;
	}

	inline static int32_t get_offset_of__popUpShower_6() { return static_cast<int32_t>(offsetof(SendMessageButtonHandler_t618142915, ____popUpShower_6)); }
	inline PopUpShower_t3186557280 * get__popUpShower_6() const { return ____popUpShower_6; }
	inline PopUpShower_t3186557280 ** get_address_of__popUpShower_6() { return &____popUpShower_6; }
	inline void set__popUpShower_6(PopUpShower_t3186557280 * value)
	{
		____popUpShower_6 = value;
		Il2CppCodeGenWriteBarrier((&____popUpShower_6), value);
	}

	inline static int32_t get_offset_of_popUpLoading_7() { return static_cast<int32_t>(offsetof(SendMessageButtonHandler_t618142915, ___popUpLoading_7)); }
	inline GameObject_t1756533147 * get_popUpLoading_7() const { return ___popUpLoading_7; }
	inline GameObject_t1756533147 ** get_address_of_popUpLoading_7() { return &___popUpLoading_7; }
	inline void set_popUpLoading_7(GameObject_t1756533147 * value)
	{
		___popUpLoading_7 = value;
		Il2CppCodeGenWriteBarrier((&___popUpLoading_7), value);
	}
};

struct SendMessageButtonHandler_t618142915_StaticFields
{
public:
	// System.Action Library.Code.UI.MessagesList.SendMessageButtonHandler::<>f__am$cache0
	Action_t3226471752 * ___U3CU3Ef__amU24cache0_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_8() { return static_cast<int32_t>(offsetof(SendMessageButtonHandler_t618142915_StaticFields, ___U3CU3Ef__amU24cache0_8)); }
	inline Action_t3226471752 * get_U3CU3Ef__amU24cache0_8() const { return ___U3CU3Ef__amU24cache0_8; }
	inline Action_t3226471752 ** get_address_of_U3CU3Ef__amU24cache0_8() { return &___U3CU3Ef__amU24cache0_8; }
	inline void set_U3CU3Ef__amU24cache0_8(Action_t3226471752 * value)
	{
		___U3CU3Ef__amU24cache0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDMESSAGEBUTTONHANDLER_T618142915_H
#ifndef MODELOBSERVER_T1172265923_H
#define MODELOBSERVER_T1172265923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.ChooseModel.ModelObserver
struct  ModelObserver_t1172265923  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Facades.Models.ModelFacade Library.Code.UI.ChooseModel.ModelObserver::_modelFacade
	RuntimeObject* ____modelFacade_2;
	// Library.Code.UI.PrefabGenerator.PrefabGenerator Library.Code.UI.ChooseModel.ModelObserver::_prefabGenerator
	RuntimeObject* ____prefabGenerator_3;
	// System.Collections.Generic.List`1<Library.Code.Domain.Entities.Model> Library.Code.UI.ChooseModel.ModelObserver::_models
	List_1_t1532750683 * ____models_4;
	// Library.Code.Facades.Elements.ElementFacade Library.Code.UI.ChooseModel.ModelObserver::_elementFacade
	RuntimeObject* ____elementFacade_5;
	// Library.Code.UI.ChooseModel.ElementsInterface Library.Code.UI.ChooseModel.ModelObserver::_elementsInterface
	RuntimeObject* ____elementsInterface_6;

public:
	inline static int32_t get_offset_of__modelFacade_2() { return static_cast<int32_t>(offsetof(ModelObserver_t1172265923, ____modelFacade_2)); }
	inline RuntimeObject* get__modelFacade_2() const { return ____modelFacade_2; }
	inline RuntimeObject** get_address_of__modelFacade_2() { return &____modelFacade_2; }
	inline void set__modelFacade_2(RuntimeObject* value)
	{
		____modelFacade_2 = value;
		Il2CppCodeGenWriteBarrier((&____modelFacade_2), value);
	}

	inline static int32_t get_offset_of__prefabGenerator_3() { return static_cast<int32_t>(offsetof(ModelObserver_t1172265923, ____prefabGenerator_3)); }
	inline RuntimeObject* get__prefabGenerator_3() const { return ____prefabGenerator_3; }
	inline RuntimeObject** get_address_of__prefabGenerator_3() { return &____prefabGenerator_3; }
	inline void set__prefabGenerator_3(RuntimeObject* value)
	{
		____prefabGenerator_3 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_3), value);
	}

	inline static int32_t get_offset_of__models_4() { return static_cast<int32_t>(offsetof(ModelObserver_t1172265923, ____models_4)); }
	inline List_1_t1532750683 * get__models_4() const { return ____models_4; }
	inline List_1_t1532750683 ** get_address_of__models_4() { return &____models_4; }
	inline void set__models_4(List_1_t1532750683 * value)
	{
		____models_4 = value;
		Il2CppCodeGenWriteBarrier((&____models_4), value);
	}

	inline static int32_t get_offset_of__elementFacade_5() { return static_cast<int32_t>(offsetof(ModelObserver_t1172265923, ____elementFacade_5)); }
	inline RuntimeObject* get__elementFacade_5() const { return ____elementFacade_5; }
	inline RuntimeObject** get_address_of__elementFacade_5() { return &____elementFacade_5; }
	inline void set__elementFacade_5(RuntimeObject* value)
	{
		____elementFacade_5 = value;
		Il2CppCodeGenWriteBarrier((&____elementFacade_5), value);
	}

	inline static int32_t get_offset_of__elementsInterface_6() { return static_cast<int32_t>(offsetof(ModelObserver_t1172265923, ____elementsInterface_6)); }
	inline RuntimeObject* get__elementsInterface_6() const { return ____elementsInterface_6; }
	inline RuntimeObject** get_address_of__elementsInterface_6() { return &____elementsInterface_6; }
	inline void set__elementsInterface_6(RuntimeObject* value)
	{
		____elementsInterface_6 = value;
		Il2CppCodeGenWriteBarrier((&____elementsInterface_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELOBSERVER_T1172265923_H
#ifndef MESSAGELISTROWNEW_T2419996211_H
#define MESSAGELISTROWNEW_T2419996211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MessagesList.New.MessageListRowNew
struct  MessageListRowNew_t2419996211  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Facades.Messages.MessagesFacade Library.Code.UI.MessagesList.New.MessageListRowNew::_messagesFacade
	RuntimeObject* ____messagesFacade_2;
	// UnityEngine.UI.Button Library.Code.UI.MessagesList.New.MessageListRowNew::_button
	Button_t2872111280 * ____button_3;
	// TMPro.TextMeshProUGUI Library.Code.UI.MessagesList.New.MessageListRowNew::_text
	TextMeshProUGUI_t934157183 * ____text_4;
	// TMPro.TextMeshProUGUI Library.Code.UI.MessagesList.New.MessageListRowNew::audioText
	TextMeshProUGUI_t934157183 * ___audioText_5;
	// TMPro.TextMeshProUGUI Library.Code.UI.MessagesList.New.MessageListRowNew::photoText
	TextMeshProUGUI_t934157183 * ___photoText_6;
	// Library.Code.Domain.Entities.Message Library.Code.UI.MessagesList.New.MessageListRowNew::_message
	Message_t1214853957 * ____message_7;
	// PhotoPopup Library.Code.UI.MessagesList.New.MessageListRowNew::_photoPopup
	PhotoPopup_t4101423206 * ____photoPopup_8;
	// UnityEngine.CanvasGroup Library.Code.UI.MessagesList.New.MessageListRowNew::_photoPopupCanvasGroup
	CanvasGroup_t3296560743 * ____photoPopupCanvasGroup_9;
	// System.Boolean Library.Code.UI.MessagesList.New.MessageListRowNew::started
	bool ___started_10;

public:
	inline static int32_t get_offset_of__messagesFacade_2() { return static_cast<int32_t>(offsetof(MessageListRowNew_t2419996211, ____messagesFacade_2)); }
	inline RuntimeObject* get__messagesFacade_2() const { return ____messagesFacade_2; }
	inline RuntimeObject** get_address_of__messagesFacade_2() { return &____messagesFacade_2; }
	inline void set__messagesFacade_2(RuntimeObject* value)
	{
		____messagesFacade_2 = value;
		Il2CppCodeGenWriteBarrier((&____messagesFacade_2), value);
	}

	inline static int32_t get_offset_of__button_3() { return static_cast<int32_t>(offsetof(MessageListRowNew_t2419996211, ____button_3)); }
	inline Button_t2872111280 * get__button_3() const { return ____button_3; }
	inline Button_t2872111280 ** get_address_of__button_3() { return &____button_3; }
	inline void set__button_3(Button_t2872111280 * value)
	{
		____button_3 = value;
		Il2CppCodeGenWriteBarrier((&____button_3), value);
	}

	inline static int32_t get_offset_of__text_4() { return static_cast<int32_t>(offsetof(MessageListRowNew_t2419996211, ____text_4)); }
	inline TextMeshProUGUI_t934157183 * get__text_4() const { return ____text_4; }
	inline TextMeshProUGUI_t934157183 ** get_address_of__text_4() { return &____text_4; }
	inline void set__text_4(TextMeshProUGUI_t934157183 * value)
	{
		____text_4 = value;
		Il2CppCodeGenWriteBarrier((&____text_4), value);
	}

	inline static int32_t get_offset_of_audioText_5() { return static_cast<int32_t>(offsetof(MessageListRowNew_t2419996211, ___audioText_5)); }
	inline TextMeshProUGUI_t934157183 * get_audioText_5() const { return ___audioText_5; }
	inline TextMeshProUGUI_t934157183 ** get_address_of_audioText_5() { return &___audioText_5; }
	inline void set_audioText_5(TextMeshProUGUI_t934157183 * value)
	{
		___audioText_5 = value;
		Il2CppCodeGenWriteBarrier((&___audioText_5), value);
	}

	inline static int32_t get_offset_of_photoText_6() { return static_cast<int32_t>(offsetof(MessageListRowNew_t2419996211, ___photoText_6)); }
	inline TextMeshProUGUI_t934157183 * get_photoText_6() const { return ___photoText_6; }
	inline TextMeshProUGUI_t934157183 ** get_address_of_photoText_6() { return &___photoText_6; }
	inline void set_photoText_6(TextMeshProUGUI_t934157183 * value)
	{
		___photoText_6 = value;
		Il2CppCodeGenWriteBarrier((&___photoText_6), value);
	}

	inline static int32_t get_offset_of__message_7() { return static_cast<int32_t>(offsetof(MessageListRowNew_t2419996211, ____message_7)); }
	inline Message_t1214853957 * get__message_7() const { return ____message_7; }
	inline Message_t1214853957 ** get_address_of__message_7() { return &____message_7; }
	inline void set__message_7(Message_t1214853957 * value)
	{
		____message_7 = value;
		Il2CppCodeGenWriteBarrier((&____message_7), value);
	}

	inline static int32_t get_offset_of__photoPopup_8() { return static_cast<int32_t>(offsetof(MessageListRowNew_t2419996211, ____photoPopup_8)); }
	inline PhotoPopup_t4101423206 * get__photoPopup_8() const { return ____photoPopup_8; }
	inline PhotoPopup_t4101423206 ** get_address_of__photoPopup_8() { return &____photoPopup_8; }
	inline void set__photoPopup_8(PhotoPopup_t4101423206 * value)
	{
		____photoPopup_8 = value;
		Il2CppCodeGenWriteBarrier((&____photoPopup_8), value);
	}

	inline static int32_t get_offset_of__photoPopupCanvasGroup_9() { return static_cast<int32_t>(offsetof(MessageListRowNew_t2419996211, ____photoPopupCanvasGroup_9)); }
	inline CanvasGroup_t3296560743 * get__photoPopupCanvasGroup_9() const { return ____photoPopupCanvasGroup_9; }
	inline CanvasGroup_t3296560743 ** get_address_of__photoPopupCanvasGroup_9() { return &____photoPopupCanvasGroup_9; }
	inline void set__photoPopupCanvasGroup_9(CanvasGroup_t3296560743 * value)
	{
		____photoPopupCanvasGroup_9 = value;
		Il2CppCodeGenWriteBarrier((&____photoPopupCanvasGroup_9), value);
	}

	inline static int32_t get_offset_of_started_10() { return static_cast<int32_t>(offsetof(MessageListRowNew_t2419996211, ___started_10)); }
	inline bool get_started_10() const { return ___started_10; }
	inline bool* get_address_of_started_10() { return &___started_10; }
	inline void set_started_10(bool value)
	{
		___started_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGELISTROWNEW_T2419996211_H
#ifndef MESSAGESLISTHANDLERNEW_T4229393496_H
#define MESSAGESLISTHANDLERNEW_T4229393496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MessagesList.New.MessagesListHandlerNew
struct  MessagesListHandlerNew_t4229393496  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Facades.Messages.MessagesFacade Library.Code.UI.MessagesList.New.MessagesListHandlerNew::_messagesFacade
	RuntimeObject* ____messagesFacade_2;
	// Library.Code.UI.PrefabGenerator.PrefabGenerator Library.Code.UI.MessagesList.New.MessagesListHandlerNew::_prefabGenerator
	RuntimeObject* ____prefabGenerator_3;

public:
	inline static int32_t get_offset_of__messagesFacade_2() { return static_cast<int32_t>(offsetof(MessagesListHandlerNew_t4229393496, ____messagesFacade_2)); }
	inline RuntimeObject* get__messagesFacade_2() const { return ____messagesFacade_2; }
	inline RuntimeObject** get_address_of__messagesFacade_2() { return &____messagesFacade_2; }
	inline void set__messagesFacade_2(RuntimeObject* value)
	{
		____messagesFacade_2 = value;
		Il2CppCodeGenWriteBarrier((&____messagesFacade_2), value);
	}

	inline static int32_t get_offset_of__prefabGenerator_3() { return static_cast<int32_t>(offsetof(MessagesListHandlerNew_t4229393496, ____prefabGenerator_3)); }
	inline RuntimeObject* get__prefabGenerator_3() const { return ____prefabGenerator_3; }
	inline RuntimeObject** get_address_of__prefabGenerator_3() { return &____prefabGenerator_3; }
	inline void set__prefabGenerator_3(RuntimeObject* value)
	{
		____prefabGenerator_3 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGESLISTHANDLERNEW_T4229393496_H
#ifndef MESSAGESLISTHANDLER_T630321282_H
#define MESSAGESLISTHANDLER_T630321282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MessagesList.MessagesListHandler
struct  MessagesListHandler_t630321282  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Facades.Messages.MessagesFacade Library.Code.UI.MessagesList.MessagesListHandler::_messagesFacade
	RuntimeObject* ____messagesFacade_2;
	// Library.Code.UI.PrefabGenerator.PrefabGenerator Library.Code.UI.MessagesList.MessagesListHandler::_prefabGenerator
	RuntimeObject* ____prefabGenerator_3;

public:
	inline static int32_t get_offset_of__messagesFacade_2() { return static_cast<int32_t>(offsetof(MessagesListHandler_t630321282, ____messagesFacade_2)); }
	inline RuntimeObject* get__messagesFacade_2() const { return ____messagesFacade_2; }
	inline RuntimeObject** get_address_of__messagesFacade_2() { return &____messagesFacade_2; }
	inline void set__messagesFacade_2(RuntimeObject* value)
	{
		____messagesFacade_2 = value;
		Il2CppCodeGenWriteBarrier((&____messagesFacade_2), value);
	}

	inline static int32_t get_offset_of__prefabGenerator_3() { return static_cast<int32_t>(offsetof(MessagesListHandler_t630321282, ____prefabGenerator_3)); }
	inline RuntimeObject* get__prefabGenerator_3() const { return ____prefabGenerator_3; }
	inline RuntimeObject** get_address_of__prefabGenerator_3() { return &____prefabGenerator_3; }
	inline void set__prefabGenerator_3(RuntimeObject* value)
	{
		____prefabGenerator_3 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGESLISTHANDLER_T630321282_H
#ifndef MESSAGELISTHANDLERSAVEDREMARKS_T3524870477_H
#define MESSAGELISTHANDLERSAVEDREMARKS_T3524870477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MessagesList.New.MessageListHandlerSavedRemarks
struct  MessageListHandlerSavedRemarks_t3524870477  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Facades.Messages.MessagesFacade Library.Code.UI.MessagesList.New.MessageListHandlerSavedRemarks::_messagesFacade
	RuntimeObject* ____messagesFacade_2;
	// Library.Code.UI.PrefabGenerator.PrefabGenerator Library.Code.UI.MessagesList.New.MessageListHandlerSavedRemarks::_prefabGenerator
	RuntimeObject* ____prefabGenerator_3;
	// UnityEngine.UI.Button Library.Code.UI.MessagesList.New.MessageListHandlerSavedRemarks::sendAllButton
	Button_t2872111280 * ___sendAllButton_4;

public:
	inline static int32_t get_offset_of__messagesFacade_2() { return static_cast<int32_t>(offsetof(MessageListHandlerSavedRemarks_t3524870477, ____messagesFacade_2)); }
	inline RuntimeObject* get__messagesFacade_2() const { return ____messagesFacade_2; }
	inline RuntimeObject** get_address_of__messagesFacade_2() { return &____messagesFacade_2; }
	inline void set__messagesFacade_2(RuntimeObject* value)
	{
		____messagesFacade_2 = value;
		Il2CppCodeGenWriteBarrier((&____messagesFacade_2), value);
	}

	inline static int32_t get_offset_of__prefabGenerator_3() { return static_cast<int32_t>(offsetof(MessageListHandlerSavedRemarks_t3524870477, ____prefabGenerator_3)); }
	inline RuntimeObject* get__prefabGenerator_3() const { return ____prefabGenerator_3; }
	inline RuntimeObject** get_address_of__prefabGenerator_3() { return &____prefabGenerator_3; }
	inline void set__prefabGenerator_3(RuntimeObject* value)
	{
		____prefabGenerator_3 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_3), value);
	}

	inline static int32_t get_offset_of_sendAllButton_4() { return static_cast<int32_t>(offsetof(MessageListHandlerSavedRemarks_t3524870477, ___sendAllButton_4)); }
	inline Button_t2872111280 * get_sendAllButton_4() const { return ___sendAllButton_4; }
	inline Button_t2872111280 ** get_address_of_sendAllButton_4() { return &___sendAllButton_4; }
	inline void set_sendAllButton_4(Button_t2872111280 * value)
	{
		___sendAllButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___sendAllButton_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGELISTHANDLERSAVEDREMARKS_T3524870477_H
#ifndef SENDMESSAGEBUTTONHANDLERIMP_T1343354843_H
#define SENDMESSAGEBUTTONHANDLERIMP_T1343354843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MessagesList.SendMessageButtonHandlerImp
struct  SendMessageButtonHandlerImp_t1343354843  : public SendMessageButtonHandler_t618142915
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDMESSAGEBUTTONHANDLERIMP_T1343354843_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (SendFileApiImpl_t545224088), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2301[3] = 
{
	SendFileApiImpl_t545224088::get_offset_of_client_0(),
	SendFileApiImpl_t545224088::get_offset_of_url_1(),
	SendFileApiImpl_t545224088::get_offset_of__dataNetworkInfoProvider_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (U3CsendFilesU3Ec__AnonStorey0_t1170884102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2302[3] = 
{
	U3CsendFilesU3Ec__AnonStorey0_t1170884102::get_offset_of_uri_0(),
	U3CsendFilesU3Ec__AnonStorey0_t1170884102::get_offset_of_callback_1(),
	U3CsendFilesU3Ec__AnonStorey0_t1170884102::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (GroupingModelApiImp_t1621005666), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2304[3] = 
{
	GroupingModelApiImp_t1621005666::get_offset_of__jsonParser_0(),
	GroupingModelApiImp_t1621005666::get_offset_of_url_1(),
	GroupingModelApiImp_t1621005666::get_offset_of__builder_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (U3CloadModelGroupingDataU3Ec__Iterator0_t580313732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2305[6] = 
{
	U3CloadModelGroupingDataU3Ec__Iterator0_t580313732::get_offset_of_U3CrequestU3E__0_0(),
	U3CloadModelGroupingDataU3Ec__Iterator0_t580313732::get_offset_of_reposneWrapper_1(),
	U3CloadModelGroupingDataU3Ec__Iterator0_t580313732::get_offset_of_U24this_2(),
	U3CloadModelGroupingDataU3Ec__Iterator0_t580313732::get_offset_of_U24current_3(),
	U3CloadModelGroupingDataU3Ec__Iterator0_t580313732::get_offset_of_U24disposing_4(),
	U3CloadModelGroupingDataU3Ec__Iterator0_t580313732::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (U3CloadModelGroupingDataU3Ec__AnonStorey1_t3430150173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2306[2] = 
{
	U3CloadModelGroupingDataU3Ec__AnonStorey1_t3430150173::get_offset_of_list_0(),
	U3CloadModelGroupingDataU3Ec__AnonStorey1_t3430150173::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (GroupCreatorApiHoloImp_t1244970665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2308[3] = 
{
	GroupCreatorApiHoloImp_t1244970665::get_offset_of__parser_0(),
	GroupCreatorApiHoloImp_t1244970665::get_offset_of_ulr_1(),
	GroupCreatorApiHoloImp_t1244970665::get_offset_of__builder_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (U3CcreateGroupU3Ec__Iterator0_t3134932817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2309[8] = 
{
	U3CcreateGroupU3Ec__Iterator0_t3134932817::get_offset_of_create_0(),
	U3CcreateGroupU3Ec__Iterator0_t3134932817::get_offset_of_U3CjsonU3E__0_1(),
	U3CcreateGroupU3Ec__Iterator0_t3134932817::get_offset_of_U3CrequestU3E__0_2(),
	U3CcreateGroupU3Ec__Iterator0_t3134932817::get_offset_of_callback_3(),
	U3CcreateGroupU3Ec__Iterator0_t3134932817::get_offset_of_U24this_4(),
	U3CcreateGroupU3Ec__Iterator0_t3134932817::get_offset_of_U24current_5(),
	U3CcreateGroupU3Ec__Iterator0_t3134932817::get_offset_of_U24disposing_6(),
	U3CcreateGroupU3Ec__Iterator0_t3134932817::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (GroupCreatorApiImp_t3941306685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2310[3] = 
{
	GroupCreatorApiImp_t3941306685::get_offset_of__parser_0(),
	GroupCreatorApiImp_t3941306685::get_offset_of_ulr_1(),
	GroupCreatorApiImp_t3941306685::get_offset_of__builder_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (U3CcreateGroupU3Ec__Iterator0_t3097045949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2311[8] = 
{
	U3CcreateGroupU3Ec__Iterator0_t3097045949::get_offset_of_create_0(),
	U3CcreateGroupU3Ec__Iterator0_t3097045949::get_offset_of_U3CjsonU3E__0_1(),
	U3CcreateGroupU3Ec__Iterator0_t3097045949::get_offset_of_U3CrequestU3E__0_2(),
	U3CcreateGroupU3Ec__Iterator0_t3097045949::get_offset_of_callback_3(),
	U3CcreateGroupU3Ec__Iterator0_t3097045949::get_offset_of_U24this_4(),
	U3CcreateGroupU3Ec__Iterator0_t3097045949::get_offset_of_U24current_5(),
	U3CcreateGroupU3Ec__Iterator0_t3097045949::get_offset_of_U24disposing_6(),
	U3CcreateGroupU3Ec__Iterator0_t3097045949::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (SendBatchMessageApiImp_t1721429217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2313[3] = 
{
	SendBatchMessageApiImp_t1721429217::get_offset_of__parser_0(),
	SendBatchMessageApiImp_t1721429217::get_offset_of_ulr_1(),
	SendBatchMessageApiImp_t1721429217::get_offset_of__builder_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (U3CsendBatchMessagesU3Ec__Iterator0_t575326176), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2314[9] = 
{
	U3CsendBatchMessagesU3Ec__Iterator0_t575326176::get_offset_of_messages_0(),
	U3CsendBatchMessagesU3Ec__Iterator0_t575326176::get_offset_of_U3CbatchU3E__0_1(),
	U3CsendBatchMessagesU3Ec__Iterator0_t575326176::get_offset_of_U3CjsonU3E__0_2(),
	U3CsendBatchMessagesU3Ec__Iterator0_t575326176::get_offset_of_U3CrequestU3E__0_3(),
	U3CsendBatchMessagesU3Ec__Iterator0_t575326176::get_offset_of_callback_4(),
	U3CsendBatchMessagesU3Ec__Iterator0_t575326176::get_offset_of_U24this_5(),
	U3CsendBatchMessagesU3Ec__Iterator0_t575326176::get_offset_of_U24current_6(),
	U3CsendBatchMessagesU3Ec__Iterator0_t575326176::get_offset_of_U24disposing_7(),
	U3CsendBatchMessagesU3Ec__Iterator0_t575326176::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (MessageTemplateApiImp_t700868765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2315[3] = 
{
	MessageTemplateApiImp_t700868765::get_offset_of__parser_0(),
	MessageTemplateApiImp_t700868765::get_offset_of_ulr_1(),
	MessageTemplateApiImp_t700868765::get_offset_of__builder_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (U3CloadMessagesTemplatesU3Ec__Iterator0_t130507051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2316[6] = 
{
	U3CloadMessagesTemplatesU3Ec__Iterator0_t130507051::get_offset_of_U3CrequestU3E__0_0(),
	U3CloadMessagesTemplatesU3Ec__Iterator0_t130507051::get_offset_of_reposneWrapper_1(),
	U3CloadMessagesTemplatesU3Ec__Iterator0_t130507051::get_offset_of_U24this_2(),
	U3CloadMessagesTemplatesU3Ec__Iterator0_t130507051::get_offset_of_U24current_3(),
	U3CloadMessagesTemplatesU3Ec__Iterator0_t130507051::get_offset_of_U24disposing_4(),
	U3CloadMessagesTemplatesU3Ec__Iterator0_t130507051::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (ModelApiImp_t398605299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2319[3] = 
{
	ModelApiImp_t398605299::get_offset_of__parser_0(),
	ModelApiImp_t398605299::get_offset_of_url_1(),
	ModelApiImp_t398605299::get_offset_of__builder_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (U3CloadModelsU3Ec__Iterator0_t3694394883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2320[6] = 
{
	U3CloadModelsU3Ec__Iterator0_t3694394883::get_offset_of_U3CrequestU3E__0_0(),
	U3CloadModelsU3Ec__Iterator0_t3694394883::get_offset_of_reposneWrapper_1(),
	U3CloadModelsU3Ec__Iterator0_t3694394883::get_offset_of_U24this_2(),
	U3CloadModelsU3Ec__Iterator0_t3694394883::get_offset_of_U24current_3(),
	U3CloadModelsU3Ec__Iterator0_t3694394883::get_offset_of_U24disposing_4(),
	U3CloadModelsU3Ec__Iterator0_t3694394883::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2327[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (ChecklistMobileUINew_t1060043268), -1, sizeof(ChecklistMobileUINew_t1060043268_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2328[11] = 
{
	ChecklistMobileUINew_t1060043268::get_offset_of_POPUP_TITLE_2(),
	ChecklistMobileUINew_t1060043268::get_offset_of__checkListFacade_3(),
	ChecklistMobileUINew_t1060043268::get_offset_of__prefabGenerator_4(),
	ChecklistMobileUINew_t1060043268::get_offset_of_started_5(),
	ChecklistMobileUINew_t1060043268::get_offset_of_approveButton_6(),
	ChecklistMobileUINew_t1060043268::get_offset_of__elementApproveStateFacade_7(),
	ChecklistMobileUINew_t1060043268::get_offset_of__popUpShower_8(),
	ChecklistMobileUINew_t1060043268::get_offset_of_dropdown_9(),
	ChecklistMobileUINew_t1060043268::get_offset_of_popUp_10(),
	ChecklistMobileUINew_t1060043268::get_offset_of_objectNumber_11(),
	ChecklistMobileUINew_t1060043268_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (U3CActionOnCheckChangeU3Ec__AnonStorey0_t4060093481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2329[2] = 
{
	U3CActionOnCheckChangeU3Ec__AnonStorey0_t4060093481::get_offset_of_itemStatus_0(),
	U3CActionOnCheckChangeU3Ec__AnonStorey0_t4060093481::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (U3CActionOnCheckChangeU3Ec__AnonStorey1_t1708225949), -1, sizeof(U3CActionOnCheckChangeU3Ec__AnonStorey1_t1708225949_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2330[4] = 
{
	U3CActionOnCheckChangeU3Ec__AnonStorey1_t1708225949::get_offset_of_status_0(),
	U3CActionOnCheckChangeU3Ec__AnonStorey1_t1708225949::get_offset_of_iitemStatus_1(),
	U3CActionOnCheckChangeU3Ec__AnonStorey1_t1708225949::get_offset_of_U3CU3Ef__refU240_2(),
	U3CActionOnCheckChangeU3Ec__AnonStorey1_t1708225949_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (U3COnEventU3Ec__AnonStorey2_t649407929), -1, sizeof(U3COnEventU3Ec__AnonStorey2_t649407929_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2331[4] = 
{
	U3COnEventU3Ec__AnonStorey2_t649407929::get_offset_of_itemStatus_0(),
	U3COnEventU3Ec__AnonStorey2_t649407929::get_offset_of_U24this_1(),
	U3COnEventU3Ec__AnonStorey2_t649407929_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
	U3COnEventU3Ec__AnonStorey2_t649407929_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (CheckListRow_t2468600002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2332[8] = 
{
	CheckListRow_t2468600002::get_offset_of_checkboxAccept_2(),
	CheckListRow_t2468600002::get_offset_of_checkboxUntouched_3(),
	CheckListRow_t2468600002::get_offset_of_checkboxRejected_4(),
	CheckListRow_t2468600002::get_offset_of_text_5(),
	CheckListRow_t2468600002::get_offset_of_isAwake_6(),
	CheckListRow_t2468600002::get_offset_of_itemStatus_7(),
	CheckListRow_t2468600002::get_offset_of_actionOnCheckChange_8(),
	CheckListRow_t2468600002::get_offset_of_itemStatusWithCheckpoint_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (CheckListRowNew_t383576096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2333[7] = 
{
	CheckListRowNew_t383576096::get_offset_of_checkboxAccept_2(),
	CheckListRowNew_t383576096::get_offset_of_checkboxRejected_3(),
	CheckListRowNew_t383576096::get_offset_of_text_4(),
	CheckListRowNew_t383576096::get_offset_of_isAwake_5(),
	CheckListRowNew_t383576096::get_offset_of_itemStatus_6(),
	CheckListRowNew_t383576096::get_offset_of__itemStatusWithCheckpoint_7(),
	CheckListRowNew_t383576096::get_offset_of_actionOnCheckChange_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (ChecklistUI_t3492366448), -1, sizeof(ChecklistUI_t3492366448_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2334[15] = 
{
	ChecklistUI_t3492366448::get_offset_of_POPUP_TITLE_2(),
	ChecklistUI_t3492366448::get_offset_of__checkListFacade_3(),
	ChecklistUI_t3492366448::get_offset_of__checkPointsFacade_4(),
	ChecklistUI_t3492366448::get_offset_of__prefabGenerator_5(),
	ChecklistUI_t3492366448::get_offset_of_started_6(),
	ChecklistUI_t3492366448::get_offset_of_approveButton_7(),
	ChecklistUI_t3492366448::get_offset_of__elementApproveStateFacade_8(),
	ChecklistUI_t3492366448::get_offset_of__popUpShower_9(),
	ChecklistUI_t3492366448::get_offset_of_popUp_10(),
	ChecklistUI_t3492366448::get_offset_of_objectNumber_11(),
	ChecklistUI_t3492366448::get_offset_of_isCheckpointsLoaded_12(),
	ChecklistUI_t3492366448::get_offset_of_isChecklistLoaded_13(),
	ChecklistUI_t3492366448::get_offset_of__loadedList_14(),
	ChecklistUI_t3492366448::get_offset_of__checkPointInfosLoaded_15(),
	ChecklistUI_t3492366448_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (U3COnEventU3Ec__AnonStorey0_t2580920443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2335[2] = 
{
	U3COnEventU3Ec__AnonStorey0_t2580920443::get_offset_of_itemStatus_0(),
	U3COnEventU3Ec__AnonStorey0_t2580920443::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (CurrentItemStatusUI_t1638528844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2337[6] = 
{
	CurrentItemStatusUI_t1638528844::get_offset_of__prefabGenerator_2(),
	CurrentItemStatusUI_t1638528844::get_offset_of__currentCheckListItemFacade_3(),
	CurrentItemStatusUI_t1638528844::get_offset_of__checkListFacade_4(),
	CurrentItemStatusUI_t1638528844::get_offset_of__currentItemStatus_5(),
	CurrentItemStatusUI_t1638528844::get_offset_of__itemStatusRows_6(),
	CurrentItemStatusUI_t1638528844::get_offset_of_started_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (ItemStatusRow_t2790536877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2338[2] = 
{
	ItemStatusRow_t2790536877::get_offset_of_button_2(),
	ItemStatusRow_t2790536877::get_offset_of_itemStatus_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (U3CsetItemStatusU3Ec__AnonStorey0_t1847096850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2339[3] = 
{
	U3CsetItemStatusU3Ec__AnonStorey0_t1847096850::get_offset_of_itemStatusDto_0(),
	U3CsetItemStatusU3Ec__AnonStorey0_t1847096850::get_offset_of_actionOnClick_1(),
	U3CsetItemStatusU3Ec__AnonStorey0_t1847096850::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (ElementRowHandler_t1682113530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2340[3] = 
{
	ElementRowHandler_t1682113530::get_offset_of__elementRowHandlerDelegate_2(),
	ElementRowHandler_t1682113530::get_offset_of__element_3(),
	ElementRowHandler_t1682113530::get_offset_of_text_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (ElementRowHandlerNew_t1658168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2341[3] = 
{
	ElementRowHandlerNew_t1658168::get_offset_of__elementRowHandlerDelegate_2(),
	ElementRowHandlerNew_t1658168::get_offset_of__element_3(),
	ElementRowHandlerNew_t1658168::get_offset_of_text_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (ElementsObserver_t3382904053), -1, sizeof(ElementsObserver_t3382904053_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2344[12] = 
{
	ElementsObserver_t3382904053::get_offset_of__elementHandlerFacade_2(),
	ElementsObserver_t3382904053::get_offset_of__prefabGenerator_3(),
	ElementsObserver_t3382904053::get_offset_of__elementFacade_4(),
	ElementsObserver_t3382904053::get_offset_of__elements_5(),
	ElementsObserver_t3382904053::get_offset_of_backButton_6(),
	ElementsObserver_t3382904053::get_offset_of__showerPopUps_7(),
	ElementsObserver_t3382904053::get_offset_of_element3DFacade_8(),
	ElementsObserver_t3382904053::get_offset_of__modelInterface_9(),
	ElementsObserver_t3382904053::get_offset_of_lasteElementLoaded_10(),
	ElementsObserver_t3382904053::get_offset_of_loadingPopUp_11(),
	ElementsObserver_t3382904053_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
	ElementsObserver_t3382904053_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (U3CaddElementsU3Ec__AnonStorey0_t2043651465), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2345[2] = 
{
	U3CaddElementsU3Ec__AnonStorey0_t2043651465::get_offset_of_contentTransform_0(),
	U3CaddElementsU3Ec__AnonStorey0_t2043651465::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (ElementsObserverNew_t2308744357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2346[10] = 
{
	ElementsObserverNew_t2308744357::get_offset_of__elementHandlerFacade_2(),
	ElementsObserverNew_t2308744357::get_offset_of__prefabGenerator_3(),
	ElementsObserverNew_t2308744357::get_offset_of__elementFacade_4(),
	ElementsObserverNew_t2308744357::get_offset_of__elements_5(),
	ElementsObserverNew_t2308744357::get_offset_of_backButton_6(),
	ElementsObserverNew_t2308744357::get_offset_of__showerPopUps_7(),
	ElementsObserverNew_t2308744357::get_offset_of_element3DFacade_8(),
	ElementsObserverNew_t2308744357::get_offset_of__modelInterface_9(),
	ElementsObserverNew_t2308744357::get_offset_of_lasteElementLoaded_10(),
	ElementsObserverNew_t2308744357::get_offset_of_loadingPopUp_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (U3CaddElementsU3Ec__AnonStorey0_t2713806289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2347[2] = 
{
	U3CaddElementsU3Ec__AnonStorey0_t2713806289::get_offset_of_contentTransform_0(),
	U3CaddElementsU3Ec__AnonStorey0_t2713806289::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (ModelElementObserver_t2747689887), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2348[8] = 
{
	ModelElementObserver_t2747689887::get_offset_of__modelFacade_2(),
	ModelElementObserver_t2747689887::get_offset_of__prefabGenerator_3(),
	ModelElementObserver_t2747689887::get_offset_of__elementFacade_4(),
	ModelElementObserver_t2747689887::get_offset_of__element3dFacade_5(),
	ModelElementObserver_t2747689887::get_offset_of__models_6(),
	ModelElementObserver_t2747689887::get_offset_of__elements_7(),
	ModelElementObserver_t2747689887::get_offset_of_modelIsChoosen_8(),
	ModelElementObserver_t2747689887::get_offset_of_started_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (U3CupdateObserverU3Ec__AnonStorey0_t2240497878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2349[2] = 
{
	U3CupdateObserverU3Ec__AnonStorey0_t2240497878::get_offset_of_contentTransform_0(),
	U3CupdateObserverU3Ec__AnonStorey0_t2240497878::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (U3CupdateObserverU3Ec__AnonStorey1_t2240497877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2350[2] = 
{
	U3CupdateObserverU3Ec__AnonStorey1_t2240497877::get_offset_of_contentTransform_0(),
	U3CupdateObserverU3Ec__AnonStorey1_t2240497877::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (ModelObserver_t1172265923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2351[5] = 
{
	ModelObserver_t1172265923::get_offset_of__modelFacade_2(),
	ModelObserver_t1172265923::get_offset_of__prefabGenerator_3(),
	ModelObserver_t1172265923::get_offset_of__models_4(),
	ModelObserver_t1172265923::get_offset_of__elementFacade_5(),
	ModelObserver_t1172265923::get_offset_of__elementsInterface_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (U3CaddModelsU3Ec__AnonStorey0_t3035878898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2352[2] = 
{
	U3CaddModelsU3Ec__AnonStorey0_t3035878898::get_offset_of_contentTransform_0(),
	U3CaddModelsU3Ec__AnonStorey0_t3035878898::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (ModelObserverNew_t1299566747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2353[6] = 
{
	ModelObserverNew_t1299566747::get_offset_of__modelFacade_2(),
	ModelObserverNew_t1299566747::get_offset_of__prefabGenerator_3(),
	ModelObserverNew_t1299566747::get_offset_of__models_4(),
	ModelObserverNew_t1299566747::get_offset_of__elementFacade_5(),
	ModelObserverNew_t1299566747::get_offset_of__elementsObserver_6(),
	ModelObserverNew_t1299566747::get_offset_of__elementsInterface_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (U3CaddModelsU3Ec__AnonStorey0_t2567043122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2354[2] = 
{
	U3CaddModelsU3Ec__AnonStorey0_t2567043122::get_offset_of_contentTransform_0(),
	U3CaddModelsU3Ec__AnonStorey0_t2567043122::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (ModelRowHandler_t2220072347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2355[4] = 
{
	ModelRowHandler_t2220072347::get_offset_of__ModelRowHandlerDelegate_2(),
	ModelRowHandler_t2220072347::get_offset_of__model_3(),
	ModelRowHandler_t2220072347::get_offset_of__element_4(),
	ModelRowHandler_t2220072347::get_offset_of_text_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (ModelRowHandlerNew_t1567160379), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2356[4] = 
{
	ModelRowHandlerNew_t1567160379::get_offset_of__ModelRowHandlerDelegate_2(),
	ModelRowHandlerNew_t1567160379::get_offset_of__model_3(),
	ModelRowHandlerNew_t1567160379::get_offset_of__element_4(),
	ModelRowHandlerNew_t1567160379::get_offset_of_text_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (ApproveElementRow_t3948802595), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2359[2] = 
{
	ApproveElementRow_t3948802595::get_offset_of_button_2(),
	ApproveElementRow_t3948802595::get_offset_of_itemStatus_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (U3CaddListenerToButtonU3Ec__AnonStorey0_t962888137), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2360[2] = 
{
	U3CaddListenerToButtonU3Ec__AnonStorey0_t962888137::get_offset_of_action_0(),
	U3CaddListenerToButtonU3Ec__AnonStorey0_t962888137::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (ElementNewStatusChoos_t3073096760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2361[6] = 
{
	ElementNewStatusChoos_t3073096760::get_offset_of__approveElements_2(),
	ElementNewStatusChoos_t3073096760::get_offset_of__prefabGenerator_3(),
	ElementNewStatusChoos_t3073096760::get_offset_of__approveElementResponse_4(),
	ElementNewStatusChoos_t3073096760::get_offset_of_okButton_5(),
	ElementNewStatusChoos_t3073096760::get_offset_of_choosenState_6(),
	ElementNewStatusChoos_t3073096760::get_offset_of_cancel_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (U3CaddRowsU3Ec__AnonStorey0_t2717169914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2362[2] = 
{
	U3CaddRowsU3Ec__AnonStorey0_t2717169914::get_offset_of_dto_0(),
	U3CaddRowsU3Ec__AnonStorey0_t2717169914::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (U3CaddRowsU3Ec__AnonStorey1_t2365418681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2363[2] = 
{
	U3CaddRowsU3Ec__AnonStorey1_t2365418681::get_offset_of_it_0(),
	U3CaddRowsU3Ec__AnonStorey1_t2365418681::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (AproveElementDto_t688774064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2364[2] = 
{
	AproveElementDto_t688774064::get_offset_of_check_0(),
	AproveElementDto_t688774064::get_offset_of_element_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (EnglishStrings_t2792403630), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (ColorRowHandler_t2612092377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2366[4] = 
{
	ColorRowHandler_t2612092377::get_offset_of_checkbox_2(),
	ColorRowHandler_t2612092377::get_offset_of_text_3(),
	ColorRowHandler_t2612092377::get_offset_of_color_4(),
	ColorRowHandler_t2612092377::get_offset_of_dto_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (ColorRowHandlerNew_t1831333649), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2367[5] = 
{
	ColorRowHandlerNew_t1831333649::get_offset_of_text_2(),
	ColorRowHandlerNew_t1831333649::get_offset_of_color_3(),
	ColorRowHandlerNew_t1831333649::get_offset_of_dto_4(),
	ColorRowHandlerNew_t1831333649::get_offset_of_onImage_5(),
	ColorRowHandlerNew_t1831333649::get_offset_of_offImage_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (GroupinColor_t2175596517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2368[3] = 
{
	GroupinColor_t2175596517::get_offset_of__modelGroupFacade_2(),
	GroupinColor_t2175596517::get_offset_of__prefabGenerator_3(),
	GroupinColor_t2175596517::get_offset_of_started_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (GroupinColorNew_t4212717807), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2369[5] = 
{
	GroupinColorNew_t4212717807::get_offset_of__modelGroupFacade_2(),
	GroupinColorNew_t4212717807::get_offset_of__prefabGenerator_3(),
	GroupinColorNew_t4212717807::get_offset_of_started_4(),
	GroupinColorNew_t4212717807::get_offset_of_existingItems_5(),
	GroupinColorNew_t4212717807::get_offset_of_topMargin_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (LanguageStrings_t1206686884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2370[47] = 
{
	LanguageStrings_t1206686884::get_offset_of_menuButtonChecklist_0(),
	LanguageStrings_t1206686884::get_offset_of_menuButtonLayers_1(),
	LanguageStrings_t1206686884::get_offset_of_menuButtonAddRemarks_2(),
	LanguageStrings_t1206686884::get_offset_of_menuButtonSavedRemarks_3(),
	LanguageStrings_t1206686884::get_offset_of_menuButtonSelectModelElement_4(),
	LanguageStrings_t1206686884::get_offset_of_checklistAcceptState_5(),
	LanguageStrings_t1206686884::get_offset_of_checklistUntouchedState_6(),
	LanguageStrings_t1206686884::get_offset_of_checklistRejectState_7(),
	LanguageStrings_t1206686884::get_offset_of_checklistApproveButton_8(),
	LanguageStrings_t1206686884::get_offset_of_layersLabel_9(),
	LanguageStrings_t1206686884::get_offset_of_addRemarksLabel_10(),
	LanguageStrings_t1206686884::get_offset_of_addRemarksCancelButton_11(),
	LanguageStrings_t1206686884::get_offset_of_addRemarksSaveButton_12(),
	LanguageStrings_t1206686884::get_offset_of_addRemarksEmptyChecklistButton_13(),
	LanguageStrings_t1206686884::get_offset_of_savedRemarksLabel_14(),
	LanguageStrings_t1206686884::get_offset_of_savedRemarksUploadButton_15(),
	LanguageStrings_t1206686884::get_offset_of_selectMELabel_16(),
	LanguageStrings_t1206686884::get_offset_of_selectMESearchModels_17(),
	LanguageStrings_t1206686884::get_offset_of_selectMESearchElements_18(),
	LanguageStrings_t1206686884::get_offset_of_photoPopupDescription_19(),
	LanguageStrings_t1206686884::get_offset_of_photoPopupPhotoHistory_20(),
	LanguageStrings_t1206686884::get_offset_of_photoPopupDeleteButton_21(),
	LanguageStrings_t1206686884::get_offset_of_photoPopupAddButton_22(),
	LanguageStrings_t1206686884::get_offset_of_modelLoadingTitle_23(),
	LanguageStrings_t1206686884::get_offset_of_modelLoadingLoading_24(),
	LanguageStrings_t1206686884::get_offset_of_modelLoadingSuccess_25(),
	LanguageStrings_t1206686884::get_offset_of_modelLoadingSuccessMessage_26(),
	LanguageStrings_t1206686884::get_offset_of_modelLoadingPartialSuccess_27(),
	LanguageStrings_t1206686884::get_offset_of_modelLoadingPartialSuccessMessage_28(),
	LanguageStrings_t1206686884::get_offset_of_uploadMessagesTitle_29(),
	LanguageStrings_t1206686884::get_offset_of_uploadMessages_30(),
	LanguageStrings_t1206686884::get_offset_of_uploadMessagesFailure_31(),
	LanguageStrings_t1206686884::get_offset_of_approveStateTitle_32(),
	LanguageStrings_t1206686884::get_offset_of_approveStateQuestion_33(),
	LanguageStrings_t1206686884::get_offset_of_approveStateStatusTitle_34(),
	LanguageStrings_t1206686884::get_offset_of_approveStateStatusChanging_35(),
	LanguageStrings_t1206686884::get_offset_of_approveStateStatusChangeFailure_36(),
	LanguageStrings_t1206686884::get_offset_of_approveStateStatusChangeFailureReason_37(),
	LanguageStrings_t1206686884::get_offset_of_approveStateRemarkSaveFailure_38(),
	LanguageStrings_t1206686884::get_offset_of_statusChangeTitle_39(),
	LanguageStrings_t1206686884::get_offset_of_statusChange_40(),
	LanguageStrings_t1206686884::get_offset_of_statusChangeFailure_41(),
	LanguageStrings_t1206686884::get_offset_of_statusChooseLabel_42(),
	LanguageStrings_t1206686884::get_offset_of_statusChooseOKButton_43(),
	LanguageStrings_t1206686884::get_offset_of_statusChooseCancelButton_44(),
	LanguageStrings_t1206686884::get_offset_of_deleteMessageTitle_45(),
	LanguageStrings_t1206686884::get_offset_of_deleteMessageQuestion_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (MessageListBuilder_t1028067940), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (MessageListBuilderSavedRemarks_t2061646460), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (MessageListHandlerSavedRemarks_t3524870477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2373[3] = 
{
	MessageListHandlerSavedRemarks_t3524870477::get_offset_of__messagesFacade_2(),
	MessageListHandlerSavedRemarks_t3524870477::get_offset_of__prefabGenerator_3(),
	MessageListHandlerSavedRemarks_t3524870477::get_offset_of_sendAllButton_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (MessageListRow_t2364007661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2374[10] = 
{
	MessageListRow_t2364007661::get_offset_of__messagesFacade_2(),
	MessageListRow_t2364007661::get_offset_of__button_3(),
	MessageListRow_t2364007661::get_offset_of__recordPanel_4(),
	MessageListRow_t2364007661::get_offset_of__textButton_5(),
	MessageListRow_t2364007661::get_offset_of__text_6(),
	MessageListRow_t2364007661::get_offset_of__rectordText_7(),
	MessageListRow_t2364007661::get_offset_of__message_8(),
	MessageListRow_t2364007661::get_offset_of__photoPopup_9(),
	MessageListRow_t2364007661::get_offset_of__photoPopupCanvasGroup_10(),
	MessageListRow_t2364007661::get_offset_of_started_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (MessagesListHandler_t630321282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2375[2] = 
{
	MessagesListHandler_t630321282::get_offset_of__messagesFacade_2(),
	MessagesListHandler_t630321282::get_offset_of__prefabGenerator_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (MessagesListHandlerNew_t4229393496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2376[2] = 
{
	MessagesListHandlerNew_t4229393496::get_offset_of__messagesFacade_2(),
	MessagesListHandlerNew_t4229393496::get_offset_of__prefabGenerator_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (MessageListRowNew_t2419996211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2377[9] = 
{
	MessageListRowNew_t2419996211::get_offset_of__messagesFacade_2(),
	MessageListRowNew_t2419996211::get_offset_of__button_3(),
	MessageListRowNew_t2419996211::get_offset_of__text_4(),
	MessageListRowNew_t2419996211::get_offset_of_audioText_5(),
	MessageListRowNew_t2419996211::get_offset_of_photoText_6(),
	MessageListRowNew_t2419996211::get_offset_of__message_7(),
	MessageListRowNew_t2419996211::get_offset_of__photoPopup_8(),
	MessageListRowNew_t2419996211::get_offset_of__photoPopupCanvasGroup_9(),
	MessageListRowNew_t2419996211::get_offset_of_started_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (NewMessageListBuilder_t2354248106), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (SendMessageButtonHandler_t618142915), -1, sizeof(SendMessageButtonHandler_t618142915_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2379[7] = 
{
	SendMessageButtonHandler_t618142915::get_offset_of__sendMessageFacade_2(),
	SendMessageButtonHandler_t618142915::get_offset_of__element3dFacade_3(),
	SendMessageButtonHandler_t618142915::get_offset_of_started_4(),
	SendMessageButtonHandler_t618142915::get_offset_of__currentElemntId_5(),
	SendMessageButtonHandler_t618142915::get_offset_of__popUpShower_6(),
	SendMessageButtonHandler_t618142915::get_offset_of_popUpLoading_7(),
	SendMessageButtonHandler_t618142915_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (SendMessageButtonHandlerImp_t1343354843), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (CloseRemark_Button_t3324403479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2381[1] = 
{
	CloseRemark_Button_t3324403479::get_offset_of_popUp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (DoSomething_t3992512957), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (ListViewHandler_t424984033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2384[4] = 
{
	ListViewHandler_t424984033::get_offset_of_textToBeFilledUpWithTemplate_2(),
	ListViewHandler_t424984033::get_offset_of__messageTemplatesFacade_3(),
	ListViewHandler_t424984033::get_offset_of__prefabGenerator_4(),
	ListViewHandler_t424984033::get_offset_of_started_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (OpenRemark_Button_t1024976505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2385[1] = 
{
	OpenRemark_Button_t1024976505::get_offset_of_popUp_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (Remark_t2047647426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2386[5] = 
{
	Remark_t2047647426::get_offset_of_button_2(),
	Remark_t2047647426::get_offset_of_remarkWindow_3(),
	Remark_t2047647426::get_offset_of_save_4(),
	Remark_t2047647426::get_offset_of_close_5(),
	Remark_t2047647426::get_offset_of_anchor_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (U3CAnchorActivateU3Ec__Iterator0_t1705974996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2387[4] = 
{
	U3CAnchorActivateU3Ec__Iterator0_t1705974996::get_offset_of_U24this_0(),
	U3CAnchorActivateU3Ec__Iterator0_t1705974996::get_offset_of_U24current_1(),
	U3CAnchorActivateU3Ec__Iterator0_t1705974996::get_offset_of_U24disposing_2(),
	U3CAnchorActivateU3Ec__Iterator0_t1705974996::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (TemplateRowHandler_t1160348300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2388[3] = 
{
	TemplateRowHandler_t1160348300::get_offset_of__messageTemplate_2(),
	TemplateRowHandler_t1160348300::get_offset_of__FillUpWithMessageTemplate_3(),
	TemplateRowHandler_t1160348300::get_offset_of_text_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (MoveElement_t2302326803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2389[15] = 
{
	MoveElement_t2302326803::get_offset_of_index_2(),
	MoveElement_t2302326803::get_offset_of_move_3(),
	MoveElement_t2302326803::get_offset_of_speed_4(),
	MoveElement_t2302326803::get_offset_of_tempX_5(),
	MoveElement_t2302326803::get_offset_of_isloaded_6(),
	MoveElement_t2302326803::get_offset_of_rotX_7(),
	MoveElement_t2302326803::get_offset_of_gObject_8(),
	MoveElement_t2302326803::get_offset_of_tiltAngle_9(),
	MoveElement_t2302326803::get_offset_of_selectedCamera_10(),
	MoveElement_t2302326803::get_offset_of_perspectiveZoomSpeed_11(),
	MoveElement_t2302326803::get_offset_of_orthoZoomSpeed_12(),
	MoveElement_t2302326803::get_offset_of_dist_13(),
	MoveElement_t2302326803::get_offset_of_dragging_14(),
	MoveElement_t2302326803::get_offset_of_toDrag_15(),
	MoveElement_t2302326803::get_offset_of_offset_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (PhotoHistory_t4222810824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2390[5] = 
{
	PhotoHistory_t4222810824::get_offset_of_thumbnail_2(),
	PhotoHistory_t4222810824::get_offset_of_date_3(),
	PhotoHistory_t4222810824::get_offset_of_filename_4(),
	PhotoHistory_t4222810824::get_offset_of_rectTransform_5(),
	PhotoHistory_t4222810824::get_offset_of_onClick_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (PhotoPopup_t4101423206), -1, sizeof(PhotoPopup_t4101423206_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2391[14] = 
{
	PhotoPopup_t4101423206::get_offset_of_closeButton_2(),
	PhotoPopup_t4101423206::get_offset_of_photoImage_3(),
	PhotoPopup_t4101423206::get_offset_of_imageDateTime_4(),
	PhotoPopup_t4101423206::get_offset_of_mainFileName_5(),
	PhotoPopup_t4101423206::get_offset_of_descriptionText_6(),
	PhotoPopup_t4101423206::get_offset_of_scrollContents_7(),
	PhotoPopup_t4101423206::get_offset_of_canvasGroup_8(),
	PhotoPopup_t4101423206::get_offset_of_deleteButton_9(),
	PhotoPopup_t4101423206::get_offset_of__messagesFacade_10(),
	PhotoPopup_t4101423206::get_offset_of_currentMessage_11(),
	PhotoPopup_t4101423206::get_offset_of__popUpShower_12(),
	PhotoPopup_t4101423206::get_offset_of_prefabGenerator_13(),
	PhotoPopup_t4101423206::get_offset_of_existingPhotoHistory_14(),
	PhotoPopup_t4101423206_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (AcceptPopUp_t454786204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2392[4] = 
{
	AcceptPopUp_t454786204::get_offset_of_okButton_2(),
	AcceptPopUp_t454786204::get_offset_of_cancelButton_3(),
	AcceptPopUp_t454786204::get_offset_of_titleText_4(),
	AcceptPopUp_t454786204::get_offset_of_infoText_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (U3ChandlePopUpU3Ec__AnonStorey0_t2649938024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2393[1] = 
{
	U3ChandlePopUpU3Ec__AnonStorey0_t2649938024::get_offset_of_popUpStuff_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (LoadingPopUp_t194048676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2394[2] = 
{
	LoadingPopUp_t194048676::get_offset_of_titleText_2(),
	LoadingPopUp_t194048676::get_offset_of_infoText_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (PopUpHandler_t3865200444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2395[2] = 
{
	PopUpHandler_t3865200444::get_offset_of_backgroundTransform_0(),
	PopUpHandler_t3865200444::get_offset_of_gameObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (PopUps_t1014109545)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2396[4] = 
{
	PopUps_t1014109545::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (PopUpScript_t4024301849), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2397[1] = 
{
	PopUpScript_t4024301849::get_offset_of_popsUpsFactory_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (SandboxPopUpFactory_t63848479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2399[2] = 
{
	SandboxPopUpFactory_t63848479::get_offset_of__gameObject_0(),
	SandboxPopUpFactory_t63848479::get_offset_of_popUpHandlers_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
