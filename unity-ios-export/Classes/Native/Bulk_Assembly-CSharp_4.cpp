﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UI.GroupinColorMobileImp
struct GroupinColorMobileImp_t1767066759;
// Library.UI.GroupinColorModel.GroupinColor
struct GroupinColor_t2175596517;
// UnityEngine.Transform
struct Transform_t3275118058;
// Library.Code.Domain.Dtos.ModelGroupDto
struct ModelGroupDto_t1808254547;
// UI.GroupinColorMobileImpNew
struct GroupinColorMobileImpNew_t3384506007;
// Library.UI.GroupinColorModel.GroupinColorNew
struct GroupinColorNew_t4212717807;
// UI.MessageListBuilderImp
struct MessageListBuilderImp_t1204586794;
// Library.Code.UI.MessagesList.MessageListBuilder
struct MessageListBuilder_t1028067940;
// System.Void
struct Void_t1841601450;
// System.String
struct String_t;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// Library.Code.Facades.ModelGrouping.ModelGroupFacade
struct ModelGroupFacade_t1233541942;
// Library.Code.UI.PrefabGenerator.PrefabGenerator
struct PrefabGenerator_t3099279183;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t2719087314;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef MESSAGELISTBUILDER_T1028067940_H
#define MESSAGELISTBUILDER_T1028067940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.UI.MessagesList.MessageListBuilder
struct  MessageListBuilder_t1028067940  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGELISTBUILDER_T1028067940_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef MESSAGELISTBUILDERIMP_T1204586794_H
#define MESSAGELISTBUILDERIMP_T1204586794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI.MessageListBuilderImp
struct  MessageListBuilderImp_t1204586794  : public MessageListBuilder_t1028067940
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGELISTBUILDERIMP_T1204586794_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MODELGROUPDTO_T1808254547_H
#define MODELGROUPDTO_T1808254547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.Code.Domain.Dtos.ModelGroupDto
struct  ModelGroupDto_t1808254547  : public RuntimeObject
{
public:
	// System.Int32 Library.Code.Domain.Dtos.ModelGroupDto::size
	int32_t ___size_0;
	// UnityEngine.Color Library.Code.Domain.Dtos.ModelGroupDto::_color
	Color_t2020392075  ____color_1;
	// System.String Library.Code.Domain.Dtos.ModelGroupDto::_name
	String_t* ____name_2;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Library.Code.Domain.Dtos.ModelGroupDto::_objects
	List_1_t1125654279 * ____objects_3;
	// System.Boolean Library.Code.Domain.Dtos.ModelGroupDto::visible
	bool ___visible_4;

public:
	inline static int32_t get_offset_of_size_0() { return static_cast<int32_t>(offsetof(ModelGroupDto_t1808254547, ___size_0)); }
	inline int32_t get_size_0() const { return ___size_0; }
	inline int32_t* get_address_of_size_0() { return &___size_0; }
	inline void set_size_0(int32_t value)
	{
		___size_0 = value;
	}

	inline static int32_t get_offset_of__color_1() { return static_cast<int32_t>(offsetof(ModelGroupDto_t1808254547, ____color_1)); }
	inline Color_t2020392075  get__color_1() const { return ____color_1; }
	inline Color_t2020392075 * get_address_of__color_1() { return &____color_1; }
	inline void set__color_1(Color_t2020392075  value)
	{
		____color_1 = value;
	}

	inline static int32_t get_offset_of__name_2() { return static_cast<int32_t>(offsetof(ModelGroupDto_t1808254547, ____name_2)); }
	inline String_t* get__name_2() const { return ____name_2; }
	inline String_t** get_address_of__name_2() { return &____name_2; }
	inline void set__name_2(String_t* value)
	{
		____name_2 = value;
		Il2CppCodeGenWriteBarrier((&____name_2), value);
	}

	inline static int32_t get_offset_of__objects_3() { return static_cast<int32_t>(offsetof(ModelGroupDto_t1808254547, ____objects_3)); }
	inline List_1_t1125654279 * get__objects_3() const { return ____objects_3; }
	inline List_1_t1125654279 ** get_address_of__objects_3() { return &____objects_3; }
	inline void set__objects_3(List_1_t1125654279 * value)
	{
		____objects_3 = value;
		Il2CppCodeGenWriteBarrier((&____objects_3), value);
	}

	inline static int32_t get_offset_of_visible_4() { return static_cast<int32_t>(offsetof(ModelGroupDto_t1808254547, ___visible_4)); }
	inline bool get_visible_4() const { return ___visible_4; }
	inline bool* get_address_of_visible_4() { return &___visible_4; }
	inline void set_visible_4(bool value)
	{
		___visible_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODELGROUPDTO_T1808254547_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef TRANSFORM_T3275118058_H
#define TRANSFORM_T3275118058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3275118058  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3275118058_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef GROUPINCOLORNEW_T4212717807_H
#define GROUPINCOLORNEW_T4212717807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.UI.GroupinColorModel.GroupinColorNew
struct  GroupinColorNew_t4212717807  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Facades.ModelGrouping.ModelGroupFacade Library.UI.GroupinColorModel.GroupinColorNew::_modelGroupFacade
	RuntimeObject* ____modelGroupFacade_2;
	// Library.Code.UI.PrefabGenerator.PrefabGenerator Library.UI.GroupinColorModel.GroupinColorNew::_prefabGenerator
	RuntimeObject* ____prefabGenerator_3;
	// System.Boolean Library.UI.GroupinColorModel.GroupinColorNew::started
	bool ___started_4;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> Library.UI.GroupinColorModel.GroupinColorNew::existingItems
	List_1_t2719087314 * ___existingItems_5;
	// System.Single Library.UI.GroupinColorModel.GroupinColorNew::topMargin
	float ___topMargin_6;

public:
	inline static int32_t get_offset_of__modelGroupFacade_2() { return static_cast<int32_t>(offsetof(GroupinColorNew_t4212717807, ____modelGroupFacade_2)); }
	inline RuntimeObject* get__modelGroupFacade_2() const { return ____modelGroupFacade_2; }
	inline RuntimeObject** get_address_of__modelGroupFacade_2() { return &____modelGroupFacade_2; }
	inline void set__modelGroupFacade_2(RuntimeObject* value)
	{
		____modelGroupFacade_2 = value;
		Il2CppCodeGenWriteBarrier((&____modelGroupFacade_2), value);
	}

	inline static int32_t get_offset_of__prefabGenerator_3() { return static_cast<int32_t>(offsetof(GroupinColorNew_t4212717807, ____prefabGenerator_3)); }
	inline RuntimeObject* get__prefabGenerator_3() const { return ____prefabGenerator_3; }
	inline RuntimeObject** get_address_of__prefabGenerator_3() { return &____prefabGenerator_3; }
	inline void set__prefabGenerator_3(RuntimeObject* value)
	{
		____prefabGenerator_3 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_3), value);
	}

	inline static int32_t get_offset_of_started_4() { return static_cast<int32_t>(offsetof(GroupinColorNew_t4212717807, ___started_4)); }
	inline bool get_started_4() const { return ___started_4; }
	inline bool* get_address_of_started_4() { return &___started_4; }
	inline void set_started_4(bool value)
	{
		___started_4 = value;
	}

	inline static int32_t get_offset_of_existingItems_5() { return static_cast<int32_t>(offsetof(GroupinColorNew_t4212717807, ___existingItems_5)); }
	inline List_1_t2719087314 * get_existingItems_5() const { return ___existingItems_5; }
	inline List_1_t2719087314 ** get_address_of_existingItems_5() { return &___existingItems_5; }
	inline void set_existingItems_5(List_1_t2719087314 * value)
	{
		___existingItems_5 = value;
		Il2CppCodeGenWriteBarrier((&___existingItems_5), value);
	}

	inline static int32_t get_offset_of_topMargin_6() { return static_cast<int32_t>(offsetof(GroupinColorNew_t4212717807, ___topMargin_6)); }
	inline float get_topMargin_6() const { return ___topMargin_6; }
	inline float* get_address_of_topMargin_6() { return &___topMargin_6; }
	inline void set_topMargin_6(float value)
	{
		___topMargin_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPINCOLORNEW_T4212717807_H
#ifndef GROUPINCOLOR_T2175596517_H
#define GROUPINCOLOR_T2175596517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Library.UI.GroupinColorModel.GroupinColor
struct  GroupinColor_t2175596517  : public MonoBehaviour_t1158329972
{
public:
	// Library.Code.Facades.ModelGrouping.ModelGroupFacade Library.UI.GroupinColorModel.GroupinColor::_modelGroupFacade
	RuntimeObject* ____modelGroupFacade_2;
	// Library.Code.UI.PrefabGenerator.PrefabGenerator Library.UI.GroupinColorModel.GroupinColor::_prefabGenerator
	RuntimeObject* ____prefabGenerator_3;
	// System.Boolean Library.UI.GroupinColorModel.GroupinColor::started
	bool ___started_4;

public:
	inline static int32_t get_offset_of__modelGroupFacade_2() { return static_cast<int32_t>(offsetof(GroupinColor_t2175596517, ____modelGroupFacade_2)); }
	inline RuntimeObject* get__modelGroupFacade_2() const { return ____modelGroupFacade_2; }
	inline RuntimeObject** get_address_of__modelGroupFacade_2() { return &____modelGroupFacade_2; }
	inline void set__modelGroupFacade_2(RuntimeObject* value)
	{
		____modelGroupFacade_2 = value;
		Il2CppCodeGenWriteBarrier((&____modelGroupFacade_2), value);
	}

	inline static int32_t get_offset_of__prefabGenerator_3() { return static_cast<int32_t>(offsetof(GroupinColor_t2175596517, ____prefabGenerator_3)); }
	inline RuntimeObject* get__prefabGenerator_3() const { return ____prefabGenerator_3; }
	inline RuntimeObject** get_address_of__prefabGenerator_3() { return &____prefabGenerator_3; }
	inline void set__prefabGenerator_3(RuntimeObject* value)
	{
		____prefabGenerator_3 = value;
		Il2CppCodeGenWriteBarrier((&____prefabGenerator_3), value);
	}

	inline static int32_t get_offset_of_started_4() { return static_cast<int32_t>(offsetof(GroupinColor_t2175596517, ___started_4)); }
	inline bool get_started_4() const { return ___started_4; }
	inline bool* get_address_of_started_4() { return &___started_4; }
	inline void set_started_4(bool value)
	{
		___started_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPINCOLOR_T2175596517_H
#ifndef GROUPINCOLORMOBILEIMP_T1767066759_H
#define GROUPINCOLORMOBILEIMP_T1767066759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI.GroupinColorMobileImp
struct  GroupinColorMobileImp_t1767066759  : public GroupinColor_t2175596517
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPINCOLORMOBILEIMP_T1767066759_H
#ifndef GROUPINCOLORMOBILEIMPNEW_T3384506007_H
#define GROUPINCOLORMOBILEIMPNEW_T3384506007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UI.GroupinColorMobileImpNew
struct  GroupinColorMobileImpNew_t3384506007  : public GroupinColorNew_t4212717807
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPINCOLORMOBILEIMPNEW_T3384506007_H



// System.Void Library.UI.GroupinColorModel.GroupinColor::.ctor()
extern "C"  void GroupinColor__ctor_m454818965 (GroupinColor_t2175596517 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Library.UI.GroupinColorModel.GroupinColor::addElementToList(UnityEngine.Transform,Library.Code.Domain.Dtos.ModelGroupDto)
extern "C"  void GroupinColor_addElementToList_m662047957 (GroupinColor_t2175596517 * __this, Transform_t3275118058 * ___contentTransform0, ModelGroupDto_t1808254547 * ___group1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Library.UI.GroupinColorModel.GroupinColorNew::.ctor()
extern "C"  void GroupinColorNew__ctor_m3189157697 (GroupinColorNew_t4212717807 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Library.UI.GroupinColorModel.GroupinColorNew::addElementToList(UnityEngine.Transform,Library.Code.Domain.Dtos.ModelGroupDto)
extern "C"  void GroupinColorNew_addElementToList_m2555246161 (GroupinColorNew_t4212717807 * __this, Transform_t3275118058 * ___contentTransform0, ModelGroupDto_t1808254547 * ___group1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Library.Code.UI.MessagesList.MessageListBuilder::.ctor()
extern "C"  void MessageListBuilder__ctor_m2998812739 (MessageListBuilder_t1028067940 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UI.GroupinColorMobileImp::.ctor()
extern "C"  void GroupinColorMobileImp__ctor_m4030707220 (GroupinColorMobileImp_t1767066759 * __this, const RuntimeMethod* method)
{
	{
		GroupinColor__ctor_m454818965(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI.GroupinColorMobileImp::addElementToList(UnityEngine.Transform,Library.Code.Domain.Dtos.ModelGroupDto)
extern "C"  void GroupinColorMobileImp_addElementToList_m2952347622 (GroupinColorMobileImp_t1767066759 * __this, Transform_t3275118058 * ___contentTransform0, ModelGroupDto_t1808254547 * ___group1, const RuntimeMethod* method)
{
	{
		// base.addElementToList(contentTransform,@group);
		Transform_t3275118058 * L_0 = ___contentTransform0;
		ModelGroupDto_t1808254547 * L_1 = ___group1;
		// base.addElementToList(contentTransform,@group);
		GroupinColor_addElementToList_m662047957(__this, L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UI.GroupinColorMobileImpNew::.ctor()
extern "C"  void GroupinColorMobileImpNew__ctor_m1861839896 (GroupinColorMobileImpNew_t3384506007 * __this, const RuntimeMethod* method)
{
	{
		GroupinColorNew__ctor_m3189157697(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UI.GroupinColorMobileImpNew::addElementToList(UnityEngine.Transform,Library.Code.Domain.Dtos.ModelGroupDto)
extern "C"  void GroupinColorMobileImpNew_addElementToList_m2720276890 (GroupinColorMobileImpNew_t3384506007 * __this, Transform_t3275118058 * ___contentTransform0, ModelGroupDto_t1808254547 * ___group1, const RuntimeMethod* method)
{
	{
		// base.addElementToList(contentTransform, @group);
		Transform_t3275118058 * L_0 = ___contentTransform0;
		ModelGroupDto_t1808254547 * L_1 = ___group1;
		// base.addElementToList(contentTransform, @group);
		GroupinColorNew_addElementToList_m2555246161(__this, L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UI.MessageListBuilderImp::.ctor()
extern "C"  void MessageListBuilderImp__ctor_m2684975437 (MessageListBuilderImp_t1204586794 * __this, const RuntimeMethod* method)
{
	{
		MessageListBuilder__ctor_m2998812739(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
