
#import "ShapePickerContainerViewController.h"
#import "Cordeel_iOS_App-Swift.h"

@interface ShapePickerContainerViewController ()

@property (nonatomic, strong) ShapePickerViewController *controllerShapePick;

@end

@implementation ShapePickerContainerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	self.preferredContentSize = CGSizeMake(320, 200);
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	if ([segue.destinationViewController isKindOfClass:[ShapePickerViewController class]]) {
        ShapePickerViewController *controller = segue.destinationViewController;
		[controller setDelegate:self];
        controller.drawingShapes = self.drawingShapes;
	}
}


#pragma mark - Actions

- (IBAction)buttonShapeSizeClicked:(UIButton *)sender {
	_buttonShapeSizeLarge.selected = NO;
	_buttonShapeSizeMedium.selected = NO;
	_buttonShapeSizeSmall.selected = NO;
	sender.selected = YES;
}

#pragma mark - ShapePicking delegate


- (void)shapePicker:(ShapePickerViewController *)sender didSelectShape:(DrawingShape *)shape {
	ShapeSize size = ShapeSizeSmall;
	if (_buttonShapeSizeMedium.selected) {
		size = ShapeSizeMedium;
	}
	else if (_buttonShapeSizeLarge.selected) {
		size = ShapeSizeLarge;
	}
    shape.size = size;
	if (_delegate && [_delegate respondsToSelector:@selector(shapePicker:didSelectShape:)]) {
		[_delegate shapePicker:sender didSelectShape:shape];
	}

}

@end
