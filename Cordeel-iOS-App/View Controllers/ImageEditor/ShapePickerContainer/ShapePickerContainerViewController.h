
#import "ShapePickerViewController.h"

@interface ShapePickerContainerViewController : UIViewController <ShapePicking>

@property (nonatomic, weak) id<ShapePicking> delegate;

@property (strong, nonatomic) IBOutlet UIButton *buttonShapeSizeSmall;
@property (strong, nonatomic) IBOutlet UIButton *buttonShapeSizeMedium;
@property (strong, nonatomic) IBOutlet UIButton *buttonShapeSizeLarge;
@property (strong, nonatomic) NSArray<DrawingShape *> *drawingShapes;

- (IBAction)buttonShapeSizeClicked:(UIButton *)sender;

@end
