
#import "PDFDrawPathView.h"
#import <QuartzCore/QuartzCore.h>

@interface PDFDrawPathView() {
}

- (CAShapeLayer *)shapeLayer;

@end

@implementation PDFDrawPathView

- (void)commonInit {
    
    self.path = [[UIBezierPath alloc] init];
    
    self.backgroundColor = [UIColor clearColor];
    self.userInteractionEnabled = NO;
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInit];
    }
    return self;
}

- (CAShapeLayer *)shapeLayer
{
    return (CAShapeLayer *)self.layer;
}

+ (Class)layerClass {
    return [CAShapeLayer class];
}

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self commonInit];
    }
    
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [self.path stroke];
}

- (void)drawPath:(UIBezierPath *)path width:(CGFloat)width color:(UIColor*)color {
    self.path = path;
  
    self.shapeLayer.strokeColor = color.CGColor;
    self.shapeLayer.fillColor = [UIColor clearColor].CGColor;
    self.shapeLayer.lineJoin = kCALineJoinRound;
    self.shapeLayer.lineCap = kCALineCapRound;
    self.shapeLayer.lineWidth = width;
    
    ((CAShapeLayer *)self.layer).path = self.path.CGPath;
    NSLog(@"1");
}

@end
