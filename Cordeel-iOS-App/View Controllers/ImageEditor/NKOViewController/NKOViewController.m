

#import "NKOViewController.h"
#import "NKOColorPickerView.h"

@interface NKOViewController()

@property (nonatomic, weak) IBOutlet NKOColorPickerView *pickerView;
@property (nonatomic, weak) IBOutlet UIButton *button;

@end

@implementation NKOViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.preferredContentSize = CGSizeMake(320, 568);
    
    __weak NKOViewController *weakSelf = self;
    
    [self.pickerView setDidChangeColorBlock:^(UIColor *color){
        [weakSelf _customizeButton];
    }];
    
    [self.pickerView setTintColor:[UIColor darkGrayColor]];
    
    [self _customizeButton];
	[self setupView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

#pragma mark - Private methods

- (void)setupView {
//    if (![Helper isNilOrEmpty:[DefaultUser shared].user.company.alertTextCritical]) {
//        _labelCritical.text = [DefaultUser shared].user.company.alertTextCritical;
//    }
//    if (![Helper isNilOrEmpty:[DefaultUser shared].user.company.alertTextVeryImportant]) {
//        _labelVeryImportant.text = [DefaultUser shared].user.company.alertTextVeryImportant;
//    }
//    if (![Helper isNilOrEmpty:[DefaultUser shared].user.company.alertTextImportant]) {
//        _labelImportant.text = [DefaultUser shared].user.company.alertTextImportant;
//    }
//    if (![Helper isNilOrEmpty:[DefaultUser shared].user.company.alertTextOK]) {
//        _labelOk.text = [DefaultUser shared].user.company.alertTextOK;
//    }
//    if (![Helper isNilOrEmpty:[DefaultUser shared].user.company.alertTextNone]) {
//        _labelNone.text = [DefaultUser shared].user.company.alertTextNone;
//    }

    _viewCritical.backgroundColor = [UIColor yellowColor];
    _viewVeryImportant.backgroundColor = [UIColor greenColor];
    _viewImportant.backgroundColor = [UIColor blueColor];
    _viewOk.backgroundColor = [UIColor redColor];
    _viewNone.backgroundColor = [UIColor blackColor];


    _viewCritical.clipsToBounds = YES;
	_viewCritical.layer.cornerRadius = 8;
	_viewVeryImportant.clipsToBounds = YES;
	_viewVeryImportant.layer.cornerRadius = 8;
	_viewImportant.clipsToBounds = YES;
	_viewImportant.layer.cornerRadius = 8;
	_viewOk.clipsToBounds = YES;
	_viewOk.layer.cornerRadius = 8;
	_viewNone.clipsToBounds = YES;
	_viewNone.layer.cornerRadius = 8;
}

- (void)_customizeButton
{
    self.button.layer.cornerRadius = 6;
    self.button.backgroundColor = self.pickerView.color;
}

- (IBAction)_onButtonClick:(id)sender
{
	if (self.delegate && [self.delegate respondsToSelector:@selector(colorPicker:didPickColor:)]) {
		[_delegate colorPicker:self didPickColor:self.button.backgroundColor];
	}
}

#pragma mark - Actions

- (IBAction)buttonQuickClicked:(UIButton *)sender {
	sender.selected = YES;
	_buttonColor.selected = NO;
	_viewQuickColors.hidden = NO;
	_pickerView.hidden = YES;
	[self.view bringSubviewToFront:_viewQuickColors];
}

- (IBAction)buttonColorClicked:(UIButton *)sender {
	sender.selected = YES;
	_buttonQuick.selected = NO;
	_viewQuickColors.hidden = YES;
	_pickerView.hidden = NO;
	[self.view bringSubviewToFront:_pickerView];
}

- (IBAction)buttonQuickColorClicked:(UIButton *)sender {
	if (_delegate && [_delegate respondsToSelector:@selector(colorPicker:didPickColor:)]) {
		if ([sender isEqual:_buttonQuickColorCritical]) {
			[_delegate colorPicker:self didPickColor: [UIColor yellowColor]];
		}
		else if ([sender isEqual:_buttonQuickColorVeryImportant]) {
			[_delegate colorPicker:self didPickColor: [UIColor greenColor]];
		}
		else if ([sender isEqual:_buttonQuickColorImportant]) {
			[_delegate colorPicker:self didPickColor: [UIColor blueColor]];
		}
		else if ([sender isEqual:_buttonQuickColorOk]) {
			[_delegate colorPicker:self didPickColor: [UIColor redColor]];
		}
		else if ([sender isEqual:_buttonQuickColorNone]) {
			[_delegate colorPicker:self didPickColor: [UIColor blackColor]];
		}
	}
}
@end
