

@protocol ColorPicker <NSObject>

-(void)colorPicker:(id)sender didPickColor:(UIColor*)color;

@end

@interface NKOViewController : UIViewController

@property (nonatomic, weak) id<ColorPicker> delegate;
@property (strong, nonatomic) IBOutlet UIView *viewQuickColors;
@property (strong, nonatomic) IBOutlet UIButton *buttonQuick;
@property (strong, nonatomic) IBOutlet UIButton *buttonColor;

@property (strong, nonatomic) IBOutlet UIButton *buttonQuickColorCritical;
@property (strong, nonatomic) IBOutlet UIButton *buttonQuickColorVeryImportant;
@property (strong, nonatomic) IBOutlet UIButton *buttonQuickColorImportant;
@property (strong, nonatomic) IBOutlet UIButton *buttonQuickColorOk;
@property (strong, nonatomic) IBOutlet UIButton *buttonQuickColorNone;

@property (strong, nonatomic) IBOutlet UIView *viewCritical;
@property (strong, nonatomic) IBOutlet UIView *viewVeryImportant;
@property (strong, nonatomic) IBOutlet UIView *viewImportant;
@property (strong, nonatomic) IBOutlet UIView *viewOk;
@property (strong, nonatomic) IBOutlet UIView *viewNone;

@property (strong, nonatomic) IBOutlet UILabel *labelCritical;
@property (strong, nonatomic) IBOutlet UILabel *labelVeryImportant;
@property (strong, nonatomic) IBOutlet UILabel *labelImportant;
@property (strong, nonatomic) IBOutlet UILabel *labelOk;
@property (strong, nonatomic) IBOutlet UILabel *labelNone;


- (IBAction)buttonQuickClicked:(UIButton *)sender;
- (IBAction)buttonColorClicked:(UIButton *)sender;

- (IBAction)buttonQuickColorClicked:(UIButton *)sender;

@end
