

typedef enum : NSInteger {
	ShapeSizeSmall = 0,
	ShapeSizeMedium,
	ShapeSizeLarge
} ShapeSize;

@class ShapePickerViewController;
@class DrawingShape;
@protocol ShapePicking <NSObject>

@optional
- (void)shapePicker:(ShapePickerViewController*)sender didSelectShape:(DrawingShape *)shape;

@end

@interface ShapePickerViewController : UICollectionViewController

@property (nonatomic, strong) NSArray<DrawingShape*> *drawingShapes;
@property (nonatomic, weak) id<ShapePicking> delegate;

@end
