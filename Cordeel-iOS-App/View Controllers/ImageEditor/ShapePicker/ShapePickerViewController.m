
#import "ShapePickerViewController.h"
#import "ShapePickCollectionViewCell.h"
#import "Cordeel_iOS_App-Swift.h"

@interface ShapePickerViewController() <UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end

@implementation ShapePickerViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	self.preferredContentSize = CGSizeMake(320, 150);
    [self.collectionView reloadData];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.drawingShapes == nil) {
        self.drawingShapes = [DrawingShapesProvider provideDrawings];
        [self.spinner stopAnimating];
        [self.collectionView reloadData];
    }
}

#pragma mark - Private methods

- (UIImage*)templateImageAtIndex:(NSInteger)index {
    DrawingShape *shape = self.drawingShapes[index];
    return [shape toPreviewImage];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
	return self.drawingShapes.count;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
	ShapePickCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellShape" forIndexPath:indexPath];
	cell.imageViewShape.image = [self templateImageAtIndex:indexPath.row];
    cell.tintColor = [UIColor blackColor];
	return cell;
}

#pragma mark - UICollectionView delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
	if (_delegate && [_delegate respondsToSelector:@selector(shapePicker:didSelectShape:)]) {
        DrawingShape *shape = self.drawingShapes[indexPath.row];
        [_delegate shapePicker:self didSelectShape:shape];
	}
}

#pragma mark - UICollectionView layout delegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    UIImage *image = [self templateImageAtIndex:indexPath.row];
	CGSize imageSize = CGSizeMake(71, 61);
	imageSize.height+=10;
	imageSize.width+=10;
	return imageSize;
}

@end
