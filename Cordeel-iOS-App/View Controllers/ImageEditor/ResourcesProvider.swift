
import Foundation

@objc protocol ResourcesProvider: class {

    func resources() -> [String]
}

