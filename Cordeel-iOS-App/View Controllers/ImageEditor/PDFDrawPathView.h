

@interface PDFDrawPathView : UIView


@property (strong, nonatomic) UIBezierPath *path;

- (void)drawPath:(UIBezierPath *)path width:(CGFloat)width color:(UIColor*)color;
@end
