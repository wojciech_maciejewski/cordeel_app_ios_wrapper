
import UIKit

class EditPath: NSObject {
    
    var path: CGMutablePath
    var transformTranslate: CGAffineTransform
    var transformOthers: CGAffineTransform
    var color: UIColor
    var lineWidth: CGFloat
    var button: OBShapedButton
    
    init(path: CGMutablePath,transformTranslate: CGAffineTransform,transformOthers: CGAffineTransform, color: UIColor, lineWidth: CGFloat, button: OBShapedButton) {
        self.path = path
        self.transformTranslate = transformTranslate
        self.transformOthers = transformOthers
        self.color = color
        self.lineWidth = lineWidth
        self.button = button
    }
}
