
import Foundation

@objc class ShapeResourcesProvider: NSObject, ResourcesProvider {

    func resources() -> [String] {
        return [
            "ia_1.png",
            "ia_2.png",
            "ia_3.png",
            "ia_4.png",
            "ia_5.png",
            "ia_6.png",
            "ia_7.png",
            "ia_8.png",
            "ia_9.png",
            "ia_10.png",
            "ia_11.png",
            "ia_12.png",
            "ia_13.png",
            "ia_14.png",
            "ia_15.png",
            "ia_16.png",
            "ia_17.png",
            "ia_18.png",
            "ia_19.png",
            "ia_20.png",
            "ia_21.png",
            "ia_22.png",
            "ia_23.png",
            "ia_24.png",
            "ia_25.png",
            "ia_26.png",
            "ia_27.png",
            "ia_28.png",
            "ia_29.png",
            "ia_30.png",
            "ia_31.png",
            "ia_32.png",
            "ia_33.png",
            "ia_34.png",
            "ia_35.png",
            "ia_36.png",
            "ia_37.png"
        ]
    }

}
