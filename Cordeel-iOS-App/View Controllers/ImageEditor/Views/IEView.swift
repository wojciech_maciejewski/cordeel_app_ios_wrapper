

import UIKit

class IEView: UIImageView {

	var topLeft = CGPoint(x: 0, y: 0)
	
	required init(coder aDecoder: NSCoder) {
	    fatalError("init(coder:) has not been implemented")
	}

	init(image: UIImage!, point: CGPoint) {
		topLeft = point
		super.init(image: image)
	}

}
