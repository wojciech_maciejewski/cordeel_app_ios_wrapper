
import UIKit

enum ImageEditMode  {
	case draw
	case drawLine
	case drawShape
	case write
	case select
    case move
}

private var shapeAssociationKey: UInt8 = 0
private var colorAssociationKey: UInt8 = 1
private var scaleAssociationKey: UInt8 = 2
private var angleAssociationKey: UInt8 = 3

extension OBShapedButton {
	var shape: DrawingShape? {
		get {
            return objc_getAssociatedObject(self, &shapeAssociationKey) as? DrawingShape
        }
		set (newValue) {
            objc_setAssociatedObject(self, &shapeAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
		}
	}
	var color : UIColor? {
		get {
			return objc_getAssociatedObject(self, &colorAssociationKey) as? UIColor
		}
		set (newValue) {
			objc_setAssociatedObject(self, &colorAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
		}
	}
	var scale : CGFloat {
		get {
			if let scaleValue = objc_getAssociatedObject(self, &scaleAssociationKey) as? CGFloat {
				return scaleValue
			}
			else {
				return 1
			}
		}
		set (newValue) {
			objc_setAssociatedObject(self, &scaleAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
		}
	}
	
	var angle : CGFloat {
		get {
			if let angleValue = objc_getAssociatedObject(self, &angleAssociationKey) as? CGFloat {
				return angleValue
			}
			else {
				return 0
			}
		}
		set (newValue) {
			objc_setAssociatedObject(self, &angleAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
		}
	}
}

let sizeTextView = CGSize(width: 150, height: 60)
private let kCommentFontSize : CGFloat = 28
let textPadding : CGFloat = 5.0

@objc protocol ImageEditorSelection: class {
	func didSelectElement(_ element : UIView, sender : AnyObject)
	func didCreateElement(_ element : UIView, sender : ImageEditorView)
}

let kPointMinDistance : CGFloat = 5.0
let kPointMinDistanceSquared = kPointMinDistance * kPointMinDistance

class ImageEditorView: UIImageView, UITextViewDelegate {
	
	// MARK: Properties
	fileprivate let panSelector : Selector = #selector(ImageEditorView.handlePan(_:))
	fileprivate let tapSelector : Selector = #selector(ImageEditorView.handleTap(_:))
	fileprivate let pinchSelector : Selector = #selector(ImageEditorView.handlePinch(_:))
	fileprivate var gesturePan = UIPanGestureRecognizer()
	fileprivate var gestureTap = UITapGestureRecognizer()
	fileprivate var gesturePinch = UIPinchGestureRecognizer()
	
	fileprivate var lastPinchFirstTouch : CGPoint?
	fileprivate var lastPinchSecondTouch : CGPoint?
	
	var penColor = UIColor(white: 1, alpha: 0.9)
	fileprivate var textColor = UIColor(red: 49/255, green: 49/255, blue: 49/255, alpha: 1)
	internal var lineWidth : CGFloat = 2.5
	
	internal var selectedItem : OBShapedButton?
	
	internal var mode : ImageEditMode = .draw {
		willSet(newValue) {
			if newValue == .select && mode != .select {
				for button in drawings {
					button.isUserInteractionEnabled = true;
				}
            }else if newValue != .select && mode == .select {
				for button in drawings {
					button.isUserInteractionEnabled = false;
				}
            }
			selectedItem?.layer.borderWidth = 0
			selectedItem?.layer.borderColor = UIColor.clear.cgColor
			selectedItem = nil
			textFieldEnterText.removeFromSuperview()
			textFieldEnterText.text = ""
		}
        didSet {
            self.isUserInteractionEnabled = !(mode == .move)
            let  superView = self.superview!
            if let scrollView = superView as? UIScrollView {
                if mode == .move {
                    scrollView.pinchGestureRecognizer?.isEnabled = true
                    scrollView.isScrollEnabled = true
                }else{
                    scrollView.pinchGestureRecognizer?.isEnabled = false
                    scrollView.isScrollEnabled = false
                }
            }
        }
	}
	internal weak var delegate : ImageEditorSelection?
	
	internal lazy var drawings : [OBShapedButton] = Array()
	fileprivate var points : [CGPoint] = Array()
	
	fileprivate let originalImage : UIImage
	fileprivate var imageViewTemp = UIImageView()
	fileprivate var oldImage  = UIImage()
	
	fileprivate var textFieldEnterText = UITextView(frame: CGRect(x: 0, y: 0, width: sizeTextView.width, height: sizeTextView.height))

	fileprivate var lastTouch = CGPoint(x: 0, y: 0)
	fileprivate var touchOffset = CGPoint(x: 0, y: 0)
	fileprivate var lastCenter = CGPoint(x: 0, y: 0)
	fileprivate var startAngle : CGFloat = 0
	
	//indicates that a line draw is in progress
	internal var drawing = true {
		didSet (oldValue) {
			if drawing && gesturePan.view != nil {
//				self.removeGestureRecognizer(gesturePan)
			}
			else if !drawing && gesturePan.view == nil {
//				self.addGestureRecognizer(gesturePan)
			}
		}
	}
	
	fileprivate var currentPoint : CGPoint?
	fileprivate var previousPoint : CGPoint?
	fileprivate var previousPreviousPoint : CGPoint?
	
	fileprivate var path : CGMutablePath?
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect)
    {
        // Drawing code
    }
    */
	
	// MARK: View lifecycle

	required init?(coder aDecoder: NSCoder) {
		originalImage = UIImage()
	    super.init(coder: aDecoder)
	}
    
    override init(image: UIImage?) {
		if let targetImage = image {
			originalImage = targetImage
		}
		else {
			originalImage = UIImage()
		}
		super.init(image: originalImage)
		contentMode = .scaleAspectFill
		self.image = originalImage
		gesturePan = UIPanGestureRecognizer(target: self, action: panSelector)
		gesturePan.maximumNumberOfTouches = 1;
		gesturePan.cancelsTouchesInView = false
		gestureTap = UITapGestureRecognizer(target: self, action: tapSelector)
		gesturePinch = UIPinchGestureRecognizer(target: self, action: pinchSelector)
		self.addGestureRecognizer(gesturePan)
		self.addGestureRecognizer(gestureTap)
		self.addGestureRecognizer(gesturePinch)
		isUserInteractionEnabled = true
        clipsToBounds = true
	}

	// MARK: Private methods
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		if drawing {
			let touch = touches.first
			
			// initializes our point records to current location
			previousPoint = touch?.previousLocation(in: self);
			previousPreviousPoint = touch?.previousLocation(in: self);
			currentPoint = touch?.location(in: self)
		
			path = CGMutablePath()
		
			imageViewTemp.frame = bounds;
			imageViewTemp.removeFromSuperview()
			imageViewTemp.image = UIImage()
			addSubview(imageViewTemp)
			points.append(lastTouch)
		
			// call touchesMoved:withEvent:, to possibly draw on zero movement
			touchesMoved(touches, with: event)
		}
	}
	
	override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
		if let _ = previousPoint, let _ = previousPreviousPoint, let _ = currentPoint, drawing {
			let touch = touches.first
			
			let point = touch?.location(in: self)
			
			// if the finger has moved less than the min dist ...
			let dx = point!.x - currentPoint!.x;
			let dy = point!.y - currentPoint!.y;
			
			if ((dx * dx + dy * dy) < kPointMinDistanceSquared) {
				// ... then ignore this movement
//				print("Ignore")
//				return;
			}
			
			// update points: previousPrevious -> mid1 -> previous -> mid2 -> current
			previousPreviousPoint = previousPoint
			previousPoint = touch?.previousLocation(in: self);
			currentPoint = touch?.location(in: self)
		
			points.append(currentPoint!)
		
			let mid1 = midPoint(previousPoint!, p2: previousPreviousPoint!)
			let mid2 = midPoint(currentPoint!, p2: previousPoint!)
			
			// to represent the finger movement, create a new path segment,
			// a quadratic bezier path from mid1 to mid2, using previous as a control point
			let subpath = CGMutablePath();
            subpath.move(to: CGPoint(x: mid1.x, y: mid1.y))
            subpath.addQuadCurve(to: CGPoint(x: mid2.x, y: mid2.y), control: CGPoint(x:previousPoint!.x, y:previousPoint!.y))
			
			// compute the rect containing the new segment plus padding for drawn line
			let bounds = subpath.boundingBox;
			let _ = bounds.insetBy(dx: -2.0 * lineWidth, dy: -2.0 * lineWidth);
			
			// append the quad curve to the accumulated path so far.
			path!.addPath(subpath)
			
			UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0);
			let context = UIGraphicsGetCurrentContext();
			context!.addPath(path!);
			
			context!.setLineCap(.round);
			context!.setLineWidth(lineWidth);
			context!.setStrokeColor(penColor.cgColor);
			
			context!.strokePath();
			
			autoreleasepool(invoking: { () -> () in
				self.imageViewTemp.image = UIGraphicsGetImageFromCurrentImageContext()
			})
			UIGraphicsEndImageContext();
			
			//		CGPathRelease(subpath);
		}
	}
	
	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		if let _ = previousPoint, let _ = previousPreviousPoint, let _ = currentPoint, drawing {
			if let drawnImage = imageViewTemp.image {
//				let boundingRect = boundingRectFromPoints(points)
				var boundingRect = path!.boundingBox.insetBy(dx: -2, dy: -2);
                boundingRect = boundingRect.intersection(self.bounds)
//				if boundingRect.size.height * boundingRect.size.width > 10 {
					var scaledRect : CGRect
					if UIScreen.main.responds(to: #selector(NSDecimalNumberBehaviors.scale)) {
						scaledRect = CGRect(x: boundingRect.origin.x * UIScreen.main.scale,
							y: boundingRect.origin.y * UIScreen.main.scale,
							width: boundingRect.size.width * UIScreen.main.scale,
							height: boundingRect.size.height * UIScreen.main.scale);
					}
					else {
						scaledRect = boundingRect
					}
					if let imageRef = drawnImage.cgImage!.cropping(to: scaledRect) {
						let viewDrawn = OBShapedButton()
						viewDrawn.setImage(UIImage(cgImage: imageRef), for: UIControlState())
						viewDrawn.sizeToFit()
						viewDrawn.frame = boundingRect;
						viewDrawn.isUserInteractionEnabled = mode == .select
						viewDrawn.addTarget(self, action: #selector(ImageEditorView.elementSelected(_:)), for: .touchUpInside)
						drawings.append(viewDrawn)
						self.addSubview(viewDrawn)
						self.image = originalImage
						imageViewTemp.removeFromSuperview()
						delegate?.didCreateElement(viewDrawn, sender: self)
					}
//				}
			}
			imageViewTemp.removeFromSuperview()
			points.removeAll(keepingCapacity: false)
		}
	}
	
	override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.touchesEnded(touches, with: event)
	}
	
	func midPoint(_ p1 : CGPoint, p2 : CGPoint) -> CGPoint {
		return CGPoint(x: (p1.x + p2.x) * 0.5, y: (p1.y + p2.y) * 0.5);
	}
	
    @objc internal func handlePan(_ sender : UIPanGestureRecognizer) {
		//Handle pan events
		/*
		*	Draw is handled in touch events
		*/
		if mode == .draw {
			return
			// MARK: Draw Logic
			if sender.state == .began {
				//start recording movement
				lastTouch = sender.location(in: self)
				imageViewTemp.frame = bounds;
				imageViewTemp.removeFromSuperview()
				imageViewTemp.image = UIImage()
				addSubview(imageViewTemp)
				points.append(lastTouch)
			}
			else if sender.state == .changed {
				//update drawing
				let touchPoint = sender.location(in: self)
				/*UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0);
				imageViewTemp.image?.drawInRect(self.bounds)
				let currentContext = UIGraphicsGetCurrentContext()
				CGContextMoveToPoint(currentContext, lastTouch.x, lastTouch.y)
				CGContextAddLineToPoint(currentContext, touchPoint.x, touchPoint.y)
				CGContextSetStrokeColorWithColor(currentContext, penColor.CGColor)
				CGContextSetLineCap(currentContext, CGLineCap.Round)
				if UIScreen.mainScreen().respondsToSelector("scale" as Selector) {
					CGContextSetLineWidth(currentContext, lineWidth * UIScreen.mainScreen().scale)
				}
				else {
					CGContextSetLineWidth(currentContext, lineWidth)
				}
				
				CGContextSetBlendMode(currentContext, CGBlendMode.Normal)
				CGContextStrokePath(currentContext)
				autoreleasepool({ () -> () in
					self.imageViewTemp.image = UIGraphicsGetImageFromCurrentImageContext()
				})
				UIGraphicsEndImageContext();*/
				points.append(touchPoint)
				lastTouch = touchPoint
			}
			else if sender.state == .ended {
				//add the drawing as a new subview
				if let drawnImage = imageViewTemp.image {
					let boundingRect = boundingRectFromPoints(points)
					if boundingRect.size.height * boundingRect.size.width > 10 {
						var scaledRect : CGRect
						if UIScreen.main.responds(to: #selector(NSDecimalNumberBehaviors.scale)) {
							scaledRect = CGRect(x: boundingRect.origin.x * UIScreen.main.scale,
								y: boundingRect.origin.y * UIScreen.main.scale,
								width: boundingRect.size.width * UIScreen.main.scale,
								height: boundingRect.size.height * UIScreen.main.scale);
						}
						else {
							scaledRect = boundingRect
						}
						if let imageRef = drawnImage.cgImage!.cropping(to: scaledRect) {
							let viewDrawn = OBShapedButton()
							viewDrawn.setImage(UIImage(cgImage: imageRef), for: UIControlState())
							viewDrawn.sizeToFit()
							viewDrawn.frame = boundingRect;
							viewDrawn.isUserInteractionEnabled = mode == .select
							viewDrawn.addTarget(self, action: #selector(ImageEditorView.elementSelected(_:)), for: .touchUpInside)
							drawings.append(viewDrawn)
							self.addSubview(viewDrawn)
							self.image = originalImage
							imageViewTemp.removeFromSuperview()
							delegate?.didCreateElement(viewDrawn, sender: self)
						}
					}
				}
				imageViewTemp.removeFromSuperview()
				points.removeAll(keepingCapacity: false)
			}
			
		}
		else if mode == .select {
			//MARK: Move logic
			if let item = selectedItem {
				if sender.state == .began {
					lastTouch = sender.location(in: self)
					touchOffset = CGPoint(x: item.center.x-lastTouch.x, y: item.center.y-lastTouch.y)
				}
				else if sender.state == .changed {
					let point = sender.location(in: self)
					item.center = CGPoint(x: item.center.x+(point.x-lastTouch.x)+touchOffset.x, y: item.center.y+(point.y-lastTouch.y)+touchOffset.y)
					lastTouch = item.center
				}
			}
		}
		else if mode == .drawLine {
			// MARK: Draw line logic
			if sender.state == .began {
				//start recording movement
				touchOffset = sender.location(in: self)
				imageViewTemp.frame = bounds;
				imageViewTemp.removeFromSuperview()
				imageViewTemp.image = UIImage()
				addSubview(imageViewTemp)
			}
			else if sender.state == .changed {
				//update drawing
				let touchPoint = sender.location(in: self)
				UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0);
				let currentContext = UIGraphicsGetCurrentContext()
				currentContext!.move(to: CGPoint(x: touchOffset.x, y: touchOffset.y))
				currentContext!.addLine(to: CGPoint(x: touchPoint.x, y: touchPoint.y))
				currentContext!.setStrokeColor(penColor.cgColor)
				currentContext!.setLineCap(CGLineCap.round)
				if UIScreen.main.responds(to: #selector(NSDecimalNumberBehaviors.scale)) {
					currentContext!.setLineWidth(lineWidth * UIScreen.main.scale)
				}
				else {
					currentContext!.setLineWidth(lineWidth)
				}
				
				currentContext!.setBlendMode(CGBlendMode.normal)
				currentContext!.strokePath()
				autoreleasepool(invoking: { () -> () in
					self.imageViewTemp.image = UIGraphicsGetImageFromCurrentImageContext()
				})
				UIGraphicsEndImageContext();
				lastTouch = touchPoint
				
			}
			else if sender.state == .ended {
				//add the drawing as a new subview
				if let drawnImage = imageViewTemp.image {
					let boundingRect = boundingRectFromPoints([lastTouch, touchOffset])
					var scaledRect : CGRect
					if UIScreen.main.responds(to: #selector(NSDecimalNumberBehaviors.scale)) {
						scaledRect = CGRect(x: boundingRect.origin.x * UIScreen.main.scale,
							y: boundingRect.origin.y * UIScreen.main.scale,
							width: boundingRect.size.width * UIScreen.main.scale,
							height: boundingRect.size.height * UIScreen.main.scale);
					}
					else {
						scaledRect = boundingRect
					}
					if let imageRef = drawnImage.cgImage!.cropping(to: scaledRect) {
						let viewDrawn = OBShapedButton()
						viewDrawn.setImage(UIImage(cgImage: imageRef), for: UIControlState())
						viewDrawn.sizeToFit()
						viewDrawn.frame = boundingRect;
						viewDrawn.isUserInteractionEnabled = mode == .select
						viewDrawn.addTarget(self, action: #selector(ImageEditorView.elementSelected(_:)), for: .touchUpInside)
						drawings.append(viewDrawn)
						self.addSubview(viewDrawn)
						self.image = originalImage
						delegate?.didCreateElement(viewDrawn, sender: self)
					}
				}
				points.removeAll(keepingCapacity: false)
				imageViewTemp.removeFromSuperview()
            }
		}
		
	}
	
    @objc internal func handleTap(_ sender : UITapGestureRecognizer) {
		if mode == .write {
			if let _ = textFieldEnterText.superview {
				textFieldEnterText.removeFromSuperview()
			}
			else {
				let tapPoint = sender.location(in: self)
				textFieldEnterText.frame = CGRect(x: tapPoint.x, y: tapPoint.y, width: sizeTextView.width, height: sizeTextView.height)
				textFieldEnterText.delegate = self;
				textFieldEnterText.font = UIFont.systemFont(ofSize: kCommentFontSize)
				textFieldEnterText.layer.borderWidth = 1
				textFieldEnterText.layer.borderColor = UIColor.lightGray.cgColor
				textFieldEnterText.textColor = textColor
				textFieldEnterText.backgroundColor = UIColor.white
				textFieldEnterText.becomeFirstResponder()
				self.addSubview(textFieldEnterText)
			}
		}
		else if mode == .select {
			selectedItem?.layer.borderColor = UIColor.clear.cgColor
			selectedItem?.layer.borderWidth = 0
			selectedItem = nil
		}
	}
	
    @objc internal func handlePinch(_ sender : UIPinchGestureRecognizer) {
		if mode == .drawShape {
			if sender.state == .began {
				//start recording movement
				touchOffset = sender.location(in: self)
				imageViewTemp.frame = bounds;
				imageViewTemp.removeFromSuperview()
				imageViewTemp.image = UIImage()
				addSubview(imageViewTemp)
			}
			else if sender.state == .changed && sender.numberOfTouches > 1 {
				//update drawing
				let touchPoint = sender.location(ofTouch: 1, in: self)
				UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0);
				let currentContext = UIGraphicsGetCurrentContext()
				touchOffset = sender.location(ofTouch: 0, in: self)
				let center = sender.location(in: self)
				currentContext!.setStrokeColor(penColor.cgColor)
				currentContext!.setLineCap(CGLineCap.round)
				if UIScreen.main.responds(to: #selector(NSDecimalNumberBehaviors.scale)) {
					currentContext!.setLineWidth(lineWidth * UIScreen.main.scale)
				}
				else {
					currentContext!.setLineWidth(lineWidth)
				}
				
				currentContext!.setBlendMode(CGBlendMode.normal)
				currentContext!.strokePath()
				autoreleasepool(invoking: { () -> () in
					self.imageViewTemp.image = UIGraphicsGetImageFromCurrentImageContext()
				})
				UIGraphicsEndImageContext();
				lastTouch = touchPoint
				lastCenter = center
				
			}
			else if sender.state == .ended{
				//add the drawing as a new subview
				if let drawnImage = imageViewTemp.image {
					let radius = max(fabs(touchOffset.y-lastTouch.y), fabs(touchOffset.x-lastTouch.x))+4*lineWidth
					let boundingRect = CGRect(x: lastCenter.x-radius/2, y: lastCenter.y-radius/2, width: radius, height: radius)
//					let boundingRect = boundingRectFromPoints([lastTouch, touchOffset])
					var scaledRect : CGRect
					if UIScreen.main.responds(to: #selector(NSDecimalNumberBehaviors.scale)) {
						scaledRect = CGRect(x: boundingRect.origin.x * UIScreen.main.scale,
						                        y: boundingRect.origin.y * UIScreen.main.scale,
						                        width: boundingRect.size.width * UIScreen.main.scale,
						                        height: boundingRect.size.height * UIScreen.main.scale);
					}
					else {
						scaledRect = boundingRect
					}
					if let imageRef = drawnImage.cgImage!.cropping(to: scaledRect) {
						let viewDrawn = OBShapedButton()
						viewDrawn.setImage(UIImage(cgImage: imageRef), for: UIControlState())
						viewDrawn.sizeToFit()
						viewDrawn.frame = boundingRect;
						viewDrawn.isUserInteractionEnabled = mode == .select
						viewDrawn.addTarget(self, action: #selector(ImageEditorView.elementSelected(_:)), for: .touchUpInside)
						drawings.append(viewDrawn)
						self.addSubview(viewDrawn)
						self.image = originalImage
						delegate?.didCreateElement(viewDrawn, sender: self)
					}
				}
				points.removeAll(keepingCapacity: false)
				imageViewTemp.removeFromSuperview()
			}
		}
		else if mode == .select {
			//MARK: Move logic
			if let item = selectedItem {
				if sender.state == .began && sender.numberOfTouches > 1 {
					lastTouch = sender.location(in: self)
					touchOffset = CGPoint(x: item.center.x-lastTouch.x, y: item.center.y-lastTouch.y)
					let firstTouch = sender.location(ofTouch: 1, in: self)
					let secondTouch = sender.location(ofTouch: 0, in: self)
					let deltaY = firstTouch.y-secondTouch.y
					let deltaX = firstTouch.x-secondTouch.x
					startAngle = atan(deltaY/deltaX)
				}
				else if sender.state == .changed && sender.numberOfTouches > 1 {
					let point = sender.location(in: self)
					let totalScale = item.scale*sender.scale
					item.center = CGPoint(x: item.center.x+(point.x-lastTouch.x)+touchOffset.x, y: item.center.y+(point.y-lastTouch.y)+touchOffset.y)
					lastTouch = item.center
					if let _ = selectedItem,
                        let shape = selectedItem?.shape,
                        let color = selectedItem?.color,
                        let image = item.image(for: UIControlState()) {
						item.setImage(self.adjustShapeImage(image, shape: shape, scale: totalScale, color: color), for: UIControlState())
					}
					item.layer.borderWidth = 1/totalScale
					item.transform = CGAffineTransform(scaleX: totalScale, y: totalScale)
					let firstTouch = sender.location(ofTouch: 1, in: self)
					let secondTouch = sender.location(ofTouch: 0, in: self)
					let deltaY = firstTouch.y-secondTouch.y
					let deltaX = firstTouch.x-secondTouch.x
					let angle = atan(deltaY/deltaX)-startAngle+item.angle
					item.transform = item.transform.rotated(by: angle)					
					lastPinchFirstTouch = firstTouch
					lastPinchSecondTouch = secondTouch
				}
				else if sender.state == .ended {
					item.scale = sender.scale*item.scale
					if let firstTouch = lastPinchFirstTouch, let secondTouch = lastPinchSecondTouch {
						/*let firstTouch = sender.locationOfTouch(1, inView: self)
						let secondTouch = sender.locationOfTouch(0, inView: self)*/
						let deltaY = firstTouch.y-secondTouch.y
						let deltaX = firstTouch.x-secondTouch.x
						item.angle = atan(deltaY/deltaX)-startAngle+item.angle
						lastPinchFirstTouch = nil
						lastPinchSecondTouch = nil
					}

				}
			}
		}
	}
	
	/**
	*	Calculate top leftmost and bottom rightmost point from array of points
	**/
	fileprivate func boundingRectFromPoints(_ points : [CGPoint]) -> CGRect {
		var topLeft = points.count > 0 ? points[0] : CGPoint(x: 0, y: 0)
		var bottomRight = points.count > 0 ? points[0] : CGPoint(x: 0, y: 0)
		for point in points {
			//dont include negative values, only interested in bounds
			if bounds.contains(point) {
				if topLeft.x > point.x {
					topLeft.x = point.x
				}
				if topLeft.y > point.y {
					topLeft.y = point.y
				}
				if bottomRight.x < point.x {
					bottomRight.x = point.x
				}
				if bottomRight.y < point.y {
					bottomRight.y = point.y
				}
			}
			
		}
		return CGRect(x: topLeft.x - lineWidth, y: topLeft.y - lineWidth, width: bottomRight.x - topLeft.x + lineWidth*2, height: bottomRight.y - topLeft.y + lineWidth*2)
	}
	
	fileprivate func calculateTextViewFrameForText(_ text : String) {
		let textRect = (text as NSString).boundingRect(with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font : textFieldEnterText.font!], context: nil);
		let textSize = CGSize(width: textRect.size.width+2*textPadding, height: textRect.size.height+2*textPadding)
		
		var calculatedFrame = textFieldEnterText.frame
		if textSize.width > sizeTextView.width {
			calculatedFrame = CGRect(x: calculatedFrame.origin.x, y: calculatedFrame.origin.y, width: textSize.width, height: calculatedFrame.size.height)
		}
		if textSize.height > sizeTextView.height {
			calculatedFrame = CGRect(x: calculatedFrame.origin.x, y: calculatedFrame.origin.y, width: calculatedFrame.size.width, height: textSize.height)
		}
		self.textFieldEnterText.frame = calculatedFrame
		
	}
	
	internal func adjustShapeImage(_ image: UIImage, shape: DrawingShape, scale: CGFloat, color: UIColor) -> UIImage? {
		UIGraphicsBeginImageContextWithOptions(image.size, false, 0);
		let currentContext = UIGraphicsGetCurrentContext()
        let image = shape.toImage(renderingColor: color)
        let bounds = CGRect(origin: CGPoint.zero, size: image.size)
        image.draw(in: bounds)
		currentContext!.setStrokeColor(color.cgColor)
		currentContext!.setLineCap(CGLineCap.round)
		if UIScreen.main.responds(to: #selector(NSDecimalNumberBehaviors.scale)) {
			currentContext!.setLineWidth((lineWidth * UIScreen.main.scale)/scale)
		}
		else {
			currentContext!.setLineWidth(lineWidth/scale)
		}
		
		currentContext!.setBlendMode(CGBlendMode.normal)
		currentContext!.strokePath()
		var scaledImage : UIImage?
		autoreleasepool(invoking: { () -> () in
			scaledImage = UIGraphicsGetImageFromCurrentImageContext()
		})
		UIGraphicsEndImageContext();
		
		return scaledImage
	}
	
    internal func draw(shape: DrawingShape) -> OBShapedButton? {
		var multiplier : CGFloat = 0.8*0.75
		if shape.size == ShapeSizeMedium {
			multiplier /= 2
		}
		else if shape.size == ShapeSizeSmall {
			multiplier /= 16
		}

		let radiusSelf = min(self.bounds.size.height*multiplier+2*lineWidth, self.bounds.size.width*multiplier+2*lineWidth)
      
        var zoomScale: CGFloat = 1.0
        var offset: CGPoint = CGPoint.zero
        let  superView = self.superview!
        if let scrollView = superView as? UIScrollView {
            zoomScale = scrollView.zoomScale
            offset = scrollView.contentOffset
        }
        let radiusSuper = min(superView.bounds.size.width/zoomScale, superView.bounds.size.height/zoomScale)
        let radius = min(radiusSelf, radiusSuper)
        let centerSelf = CGPoint(x: self.bounds.size.width/2, y: self.bounds.size.height/2)
        let centerSuper = CGPoint(x: superView.bounds.size.width/2, y: superView.bounds.size.height/2)
        
        let centerX = (centerSelf.x < centerSuper.x/zoomScale) ? centerSelf.x : ((centerSuper.x+offset.x)/zoomScale)
        let centerY = (centerSelf.y < centerSuper.y/zoomScale) ? centerSelf.y : ((centerSuper.y+offset.y)/zoomScale)
        let center = CGPoint(x: centerX, y: centerY)
    

        let boundingRect = CGRect(x: center.x-radius/2-2*lineWidth, y: center.y-radius/2-2*lineWidth, width: radius+4*lineWidth, height: radius+4*lineWidth)
        var scaledRect : CGRect
        if UIScreen.main.responds(to: #selector(NSDecimalNumberBehaviors.scale)) {
            scaledRect = CGRect(x: boundingRect.origin.x * UIScreen.main.scale,
                                y: boundingRect.origin.y * UIScreen.main.scale,
                                width: boundingRect.size.width * UIScreen.main.scale,
                                height: boundingRect.size.height * UIScreen.main.scale);
        }
        else {
            scaledRect = boundingRect
        }

        let viewDrawn = OBShapedButton()
        let snapshot = shape.toImage()
        viewDrawn.setImage(snapshot, for: UIControlState())
        viewDrawn.tintColor = self.penColor
        viewDrawn.sizeToFit()
        viewDrawn.frame = boundingRect;
        viewDrawn.isUserInteractionEnabled = mode == .select
        viewDrawn.addTarget(self, action: #selector(ImageEditorView.elementSelected(_:)), for: .touchUpInside)

        drawings.append(viewDrawn)
        self.addSubview(viewDrawn)
        self.image = originalImage
        viewDrawn.color = penColor.copy() as? UIColor
        viewDrawn.shape = shape
        return viewDrawn

	}

	
	// MARK: Public methods
	/**
	*	Get the image with the edits for export
	**/
	internal func editedImage()->UIImage! {
		//clear selection (if active)
		selectedItem?.layer.borderWidth = 0
		selectedItem?.layer.borderColor = UIColor.clear.cgColor
		selectedItem = nil
		if UIScreen.main.responds(to: #selector(NSDecimalNumberBehaviors.scale)) {
			UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
		}
		else {
			UIGraphicsBeginImageContext(self.bounds.size);
		}
		if let context = UIGraphicsGetCurrentContext() {
			self.layer.render(in: context);
			autoreleasepool { () -> () in
				self.image = UIGraphicsGetImageFromCurrentImageContext();
			}
			UIGraphicsEndImageContext();
			return image!;
		}
		return UIImage()
	}
	
    @objc internal func elementSelected(_ sender : OBShapedButton) {
		if mode != .select {
			return
		}
		selectedItem?.layer.borderWidth = 0
		selectedItem?.layer.borderColor = UIColor.clear.cgColor
		selectedItem = nil
		delegate?.didSelectElement(sender, sender: self)
		selectedItem = sender
		selectedItem?.layer.borderColor = UIColor.lightGray.cgColor
		selectedItem?.layer.borderWidth = 1
	}
	
	internal func deleteSelectedItem() {
		if let item = selectedItem {
			if let index = drawings.index(of: item) {
				drawings.remove(at: index)
			}
			item.removeFromSuperview()
			selectedItem = nil
		}
	}
	// MARK: UITextView delegate
	func textViewDidEndEditing(_ textView: UITextView) {
		let text = OBShapedButton()
		text.titleLabel?.font = UIFont.systemFont(ofSize: kCommentFontSize)
		text.contentEdgeInsets = UIEdgeInsets(top: textPadding, left: textPadding, bottom: textPadding, right: textPadding)
		text.setTitleColor(textColor, for: UIControlState())
		text.setTitle(textFieldEnterText.text, for: UIControlState())
		text.frame = CGRect(origin: textFieldEnterText.frame.origin, size: CGSize.zero)
		text.isUserInteractionEnabled = mode == .select
		text.titleLabel?.numberOfLines = 0;
		text.titleLabel?.lineBreakMode = .byWordWrapping
		text.addTarget(self, action: #selector(ImageEditorView.elementSelected(_:)), for: .touchUpInside)
		text.backgroundColor = UIColor.white
		self.addSubview(text)
		text.sizeToFit()
		drawings.append(text)
		
		delegate?.didCreateElement(text, sender: self)
		
		textFieldEnterText.text = ""
		textFieldEnterText.removeFromSuperview()
	}
	
	func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
		let textToAdd = (textView.text as NSString).replacingCharacters(in: range,
			with:text)
		calculateTextViewFrameForText(textToAdd)
		return true
	}
}
