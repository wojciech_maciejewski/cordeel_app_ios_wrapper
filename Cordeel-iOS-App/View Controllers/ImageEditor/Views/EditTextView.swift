
import UIKit

class EditTextView: NSObject {
    
    var frame: CGRect
    var text: NSString?
    var font: UIFont?
    var transformTranslate: CGAffineTransform
    var transformOthers: CGAffineTransform
    var button: OBShapedButton
    
    init(frame: CGRect, text: NSString?, font: UIFont?, transformTranslate: CGAffineTransform,transformOthers: CGAffineTransform, button: OBShapedButton) {
        self.frame = frame
        self.text = text
        self.font = font
        self.transformTranslate = transformTranslate
        self.transformOthers = transformOthers
        self.button = button
    }
}
