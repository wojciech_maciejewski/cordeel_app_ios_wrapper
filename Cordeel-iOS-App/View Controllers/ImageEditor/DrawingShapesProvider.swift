
import Foundation

@objc class DrawingShapesProvider: NSObject {

    private static let shared = DrawingShapesProvider()
    private var drawingsShapes: [DrawingShape]?

    @objc class func provideDrawings() -> [DrawingShape] {
        let sharedInstance = DrawingShapesProvider.shared
        if sharedInstance.drawingsShapes == nil {
            let resourcesProvider = ShapeResourcesProvider()
            sharedInstance.drawingsShapes = sharedInstance.loadDrawingShapes(provider: resourcesProvider)
        }
        return sharedInstance.drawingsShapes!
    }

    @objc func loadDrawingShapes(provider: ResourcesProvider) -> [DrawingShape] {
        var shapes: [DrawingShape] = []
        provider.resources().forEach {
            shapes.append(DrawingShape(resourceName: $0))
        }
        return shapes
    }

}

