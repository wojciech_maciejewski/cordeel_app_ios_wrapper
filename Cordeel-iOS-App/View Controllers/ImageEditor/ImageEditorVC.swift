
import UIKit

let kTagImageAlertViewClearAll = 5

@objc protocol ImageEditorDelegate {
    func didFinishEditingImage(_ sender : AnyObject, editedImage : UIImage, imageFromCamera: Bool) -> Void
	func didCancelEditingImage(_ sender : AnyObject) -> Void
	func didCreateElement(_ element : UIImage, sender : AnyObject) -> Void
}

class ImageEditorVC: UIViewController, ColorPicker, UIAlertViewDelegate, ShapePicking,UIScrollViewDelegate {

	var imageToEdit : UIImage?
	var editor = ImageEditorView(image: UIImage())
    internal var imageFromCamera: Bool = false
    var keepImageSize: Bool = false
    @IBOutlet weak var moveBtn: UIButton!
	
	@IBOutlet var editButtons: [UIButton]!
	var editDelegate : ImageEditorDelegate?

	@IBOutlet weak var buttonDismiss: UIButton!
	@IBOutlet weak var viewEditor: UIScrollView!
	@IBOutlet weak var buttonUse: UIButton!
	@IBOutlet var buttonSelectShape: UIButton!
	@IBOutlet var buttonSelectElement: UIButton!
    private var drawingShapes: [DrawingShape]?

	var penColor : UIColor? = UIColor(red: 0.0, green: 122.0/255, blue: 255.0/255, alpha: 1.0)

    static func instantiate(with image: UIImage, delegate: ImageEditorDelegate?) -> ImageEditorVC {
        let controller = UIStoryboard(name: "ImageEditor", bundle: nil).instantiateInitialViewController() as! ImageEditorVC
        controller.imageToEdit = image
        controller.editDelegate = delegate
        return controller
    }

	override func viewDidLoad() {
		super.viewDidLoad()
        createImageEditor();
		buttonDismiss.layer.borderColor = UIColor.lightGray.cgColor
		buttonDismiss.layer.borderWidth = 1
		buttonUse.layer.borderColor = UIColor.lightGray.cgColor
		buttonUse.layer.borderWidth = 1
	}
    
    fileprivate func createImageEditor() {
        guard let existingImage = imageToEdit else {
            return;
        }
        
        if keepImageSize {
            editor = ImageEditorView(image: existingImage)
        }else {
            editor = ImageEditorView(image: adjustImageSize(existingImage))
            moveBtn.isHidden = true;
        }
        viewEditor.addSubview(editor)
        if let color = penColor {
            editor.penColor = color
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewEditor.isHidden = true
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateImageEditorLayout()
        viewEditor.isHidden = false
        guard let existingImage = imageToEdit else {
            return;
        }
        viewEditor.maximumZoomScale = 1.0;
        viewEditor.minimumZoomScale = 1.0;
        viewEditor.zoomScale = 1.0;
        if keepImageSize && isBiggerThanScrollView() {
            viewEditor.minimumZoomScale = min(viewEditor.frame.width/existingImage.size.width, viewEditor.frame.height/existingImage.size.height)
        }
        viewEditor.zoomScale = viewEditor.minimumZoomScale
        viewEditor.pinchGestureRecognizer?.isEnabled = false
        viewEditor.isScrollEnabled = false
        editor.drawing = false
        editor.mode = .move

    }

    fileprivate func updateImageEditorLayout() {
        guard let existingImage = imageToEdit else {
            return;
        }
        
        if keepImageSize {
            editor.bounds = CGRect(x: 0,
                                   y: 0,
                                   width: existingImage.size.width,
                                   height: existingImage.size.height)
            if isBiggerThanScrollView() {
                viewEditor.contentSize = existingImage.size
                moveBtn.isHidden = false
            }else {
                viewEditor.contentSize = existingImage.size
                moveBtn.isHidden = true
            }
            editor.center = CGPoint(x: viewEditor.frame.width / 2.0, y: viewEditor.frame.height / 2.0);
            var imageRect = editor.frame;
            if (imageRect.origin.x < 0) {
                imageRect.origin.x = 0
            }
            if (imageRect.origin.y < 0) {
                imageRect.origin.y = 0
            }
            editor.frame = imageRect
        }else {
            editor.frame = CGRect(x: 0,
                                  y: 0,
                                  width: viewEditor.frame.width,
                                  height: viewEditor.frame.height)
            moveBtn.isHidden = true
        }
    }
    
    fileprivate func isBiggerThanScrollView() -> Bool {
        guard let existingImage = imageToEdit else {
            return false
        }
        
        var ret = false
        
        if max(existingImage.size.width, existingImage.size.height) >
            max(viewEditor.frame.width, viewEditor.frame.height) {
            ret = true
        }
        
        return ret
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return editor
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        let offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width) ? (scrollView.bounds.size.width - scrollView.contentSize.width)/2 : 0.0
        let offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height) ? (scrollView.bounds.size.height - scrollView.contentSize.height)/2 : 0.0
        editor.center = CGPoint(x: scrollView.contentSize.width/2 + offsetX,y: scrollView.contentSize.height/2 + offsetY)
    }

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "segueChooseColor" {
			if let destination = segue.destination as? NKOViewController {
				destination.delegate = self
			}
		}
		else if segue.identifier == "seguePickShape" {
			if let destination = segue.destination as? ShapePickerContainerViewController {
				destination.delegate = self
                destination.drawingShapes = self.drawingShapes
			}
		}
	}
	
	// MARK: Private methods
	
	func markButtonState(_ button : UIButton) {
		if !button.isSelected {
			for member in editButtons {
				member.isSelected = false
				member.alpha = 1
			}
			button.isSelected = true
			button.alpha = 0.5
		}
	}
	
	func clearEdits() {
		for member in editor.drawings {
			member.removeFromSuperview()
		}
	}
	
	fileprivate func adjustImageSize(_ targetImage : UIImage) -> UIImage {
		var calculatedSize = targetImage.size
		var screenSize = UIScreen.main.bounds.size
		let screenScale = UIScreen.main.scale
		screenSize.width *= screenScale
		screenSize.height *= screenScale
		
		if targetImage.size.height > screenSize.height {
			let scaleFactorHeight = screenSize.height / calculatedSize.height
			calculatedSize.height = screenSize.height
			calculatedSize.width *= scaleFactorHeight
			
			if calculatedSize.width > screenSize.width {
				let scaleFactorWidth = screenSize.width / calculatedSize.width
				calculatedSize.width = screenSize.width
				calculatedSize.height *= scaleFactorWidth
			}
		}
		else if targetImage.size.width > screenSize.width {
			let scaleFactorWidth = calculatedSize.width / targetImage.size.width
			calculatedSize.width = screenSize.width
			calculatedSize.height *= scaleFactorWidth
			
			if calculatedSize.height > screenSize.height {
				let scaleFactorHeight = screenSize.height / calculatedSize.height
				calculatedSize.height = screenSize.height
				calculatedSize.width *= scaleFactorHeight
			}
		}
		
		return scale(targetImage, newSize: calculatedSize)
	}
	
	fileprivate func scale(_ image : UIImage, newSize : CGSize) -> UIImage {
		var scaledSize = newSize
		let scaleFactor : CGFloat = newSize.height/image.size.height
		
		scaledSize.width = newSize.width * scaleFactor
		scaledSize.height = newSize.height * scaleFactor
		
		UIGraphicsBeginImageContextWithOptions(scaledSize, false, 0.0 );
		let scaledImageRect = CGRect(x: 0.0, y: 0.0, width: scaledSize.width, height: scaledSize.height );
		image.draw(in: scaledImageRect)
		let scaledImage = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		return scaledImage!;
	}
	
	// MARK: Actions
    @IBAction func buttonMoveClicked(_ sender: UIButton) {
        editor.drawing = false
        editor.mode = .move
        markButtonState(sender)
    }

	@IBAction func buttonCommentClicked(_ sender: UIButton) {
		editor.drawing = false
		editor.mode = .write
		markButtonState(sender)
	}
	
	@IBAction func buttonColorsClicked(_ sender: UIButton) {
//		performSegueWithIdentifier("segueChooseColor", sender: nil);
	}
	
	@IBAction func buttonRulerClicked(_ sender: UIButton) {
		editor.drawing = false
		editor.mode = .drawLine
		markButtonState(sender)
	}
	
	@IBAction func buttonPenClicked(_ sender: UIButton) {
		editor.drawing = true
		editor.mode = .draw
		markButtonState(sender)
	}
	
	@IBAction func buttonMainClicked(_ sender: UIButton) {
		let alert = UIAlertView(title: NSLocalizedString("Clear edits", comment: ""), message: NSLocalizedString("Are you sure you wish to clear all edits?", comment: ""), delegate: self, cancelButtonTitle: NSLocalizedString("Cancel", comment: ""), otherButtonTitles: NSLocalizedString("Clear", comment: ""))
		alert.tag = kTagImageAlertViewClearAll
		alert.show()
		
	}
	
	@IBAction func buttonSelectionClicked(_ sender: AnyObject) {
		editor.drawing = false
		editor.mode = .select
		markButtonState(sender as! UIButton)
	}

	@IBAction func buttonDeleteClicked(_ sender: AnyObject) {
		editor.deleteSelectedItem()
	}
	@IBAction func buttonDismissClicked(_ sender: UIButton) {
		if let existingDelegate = editDelegate {
			existingDelegate.didCancelEditingImage(self)
		}
		else {
			dismiss(animated: true, completion: nil)
		}
	}
	
	@IBAction func buttonUseClicked(_ sender: UIButton) {
		if let existingDelegate = editDelegate {
            existingDelegate.didFinishEditingImage(self, editedImage: editor.editedImage(), imageFromCamera: imageFromCamera)
		}
		else {
			dismiss(animated: true, completion: nil)
		}
	}
	
	// MARK: ColorPicker delegate
	
	func colorPicker(_ sender: Any!, didPick color: UIColor!) {
		(sender as! UIViewController).dismiss(animated: true, completion: { () -> Void in
			self.editor.penColor = color
		})
	}
	
	// MARK: Shape Picker delegate

    func shapePicker(_ sender: ShapePickerViewController!, didSelect shape: DrawingShape) {
		sender.dismiss(animated: true, completion: { () -> Void in
			self.editor.drawing = false
			self.editor.mode = .drawShape
            self.markButtonState(self.buttonSelectShape)
            if let item = self.editor.draw(shape: shape) {
				self.editor.drawing = false
				self.editor.mode = .select
				self.markButtonState(self.buttonSelectElement)
				self.editor.elementSelected(item)
			}
		})
	}
 
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: {context in
            self.viewEditor.isHidden = true
            }, completion: {context in
                self.reloadView()
                self.viewEditor.isHidden = false
            }
        )
    }
    
    func reloadView() {
        guard let existingImage = imageToEdit else {
            return;
        }
        viewEditor.maximumZoomScale = 1.0;
        viewEditor.minimumZoomScale = 1.0;
        viewEditor.zoomScale = 1.0;
        viewEditor.contentOffset = CGPoint.zero
        updateImageEditorLayout()
        if keepImageSize && isBiggerThanScrollView() {
            viewEditor.minimumZoomScale = min(viewEditor.frame.width/existingImage.size.width, viewEditor.frame.height/existingImage.size.height)
        }
        
        viewEditor.zoomScale = viewEditor.minimumZoomScale
    }
	
	// MARK: UIAlertView delegate
	
	func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
		if alertView.tag == kTagImageAlertViewClearAll {
			if buttonIndex != alertView.cancelButtonIndex {
				clearEdits()
			}
		}
	}
}

