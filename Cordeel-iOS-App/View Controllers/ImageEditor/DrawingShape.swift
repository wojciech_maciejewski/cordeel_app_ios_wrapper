
import Foundation


@objc class DrawingShape: NSObject {

    let previewSize = CGSize(width: 162, height: 162)
    let resourceName: String
    @objc var size: ShapeSize = ShapeSizeMedium
    let image: UIImage
    let preview: UIImage

    init(resourceName: String) {
        self.resourceName = resourceName
        guard
            let image = UIImage(named: resourceName) else {
                fatalError("Error loading file '\(resourceName) image'")
        }
        self.image = image.withRenderingMode(.alwaysTemplate)
        self.preview = self.image.resize(to: self.previewSize)
        super.init()
    }

    @objc func toImage() -> UIImage {
        return image
    }

    @objc func toPreviewImage() -> UIImage {
        return preview
    }

    @objc func toImage(renderingColor color: UIColor) -> UIImage {
        return image.filledWithColor(color)
    }

}

fileprivate extension UIView {

    func toImage() -> UIImage {
        UIGraphicsBeginImageContext(self.frame.size)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }

}

fileprivate extension UIImage {

    func filledWithColor(_ color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color.setFill()

        let context = UIGraphicsGetCurrentContext()
        context!.translateBy(x: 0, y: self.size.height)
        context!.scaleBy(x: 1.0, y: -1.0);
        context!.setBlendMode(CGBlendMode.normal)

        let rect = CGRect(x:0, y:0, width:self.size.width, height:self.size.height)
        context!.clip(to: rect, mask: self.cgImage!)
        context!.fill(rect)

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }


    func resize(to size: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(size)
        self.draw(in: CGRect(origin: .zero, size: size))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }

}
