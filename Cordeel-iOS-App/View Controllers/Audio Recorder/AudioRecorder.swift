//
//  AudioRecorder.swift
//  Cordeel-iOS-App
//
//  Created by Adam on 02.06.2018.
//  Copyright © 2018 SmartAr. All rights reserved.
//
import UIKit
import AVFoundation

class AudioRecorder: UIViewController {

    typealias CompletionHandler = (String) -> Void

    private var recorder: AVAudioRecorder!
    private var player: AVAudioPlayer!
    @IBOutlet private weak var recordButton: UIButton!
    @IBOutlet private weak var stopButton: UIButton!
    @IBOutlet private weak var playButton: UIButton!
    @IBOutlet private weak var saveButton: UIButton!
    @IBOutlet private weak var cancelButton: UIButton!
    @IBOutlet private weak var statusLabel: UILabel!
    private var meterTimer: Timer!
    private var soundFileURL: URL!
    private var completionHandler: CompletionHandler?
    private weak var parentController: UIViewController?

    internal static func instantiate(inParent parent: UIViewController) -> AudioRecorder {
        let storyboard = UIStoryboard(name: "AudioRecorder", bundle: nil)
        let recorder = storyboard.instantiateViewController(withIdentifier: "AudioRecorder") as! AudioRecorder
        recorder.parentController = parent
        return recorder
    }

    internal func recordAudio(completionHandler: @escaping CompletionHandler) {
        self.completionHandler = completionHandler
        parentController?.present(self, animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setSessionPlayback()
        registerForNotifications()
        checkHeadphones()
        configureButtons()
    }

    private func configureButtons() {
        cancelButton.setTitle(TextStrings.cancel, for: .normal)
        recordButton.setTitle(TextStrings.record, for: .normal)
        stopButton.setTitle(TextStrings.stop, for: .normal)
        playButton.setTitle(TextStrings.play, for: .normal)
        saveButton.setTitle(TextStrings.save, for: .normal)
    }

    @objc private func updateAudioMeter(_ timer: Timer) {
        if let recorder = self.recorder {
            if recorder.isRecording {
                let min = Int(recorder.currentTime / 60)
                let sec = Int(recorder.currentTime.truncatingRemainder(dividingBy: 60))
                statusLabel.text = String(format: "%02d:%02d", min, sec)
                recorder.updateMeters()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        recorder = nil
        player = nil
    }

    @IBAction private func cancelButtonDidTap(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction private func saveButtonDidTap(_ sender: UIButton) {
        self.recorder = nil
        self.player = nil
        self.dismiss(animated: true) {
            self.completionHandler?(self.soundFileURL.path)
        }
    }

    @IBAction private func recordButtonDidTap(_ sender: UIButton) {
        if player != nil && player.isPlaying {
            player.stop()
        }
        if recorder == nil {
            playButton.isEnabled = false
            stopButton.isEnabled = true
            recordWithPermission(true)
            return
        }

        if recorder != nil && recorder.isRecording {
            recorder.pause()
            recordButton.setTitle(TextStrings.continue, for: .normal)

        } else {
            recordButton.setTitle(TextStrings.pause, for: .normal)
            playButton.isEnabled = false
            stopButton.isEnabled = true
            recordWithPermission(false)
        }
    }

    @IBAction private func stopButtonDidTap(_ sender: UIButton) {
        recorder?.stop()
        player?.stop()
        meterTimer.invalidate()
        recordButton.setTitle(TextStrings.record, for: .normal)
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setActive(false)
            playButton.isEnabled = true
            stopButton.isEnabled = false
            recordButton.isEnabled = true
        } catch {
            NSLog("could not make session inactive")
            NSLog(error.localizedDescription)
        }
    }

    @IBAction private func playButtonDidTap(_ sender: UIButton) {
        play()
    }

    private func play() {
        let url: URL? = self.recorder?.url ?? self.soundFileURL
        do {
            self.player = try AVAudioPlayer(contentsOf: url!)
            stopButton.isEnabled = true
            player.delegate = self
            player.prepareToPlay()
            player.volume = 1.0
            playButton.isEnabled = false
            stopButton.isEnabled = true
            player.play()
        } catch {
            self.player = nil
            NSLog(error.localizedDescription)
        }
    }

    private func setupRecorder() {
        let format = DateFormatter()
        format.dateFormat="yyyy-MM-dd-HH-mm-ss"
        let currentFileName = "recording-\(format.string(from: Date())).m4a"
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        self.soundFileURL = documentsDirectory.appendingPathComponent(currentFileName)
        if FileManager.default.fileExists(atPath: soundFileURL.absoluteString) {
            NSLog("soundfile \(soundFileURL.absoluteString) exists")
        }
        let recordSettings: [String: Any] = [
            AVFormatIDKey: kAudioFormatAppleLossless,
            AVEncoderAudioQualityKey: AVAudioQuality.max.rawValue,
            AVEncoderBitRateKey: 32000,
            AVNumberOfChannelsKey: 2,
            AVSampleRateKey: 44100.0
        ]


        do {
            recorder = try AVAudioRecorder(url: soundFileURL, settings: recordSettings)
            recorder.delegate = self
            recorder.isMeteringEnabled = true
            recorder.prepareToRecord()
        } catch {
            recorder = nil
            NSLog(error.localizedDescription)
        }
    }

    private func recordWithPermission(_ setup: Bool) {
        AVAudioSession.sharedInstance().requestRecordPermission { [unowned self] granted in
            DispatchQueue.main.async {
                if granted {
                    self.setSessionPlayAndRecord()
                    if setup { self.setupRecorder() }
                    self.recorder.record()
                    self.recordButton.setTitle(TextStrings.pause, for: .normal)
                    self.meterTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.updateAudioMeter(_:)), userInfo: nil, repeats: true)
                } else {
                    self.recordButton.setTitle(TextStrings.record, for: .normal)
                    self.presentGoToSettingsSelectionSheet()
                }
            }
        }
    }

    private func presentGoToSettingsSelectionSheet() {
        let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
        let alertPermission = UIAlertController(title: TextStrings.appNeedsAccessToMic, message: TextStrings.grantAccessManually, preferredStyle: .alert)
        alertPermission.addAction(UIAlertAction(title: TextStrings.settings, style: .default, handler: { _ in
            UIApplication.shared.openURL(settingsUrl!)
        }))
        alertPermission.addAction(UIAlertAction(title: TextStrings.cancel, style: .cancel, handler: nil))
        alertPermission.view.tintColor = .black
        self.present(alertPermission, animated: true, completion: nil)
    }

    private func setSessionPlayback() {
        let session = AVAudioSession.sharedInstance()

        do {
            try session.setCategory(AVAudioSessionCategoryPlayback, with: .defaultToSpeaker)

        } catch {
            NSLog(error.localizedDescription)
        }

        do {
            try session.setActive(true)
        } catch {
            NSLog(error.localizedDescription)
        }
    }

    private func setSessionPlayAndRecord() {
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setCategory(AVAudioSessionCategoryPlayAndRecord, with: .defaultToSpeaker)
        } catch {
            NSLog(error.localizedDescription)
        }

        do {
            try session.setActive(true)
        } catch {
            NSLog(error.localizedDescription)
        }
    }

    private func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(AudioRecorder.routeChange(_:)), name: NSNotification.Name.AVAudioSessionRouteChange, object: nil)
    }

    @objc func routeChange(_ notification: Notification) {
        if let userInfo = (notification as NSNotification).userInfo {
            if let reason = userInfo[AVAudioSessionRouteChangeReasonKey] as? UInt {
                switch AVAudioSessionRouteChangeReason(rawValue: reason)! {
                case AVAudioSessionRouteChangeReason.newDeviceAvailable:
                    checkHeadphones()
                case AVAudioSessionRouteChangeReason.oldDeviceUnavailable:
                    checkHeadphones()
                default:
                    break
                }
            }
        }
    }

    private func checkHeadphones() {
        let currentRoute = AVAudioSession.sharedInstance().currentRoute
        if !currentRoute.outputs.isEmpty {
            for description in currentRoute.outputs {
                if description.portType == AVAudioSessionPortHeadphones {
                    NSLog("headphones are plugged in")
                    break
                } else {
                    NSLog("headphones are unplugged")
                }
            }
        } else {
            NSLog("checking headphones requires a connection to a device")
        }
    }

    private func exportAsset(_ asset: AVAsset, fileName: String) {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let trimmedSoundFileURL = documentsDirectory.appendingPathComponent(fileName)
        if FileManager.default.fileExists(atPath: trimmedSoundFileURL.absoluteString) {
            do {
                if try trimmedSoundFileURL.checkResourceIsReachable() {
                    NSLog("is reachable")
                }

                try FileManager.default.removeItem(atPath: trimmedSoundFileURL.absoluteString)
            } catch {
                NSLog(error.localizedDescription)
            }

        }

        if let exporter = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetAppleM4A) {
            exporter.outputFileType = AVFileType.m4a
            exporter.outputURL = trimmedSoundFileURL

            let duration = CMTimeGetSeconds(asset.duration)
            if duration < 5.0 {
                NSLog("sound is not long enough")
                return
            }
            // e.g. the first 5 seconds
            let startTime = CMTimeMake(0, 1)
            let stopTime = CMTimeMake(5, 1)
            exporter.timeRange = CMTimeRangeFromTimeToTime(startTime, stopTime)
            exporter.exportAsynchronously(completionHandler: {
                switch exporter.status {
                case  AVAssetExportSessionStatus.failed:
                    if let e = exporter.error {
                        NSLog("export failed \(e)")
                    }

                case AVAssetExportSessionStatus.cancelled:
                    NSLog("export cancelled \(String(describing: exporter.error))")
                default:
                    NSLog("export complete")
                }
            })
        } else {
            NSLog("cannot create AVAssetExportSession for asset \(asset)")
        }

    }
}
extension AudioRecorder: AVAudioRecorderDelegate {

    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        stopButton.isEnabled = false
        playButton.isEnabled = true
        saveButton.isEnabled = true
        recordButton.setTitle(TextStrings.record, for: UIControlState())
        self.recorder = nil
    }

    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        if let e = error {
            NSLog("\(e.localizedDescription)")
        }
    }

}

extension AudioRecorder: AVAudioPlayerDelegate {

    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        recordButton.isEnabled = true
        stopButton.isEnabled = false
        playButton.isEnabled = true
    }

    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        if let e = error {
            NSLog("\(e.localizedDescription)")
        }

    }
}
