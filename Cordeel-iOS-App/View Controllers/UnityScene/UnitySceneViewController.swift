
import UIKit

class UnitySceneViewController: UIViewController {

    @IBOutlet weak var spinner: UIActivityIndicatorView!
    private lazy var imagePicker = ImagePicker(parent: self)
    private var audioRecorder: AudioRecorder?
    private let notificationCenter = NotificationCenter.default

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        notificationCenter.addObserver(self, selector: #selector(UnitySceneViewController.appDidBecomeActive), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        Unity.registerListener(self)
        Unity.start()
    }

    deinit {
       notificationCenter.removeObserver(self)
    }

    @objc func appDidBecomeActive() {
        if Unity.isRunning {
            openResourceIfLinked()
        }
    }

    private func showUnitySubView() {
        if let unityView = Unity.view {
            view?.insertSubview(unityView, at: 0)
            unityView.translatesAutoresizingMaskIntoConstraints = false
            if #available(iOS 11.0, *) {
                NSLayoutConstraint.activate([
                    unityView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                    unityView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
                    unityView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
                    unityView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
                    ])
            } else {
                let topPadding: CGFloat = UIDevice.current.isIphoneX ? 40 : 20
                NSLayoutConstraint.activate([
                    unityView.topAnchor.constraint(equalTo: view.topAnchor, constant: topPadding),
                    unityView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
                    unityView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                    unityView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
                    ])
            }
       }
    }

    private func openResourceIfLinked() {
        if let resourceId = DeepLinkParser.getLinkedResourceId() {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                DeepLinkParser.removeLinkedResource()
                Unity.makeCall(class: .main, method: .openResource, value: resourceId)
            }
        }
    }
}

extension UnitySceneViewController: UnityListener {

    func unityDidStart() {
        spinner.stopAnimating()
        showUnitySubView()
    }

    func unityDidCallPickPhoto() {
        imagePicker.pickPhoto { path in
            Unity.makeCall(class: .main, method: .imageCaptured, value: path)
        }
    }

    func unityDidCallPickAudio() {
        audioRecorder = AudioRecorder.instantiate(inParent: self)
        audioRecorder?.recordAudio { path in
             Unity.makeCall(class: .main, method: .audioCaptured, value: path)
        }
    }

}
