//
//  UnitySceneViewController+ImagePicker.swift
//  Cordeel-iOS-App
//
//  Created by Adam on 01.06.2018.
//  Copyright © 2018 SmartAr. All rights reserved.
//

import Foundation
import Photos

class ImagePicker: NSObject {

    typealias CompletionHandler = (String) -> Void

    private weak var parent: UIViewController?
    private var completionHandler: CompletionHandler?
    private var imageEditor: ImageEditorVC?
    private var picker: UIImagePickerController?

    init(parent: UIViewController) {
        super.init()
        self.parent = parent
    }

    internal func pickPhoto(completionHandler: @escaping CompletionHandler) {
        self.completionHandler = completionHandler
        presentPhotoSourceSelectionSheet()
    }

    private func presentPhotoSourceSelectionSheet() {
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            DispatchQueue.main.async {  self.presentImagePickerWithSource(.photoLibrary) }
            return
        }
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: TextStrings.cancel, style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: TextStrings.takePhoto, style: .default, handler: {(action) -> Void in
            DispatchQueue.main.async { self.presentImagePickerWithSource(.camera)  }
        }))
        alert.addAction(UIAlertAction(title: TextStrings.selectPhoto, style: .default, handler: { [weak self] _ in
            switch PHPhotoLibrary.authorizationStatus() {
            case .authorized:  DispatchQueue.main.async { self?.presentImagePickerWithSource(.photoLibrary) }
            case .denied, .restricted: self?.presentGoToSettingsSelectionSheet()
            case .notDetermined:
                PHPhotoLibrary.requestAuthorization({ [weak self] status in
                    if status == .authorized {
                        DispatchQueue.main.async {  self?.presentImagePickerWithSource(.photoLibrary) }
                    }
                })
            }
        }))
        alert.view.tintColor = .black
        if let sourceView = parent?.view {
            let sourceRect = CGRect(origin: CGPoint(x: 330, y: sourceView.bounds.height - 80), size: .zero)
            if let presenter = alert.popoverPresentationController {
                presenter.sourceView = sourceView
                presenter.sourceRect = sourceRect
                presenter.permittedArrowDirections = [.left]
            }
        }
        parent?.present(alert, animated: true, completion: nil)
    }

    private func presentGoToSettingsSelectionSheet() {
        let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
        let alertPermission = UIAlertController(title: TextStrings.appNeedsAccessToPhotoLibrary, message: TextStrings.grantAccessManually, preferredStyle: .alert)
        alertPermission.addAction(UIAlertAction(title: TextStrings.settings, style: .default, handler: { _ in
            UIApplication.shared.openURL(settingsUrl!)
        }))
        alertPermission.addAction(UIAlertAction(title: TextStrings.cancel, style: .cancel, handler: nil))
        if UIApplication.shared.applicationState == .active {
            alertPermission.view.tintColor = .black
            parent?.present(alertPermission, animated: true, completion: nil)
        }
    }

    private func presentImagePickerWithSource(_ sourceType: UIImagePickerControllerSourceType) {
        picker = UIImagePickerController()
        picker?.automaticallyAdjustsScrollViewInsets = false
        picker?.sourceType = sourceType
        if sourceType == .camera {
            picker?.cameraDevice = UIImagePickerController.isCameraDeviceAvailable(.rear) ? .rear : .front
        }
        picker?.delegate = self
        parent?.present(picker!, animated: true, completion: nil)
    }

    private func openImageEditor(with image: UIImage) {
        imageEditor = ImageEditorVC.instantiate(with: image, delegate: self)
        parent?.present(imageEditor!, animated: true, completion: nil)
    }

}

extension ImagePicker: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        picker.dismiss(animated: true, completion: { [weak self] in
            self?.openImageEditor(with: image)
        })
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            imagePickerController(picker, didFinishPickingImage: image, editingInfo: info as [String : AnyObject])
        }
    }

    func saveData(_ data: Data, toFileNamed name: String, completion: (String) -> Void) throws {
        if let url = URL.createUrlForNewFile(named: name, in: .documentDirectory) {
            try data.write(to: url, options: .atomicWrite)
            completion(url.path)
        }
    }
}

extension ImagePicker: ImageEditorDelegate {

    func didFinishEditingImage(_ sender: AnyObject, editedImage: UIImage, imageFromCamera: Bool) {
        imageEditor?.dismiss(animated: true, completion: { [weak self] in
        if let data = UIImageJPEGRepresentation(editedImage, 1.0)  {
            do {
                try self?.saveData(data, toFileNamed: UUID().uuidString + ".jpg") { path in
                    self?.completionHandler?(path)
                }
            } catch {
                NSLog("Error saving image")
            }
        }
        })
    }

    func didCancelEditingImage(_ sender: AnyObject) {
         imageEditor?.dismiss(animated: true, completion: nil)
    }

    func didCreateElement(_ element: UIImage, sender: AnyObject) {

    }

}
