//
//  TextStrings.swift
//  Cordeel-iOS-App
//
//  Created by Adam on 01.06.2018.
//  Copyright © 2018 SmartAr. All rights reserved.
//

import Foundation

struct TextStrings {

    static let cancel = "Cancel"
    static let takePhoto = "Take photo"
    static let selectPhoto = "Select photo from library"
    static let appNeedsAccessToPhotoLibrary = "Cordeel app needs access to your photo library"
    static let appNeedsAccessToMic = "Cordeel app needs access to the microphone"
    static let grantAccessManually = "Would you like to grant the access in the app's settings"
    static let settings = "Settings"
    static let `continue` = "Continue"
    static let pause = "Pause"
    static let record = "Record"
    static let stop = "Stop"
    static let play = "Play"
    static let save = "Save"

}
