//
//  Notifications.h
//  Cordeel-iOS-App
//
//  Created by Adam on 09/07/2018.
//  Copyright © 2018 SmartAr. All rights reserved.
//

#ifndef Notifications_h
#define Notifications_h

static NSString* const kUnityDidCallPickPhoto = @"UnityDidCallPickPhoto";
static NSString* const kUnityDidCallPickAudio = @"UnityDidCallPickAudio";
static NSString* const kUnityReady = @"UnityReady";

#endif /* Notifications_h */
