//
//  DeepLinkParser
//  Cordeel-iOS-App
//
//  Created by Adam on 18/06/2018.
//  Copyright © 2018 SmartAr. All rights reserved.
//

import Foundation

class DeepLinkParser {

    static let deepLinkMask = "cordeelApp://element/"
    static let userDefalults = UserDefaults.standard
    static let openResourceKey = "openResourceKey"

    static func handleDeepLink(_ link: URL) -> Bool {
        let link = link.absoluteString
        if link.contains(deepLinkMask) {
            let elementId = link.replacingOccurrences(of: deepLinkMask, with: "")
            setLinkedResource(with: elementId)
            return true
        }
        return false
    }

    static func setLinkedResource(with id: String?) {
        userDefalults.setValue(id, forKey: openResourceKey)
        userDefalults.synchronize()
    }


    static func getLinkedResourceId() -> String? {
        return userDefalults.string(forKey: openResourceKey)
    }

    static func removeLinkedResource() {
        setLinkedResource(with: nil)
    }

}
