//
//  MessageExchanger.swift
//  Cordeel-iOS-App
//
//  Created by Adam on 01.06.2018.
//  Copyright © 2018 SmartAr. All rights reserved.
//

import Foundation

@objc protocol UnityListener {

    func unityDidStart()
    func unityDidCallPickPhoto()
    func unityDidCallPickAudio()
}

class Unity: NSObject {

    private static weak var listener: UnityListener?

    internal static var isRunning: Bool { return (UIApplication.shared.delegate as? AppDelegate)?.isUnityRunning ?? false }

    internal static func registerListener(_ listener: UnityListener) {
        self.listener = listener
    }

    internal static func start() {
        let delegate = UIApplication.shared.delegate as? AppDelegate
        delegate?.startUnity()
        NotificationCenter.default.addObserver(self, selector: #selector(Unity.didStart), name: Notification.Name(kUnityReady), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(Unity.didCallPickPhoto), name: Notification.Name(kUnityDidCallPickPhoto), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(Unity.didCallPickAudio), name: Notification.Name(kUnityDidCallPickAudio), object: nil)
    }

    internal static var view: UIView? { return UnityGetGLView() }

    // call unity

    internal enum UnityClass: String {
        case main = "Main Camera"
    }

    internal enum UnityMethod: String {
        case imageCaptured
        case audioCaptured
        case openResource = "setIdOfElementToLoad"
    }

    internal static func makeCall(`class`: UnityClass, method: UnityMethod, value: String) {
        UnityPostMessage(`class`.rawValue, method.rawValue, value)
    }

    // listen to unity

    @objc internal static func didCallPickPhoto() {
        listener?.unityDidCallPickPhoto()
    }

    @objc internal static func didCallPickAudio() {
        listener?.unityDidCallPickAudio()
    }

    @objc internal static func didStart() {
        listener?.unityDidStart()
    }
}
