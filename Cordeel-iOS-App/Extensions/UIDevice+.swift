//
//  UIDevice+.swift
//  Cordeel-iOS-App
//
//  Created by Adam on 09/07/2018.
//  Copyright © 2018 SmartAr. All rights reserved.
//

import Foundation

extension UIDevice {

    var isIphoneX: Bool { return UIScreen.main.bounds.height == 812 }

}
