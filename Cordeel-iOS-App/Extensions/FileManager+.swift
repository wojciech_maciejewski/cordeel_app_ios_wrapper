//
//  FileManager+.swift
//  Cordeel-iOS-App
//
//  Created by Adam on 01.06.2018.
//  Copyright © 2018 SmartAr. All rights reserved.
//

import Foundation

extension FileManager {

    static func directoryURL(for directory: SearchPathDirectory) -> URL? {
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: directory, in: .userDomainMask)
        return urls.first
    }

}
