//
//  URL+.swift
//  Cordeel-iOS-App
//
//  Created by Adam on 01.06.2018.
//  Copyright © 2018 SmartAr. All rights reserved.
//

import Foundation

extension URL {

    static func createUrlForNewFile(named fileName: String, in directory: FileManager.SearchPathDirectory) -> URL? {
        guard let directoryURL = FileManager.directoryURL(for: directory) else {
            return nil
        }
        return directoryURL.appendingPathComponent(fileName)
    }
}
